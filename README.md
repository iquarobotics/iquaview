# IQUAview

IQUAview is the graphical user interface (GUI) that allows users to operate the family of vehicles from Iqua Robotics in a simple and user-friendly manner. IQUAview provides a front-end to the COLA2 architecture, which resides at the core of Iqua vehicles, in order to be able to communicate with the robot, configure a basic set of parameters, plan missions and monitor them using an intuitive graphical interface.

IQUAview runs on Linux platforms and is developed using the [Qt toolkit](https://www.qt.io) and the popular [QGIS](http://qgis.org) software package, thus leveraging GIS capabilities to efficiently work with georeferenced data.

[TOC]

## Basic features

* Customization of the interface display with raster maps, vector charts or contextual information of the operational site in a large number of supported data formats.
* Easy access to vehicle and sensors configuration, pre-dive checks and routine procedures (teleoperation mode, IMU calibration, status monitoring, etc).
* Graphical mission programming providing high flexibility in the design through individual waypoint customization or the addition of predefined mission patterns.
* Dual command and monitoring of the vehicle in two operational modes: WiFi connection (through surface buoy or vehicle WiFi antenna) or acoustic communication (through
USBL/modem transponder).

Besides, Iqua Robotics is providing a series of plugins designed to interface particular sensors (USBL systems, multibeams, sidescan sonars or cameras) so that their configuration and operation can be performed from IQUAview.

## User manual

Download [IQUAview's user manual](https://bitbucket.org/iquarobotics/iquaview/wiki/IQUAview_user_manual_v24.01.pdf) to learn in more detail the different features of IQUAview and how to use the interface to operate the vehicles of Iqua Robotics.
You can test IQUAview together with a simulated instance of the software architecture of IQUA's vehicles. For that, check first the [COLA2 wiki](https://bitbucket.org/iquarobotics/cola2_wiki/src/master/README.md).

## Installation

IQUAview is compatible with Ubuntu 20.04 LTS. Although the vehicle's architecture is based on the Robot Operative System (ROS), the GUI itself is ROS agnostic. However, in order to run some specific ROS nodes in the control station PC (such as the teleoperation node that must run from the PC in which the joystick is plugged) we recommend you to also install ROS. The current supported version for IQUAview is ROS Noetic. You can follow the instructions in <http://www.ros.org> to install and setup ROS.

IQUAview requires the installation of QGIS package (>=3.26 version).

* Install QGIS

    ```bash
    # To update your system repositories.
    sudo echo 'deb https://qgis.org/ubuntu focal main' > /tmp/qgis.list
    sudo cp /tmp/qgis.list /etc/apt/sources.list.d/qgis.list
    # To add required key.
    sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys D155B8E6A419C5BE
    # Update repositories info.
    sudo apt update
    # Install.
    sudo apt install qgis qgis-plugin-grass
    ```

To complete the IQUAview installation follow the next steps:

* Install external required libraries:

    ```bash
    sudo apt-get install \
        iputils-ping \
        ntpdate \
        python3-gnupg \
        python3-lxml \
        python3-pip \
        python3-pyqt5 \
        python3-pyqt5.qtmultimedia \
        python3-serial
    pip3 install \
        dataclasses \
        Flask \
        Flask-SocketIO \
        geopy \
        gevent \
        gevent-websocket \
        importlib_metadata==4.4 \
        markdown \
        matplotlib==3.7.3 \
        pysftp
    ```

* Install the cola2_lib library:

    ```bash
    git clone https://bitbucket.org/iquarobotics/cola2_lib.git
    cd cola2_lib
    # check the required dependencies and then
    mkdir build
    cd build
    cmake ..
    make
    sudo make install
    ```

* Install IQUAview library:

    ```bash
    pip3 install git+https://bitbucket.org/iquarobotics/iquaview_lib.git
    ```

* Setup SSH keys:
  To allow IQUAview to pass files between your control station and your vehicle seamlessly, you will need to set up your ssh-key either in your vehicle or in your own localhost (for simulation purposes). If you do not have any ssh-key created you can do:

    ```bash
    sudo apt-get install openssh-server
    ssh-keygen -t rsa -m PEM
    ```

  and press enter to select the default answers to the questions.

  Once you have an ssh-key you can set it up in your vehicle or in your localhost with the following command:

  ```bash
  ssh-copy-id -i ~/.ssh/id_rsa user@host
  ```

  where _id_rsa_ is the name of your key (change it if you named it differently) and adapt the _user_ and _host_ to your vehicle ip or 127.0.0.1 (for simulation setup).

* Install IQUAview package: this can be done in two different ways:
    * **Via Pip**:
      IQUAview package can be installed directly on the system without the need to download the bitbucket repository:

        ```bash
        pip3 install git+https://bitbucket.org/iquarobotics/iquaview.git
        ```

    * **Via Bitbucket**:
      If you do not want to do a system install of IQUAview, you can simply clone IQUAview repository somewhere inside your user home and you are ready to go.

        ```bash
        git clone https://bitbucket.org/iquarobotics/iquaview.git
        ```

If you are going to use IQUAview together with a vehicle from IQUA Robotics you will need also to ensure that the [iquaview_server](https://bitbucket.org/iquarobotics/iquaview_server/src/master) package is installed in your vehicle. You can follow the instructions in the package for its installation. This step can be skipped if you are going to use IQUAview only in simulation.

## Launching IQUAview

If the package has been installed via pip, you can launch IQUAview from the Ubuntu launcher or the terminal by typing:

```bash
iquaview
```

If you have simply downloaded the bitbucket repository, go inside the repository folder and type:

```bash
./run.sh
```

If you wish to create a desktop shortcut for launching the GUI, you can create a file named `iquaview.desktop` with the following content and copy it in your home’s desktop folder. Be
aware to change `IQUAVIEW_PATH` to the appropriate paths of your installation.

```none
[Desktop Entry]
Name=IQUAview
Exec=bash -c "source ~/catkin_ws/devel/setup.bash && IQUAVIEW_PATH/run.sh"
Terminal=true
Type=Application
Icon=IQUAVIEW_PATH/iquaview/src/resources/iquaview.png
```

Finally, right click on the file you just created on the Desktop and under the Permissions tab, check "Allow executing file as a program" to enable the shortcut.
