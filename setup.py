#!/usr/bin/env python

from distutils.core import setup

from iquaview.src import __version__, NAME

setup(
    name=NAME,
    version=__version__,
    author="Iqua Robotics",
    author_email="software@iquarobotics.com",
    description="IQUA Robotics graphical user interface",
    license="Read LICENSE.txt",
    packages=[
        f"{NAME}",
        f"{NAME}.src",
        f"{NAME}.src.connection",
        f"{NAME}.src.connection.settings",
        f"{NAME}.src.mapsetup",
        f"{NAME}.src.mapsetup.decoration",
        f"{NAME}.src.mission",
        f"{NAME}.src.mission.maptools",
        f"{NAME}.src.mission.missionedition",
        f"{NAME}.src.mission.missiontemplates",
        f"{NAME}.src.plugins",
        f"{NAME}.src.range_bearing",
        f"{NAME}.src.tools",
        f"{NAME}.src.tools.profile_tool",
        f"{NAME}.src.tools.web_server",
        f"{NAME}.src.tools.web_server.static",
        f"{NAME}.src.tools.web_server.static.images",
        f"{NAME}.src.tools.web_server.static.js",
        f"{NAME}.src.tools.web_server.static.js.lib",
        f"{NAME}.src.tools.web_server.static.js.lib.socket_io",
        f"{NAME}.src.tools.web_server.static.js.lib.socket_io.1_3_6",
        f"{NAME}.src.tools.web_server.static.js.lib.socket_io.4_7_4",
        f"{NAME}.src.tools.web_server.templates",
        f"{NAME}.src.tracks",
        f"{NAME}.src.ui",
        f"{NAME}.src.vehicle",
        f"{NAME}.src.vehicle.checklist",
        f"{NAME}.src.vehicle.vehiclewidgets",
    ],
    include_package_data=True,
    package_data={
        "": [
            "*.css",
            "*.html",
            "*.ico",
            "*.js",
            "*.png",
            "*.svg",
            "*.map",
        ],
        "iquaview": ["*.xml"],
    },
    entry_points={
        "gui_scripts": ["iquaview = iquaview.src.__main__:main"],
    },
    data_files=[
        ("share/applications/", ["iquaview.desktop"]),
        ("share/icons/hicolor/scalable/apps/", ["iquaview/src/resources/iquaview.png"]),
    ],
    install_requires=[
        # iquaview
        "bs4",
        "dataclasses",
        "Flask",
        "Flask-SocketIO",
        "geopy",
        "gevent",
        "gevent-websocket",
        "importlib_metadata",
        "markdown",
        "matplotlib==3.7.3",
        "pysftp",
        # iquaview_lib
        "lxml",
        "pyopenssl>=24.0.0",
        "paramiko>=2.7.2",
    ],
)
