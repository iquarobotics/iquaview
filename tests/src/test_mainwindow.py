# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import unittest

from qgis.core import QgsApplication
from qgis.gui import QgsMapToolZoom, QgsMapToolPan

from iquaview.src import mainwindow
from iquaview.src.mapsetup.movelandmarktool import MoveLandmarkTool
from iquaview.src.tools import measuretool

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)

qgs = QgsApplication([], False)


class TestMainWindow(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        print("TestMainWindow.setUpClass")
        cls.qgs = QgsApplication.instance()
        cls.qgs.initQgis()
        cls.mainwindow = mainwindow.MainWindow()
        cls.mainwindow.is_test = True

    @classmethod
    def tearDownClass(cls) -> None:
        print("TestMainWindow.tearDownClass")
        cls.mainwindow.close()
        cls.mainwindow.deleteLater()

    def test_actions(self):
        print("TestMainWindow.test_actions")
        self.mainwindow.move_landmark_point_action.trigger()
        tool = self.mainwindow.canvas.mapTool()
        self.assertIsInstance(tool, MoveLandmarkTool)

        self.mainwindow.zoom_in_action.trigger()
        tool = self.mainwindow.canvas.mapTool()
        self.assertIsInstance(tool, QgsMapToolZoom)

        self.mainwindow.pan_action.trigger()
        tool = self.mainwindow.canvas.mapTool()
        self.assertIsInstance(tool, QgsMapToolPan)

        self.mainwindow.zoom_out_action.trigger()
        tool = self.mainwindow.canvas.mapTool()
        self.assertIsInstance(tool, QgsMapToolZoom)

        self.mainwindow.measure_distance_action.trigger()
        tool = self.mainwindow.canvas.mapTool()
        self.assertIsInstance(tool, measuretool.MeasureDistanceTool)

        self.mainwindow.measure_angle_action.trigger()
        tool = self.mainwindow.canvas.mapTool()
        self.assertIsInstance(tool, measuretool.MeasureAngleTool)

        self.mainwindow.measure_area_action.trigger()
        tool = self.mainwindow.canvas.mapTool()
        self.assertIsInstance(tool, measuretool.MeasureAreaTool)

        # Hide and show legend dock
        status = self.mainwindow.legend_dock.isHidden()
        self.mainwindow.legend_action.trigger()
        self.assertEqual(not status, self.mainwindow.legend_dock.isHidden())
        self.mainwindow.legend_action.trigger()
        self.assertEqual(status, self.mainwindow.legend_dock.isHidden())

        # Hide and show log dock
        status = self.mainwindow.log_dock.isHidden()
        self.mainwindow.log_action.trigger()
        self.assertEqual(not status, self.mainwindow.log_dock.isHidden())
        self.mainwindow.log_action.trigger()
        self.assertEqual(status, self.mainwindow.log_dock.isHidden())

        # Hide and show scale bar
        status = self.mainwindow.scale_bar.isVisible()
        self.mainwindow.scale_bar_action.trigger()
        self.assertEqual(not status, self.mainwindow.scale_bar.isVisible())
        self.mainwindow.scale_bar_action.trigger()
        self.assertEqual(status, self.mainwindow.scale_bar.isVisible())

        # Hide and show north arrow
        status = self.mainwindow.north_arrow.isVisible()
        self.mainwindow.north_arrow_action.trigger()
        self.assertEqual(not status, self.mainwindow.north_arrow.isVisible())
        self.mainwindow.north_arrow_action.trigger()
        self.assertEqual(status, self.mainwindow.north_arrow.isVisible())

        # Hide and show project toolbar
        status = self.mainwindow.project_toolbar.isHidden()
        self.mainwindow.project_toolbar_action.trigger()
        self.assertEqual(not status, self.mainwindow.project_toolbar.isHidden())
        self.mainwindow.project_toolbar_action.trigger()
        self.assertEqual(status, self.mainwindow.project_toolbar.isHidden())

        # Hide and show view toolbar
        status = self.mainwindow.view_toolbar.isHidden()
        self.mainwindow.view_toolbar_action.trigger()
        self.assertEqual(not status, self.mainwindow.view_toolbar.isHidden())
        self.mainwindow.view_toolbar_action.trigger()
        self.assertEqual(status, self.mainwindow.view_toolbar.isHidden())

        # Hide and show tools toolbar
        status = self.mainwindow.tools_toolbar.isHidden()
        self.mainwindow.tools_toolbar_action.trigger()
        self.assertEqual(not status, self.mainwindow.tools_toolbar.isHidden())
        self.mainwindow.tools_toolbar_action.trigger()
        self.assertEqual(status, self.mainwindow.tools_toolbar.isHidden())

        # Hide and show mission toolbar
        status = self.mainwindow.mission_toolbar.isHidden()
        self.mainwindow.mission_toolbar_action.trigger()
        self.assertEqual(not status, self.mainwindow.mission_toolbar.isHidden())
        self.mainwindow.mission_toolbar_action.trigger()
        self.assertEqual(status, self.mainwindow.mission_toolbar.isHidden())

        # Hide and show connection toolbar
        status = self.mainwindow.connection_toolbar.isHidden()
        self.mainwindow.connection_toolbar_action.trigger()
        self.assertEqual(not status, self.mainwindow.connection_toolbar.isHidden())
        self.mainwindow.connection_toolbar_action.trigger()
        self.assertEqual(status, self.mainwindow.connection_toolbar.isHidden())


if __name__ == "__main__":
    unittest.main()
