# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import unittest
from copy import copy

from PyQt5.QtGui import QColor
from qgis.core import QgsProject, QgsApplication
from qgis.gui import QgsMapCanvas

from iquaview.src import styles
from iquaview.src.mapsetup.decoration import northarrow, scalebar
from iquaview.src.mission.missioncontroller import MissionController
from iquaview_lib.cola2api.mission_types import (Mission,
                                                 MissionStep,
                                                 MissionGoto)
from iquaview_lib.vehicle.vehicleinfo import VehicleInfo

from iquaview_lib import resources_rc

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)

from iquaview_lib.config import Config


class TestStyles(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        print("TestStyles.setUp")
        cls.app = QgsApplication.instance()
        if cls.app is None:
            cls.app = QgsApplication([], False)

        cls.config = Config()
        cls.config.load()

        xml_filename = "{}".format(os.path.abspath(os.path.expanduser(cls.config.settings['last_auv_config_xml'])))
        cls.vehicle_info = VehicleInfo(xml_filename)

        cls.proj = QgsProject.instance()
        cls.proj.setFileName("")

        cls.canvas = QgsMapCanvas()
        cls.view = None
        cls.wp_dock = None
        cls.templates_dock = None
        cls.minfo_dock = None
        cls.msg_log = QgsApplication.messageLog()
        cls.mission_name = "temp_mission"

        cls.m_ctrl = MissionController(cls.config, cls.vehicle_info, cls.proj, cls.canvas, cls.view,
                                        cls.wp_dock, cls.templates_dock, cls.minfo_dock, cls.msg_log)
        write_temp_mission_xml("mission1")
        write_temp_mission_xml("mission2")
        cls.m_ctrl.load_mission(os.getcwd() + "/mission1" + ".xml")
        cls.m_ctrl.load_mission(os.getcwd() + "/mission2" + ".xml")

        cls.scale_bar = scalebar.ScaleBar(cls.canvas, cls.config)
        cls.north_arrow = northarrow.NorthArrow(cls.canvas, cls.config)

        cls.styles_dlg = styles.StylesWidget(cls.config, cls.canvas, cls.m_ctrl, cls.north_arrow, cls.scale_bar)

    @classmethod
    def tearDownClass(cls) -> None:
        print("TestStyles.tearDown")
        os.remove("mission1.xml")
        os.remove("mission2.xml")

    def test(self):
        print("TestStyles.test")
        new_color = QColor("Red")

        def_color = self.north_arrow.default_color
        self.styles_dlg.north_arrow_color_button.setColor(new_color)
        self.assertEqual(new_color.name(), self.north_arrow.current_color)
        self.styles_dlg.reset_color_north_arrow_btn.click()
        self.assertEqual(def_color.name(), self.styles_dlg.north_arrow_color_button.color().name())

        def_color = self.scale_bar.get_default_color()
        self.styles_dlg.change_scale_bar_color(new_color)
        self.assertEqual(new_color.name(), self.scale_bar.get_current_color().name())
        self.styles_dlg.reset_color_scale_bar_btn.click()
        self.assertEqual(def_color.name(), self.styles_dlg.scalebar_color_button.color().name())

        def_color = self.styles_dlg.start_end_color_button.color()
        self.styles_dlg.start_end_color_button.setColor(new_color)
        for mt in self.m_ctrl.get_mission_list():
            self.assertEqual(new_color.name(), mt.get_current_start_end_marker_color().name())
        self.styles_dlg.missions_combo_box.setCurrentIndex(1)
        self.styles_dlg.change_start_end_color(new_color)
        self.styles_dlg.reset_color_start_end_btn.click()
        self.assertEqual(def_color.name(), self.styles_dlg.start_end_color_button.color().name())

        def_color = self.styles_dlg.missions_color_button.color()
        self.styles_dlg.missions_color_button.setColor(new_color)
        for mt in self.m_ctrl.get_mission_list():
            self.assertEqual(new_color.name(), mt.get_current_track_color().name())
        self.styles_dlg.missions_combo_box.setCurrentIndex(1)
        self.styles_dlg.change_missions_color(new_color)
        self.styles_dlg.reset_color_missions_btn.click()
        self.assertEqual(def_color.name(), self.styles_dlg.missions_color_button.color().name())

        self.styles_dlg.save_as_defaults()

        self.styles_dlg.close()


def write_temp_mission_xml(mission_name):
    mission = Mission()

    wp1 = MissionGoto(-41.777, 3.030, 15.0, 0.0, 0, 0.5, 2.0, True)
    wp2 = MissionGoto(-41.787, 3.034, 15.0, 0.0, 0, 0.5, 2.0, True)
    wp3 = MissionGoto(-41.777, 3.030, 15.0, 0.0, 0, 0.5, 2.0, True)

    mission_step1 = MissionStep()
    mission_step2 = MissionStep()
    mission_step3 = MissionStep()

    mission_step1.add_maneuver(wp1)
    mission_step2.add_maneuver(wp2)
    mission_step3.add_maneuver(wp3)

    mission.add_step(mission_step1)
    mission.add_step(mission_step2)
    mission.add_step(mission_step3)

    mission.write_mission(mission_name + '.xml')


if __name__ == "__main__":
    unittest.main()
