# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import unittest

from qgis.core import QgsApplication
from iquaview_lib.config import Config
from iquaview_lib.vehicle.vehicleinfo import VehicleInfo
from iquaview_lib.xmlconfighandler.auvconfigcheckxml import ConfigCheckXML

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)


class TestAuvConfigCheckXML(unittest.TestCase):

    def setUp(self):
        print("TestAuvConfigCheckXML.setUp")
        self.app = QgsApplication.instance()
        if self.app is None:
            self.app = QgsApplication([], False)
        self.config = Config()
        self.config.load()

        xml_filename = "{}".format(os.path.abspath(os.path.expanduser(self.config.settings['last_auv_config_xml'])))
        self.vehicle_info = VehicleInfo(xml_filename)

        self.config_check_xml = ConfigCheckXML(self.config)

    def test_exists(self):
        print("TestAuvConfigCheckXML.test_exists")
        self.assertTrue(self.config_check_xml.exists())


if __name__ == "__main__":
    unittest.main()
