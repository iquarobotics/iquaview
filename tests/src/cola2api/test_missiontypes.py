# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import unittest

from iquaview_lib.cola2api import mission_types

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)


class TestMissionTypes(unittest.TestCase):

    def test_types(self):
        print("TestMissionTypes.test_types")
        self.assertTrue(issubclass(mission_types.MissionGoto, mission_types.MissionManeuver))
        self.assertTrue(issubclass(mission_types.MissionSection, mission_types.MissionManeuver))
        self.assertTrue(issubclass(mission_types.MissionPark, mission_types.MissionManeuver))

        mission = mission_types.Mission()
        mw = mission_types.MissionGoto(41.3, 3.3, 0.0, 0.0, 0, 1.0, 2.0, True)
        ms = mission_types.MissionSection(41.777, 3.030, 15.0,
                                          41.787, 3.034, 15.0, 0.0,
                                          0, 1.0, 2.0, True)
        mp = mission_types.MissionPark(41.777, 3.030, 15.0, 0.0,
                                       0.5, True,
                                       0, 0.5, 30, True)

        self.assertEqual(mission.get_length(), 0)
        self.assertEqual(mission.size(), 0)
        mission_step = mission_types.MissionStep()
        mission_step.add_maneuver(mw)
        mission.add_step(mission_step)
        self.assertEqual(mission.get_length(), 1)
        self.assertEqual(mission.size(), 1)
        mission_step = mission_types.MissionStep()
        mission_step.add_maneuver(mw)
        mission.add_step(mission_step)
        self.assertEqual(mission.get_length(), 2)
        self.assertEqual(mission.size(), 2)
        mission_step = mission_types.MissionStep()
        mission_step.add_maneuver(mw)
        mission.add_step(mission_step)
        self.assertEqual(mission.get_length(), 3)
        self.assertEqual(mission.size(), 3)
        mission.remove_step(2)
        self.assertEqual(mission.get_length(), 2)
        self.assertEqual(mission.size(), 2)
        mission.remove_step(1)
        self.assertEqual(mission.get_length(), 1)
        self.assertEqual(mission.size(), 1)
        mission.remove_step(0)
        self.assertEqual(mission.get_length(), 0)
        self.assertEqual(mission.size(), 0)


if __name__ == "__main__":
    unittest.main()
