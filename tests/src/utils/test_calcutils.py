# -*- coding: utf-8 -*-
# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

import math
import os
import sys
import unittest

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)

from qgis.core import QgsPointXY

from iquaview_lib.utils.calcutils import (wrap_angle,
                                          get_angle_of_line_between_two_points,
                                          calc_slope,
                                          calc_is_collinear,
                                          calc_middle_point,
                                          distance_ellipsoid,
                                          bearing,
                                          endpoint_sphere,
                                          distance_plane,
                                          project_point_to_line,
                                          distance_to_segment,
                                          is_between)


class TestCalcUtils(unittest.TestCase):

    def setUp(self):
        print("TestCalcUtils.setUp")
        pass

    def test(self):
        print("TestCalcUtils.test")
        # wrap angle
        self.assertEqual(wrap_angle(0), 0)
        self.assertEqual(wrap_angle(2 * math.pi), 0)
        self.assertEqual(wrap_angle(3 * math.pi), math.pi)

        # get_angle_of_line_between_two_points
        self.assertEqual(get_angle_of_line_between_two_points(0, 0, 1, 0), 0.0)
        self.assertEqual(get_angle_of_line_between_two_points(0, 0, 0, 1), 90.0)
        self.assertEqual(get_angle_of_line_between_two_points(0, 0, -1, 0), 180.0)
        self.assertEqual(get_angle_of_line_between_two_points(0, 0, 0, -1), -90.0)
        self.assertEqual(get_angle_of_line_between_two_points(0, 0, 0, 0), 0.0)

        # calc_slope
        self.assertEqual(calc_slope(0, 0, 0, 0), 0.0)
        self.assertEqual(calc_slope(0, 0, 1, 0), 0.0)
        self.assertEqual(calc_slope(0, 0, -1, 0), 0.0)
        self.assertEqual(calc_slope(0, 0, 1, 1), 1.0)
        self.assertEqual(calc_slope(0, 0, 1, 2), 2.0)
        self.assertEqual(calc_slope(0, 0, 2, 1), 0.5)
        self.assertEqual(calc_slope(0, 0, 1, -1), -1.0)
        self.assertEqual(calc_slope(0, 0, -1, -1), 1.0)
        self.assertEqual(calc_slope(0, 0, -1, 1), -1.0)
        self.assertEqual(calc_slope(0, 0, 0, 1), math.inf)
        self.assertEqual(calc_slope(0, 0, 0, -1), -math.inf)

        # calc_is_collinear
        self.assertEqual(calc_is_collinear(0, 0, 0, 0, 0, 0), 0)
        self.assertEqual(calc_is_collinear(0, 0, 0, 1, 1, 0), 1)
        self.assertEqual(calc_is_collinear(0, 0, 0, 1, -1, 0), -1)
        self.assertEqual(calc_is_collinear(0, -1, 0, 1, 0, 0), 0)
        self.assertEqual(calc_is_collinear(1, 0, -1, 0, 0, -1), -1)
        self.assertEqual(calc_is_collinear(1, 0, -1, 0, 0, 1), 1)
        self.assertEqual(calc_is_collinear(0, -1, 0, 1, 0, 0), 0)
        self.assertEqual(calc_is_collinear(0, -1, 0, 1, 0, 0), 0)
        self.assertEqual(calc_is_collinear(0, -1, 0, 1, 0, 0), 0)

        # calc_middle_point
        self.assertEqual(calc_middle_point(0, 0, 0, 0), (0, 0))
        self.assertEqual(calc_middle_point(1, 0, 1, 0), (1, 0))
        self.assertEqual(calc_middle_point(-1, 0, 1, 0), (0, 0))
        self.assertEqual(calc_middle_point(1, 1, -3, -3), (-1, -1))

        # distance_ellipsoid

        self.assertEqual(0, distance_ellipsoid(0, 0, 0, 0))

        # 1° longitude difference in the equator equals to 111 km
        p1_x = 0
        p1_y = 0
        p2_x = 1
        p2_y = 0
        d = round(distance_ellipsoid(p1_x, p1_y, p2_x, p2_y) / 1000)  # to pass from m to km
        self.assertEqual(111, d)

        # 1° longitude difference at 40 degrees north or south equals 85 km
        p1_x = 0
        p1_y = 40
        p2_x = 1
        p2_y = 40
        d = round(distance_ellipsoid(p1_x, p1_y, p2_x, p2_y) / 1000)
        self.assertEqual(85, d)

        # 1ª latitude difference anywhere equals to 111 km
        p1_x = 0
        p1_y = 0
        p2_x = 0
        p2_y = 1
        d = round(distance_ellipsoid(p1_x, p1_y, p2_x, p2_y) / 1000)
        self.assertEqual(111, d)

        # bearing
        p1 = QgsPointXY(0, 0)
        self.assertEqual(0, bearing(p1.x(), p1.y(), p1.x(), p1.y()))

        p1 = QgsPointXY(0, 0)
        p2 = QgsPointXY(1, 0)
        self.assertEqual(90, bearing(p1.x(), p1.y(), p2.x(), p2.y()))
        self.assertEqual(-90, bearing(p2.x(), p2.y(), p1.x(), p1.y()))

        p1 = QgsPointXY(0, 0)
        p2 = QgsPointXY(0, 1)
        self.assertEqual(0, bearing(p1.x(), p1.y(), p2.x(), p2.y()))
        self.assertEqual(180, bearing(p2.x(), p2.y(), p1.x(), p1.y()))

        # endpoint_sphere
        p1 = QgsPointXY(0, 0)
        p2_x, p2_y = endpoint_sphere(p1.x(), p1.y(), 111300, 90)  # 1ª latitude difference anywhere is 111 km 300m aprox
        self.assertAlmostEqual(1, p2_x, 3)

        p2_x, p2_y = endpoint_sphere(p1.x(), p1.y(), 111300, 180)
        self.assertAlmostEqual(-1, p2_y, 3)

        # endpoint ellipsoid
        p1_x = 40
        p1_y = 3
        p2_x = 40
        p2_y = 4
        dist = distance_ellipsoid(p1_x, p1_y, p2_x, p2_y)
        bear = bearing(p1_x, p1_y, p2_x, p2_y)  # sphere approximation, will bring precision loss

        # distance_plane
        p1, p2 = QgsPointXY(1, 0), QgsPointXY(2, 0)
        self.assertEqual(1, distance_plane(p1.x(), p1.y(), p2.x(), p2.y()))

        p1, p2 = QgsPointXY(0, 1), QgsPointXY(0, 2)
        self.assertEqual(1, distance_plane(p1.x(), p1.y(), p2.x(), p2.y()))

        p1, p2 = QgsPointXY(-2, 0), QgsPointXY(2, 0)
        self.assertEqual(4, distance_plane(p1.x(), p1.y(), p2.x(), p2.y()))

        p1, p2 = QgsPointXY(0, 0), QgsPointXY(3, 4)
        self.assertEqual(5, distance_plane(p1.x(), p1.y(), p2.x(), p2.y()))

        # project_point_to_line
        p1, p_start, p_end = QgsPointXY(3, 5), QgsPointXY(0, 0), QgsPointXY(5, 0)
        p_x, p_y = project_point_to_line(p1.x(), p1.y(),
                                         p_start.x(), p_start.y(),
                                         p_end.x(), p_end.y())
        self.assertEqual(QgsPointXY(3, 0), QgsPointXY(p_x, p_y))

        p1, p_start, p_end = QgsPointXY(3, -5), QgsPointXY(0, 0), QgsPointXY(5, 0)
        p_x, p_y = project_point_to_line(p1.x(), p1.y(),
                                         p_start.x(), p_start.y(),
                                         p_end.x(), p_end.y())
        self.assertEqual(QgsPointXY(3, 0), QgsPointXY(p_x, p_y))

        p1, p_start, p_end = QgsPointXY(3, 5), QgsPointXY(-15, 0), QgsPointXY(5, 0)
        p_x, p_y = project_point_to_line(p1.x(), p1.y(),
                                         p_start.x(), p_start.y(),
                                         p_end.x(), p_end.y())
        self.assertEqual(QgsPointXY(3, 0), QgsPointXY(p_x, p_y))

        p1, p_start, p_end = QgsPointXY(5, 0), QgsPointXY(0, 0), QgsPointXY(4, 0)
        p_x, p_y = project_point_to_line(p1.x(), p1.y(),
                                         p_start.x(), p_start.y(),
                                         p_end.x(), p_end.y())
        self.assertEqual(QgsPointXY(5, 0), QgsPointXY(p_x, p_y))

        # distance_to_segment
        point, p_start, p_end = QgsPointXY(3, 5), QgsPointXY(0, 0), QgsPointXY(5, 0)
        self.assertEqual(5, distance_to_segment(point.x(), point.y(),
                                                p_start.x(), p_start.y(),
                                                p_end.x(), p_end.y()))

        point, p_start, p_end = QgsPointXY(3, -5), QgsPointXY(0, 0), QgsPointXY(5, 0)
        self.assertEqual(5, distance_to_segment(point.x(), point.y(),
                                                p_start.x(), p_start.y(),
                                                p_end.x(), p_end.y()))

        point, p_start, p_end = QgsPointXY(3, 0), QgsPointXY(0, 0), QgsPointXY(5, 0)
        self.assertEqual(0, distance_to_segment(point.x(), point.y(),
                                                p_start.x(), p_start.y(),
                                                p_end.x(), p_end.y()))

        point, p_start, p_end = QgsPointXY(3, 5), QgsPointXY(0, 0), QgsPointXY(3, 5)
        self.assertEqual(0, distance_to_segment(point.x(), point.y(),
                                                p_start.x(), p_start.y(),
                                                p_end.x(), p_end.y()))

        point, p_start, p_end = QgsPointXY(3, 5), QgsPointXY(3, 5), QgsPointXY(5, 0)
        self.assertEqual(0, distance_to_segment(point.x(), point.y(),
                                                p_start.x(), p_start.y(),
                                                p_end.x(), p_end.y()))

        # is_between
        point, p_start, p_end = QgsPointXY(3, 5), QgsPointXY(0, 0), QgsPointXY(5, 0)
        self.assertEqual(True, is_between(point.x(), point.y(),
                                          p_start.x(), p_start.y(),
                                          p_end.x(), p_end.y()))

        point, p_start, p_end = QgsPointXY(0, 3), QgsPointXY(0, 0), QgsPointXY(5, 0)
        self.assertEqual(True, is_between(point.x(), point.y(),
                                          p_start.x(), p_start.y(),
                                          p_end.x(), p_end.y()))

        point, p_start, p_end = QgsPointXY(0, 0), QgsPointXY(0, 0), QgsPointXY(5, 0)
        self.assertEqual(True, is_between(point.x(), point.y(),
                                          p_start.x(), p_start.y(),
                                          p_end.x(), p_end.y()))

        point, p_start, p_end = QgsPointXY(5, 0), QgsPointXY(0, 0), QgsPointXY(5, 0)
        self.assertEqual(True, is_between(point.x(), point.y(),
                                          p_start.x(), p_start.y(),
                                          p_end.x(), p_end.y()))

        point, p_start, p_end = QgsPointXY(6, 0), QgsPointXY(0, 0), QgsPointXY(5, 0)
        self.assertEqual(False, is_between(point.x(), point.y(),
                                           p_start.x(), p_start.y(),
                                           p_end.x(), p_end.y()))

        point, p_start, p_end = QgsPointXY(-1, 0), QgsPointXY(0, 0), QgsPointXY(5, 0)
        self.assertEqual(False, is_between(point.x(), point.y(),
                                           p_start.x(), p_start.y(),
                                           p_end.x(), p_end.y()))


if __name__ == "__main__":
    unittest.main()
