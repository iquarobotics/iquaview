# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

import os
import subprocess
import sys
import unittest
from unittest.mock import Mock

from qgis.core import QgsApplication

import pysftp
from PyQt5.QtCore import Qt, QPoint
from PyQt5.QtTest import QTest
from PyQt5.QtWidgets import QMessageBox

import iquaview_lib.connection.sftp.sftp_connection_dialog as sftpconnectiondialog
from iquaview_lib.connection.sftp.sftp_connection_dialog import SFTPConnectionDialog
from iquaview_lib.utils import busywidget

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)


class TestSftpConnectionDialog(unittest.TestCase):

    def setUp(self):
        print("TestSftpConnectionDialog.setUp")
        self.app = QgsApplication.instance()
        if self.app is None:
            self.app = QgsApplication([], False)
        # Create mock objects
        # task thread mock
        busywidget.TaskThread = Mock()
        tt_instance = busywidget.TaskThread.return_value  # This is to edit return values for its methods
        tt_instance.has_error = Mock(return_value=False)

        # Mock pysft Connection
        pysftp.Connection = Mock()
        self.connection_instance = pysftp.Connection.return_value
        self.connection_instance.stat = Mock()
        c_stat_instance = self.connection_instance.stat.return_value
        c_stat_instance.st_size = 35  # default size
        c_stat_instance.st_mtime = 1574680176  # default timestamp

        # busy widget mock, need to return different things when get_info is called
        busywidget.BusyWidget = Mock()
        self.bw_instance = busywidget.BusyWidget.return_value

        # Mocking system libraries locally on sftpconnectiondialog
        sftpconnectiondialog.stat = Mock()
        stat_instance = sftpconnectiondialog.stat.return_value
        stat_instance.st_size = 35  # default size
        stat_instance.st_mtime = 1574680176  # default timestamp
        sftpconnectiondialog.S_ISDIR = Mock(return_value=True)
        sftpconnectiondialog.S_ISREG = Mock(return_value=True)

        # Create remote directory structure
        self.remote_path = "~/Q/W/E"
        remote_files = ["file1.file", "mission.xml", "layer.file"]  # inside /home/usr/Q/W/E/
        remote_paths = ["/home", "/home/usr", "/home/usr/Q", "/home/usr/Q/W", "/home/usr/Q/W/E"]
        rdir1_content = ["usr"]  # /home
        rdir2_content = ["Q"]  # /home/usr
        rdir3_content = ["W", "f1"]  # /home/usr/Q
        rdir4_content = ["E", "f2"]  # /home/usr/Q/W
        rdir5_content = ["folder"]  # /home/usr/Q/W/E
        desired_structure = ['/home/usr/Q/W', '/home/usr/Q/f1',
                             '/home/usr/Q/W/E', '/home/usr/Q/W/f2',
                             '/home/usr/Q/W/E/folder']

        # Create a local directory structure
        self.local_path = "/home/A/B/C"
        local_files = ["file1.file", "mission.xml", "file3.file"]  # /home/A/B/C
        ldir1_content = ['A', "f1", "f2"]  # /home   only folders
        ldir2_content = ['B', "a1", "a2"]  # /home/A  only folders
        ldir3_content = ['C']  # /home/A/B  only folders

        # Prepare mock results for listdir
        remote_listdir_results = [rdir1_content, rdir2_content, rdir3_content, rdir4_content, rdir5_content]
        self.connection_instance.listdir = Mock(side_effect=remote_listdir_results)

        # This can be called before init due to it being 'static'. Also needed because the busy widget that
        # calls this function is mocked and never called.
        remote_folders = SFTPConnectionDialog.explore_remote_tree(None, self.connection_instance, remote_paths)

        # Assert folders were read correctly
        for folder in desired_structure:
            self.assertTrue(folder in remote_folders)

        # Prepare mock results for busywidget get_info
        self.get_info_values = [self.connection_instance, self.connection_instance, remote_folders, remote_folders,
                                [rdir5_content, remote_files], [rdir5_content, remote_files]]
        self.bw_instance.get_info = Mock(side_effect=self.get_info_values)

        # Prepare mock results for os.listdir, locally
        local_listdir_results = [ldir1_content, ldir2_content, ldir3_content, local_files]
        sftpconnectiondialog.listdir = Mock(side_effect=local_listdir_results)

        # list of booleans that has as many trues as folders in the structure, and as many falses as files.
        values = [*[True] * (len(ldir1_content) + len(ldir2_content) + len(ldir3_content)), *[False] * len(local_files)]

        # Prepare mock results for S_ISDIR calls
        sftpconnectiondialog.S_ISDIR = Mock(side_effect=values)

    @unittest.skip("Skip TestSFTPConnectionDialog.test_actions")
    def test_actions(self):
        print("TestSFTPConnectionDialog.test_actions")
        dlg = SFTPConnectionDialog("usr", "127.0.0.1", self.local_path, self.remote_path)
        QMessageBox.question = Mock(return_value=QMessageBox.Yes)
        QMessageBox.information = Mock()
        QMessageBox.critical = Mock()
        QMessageBox.warning = Mock()
        # remote files
        remote_files = ["file1.file", "mission.xml", "layer.file"]
        local_files = ["file1.file", "mission.xml", "file3.file"]

        # Mocks for transfers
        self.connection_instance.pwd = "/home/usr/Q/W/E"
        self.connection_instance.put = Mock()
        self.connection_instance.get = Mock()
        self.bw_instance.get_info = Mock(return_value=[[], remote_files])
        sftpconnectiondialog.SFTPTransferProgressDialog.show = Mock()
        sftpconnectiondialog.listdir = Mock(return_value=local_files)
        sftpconnectiondialog.S_ISDIR = Mock(return_value=False)
        sftpconnectiondialog.S_ISREG = Mock(return_value=True)
        QMessageBox.question = Mock(return_value=QMessageBox.Yes)
        QMessageBox.information = Mock()

        # Transfer all files from local to remote, one by one
        # Mock for the reload action after the transfer
        for i in range(0, dlg.local_table_model.rowCount()):
            dlg.local_table_view.selectRow(i)
            QTest.mouseClick(dlg.transfer_to_remote_btn, Qt.LeftButton)

        # Transfer all files from remote to local, one by one
        # Mock for the reload action after the transfer
        for i in range(0, dlg.remote_table_model.rowCount()):
            dlg.remote_table_view.selectRow(i)
            QTest.mouseClick(dlg.transfer_to_local_btn, Qt.LeftButton)
        # Assert that the files were sent
        self.assertEqual(self.connection_instance.put.call_count, dlg.local_table_model.rowCount())
        self.assertEqual(self.connection_instance.get.call_count, dlg.remote_table_model.rowCount())

        # Mocks for deleting files
        subprocess.run = Mock()
        self.connection_instance.remove = Mock()

        # Test deleting files
        dlg.local_table_view.selectRow(0)
        dlg.delete_local_selected_files()
        self.assertTrue(subprocess.run.called)

        dlg.remote_table_view.selectRow(0)
        dlg.delete_remote_selected_files()
        self.assertTrue(self.connection_instance.remove.called)

        # Mocks for renaming files
        sftpconnectiondialog.QInputDialog.getText = Mock(return_value=["new_file_name.file", True])
        subprocess.run = Mock()
        self.connection_instance.rename = Mock()

        # Test renaming files
        dlg.local_table_view.selectRow(0)
        dlg.rename_local_file()
        self.assertTrue(subprocess.run.called)

        dlg.remote_table_view.selectRow(0)
        dlg.rename_remote_file()
        self.assertTrue(self.connection_instance.rename.called)

    @unittest.skip("Skip TestSFTPConnectionDialog.test_dialog")
    def test_dialog(self):
        print("TestSFTPConnectionDialog.test_dialog")
        dlg = SFTPConnectionDialog("usr", "127.0.0.1", self.local_path, self.remote_path,
                                   file_extension=["file"], filter_by_extension=True)
        # Testing context menu creation
        dlg.local_table_view.selectRow(0)
        dlg.local_table_view.customContextMenuRequested.emit(QPoint(0, 0))
        dlg.remote_table_view.selectRow(1)
        dlg.remote_table_view.customContextMenuRequested.emit(QPoint(0, 0))

        # Show help, mock exec first to just create the object but not show it
        sftpconnectiondialog.SFTPHelpDialog.exec_ = Mock()
        QTest.mouseClick(dlg.help_btn, Qt.LeftButton)
        self.assertTrue(sftpconnectiondialog.SFTPHelpDialog.exec_.called)
