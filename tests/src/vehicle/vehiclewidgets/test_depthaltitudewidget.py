# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import unittest

from distutils.version import LooseVersion

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)

from PyQt5.QtCore import QT_VERSION_STR
from qgis.core import QgsApplication

from iquaview.src.vehicle.vehiclewidgets.depthaltitudewidget import DepthAltitudeCanvas


class TestDepthAltitudeCanvasWidget(unittest.TestCase):


    @unittest.skipIf(LooseVersion(QT_VERSION_STR) >= LooseVersion("5.12.0"),
                         "not tested in this library version")
    @classmethod
    def setUpClass(cls) -> None:
        print("TestDepthAltitudeCanvasWidget.setUp")
        cls.app = QgsApplication.instance()
        if cls.app is None:
            cls.app = QgsApplication([], False)

        cls.depth_altitude_canvas = DepthAltitudeCanvas(None, f_width=3, f_height=2, dpi=100)

    @unittest.skipIf(LooseVersion(QT_VERSION_STR) >= LooseVersion("5.12.0"),
                     "not tested in this library version")
    def test_set_data(self):
        print("TestDepthAltitudeCanvasWidget.test_set_data")
        yaw = {'position': {'depth': 10},
               'altitude': 10}

        self.depth_altitude_canvas.set_data(yaw)


if __name__ == "__main__":
    unittest.main()
