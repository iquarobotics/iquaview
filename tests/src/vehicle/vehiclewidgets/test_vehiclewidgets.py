# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import unittest
from copy import copy
from distutils.version import LooseVersion

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)

from qgis.core import QgsApplication
from PyQt5.QtCore import QT_VERSION_STR

from iquaview_lib.config import Config
from iquaview_lib.vehicle.vehicleinfo import VehicleInfo
from iquaview_lib.vehicle.vehicledata import VehicleData

from iquaview.src.vehicle.vehiclewidgets.vehiclewidgets import VehicleWidgets


class TestVehicleWidgets(unittest.TestCase):

    @unittest.skipIf(LooseVersion(QT_VERSION_STR) >= LooseVersion("5.12.0"),
                     "not tested in this library version")
    @classmethod
    def setUpClass(cls) -> None:
        print("TestVehicleWidgets.setUpClass")
        cls.app = QgsApplication.instance()
        if cls.app is None:
            cls.app = QgsApplication([], False)

        cls.config = Config()
        cls.config.load()
        cls.config.settings = copy(cls.config.settings)

        xml_filename = "{}".format(os.path.abspath(os.path.expanduser(cls.config.settings['last_auv_config_xml'])))
        cls.vehicle_info = VehicleInfo(xml_filename)
        cls.vd = VehicleData(xml_filename, cls.vehicle_info)
        cls.vehicle_widgets = VehicleWidgets(cls.vehicle_info, cls.vd)

    @unittest.skipIf(LooseVersion(QT_VERSION_STR) >= LooseVersion("5.12.0"),
                     "not tested in this library version")
    def test_refresh(self):
        print("TestVehicleWidgets.test_refresh")
        self.vehicle_widgets.connect()
        self.vehicle_widgets.refresh()
        self.vehicle_widgets.disconnect_object()


if __name__ == "__main__":
    unittest.main()
