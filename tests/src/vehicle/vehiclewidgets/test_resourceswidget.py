# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import unittest

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)

from qgis.core import QgsApplication
from iquaview.src.vehicle.vehiclewidgets.resourceswidget import ResourcesWidget

class TestResourcesWidget(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        print("TestResourcesWidget.setUpClass")
        cls.app = QgsApplication.instance()
        if cls.app is None:
            cls.app = QgsApplication([], False)

        cls.resources_widget = ResourcesWidget()

    # def test_set_data(self):
    #     self.resources_widget.set_data(50, 30, 20)


if __name__ == "__main__":
    unittest.main()
