# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import unittest

from qgis.core import QgsApplication

from iquaview_lib.connection.settings.gpsconnectionwidget import GPSConnectionWidget
from iquaview_lib.config import Config
from iquaview_lib.vehicle.vehicleinfo import VehicleInfo

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)


class TestGpsConnectionWidget(unittest.TestCase):

    def setUp(self):
        print("TestGpsConnectionWidget.setUp")
        self.app = QgsApplication.instance()
        if self.app is None:
            self.app = QgsApplication([], False)

        self.config = Config()
        self.config.load()

        xml_filename = "{}".format(os.path.abspath(os.path.expanduser(self.config.settings['last_auv_config_xml'])))
        self.vehicle_info = VehicleInfo(xml_filename)

    def test_validator(self):
        print("TestGpsConnectionWidget.test_validator")
        self.gps_connection_widget = GPSConnectionWidget()

        self.gps_connection_widget.serialPortRadioButton.setChecked(False)
        self.gps_connection_widget.ethernet_RadioButton.setChecked(True)

        self.gps_connection_widget.serial_port = 'internalGPS'
        self.gps_connection_widget.serial_baudrate = 4800

        self.gps_connection_widget.ip = '127.0.0.1'
        self.gps_connection_widget.hdt_port = 4000
        self.gps_connection_widget.gga_port = 4000

        self.assertTrue(self.gps_connection_widget.is_valid())


if __name__ == "__main__":
    unittest.main()
