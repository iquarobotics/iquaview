# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

# import os
# import sys
# import unittest
#
# from PyQt5.QtWidgets import QApplication
#
# from iquaview_evologics_usbl.usblconnectionwidget import USBLConnectionWidget
# from iquaview_lib.config import Config
# from iquaview_lib.vehicle.vehicleinfo import VehicleInfo
#
# srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
# iquaview_root_path = srcpath + '/../'
# sys.path.append(iquaview_root_path)
#
#
# class TestUsblconnectionWidget(unittest.TestCase):
#
#     def setUp(self):
#         self.app = QApplication.instance()
#
#         self.config = Config()
#         self.config.load()
#
#         xml_filename = "{}".format(os.path.abspath(os.path.expanduser(self.config.settings['last_auv_config_xml'])))
#         self.vehicle_info = VehicleInfo(xml_filename)
#
#     def test_validator(self):
#         self.usbl_connection_widget = USBLConnectionWidget()
#         self.usbl_connection_widget.ip = self.config.settings['usbl_ip']
#         self.usbl_connection_widget.port = self.config.settings['usbl_port']
#         self.usbl_connection_widget.ownid = self.config.settings['usbl_own_id']
#         self.usbl_connection_widget.targetid = self.config.settings['usbl_target_id']
#
#         self.assertTrue(self.usbl_connection_widget.is_usbl_valid())
#
#
# if __name__ == "__main__":
#     unittest.main()
