# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

import sys
import unittest

from qgis.core import QgsApplication

from iquaview.src.mainwindow import MainWindow


class TestDataOutputManager(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        print("TestDataOutputManager.setUpClass")
        cls.app = QgsApplication.instance()
        if cls.app is None:
            cls.app = QgsApplication([], False)

        cls.mainwindow = MainWindow()
        cls.mainwindow.is_test = True

    @classmethod
    def tearDownClass(cls) -> None:
        print("TestDataOutputManager.tearDownClass")
        cls.mainwindow.close()
        cls.mainwindow.deleteLater()

    def test_new_connection(self):
        print("TestDataOutputManager.test_connection")
        data_output_manager = self.mainwindow.options_dialog.data_output_manager_widget
        # get len before add new connection
        temp_len = len(data_output_manager.temp_connections)
        data_output_manager.new_connection()
        # only one connection added, get first
        self.assertEqual(len(data_output_manager.temp_connections), temp_len + 1)
        data_output_manager.on_close()
        self.assertEqual(len(data_output_manager.temp_connections), temp_len)


if __name__ == "__main__":
    unittest.main()
