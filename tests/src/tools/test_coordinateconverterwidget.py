# -*- coding: utf-8 -*-
# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import unittest

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)

from qgis.core import QgsApplication
from iquaview.src.tools import coordinateconverterwidget
from iquaview_lib.config import Config

class TestCoordinateConverter(unittest.TestCase):

    def setUp(self):
        print("TestCoordinateConverter.setUp")
        self.app = QgsApplication.instance()
        if self.app is None:
            self.app = QgsApplication([], False)

        self.config = Config()
        self.config.load()

        self.coordinateconverter = coordinateconverterwidget.CoordinateConverterDialog(self.config)

    def test_lat_lon(self):
        lat = 41.77785501
        lon = 3.03357015
        self.coordinateconverter.coordinate_converter_lineedit.setText("{}, {}".format(lat, lon))
        self.coordinateconverter.coordinate_converter_lineedit.convert()

        self.assertAlmostEqual(lat, self.coordinateconverter.coordinate_converter_lineedit.get_latitude(), 7)
        self.assertAlmostEqual(lon, self.coordinateconverter.coordinate_converter_lineedit.get_longitude(), 7)

    def test_converter(self):
        print("TestCoordinateConverter.test_converter")
        lat = 41.77785501
        lon = 3.03357015
        expected_dd = "41.7778550°N, 3.0335702°E"
        expected_dm = "41° 46.6713006'N, 003° 02.0142090'E"
        expected_dms = "41° 46' 40.2780360\"N, 003° 02' 00.8525400\"E"
        self.evaluate_lat_lon(lat, lon, expected_dd, expected_dm, expected_dms)
        lat = "41° 39.3833188' N"
        lon = "002° 53.9797413' E"
        expected_dd = "41.6563886°N, 2.8996624°E"
        expected_dm = "41° 39.3833188'N, 002° 53.9797413'E"
        expected_dms = "41° 39' 22.9991280\"N, 002° 53' 58.7844780\"E"
        self.evaluate_lat_lon(lat, lon, expected_dd, expected_dm, expected_dms)
        lat = "39° 57' 44.89661\" N"
        lon = "000° 01' 07.08267\" E"
        expected_dd = "39.9624713°N, 0.0186341°E"
        expected_dm = "39° 57.7482768'N, 000° 01.1180445'E"
        expected_dms = "39° 57' 44.8966100\"N, 000° 01' 07.0826700\"E"
        self.evaluate_lat_lon(lat, lon, expected_dd, expected_dm, expected_dms)
        lat = "3825.7762N"
        lon = "0906.6963W"
        expected_dd = "38.4296033°N, 9.1116050°W"
        expected_dm = "38° 25.7762000'N, 009° 06.6963000'W"
        expected_dms = "38° 25' 46.5720000\"N, 009° 06' 41.7780000\"W"
        self.evaluate_lat_lon(lat, lon, expected_dd, expected_dm, expected_dms)
        lat = "3825.7762N"
        lon = "0906.6963W"
        expected_dd = "38.4296033°N, 9.1116050°W"
        expected_dm = "38° 25.7762000'N, 009° 06.6963000'W"
        expected_dms = "38° 25' 46.5720000\"N, 009° 06' 41.7780000\"W"
        self.evaluate_lat_lon(lat, lon, expected_dd, expected_dm, expected_dms)
        lat = "38.429603333N"
        lon = "009.111605W"
        expected_dd = "38.4296033°N, 9.1116050°W"
        expected_dm = "38° 25.7762000'N, 009° 06.6963000'W"
        expected_dms = "38° 25' 46.5719988\"N, 009° 06' 41.7780000\"W"
        self.evaluate_lat_lon(lat, lon, expected_dd, expected_dm, expected_dms)
        lat = "38.429603333N"
        lon = "009.111605W"
        expected_dd = "38.4296033°N, 9.1116050°W"
        expected_dm = "38° 25.7762000'N, 009° 06.6963000'W"
        expected_dms = "38° 25' 46.5719988\"N, 009° 06' 41.7780000\"W"
        self.evaluate_lat_lon(lat, lon, expected_dd, expected_dm, expected_dms)
        lat = "382546.57200 N"
        lon = "0090641.77800 W"
        expected_dd = "38.4296033°N, 9.1116050°W"
        expected_dm = "38° 25.7762000'N, 009° 06.6963000'W"
        expected_dms = "38° 25' 46.5720000\"N, 009° 06' 41.7780000\"W"
        self.evaluate_lat_lon(lat, lon, expected_dd, expected_dm, expected_dms)
        lat = "38 25.7762000N"
        lon = "009 06.6963000W"
        expected_dd = "38.4296033°N, 9.1116050°W"
        expected_dm = "38° 25.7762000'N, 009° 06.6963000'W"
        expected_dms = "38° 25' 46.5720000\"N, 009° 06' 41.7780000\"W"
        self.evaluate_lat_lon(lat, lon, expected_dd, expected_dm, expected_dms)
        lat = "38º 25.7762000N"
        lon = "009º 06.6963000W"
        expected_dd = "38.4296033°N, 9.1116050°W"
        expected_dm = "38° 25.7762000'N, 009° 06.6963000'W"
        expected_dms = "38° 25' 46.5720000\"N, 009° 06' 41.7780000\"W"
        self.evaluate_lat_lon(lat, lon, expected_dd, expected_dm, expected_dms)
        lat = "38 25.7762000  N  "
        lon = "009º 06.6963000'  W "
        expected_dd = "38.4296033°N, 9.1116050°W"
        expected_dm = "38° 25.7762000'N, 009° 06.6963000'W"
        expected_dms = "38° 25' 46.5720000\"N, 009° 06' 41.7780000\"W"
        self.evaluate_lat_lon(lat, lon, expected_dd, expected_dm, expected_dms)

    def evaluate_lat_lon(self, lat, lon, expected_dd, expected_dm, expected_dms):
        self.coordinateconverter.coordinate_converter_lineedit.setText("{}, {}".format(lat, lon))
        self.coordinateconverter.coordinate_converter_lineedit.convert()

        self.assertEqual(self.coordinateconverter.coordinate_converter_lineedit.get_decimal_degrees(),
                         expected_dd)
        self.assertEqual(self.coordinateconverter.coordinate_converter_lineedit.get_degrees_minutes(),
                         expected_dm)
        self.assertEqual(self.coordinateconverter.coordinate_converter_lineedit.get_degrees_minutes_seconds(),
                         expected_dms)


if __name__ == '__main__':
    unittest.main()
