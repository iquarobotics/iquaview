# -*- coding: utf-8 -*-
# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import unittest

from qgis.core import QgsApplication
from PyQt5.QtCore import Qt, QPoint
from PyQt5.QtTest import QTest

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)

from iquaview.src.mainwindow import MainWindow
from matplotlib.backend_bases import MouseEvent
from iquaview.src.mapsetup.addlayers import add_layers_to_project

MISSION_NAME = "/home/user/iquaview_projects/test/missions/test_mission.xml"
TIF_NAME = "/home/user/iquaview_projects/test/layers/test_bathymetry.tif"


class TestProfileToolModule(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        print("TestProfileToolModule.setUpClass")
        cls.app = QgsApplication.instance()
        if cls.app is None:
            cls.app = QgsApplication([], False)

        cls.mainwindow = MainWindow()
        cls.mainwindow.is_test = True

    @classmethod
    def tearDownClass(cls) -> None:
        print("TestProfileToolModule.tearDownClass")
        cls.mainwindow.close()
        cls.mainwindow.deleteLater()

    @unittest.skipIf(not os.path.exists(MISSION_NAME) or not os.path.exists(TIF_NAME),
                     f"{MISSION_NAME} or {TIF_NAME} not exists.")
    def test_profiletoolmodule(self):
        print("TestProfileToolModule.test_profiletoolmodule")

        self.mainwindow.new_project_action.trigger()
        self.mainwindow.proj.write("/home/user/iquaview_projects/test/Project.qgs")
        self.mainwindow.load_project("/home/user/iquaview_projects/test/Project.qgs")

        add_layers_to_project(self.mainwindow.proj, self.mainwindow.canvas, [TIF_NAME], "test_bathymetry")
        self.mainwindow.mission_ctrl.load_mission(MISSION_NAME)
        self.mainwindow.proj.write("/home/user/iquaview_projects/test/Project.qgs")

        test_layer = None
        for layer in self.mainwindow.root.checkedLayers():
            if layer.name() == "test_mission":
                test_layer = layer
                break
        self.mainwindow.view.setCurrentLayer(test_layer)
        self.mainwindow.mission_module.edit_wp_mission_action.trigger()

        profile_tool_action = self.mainwindow.profile_tool_module.profile_tool_action
        QTest.mouseClick(self.mainwindow.tools_toolbar.widgetForAction(profile_tool_action), Qt.LeftButton)
        self.assertTrue(profile_tool_action.isChecked())
        self.assertIsNotNone(self.mainwindow.profile_tool_module.get_map_tool_renderer())
        self.assertIsNotNone(self.mainwindow.profile_tool_module.profile_tool_dockwidget)
        self.assertIsInstance(self.mainwindow.canvas.mapTool(), type(self.mainwindow.profile_tool_module.get_map_tool()))

        QTest.mouseClick(self.mainwindow.canvas.viewport(), Qt.LeftButton, Qt.NoModifier, QPoint(100, 100))
        QTest.mouseClick(self.mainwindow.canvas.viewport(), Qt.LeftButton, Qt.NoModifier, QPoint(100, 150))
        QTest.mouseClick(self.mainwindow.canvas.viewport(), Qt.LeftButton, Qt.NoModifier, QPoint(100, 200))
        self.assertTrue(len(self.mainwindow.profile_tool_module.profile_tool_dockwidget.widget().point_list) == 3)

        self.mainwindow.profile_tool_module.profile_tool_dockwidget.widget().mouse_event_mpl(
            MouseEvent('motion_notify_event',
                       self.mainwindow.profile_tool_module.profile_tool_dockwidget.widget().plot.figure.canvas,
                       x=200,
                       y=100))
        self.assertTrue(self.mainwindow.profile_tool_module.profile_tool_dockwidget.widget().annot is not None)

        self.mainwindow.profile_tool_module.profile_tool_dockwidget.widget().mouse_exit_mpl(None)
        self.assertTrue(self.mainwindow.profile_tool_module.profile_tool_dockwidget.widget().annot is None)

        QTest.mouseClick(self.mainwindow.canvas.viewport(), Qt.RightButton, Qt.NoModifier, QPoint(100, 210))
        self.assertTrue(len(self.mainwindow.profile_tool_module.profile_tool_dockwidget.widget().point_list) == 0)

        self.mainwindow.view.setCurrentLayer(test_layer)
        self.assertTrue(len(self.mainwindow.profile_tool_module.profile_tool_dockwidget.widget().point_list) == 24)

        self.mainwindow.profile_tool_module.profile_tool_dockwidget.widget().mouse_event_mpl(
            MouseEvent('motion_notify_event',
                       self.mainwindow.profile_tool_module.profile_tool_dockwidget.widget().plot.figure.canvas,
                       x=200,
                       y=100))
        self.assertTrue(self.mainwindow.profile_tool_module.profile_tool_dockwidget.widget().annot is not None)

        self.mainwindow.profile_tool_module.profile_tool_dockwidget.widget().mouse_exit_mpl(None)
        self.assertTrue(self.mainwindow.profile_tool_module.profile_tool_dockwidget.widget().annot is None)

        self.mainwindow.profile_tool_module.profile_tool_action.trigger()
        self.assertIsNone(self.mainwindow.profile_tool_module.profile_tool_dockwidget)
        self.assertNotIsInstance(self.mainwindow.canvas.mapTool(),
                                 type(self.mainwindow.profile_tool_module.get_map_tool()))
        self.assertIsInstance(self.mainwindow.canvas.mapTool(),
                              type(self.mainwindow.mission_ctrl.get_edit_wp_mission_tool()))


if __name__ == '__main__':
    unittest.main()
