# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import unittest

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)

from qgis.core import QgsApplication
from iquaview_lib.vessel.vesselpossystem import VesselPositionSystem
from iquaview_lib.config import Config


class TestVesselPosSystem(unittest.TestCase):

    def setUp(self):
        print("TestVesselPosSystem.setUp")
        self.app = QgsApplication.instance()
        if self.app is None:
            self.app = QgsApplication([], False)
        self.config = Config()
        self.config.load()

        self.vps = VesselPositionSystem(self.config)

    def test_spinbox(self):
        print("TestVesselPosSystem.test_spinbox")
        self.assertEqual(type(self.vps.vessel_width_doubleSpinBox.value()), type(0.0))
        self.assertEqual(type(self.vps.vessel_length_doubleSpinBox.value()), type(0.0))
        self.assertEqual(type(self.vps.vrp_x_offset_doubleSpinBox.value()), type(0.0))
        self.assertEqual(type(self.vps.vrp_y_offset_doubleSpinBox.value()), type(0.0))
        self.assertEqual(type(self.vps.gps_x_offset_doubleSpinBox.value()), type(0.0))
        self.assertEqual(type(self.vps.gps_y_offset_doubleSpinBox.value()), type(0.0))
        self.assertEqual(type(self.vps.gps_heading_doubleSpinBox.value()), type(0.0))
        self.assertEqual(type(self.vps.usbl_x_offset_doubleSpinBox.value()), type(0.0))
        self.assertEqual(type(self.vps.usbl_y_offset_doubleSpinBox.value()), type(0.0))
        self.assertEqual(type(self.vps.usbl_z_offset_doubleSpinBox.value()), type(0.0))

    def test_accept(self):
        print("TestVesselPosSystem.test_accept")
        width = 10
        length = 40
        vrp_x = 4
        vrp_y = -2
        gps_x = 1
        gps_y = 2
        gps_heading = 25.5

        self.vps.vessel_width_doubleSpinBox.setValue(width)
        self.vps.vessel_length_doubleSpinBox.setValue(length)
        self.vps.vrp_x_offset_doubleSpinBox.setValue(vrp_x)
        self.vps.vrp_y_offset_doubleSpinBox.setValue(vrp_y)
        self.vps.gps_x_offset_doubleSpinBox.setValue(gps_x)
        self.vps.gps_y_offset_doubleSpinBox.setValue(gps_y)
        self.vps.gps_heading_doubleSpinBox.setValue(gps_heading)

        self.vps.on_accept()

        vessel_name = self.config.settings['vessel_name']

        self.assertEqual(width, self.config.settings['vessel_configuration'][vessel_name]['vessel_width'])
        self.assertEqual(length, self.config.settings['vessel_configuration'][vessel_name]['vessel_length'])

        self.assertEqual(vrp_x, self.config.settings['vessel_configuration'][vessel_name]["vrp_offset_x"])
        self.assertEqual(vrp_y, self.config.settings['vessel_configuration'][vessel_name]["vrp_offset_y"])

        self.assertEqual(gps_x, self.config.settings['vessel_configuration'][vessel_name]['gps_offset_x'])
        self.assertEqual(gps_y, self.config.settings['vessel_configuration'][vessel_name]['gps_offset_y'])
        self.assertEqual(gps_heading, self.config.settings['vessel_configuration'][vessel_name]['gps_offset_heading'])

    def test_prints(self):
        print("TestVesselPosSystem.test_prints")
        self.vps.print_top_view()
        self.vps.print_side_view()


if __name__ == "__main__":
    unittest.main()
