# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Unit tests for addlayers.py
"""

import os
import sys
import unittest
from unittest.mock import Mock

from PyQt5.QtCore import QVariant
from PyQt5.QtWidgets import QMessageBox
from qgis.core import (Qgis,
                       QgsApplication,
                       QgsProject,
                       QgsApplication,
                       QgsFields,
                       QgsField,
                       QgsVectorFileWriter,
                       QgsFeature,
                       QgsGeometry,
                       QgsPointXY,
                       QgsVectorLayer,
                       QgsCoordinateTransformContext)
from qgis.gui import QgsMapCanvas

from iquaview_lib import resources_rc

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)

from iquaview.src.mapsetup.addlayers import (AddLayersDlg,
                                             VECTOR_LAYER,
                                             DELIMITED_TEXT_LAYER,
                                             RASTER_LAYER,
                                             TILED_WEB_MAP_LAYER)


class TestAddLayersDlg(unittest.TestCase):
    """
    Unit tests for addlayers.py
    """
    @classmethod
    def setUpClass(cls) -> None:
        print("TestAddLayersDlg.setUp")
        cls.app = QgsApplication.instance()
        if cls.app is None:
            cls.app = QgsApplication([], False)

        cls.proj = QgsProject.instance()
        cls.canvas = QgsMapCanvas()
        cls.msg_log = QgsApplication.messageLog()

        cls.proj.setFileName("")
        cls.dialog = AddLayersDlg(cls.proj, cls.canvas, cls.msg_log)

    @classmethod
    def tearDownClass(cls) -> None:
        print("TestAddLayersDlg.tearDownClass")

    def tearDown(self) -> None:
        self.proj.clear()

    def test_add_vector_layer(self):
        print("TestAddLayersDlg.test_add_vector_layer")
        QMessageBox.warning = Mock()
        self.dialog.set_current_page(VECTOR_LAYER)
        # empty
        self.dialog.vector_mQgsFileWidget.fileChanged.emit("")
        self.assertEqual(self.dialog.vector_name_lineEdit.text(), "")
        # valid
        self.dialog.vector_mQgsFileWidget.fileChanged.emit("test_vector_layer")
        self.assertEqual(self.dialog.vector_name_lineEdit.text(), "test_vector_layer")

        # create fields
        layer_fields = QgsFields()
        layer_fields.append(QgsField('ID', QVariant.Int))
        layer_fields.append(QgsField('Value', QVariant.Double))
        layer_fields.append(QgsField('Name', QVariant.String))

        shapefile = '/tmp/test_vector_layer.shp'
        layer = QgsVectorLayer(
            "MultiPoint?crs=epsg:4326&field=id:integer&field=value:double&field=name:string",
            shapefile,
            "memory")
        feat = QgsFeature()
        feat.setGeometry(QgsGeometry.fromPointXY(QgsPointXY(455618, 4632221)))
        feat.setAttributes([1, 1.1, 'one'])
        layer.addFeature(feat)

        options = QgsVectorFileWriter.SaveVectorOptions()
        options.driverName = "ESRI Shapefile"

        # if version is greater than 3.20
        if Qgis.QGIS_VERSION_INT >= 32000:
            writer = QgsVectorFileWriter.writeAsVectorFormatV3(layer, shapefile,
                                                               QgsCoordinateTransformContext(),
                                                               options)
        else:
            writer = QgsVectorFileWriter.writeAsVectorFormatV2(layer, shapefile,
                                                               QgsCoordinateTransformContext(),
                                                               options)

        self.dialog.vector_mQgsFileWidget.setFilePath(shapefile)
        # cannot be saved in the project because it has not been initialised.
        self.dialog.on_accept()

        # define project
        self.proj.setFileName("/tmp/test_project.qgs")
        self.proj.write()

        self.dialog.on_accept()

    @unittest.skip("TODO: fix this test")
    def test_tiled_web_map_layer(self):
        print("TestAddLayersDlg.test_tiled_web_map_layer")
        self.dialog.set_current_page(TILED_WEB_MAP_LAYER)

        self.dialog.mCheckBoxZMax_2.setChecked(True)

        # define project
        self.proj.setFileName("/tmp/test_project.qgs")
        self.proj.write()

        for i in range(0, self.dialog.url_comboBox.count()):
            self.dialog.url_comboBox.setCurrentIndex(i)
            self.dialog.on_accept()

if __name__ == "__main__":
    unittest.main()
