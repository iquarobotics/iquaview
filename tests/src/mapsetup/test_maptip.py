# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Unit tests for addlayers.py
"""

import os
import sys
import unittest
from unittest.mock import patch, PropertyMock

from PyQt5.QtCore import QTimer, QPoint
from PyQt5.QtWidgets import QToolTip
from qgis.core import QgsApplication
from qgis.gui import QgsMapCanvas

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)

from iquaview.src.mapsetup.maptip import MapTip


class TestMapTip(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        print("TestMapTip.setUp")
        cls.app = QgsApplication.instance()
        if cls.app is None:
            cls.app = QgsApplication([], False)

        cls.canvas = QgsMapCanvas()

        cls.map_tip = MapTip(cls.canvas)

    def test_map_tip_initialization(self):
        self.assertIsInstance(self.map_tip, MapTip)
        self.assertIsInstance(self.map_tip.map_tips_timer, QTimer)

    def test_reset_map_tip(self):
        # Set up an initial state where QToolTip is visible
        QToolTip.showText(QPoint(0, 0), 'InitialTooltipText')

        # Call reset_map_tip
        with patch('PyQt5.QtWidgets.QToolTip.hideText', new_callable=PropertyMock) as mock_hide_text:
            self.map_tip.reset_map_tip()

        # Assert that QToolTip.hideText was accessed
        mock_hide_text.assert_called()


if __name__ == "__main__":
    unittest.main()
