# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import unittest

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)

from qgis.gui import QgsMapCanvas
from qgis.core import QgsProject, QgsWkbTypes, QgsApplication

from iquaview.src.mapsetup.pointfeaturedlg import PointFeatureDlg
from iquaview_lib.config import Config
from iquaview_lib import resources_rc


class TestPointFeatureDlg(unittest.TestCase):

    def setUp(self):
        print("TestPointFeatureDlg.setUp")
        self.app = QgsApplication.instance()
        if self.app is None:
            self.app = QgsApplication([], False)

        self.canvas = QgsMapCanvas()
        self.proj = QgsProject.instance()
        self.config = Config()
        self.config.load()

        self.point_feature_dlg = PointFeatureDlg(self.canvas, self.proj, self.config)
        self.point_feature_dlg.landmark_added.connect(self.add_map_layer)
        self.point_feature_dlg.reset()

    def tearDown(self) -> None:
        print("TestPointFeatureDlg.tearDown")

    def test_add_point(self):
        print("TestPointFeatureDlg.test_add_point")
        lat = 41.1566
        lon = 3.24556
        self.point_feature_dlg.coordinate_converter_lineedit.setText("{}, {}".format(lat, lon))
        self.point_feature_dlg.coordinate_converter_lineedit.convert()
        self.point_feature_dlg.accept()

        layers = list(self.proj.mapLayers().values())
        self.assertEqual(1, len(layers))
        self.assertEqual(QgsWkbTypes.PointGeometry, layers[0].geometryType())

        for feature in layers[0].getFeatures():
            wp = feature.geometry().asPoint()
            self.assertAlmostEqual(lat, wp.y(), 8)
            self.assertAlmostEqual(lon, wp.x(), 8)

    def add_map_layer(self, layer):
        print("TestPointFeatureDlg.add_map_layer")
        self.proj.addMapLayer(layer, True)


if __name__ == "__main__":
    unittest.main()
