# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import unittest
from copy import copy
from unittest.mock import Mock

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)

from PyQt5.QtWidgets import QMessageBox

from qgis.core import QgsApplication, QgsPointXY, QgsMapLayer

from iquaview_lib.cola2api.mission_types import Mission
from iquaview.src.mapsetup.pointfeaturedlg import PointFeatureDlg
from iquaview.src import mainwindow

from iquaview_lib import resources_rc


class TestMenuProvider(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        print("TestMenuProvider.setUpClass")
        cls.app = QgsApplication.instance()
        if cls.app is None:
            cls.app = QgsApplication([], False)

        cls.mainwindow = mainwindow.MainWindow()
        cls.mainwindow.is_test = True

        # Add a mission layer
        cls.mission_name = "temp_mission"
        write_temp_mission_xml(cls.mission_name)
        cls.mainwindow.mission_ctrl.load_mission(os.getcwd() + "/" + cls.mission_name + ".xml")

        # Init point feature dlg (to add landmarks)
        cls.point_feature_dlg = PointFeatureDlg(cls.mainwindow.canvas, cls.mainwindow.proj, cls.mainwindow.config)
        cls.point_feature_dlg.reset()

        print("TestMenuProvider.setUp")

        # Add a landmark layer
        point = QgsPointXY(5.155, 7.753)
        cls.point_feature_dlg.add_new_landmark(point)

    @classmethod
    def tearDownClass(cls) -> None:
        print("TestMenuProvider.tearDownClass")
        print("TestMenuProvider.tearDown")
        os.remove(cls.mission_name + ".xml")
        cls.mainwindow.proj.removeAllMapLayers()
        cls.mainwindow.close()
        cls.mainwindow.deleteLater()

    def setUp(self) -> None:
        self.mainwindow.mission_ctrl.mission_added.connect(self.add_map_layer)
        self.point_feature_dlg.landmark_added.connect(self.add_map_layer)

    def test(self):
        print("TestMenuProvider.test")
        for layer in self.mainwindow.proj.mapLayers().values():
            self.mainwindow.view.setCurrentLayer(layer)
            self.mainwindow.menu_provider.createContextMenu()
            QMessageBox.question = Mock(return_value=QMessageBox.Yes)
            self.mainwindow.menu_provider.remove_group_or_layer()

        self.app.processEvents()
        self.mainwindow.proj.removeAllMapLayers()
        self.assertEqual(0, len(self.mainwindow.proj.mapLayers()))

    def add_map_layer(self, layer):
        """
        Add layer to  the current project, view and canvas
        :param layer: layer
        :type layer: QgsMapLayer
        """
        print("TestMenuProvider.add_map_layer")
        self.mainwindow.proj.addMapLayer(layer, True)
        self.mainwindow.view.setCurrentLayer(layer)
        self.mainwindow.canvas.refresh()


def write_temp_mission_xml(name):
    print("TestMenuProvider.write_temp_mission_xml")
    mission = Mission()
    mission.write_mission(name + '.xml')


if __name__ == "__main__":
    unittest.main()
