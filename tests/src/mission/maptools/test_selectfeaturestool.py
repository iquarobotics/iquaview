# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import unittest
from copy import copy

from PyQt5.QtCore import QPoint, Qt, QEvent
from PyQt5.QtGui import QKeyEvent
from qgis.core import QgsProject, QgsApplication, QgsPointXY
from qgis.gui import QgsMapCanvas, QgsMapMouseEvent

from iquaview.src.mission.maptools.selectfeaturestool import SelectFeaturesTool
from iquaview.src.mission.missioncontroller import MissionController
from iquaview_lib.cola2api.mission_types import (Mission,
                                                 MissionStep,
                                                 MissionGoto)
from iquaview_lib.config import Config
from iquaview_lib.vehicle.vehicleinfo import VehicleInfo

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)


class TestEditTool(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        print("TestEditTool.setUpClass")
        cls.app = QgsApplication.instance()
        if cls.app is None:
            cls.app = QgsApplication([], False)

    def setUp(self):
        print("TestEditTool.setUp")
        self.config = Config()
        self.config.load()

        xml_filename = "{}".format(os.path.abspath(os.path.expanduser(self.config.settings['last_auv_config_xml'])))
        self.vehicle_info = VehicleInfo(xml_filename)

        self.proj = QgsProject.instance()
        self.proj.setFileName("")

        self.canvas = QgsMapCanvas()
        self.view = None
        self.wp_dock = None
        self.templates_dock = None
        self.minfo_dock = None
        self.msg_log = QgsApplication.messageLog()

        self.mission_ctrl = MissionController(self.config, self.vehicle_info, self.proj, self.canvas, self.view,
                                              self.wp_dock, self.templates_dock, self.minfo_dock, self.msg_log)

        self.mission_name = "temp_selectfeaturestool_mission"
        self.write_temp_mission_xml()
        self.mission_ctrl.load_mission(os.getcwd() + "/" + self.mission_name + ".xml")

        self.mission_track = self.mission_ctrl.mission_list[0]

        self.select_tool = SelectFeaturesTool(self.mission_track, self.canvas)


    def tearDown(self):
        print("TestEditTool.tearDown")
        os.remove(self.mission_name + ".xml")

    def test_individual_selection(self):
        print("TestEditTool.test_individual_selection")
        x = self.mission_track.get_step(0).get_maneuver().final_longitude
        y = self.mission_track.get_step(0).get_maneuver().final_latitude
        first_wp_canvas = self.select_tool.toCanvasCoordinates(QgsPointXY(float(x), float(y)))

        x = self.mission_track.get_step(1).get_maneuver().final_longitude
        y = self.mission_track.get_step(1).get_maneuver().final_latitude
        second_wp_canvas = self.select_tool.toCanvasCoordinates(QgsPointXY(float(x), float(y)))

        x = self.mission_track.get_step(2).get_maneuver().final_longitude
        y = self.mission_track.get_step(2).get_maneuver().final_latitude
        third_wp_canvas = self.select_tool.toCanvasCoordinates(QgsPointXY(float(x), float(y)))

        key_press_event = QKeyEvent(QEvent.KeyPress, Qt.Key_Control, Qt.NoModifier)
        self.select_tool.keyPressEvent(key_press_event)

        press_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, first_wp_canvas, Qt.LeftButton)
        release_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonRelease, first_wp_canvas, Qt.LeftButton)
        self.select_tool.canvasPressEvent(press_event)
        self.select_tool.canvasReleaseEvent(release_event)

        press_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, second_wp_canvas, Qt.LeftButton)
        release_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonRelease, second_wp_canvas, Qt.LeftButton)
        self.select_tool.canvasPressEvent(press_event)
        self.select_tool.canvasReleaseEvent(release_event)

        press_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, third_wp_canvas, Qt.LeftButton)
        release_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonRelease, third_wp_canvas, Qt.LeftButton)
        self.select_tool.canvasPressEvent(press_event)
        self.select_tool.canvasReleaseEvent(release_event)

        key_release_event = QKeyEvent(QEvent.KeyRelease, Qt.Key_Control, Qt.NoModifier)
        self.select_tool.keyReleaseEvent(key_release_event)

        self.assertEqual(3, len(self.select_tool.get_indexes_within_list()))

        # delete wp
        for step in reversed(range(0, 3)):
            self.mission_track.remove_step(step)

        self.assertEqual(0, len(self.select_tool.get_indexes_within_list()))

    def test_multiple_selection(self):
        print("TestEditTool.test_multiple_selection")
        press_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, QPoint(0, 0), Qt.LeftButton)
        release_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonRelease, QPoint(0, 0), Qt.LeftButton)
        self.select_tool.canvasPressEvent(press_event)
        self.select_tool.canvasReleaseEvent(release_event)

        move_event = QgsMapMouseEvent(self.canvas, QEvent.MouseMove, QPoint(10, -60))
        self.select_tool.canvasMoveEvent(move_event)

        press_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, QPoint(10, -60), Qt.LeftButton)
        release_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonRelease, QPoint(10, -60), Qt.LeftButton)
        self.select_tool.canvasPressEvent(press_event)
        self.select_tool.canvasReleaseEvent(release_event)

        self.assertEqual(3, len(self.select_tool.get_indexes_within_list()))

        # delete wp
        for step in reversed(range(0, 3)):
            self.mission_track.remove_step(step)

        self.assertEqual(0, len(self.select_tool.get_indexes_within_list()))

    def write_temp_mission_xml(self):
        mission = Mission()

        wp1 = MissionGoto(42, 4, 15.0, 0.0, 0, 0.5, 2.0, True)
        wp2 = MissionGoto(45, 7, 15.0, 0.0, 0, 0.5, 2.0, True)
        wp3 = MissionGoto(39, 2, 15.0, 0.0, 0, 0.5, 2.0, True)

        mission_step1 = MissionStep()
        mission_step2 = MissionStep()
        mission_step3 = MissionStep()

        mission_step1.add_maneuver(wp1)
        mission_step2.add_maneuver(wp2)
        mission_step3.add_maneuver(wp3)

        mission.add_step(mission_step1)
        mission.add_step(mission_step2)
        mission.add_step(mission_step3)

        mission.write_mission(self.mission_name + '.xml')


if __name__ == "__main__":
    unittest.main()
