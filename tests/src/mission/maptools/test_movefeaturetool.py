# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import unittest
from copy import copy
from unittest.mock import Mock

from PyQt5.QtCore import QPoint, Qt, QEvent
from PyQt5.QtGui import QKeyEvent
from PyQt5.QtWidgets import QMessageBox
from qgis.core import QgsProject, QgsApplication, QgsPointXY
from qgis.gui import QgsMapCanvas, QgsMapMouseEvent

from iquaview.src.mission.maptools.movefeaturetool import MoveFeatureTool
from iquaview.src.mission.missioncontroller import MissionController
from iquaview_lib.cola2api.mission_types import (Mission,
                                                 MissionStep,
                                                 MissionGoto)
from iquaview_lib.config import Config
from iquaview_lib.vehicle.vehicleinfo import VehicleInfo

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)


class TestMoveTool(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        print("TestMoveTool.setUpClass")
        cls.app = QgsApplication.instance()
        if cls.app is None:
            cls.app = QgsApplication([], False)

    def setUp(self):
        print("TestMoveTool.setUp")
        self.config = Config()
        self.config.load()

        xml_filename = "{}".format(os.path.abspath(os.path.expanduser(self.config.settings['last_auv_config_xml'])))
        self.vehicle_info = VehicleInfo(xml_filename)

        self.proj = QgsProject.instance()
        self.proj.setFileName("")

        self.canvas = QgsMapCanvas()
        self.view = None
        self.wp_dock = None
        self.templates_dock = None
        self.minfo_dock = None
        self.msg_log = QgsApplication.messageLog()

        self.mission_ctrl = MissionController(self.config, self.vehicle_info, self.proj, self.canvas, self.view,
                                              self.wp_dock, self.templates_dock, self.minfo_dock, self.msg_log)

        self.mission_name = "temp_movefeaturetool_mission"
        self.write_temp_mission_xml()
        self.mission_ctrl.load_mission(os.getcwd() + "/" + self.mission_name + ".xml")

        self.mission_track = self.mission_ctrl.mission_list[0]

        self.move_tool = MoveFeatureTool(self.mission_track, self.canvas, self.msg_log)

    @classmethod
    def tearDownClass(cls) -> None:
        print("TestMoveTool.tearDownClass")

    def tearDown(self):
        print("TestMoveTool.tearDown")
        os.remove(self.mission_name + ".xml")

    def test_rotation(self):
        print("TestMoveTool.test_rotation")
        m_step = self.mission_track.get_step(0)

        ini_m_point = QgsPointXY(float(m_step.get_maneuver().final_latitude),
                                 float(m_step.get_maneuver().final_longitude))

        ini_p = QPoint(0, 0)
        end_p = QPoint(200, 50)
        self.do_rotation(ini_p, end_p)

        final_m_point = QgsPointXY(float(m_step.get_maneuver().final_latitude),
                                   float(m_step.get_maneuver().final_longitude))

        # if point rotated, it must have changed
        self.assertNotEqual(ini_m_point, final_m_point)

    def test_move(self):
        print("TestMoveTool.test_move")
        m_step = self.mission_track.get_step(0)
        ini_m_point = QgsPointXY(float(m_step.get_maneuver().final_latitude),
                                 float(m_step.get_maneuver().final_longitude))

        # get the first point
        first_wp = QgsPointXY(float(m_step.get_maneuver().final_longitude), float(m_step.get_maneuver().final_latitude))

        ini_p = self.move_tool.toCanvasCoordinates(first_wp)
        end_p = QPoint(ini_p.x() + 25, ini_p.y() + 25)
        self.do_move(ini_p, end_p)
        QMessageBox.question = Mock(return_value=QMessageBox.Yes)
        m_point = QgsPointXY(float(m_step.get_maneuver().final_latitude), float(m_step.get_maneuver().final_longitude))

        # if point moved, it must have changed
        self.assertNotEqual(ini_m_point, m_point)

    def test_undo(self):
        print("TestMoveTool.test_undo")
        latitude = self.mission_track.get_step(0).get_maneuver().final_latitude
        longitude = self.mission_track.get_step(0).get_maneuver().final_longitude
        first_wp = QgsPointXY(float(longitude), float(latitude))

        pos_list = []
        pos_list.append(first_wp)

        for i in range(0, 25):
            # Drag the mission
            first_wp = QgsPointXY(float(longitude), float(latitude))
            ini_p = self.move_tool.toCanvasCoordinates(first_wp)
            end_p = QPoint(ini_p.x() + 1, ini_p.y() + 1)
            self.do_move(ini_p, end_p)

            # Save position
            pos_list.append(QgsPointXY(float(longitude), float(latitude)))

            # Rotate the mission
            ini_p = QPoint(0, 0)
            end_p = QPoint(50, 50)
            self.do_rotation(ini_p, end_p)

            # Save position
            pos_list.append(QgsPointXY(float(longitude), float(latitude)))

        # Check for every saved position, check with current position and undo last move or rotation
        for pos in reversed(pos_list):
            curr_pos = QgsPointXY(float(longitude), float(latitude))
            self.assertEqual(pos, curr_pos)
            self.do_undo()

    def do_rotation(self, ini_p, end_p):
        """ simulate a rotation """
        # create fake events
        ini_move_event = QgsMapMouseEvent(self.canvas, QEvent.MouseMove, ini_p)
        final_move_event = QgsMapMouseEvent(self.canvas, QEvent.MouseMove, end_p)
        release_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonRelease, end_p, Qt.LeftButton)
        key_press_event = QKeyEvent(QEvent.KeyPress, Qt.Key_Shift, Qt.NoModifier)
        key_release_event = QKeyEvent(QEvent.KeyRelease, Qt.Key_Shift, Qt.NoModifier)

        # calling the events
        QgsApplication.keyboardModifiers = Mock(return_value=Qt.ShiftModifier)
        self.move_tool.keyPressEvent(key_press_event)
        self.move_tool.start_rotation()
        self.move_tool.canvasMoveEvent(ini_move_event)
        self.move_tool.canvasMoveEvent(final_move_event)
        self.move_tool.canvasReleaseEvent(release_event)
        QgsApplication.keyboardModifiers = Mock(return_value=Qt.NoModifier)
        self.move_tool.keyReleaseEvent(key_release_event)

    def do_move(self, ini_p, end_p):
        """ simulate moving mission by dragging """
        # create fake events
        press_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, ini_p, Qt.LeftButton)
        move_event = QgsMapMouseEvent(self.canvas, QEvent.MouseMove, end_p)
        release_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonRelease, end_p, Qt.LeftButton)

        # calling the events
        self.move_tool.start_move(press_event.pos())
        self.move_tool.canvasMoveEvent(move_event)
        self.move_tool.canvasReleaseEvent(release_event)

    def do_undo(self):
        """ Simulate ctrl+z press """
        # create fake events
        key_press_event = QKeyEvent(QEvent.KeyPress, Qt.Key_Z, Qt.ControlModifier)
        key_release_event = QKeyEvent(QEvent.KeyRelease, Qt.Key_Z, Qt.ControlModifier)

        # calling the events
        self.move_tool.keyPressEvent(key_press_event)
        self.move_tool.keyReleaseEvent(key_release_event)

    def write_temp_mission_xml(self):
        mission = Mission()

        wp1 = MissionGoto(-41.7813293, 3.03173175, 15.0, 0.0, 0, 0.5, 2.0, True)
        wp2 = MissionGoto(-41.787, 3.034, 15.0, 0.0, 0, 0.5, 2.0, True)
        wp3 = MissionGoto(-41.777, 3.030, 15.0, 0.0, 0, 0.5, 2.0, True)
        wp3 = MissionGoto(-42, 3, 15.0, 0.0, 0, 0.5, 2.0, True)

        mission_step1 = MissionStep()
        mission_step2 = MissionStep()
        mission_step3 = MissionStep()

        mission_step1.add_maneuver(wp1)
        mission_step2.add_maneuver(wp2)
        mission_step3.add_maneuver(wp3)

        mission.add_step(mission_step1)
        mission.add_step(mission_step2)
        mission.add_step(mission_step3)

        mission.write_mission(self.mission_name + '.xml')


if __name__ == "__main__":
    unittest.main()
