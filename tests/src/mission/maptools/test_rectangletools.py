# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import unittest
from distutils.version import LooseVersion

from PyQt5.QtCore import QPoint, QEvent, Qt, QT_VERSION_STR
from PyQt5.QtGui import QKeyEvent
from qgis.core import QgsPointXY, QgsCoordinateReferenceSystem, QgsDistanceArea, QgsApplication
from qgis.gui import QgsMapCanvas, QgsMapMouseEvent

from iquaview.src.mission.maptools.rectangletools import Rectangle, RectBy3PointsTool, RectByFixedExtentTool, \
    RectFromCenterTool, RectFromCenterFixedTool
from iquaview_lib.utils.calcutils import distance_ellipsoid

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)


class TestRectangleTools(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        print("TestRectangleTools.setUpClass")
        cls.app = QgsApplication.instance()
        if cls.app is None:
            cls.app = QgsApplication([], False)

    def setUp(self):
        print("TestRectangleTools.setUp")
        self.rectangle = Rectangle(QgsCoordinateReferenceSystem.fromEpsgId(4326))

        self.canvas = QgsMapCanvas()

        self.rect_by_3_points_tool = RectBy3PointsTool(self.canvas)

        self.x_length = 45
        self.y_length = 30
        self.rect_by_fixed_extent_tool = RectByFixedExtentTool(self.canvas, self.x_length, self.y_length)

        self.rect_from_center_tool = RectFromCenterTool(self.canvas)

        self.rect_from_center_fixed_tool = RectFromCenterFixedTool(self.canvas, self.x_length, self.y_length)

        self.geom = None

    @unittest.skipIf(LooseVersion(QT_VERSION_STR) >= LooseVersion("5.12.0"),
                     "not tested in this library version")
    def test_rectangle(self):
        print("TestRectangleTools.test_rectangle")
        center_point = QgsPointXY(3, 3)
        corner_point = QgsPointXY(5, 5)
        angle = 0

        geom = self.rectangle.get_rect_from_center(center_point, corner_point, angle)

        # contains center point
        self.assertEqual(True, geom.contains(center_point))
        # 5 point geometry
        for multipoint_list in geom.asPolygon():
            self.assertEqual(5, len(multipoint_list))

        geom = self.rectangle.get_rect_projection(geom, center_point)
        self.assertEqual(True, geom.contains(center_point))
        for multipoint_list in geom.asPolygon():
            self.assertEqual(5, len(multipoint_list))

        angle = 1.2
        geom = self.rectangle.get_rect_from_center(center_point, corner_point, angle)
        self.assertEqual(True, geom.contains(QgsPointXY(3, 3)))
        for multipoint_list in geom.asPolygon():
            self.assertEqual(5, len(multipoint_list))

        ini_rot = QgsPointXY(10, 10)
        end_rot = QgsPointXY(0, 10)
        rect_rotated = self.rectangle.get_rect_rotated(geom, center_point, ep=end_rot, ip=ini_rot, delta=0.1)

        geom = rect_rotated[0]
        angle = rect_rotated[1]

        self.assertEqual(float, type(angle))
        self.assertEqual(True, geom.contains(QgsPointXY(3, 3)))
        for multipoint_list in geom.asPolygon():
            self.assertEqual(5, len(multipoint_list))

        p1 = QgsPointXY(1, 1)
        p2 = QgsPointXY(4, 1)
        p3 = QgsPointXY(1, 4)
        geom = self.rectangle.get_rect_by3_points(p1, p2, p3)
        self.assertEqual(True, geom.contains(QgsPointXY(2, 2)))
        for multipoint_list in geom.asPolygon():
            self.assertEqual(5, len(multipoint_list))

    @unittest.skipIf(LooseVersion(QT_VERSION_STR) >= LooseVersion("5.12.0"),
                     "not tested in this library version")
    def test_rect_by_3_points_tool(self):
        print("TestRectangleTools.test_rect_by_3_points_tool")
        self.rect_by_3_points_tool.activate()

        point_1 = QPoint(0, 0)
        point_2 = QPoint(10, 0)
        point_3 = QPoint(10, 10)

        d1 = QgsDistanceArea().measureLine(QgsPointXY(point_1), QgsPointXY(point_2))
        d2 = QgsDistanceArea().measureLine(QgsPointXY(point_2), QgsPointXY(point_3))
        area = d1 * d2

        mouse_press_event_1 = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, point_1, Qt.LeftButton)
        mouse_move_event_1 = QgsMapMouseEvent(self.canvas, QEvent.MouseMove, point_2)
        mouse_press_event_2 = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, point_2, Qt.LeftButton)
        mouse_move_event_2 = QgsMapMouseEvent(self.canvas, QEvent.MouseMove, point_3)
        mouse_press_event_3 = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, point_3, Qt.LeftButton)

        self.rect_by_3_points_tool.rbFinished.connect(self.catch_signal_rb_geom)

        # Create the rect geometry
        self.rect_by_3_points_tool.canvasPressEvent(mouse_press_event_1)
        self.rect_by_3_points_tool.canvasMoveEvent(mouse_move_event_1)
        self.rect_by_3_points_tool.canvasPressEvent(mouse_press_event_2)
        self.rect_by_3_points_tool.canvasMoveEvent(mouse_move_event_2)
        self.rect_by_3_points_tool.canvasPressEvent(mouse_press_event_3)

        # here the tool emits a signal with the geometry created, caught with the connect above

        # area  wont be exact due to the CRS approximations to an spheroid
        self.assertAlmostEqual(self.geom.area(), area, 3)

        event_key_shift = QKeyEvent(QEvent.KeyPress, Qt.Key_Shift, Qt.NoModifier)
        event_key_escape = QKeyEvent(QEvent.KeyPress, Qt.Key_Escape, Qt.NoModifier)

        self.rect_by_3_points_tool.keyPressEvent(event_key_shift)
        self.rect_by_3_points_tool.keyReleaseEvent(event_key_escape)

        self.rect_by_3_points_tool.deactivate()

    def test_rect_fixed_extent_tool(self):
        print("TestRectangleTools.test_rect_fixed_extent_tool")
        self.rect_by_fixed_extent_tool.activate()

        point_1 = QPoint(0, 0)
        point_2 = QPoint(10, 0)
        point_3 = QPoint(10, 10)

        mouse_press_event_1 = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, point_1, Qt.LeftButton)
        mouse_move_event_1 = QgsMapMouseEvent(self.canvas, QEvent.MouseMove, point_2)
        mouse_press_event_2 = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, point_2, Qt.LeftButton)
        mouse_move_event_2 = QgsMapMouseEvent(self.canvas, QEvent.MouseMove, point_3)
        mouse_press_event_3 = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, point_3, Qt.LeftButton)

        self.rect_by_fixed_extent_tool.rbFinished.connect(self.catch_signal_rb_geom)

        self.rect_by_fixed_extent_tool.canvasPressEvent(mouse_press_event_1)
        self.rect_by_fixed_extent_tool.canvasMoveEvent(mouse_move_event_1)
        self.rect_by_fixed_extent_tool.canvasPressEvent(mouse_press_event_2)
        self.rect_by_fixed_extent_tool.canvasMoveEvent(mouse_move_event_2)
        self.rect_by_fixed_extent_tool.canvasPressEvent(mouse_press_event_3)

        d1 = distance_ellipsoid(self.geom.vertexAt(0).x(), self.geom.vertexAt(0).y(),
                                self.geom.vertexAt(1).x(), self.geom.vertexAt(1).y())
        d2 = distance_ellipsoid(self.geom.vertexAt(1).x(), self.geom.vertexAt(1).y(),
                                self.geom.vertexAt(2).x(), self.geom.vertexAt(2).y())
        d3 = distance_ellipsoid(self.geom.vertexAt(2).x(), self.geom.vertexAt(2).y(),
                                self.geom.vertexAt(3).x(), self.geom.vertexAt(3).y())
        d4 = distance_ellipsoid(self.geom.vertexAt(3).x(), self.geom.vertexAt(3).y(),
                                self.geom.vertexAt(4).x(), self.geom.vertexAt(4).y())

        # check that all sides are correctly fixed by the lengths
        self.assertAlmostEqual(d1, self.x_length, 0)
        self.assertAlmostEqual(d2, self.y_length, 0)
        self.assertAlmostEqual(d3, d1, 0)
        self.assertAlmostEqual(d4, d2, 0)

        event_key_shift = QKeyEvent(QEvent.KeyPress, Qt.Key_Shift, Qt.NoModifier)
        event_key_escape = QKeyEvent(QEvent.KeyPress, Qt.Key_Escape, Qt.NoModifier)

        self.rect_by_fixed_extent_tool.keyPressEvent(event_key_shift)
        self.rect_by_fixed_extent_tool.keyReleaseEvent(event_key_escape)

        self.rect_by_fixed_extent_tool.deactivate()

    @unittest.skipIf(LooseVersion(QT_VERSION_STR) >= LooseVersion("5.12.0"),
                     "not tested in this library version")
    def test_rect_from_center_tool(self):
        print("TestRectangleTools.test_rect_from_center_tool")
        self.rect_from_center_tool.activate()

        point_1 = QPoint(2, 2)
        point_2 = QPoint(3, 3)

        area = abs(point_1.x() - point_2.x()) * 2 * abs(point_1.y() - point_2.y()) * 2

        mouse_press_event_1 = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, point_1, Qt.LeftButton)
        mouse_move_event = QgsMapMouseEvent(self.canvas, QEvent.MouseMove, point_2)
        mouse_press_event_2 = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, point_2, Qt.LeftButton)

        self.rect_from_center_tool.rbFinished.connect(self.catch_signal_rb_geom)

        self.rect_from_center_tool.canvasPressEvent(mouse_press_event_1)
        self.rect_from_center_tool.canvasMoveEvent(mouse_move_event)
        self.rect_from_center_tool.canvasPressEvent(mouse_press_event_2)

        self.assertAlmostEqual(self.geom.area(), area, 3)

        event_key_shift = QKeyEvent(QEvent.KeyPress, Qt.Key_Shift, Qt.NoModifier)
        event_key_escape = QKeyEvent(QEvent.KeyPress, Qt.Key_Escape, Qt.NoModifier)

        self.rect_from_center_tool.keyPressEvent(event_key_shift)
        self.rect_from_center_tool.keyReleaseEvent(event_key_escape)

        self.rect_from_center_tool.deactivate()

    def test_rect_from_center_fixed_tool(self):
        print("TestRectangleTools.test_rect_from_center_fixed_tool")
        self.rect_from_center_fixed_tool.activate()

        point_1 = QPoint(2, 2)
        point_2 = QPoint(3, 3)

        mouse_press_event_1 = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, point_1, Qt.LeftButton)
        mouse_move_event = QgsMapMouseEvent(self.canvas, QEvent.MouseMove, point_2)
        mouse_press_event_2 = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, point_2, Qt.LeftButton)

        self.rect_from_center_fixed_tool.rbFinished.connect(self.catch_signal_rb_geom)

        self.rect_from_center_fixed_tool.canvasPressEvent(mouse_press_event_1)
        self.rect_from_center_fixed_tool.canvasMoveEvent(mouse_move_event)
        self.rect_from_center_fixed_tool.canvasPressEvent(mouse_press_event_2)

        d1 = distance_ellipsoid(self.geom.vertexAt(0).x(), self.geom.vertexAt(0).y(),
                                self.geom.vertexAt(1).x(), self.geom.vertexAt(1).y())
        d2 = distance_ellipsoid(self.geom.vertexAt(1).x(), self.geom.vertexAt(1).y(),
                                self.geom.vertexAt(2).x(), self.geom.vertexAt(2).y())
        d3 = distance_ellipsoid(self.geom.vertexAt(2).x(), self.geom.vertexAt(2).y(),
                                self.geom.vertexAt(3).x(), self.geom.vertexAt(3).y())
        d4 = distance_ellipsoid(self.geom.vertexAt(3).x(), self.geom.vertexAt(3).y(),
                                self.geom.vertexAt(4).x(), self.geom.vertexAt(4).y())

        # check that all sides are correctly fixed by the lengths
        self.assertAlmostEqual(d1, self.x_length, 0)
        self.assertAlmostEqual(d2, self.y_length, 0)
        self.assertAlmostEqual(d3, d1, 0)
        self.assertAlmostEqual(d4, d2, 0)

        event_key_shift = QKeyEvent(QEvent.KeyPress, Qt.Key_Shift, Qt.NoModifier)
        event_key_escape = QKeyEvent(QEvent.KeyPress, Qt.Key_Escape, Qt.NoModifier)

        self.rect_from_center_fixed_tool.keyPressEvent(event_key_shift)
        self.rect_from_center_fixed_tool.keyReleaseEvent(event_key_escape)

        self.rect_from_center_fixed_tool.deactivate()

    def catch_signal_rb_geom(self, geom):
        self.geom = geom


if __name__ == "__main__":
    unittest.main()
