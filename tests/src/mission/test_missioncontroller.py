# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import unittest
from unittest.mock import Mock

from PyQt5.QtWidgets import QDialog, QMessageBox
from qgis.core import QgsVectorLayer, QgsApplication

from iquaview.src.mainwindow import MainWindow
from iquaview.src.mission.missiontrack import MissionTrack
from iquaview.src.mission.newmissiondlg import NewMissionDlg

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)


class TestMissionController(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        print("TestMissionController.setUpClass")
        cls.app = QgsApplication.instance()
        if cls.app is None:
            cls.app = QgsApplication([], False)

        cls.mainwindow = MainWindow()
        cls.mainwindow.is_test = True

    @classmethod
    def tearDownClass(cls) -> None:
        print("TestMissionController.tearDownClass")
        cls.mainwindow.close()
        cls.mainwindow.deleteLater()

    def setUp(self):
        print("TestMissionController.setUp")
        self.m_ctrl = self.mainwindow.mission_ctrl

    def test_mission(self):
        print("TestMissionController.test_mission")
        # Mock NewMissionDlg methods
        NewMissionDlg.exec_ = Mock(return_value=QDialog.Rejected)
        self.m_ctrl.new_mission()

        NewMissionDlg.exec_ = Mock(return_value=QDialog.Accepted)
        NewMissionDlg.get_mission_name = Mock(return_value="mission_1")
        self.m_ctrl.new_mission()
        self.mainwindow.layer_changed()

        mission = self.m_ctrl.get_current_mission()
        layer = self.m_ctrl.get_current_mission_layer()
        path = self.m_ctrl.get_current_mission_absolute_path()
        name = self.m_ctrl.get_current_mission_name()

        self.assertEqual(MissionTrack, type(mission))
        self.assertEqual(QgsVectorLayer, type(layer))
        self.assertEqual(str, type(path))
        self.assertEqual(str, type(name))

        self.m_ctrl.set_current_mission_filename("mission_2")
        self.m_ctrl.set_current_mission_name("mission_2")
        self.assertEqual("mission_2", self.m_ctrl.get_current_mission_name())

        self.m_ctrl.remove_mission(layer)

        self.assertFalse(mission in self.m_ctrl.get_mission_list())

    def test_tools(self):
        print("TestMissionController.test_tools")
        # Create an empty mission
        NewMissionDlg.exec_ = Mock(return_value=QDialog.Accepted)
        NewMissionDlg.get_mission_name = Mock(return_value="mission_1")
        self.m_ctrl.new_mission()

        # Open the different tools
        self.m_ctrl.edit_wp_mission()
        self.m_ctrl.finish_edit_wp_mission()

        self.m_ctrl.select_features_mission()
        self.m_ctrl.finish_select_features_mission()

        self.m_ctrl.add_template_mission()
        self.m_ctrl.close_template_editing()

        self.m_ctrl.move_mission()
        self.m_ctrl.finish_move_mission()

    def test_saving(self):
        print("TestMissionController.test_saving")
        # Create an empty mission
        NewMissionDlg.exec_ = Mock(return_value=QDialog.Accepted)
        NewMissionDlg.get_mission_name = Mock(return_value="mission_1")
        self.m_ctrl.new_mission()

        # Mock missiontrack save mission method, we are only testing if the method is called
        MissionTrack.save_mission = Mock(return_value=True)

        mission_path = self.m_ctrl.get_current_mission_absolute_path()

        # Mock a No answer
        QMessageBox.question = Mock(return_value=QMessageBox.No)
        self.m_ctrl.save_mission()

        # Mission should not be saved
        self.assertFalse(MissionTrack.save_mission.called)

        # Mock a Yes answer
        QMessageBox.question = Mock(return_value=QMessageBox.Yes)
        self.m_ctrl.save_mission()

        # Mission should be saved and exist in the system
        self.assertTrue(MissionTrack.save_mission.called)
