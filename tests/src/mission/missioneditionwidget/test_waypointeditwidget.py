# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import unittest
from unittest.mock import Mock

from PyQt5.QtCore import Qt
from PyQt5.QtTest import QTest
from PyQt5.QtWidgets import QMessageBox, QDialog
from qgis.core import QgsProject, QgsApplication
from qgis.gui import QgsMapCanvas

from iquaview.src.mission.missioncontroller import MissionController
from iquaview.src.mission.missionedition.loadaddactiondialog import Loadaddactiondialog
from iquaview.src.mission.missionedition.waypointeditwidget import WaypointEditWidget
from iquaview.src.mission.missiontrack import MissionTrack
from iquaview_lib.cola2api.mission_types import (Mission,
                                                 MissionStep,
                                                 Parameter,
                                                 MissionAction,
                                                 MissionSection,
                                                 MissionPark,
                                                 MissionGoto)
from iquaview_lib.config import Config
from iquaview_lib.vehicle.vehicleinfo import VehicleInfo

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)


class TestWaypointEditWidget(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        print("TestWaypointEditWidget.setUpClass")
        cls.app = QgsApplication.instance()
        if cls.app is None:
            cls.app = QgsApplication([], False)

        cls.config = Config()
        cls.config.load()

        xml_filename = "{}".format(os.path.abspath(os.path.expanduser(cls.config.settings['last_auv_config_xml'])))
        cls.vehicle_info = VehicleInfo(xml_filename)

        cls.proj = QgsProject.instance()
        cls.proj.setFileName("")

        cls.canvas = QgsMapCanvas()
        cls.view = None
        cls.wp_dock = None
        cls.templates_dock = None
        cls.minfo_dock = None
        cls.msg_log = QgsApplication.messageLog()

        cls.mission_ctrl = MissionController(cls.config, cls.vehicle_info, cls.proj, cls.canvas, cls.view,
                                             cls.wp_dock, cls.templates_dock, cls.minfo_dock, cls.msg_log)

        cls.mission_name = "temp_edittool_mission"
        write_temp_mission_xml(cls.mission_name)
        cls.mission_ctrl.load_mission(os.getcwd() + "/" + cls.mission_name + ".xml")

        cls.mission_track = cls.mission_ctrl.mission_list[0]
        cls.vehicle_name = cls.vehicle_info.get_vehicle_namespace()

        # Mock warnings so no warning dialogs are actually created
        QMessageBox.warning = Mock()

    @classmethod
    def tearDownClass(cls) -> None:
        print("TestWaypointEditWidget.tearDownClass")
        os.remove(cls.mission_name + ".xml")

    def test_single_edition(self):
        print("TestWaypointEditWidget.test_single_edition")
        widget = WaypointEditWidget(self.config, self.mission_track, self.vehicle_name, multiple_edition=False)

        # z = 0 in altitude mode, forcing a warning
        widget.heave_mode_widget.on_heave_mode_box_changed(1)
        widget.heave_mode_widget.altitude_doubleSpinBox.setValue(0.0)

        # test different depth values
        widget.heave_mode_widget.altitude_doubleSpinBox.setValue(1)
        widget.heave_mode_widget.altitude_doubleSpinBox.setValue(99)
        widget.heave_mode_widget.altitude_doubleSpinBox.setValue(3.14)
        widget.heave_mode_widget.altitude_doubleSpinBox.setValue(-1)

        # Deactivate altitude mode
        widget.heave_mode_widget.on_heave_mode_box_changed(0)

        # test different z values
        widget.heave_mode_widget.depth_doubleSpinBox.setValue(1)
        widget.heave_mode_widget.depth_doubleSpinBox.setValue(99)
        widget.heave_mode_widget.depth_doubleSpinBox.setValue(3.14)
        widget.heave_mode_widget.depth_doubleSpinBox.setValue(-1)  # this should cause warning, it doesn't
        widget.heave_mode_widget.depth_doubleSpinBox.setValue(0.0)

        # test different coordinates
        widget.latitude_widget.coordinate_lineEdit.setText("0.0")
        widget.longitude_widget.coordinate_lineEdit.setText("0.0")
        widget.latitude_widget.coordinate_lineEdit.setText("1")
        widget.longitude_widget.coordinate_lineEdit.setText("-1")
        widget.latitude_widget.coordinate_lineEdit.setText("-1")
        widget.longitude_widget.coordinate_lineEdit.setText("1")
        widget.latitude_widget.coordinate_lineEdit.setText("75")
        widget.longitude_widget.coordinate_lineEdit.setText("75")
        widget.latitude_widget.coordinate_lineEdit.setText("-75")
        widget.longitude_widget.coordinate_lineEdit.setText("-75")
        widget.latitude_widget.coordinate_lineEdit.setText("41")
        widget.longitude_widget.coordinate_lineEdit.setText("3")

        # test different speeds
        widget.speed_widget.speed_doubleSpinBox.setValue(0.0)
        widget.speed_widget.speed_doubleSpinBox.setValue(-1)  # this should cause warning, it doesn't
        widget.speed_widget.speed_doubleSpinBox.setValue(1)
        widget.speed_widget.speed_doubleSpinBox.setValue(2)
        widget.speed_widget.speed_doubleSpinBox.setValue(0.2)
        widget.speed_widget.speed_doubleSpinBox.setValue(0.5)

        # test different tolerances
        widget.park_widget.tolerance_widget.tolerance_xy_doubleSpinBox.setValue(0.0)  # this should cause warning, it doesn't
        widget.park_widget.tolerance_widget.tolerance_xy_doubleSpinBox.setValue(0.5)
        widget.park_widget.tolerance_widget.tolerance_xy_doubleSpinBox.setValue(1)
        widget.park_widget.tolerance_widget.tolerance_xy_doubleSpinBox.setValue(-1)  # this should cause warning, it doesn't
        widget.park_widget.tolerance_widget.tolerance_xy_doubleSpinBox.setValue(2)

        # Check buttons
        QTest.mouseClick(widget.nextWpButton, Qt.LeftButton)
        QTest.mouseClick(widget.previousWpButton, Qt.LeftButton)

        # Check that mission_track remove_step is called when "delete waypoint" button is pressed
        # temporary store real method
        real_method = MissionTrack.remove_step
        MissionTrack.remove_step = Mock()
        QTest.mouseClick(widget.delete_pushButton, Qt.LeftButton)
        self.assertTrue(MissionTrack.remove_step.called)

        # restore remove_step method
        MissionTrack.remove_step = real_method

        # Changing maneuver type
        widget.heave_mode_widget.on_heave_mode_box_changed(1)
        widget.heave_mode_widget.on_heave_mode_box_changed(2)
        widget.heave_mode_widget.on_heave_mode_box_changed(0)

        # Add action
        Loadaddactiondialog.exec_ = Mock(return_value=QDialog.Accepted)
        QTest.mouseClick(widget.mission_actions_widget.addAction_toolButton, Qt.LeftButton)

        # Remove action
        QTest.mouseClick(widget.mission_actions_widget.removeAction_toolButton, Qt.LeftButton)
        widget.mission_actions_widget.update_remove_button()

    def test_multiple_edition(self):
        print("TestWaypointEditWidget.test_multiple_edition")
        widget = WaypointEditWidget(self.config, self.mission_track, self.vehicle_name, multiple_edition=True)
        widget.heave_mode_widget.on_heave_mode_box_changed(0)

        # Select all waypoints from the mission
        waypoints = list(range(0, self.mission_track.get_mission_length()))
        widget.show_multiple_features(waypoints)

        # z = 0 in altitude mode, forcing a warning
        widget.heave_mode_widget.on_heave_mode_box_changed(1)
        widget.heave_mode_widget.set_altitude(0.0)

        # test different z values
        widget.heave_mode_widget.set_altitude(1)
        widget.heave_mode_widget.set_altitude(99)
        widget.heave_mode_widget.set_altitude(3.14)
        widget.heave_mode_widget.set_altitude(-1)

        # Deactivate altitude mode
        widget.heave_mode_widget.on_heave_mode_box_changed(0)

        # test different z values
        widget.heave_mode_widget.depth_doubleSpinBox.setValue(1)
        widget.heave_mode_widget.depth_doubleSpinBox.setValue(99)
        widget.heave_mode_widget.depth_doubleSpinBox.setValue(3.14)
        widget.heave_mode_widget.depth_doubleSpinBox.setValue(-1)  # this should cause warning, it doesn't
        widget.heave_mode_widget.depth_doubleSpinBox.setValue(0.0)

        # test different coordinates
        widget.latitude_widget.coordinate_lineEdit.setText("0.0")
        widget.longitude_widget.coordinate_lineEdit.setText("0.0")
        widget.latitude_widget.coordinate_lineEdit.setText("1")
        widget.longitude_widget.coordinate_lineEdit.setText("-1")
        widget.latitude_widget.coordinate_lineEdit.setText("-1")
        widget.longitude_widget.coordinate_lineEdit.setText("1")
        widget.latitude_widget.coordinate_lineEdit.setText("75")
        widget.longitude_widget.coordinate_lineEdit.setText("75")
        widget.latitude_widget.coordinate_lineEdit.setText("-75")
        widget.longitude_widget.coordinate_lineEdit.setText("-75")
        widget.latitude_widget.coordinate_lineEdit.setText("41")
        widget.longitude_widget.coordinate_lineEdit.setText("3")

        # test different speeds
        widget.speed_widget.speed_doubleSpinBox.setValue(0.0)
        widget.speed_widget.speed_doubleSpinBox.setValue(1)
        widget.speed_widget.speed_doubleSpinBox.setValue(2)
        widget.speed_widget.speed_doubleSpinBox.setValue(0.2)
        widget.speed_widget.speed_doubleSpinBox.setValue(0.5)

        # test different tolerances
        widget.park_widget.tolerance_widget.tolerance_xy_doubleSpinBox.setValue(0.0)  # this should cause warning, it doesn't
        widget.park_widget.tolerance_widget.tolerance_xy_doubleSpinBox.setValue(0.5)
        widget.park_widget.tolerance_widget.tolerance_xy_doubleSpinBox.setValue(1)
        widget.park_widget.tolerance_widget.tolerance_xy_doubleSpinBox.setValue(-1)  # this should cause warning, it doesn't
        widget.park_widget.tolerance_widget.tolerance_xy_doubleSpinBox.setValue(2)

        # Check change waypoint buttons
        QTest.mouseClick(widget.previousWpButton, Qt.LeftButton)
        QTest.mouseClick(widget.nextWpButton, Qt.LeftButton)
        QTest.mouseClick(widget.nextWpButton, Qt.LeftButton)
        QTest.mouseClick(widget.previousWpButton, Qt.LeftButton)

        # Check that mission_track remove_step is called when "delete waypoint" button is pressed
        # temporary store real method
        real_method = MissionTrack.remove_step
        MissionTrack.remove_step = Mock()
        QTest.mouseClick(widget.delete_pushButton, Qt.LeftButton)
        self.assertTrue(MissionTrack.remove_step.called)

        # restore remove_step method
        MissionTrack.remove_step = real_method

        # Changing maneuver type
        widget.heave_mode_widget.on_heave_mode_box_changed(1)
        widget.show_multiple_features(waypoints)

        widget.heave_mode_widget.on_heave_mode_box_changed(2)
        widget.show_multiple_features(waypoints)

        widget.heave_mode_widget.on_heave_mode_box_changed(0)
        widget.show_multiple_features(waypoints)

        # Add action
        Loadaddactiondialog.exec_ = Mock(return_value=QDialog.Accepted)
        QTest.mouseClick(widget.mission_actions_widget.addAction_toolButton, Qt.LeftButton)

        # Remove action
        QTest.mouseClick(widget.mission_actions_widget.removeAction_toolButton, Qt.LeftButton)
        widget.mission_actions_widget.update_remove_button()

def write_temp_mission_xml(mission_name):
    mission = Mission()
    mission_step = MissionStep()
    param = Parameter("abcd")
    param_2 = Parameter("2")
    parameters = []
    parameters.append(param)
    parameters.append(param_2)
    action = MissionAction("action1", "action1", parameters)
    mission_step.add_action(action)
    wp = MissionGoto(41.777, 3.030, 15.0, 0.0,
                     0, 0.5, 2.0, True)
    mission_step.add_maneuver(wp)
    mission.add_step(mission_step)
    mission_step2 = MissionStep()
    sec = MissionSection(41.777, 3.030, 15.0,
                         41.777, 3.030, 15.0, 0.0,
                         0, 0.5, 2.0, True)
    mission_step2.add_maneuver(sec)
    mission.add_step(mission_step2)
    mission_step3 = MissionStep()
    park = MissionPark(41.777, 3.030, 15.0, 0.0,
                       0.5, True,
                       0, 0.5, 120, True)
    mission_step3.add_maneuver(park)
    mission.add_step(mission_step3)
    mission.write_mission(mission_name + '.xml')
