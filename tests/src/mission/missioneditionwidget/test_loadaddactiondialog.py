# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import unittest
from copy import copy

from qgis.core import QgsApplication
from iquaview.src.mission.missionedition.loadaddactiondialog import Loadaddactiondialog
from iquaview_lib.config import Config

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)


class TestLoadAddActionDialog(unittest.TestCase):

    def setUp(self):
        print("TestLoadAddActionDialog.setUp")
        self.app = QgsApplication.instance()
        if self.app is None:
            self.app = QgsApplication([], False)
        self.config = Config()
        self.config.load()

        self.add_action_dlg = Loadaddactiondialog(self.config, [])

    def test(self):
        print("TestLoadAddActionDialog.test")
        checked_actions = self.add_action_dlg.checked_actions()
        self.assertIsInstance(checked_actions, list)


if __name__ == "__main__":
    unittest.main()
