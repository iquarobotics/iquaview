# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

from iquaview_lib.canvasdrawer.canvastrackstyle import AUTOMATIC_MODE, CanvasTrackStyle
from iquaview_lib.canvasdrawer.canvasmarker import CanvasMarker
from qgis.core import QgsPointXY, QgsApplication
from qgis.gui import QgsMapCanvas
from PyQt5.QtGui import QColor
from PyQt5.QtCore import Qt, QPointF
from PyQt5.QtWidgets import QApplication
import os
import sys
import unittest
from copy import copy

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)


class TestCanvasMarker(unittest.TestCase):

    def setUp(self):
        print("TestCanvasMarker.setUp")
        self.app = QgsApplication.instance()
        if self.app is None:
            self.app = QgsApplication([], False)

        self.canvas = QgsMapCanvas()
        self.canvas.setObjectName("canvas")
        config_track_style = CanvasTrackStyle()
        config_track_style.color = QColor(Qt.darkGray).name()
        config_track_style.marker_active = False
        config_track_style.marker_svg = None
        config_track_style.marker_mode = AUTOMATIC_MODE

        self.marker = CanvasMarker(self.canvas, config_track_style)

    def test_types(self):
        print("TestCanvasMarker.test_types")
        self.assertEqual(type(self.marker.get_color()), str)
        self.assertEqual(type(self.marker.get_color()),  type(QColor(Qt.darkGray).name()))
        self.assertEqual(type(self.marker.get_marker_mode()), str)
        self.assertEqual(self.marker.get_marker_mode(), 'auto')

        # if not svg defined, type is None
        self.assertEqual(type(self.marker.get_length()), type(None))
        self.assertEqual(type(self.marker.get_width()), type(None))
        self.assertEqual(type(self.marker.get_size()), int)
        self.assertEqual(type(self.marker.get_center()), QPointF)

        size = 55
        length = 17.0
        width = 29.0
        marker_mode = AUTOMATIC_MODE
        color = QColor(Qt.cyan).name()
        center = QgsPointXY(13.22, 2)

        self.marker.set_size(size)
        self.marker.set_length(length)
        self.marker.set_width(width)
        self.marker.set_color(color)
        self.marker.set_center(center, heading=0.0)

        self.assertEqual(self.marker.get_size(), size)
        self.assertEqual(self.marker.get_length(), length)
        self.assertEqual(self.marker.get_width(), width)
        self.assertEqual(self.marker.get_marker_mode(), marker_mode)
        self.assertEqual(self.marker.get_color(), color)
        self.assertEqual(self.marker.get_center(), self.marker.toCanvasCoordinates(center))

        self.assertEqual(type(self.marker.get_color()), str)
        self.assertEqual(type(self.marker.get_marker_mode()), str)
        self.assertEqual(type(self.marker.get_length()), float)
        self.assertEqual(type(self.marker.get_width()), float)
        self.assertEqual(type(self.marker.get_size()), int)
        self.assertEqual(type(self.marker.get_center()), QPointF)


if __name__ == "__main__":
    unittest.main()
