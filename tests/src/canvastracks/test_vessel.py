# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import time
import unittest
from unittest.mock import Mock

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)

from PyQt5.QtWidgets import QMessageBox

from qgis.core import QgsApplication

from iquaview.src import mainwindow

from iquaview_lib import resources_rc


class TestVessel(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        print("TestVessel.setUpClass")

        cls.app = QgsApplication.instance()
        if cls.app is None:
            cls.app = QgsApplication([], False)

        cls.mainwindow = mainwindow.MainWindow()
        cls.mainwindow.is_test = True

    @classmethod
    def tearDownClass(cls) -> None:
        print("TestVessel.tearDownClass")
        cls.mainwindow.close()
        cls.mainwindow.deleteLater()

    def test_connect(self):
        print("TestVessel.test_connect")
        QMessageBox.critical = Mock()

        self.assertEqual(False, self.mainwindow.vessel.vesselentity.is_connected())
        try:
            self.mainwindow.vessel.vesselentity.connect()
        except Exception as e:
            print("not connected")
        self.mainwindow.vessel.vesselentity.close()
        self.assertEqual(False, self.mainwindow.vessel.vesselentity.is_connected())

    def test_gps_update_canvas(self):
        print("TestVessel.test_gps_update_canvas")
        QMessageBox.critical = Mock()
        try:
            self.mainwindow.vessel.vesselentity.connect()
        except Exception as e:
            print("not connected")
        # mock gps data
        data = get_mock_data()
        self.mainwindow.vessel.vesselentity.driver.get_data = Mock(return_value=data)

        self.mainwindow.vessel.vesselentity.update_canvas()

def get_mock_data():
    data = {}
    data['time'] = time.time()
    data['latitude'] = 41.77770082113855
    data['longitude'] = 3.0332993934595223
    data['heading'] = 15
    data['quality'] = 1
    data['altitude'] = 0
    data['orientation'] = 15
    data['orientation_time'] = time.time()
    data['status'] = "new_data"

    return data


if __name__ == "__main__":
    unittest.main()
