# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import unittest
from copy import copy
from unittest.mock import Mock

from qgis.core import QgsApplication

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)

from iquaview.src.generaloptions import GeneralOptionsWidget
from iquaview_lib.config import Config


class TestGeneralOptions(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        print("TestGeneralOptions.setUpClass")

        cls.app = QgsApplication.instance()
        if cls.app is None:
            cls.app = QgsApplication([], False)
        # sparus2 default
        cls.config = Config()
        cls.config.load()

        cls.options_dialog = GeneralOptionsWidget(cls.config)

    def test_coordinate_combobox(self):
        print("TestGeneralOptions.test_coordinate_combobox")
        self.options_dialog.set_coordinate_configuration()
        # check equal to default value in config
        self.assertEqual(self.options_dialog.coordinate_comboBox.currentIndex(), 0)

        self.config.settings['coordinate_format'] = 'degree_minute'
        self.options_dialog.set_coordinate_configuration()
        self.assertEqual(self.options_dialog.coordinate_comboBox.currentIndex(), 1)

        self.config.settings['coordinate_format'] = 'degree_minute_second'
        self.options_dialog.set_coordinate_configuration()
        self.assertEqual(self.options_dialog.coordinate_comboBox.currentIndex(), 2)

        self.config.settings['coordinate_format'] = 'degree'
        self.options_dialog.set_coordinate_configuration()
        self.assertEqual(self.options_dialog.coordinate_comboBox.currentIndex(), 0)

if __name__ == "__main__":
    unittest.main()
