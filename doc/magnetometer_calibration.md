# Magnetomer calibration

```mermaid
sequenceDiagram
    box Controlstation
        participant view as IQUAview
    end

    box Vehicle
        participant calib as imu_calibration_node
        participant estim as imu_angle_estimator_node
        participant capt as captain_node
        participant contr as controller_node
        participant fs as filesystem
    end

    view->>+calib: enable
    calib->>+capt: enable_external_mission
    capt-->>calib: mission_id
    calib->>+estim: start_magnetometer_calibration

    rect rgba(0, 200, 200, .1)
        loop Calibration mission
            view->>view: listen to captain/feedback
            calib->>calib: advance mission actions
            calib->>contr: publish velocities/setpoint
            calib->>capt: publish feedback
        end
    end

    calib->>+estim: calibrate_magnetometer
    estim->>-calib: res.message = <filename>
    deactivate estim

    calib->>capt: publish SUCCESS + {key: file_name, value: <filename>}
    calib->>capt: disable_external_mission
    deactivate capt
    deactivate calib

    view->>view: receive mission SUCCESS + {key: file_name, value: <filename>}
    fs->>view: copy cola2_<ns>/calibrations/<filename> image to /tmp
    view->>view: show image/plot and ask user for confirmation
    view->>fs: copy cola2_<ns>/calibrations/<filename> to cola2_<ns>/calibrations/calibration_iron.yaml
    fs->>view:  copy cola2_<ns>/calibrations/<filename> yaml to /tmp

    view->>view: load params to param server (hard_iron + soft_iron)
    view->>estim: reload_params
```
