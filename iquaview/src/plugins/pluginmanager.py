# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
    Class to manage multiple plugins
"""
import importlib
import inspect
import logging
import os
import subprocess
import sys
from typing import List, Optional

from PyQt5.QtCore import QThreadPool, Qt, pyqtSignal
from PyQt5.QtWidgets import (QAction,
                             QDialog,
                             QTableWidgetItem,
                             QFileDialog,
                             QMenuBar, QMenu,
                             QMainWindow,
                             QMessageBox,
                             QHeaderView)
from qgis.core import QgsProject
from qgis.gui import QgsMapCanvas

from iquaview.src.plugins.reload_recursive import reload_recursive
from iquaview.src.ui.ui_pluginmanager import Ui_PluginManager
from iquaview_lib.baseclasses.pluginbase import PluginBase
from iquaview_lib.config import Config
from iquaview_lib.entity.entitiesmanager import EntitiesManager
from iquaview_lib.utils.workerthread import Worker
from iquaview_lib.vehicle.vehicledata import VehicleData
from iquaview_lib.vehicle.vehicleinfo import VehicleInfo
from iquaview_lib.vehicle.vehiclestatus import VehicleStatus

if sys.version_info >= (3, 8):
    from importlib import metadata as importlib_metadata
else:
    import importlib_metadata

logger = logging.getLogger(__name__)

PACKAGE_NAME = 0
PACKAGE_VERSION = 1


class PluginManager(QDialog, Ui_PluginManager):
    """ Class to manage multiple plugins """
    append_text_signal = pyqtSignal(str)
    reload_signal = pyqtSignal(bool)
    plugins_loaded_signal = pyqtSignal()

    def __init__(self,
                 proj: QgsProject,
                 canvas: QgsMapCanvas,
                 config: Config,
                 vehicle_info: VehicleInfo,
                 vehicle_data: VehicleData,
                 vehicle_status: VehicleStatus,
                 menubar: QMenuBar,
                 view_menu_toolbar: QMenu,
                 entities_manager: EntitiesManager,
                 parent: Optional[QMainWindow] = None) -> None:
        """
        Constructor
        :param proj: proj is an instance of QgsProject that encapsulates a QGIS project,
                     including sets of map layers and their styles, layouts, annotations, canvases...
        :type proj: QgsProject
        :param canvas: canvas is a class for displaying all GIS data types on a canvas
        :type canvas: QgsMapCanvas
        :param config: config handle the loading/saving of the different IquaView configuration parameters
        :type config: Config
        :param vehicle_info: read and store the xml structure associated to the vehicle_info tag in the AUV config file
        :type vehicle_info: VehicleInfo
        :param vehicle_data: intermediate class between ros and iquaview in order to obtain vehicle data
        :type vehicle_data: VehicleData
        :param vehicle_status: Provides functions to check the vehicle status by decoding the status code (StatusCode)
        :type vehicle_status: VehicleStatus
        :param menubar: A menu bar consists of a list of pull-down menu items.
        :type menubar: QMenuBar
        :param view_menu_toolbar: view menu
        :type view_menu_toolbar: QMenu
        :param parent: The QMainWindow class provides a main application window
        :type parent: QMainWindow
        """
        super().__init__(parent)
        self.setWindowFlags(Qt.CustomizeWindowHint
                            | Qt.Window | Qt.WindowMaximizeButtonHint | Qt.WindowCloseButtonHint)
        self.setupUi(self)
        self.proj = proj
        self.canvas = canvas
        self.config = config
        self.vehicle_info = vehicle_info
        self.vehicle_data = vehicle_data
        self.vehicle_status = vehicle_status
        self.menubar = menubar
        self.view_menu_toolbar = view_menu_toolbar
        self.entities_manager = entities_manager
        self.parent = parent
        self.threadpool = QThreadPool()
        self.plugins_list: List[PluginBase] = []
        self.plugin_manager = menubar.addMenu("Plugins")
        self.plugin_manager_action = QAction("Plugins manager...", self)

        self.plugin_manager_action.triggered.connect(self.open_plugin_manager)
        self.plugin_manager.addActions([self.plugin_manager_action,
                                        self.plugin_manager.addSeparator()])

    def init(self) -> None:
        """
        Separate function to finish initialization, so that mainwindow.plugin_manager exists
        while loadin plugins, so they can access the PluginManager signals.
        """
        self.reload(initializing=True)

        self.add_plugin_toolButton.clicked.connect(self.add_new_plugin)
        self.remove_plugin_toolButton.clicked.connect(self.remove_plugin)
        self.append_text_signal.connect(self.append_text)
        self.reload_signal.connect(self.reload)

        # for every column and row resize to contents
        for i in range(self.tableWidget.columnCount()):
            self.tableWidget.horizontalHeader().setSectionResizeMode(i, QHeaderView.ResizeToContents)

        for i in range(self.tableWidget.rowCount()):
            self.tableWidget.verticalHeader().setSectionResizeMode(i, QHeaderView.ResizeToContents)

        self.tableWidget.horizontalHeader().setStretchLastSection(True)
        self.tableWidget.verticalHeader().setVisible(False)
        self.tableWidget.setSortingEnabled(True)

    def about(self):
        """ Get information about plugins. """
        about_message = ""
        if self.plugins_list:
            about_message = "<p> Plugins versions: </p>"
            about_message += "<ul>"
            for plugin in self.plugins_list:
                about_message += f"<li>{plugin.version()}</li>"
            about_message += "</ul>"

        return about_message

    def get_plugin(self, module_name: str) -> Optional[PluginBase]:
        """Get plugin by name if it exists."""
        for item in self.plugins_list:
            if item.module_name == module_name:
                return item
        return None

    def open_plugin_manager(self):
        """ Show Plugin Manager dialog"""
        self.reload()
        self.show()
        self.raise_()

    def import_plugin(self, name: str, initializing: bool = False):
        """
        Import plugin to iquaview
        :param name: name of the module
        :type name: str
        :param initializing: Determines regardless of whether the function has been called at application initialization
        :type initializing: bool
        """
        reload_recursive(name.replace('-', '_'))
        plugin_module = importlib.import_module(name.replace('-', '_'))

        for _key, cls in inspect.getmembers(plugin_module, inspect.isclass):
            if (issubclass(cls, PluginBase)
                    and cls != PluginBase
                    and not self.is_plugin_in_list(cls)):
                new_plugin = cls(proj=self.proj,
                                 canvas=self.canvas,
                                 config=self.config,
                                 vehicle_info=self.vehicle_info,
                                 vehicle_data=self.vehicle_data,
                                 vehicle_status=self.vehicle_status,
                                 plugin_bar=self.plugin_manager,
                                 view_menu_toolbar=self.view_menu_toolbar,
                                 entities_manager=self.entities_manager,
                                 parent=self.parent)

                self.plugins_list.append(new_plugin)
                if new_plugin.is_module_found():
                    logger.info(new_plugin.version())

                # split version in name and vers
                name, version = new_plugin.version().split(' ')
                self.tableWidget.insertRow(self.tableWidget.rowCount())
                plugin_name_item = QTableWidgetItem()
                plugin_name_item.setFlags(Qt.ItemIsEnabled | Qt.ItemIsSelectable)
                plugin_name_item.setText(name)
                plugin_name_item.setData(Qt.UserRole, new_plugin)
                self.tableWidget.setItem(self.tableWidget.rowCount() - 1, PACKAGE_NAME, plugin_name_item)

                plugin_version_item = QTableWidgetItem()
                plugin_version_item.setFlags(Qt.ItemIsEnabled | Qt.ItemIsSelectable)
                plugin_version_item.setText(version)
                plugin_version_item.setTextAlignment(Qt.AlignCenter)
                self.tableWidget.setItem(self.tableWidget.rowCount() - 1, PACKAGE_VERSION, plugin_version_item)

                # required only for usbl
                if new_plugin.module_name == 'iquaview_evologics_usbl' and not initializing:
                    message = "A new version iquaview_evologics_usbl has been detected. " + \
                              "Some changes will not take effect until the application is restarted. " + \
                              "Do you want to restart Iquaview? " \
                              "\n\nRemember to save your changes to the project before restarting."
                    reply = QMessageBox.question(self, 'Restart Confirmation', message, QMessageBox.Yes,
                                                 QMessageBox.No)

                    if reply == QMessageBox.Yes:
                        logger.info(f"argv was {sys.argv}")
                        logger.info(f"sys.executable was {sys.executable}")
                        logger.info("restart now")

                        os.execv(sys.executable, ['python3'] + sys.argv)

                # for every column and row resize to contents
        for i in range(self.tableWidget.columnCount()):
            self.tableWidget.horizontalHeader().setSectionResizeMode(i, QHeaderView.ResizeToContents)

        for i in range(self.tableWidget.rowCount()):
            self.tableWidget.verticalHeader().setSectionResizeMode(i, QHeaderView.ResizeToContents)

    def reload(self, initializing: bool = False):
        """
        Load plugins to iquaview
        :param initializing: Determines regardless of whether the function has been called at application initialization
        :type initializing: bool
        """
        dists = importlib_metadata.distributions()
        for dist in dists:
            name = str(dist.metadata["Name"])
            if name.startswith("iquaview-") and name != 'iquaview-lib':
                self.import_plugin(name, initializing)

        # if list is empty, disable remove toolButton
        if not self.plugins_list:
            self.remove_plugin_toolButton.setEnabled(False)
        else:
            self.remove_plugin_toolButton.setEnabled(True)

        # emit signal after all plugins are loaded
        self.plugins_loaded_signal.emit()

    def is_plugin_in_list(self, plugin):
        """
        Check if plugin is in plugin list
        :param plugin: plugin to check
        :return: return True if plugin is in plugin list, otherwise return False
        """
        found = False
        for item in self.plugins_list:
            if (type(item) == plugin) or (type(item).__name__ == plugin.__name__):
                found = True
        return found

    def add_new_plugin(self):
        """ Install and add new plugin to iquaview"""
        directory = QFileDialog.getExistingDirectory(self, "Select plugin directory")

        if directory:
            cmd = f"cd {directory} && pip3 install ."
            response = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            worker = Worker(lambda: self.worker_result(response))
            self.threadpool.start(worker)

    def worker_result(self, result):
        """
        Add result to plaintextEdit
        :param result: result of Worker
        :type result: Union[Popen[bytes], Popen]
        """
        log = []
        while result.stdout is not None:
            line = result.stdout.readline().decode("utf-8")
            output = line.rstrip('\r\n')
            log.append(line)
            self.append_text_signal.emit(output)
            if not line:
                result.stdout.flush()
                break

        logger.info(''.join(log))
        self.reload_signal.emit(False)

    def append_text(self, text):
        """
        Append text to plaintextEdit
        :param text: text to append
        :type text: str
        """
        self.plainTextEdit.appendPlainText(text)

    def remove_plugin(self):
        """ Uninstall and remove plugin from iquaview"""
        current_row = self.tableWidget.currentRow()
        if current_row is not None:
            current_name = self.tableWidget.item(current_row, PACKAGE_NAME).text()
            current_data: PluginBase = self.tableWidget.item(current_row, PACKAGE_NAME).data(Qt.UserRole)
            confirmation_msg = f"Are you sure you want to remove {current_name} plugin?"

            reply = QMessageBox.question(self, 'Removal confirmation',
                                         confirmation_msg, QMessageBox.Yes | QMessageBox.No,
                                         defaultButton=QMessageBox.Yes)
            if reply == QMessageBox.Yes:
                plugin = current_data
                plugin.disconnect_module()
                self.plugins_list.remove(plugin)
                self.tableWidget.removeRow(current_row)
                plugin.remove()
                cmd = f"pip3 uninstall -y {plugin.module_name}"
                response = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                worker = Worker(lambda: self.worker_result(response))
                self.threadpool.start(worker)

                # required only for usbl
                if plugin.module_name == 'iquaview_evologics_usbl':
                    message = "Module iquaview_evologics_usbl removed. Some IQUAview tools may not be available. " + \
                              "Do you want to restart Iquaview? " \
                              "\n\nRemember to save your changes to the project before restarting."
                    reply = QMessageBox.question(self, 'Restart Confirmation', message, QMessageBox.Yes, QMessageBox.No)

                    if reply == QMessageBox.Yes:
                        logger.info(f"argv was {sys.argv}")
                        logger.info(f"sys.executable was {sys.executable}")
                        logger.info("restart now")

                        os.execv(sys.executable, ['python3'] + sys.argv)

    def disconnect_plugins(self):
        """ Disconnect plugins"""
        for item in self.plugins_list:
            if item.is_module_found():
                item.disconnect_module()

        self.threadpool.clear()
