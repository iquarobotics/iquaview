# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Tree model for the track tree view
"""

from PyQt5.QtCore import Qt, QMimeData, pyqtSignal, QModelIndex
from PyQt5.QtGui import QStandardItemModel, QIcon, QStandardItem
from PyQt5.QtWidgets import QTreeView

from iquaview.src.tracks.tracktreeitem import TrackTreeItem, Item
from iquaview_lib.baseclasses.entitybase import EntityBase
from iquaview_lib.entity.entitiesmanager import EntitiesManager

MIME_TYPE_TEXT = "text/plain"
MIME_TYPE_EXPANDED = "expanded"


class TrackTreeModel(QStandardItemModel):
    """ Tree model for the track tree view """
    expand_entity_signal = pyqtSignal(QModelIndex, bool)

    def __init__(self, **kwargs):
        super().__init__()
        self.entities_manager: EntitiesManager = kwargs['entities_manager']
        self.tree_view: QTreeView = kwargs['tree_view']

    def mimeTypes(self):
        """ Return the mime types supported by the model """
        return [MIME_TYPE_TEXT, MIME_TYPE_EXPANDED]

    def mimeData(self, indexes):
        """
        Override the mimeData method to return the mime data for the given indexes.
        :param indexes: indexes of the items to be dragged
        :type indexes: list of QModelIndex
        :return: mime data
        :rtype: QMimeData
        """
        if isinstance(self.itemFromIndex(indexes[0]), TrackTreeItem):
            mimedata = QMimeData()
            entity = indexes[0].data(Item.ENTITY)
            mimedata.setData(MIME_TYPE_TEXT, str(entity.position).encode())
            mimedata.setData(MIME_TYPE_EXPANDED, str(self.tree_view.isExpanded(indexes[0])).encode())
            return mimedata

        return QMimeData()

    def dropMimeData(self, data, action, row, column, parent):
        """
        Override the dropMimeData method to handle the drop of a mime data.
        :param data: mime data
        :type data: QMimeData
        :param action: action to be performed
        :type action: Qt.DropAction
        :param row:  row where the drop should be performed
        :type row: int
        :param column: column where the drop should be performed
        :type column: int
        :param parent: parent of the drop
        :type parent: QModelIndex
        :return: True if the drop was successful, False otherwise
        :rtype: bool
        """
        del action, column, parent
        position = int(bytes(data.data(MIME_TYPE_TEXT)).decode())
        entity = self.entities_manager.entities_dict[position]
        if row == -1:
            row = self.rowCount()
        expanded = bytes(data.data(MIME_TYPE_EXPANDED)).decode() == 'True'
        self.add_entity(entity, row, expanded)

        return True

    def add_entity(self, entity: EntityBase, row: int, expanded: bool = True, loading_entities: bool = False) -> None:
        """
        Add an entity to the model.
        :param entity: entity to be added
        :type entity: EntityBase
        :param row: row where the entity should be added
        :type row: int
        :param expanded: True if the entity should be expanded, False otherwise
        :type expanded: bool
        :param loading_entities: True if the entities are being loaded, False otherwise
        :type loading_entities: bool
        :return: None
        """
        parent1 = TrackTreeItem(QIcon(":/resources/default_marker.svg"), entity.name)
        parent1.setCheckable(True)
        parent1.setDropEnabled(False)
        parent1.setData(entity, Item.ENTITY)
        visible = Qt.Checked if entity.visible else Qt.Unchecked
        parent1.setCheckState(visible)

        self.insert_item(parent1, row, loading_entities)
        childs = []
        # information
        for information in entity.information_fields():
            child = QStandardItem()
            child.setData(information, Item.INFORMATION)
            child.setDragEnabled(False)
            child.setDropEnabled(False)
            child.setCheckable(False)
            child.setEditable(False)
            childs.append(child)

        parent1.appendRows(childs)
        self.expand_entity_signal.emit(parent1.index(), expanded)

    def insert_item(self, item: TrackTreeItem, row: int, loading_entities: bool = False):
        """
        Insert an item in the model.
        If loading_entities is True, get previous entities order to insert at the same position.
        :param item: item to be inserted
        :type item: TrackTreeItem
        :param row: row where the item should be inserted
        :type row: int
        :param loading_entities: True if the entities are being loaded, False otherwise
        :type loading_entities: bool
        :return: None
        """
        if loading_entities:
            if self.rowCount() == 0:
                # if the model is empty, append the item
                self.appendRow(item)
            else:
                # if the model is not empty, insert the item
                inserted = False
                for i in range(self.rowCount()):
                    # get position of the entity at the current row
                    previous_entities_order = self.entities_manager.get_previous_entities_order()
                    # get name
                    item_list_name = self.item(i).data(Item.ENTITY).name
                    item_name = item.data(Item.ENTITY).name
                    # get current pos
                    default_position = self.item(i).data(Item.ENTITY).position
                    default_item_position = item.data(Item.ENTITY).position
                    # get pos from previous order
                    position = previous_entities_order.get(item_list_name, default_position)
                    entity_idx = previous_entities_order.get(item_name, default_item_position)

                    # if entity.position of the current item is lower than the previous one, insert the item
                    if entity_idx <= position:
                        self.insertRow(i, item)
                        inserted = True
                        break
                if not inserted:
                    self.appendRow(item)
        else:
            # if not loading entities, insert the item at the given row
            self.insertRow(row, item)
