# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.


"""
    This file contains the TrackTreeWidget class, which is the widget that contains the tracks
"""
import logging

from PyQt5.QtCore import QModelIndex, Qt, QPoint, QSize, QItemSelection
from PyQt5.QtGui import QIcon, QPainter, QPixmap
from PyQt5.QtWidgets import (
    QAbstractItemView,
    QAction,
    QMenu,
    QStyle,
    QStyledItemDelegate,
    QStyleOptionViewItem,
    QToolBar,
    QWidget,
)

from iquaview.src.tracks.tracktreeitem import TrackTreeItem, Item
from iquaview.src.tracks.tracktreemodel import TrackTreeModel
from iquaview.src.ui.ui_track_treewidget import Ui_TrackTreeWidget
from iquaview_lib.baseclasses.entitybase import EntityBase, LabelData
from iquaview_lib.entity.entitiesmanager import EntitiesManager

logger = logging.getLogger(__name__)


class StyledItemDelegate(QStyledItemDelegate):
    """Custom delegate for the track tree widget"""

    def initStyleOption(self, option, index):
        """
        Initialize the style option with the data from the model and the index
        :param option: option to be initialized
        :type option: QStyleOptionViewItem
        :param index: index of the item
        :type index: QModelIndex
        """
        super().initStyleOption(option, index)
        if isinstance(self.parent().model.itemFromIndex(index), TrackTreeItem):
            entity = index.data(Item.ENTITY)
            option.icon = QIcon(entity.canvasdrawer.generate_icon(margin=QSize(1, 1), size=QSize(20, 30)))
            pixmap: QPixmap = entity.canvasdrawer.generate_icon(margin=QSize(10, 10), size=QSize(200, 300))

    def paint(self, painter: QPainter, option: QStyleOptionViewItem, index: QModelIndex) -> None:
        """
        Paint the item
        :param painter: painter to be used
        :type painter: QPainter
        :param option: option to be used
        :type option: QStyleOptionViewItem
        :param index: index of the item
        :type index: QModelIndex
        :return: None
        """
        painter.eraseRect(option.rect)
        super().paint(painter, option, index)
        if type(self.parent().model.itemFromIndex(index)) == TrackTreeItem:
            entity: EntityBase = index.data(Item.ENTITY)
            if entity.is_connected() and entity.warnings():
                icon = QIcon(":/resources/connection_orange.svg")
            elif entity.is_connected():
                icon = QIcon(":/resources/connection_green.svg")
            else:
                icon = QIcon(":/resources/connection_red.svg")

            icon.paint(painter, option.rect, Qt.AlignRight)
            if entity.is_locked():
                icon = QIcon(":/resources/mActionZoomToAreaLocked.svg")
                painter.save()
                option.rect.setWidth(option.rect.width() - 20)
                icon.paint(painter, option.rect, Qt.AlignRight)
                painter.restore()
        else:
            item = self.parent().model.itemFromIndex(index)
            while item.parent() is not None:
                item = item.parent()
            info = index.data(Item.INFORMATION)
            label_data: LabelData = item.data(Item.ENTITY).get_information(info[0])
            text = [label_data.label_name, label_data.text]
            painter.save()
            default_pen = painter.pen()
            self.initStyleOption(option, index)
            option.text = ""
            style = option.widget.style()
            style.drawControl(QStyle.CE_ItemViewItem, option, painter, option.widget)
            offset = 3
            ellipsis = "..."
            ellipsis_width = painter.fontMetrics().width(ellipsis)
            right_border = option.rect.left() + option.rect.width() - offset

            overflow = False
            option.rect.moveRight(option.rect.right() + offset)
            for idx, field in enumerate(text):
                if (idx + 1) % 2 == 0:
                    painter.setPen(label_data.color)
                else:
                    painter.setPen(default_pen)
                part_width = painter.fontMetrics().width(field)
                draw_rect = option.rect
                if (draw_rect.left() + part_width + ellipsis_width) > right_border:
                    draw_rect.setWidth(right_border - draw_rect.left() - ellipsis_width)
                    overflow = True

                style.drawItemText(painter, draw_rect, option.displayAlignment, option.palette, True, field)

                if overflow:
                    draw_rect.setLeft(right_border - ellipsis_width)
                    draw_rect.setWidth(ellipsis_width)
                    style.drawItemText(painter, draw_rect, option.displayAlignment, option.palette, True, ellipsis)

                option.rect.moveRight(option.rect.right() + part_width)
            painter.restore()


class TrackTreeWidget(QWidget, Ui_TrackTreeWidget):
    """TrackTreeWidget class"""

    def __init__(self, entities_manager: EntitiesManager):
        super().__init__()
        self.setupUi(self)
        self.entities_manager = entities_manager

        kwargs = {"entities_manager": self.entities_manager, "tree_view": self.treeView}
        self.model = TrackTreeModel(**kwargs)
        self.model.setHorizontalHeaderLabels(["Name"])
        self.model.itemChanged.connect(self.update_entity)
        self.model.rowsInserted.connect(self.update_positions)
        self.model.rowsRemoved.connect(self.update_positions)
        self.model.expand_entity_signal.connect(self.expand_entity)
        self.treeView.setHeaderHidden(True)
        self.treeView.setModel(self.model)
        self.treeView.setUniformRowHeights(True)
        self.treeView.setDragDropMode(QAbstractItemView.InternalMove)
        self.treeView.setContextMenuPolicy(Qt.CustomContextMenu)
        self.treeView.setItemDelegate(StyledItemDelegate(self))
        self.treeView.customContextMenuRequested.connect(self.show_context_menu)
        self.treeView.selectionModel().selectionChanged.connect(self.enable_icons)

        self.entities_manager.entity_added_signal.connect(self.add_entity)
        self.entities_manager.entity_added_signal.connect(self.update_tooltip)
        self.entities_manager.entity_removed_signal.connect(self.remove_entity)
        self.entities_manager.entity_updated_signal.connect(self.treeView.model().layoutChanged.emit)
        self.entities_manager.entity_updated_signal.connect(self.update_tooltip)
        self.entities_manager.entity_updated_signal.connect(self.update_icons)

        self.new_entity_action = QAction(QIcon(":/resources/mActionAdd.svg"), "New Entity...", self)
        self.connect_action = QAction(QIcon(":/resources/link_icon.svg"), "Connect", self)
        self.zoom_action = QAction(QIcon(":/resources/mActionZoomToArea.svg"), "Center and zoom to map", self)
        self.lock_action = QAction(QIcon(":/resources/mActionZoomToAreaUnlocked.svg"), "Lock Center", self)
        self.export_track_action = QAction(QIcon(":/resources/export_file.svg"), "Export Track", self)
        self.clear_track_action = QAction(QIcon(":/resources/eraser.svg"), "Clear Track", self)
        self.properties_action = QAction(QIcon(":/resources/mActionOptions.svg"), "Properties", self)
        self.remove_action = QAction(QIcon(":/resources/mActionDeleteSelected.svg"), "Remove Entity", self)

        self.connect_action.setCheckable(True)
        self.lock_action.setCheckable(True)

        self.tracks_toolbar = QToolBar("Tracks")
        self.tracks_toolbar.setIconSize(QSize(16, 16))
        self.tracks_toolbar.setObjectName("Tracks")
        self.tracks_toolbar.addAction(self.new_entity_action)
        self.tracks_toolbar.addAction(self.remove_action)
        self.tracks_toolbar.addSeparator()
        self.tracks_toolbar.addAction(self.connect_action)
        self.tracks_toolbar.addSeparator()
        self.tracks_toolbar.addAction(self.zoom_action)
        self.tracks_toolbar.addAction(self.lock_action)
        self.tracks_toolbar.addSeparator()
        self.tracks_toolbar.addAction(self.export_track_action)
        self.tracks_toolbar.addAction(self.clear_track_action)
        self.tracks_toolbar.addSeparator()
        self.tracks_toolbar.addAction(self.properties_action)

        self.options_widget.layout().addWidget(self.tracks_toolbar)

        self.new_entity_action.triggered.connect(self.entities_manager.new_entity)
        self.connect_action.triggered.connect(
            lambda: self.entities_manager.connect_entity(
                self.get_base_parent(self.treeView.currentIndex()).data(Item.ENTITY)
            )
        )
        self.connect_action.triggered.connect(self.update_icons)
        self.zoom_action.triggered.connect(
            lambda: self.entities_manager.zoom_track(
                self.get_base_parent(self.treeView.currentIndex()).data(Item.ENTITY)
            )
        )
        self.lock_action.triggered.connect(
            lambda: self.entities_manager.lock_track(
                self.get_base_parent(self.treeView.currentIndex()).data(Item.ENTITY)
            )
        )
        self.lock_action.triggered.connect(self.update_icons)
        self.export_track_action.triggered.connect(
            lambda: self.entities_manager.save_track(
                self.get_base_parent(self.treeView.currentIndex()).data(Item.ENTITY)
            )
        )
        self.clear_track_action.triggered.connect(
            lambda: self.entities_manager.clear_track(
                self.get_base_parent(self.treeView.currentIndex()).data(Item.ENTITY)
            )
        )
        self.properties_action.triggered.connect(
            lambda: self.entities_manager.edit_entity(
                self.get_base_parent(self.treeView.currentIndex()).data(Item.ENTITY)
            )
        )
        self.remove_action.triggered.connect(
            lambda: self.entities_manager.ask_and_remove_entity(
                self.get_base_parent(self.treeView.currentIndex()).data(Item.ENTITY)
            )
        )

        self.enable_icons(None, None)

    def enable_icons(self, selected: QItemSelection, desselected: QItemSelection) -> None:
        """
        Enable or disable icons in the toolbar.
        :param selected: The selected item.
        :type selected: QItemSelection
        :param desselected: The deselected item.
        :type desselected: QItemSelection
        :return: None
        """
        permanent = False
        indexes = selected.indexes() if selected is not None else None
        self.connect_action.setDisabled(not indexes)
        self.zoom_action.setDisabled(not indexes)
        self.lock_action.setDisabled(not indexes)
        self.export_track_action.setDisabled(not indexes)
        self.clear_track_action.setDisabled(not indexes)
        self.properties_action.setDisabled(not indexes)
        if indexes is not None:
            for i in range(len(indexes)):
                item = self.model.itemFromIndex(indexes[i])
                while item.parent() is not None:
                    item = item.parent()
                entity: EntityBase = item.data(Item.ENTITY)
                if entity.permanent is not None:
                    permanent = True
                    break
        self.remove_action.setDisabled(not indexes or permanent)
        if indexes:
            self.update_icons()
        else:
            self.connect_action.setChecked(False)

    def get_base_parent(self, index: QModelIndex):
        """
        Return base parent item
        :param index: current index
        :type index: QModelIndex
        :return: parent of the current index item
        :return: TrackTreeItem
        """
        item = self.model.itemFromIndex(index)
        if item:
            while item.parent() is not None:
                item = item.parent()
        return item

    def update_icons(self) -> None:
        index = self.treeView.currentIndex()
        item = self.get_base_parent(index)
        if item is not None:
            entity: EntityBase = item.data(Item.ENTITY)
            self.connect_action.setChecked(entity.is_connected())
            self.zoom_action.setDisabled(entity.canvasdrawer.hidden)
            self.lock_action.setDisabled(entity.canvasdrawer.hidden)
            self.export_track_action.setDisabled(entity.canvasdrawer.hidden or not entity.canvasdrawer.can_export())
            self.clear_track_action.setDisabled(entity.canvasdrawer.hidden)
            if not entity.is_connected():
                self.connect_action.setText("Connect")
            else:
                self.connect_action.setText("Disconnect")

            self.lock_action.setChecked(entity.is_locked())
            if entity.is_locked():
                self.lock_action.setIcon(QIcon(":/resources/mActionZoomToAreaUnlocked.svg"))
                self.lock_action.setText("Unlock Center")

            else:
                self.lock_action.setIcon(QIcon(":/resources/mActionZoomToAreaLocked.svg"))
                self.lock_action.setText("Lock Center")

    def show_context_menu(self, pos: QPoint) -> None:
        """
        Only callable by signals of TreeView. Creates a context menu for the elements selected in the tableView
        :param pos: position to create context menu from the tableView perspective
        :type pos: QPoint

        """
        menu = QMenu()
        index = self.treeView.indexAt(pos)

        if index.isValid():
            menu.addAction(self.connect_action)
            menu.addSeparator()
            menu.addAction(self.zoom_action)
            menu.addAction(self.lock_action)
            menu.addSeparator()
            menu.addAction(self.export_track_action)
            menu.addAction(self.clear_track_action)
            menu.addSeparator()
            menu.addAction(self.properties_action)
            menu.addSeparator()
            menu.addAction(self.remove_action)
        else:
            menu.addAction(self.new_entity_action)

        action = menu.exec_(self.treeView.mapToGlobal(pos))

    def add_entity(self, entity: EntityBase, loading_entities: bool = False) -> None:
        """
        Add a new entity to the treeView
        :param entity: entity to add
        :param loading_entities: True if the entities are being loaded, False otherwise
        :return: None
        """
        i = self.model.rowCount(QModelIndex())
        if loading_entities:
            names = self.entities_manager.get_previous_entities_order()
            for name, idx in names.items():
                if name == entity.name:
                    i = idx
                    break
        self.model.add_entity(entity, i, loading_entities=loading_entities)

    def update_entity(self, item: TrackTreeItem) -> None:
        """
        Update the entity in the treeView
        :param item: item to update
        :type item: TrackTreeItem
        """
        while item.parent() is not None:
            item = item.parent()
        renamed = False
        entity: EntityBase = item.data(Item.ENTITY)

        visibility_changed = (item.checkState() == Qt.Checked) != entity.visible
        entity.visible = item.checkState() == Qt.Checked
        if entity.visible:
            entity.show()
        else:
            entity.hide()

        if item.text() != "" and item.text() != entity.name:
            # apply name change
            entity.name = item.text()
            entity.on_name_changed()
            renamed = True
        else:
            # undo name change
            item.setText(entity.name)

        if renamed or visibility_changed:
            entity.save_basic_changes()

    def update_tooltip(self, entity: EntityBase) -> None:
        """
        Updates the tooltip of the entity
        :param entity: entity to update the tooltip
        :type entity: EntityBase
        """
        warnings = entity.warnings()
        if entity.is_connected() and warnings:
            connection_text = ""
            for i in range(len(warnings)):
                connection_text += str(warnings[i])
                if i != len(warnings) - 1:
                    connection_text += "\n"
        elif entity.is_connected():
            connection_text = "Connected"
        else:
            connection_text = "Disconnected"

        for i in range(0, self.model.rowCount(QModelIndex())):
            item_entity = self.model.item(i, 0).data(Item.ENTITY)
            if entity == item_entity:
                if entity.permanent is None:
                    tooltip = f"<b>{entity.name}</b><br> {connection_text}"
                else:
                    tooltip = f"<b>{entity.name}</b> ({entity.permanent})<br> {connection_text}"
                self.model.item(i, 0).setToolTip(tooltip)
                break

    def update_positions(self) -> None:
        """
        Update the positions of the entities.
        """
        if not self.entities_manager.loading_entities:
            self.entities_manager.config.settings["input_tracks"] = {}
            self.entities_manager.entities_dict = {}
            for i in range(self.model.rowCount()):
                item_entity: EntityBase = self.model.item(i, 0).data(Item.ENTITY)
                item_entity.position = i
                self.entities_manager.entities_dict[i] = item_entity

                item_entity.save_basic_changes()

    def expand_entity(self, index: QModelIndex, expand: bool):
        """
        Expands the model item specified by the index.
        :param index: index to expand
        :type index: QModelIndex
        :param expand: True to expand, False to collapse
        :type expand: bool
        """
        self.treeView.setExpanded(index, expand)

    def remove_entity(self, entity: EntityBase) -> None:
        """
        Removes the entity from the model.
        :param entity: entity to remove
        :type entity: EntityBase
        """
        for i in range(0, self.model.rowCount(QModelIndex())):
            item_entity = self.model.item(i, 0).data(Item.ENTITY)
            if entity == item_entity:
                self.model.removeRow(i)
                break
