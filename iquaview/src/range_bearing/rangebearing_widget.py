# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.


"""
    This file contains the RangeBearingWidget class
"""
import logging
from math import degrees

from PyQt5.QtCore import Qt, QSortFilterProxyModel, QModelIndex, QTimer
from PyQt5.QtGui import QStandardItemModel, QStandardItem, QCloseEvent
from PyQt5.QtWidgets import QWidget, QAction
from cola2.utils.angles import wrap_angle
from qgis.core import (QgsDistanceArea,
                       QgsProject,
                       QgsUnitTypes)

from iquaview.src.ui.ui_range_bearing import Ui_RangeBearing
from iquaview_lib.baseclasses.entitybase import EntityBase
from iquaview_lib.entity.entitiesmanager import EntitiesManager
from iquaview_lib.canvasdrawer.canvastrack import CanvasTrack

logger = logging.getLogger(__name__)

VISIBILITY_COLUMN = Qt.UserRole
SORT_COLUMN = Qt.UserRole + 1
IDENTIFIER_COLUMN = Qt.UserRole + 2
ENTITY_COLUMN = Qt.UserRole + 3


class ProxyModel(QSortFilterProxyModel):

    def filterAcceptsRow(self, source_row, source_parent):
        if self.sourceModel():
            index = self.sourceModel().index(source_row, 0, source_parent)
            return self.sourceModel().data(index, VISIBILITY_COLUMN)
        return True

    def lessThan(self, left: QModelIndex, right: QModelIndex) -> bool:
        data_left = self.sourceModel().data(left, SORT_COLUMN)
        data_right = self.sourceModel().data(right, SORT_COLUMN)
        if not data_left or not data_right:
            return super().lessThan(left, right)
        return str(data_left).lower() < str(data_right).lower()


class RangeBearingWidget(QWidget, Ui_RangeBearing):
    """ TrackTreeWidget class """

    def __init__(self, canvas, entities_manager: EntitiesManager):
        super().__init__()
        self.setupUi(self)
        self.canvas = canvas
        self.entities_manager = entities_manager
        self.previous_index = 0
        entities_manager.entity_added_signal.connect(self.entity_added)
        entities_manager.entity_updated_signal[EntityBase].connect(self.update_entity)
        entities_manager.entity_removed_signal.connect(self.entity_removed)

        self.entities = {}
        self.tree_items = {}
        self.cb_items = {}

        self.comboBox.currentIndexChanged.connect(self.combo_box_index_changed)
        self.cb_model = QStandardItemModel(0, 1, self)
        self.cb_proxy_model = ProxyModel()
        self.cb_proxy_model.setSourceModel(self.cb_model)
        self.comboBox.setModel(self.cb_proxy_model)

        self.tv_model = QStandardItemModel(0, 2, self)
        self.tv_proxy_model = ProxyModel()
        self.tv_proxy_model.setSourceModel(self.tv_model)
        self.treeView.setModel(self.tv_proxy_model)

        self.resize_columns_action = QAction("Fit width", self)
        self.resize_columns_action.triggered.connect(self.resize_tree_columns)
        self.expand_all_action = QAction("Expand all", self)
        self.expand_all_action.triggered.connect(self.treeView.expandAll)
        self.treeView.addActions([self.resize_columns_action, self.expand_all_action])
        self.treeView.setContextMenuPolicy(Qt.ActionsContextMenu)

        self.distance_calc = QgsDistanceArea()
        self.proj_changed()
        self.canvas.destinationCrsChanged.connect(self.proj_changed)

        self.timer = QTimer()
        self.timer.timeout.connect(self.update_view)
        if not self.timer.isActive():
            self.timer.start(1000)

    def proj_changed(self):
        """
        Projection changed
        """
        crs = self.canvas.mapSettings().destinationCrs()
        ellipsoid = QgsProject.instance().ellipsoid()
        if self.distance_calc.sourceCrs() != crs:
            self.distance_calc.setSourceCrs(crs, QgsProject.instance().transformContext())
        if self.distance_calc.ellipsoid() != ellipsoid:
            self.distance_calc.setEllipsoid(ellipsoid)

    def entity_added(self, entity: EntityBase):
        # Check entity is track
        if not issubclass(type(entity.canvasdrawer), CanvasTrack):
            return

        # Append to entities list
        self.entities[entity.identifier] = entity

        # Create tree view items
        visible = self.comboBox.currentData(IDENTIFIER_COLUMN) != entity.identifier and not entity.canvasdrawer.hidden
        item = QStandardItem(entity.name)
        item.setCheckable(False)
        item.setSelectable(False)
        item.setData(visible, VISIBILITY_COLUMN)
        item.setData(entity.name, SORT_COLUMN)
        item.setData(entity.identifier, IDENTIFIER_COLUMN)
        item.setData(entity, ENTITY_COLUMN)
        self.tree_items[entity.identifier] = item
        range_label_item = QStandardItem("Range")
        range_label_item.setData(True, VISIBILITY_COLUMN)
        range_label_item.setData(1, SORT_COLUMN)
        range_item = QStandardItem("-")
        range_item.setData(True, VISIBILITY_COLUMN)
        item.appendRow([range_label_item, range_item])
        bearing_label_item = QStandardItem("Bearing")
        bearing_label_item.setData(True, VISIBILITY_COLUMN)
        bearing_label_item.setData(1, SORT_COLUMN)
        bearing_item = QStandardItem("-")
        bearing_item.setData(True, VISIBILITY_COLUMN)
        item.appendRow([bearing_label_item, bearing_item])
        relative_bearing_label_item = QStandardItem("Rel Bearing")
        relative_bearing_label_item.setData(True, VISIBILITY_COLUMN)
        relative_bearing_label_item.setData(2, SORT_COLUMN)
        relative_bearing_item = QStandardItem("-")
        relative_bearing_item.setData(True, VISIBILITY_COLUMN)
        item.appendRow([relative_bearing_label_item, relative_bearing_item])
        self.tv_model.appendRow(item)
        self.treeView.sortByColumn(0, Qt.AscendingOrder)
        self.treeView.expand(self.tv_proxy_model.mapFromSource(self.tv_model.indexFromItem(item)))
        self.resize_tree_columns()

        # Add to combo box
        cb_item = QStandardItem(entity.name)
        cb_item.setData(entity.identifier, IDENTIFIER_COLUMN)
        cb_item.setData(entity.name, SORT_COLUMN)
        cb_item.setData(True, VISIBILITY_COLUMN)
        self.cb_model.appendRow(cb_item)
        self.comboBox.model().sort(0, Qt.AscendingOrder)
        self.cb_items[entity.identifier] = cb_item

    def entity_removed(self, entity: EntityBase):
        self.cb_model.removeRow(self.cb_model.indexFromItem(self.cb_items[entity.identifier]).row())
        self.tv_model.removeRow(self.tv_model.indexFromItem(self.tree_items[entity.identifier]).row())
        del self.entities[entity.identifier]
        del self.tree_items[entity.identifier]
        del self.cb_items[entity.identifier]

    def update_entity(self, entity: EntityBase) -> None:

        if entity.identifier not in self.tree_items:
            return
        self.tree_items[entity.identifier].setText(entity.name)
        self.tree_items[entity.identifier].setData(entity.name, SORT_COLUMN)
        self.cb_items[entity.identifier].setText(entity.name)
        self.cb_items[entity.identifier].setData(entity.name, SORT_COLUMN)

    def update_view(self):
        """
        Update tree view items
        :return:
        """
        if len(self.entities) == 0:
          return
        base_entity: EntityBase = self.entities[self.comboBox.currentData(IDENTIFIER_COLUMN)]
        base_point = base_entity.canvasdrawer.get_center()
        self.comboBox.model().sort(0, Qt.AscendingOrder)
        self.proj_changed()

        for key, item in self.tree_items.items():
            entity: EntityBase = self.entities[item.data(IDENTIFIER_COLUMN)]
            entity_point = entity.canvasdrawer.get_center()
            visible = (item.data(IDENTIFIER_COLUMN) != self.comboBox.currentData(IDENTIFIER_COLUMN)
                       and not entity.canvasdrawer.hidden)
            item.setData(visible, VISIBILITY_COLUMN)
            if item.data(IDENTIFIER_COLUMN) == self.comboBox.itemData(self.previous_index, IDENTIFIER_COLUMN):
                self.treeView.expandAll()
            if (entity_point.x() == 0 or base_point.x() == 0
                    or base_entity.canvasdrawer.hidden or entity.canvasdrawer.hidden):
                item.child(0, 1).setText("-")
                item.child(1, 1).setText("-")
                item.child(2, 1).setText("-")
            else:
                distance = self.distance_calc.measureLine(base_point, entity_point)
                text = self.distance_calc.formatDistance(distance, 3, QgsUnitTypes.DistanceMeters, False)
                bearing = self.distance_calc.bearing(base_point, entity_point)
                if bearing == float('nan'):
                    item.child(0,1).setText("-")
                    item.child(1,1).setText("-")
                    item.child(2,1).setText("-")
                else:
                    item.child(0, 1).setText(text)
                    item.child(1, 1).setText(f"{((degrees(wrap_angle(bearing)) + 360) % 360):.3F} °")
                    item.child(2, 1).setText(
                        f"{degrees(wrap_angle(wrap_angle(bearing) - wrap_angle(base_entity.canvasdrawer.heading))):.3F} °")
        self.resize_tree_columns()
        self.treeView.sortByColumn(0, Qt.AscendingOrder)

    def combo_box_index_changed(self, index):
        current_identifier = self.comboBox.currentData(IDENTIFIER_COLUMN)
        previous_identifier = self.comboBox.itemData(self.previous_index, IDENTIFIER_COLUMN)
        for i in range(len(self.entities)):
            entity = self.tv_model.item(i).data(ENTITY_COLUMN)
            visible = (self.tv_model.item(i).data(IDENTIFIER_COLUMN) != current_identifier
                       and not entity.canvasdrawer.hidden)
            self.tv_model.item(i).setData(visible, VISIBILITY_COLUMN)
            if self.tv_model.item(i).data(IDENTIFIER_COLUMN) == previous_identifier:
                self.treeView.expandAll()

        self.previous_index = index
        self.resize_tree_columns()

    def resize_tree_columns(self):
        self.treeView.resizeColumnToContents(0)
        self.treeView.resizeColumnToContents(1)

    def close(self) -> bool:
        self.timer.stop()
        super().close()
