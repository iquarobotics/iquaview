# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_mapping_mission.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MappingMissionWidget(object):
    def setupUi(self, MappingMissionWidget):
        MappingMissionWidget.setObjectName("MappingMissionWidget")
        MappingMissionWidget.resize(283, 288)
        self.gridLayout = QtWidgets.QGridLayout(MappingMissionWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setSpacing(0)
        self.gridLayout.setObjectName("gridLayout")
        self.mapping_groupBox = QtWidgets.QGroupBox(MappingMissionWidget)
        self.mapping_groupBox.setStyleSheet("QGroupBox {\n"
"    border: 1px solid silver;\n"
"    border-radius: 6px;\n"
"    margin-top: 6px;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 7px;\n"
"    padding: 0px 5px 0px 5px;\n"
"}")
        self.mapping_groupBox.setCheckable(False)
        self.mapping_groupBox.setChecked(False)
        self.mapping_groupBox.setObjectName("mapping_groupBox")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.mapping_groupBox)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.distance_covered_turn_label = QtWidgets.QLabel(self.mapping_groupBox)
        self.distance_covered_turn_label.setWordWrap(True)
        self.distance_covered_turn_label.setObjectName("distance_covered_turn_label")
        self.gridLayout_2.addWidget(self.distance_covered_turn_label, 0, 0, 1, 3)
        self.distance_covered_to_turn_doubleSpinBox = QtWidgets.QDoubleSpinBox(self.mapping_groupBox)
        self.distance_covered_to_turn_doubleSpinBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedKingdom))
        self.distance_covered_to_turn_doubleSpinBox.setMaximum(100000.0)
        self.distance_covered_to_turn_doubleSpinBox.setProperty("value", 4.0)
        self.distance_covered_to_turn_doubleSpinBox.setObjectName("distance_covered_to_turn_doubleSpinBox")
        self.gridLayout_2.addWidget(self.distance_covered_to_turn_doubleSpinBox, 0, 3, 1, 1)
        self.ending_actions_widget = MissionActionsWidget(self.mapping_groupBox)
        self.ending_actions_widget.setObjectName("ending_actions_widget")
        self.gridLayout_2.addWidget(self.ending_actions_widget, 4, 0, 1, 4)
        self.starting_actions_widget = MissionActionsWidget(self.mapping_groupBox)
        self.starting_actions_widget.setObjectName("starting_actions_widget")
        self.gridLayout_2.addWidget(self.starting_actions_widget, 3, 0, 1, 4)
        self.turning_label = QtWidgets.QLabel(self.mapping_groupBox)
        self.turning_label.setObjectName("turning_label")
        self.gridLayout_2.addWidget(self.turning_label, 1, 0, 1, 1)
        self.turning_zone_horizontalLayout = QtWidgets.QHBoxLayout()
        self.turning_zone_horizontalLayout.setObjectName("turning_zone_horizontalLayout")
        self.inside_radioButton = QtWidgets.QRadioButton(self.mapping_groupBox)
        self.inside_radioButton.setChecked(True)
        self.inside_radioButton.setObjectName("inside_radioButton")
        self.turning_zone_horizontalLayout.addWidget(self.inside_radioButton)
        self.outside_radioButton = QtWidgets.QRadioButton(self.mapping_groupBox)
        self.outside_radioButton.setObjectName("outside_radioButton")
        self.turning_zone_horizontalLayout.addWidget(self.outside_radioButton)
        self.gridLayout_2.addLayout(self.turning_zone_horizontalLayout, 1, 3, 1, 1)
        self.gridLayout.addWidget(self.mapping_groupBox, 0, 0, 1, 1)

        self.retranslateUi(MappingMissionWidget)
        QtCore.QMetaObject.connectSlotsByName(MappingMissionWidget)

    def retranslateUi(self, MappingMissionWidget):
        _translate = QtCore.QCoreApplication.translate
        MappingMissionWidget.setWindowTitle(_translate("MappingMissionWidget", "Form"))
        self.mapping_groupBox.setTitle(_translate("MappingMissionWidget", "Mapping"))
        self.distance_covered_turn_label.setText(_translate("MappingMissionWidget", "Transect to turn distance:"))
        self.ending_actions_widget.setProperty("label", _translate("MappingMissionWidget", "Ending actions:"))
        self.starting_actions_widget.setProperty("label", _translate("MappingMissionWidget", "Starting actions:"))
        self.turning_label.setText(_translate("MappingMissionWidget", "Turning zone:"))
        self.inside_radioButton.setText(_translate("MappingMissionWidget", "Inside"))
        self.outside_radioButton.setText(_translate("MappingMissionWidget", "Outside"))
from iquaview.src.mission.missionedition.missionactionswidget import MissionActionsWidget

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MappingMissionWidget = QtWidgets.QWidget()
    ui = Ui_MappingMissionWidget()
    ui.setupUi(MappingMissionWidget)
    MappingMissionWidget.show()
    sys.exit(app.exec_())
