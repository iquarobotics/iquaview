# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_go_to_dlg.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_GoToDialog(object):
    def setupUi(self, GoToDialog):
        GoToDialog.setObjectName("GoToDialog")
        GoToDialog.resize(349, 185)
        self.gridLayout = QtWidgets.QGridLayout(GoToDialog)
        self.gridLayout.setObjectName("gridLayout")
        self.buttonBox = QtWidgets.QDialogButtonBox(GoToDialog)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Close|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout.addWidget(self.buttonBox, 2, 0, 1, 1)
        self.gridLayout_2 = QtWidgets.QGridLayout()
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.latitude_widget = LatitudeWidget(GoToDialog)
        self.latitude_widget.setMinimumSize(QtCore.QSize(0, 0))
        self.latitude_widget.setObjectName("latitude_widget")
        self.gridLayout_2.addWidget(self.latitude_widget, 0, 0, 1, 2)
        self.tolerance_widget = ToleranceWidget(GoToDialog)
        self.tolerance_widget.setMinimumSize(QtCore.QSize(0, 0))
        self.tolerance_widget.setObjectName("tolerance_widget")
        self.gridLayout_2.addWidget(self.tolerance_widget, 7, 0, 1, 3)
        self.getCoordinatesButton = QtWidgets.QPushButton(GoToDialog)
        self.getCoordinatesButton.setMaximumSize(QtCore.QSize(27, 16777215))
        self.getCoordinatesButton.setText("")
        self.getCoordinatesButton.setObjectName("getCoordinatesButton")
        self.gridLayout_2.addWidget(self.getCoordinatesButton, 0, 2, 2, 1)
        self.heave_mode_widget = HeaveModeWidget(GoToDialog)
        self.heave_mode_widget.setObjectName("heave_mode_widget")
        self.gridLayout_2.addWidget(self.heave_mode_widget, 2, 0, 1, 3)
        self.longitude_widget = LongitudeWidget(GoToDialog)
        self.longitude_widget.setMinimumSize(QtCore.QSize(0, 0))
        self.longitude_widget.setObjectName("longitude_widget")
        self.gridLayout_2.addWidget(self.longitude_widget, 1, 0, 1, 2)
        self.speed_widget = SpeedWidget(GoToDialog)
        self.speed_widget.setMinimumSize(QtCore.QSize(0, 0))
        self.speed_widget.setObjectName("speed_widget")
        self.gridLayout_2.addWidget(self.speed_widget, 5, 0, 1, 3)
        self.no_altitude_widget = NoAltitudeWidget(GoToDialog)
        self.no_altitude_widget.setMinimumSize(QtCore.QSize(0, 0))
        self.no_altitude_widget.setObjectName("no_altitude_widget")
        self.gridLayout_2.addWidget(self.no_altitude_widget, 3, 0, 1, 3)
        self.gridLayout.addLayout(self.gridLayout_2, 0, 0, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem, 1, 0, 1, 1)

        self.retranslateUi(GoToDialog)
        QtCore.QMetaObject.connectSlotsByName(GoToDialog)

    def retranslateUi(self, GoToDialog):
        _translate = QtCore.QCoreApplication.translate
        GoToDialog.setWindowTitle(_translate("GoToDialog", "Go To"))


from iquaview.src.mission.missionedition.heavemodewidget import HeaveModeWidget
from iquaview.src.mission.missionedition.latitudewidget import LatitudeWidget
from iquaview.src.mission.missionedition.longitudewidget import LongitudeWidget
from iquaview.src.mission.missionedition.noaltitudewidget import NoAltitudeWidget
from iquaview.src.mission.missionedition.speedwidget import SpeedWidget
from iquaview.src.mission.missionedition.tolerancewidget import ToleranceWidget

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    GoToDialog = QtWidgets.QDialog()
    ui = Ui_GoToDialog()
    ui.setupUi(GoToDialog)
    GoToDialog.show()
    sys.exit(app.exec_())

