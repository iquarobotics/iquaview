# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_profile_tool.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_ProfileTool(object):
    def setupUi(self, ProfileTool):
        ProfileTool.setObjectName("ProfileTool")
        ProfileTool.resize(868, 300)
        ProfileTool.setMinimumSize(QtCore.QSize(0, 300))
        self.gridLayout_2 = QtWidgets.QGridLayout(ProfileTool)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.pt_verticalLayout = QtWidgets.QVBoxLayout()
        self.pt_verticalLayout.setSpacing(0)
        self.pt_verticalLayout.setObjectName("pt_verticalLayout")
        self.menu_widget = QtWidgets.QWidget(ProfileTool)
        self.menu_widget.setObjectName("menu_widget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.menu_widget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.draw_path_radioButton = QtWidgets.QRadioButton(self.menu_widget)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/resources/mActionEditCell.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.draw_path_radioButton.setIcon(icon)
        self.draw_path_radioButton.setObjectName("draw_path_radioButton")
        self.horizontalLayout.addWidget(self.draw_path_radioButton)
        self.plot_mission_radioButton = QtWidgets.QRadioButton(self.menu_widget)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/resources/mActionMission.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.plot_mission_radioButton.setIcon(icon1)
        self.plot_mission_radioButton.setObjectName("plot_mission_radioButton")
        self.horizontalLayout.addWidget(self.plot_mission_radioButton)
        self.line = QtWidgets.QFrame(self.menu_widget)
        self.line.setFrameShape(QtWidgets.QFrame.VLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.horizontalLayout.addWidget(self.line)
        self.mission_name_label = QtWidgets.QLabel(self.menu_widget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.mission_name_label.sizePolicy().hasHeightForWidth())
        self.mission_name_label.setSizePolicy(sizePolicy)
        self.mission_name_label.setObjectName("mission_name_label")
        self.horizontalLayout.addWidget(self.mission_name_label)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.pt_verticalLayout.addWidget(self.menu_widget)
        self.figure_widget = QtWidgets.QWidget(ProfileTool)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.figure_widget.sizePolicy().hasHeightForWidth())
        self.figure_widget.setSizePolicy(sizePolicy)
        self.figure_widget.setObjectName("figure_widget")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.figure_widget)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setSpacing(0)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.pt_verticalLayout.addWidget(self.figure_widget)
        self.gridLayout_2.addLayout(self.pt_verticalLayout, 0, 0, 2, 1)

        self.retranslateUi(ProfileTool)
        self.plot_mission_radioButton.toggled['bool'].connect(self.mission_name_label.setVisible)
        self.plot_mission_radioButton.toggled['bool'].connect(self.line.setVisible)
        QtCore.QMetaObject.connectSlotsByName(ProfileTool)
        ProfileTool.setTabOrder(self.draw_path_radioButton, self.plot_mission_radioButton)

    def retranslateUi(self, ProfileTool):
        _translate = QtCore.QCoreApplication.translate
        ProfileTool.setWindowTitle(_translate("ProfileTool", "Profile Tool"))
        self.draw_path_radioButton.setText(_translate("ProfileTool", "Draw path"))
        self.plot_mission_radioButton.setText(_translate("ProfileTool", "Plot mission"))
        self.mission_name_label.setText(_translate("ProfileTool", "No mission selected"))

from iquaview_lib import resources_rc


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    ProfileTool = QtWidgets.QWidget()
    ui = Ui_ProfileTool()
    ui.setupUi(ProfileTool)
    ProfileTool.show()
    sys.exit(app.exec_())
