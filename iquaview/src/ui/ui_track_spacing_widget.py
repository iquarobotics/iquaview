# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_track_spacing_widget.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_TrackSpacingWidget(object):
    def setupUi(self, TrackSpacingWidget):
        TrackSpacingWidget.setObjectName("TrackSpacingWidget")
        TrackSpacingWidget.resize(246, 144)
        self.verticalLayout = QtWidgets.QVBoxLayout(TrackSpacingWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.groupBox = QtWidgets.QGroupBox(TrackSpacingWidget)
        self.groupBox.setStyleSheet("QGroupBox {\n"
"    border: 1px solid silver;\n"
"    border-radius: 6px;\n"
"    margin-top: 6px;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 7px;\n"
"    padding: 0px 5px 0px 5px;\n"
"}")
        self.groupBox.setCheckable(False)
        self.groupBox.setChecked(False)
        self.groupBox.setObjectName("groupBox")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.groupBox)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.mounting_angle_label = QtWidgets.QLabel(self.groupBox)
        self.mounting_angle_label.setObjectName("mounting_angle_label")
        self.gridLayout_2.addWidget(self.mounting_angle_label, 3, 0, 1, 1)
        self.range_label = QtWidgets.QLabel(self.groupBox)
        self.range_label.setObjectName("range_label")
        self.gridLayout_2.addWidget(self.range_label, 0, 0, 1, 1)
        self.vertical_aperture_label = QtWidgets.QLabel(self.groupBox)
        self.vertical_aperture_label.setObjectName("vertical_aperture_label")
        self.gridLayout_2.addWidget(self.vertical_aperture_label, 2, 0, 1, 1)
        self.range_double_spin_box = QtWidgets.QDoubleSpinBox(self.groupBox)
        self.range_double_spin_box.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedKingdom))
        self.range_double_spin_box.setMinimum(1.0)
        self.range_double_spin_box.setMaximum(10000.0)
        self.range_double_spin_box.setProperty("value", 30.0)
        self.range_double_spin_box.setObjectName("range_double_spin_box")
        self.gridLayout_2.addWidget(self.range_double_spin_box, 0, 1, 1, 1)
        self.desired_overlap_label = QtWidgets.QLabel(self.groupBox)
        self.desired_overlap_label.setObjectName("desired_overlap_label")
        self.gridLayout_2.addWidget(self.desired_overlap_label, 4, 0, 1, 1)
        self.vertical_aperture_double_spin_box = QtWidgets.QDoubleSpinBox(self.groupBox)
        self.vertical_aperture_double_spin_box.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedKingdom))
        self.vertical_aperture_double_spin_box.setMaximum(90.0)
        self.vertical_aperture_double_spin_box.setProperty("value", 45.0)
        self.vertical_aperture_double_spin_box.setObjectName("vertical_aperture_double_spin_box")
        self.gridLayout_2.addWidget(self.vertical_aperture_double_spin_box, 2, 1, 1, 1)
        self.mounting_angle_double_spin_box = QtWidgets.QDoubleSpinBox(self.groupBox)
        self.mounting_angle_double_spin_box.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedKingdom))
        self.mounting_angle_double_spin_box.setMaximum(90.0)
        self.mounting_angle_double_spin_box.setProperty("value", 20.0)
        self.mounting_angle_double_spin_box.setObjectName("mounting_angle_double_spin_box")
        self.gridLayout_2.addWidget(self.mounting_angle_double_spin_box, 3, 1, 1, 1)
        self.overlap_spin_box = QtWidgets.QDoubleSpinBox(self.groupBox)
        self.overlap_spin_box.setEnabled(True)
        self.overlap_spin_box.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedKingdom))
        self.overlap_spin_box.setMaximum(100.0)
        self.overlap_spin_box.setProperty("value", 40.0)
        self.overlap_spin_box.setObjectName("overlap_spin_box")
        self.gridLayout_2.addWidget(self.overlap_spin_box, 4, 1, 1, 1)
        self.verticalLayout.addWidget(self.groupBox)

        self.retranslateUi(TrackSpacingWidget)
        QtCore.QMetaObject.connectSlotsByName(TrackSpacingWidget)

    def retranslateUi(self, TrackSpacingWidget):
        _translate = QtCore.QCoreApplication.translate
        TrackSpacingWidget.setWindowTitle(_translate("TrackSpacingWidget", "Track Spacing"))
        self.groupBox.setTitle(_translate("TrackSpacingWidget", "Track Spacing"))
        self.mounting_angle_label.setText(_translate("TrackSpacingWidget", "Mounting angle:"))
        self.range_label.setText(_translate("TrackSpacingWidget", "Range:"))
        self.vertical_aperture_label.setText(_translate("TrackSpacingWidget", "Vertical aperture:"))
        self.desired_overlap_label.setText(_translate("TrackSpacingWidget", "Desired overlap %"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    TrackSpacingWidget = QtWidgets.QWidget()
    ui = Ui_TrackSpacingWidget()
    ui.setupUi(TrackSpacingWidget)
    TrackSpacingWidget.show()
    sys.exit(app.exec_())
