# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_speed_widget.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_SpeedWidget(object):
    def setupUi(self, SpeedWidget):
        SpeedWidget.setObjectName("SpeedWidget")
        SpeedWidget.resize(356, 44)
        self.horizontalLayout = QtWidgets.QHBoxLayout(SpeedWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.speedLabel = QtWidgets.QLabel(SpeedWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.speedLabel.sizePolicy().hasHeightForWidth())
        self.speedLabel.setSizePolicy(sizePolicy)
        self.speedLabel.setMinimumSize(QtCore.QSize(119, 0))
        self.speedLabel.setObjectName("speedLabel")
        self.horizontalLayout.addWidget(self.speedLabel)
        self.speed_doubleSpinBox = QtWidgets.QDoubleSpinBox(SpeedWidget)
        self.speed_doubleSpinBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedKingdom))
        self.speed_doubleSpinBox.setMaximum(100000.0)
        self.speed_doubleSpinBox.setProperty("value", 0.5)
        self.speed_doubleSpinBox.setObjectName("speed_doubleSpinBox")
        self.horizontalLayout.addWidget(self.speed_doubleSpinBox)

        self.retranslateUi(SpeedWidget)
        QtCore.QMetaObject.connectSlotsByName(SpeedWidget)

    def retranslateUi(self, SpeedWidget):
        _translate = QtCore.QCoreApplication.translate
        SpeedWidget.setWindowTitle(_translate("SpeedWidget", "Speed"))
        self.speedLabel.setText(_translate("SpeedWidget", "Speed (m/s): "))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    SpeedWidget = QtWidgets.QWidget()
    ui = Ui_SpeedWidget()
    ui.setupUi(SpeedWidget)
    SpeedWidget.show()
    sys.exit(app.exec_())

