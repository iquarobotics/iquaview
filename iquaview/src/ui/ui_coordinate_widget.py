# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_coordinate_widget.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_CoordinateWidget(object):
    def setupUi(self, CoordinateWidget):
        CoordinateWidget.setObjectName("CoordinateWidget")
        CoordinateWidget.resize(306, 34)
        self.horizontalLayout = QtWidgets.QHBoxLayout(CoordinateWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.coordinate_label = QtWidgets.QLabel(CoordinateWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.coordinate_label.sizePolicy().hasHeightForWidth())
        self.coordinate_label.setSizePolicy(sizePolicy)
        self.coordinate_label.setMinimumSize(QtCore.QSize(119, 0))
        self.coordinate_label.setObjectName("coordinate_label")
        self.horizontalLayout.addWidget(self.coordinate_label)
        self.coordinate_lineEdit = QtWidgets.QLineEdit(CoordinateWidget)
        self.coordinate_lineEdit.setObjectName("coordinate_lineEdit")
        self.horizontalLayout.addWidget(self.coordinate_lineEdit)

        self.retranslateUi(CoordinateWidget)
        QtCore.QMetaObject.connectSlotsByName(CoordinateWidget)

    def retranslateUi(self, CoordinateWidget):
        _translate = QtCore.QCoreApplication.translate
        CoordinateWidget.setWindowTitle(_translate("CoordinateWidget", "Longitude"))
        self.coordinate_label.setText(_translate("CoordinateWidget", "Coordinate:"))
        self.coordinate_lineEdit.setText(_translate("CoordinateWidget", "0.0"))
