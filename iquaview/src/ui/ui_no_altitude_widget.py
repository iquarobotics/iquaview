# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_no_altitude_widget.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_NoAltitudeWidget(object):
    def setupUi(self, NoAltitudeWidget):
        NoAltitudeWidget.setObjectName("NoAltitudeWidget")
        NoAltitudeWidget.resize(294, 43)
        self.horizontalLayout = QtWidgets.QHBoxLayout(NoAltitudeWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.no_altitude_label = QtWidgets.QLabel(NoAltitudeWidget)
        self.no_altitude_label.setMinimumSize(QtCore.QSize(119, 0))
        self.no_altitude_label.setObjectName("no_altitude_label")
        self.horizontalLayout.addWidget(self.no_altitude_label)
        self.no_altitude_comboBox = QtWidgets.QComboBox(NoAltitudeWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.no_altitude_comboBox.sizePolicy().hasHeightForWidth())
        self.no_altitude_comboBox.setSizePolicy(sizePolicy)
        self.no_altitude_comboBox.setObjectName("no_altitude_comboBox")
        self.no_altitude_comboBox.addItem("")
        self.no_altitude_comboBox.addItem("")
        self.horizontalLayout.addWidget(self.no_altitude_comboBox)

        self.retranslateUi(NoAltitudeWidget)
        QtCore.QMetaObject.connectSlotsByName(NoAltitudeWidget)

    def retranslateUi(self, NoAltitudeWidget):
        _translate = QtCore.QCoreApplication.translate
        NoAltitudeWidget.setWindowTitle(_translate("NoAltitudeWidget", "No Altitude"))
        self.no_altitude_label.setToolTip(_translate("NoAltitudeWidget", "No altitude reactive behaviour"))
        self.no_altitude_label.setText(_translate("NoAltitudeWidget", "No Altitude:"))
        self.no_altitude_comboBox.setToolTip(_translate("NoAltitudeWidget", "No altitude reactive behaviour"))
        self.no_altitude_comboBox.setItemText(0, _translate("NoAltitudeWidget", "Go up"))
        self.no_altitude_comboBox.setItemText(1, _translate("NoAltitudeWidget", "Ignore"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    NoAltitudeWidget = QtWidgets.QWidget()
    ui = Ui_NoAltitudeWidget()
    ui.setupUi(NoAltitudeWidget)
    NoAltitudeWidget.show()
    sys.exit(app.exec_())

