# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_tolerance_widget.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_ToleranceWidget(object):
    def setupUi(self, ToleranceWidget):
        ToleranceWidget.setObjectName("ToleranceWidget")
        ToleranceWidget.resize(296, 44)
        self.horizontalLayout = QtWidgets.QHBoxLayout(ToleranceWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.tolerance_label = QtWidgets.QLabel(ToleranceWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tolerance_label.sizePolicy().hasHeightForWidth())
        self.tolerance_label.setSizePolicy(sizePolicy)
        self.tolerance_label.setMinimumSize(QtCore.QSize(119, 0))
        self.tolerance_label.setObjectName("tolerance_label")
        self.horizontalLayout.addWidget(self.tolerance_label)
        self.tolerance_xy_doubleSpinBox = QtWidgets.QDoubleSpinBox(ToleranceWidget)
        self.tolerance_xy_doubleSpinBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedKingdom))
        self.tolerance_xy_doubleSpinBox.setMaximum(100000.0)
        self.tolerance_xy_doubleSpinBox.setProperty("value", 2.0)
        self.tolerance_xy_doubleSpinBox.setObjectName("tolerance_xy_doubleSpinBox")
        self.horizontalLayout.addWidget(self.tolerance_xy_doubleSpinBox)

        self.retranslateUi(ToleranceWidget)
        QtCore.QMetaObject.connectSlotsByName(ToleranceWidget)

    def retranslateUi(self, ToleranceWidget):
        _translate = QtCore.QCoreApplication.translate
        ToleranceWidget.setWindowTitle(_translate("ToleranceWidget", "Tolerance"))
        self.tolerance_label.setText(_translate("ToleranceWidget", "Tolerance XY (m):"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    ToleranceWidget = QtWidgets.QWidget()
    ui = Ui_ToleranceWidget()
    ui.setupUi(ToleranceWidget)
    ToleranceWidget.show()
    sys.exit(app.exec_())

