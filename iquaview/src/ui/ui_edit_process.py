# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_edit_process.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_EditProcessDialog(object):
    def setupUi(self, EditProcessDialog):
        EditProcessDialog.setObjectName("EditProcessDialog")
        EditProcessDialog.resize(426, 105)
        self.gridLayout = QtWidgets.QGridLayout(EditProcessDialog)
        self.gridLayout.setObjectName("gridLayout")
        self.title_label = QtWidgets.QLabel(EditProcessDialog)
        self.title_label.setObjectName("title_label")
        self.gridLayout.addWidget(self.title_label, 0, 0, 1, 1)
        self.title_lineEdit = QtWidgets.QLineEdit(EditProcessDialog)
        self.title_lineEdit.setObjectName("title_lineEdit")
        self.gridLayout.addWidget(self.title_lineEdit, 0, 1, 1, 1)
        self.process_label = QtWidgets.QLabel(EditProcessDialog)
        self.process_label.setObjectName("process_label")
        self.gridLayout.addWidget(self.process_label, 1, 0, 1, 1)
        self.process_lineEdit = QtWidgets.QLineEdit(EditProcessDialog)
        self.process_lineEdit.setObjectName("process_lineEdit")
        self.gridLayout.addWidget(self.process_lineEdit, 1, 1, 1, 1)
        self.buttonBox = QtWidgets.QDialogButtonBox(EditProcessDialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Close|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout.addWidget(self.buttonBox, 2, 0, 1, 2)

        self.retranslateUi(EditProcessDialog)
        self.buttonBox.rejected.connect(EditProcessDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(EditProcessDialog)

    def retranslateUi(self, EditProcessDialog):
        _translate = QtCore.QCoreApplication.translate
        EditProcessDialog.setWindowTitle(_translate("EditProcessDialog", "Edit Process"))
        self.title_label.setText(_translate("EditProcessDialog", "Title:"))
        self.process_label.setText(_translate("EditProcessDialog", "Process:"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    EditProcessDialog = QtWidgets.QDialog()
    ui = Ui_EditProcessDialog()
    ui.setupUi(EditProcessDialog)
    EditProcessDialog.show()
    sys.exit(app.exec_())
