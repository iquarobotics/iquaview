# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_pluginmanager.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_PluginManager(object):
    def setupUi(self, PluginManager):
        PluginManager.setObjectName("PluginManager")
        PluginManager.resize(403, 367)
        self.verticalLayout = QtWidgets.QVBoxLayout(PluginManager)
        self.verticalLayout.setContentsMargins(9, 9, 9, 9)
        self.verticalLayout.setObjectName("verticalLayout")
        self.splitter = QtWidgets.QSplitter(PluginManager)
        self.splitter.setOrientation(QtCore.Qt.Vertical)
        self.splitter.setChildrenCollapsible(False)
        self.splitter.setObjectName("splitter")
        self.installed_plugins_widget = QtWidgets.QWidget(self.splitter)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(2)
        sizePolicy.setHeightForWidth(self.installed_plugins_widget.sizePolicy().hasHeightForWidth())
        self.installed_plugins_widget.setSizePolicy(sizePolicy)
        self.installed_plugins_widget.setObjectName("installed_plugins_widget")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.installed_plugins_widget)
        self.gridLayout_2.setContentsMargins(0, 0, 0, 9)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.remove_plugin_toolButton = QtWidgets.QToolButton(self.installed_plugins_widget)
        self.remove_plugin_toolButton.setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/resources/mActionDeleteSelected.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.remove_plugin_toolButton.setIcon(icon)
        self.remove_plugin_toolButton.setAutoRaise(True)
        self.remove_plugin_toolButton.setObjectName("remove_plugin_toolButton")
        self.gridLayout_2.addWidget(self.remove_plugin_toolButton, 0, 2, 1, 1)
        self.tableWidget = QtWidgets.QTableWidget(self.installed_plugins_widget)
        self.tableWidget.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.tableWidget.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(2)
        self.tableWidget.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(1, item)
        self.tableWidget.horizontalHeader().setCascadingSectionResizes(False)
        self.tableWidget.verticalHeader().setCascadingSectionResizes(False)
        self.gridLayout_2.addWidget(self.tableWidget, 1, 0, 1, 3)
        self.add_plugin_toolButton = QtWidgets.QToolButton(self.installed_plugins_widget)
        self.add_plugin_toolButton.setText("")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/resources/mActionAdd.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.add_plugin_toolButton.setIcon(icon1)
        self.add_plugin_toolButton.setAutoRaise(True)
        self.add_plugin_toolButton.setObjectName("add_plugin_toolButton")
        self.gridLayout_2.addWidget(self.add_plugin_toolButton, 0, 1, 1, 1)
        self.installed_plugins_label = QtWidgets.QLabel(self.installed_plugins_widget)
        self.installed_plugins_label.setObjectName("installed_plugins_label")
        self.gridLayout_2.addWidget(self.installed_plugins_label, 0, 0, 1, 1)
        self.log_widget = QtWidgets.QWidget(self.splitter)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.log_widget.sizePolicy().hasHeightForWidth())
        self.log_widget.setSizePolicy(sizePolicy)
        self.log_widget.setObjectName("log_widget")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.log_widget)
        self.gridLayout_3.setContentsMargins(0, 9, 0, 0)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.log_label = QtWidgets.QLabel(self.log_widget)
        self.log_label.setObjectName("log_label")
        self.gridLayout_3.addWidget(self.log_label, 0, 0, 1, 1)
        self.plainTextEdit = QtWidgets.QPlainTextEdit(self.log_widget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.plainTextEdit.sizePolicy().hasHeightForWidth())
        self.plainTextEdit.setSizePolicy(sizePolicy)
        self.plainTextEdit.setReadOnly(True)
        self.plainTextEdit.setObjectName("plainTextEdit")
        self.gridLayout_3.addWidget(self.plainTextEdit, 1, 0, 1, 1)
        self.verticalLayout.addWidget(self.splitter)

        self.retranslateUi(PluginManager)
        QtCore.QMetaObject.connectSlotsByName(PluginManager)

    def retranslateUi(self, PluginManager):
        _translate = QtCore.QCoreApplication.translate
        PluginManager.setWindowTitle(_translate("PluginManager", "Plugin Manager"))
        self.remove_plugin_toolButton.setToolTip(_translate("PluginManager", "Remove plugin"))
        item = self.tableWidget.horizontalHeaderItem(0)
        item.setText(_translate("PluginManager", "Package"))
        item = self.tableWidget.horizontalHeaderItem(1)
        item.setText(_translate("PluginManager", "Version"))
        self.add_plugin_toolButton.setToolTip(_translate("PluginManager", "Add new plugin"))
        self.installed_plugins_label.setText(_translate("PluginManager", "Installed plugins:"))
        self.log_label.setText(_translate("PluginManager", "Log:"))
from iquaview_lib import resources_rc


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    PluginManager = QtWidgets.QDialog()
    ui = Ui_PluginManager()
    ui.setupUi(PluginManager)
    PluginManager.show()
    sys.exit(app.exec_())
