# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_mission_table_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MissionTableDialog(object):
    def setupUi(self, MissionTableDialog):
        MissionTableDialog.setObjectName("MissionTableDialog")
        MissionTableDialog.resize(1236, 329)
        self.verticalLayout = QtWidgets.QVBoxLayout(MissionTableDialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.toolbar = QtWidgets.QToolBar(MissionTableDialog)
        self.toolbar.setIconSize(QtCore.QSize(16, 16))
        self.toolbar.setFloatable(False)
        self.toolbar.setObjectName("toolbar")
        self.verticalLayout.addWidget(self.toolbar)
        self.tableWidget = QtWidgets.QTableWidget(MissionTableDialog)
        self.tableWidget.setStyleSheet("QTableWidget QTableCornerButton::section {\n"
"    border: 1px solid #fffff8;\n"
"}")
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(12)
        self.tableWidget.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(3, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(4, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(5, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(6, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(7, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(8, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(9, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(10, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(11, item)
        self.tableWidget.horizontalHeader().setCascadingSectionResizes(False)
        self.tableWidget.horizontalHeader().setStretchLastSection(True)
        self.verticalLayout.addWidget(self.tableWidget)

        self.retranslateUi(MissionTableDialog)
        QtCore.QMetaObject.connectSlotsByName(MissionTableDialog)

    def retranslateUi(self, MissionTableDialog):
        _translate = QtCore.QCoreApplication.translate
        MissionTableDialog.setWindowTitle(_translate("MissionTableDialog", "Waypoints Table"))
        self.toolbar.setWindowTitle(_translate("MissionTableDialog", "toolBar"))
        item = self.tableWidget.horizontalHeaderItem(0)
        item.setText(_translate("MissionTableDialog", "Latitude"))
        item = self.tableWidget.horizontalHeaderItem(1)
        item.setText(_translate("MissionTableDialog", "Longitude"))
        item = self.tableWidget.horizontalHeaderItem(2)
        item.setText(_translate("MissionTableDialog", "Heave Mode"))
        item = self.tableWidget.horizontalHeaderItem(3)
        item.setText(_translate("MissionTableDialog", "Depth"))
        item = self.tableWidget.horizontalHeaderItem(4)
        item.setText(_translate("MissionTableDialog", "Altitude"))
        item = self.tableWidget.horizontalHeaderItem(5)
        item.setText(_translate("MissionTableDialog", "No Altitude "))
        item = self.tableWidget.horizontalHeaderItem(6)
        item.setText(_translate("MissionTableDialog", "Park"))
        item = self.tableWidget.horizontalHeaderItem(7)
        item.setText(_translate("MissionTableDialog", "Park Time"))
        item = self.tableWidget.horizontalHeaderItem(8)
        item.setText(_translate("MissionTableDialog", "Park Yaw"))
        item = self.tableWidget.horizontalHeaderItem(9)
        item.setText(_translate("MissionTableDialog", "Tolerance XY"))
        item = self.tableWidget.horizontalHeaderItem(10)
        item.setText(_translate("MissionTableDialog", "Speed"))
        item = self.tableWidget.horizontalHeaderItem(11)
        item.setText(_translate("MissionTableDialog", "Actions"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MissionTableDialog = QtWidgets.QDialog()
    ui = Ui_MissionTableDialog()
    ui.setupUi(MissionTableDialog)
    MissionTableDialog.show()
    sys.exit(app.exec_())

