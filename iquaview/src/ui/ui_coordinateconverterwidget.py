# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_coordinateconverterwidget.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_CoordinateConverterDialog(object):
    def setupUi(self, CoordinateConverterDialog):
        CoordinateConverterDialog.setObjectName("CoordinateConverterDialog")
        CoordinateConverterDialog.resize(580, 151)
        self.gridLayout = QtWidgets.QGridLayout(CoordinateConverterDialog)
        self.gridLayout.setObjectName("gridLayout")
        self.coordinates_label = QtWidgets.QLabel(CoordinateConverterDialog)
        self.coordinates_label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.coordinates_label.setObjectName("coordinates_label")
        self.gridLayout.addWidget(self.coordinates_label, 0, 0, 1, 1)
        self.degree_label = QtWidgets.QLabel(CoordinateConverterDialog)
        self.degree_label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.degree_label.setObjectName("degree_label")
        self.gridLayout.addWidget(self.degree_label, 1, 0, 1, 1)
        self.degree_latlon_label = QtWidgets.QLabel(CoordinateConverterDialog)
        self.degree_latlon_label.setMinimumSize(QtCore.QSize(0, 30))
        self.degree_latlon_label.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.degree_latlon_label.setLineWidth(6)
        self.degree_latlon_label.setAlignment(QtCore.Qt.AlignCenter)
        self.degree_latlon_label.setTextInteractionFlags(QtCore.Qt.LinksAccessibleByMouse|QtCore.Qt.TextSelectableByMouse)
        self.degree_latlon_label.setObjectName("degree_latlon_label")
        self.gridLayout.addWidget(self.degree_latlon_label, 1, 1, 1, 1)
        self.degreeminute_label = QtWidgets.QLabel(CoordinateConverterDialog)
        self.degreeminute_label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.degreeminute_label.setObjectName("degreeminute_label")
        self.gridLayout.addWidget(self.degreeminute_label, 2, 0, 1, 1)
        self.degreeminute_latlon_label = QtWidgets.QLabel(CoordinateConverterDialog)
        self.degreeminute_latlon_label.setMinimumSize(QtCore.QSize(0, 30))
        self.degreeminute_latlon_label.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.degreeminute_latlon_label.setAlignment(QtCore.Qt.AlignCenter)
        self.degreeminute_latlon_label.setTextInteractionFlags(QtCore.Qt.LinksAccessibleByMouse|QtCore.Qt.TextSelectableByMouse)
        self.degreeminute_latlon_label.setObjectName("degreeminute_latlon_label")
        self.gridLayout.addWidget(self.degreeminute_latlon_label, 2, 1, 1, 1)
        self.degreeminutesecond_label = QtWidgets.QLabel(CoordinateConverterDialog)
        self.degreeminutesecond_label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.degreeminutesecond_label.setObjectName("degreeminutesecond_label")
        self.gridLayout.addWidget(self.degreeminutesecond_label, 3, 0, 1, 1)
        self.degreeminutesecond_latlon_label = QtWidgets.QLabel(CoordinateConverterDialog)
        self.degreeminutesecond_latlon_label.setMinimumSize(QtCore.QSize(0, 30))
        self.degreeminutesecond_latlon_label.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.degreeminutesecond_latlon_label.setAlignment(QtCore.Qt.AlignCenter)
        self.degreeminutesecond_latlon_label.setTextInteractionFlags(QtCore.Qt.LinksAccessibleByMouse|QtCore.Qt.TextSelectableByMouse)
        self.degreeminutesecond_latlon_label.setObjectName("degreeminutesecond_latlon_label")
        self.gridLayout.addWidget(self.degreeminutesecond_latlon_label, 3, 1, 1, 1)

        self.retranslateUi(CoordinateConverterDialog)
        QtCore.QMetaObject.connectSlotsByName(CoordinateConverterDialog)

    def retranslateUi(self, CoordinateConverterDialog):
        _translate = QtCore.QCoreApplication.translate
        CoordinateConverterDialog.setWindowTitle(_translate("CoordinateConverterDialog", "Dialog"))
        self.coordinates_label.setText(_translate("CoordinateConverterDialog", "Coordinates (lat, lon):"))
        self.degree_label.setToolTip(_translate("CoordinateConverterDialog", "Decimal Degrees"))
        self.degree_label.setText(_translate("CoordinateConverterDialog", "Decimal Degrees"))
        self.degree_latlon_label.setText(_translate("CoordinateConverterDialog", f"000.000000°, 000.000000°"))
        self.degreeminute_label.setToolTip(_translate("CoordinateConverterDialog", "Degreees Minutes"))
        self.degreeminute_label.setText(_translate("CoordinateConverterDialog", "Degrees Minutes"))
        self.degreeminute_latlon_label.setText(_translate("CoordinateConverterDialog", f"000°00.0000\', 000°00.0000\'"))
        self.degreeminutesecond_label.setToolTip(_translate("CoordinateConverterDialog", "Degrees Minutes Seconds"))
        self.degreeminutesecond_label.setText(_translate("CoordinateConverterDialog", "Degrees Minutes Seconds"))
        self.degreeminutesecond_latlon_label.setText(_translate("CoordinateConverterDialog", f"000°00\'00.000\'\', 000°00\'00.000\'\'"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    CoordinateConverterDialog = QtWidgets.QDialog()
    ui = Ui_CoordinateConverterDialog()
    ui.setupUi(CoordinateConverterDialog)
    CoordinateConverterDialog.show()
    sys.exit(app.exec_())

