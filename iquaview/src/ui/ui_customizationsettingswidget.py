# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_customizationsettingswidget.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_CustomizationSettingsWidget(object):
    def setupUi(self, CustomizationSettingsWidget):
        CustomizationSettingsWidget.setObjectName("CustomizationSettingsWidget")
        CustomizationSettingsWidget.resize(566, 237)
        self.gridLayout = QtWidgets.QGridLayout(CustomizationSettingsWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.scrollArea = QtWidgets.QScrollArea(CustomizationSettingsWidget)
        self.scrollArea.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 564, 235))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.scrollAreaWidgetContents)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.rosparam_widget = QtWidgets.QWidget(self.scrollAreaWidgetContents)
        self.rosparam_widget.setObjectName("rosparam_widget")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.rosparam_widget)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.verticalLayout.addWidget(self.rosparam_widget)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.gridLayout.addWidget(self.scrollArea, 0, 0, 1, 1)

        self.retranslateUi(CustomizationSettingsWidget)
        QtCore.QMetaObject.connectSlotsByName(CustomizationSettingsWidget)

    def retranslateUi(self, CustomizationSettingsWidget):
        _translate = QtCore.QCoreApplication.translate
        CustomizationSettingsWidget.setWindowTitle(_translate("CustomizationSettingsWidget", "Customization Settings"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    CustomizationSettingsWidget = QtWidgets.QWidget()
    ui = Ui_CustomizationSettingsWidget()
    ui.setupUi(CustomizationSettingsWidget)
    CustomizationSettingsWidget.show()
    sys.exit(app.exec_())
