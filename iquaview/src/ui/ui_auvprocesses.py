# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_auvprocesses.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_AUVProcessesWidget(object):
    def setupUi(self, AUVProcessesWidget):
        AUVProcessesWidget.setObjectName("AUVProcessesWidget")
        AUVProcessesWidget.resize(270, 45)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(AUVProcessesWidget.sizePolicy().hasHeightForWidth())
        AUVProcessesWidget.setSizePolicy(sizePolicy)
        AUVProcessesWidget.setMinimumSize(QtCore.QSize(0, 45))
        self.horizontalLayout = QtWidgets.QHBoxLayout(AUVProcessesWidget)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.master_antenna_toolButton = QtWidgets.QToolButton(AUVProcessesWidget)
        self.master_antenna_toolButton.setText("")
        self.master_antenna_toolButton.setIconSize(QtCore.QSize(12, 12))
        self.master_antenna_toolButton.setPopupMode(QtWidgets.QToolButton.InstantPopup)
        self.master_antenna_toolButton.setToolButtonStyle(QtCore.Qt.ToolButtonIconOnly)
        self.master_antenna_toolButton.setAutoRaise(True)
        self.master_antenna_toolButton.setObjectName("master_antenna_toolButton")
        self.horizontalLayout.addWidget(self.master_antenna_toolButton)
        self.master_antenna_label = QtWidgets.QLabel(AUVProcessesWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.master_antenna_label.sizePolicy().hasHeightForWidth())
        self.master_antenna_label.setSizePolicy(sizePolicy)
        self.master_antenna_label.setObjectName("master_antenna_label")
        self.horizontalLayout.addWidget(self.master_antenna_label)
        self.connection_indicator = QtWidgets.QLabel(AUVProcessesWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.connection_indicator.sizePolicy().hasHeightForWidth())
        self.connection_indicator.setSizePolicy(sizePolicy)
        self.connection_indicator.setMinimumSize(QtCore.QSize(12, 12))
        self.connection_indicator.setMaximumSize(QtCore.QSize(12, 12))
        self.connection_indicator.setText("")
        self.connection_indicator.setPixmap(QtGui.QPixmap(":/resources/red_led.svg"))
        self.connection_indicator.setScaledContents(True)
        self.connection_indicator.setObjectName("connection_indicator")
        self.horizontalLayout.addWidget(self.connection_indicator)
        self.connection_label = QtWidgets.QLabel(AUVProcessesWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.connection_label.sizePolicy().hasHeightForWidth())
        self.connection_label.setSizePolicy(sizePolicy)
        self.connection_label.setMaximumSize(QtCore.QSize(55, 27))
        self.connection_label.setObjectName("connection_label")
        self.horizontalLayout.addWidget(self.connection_label)
        self.cola2_indicator = QtWidgets.QLabel(AUVProcessesWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.cola2_indicator.sizePolicy().hasHeightForWidth())
        self.cola2_indicator.setSizePolicy(sizePolicy)
        self.cola2_indicator.setMinimumSize(QtCore.QSize(12, 12))
        self.cola2_indicator.setMaximumSize(QtCore.QSize(12, 12))
        self.cola2_indicator.setText("")
        self.cola2_indicator.setPixmap(QtGui.QPixmap(":/resources/red_led.svg"))
        self.cola2_indicator.setScaledContents(True)
        self.cola2_indicator.setObjectName("cola2_indicator")
        self.horizontalLayout.addWidget(self.cola2_indicator)
        self.cola2_label = QtWidgets.QLabel(AUVProcessesWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.cola2_label.sizePolicy().hasHeightForWidth())
        self.cola2_label.setSizePolicy(sizePolicy)
        self.cola2_label.setMaximumSize(QtCore.QSize(55, 27))
        self.cola2_label.setObjectName("cola2_label")
        self.horizontalLayout.addWidget(self.cola2_label)
        self.processes_toolButton = QtWidgets.QToolButton(AUVProcessesWidget)
        self.processes_toolButton.setPopupMode(QtWidgets.QToolButton.DelayedPopup)
        self.processes_toolButton.setToolButtonStyle(QtCore.Qt.ToolButtonIconOnly)
        self.processes_toolButton.setAutoRaise(True)
        self.processes_toolButton.setArrowType(QtCore.Qt.DownArrow)
        self.processes_toolButton.setObjectName("processes_toolButton")
        self.horizontalLayout.addWidget(self.processes_toolButton)

        self.retranslateUi(AUVProcessesWidget)
        QtCore.QMetaObject.connectSlotsByName(AUVProcessesWidget)

    def retranslateUi(self, AUVProcessesWidget):
        _translate = QtCore.QCoreApplication.translate
        AUVProcessesWidget.setWindowTitle(_translate("AUVProcessesWidget", "AUV Connection"))
        self.master_antenna_label.setText(_translate("AUVProcessesWidget", "Master"))
        self.connection_label.setText(_translate("AUVProcessesWidget", "Vehicle"))
        self.cola2_label.setText(_translate("AUVProcessesWidget", "COLA2"))
        self.processes_toolButton.setText(_translate("AUVProcessesWidget", "..."))
from iquaview_lib import resources_rc


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    AUVProcessesWidget = QtWidgets.QWidget()
    ui = Ui_AUVProcessesWidget()
    ui.setupUi(AUVProcessesWidget)
    AUVProcessesWidget.show()
    sys.exit(app.exec_())
