# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_heave_mode.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_HeaveModeWidget(object):
    def setupUi(self, HeaveModeWidget):
        HeaveModeWidget.setObjectName("HeaveModeWidget")
        HeaveModeWidget.resize(228, 93)
        self.gridLayout = QtWidgets.QGridLayout(HeaveModeWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setHorizontalSpacing(3)
        self.gridLayout.setObjectName("gridLayout")
        self.altitude_label = QtWidgets.QLabel(HeaveModeWidget)
        self.altitude_label.setObjectName("altitude_label")
        self.gridLayout.addWidget(self.altitude_label, 3, 0, 1, 2)
        self.altitude_doubleSpinBox = QtWidgets.QDoubleSpinBox(HeaveModeWidget)
        self.altitude_doubleSpinBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedKingdom))
        self.altitude_doubleSpinBox.setMaximum(100000.0)
        self.altitude_doubleSpinBox.setProperty("value", 1.0)
        self.altitude_doubleSpinBox.setObjectName("altitude_doubleSpinBox")
        self.gridLayout.addWidget(self.altitude_doubleSpinBox, 3, 2, 1, 1)
        self.depth_label = QtWidgets.QLabel(HeaveModeWidget)
        self.depth_label.setObjectName("depth_label")
        self.gridLayout.addWidget(self.depth_label, 2, 0, 1, 1)
        self.depth_doubleSpinBox = QtWidgets.QDoubleSpinBox(HeaveModeWidget)
        self.depth_doubleSpinBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedKingdom))
        self.depth_doubleSpinBox.setSpecialValueText("")
        self.depth_doubleSpinBox.setMaximum(100000.0)
        self.depth_doubleSpinBox.setObjectName("depth_doubleSpinBox")
        self.gridLayout.addWidget(self.depth_doubleSpinBox, 2, 2, 1, 1)
        self.heave_mode_comboBox = QtWidgets.QComboBox(HeaveModeWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.heave_mode_comboBox.sizePolicy().hasHeightForWidth())
        self.heave_mode_comboBox.setSizePolicy(sizePolicy)
        self.heave_mode_comboBox.setObjectName("heave_mode_comboBox")
        self.heave_mode_comboBox.addItem("")
        self.heave_mode_comboBox.addItem("")
        self.heave_mode_comboBox.addItem("")
        self.gridLayout.addWidget(self.heave_mode_comboBox, 0, 2, 1, 1)
        self.heave_mode_label = QtWidgets.QLabel(HeaveModeWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.heave_mode_label.sizePolicy().hasHeightForWidth())
        self.heave_mode_label.setSizePolicy(sizePolicy)
        self.heave_mode_label.setMinimumSize(QtCore.QSize(119, 0))
        self.heave_mode_label.setObjectName("heave_mode_label")
        self.gridLayout.addWidget(self.heave_mode_label, 0, 0, 1, 1)

        self.retranslateUi(HeaveModeWidget)
        QtCore.QMetaObject.connectSlotsByName(HeaveModeWidget)
        HeaveModeWidget.setTabOrder(self.heave_mode_comboBox, self.depth_doubleSpinBox)
        HeaveModeWidget.setTabOrder(self.depth_doubleSpinBox, self.altitude_doubleSpinBox)

    def retranslateUi(self, HeaveModeWidget):
        _translate = QtCore.QCoreApplication.translate
        HeaveModeWidget.setWindowTitle(_translate("HeaveModeWidget", "Heave Mode"))
        self.altitude_label.setText(_translate("HeaveModeWidget", "Altitude (m):"))
        self.depth_label.setText(_translate("HeaveModeWidget", "Depth (m): "))
        self.heave_mode_comboBox.setItemText(0, _translate("HeaveModeWidget", "Depth"))
        self.heave_mode_comboBox.setItemText(1, _translate("HeaveModeWidget", "Altitude"))
        self.heave_mode_comboBox.setItemText(2, _translate("HeaveModeWidget", "Both"))
        self.heave_mode_label.setText(_translate("HeaveModeWidget", "Heave Mode: "))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    HeaveModeWidget = QtWidgets.QWidget()
    ui = Ui_HeaveModeWidget()
    ui.setupUi(HeaveModeWidget)
    HeaveModeWidget.show()
    sys.exit(app.exec_())

