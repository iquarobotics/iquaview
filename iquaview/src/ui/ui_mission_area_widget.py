# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_mission_area_widget.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MissionAreaWidget(object):
    def setupUi(self, MissionAreaWidget):
        MissionAreaWidget.setObjectName("MissionAreaWidget")
        MissionAreaWidget.resize(265, 171)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MissionAreaWidget.sizePolicy().hasHeightForWidth())
        MissionAreaWidget.setSizePolicy(sizePolicy)
        self.verticalLayout = QtWidgets.QVBoxLayout(MissionAreaWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.missionAreaGroupBox = QtWidgets.QGroupBox(MissionAreaWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.missionAreaGroupBox.sizePolicy().hasHeightForWidth())
        self.missionAreaGroupBox.setSizePolicy(sizePolicy)
        self.missionAreaGroupBox.setStyleSheet("QGroupBox {\n"
"    border: 1px solid silver;\n"
"    border-radius: 6px;\n"
"    margin-top: 6px;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 7px;\n"
"    padding: 0px 5px 0px 5px;\n"
"}")
        self.missionAreaGroupBox.setObjectName("missionAreaGroupBox")
        self.gridLayout = QtWidgets.QGridLayout(self.missionAreaGroupBox)
        self.gridLayout.setObjectName("gridLayout")
        self.automaticExtent = QtWidgets.QRadioButton(self.missionAreaGroupBox)
        self.automaticExtent.setChecked(True)
        self.automaticExtent.setObjectName("automaticExtent")
        self.gridLayout.addWidget(self.automaticExtent, 0, 0, 1, 2)
        self.fixedExtent = QtWidgets.QRadioButton(self.missionAreaGroupBox)
        self.fixedExtent.setObjectName("fixedExtent")
        self.gridLayout.addWidget(self.fixedExtent, 1, 0, 1, 2)
        self.fixed_extend_widget = QtWidgets.QWidget(self.missionAreaGroupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.fixed_extend_widget.sizePolicy().hasHeightForWidth())
        self.fixed_extend_widget.setSizePolicy(sizePolicy)
        self.fixed_extend_widget.setObjectName("fixed_extend_widget")
        self.gridLayout_4 = QtWidgets.QGridLayout(self.fixed_extend_widget)
        self.gridLayout_4.setContentsMargins(20, 0, 0, 0)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.alongTrackLabel = QtWidgets.QLabel(self.fixed_extend_widget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.alongTrackLabel.sizePolicy().hasHeightForWidth())
        self.alongTrackLabel.setSizePolicy(sizePolicy)
        self.alongTrackLabel.setObjectName("alongTrackLabel")
        self.gridLayout_4.addWidget(self.alongTrackLabel, 0, 0, 1, 1)
        self.alongTLength = QtWidgets.QDoubleSpinBox(self.fixed_extend_widget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.alongTLength.sizePolicy().hasHeightForWidth())
        self.alongTLength.setSizePolicy(sizePolicy)
        self.alongTLength.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedKingdom))
        self.alongTLength.setMaximum(100000.0)
        self.alongTLength.setProperty("value", 10.0)
        self.alongTLength.setObjectName("alongTLength")
        self.gridLayout_4.addWidget(self.alongTLength, 0, 1, 1, 1)
        self.acrossTrackLabel = QtWidgets.QLabel(self.fixed_extend_widget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.acrossTrackLabel.sizePolicy().hasHeightForWidth())
        self.acrossTrackLabel.setSizePolicy(sizePolicy)
        self.acrossTrackLabel.setObjectName("acrossTrackLabel")
        self.gridLayout_4.addWidget(self.acrossTrackLabel, 1, 0, 1, 1)
        self.acrossTLength = QtWidgets.QDoubleSpinBox(self.fixed_extend_widget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.acrossTLength.sizePolicy().hasHeightForWidth())
        self.acrossTLength.setSizePolicy(sizePolicy)
        self.acrossTLength.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedKingdom))
        self.acrossTLength.setMaximum(100000.0)
        self.acrossTLength.setProperty("value", 10.0)
        self.acrossTLength.setObjectName("acrossTLength")
        self.gridLayout_4.addWidget(self.acrossTLength, 1, 1, 1, 1)
        self.gridLayout.addWidget(self.fixed_extend_widget, 2, 0, 1, 2)
        self.drawRectangleButton = QtWidgets.QPushButton(self.missionAreaGroupBox)
        self.drawRectangleButton.setCheckable(True)
        self.drawRectangleButton.setAutoExclusive(False)
        self.drawRectangleButton.setAutoDefault(False)
        self.drawRectangleButton.setDefault(False)
        self.drawRectangleButton.setFlat(False)
        self.drawRectangleButton.setObjectName("drawRectangleButton")
        self.gridLayout.addWidget(self.drawRectangleButton, 3, 1, 1, 1)
        self.centerOnTargetButton = QtWidgets.QPushButton(self.missionAreaGroupBox)
        self.centerOnTargetButton.setCheckable(True)
        self.centerOnTargetButton.setObjectName("centerOnTargetButton")
        self.gridLayout.addWidget(self.centerOnTargetButton, 3, 0, 1, 1)
        self.verticalLayout.addWidget(self.missionAreaGroupBox)

        self.retranslateUi(MissionAreaWidget)
        self.fixedExtent.toggled['bool'].connect(self.fixed_extend_widget.setVisible)
        QtCore.QMetaObject.connectSlotsByName(MissionAreaWidget)
        MissionAreaWidget.setTabOrder(self.automaticExtent, self.fixedExtent)
        MissionAreaWidget.setTabOrder(self.fixedExtent, self.alongTLength)
        MissionAreaWidget.setTabOrder(self.alongTLength, self.acrossTLength)

    def retranslateUi(self, MissionAreaWidget):
        _translate = QtCore.QCoreApplication.translate
        MissionAreaWidget.setWindowTitle(_translate("MissionAreaWidget", "MissionArea"))
        self.missionAreaGroupBox.setTitle(_translate("MissionAreaWidget", "Mission Area"))
        self.automaticExtent.setText(_translate("MissionAreaWidget", "Automatic extent"))
        self.fixedExtent.setText(_translate("MissionAreaWidget", "Fixed extent "))
        self.alongTrackLabel.setText(_translate("MissionAreaWidget", "Along track (m)"))
        self.acrossTrackLabel.setText(_translate("MissionAreaWidget", "Across track (m)"))
        self.drawRectangleButton.setText(_translate("MissionAreaWidget", "Draw Rectangle"))
        self.centerOnTargetButton.setText(_translate("MissionAreaWidget", "Center on Target"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MissionAreaWidget = QtWidgets.QWidget()
    ui = Ui_MissionAreaWidget()
    ui.setupUi(MissionAreaWidget)
    MissionAreaWidget.show()
    sys.exit(app.exec_())
