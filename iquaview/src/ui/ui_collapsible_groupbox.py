# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_collapsible_groupbox.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_mCollapsibleGroupBox(object):
    def setupUi(self, mCollapsibleGroupBox):
        mCollapsibleGroupBox.setObjectName("mCollapsibleGroupBox")
        mCollapsibleGroupBox.resize(400, 77)
        self.verticalLayout = QtWidgets.QVBoxLayout(mCollapsibleGroupBox)
        self.verticalLayout.setObjectName("verticalLayout")
        self.vboxlayout = QtWidgets.QVBoxLayout()
        self.vboxlayout.setObjectName("vboxlayout")
        self.verticalLayout.addLayout(self.vboxlayout)
        self.add_pushButton = QtWidgets.QPushButton(mCollapsibleGroupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.add_pushButton.sizePolicy().hasHeightForWidth())
        self.add_pushButton.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setBold(False)
        font.setItalic(True)
        font.setUnderline(False)
        font.setWeight(50)
        font.setStrikeOut(False)
        font.setKerning(True)
        self.add_pushButton.setFont(font)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/resources/mActionAdd.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.add_pushButton.setIcon(icon)
        self.add_pushButton.setFlat(False)
        self.add_pushButton.setObjectName("add_pushButton")
        self.verticalLayout.addWidget(self.add_pushButton)

        self.retranslateUi(mCollapsibleGroupBox)
        QtCore.QMetaObject.connectSlotsByName(mCollapsibleGroupBox)

    def retranslateUi(self, mCollapsibleGroupBox):
        _translate = QtCore.QCoreApplication.translate
        mCollapsibleGroupBox.setWindowTitle(_translate("mCollapsibleGroupBox", "gsCollapsibleGroupBox"))
        mCollapsibleGroupBox.setTitle(_translate("mCollapsibleGroupBox", "Group"))
        self.add_pushButton.setText(_translate("mCollapsibleGroupBox", "New"))

from qgis import gui
from iquaview_lib import resources_rc

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    mCollapsibleGroupBox = QtWidgets.QgsCollapsibleGroupBox()
    ui = Ui_mCollapsibleGroupBox()
    ui.setupUi(mCollapsibleGroupBox)
    mCollapsibleGroupBox.show()
    sys.exit(app.exec_())

