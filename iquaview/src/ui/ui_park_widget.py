# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_park_widget.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_ParkWidget(object):
    def setupUi(self, ParkWidget):
        ParkWidget.setObjectName("ParkWidget")
        ParkWidget.resize(254, 111)
        self.gridLayout = QtWidgets.QGridLayout(ParkWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.park_state_label = QtWidgets.QLabel(ParkWidget)
        self.park_state_label.setMinimumSize(QtCore.QSize(119, 0))
        self.park_state_label.setObjectName("park_state_label")
        self.gridLayout.addWidget(self.park_state_label, 0, 0, 1, 1)
        self.yaw_label = QtWidgets.QLabel(ParkWidget)
        self.yaw_label.setObjectName("yaw_label")
        self.gridLayout.addWidget(self.yaw_label, 2, 0, 1, 1)
        self.park_state_comboBox = QtWidgets.QComboBox(ParkWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.park_state_comboBox.sizePolicy().hasHeightForWidth())
        self.park_state_comboBox.setSizePolicy(sizePolicy)
        self.park_state_comboBox.setObjectName("park_state_comboBox")
        self.park_state_comboBox.addItem("")
        self.park_state_comboBox.addItem("")
        self.park_state_comboBox.addItem("")
        self.gridLayout.addWidget(self.park_state_comboBox, 0, 1, 1, 1)
        self.park_yaw_doubleSpinBox = QtWidgets.QDoubleSpinBox(ParkWidget)
        self.park_yaw_doubleSpinBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedKingdom))
        self.park_yaw_doubleSpinBox.setMaximum(360.0)
        self.park_yaw_doubleSpinBox.setObjectName("park_yaw_doubleSpinBox")
        self.gridLayout.addWidget(self.park_yaw_doubleSpinBox, 2, 1, 1, 1)
        self.parkTime_label = QtWidgets.QLabel(ParkWidget)
        self.parkTime_label.setObjectName("parkTime_label")
        self.gridLayout.addWidget(self.parkTime_label, 1, 0, 1, 1)
        self.parktime_doubleSpinBox = QtWidgets.QDoubleSpinBox(ParkWidget)
        self.parktime_doubleSpinBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedKingdom))
        self.parktime_doubleSpinBox.setMaximum(1000000.0)
        self.parktime_doubleSpinBox.setProperty("value", 10.0)
        self.parktime_doubleSpinBox.setObjectName("parktime_doubleSpinBox")
        self.gridLayout.addWidget(self.parktime_doubleSpinBox, 1, 1, 1, 1)
        self.tolerance_widget = ToleranceWidget(ParkWidget)
        self.tolerance_widget.setMinimumSize(QtCore.QSize(0, 0))
        self.tolerance_widget.setObjectName("tolerance_widget")
        self.gridLayout.addWidget(self.tolerance_widget, 3, 0, 1, 2)

        self.retranslateUi(ParkWidget)
        QtCore.QMetaObject.connectSlotsByName(ParkWidget)
        ParkWidget.setTabOrder(self.park_state_comboBox, self.parktime_doubleSpinBox)
        ParkWidget.setTabOrder(self.parktime_doubleSpinBox, self.park_yaw_doubleSpinBox)

    def retranslateUi(self, ParkWidget):
        _translate = QtCore.QCoreApplication.translate
        ParkWidget.setWindowTitle(_translate("ParkWidget", "Park"))
        self.park_state_label.setText(_translate("ParkWidget", "Park:"))
        self.yaw_label.setText(_translate("ParkWidget", f"Park Yaw (°):"))
        self.park_state_comboBox.setItemText(0, _translate("ParkWidget", "Disabled"))
        self.park_state_comboBox.setItemText(1, _translate("ParkWidget", "Anchor"))
        self.park_state_comboBox.setItemText(2, _translate("ParkWidget", "Holonomic"))
        self.parkTime_label.setText(_translate("ParkWidget", "Park Time (s): "))

from iquaview.src.mission.missionedition.tolerancewidget import ToleranceWidget

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    ParkWidget = QtWidgets.QWidget()
    ui = Ui_ParkWidget()
    ui.setupUi(ParkWidget)
    ParkWidget.show()
    sys.exit(app.exec_())

