# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_web_server.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_WebServerWidget(object):
    def setupUi(self, WebServerWidget):
        WebServerWidget.setObjectName("WebServerWidget")
        WebServerWidget.resize(273, 179)
        self.gridLayout = QtWidgets.QGridLayout(WebServerWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.state_label = QtWidgets.QLabel(WebServerWidget)
        self.state_label.setObjectName("state_label")
        self.gridLayout.addWidget(self.state_label, 0, 0, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem, 2, 1, 1, 1)
        self.port_label = QtWidgets.QLabel(WebServerWidget)
        self.port_label.setObjectName("port_label")
        self.gridLayout.addWidget(self.port_label, 0, 2, 1, 1, QtCore.Qt.AlignRight)
        self.state_hboxlayout = QtWidgets.QHBoxLayout()
        self.state_hboxlayout.setObjectName("state_hboxlayout")
        self.gridLayout.addLayout(self.state_hboxlayout, 0, 1, 1, 1)
        self.port_text = QtWidgets.QLineEdit(WebServerWidget)
        self.port_text.setEnabled(True)
        self.port_text.setMaximumSize(QtCore.QSize(55, 16777215))
        self.port_text.setObjectName("port_text")
        self.gridLayout.addWidget(self.port_text, 0, 3, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem1, 0, 4, 1, 1)
        self.missions_horizontalLayout = QtWidgets.QHBoxLayout()
        self.missions_horizontalLayout.setObjectName("missions_horizontalLayout")
        self.missions_mGroupBox = gui.QgsCollapsibleGroupBox(WebServerWidget)
        self.missions_mGroupBox.setObjectName("missions_mGroupBox")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.missions_mGroupBox)
        self.verticalLayout.setObjectName("verticalLayout")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setSpacing(1)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.select_checkBox = QtWidgets.QCheckBox(self.missions_mGroupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.select_checkBox.sizePolicy().hasHeightForWidth())
        self.select_checkBox.setSizePolicy(sizePolicy)
        self.select_checkBox.setText("")
        self.select_checkBox.setTristate(False)
        self.select_checkBox.setObjectName("select_checkBox")
        self.horizontalLayout.addWidget(self.select_checkBox)
        self.select_toolButton = QtWidgets.QToolButton(self.missions_mGroupBox)
        self.select_toolButton.setText("")
        self.select_toolButton.setIconSize(QtCore.QSize(4, 16))
        self.select_toolButton.setToolButtonStyle(QtCore.Qt.ToolButtonIconOnly)
        self.select_toolButton.setAutoRaise(True)
        self.select_toolButton.setArrowType(QtCore.Qt.DownArrow)
        self.select_toolButton.setObjectName("select_toolButton")
        self.horizontalLayout.addWidget(self.select_toolButton)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem2)
        self.verticalLayout_2.addLayout(self.horizontalLayout)
        self.missions_listWidget = QtWidgets.QListWidget(self.missions_mGroupBox)
        self.missions_listWidget.setObjectName("missions_listWidget")
        self.verticalLayout_2.addWidget(self.missions_listWidget)
        self.verticalLayout.addLayout(self.verticalLayout_2)
        self.missions_horizontalLayout.addWidget(self.missions_mGroupBox)
        self.gridLayout.addLayout(self.missions_horizontalLayout, 1, 0, 1, 5)

        self.retranslateUi(WebServerWidget)
        QtCore.QMetaObject.connectSlotsByName(WebServerWidget)

    def retranslateUi(self, WebServerWidget):
        _translate = QtCore.QCoreApplication.translate
        WebServerWidget.setWindowTitle(_translate("WebServerWidget", "Form"))
        self.state_label.setText(_translate("WebServerWidget", "Web Server State:"))
        self.port_label.setText(_translate("WebServerWidget", "Port:"))
        self.missions_mGroupBox.setTitle(_translate("WebServerWidget", "Missions"))
from qgis import gui


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    WebServerWidget = QtWidgets.QWidget()
    ui = Ui_WebServerWidget()
    ui.setupUi(WebServerWidget)
    WebServerWidget.show()
    sys.exit(app.exec_())
