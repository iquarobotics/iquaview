# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_imu_calibration.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_CalibrationIMU(object):
    def setupUi(self, CalibrationIMU):
        CalibrationIMU.setObjectName("CalibrationIMU")
        CalibrationIMU.resize(400, 300)
        self.verticalLayout = QtWidgets.QVBoxLayout(CalibrationIMU)
        self.verticalLayout.setObjectName("verticalLayout")
        self.image_label = QtWidgets.QLabel(CalibrationIMU)
        self.image_label.setAlignment(QtCore.Qt.AlignCenter)
        self.image_label.setObjectName("image_label")
        self.verticalLayout.addWidget(self.image_label)
        self.buttonBox = QtWidgets.QDialogButtonBox(CalibrationIMU)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(CalibrationIMU)
        self.buttonBox.accepted.connect(CalibrationIMU.accept)
        self.buttonBox.rejected.connect(CalibrationIMU.reject)
        QtCore.QMetaObject.connectSlotsByName(CalibrationIMU)

    def retranslateUi(self, CalibrationIMU):
        _translate = QtCore.QCoreApplication.translate
        CalibrationIMU.setWindowTitle(_translate("CalibrationIMU", "IMU Calibration"))
        self.image_label.setText(_translate("CalibrationIMU", "image"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    CalibrationIMU = QtWidgets.QDialog()
    ui = Ui_CalibrationIMU()
    ui.setupUi(CalibrationIMU)
    CalibrationIMU.show()
    sys.exit(app.exec_())

