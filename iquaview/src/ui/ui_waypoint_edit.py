# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_waypoint_edit.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_WaypointEditWidget(object):
    def setupUi(self, WaypointEditWidget):
        WaypointEditWidget.setObjectName("WaypointEditWidget")
        WaypointEditWidget.resize(320, 289)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(WaypointEditWidget.sizePolicy().hasHeightForWidth())
        WaypointEditWidget.setSizePolicy(sizePolicy)
        WaypointEditWidget.setMinimumSize(QtCore.QSize(320, 0))
        self.verticalLayout = QtWidgets.QVBoxLayout(WaypointEditWidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.scrollArea = QtWidgets.QScrollArea(WaypointEditWidget)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 300, 269))
        self.scrollAreaWidgetContents.setMinimumSize(QtCore.QSize(0, 0))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.scrollAreaWidgetContents)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.waypoint_editor_gridLayout = QtWidgets.QGridLayout()
        self.waypoint_editor_gridLayout.setObjectName("waypoint_editor_gridLayout")
        self.delete_pushButton = QtWidgets.QPushButton(self.scrollAreaWidgetContents)
        self.delete_pushButton.setObjectName("delete_pushButton")
        self.waypoint_editor_gridLayout.addWidget(self.delete_pushButton, 10, 0, 1, 2)
        self.titleLayout = QtWidgets.QHBoxLayout()
        self.titleLayout.setObjectName("titleLayout")
        self.previousWpButtonLayout = QtWidgets.QHBoxLayout()
        self.previousWpButtonLayout.setObjectName("previousWpButtonLayout")
        self.previousWpButton = QtWidgets.QPushButton(self.scrollAreaWidgetContents)
        self.previousWpButton.setMaximumSize(QtCore.QSize(30, 16777215))
        self.previousWpButton.setObjectName("previousWpButton")
        self.previousWpButtonLayout.addWidget(self.previousWpButton)
        self.titleLayout.addLayout(self.previousWpButtonLayout)
        self.TitleWaypointLabel = QtWidgets.QLabel(self.scrollAreaWidgetContents)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.TitleWaypointLabel.sizePolicy().hasHeightForWidth())
        self.TitleWaypointLabel.setSizePolicy(sizePolicy)
        self.TitleWaypointLabel.setTextFormat(QtCore.Qt.AutoText)
        self.TitleWaypointLabel.setScaledContents(False)
        self.TitleWaypointLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.TitleWaypointLabel.setWordWrap(False)
        self.TitleWaypointLabel.setObjectName("TitleWaypointLabel")
        self.titleLayout.addWidget(self.TitleWaypointLabel)
        self.nextWpButtonLayout = QtWidgets.QHBoxLayout()
        self.nextWpButtonLayout.setObjectName("nextWpButtonLayout")
        self.nextWpButton = QtWidgets.QPushButton(self.scrollAreaWidgetContents)
        self.nextWpButton.setEnabled(True)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.nextWpButton.sizePolicy().hasHeightForWidth())
        self.nextWpButton.setSizePolicy(sizePolicy)
        self.nextWpButton.setMaximumSize(QtCore.QSize(30, 16777215))
        self.nextWpButton.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.nextWpButton.setAutoFillBackground(False)
        self.nextWpButton.setObjectName("nextWpButton")
        self.nextWpButtonLayout.addWidget(self.nextWpButton)
        self.titleLayout.addLayout(self.nextWpButtonLayout)
        self.waypoint_editor_gridLayout.addLayout(self.titleLayout, 0, 0, 1, 2)
        self.heave_mode_widget = HeaveModeWidget(self.scrollAreaWidgetContents)
        self.heave_mode_widget.setObjectName("heave_mode_widget")
        self.waypoint_editor_gridLayout.addWidget(self.heave_mode_widget, 3, 0, 2, 2)
        self.latitude_widget = LatitudeWidget(self.scrollAreaWidgetContents)
        self.latitude_widget.setMinimumSize(QtCore.QSize(0, 0))
        self.latitude_widget.setObjectName("latitude_widget")
        self.waypoint_editor_gridLayout.addWidget(self.latitude_widget, 1, 0, 1, 2)
        self.longitude_widget = LongitudeWidget(self.scrollAreaWidgetContents)
        self.longitude_widget.setMinimumSize(QtCore.QSize(0, 0))
        self.longitude_widget.setObjectName("longitude_widget")
        self.waypoint_editor_gridLayout.addWidget(self.longitude_widget, 2, 0, 1, 2)
        self.no_altitude_widget = NoAltitudeWidget(self.scrollAreaWidgetContents)
        self.no_altitude_widget.setMinimumSize(QtCore.QSize(0, 0))
        self.no_altitude_widget.setObjectName("no_altitude_widget")
        self.waypoint_editor_gridLayout.addWidget(self.no_altitude_widget, 5, 0, 1, 2)
        self.mission_actions_widget = MissionActionsWidget(self.scrollAreaWidgetContents)
        self.mission_actions_widget.setMinimumSize(QtCore.QSize(0, 0))
        self.mission_actions_widget.setObjectName("mission_actions_widget")
        self.waypoint_editor_gridLayout.addWidget(self.mission_actions_widget, 8, 0, 1, 2)
        self.park_widget = ParkWidget(self.scrollAreaWidgetContents)
        self.park_widget.setMinimumSize(QtCore.QSize(0, 0))
        self.park_widget.setObjectName("park_widget")
        self.waypoint_editor_gridLayout.addWidget(self.park_widget, 6, 0, 1, 2)
        self.speed_widget = SpeedWidget(self.scrollAreaWidgetContents)
        self.speed_widget.setMinimumSize(QtCore.QSize(0, 0))
        self.speed_widget.setObjectName("speed_widget")
        self.waypoint_editor_gridLayout.addWidget(self.speed_widget, 7, 0, 1, 2)
        self.verticalLayout_3.addLayout(self.waypoint_editor_gridLayout)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.verticalLayout.addWidget(self.scrollArea)

        self.retranslateUi(WaypointEditWidget)
        QtCore.QMetaObject.connectSlotsByName(WaypointEditWidget)
        WaypointEditWidget.setTabOrder(self.scrollArea, self.previousWpButton)
        WaypointEditWidget.setTabOrder(self.previousWpButton, self.nextWpButton)
        WaypointEditWidget.setTabOrder(self.nextWpButton, self.delete_pushButton)

    def retranslateUi(self, WaypointEditWidget):
        _translate = QtCore.QCoreApplication.translate
        WaypointEditWidget.setWindowTitle(_translate("WaypointEditWidget", "Form"))
        self.delete_pushButton.setText(_translate("WaypointEditWidget", "Delete Waypoint"))
        self.previousWpButton.setText(_translate("WaypointEditWidget", "<-"))
        self.TitleWaypointLabel.setText(_translate("WaypointEditWidget", "No waypoints"))
        self.nextWpButton.setText(_translate("WaypointEditWidget", "->"))

from iquaview.src.mission.missionedition.heavemodewidget import HeaveModeWidget
from iquaview.src.mission.missionedition.latitudewidget import LatitudeWidget
from iquaview.src.mission.missionedition.longitudewidget import LongitudeWidget
from iquaview.src.mission.missionedition.missionactionswidget import MissionActionsWidget
from iquaview.src.mission.missionedition.noaltitudewidget import NoAltitudeWidget
from iquaview.src.mission.missionedition.parkwidget import ParkWidget
from iquaview.src.mission.missionedition.speedwidget import SpeedWidget

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    WaypointEditWidget = QtWidgets.QWidget()
    ui = Ui_WaypointEditWidget()
    ui.setupUi(WaypointEditWidget)
    WaypointEditWidget.show()
    sys.exit(app.exec_())

