# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_point_feature.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_PointFeature(object):
    def setupUi(self, PointFeature):
        PointFeature.setObjectName("PointFeature")
        PointFeature.resize(480, 101)
        self.verticalLayout = QtWidgets.QVBoxLayout(PointFeature)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(PointFeature)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.coordinates_vboxlayout = QtWidgets.QVBoxLayout()
        self.coordinates_vboxlayout.setObjectName("coordinates_vboxlayout")
        self.horizontalLayout.addLayout(self.coordinates_vboxlayout)
        self.getCoordinatesButton = QtWidgets.QPushButton(PointFeature)
        self.getCoordinatesButton.setMaximumSize(QtCore.QSize(27, 16777215))
        self.getCoordinatesButton.setText("")
        self.getCoordinatesButton.setObjectName("getCoordinatesButton")
        self.horizontalLayout.addWidget(self.getCoordinatesButton)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.status_bar = QtWidgets.QStatusBar(PointFeature)
        self.status_bar.setSizeGripEnabled(False)
        self.status_bar.setObjectName("status_bar")
        self.horizontalLayout_2.addWidget(self.status_bar)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.copy_button = QtWidgets.QPushButton(PointFeature)
        self.copy_button.setMinimumSize(QtCore.QSize(27, 0))
        self.copy_button.setMaximumSize(QtCore.QSize(27, 16777215))
        self.copy_button.setFocusPolicy(QtCore.Qt.NoFocus)
        self.copy_button.setText("")
        self.copy_button.setObjectName("copy_button")
        self.horizontalLayout_2.addWidget(self.copy_button)
        self.buttonBox = QtWidgets.QDialogButtonBox(PointFeature)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Close|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.horizontalLayout_2.addWidget(self.buttonBox)
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.retranslateUi(PointFeature)
        self.buttonBox.accepted.connect(PointFeature.accept)
        self.buttonBox.rejected.connect(PointFeature.reject)
        QtCore.QMetaObject.connectSlotsByName(PointFeature)

    def retranslateUi(self, PointFeature):
        _translate = QtCore.QCoreApplication.translate
        PointFeature.setWindowTitle(_translate("PointFeature", "Point Feature"))
        self.label.setText(_translate("PointFeature", "Enter new coordinates as \"latitude, longitude\""))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    PointFeature = QtWidgets.QDialog()
    ui = Ui_PointFeature()
    ui.setupUi(PointFeature)
    PointFeature.show()
    sys.exit(app.exec_())
