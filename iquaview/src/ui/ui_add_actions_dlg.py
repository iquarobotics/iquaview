# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_add_actions_dlg.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_AddActionDlg(object):
    def setupUi(self, AddActionDlg):
        AddActionDlg.setObjectName("AddActionDlg")
        AddActionDlg.resize(415, 287)
        self.gridLayout = QtWidgets.QGridLayout(AddActionDlg)
        self.gridLayout.setObjectName("gridLayout")
        self.listWidget = QtWidgets.QListWidget(AddActionDlg)
        self.listWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.listWidget.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectItems)
        self.listWidget.setObjectName("listWidget")
        self.gridLayout.addWidget(self.listWidget, 0, 0, 1, 1)
        self.buttonBox = QtWidgets.QDialogButtonBox(AddActionDlg)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout.addWidget(self.buttonBox, 1, 0, 1, 1)

        self.retranslateUi(AddActionDlg)
        self.buttonBox.accepted.connect(AddActionDlg.accept)
        self.buttonBox.rejected.connect(AddActionDlg.reject)
        QtCore.QMetaObject.connectSlotsByName(AddActionDlg)

    def retranslateUi(self, AddActionDlg):
        _translate = QtCore.QCoreApplication.translate
        AddActionDlg.setWindowTitle(_translate("AddActionDlg", "Add Actions"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    AddActionDlg = QtWidgets.QDialog()
    ui = Ui_AddActionDlg()
    ui.setupUi(AddActionDlg)
    AddActionDlg.show()
    sys.exit(app.exec_())
