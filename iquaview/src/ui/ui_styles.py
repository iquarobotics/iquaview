# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_styles.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_StylesWidget(object):
    def setupUi(self, StylesWidget):
        StylesWidget.setObjectName("StylesWidget")
        StylesWidget.resize(329, 268)
        self.verticalLayout = QtWidgets.QVBoxLayout(StylesWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.map_deco_groupbox = QtWidgets.QGroupBox(StylesWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.map_deco_groupbox.sizePolicy().hasHeightForWidth())
        self.map_deco_groupbox.setSizePolicy(sizePolicy)
        self.map_deco_groupbox.setStyleSheet("QGroupBox {\n"
"    border: 1px solid silver;\n"
"    border-radius: 6px;\n"
"    margin-top: 6px;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 7px;\n"
"    padding: 0px 5px 0px 5px;\n"
"}")
        self.map_deco_groupbox.setObjectName("map_deco_groupbox")
        self.gridLayout = QtWidgets.QGridLayout(self.map_deco_groupbox)
        self.gridLayout.setObjectName("gridLayout")
        self.label_2 = QtWidgets.QLabel(self.map_deco_groupbox)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 1)
        self.north_arrow_color_button = gui.QgsColorButton(self.map_deco_groupbox)
        self.north_arrow_color_button.setMinimumSize(QtCore.QSize(80, 16))
        self.north_arrow_color_button.setMaximumSize(QtCore.QSize(80, 16777215))
        self.north_arrow_color_button.setFocusPolicy(QtCore.Qt.NoFocus)
        self.north_arrow_color_button.setObjectName("north_arrow_color_button")
        self.gridLayout.addWidget(self.north_arrow_color_button, 0, 1, 1, 1)
        self.label_1 = QtWidgets.QLabel(self.map_deco_groupbox)
        self.label_1.setObjectName("label_1")
        self.gridLayout.addWidget(self.label_1, 0, 0, 1, 1)
        self.scalebar_color_button = gui.QgsColorButton(self.map_deco_groupbox)
        self.scalebar_color_button.setMinimumSize(QtCore.QSize(80, 16))
        self.scalebar_color_button.setMaximumSize(QtCore.QSize(80, 16777215))
        self.scalebar_color_button.setFocusPolicy(QtCore.Qt.NoFocus)
        self.scalebar_color_button.setObjectName("scalebar_color_button")
        self.gridLayout.addWidget(self.scalebar_color_button, 1, 1, 1, 1)
        self.reset_color_north_arrow_btn = QtWidgets.QPushButton(self.map_deco_groupbox)
        self.reset_color_north_arrow_btn.setMaximumSize(QtCore.QSize(27, 16777215))
        self.reset_color_north_arrow_btn.setFocusPolicy(QtCore.Qt.NoFocus)
        self.reset_color_north_arrow_btn.setText("")
        self.reset_color_north_arrow_btn.setObjectName("reset_color_north_arrow_btn")
        self.gridLayout.addWidget(self.reset_color_north_arrow_btn, 0, 2, 1, 1)
        self.reset_color_scale_bar_btn = QtWidgets.QPushButton(self.map_deco_groupbox)
        self.reset_color_scale_bar_btn.setMaximumSize(QtCore.QSize(27, 16777215))
        self.reset_color_scale_bar_btn.setFocusPolicy(QtCore.Qt.NoFocus)
        self.reset_color_scale_bar_btn.setText("")
        self.reset_color_scale_bar_btn.setObjectName("reset_color_scale_bar_btn")
        self.gridLayout.addWidget(self.reset_color_scale_bar_btn, 1, 2, 1, 1)
        self.verticalLayout.addWidget(self.map_deco_groupbox)
        self.mission_style_group_box = QtWidgets.QGroupBox(StylesWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.mission_style_group_box.sizePolicy().hasHeightForWidth())
        self.mission_style_group_box.setSizePolicy(sizePolicy)
        self.mission_style_group_box.setStyleSheet("QGroupBox {\n"
"    border: 1px solid silver;\n"
"    border-radius: 6px;\n"
"    margin-top: 6px;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 7px;\n"
"    padding: 0px 5px 0px 5px;\n"
"}")
        self.mission_style_group_box.setObjectName("mission_style_group_box")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.mission_style_group_box)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.missions_combo_box = QtWidgets.QComboBox(self.mission_style_group_box)
        self.missions_combo_box.setObjectName("missions_combo_box")
        self.gridLayout_2.addWidget(self.missions_combo_box, 0, 0, 1, 3)
        self.missions_color_button = gui.QgsColorButton(self.mission_style_group_box)
        self.missions_color_button.setMinimumSize(QtCore.QSize(80, 16))
        self.missions_color_button.setMaximumSize(QtCore.QSize(80, 16777215))
        self.missions_color_button.setFocusPolicy(QtCore.Qt.NoFocus)
        self.missions_color_button.setObjectName("missions_color_button")
        self.gridLayout_2.addWidget(self.missions_color_button, 1, 1, 1, 1)
        self.reset_color_missions_btn = QtWidgets.QPushButton(self.mission_style_group_box)
        self.reset_color_missions_btn.setMaximumSize(QtCore.QSize(27, 16777215))
        self.reset_color_missions_btn.setFocusPolicy(QtCore.Qt.NoFocus)
        self.reset_color_missions_btn.setText("")
        self.reset_color_missions_btn.setObjectName("reset_color_missions_btn")
        self.gridLayout_2.addWidget(self.reset_color_missions_btn, 1, 2, 1, 1)
        self.label = QtWidgets.QLabel(self.mission_style_group_box)
        self.label.setObjectName("label")
        self.gridLayout_2.addWidget(self.label, 1, 0, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.mission_style_group_box)
        self.label_3.setObjectName("label_3")
        self.gridLayout_2.addWidget(self.label_3, 2, 0, 1, 1)
        self.start_end_color_button = gui.QgsColorButton(self.mission_style_group_box)
        self.start_end_color_button.setMinimumSize(QtCore.QSize(80, 16))
        self.start_end_color_button.setMaximumSize(QtCore.QSize(80, 16777215))
        self.start_end_color_button.setFocusPolicy(QtCore.Qt.NoFocus)
        self.start_end_color_button.setObjectName("start_end_color_button")
        self.gridLayout_2.addWidget(self.start_end_color_button, 2, 1, 1, 1)
        self.reset_color_start_end_btn = QtWidgets.QPushButton(self.mission_style_group_box)
        self.reset_color_start_end_btn.setMaximumSize(QtCore.QSize(27, 16777215))
        self.reset_color_start_end_btn.setFocusPolicy(QtCore.Qt.NoFocus)
        self.reset_color_start_end_btn.setText("")
        self.reset_color_start_end_btn.setObjectName("reset_color_start_end_btn")
        self.gridLayout_2.addWidget(self.reset_color_start_end_btn, 2, 2, 1, 1)
        self.verticalLayout.addWidget(self.mission_style_group_box)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)

        self.retranslateUi(StylesWidget)
        QtCore.QMetaObject.connectSlotsByName(StylesWidget)

    def retranslateUi(self, StylesWidget):
        _translate = QtCore.QCoreApplication.translate
        StylesWidget.setWindowTitle(_translate("StylesWidget", "Form"))
        self.map_deco_groupbox.setTitle(_translate("StylesWidget", "Map Decorations"))
        self.label_2.setText(_translate("StylesWidget", "Scale Bar"))
        self.label_1.setText(_translate("StylesWidget", "North Arrow"))
        self.mission_style_group_box.setTitle(_translate("StylesWidget", "Mission Style"))
        self.label.setText(_translate("StylesWidget", "Track"))
        self.label_3.setText(_translate("StylesWidget", "Start & End Markers"))
from qgis import gui


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    StylesWidget = QtWidgets.QWidget()
    ui = Ui_StylesWidget()
    ui.setupUi(StylesWidget)
    StylesWidget.show()
    sys.exit(app.exec_())
