# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_dataoutputconnection.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_DataOutputConnectionWidget(object):
    def setupUi(self, DataOutputConnectionWidget):
        DataOutputConnectionWidget.setObjectName("DataOutputConnectionWidget")
        DataOutputConnectionWidget.resize(498, 228)
        self.verticalLayout = QtWidgets.QVBoxLayout(DataOutputConnectionWidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.groupBox = QtWidgets.QGroupBox(DataOutputConnectionWidget)
        self.groupBox.setStyleSheet("QGroupBox {\n"
"    border: 1px solid silver;\n"
"    border-radius: 6px;\n"
"    margin-top: 6px;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 7px;\n"
"    padding: 0px 5px 0px 5px;\n"
"}")
        self.groupBox.setObjectName("groupBox")
        self.gridLayout = QtWidgets.QGridLayout(self.groupBox)
        self.gridLayout.setObjectName("gridLayout")
        self.device_lineEdit = QtWidgets.QLineEdit(self.groupBox)
        self.device_lineEdit.setEnabled(True)
        self.device_lineEdit.setObjectName("device_lineEdit")
        self.gridLayout.addWidget(self.device_lineEdit, 5, 1, 1, 2)
        self.protocol_label = QtWidgets.QLabel(self.groupBox)
        self.protocol_label.setObjectName("protocol_label")
        self.gridLayout.addWidget(self.protocol_label, 4, 3, 1, 1)
        self.ggaport_lineEdit = QtWidgets.QLineEdit(self.groupBox)
        self.ggaport_lineEdit.setObjectName("ggaport_lineEdit")
        self.gridLayout.addWidget(self.ggaport_lineEdit, 6, 4, 1, 1)
        self.ggaport_label = QtWidgets.QLabel(self.groupBox)
        self.ggaport_label.setObjectName("ggaport_label")
        self.gridLayout.addWidget(self.ggaport_label, 6, 3, 1, 1)
        self.baudrate_label = QtWidgets.QLabel(self.groupBox)
        self.baudrate_label.setEnabled(True)
        self.baudrate_label.setObjectName("baudrate_label")
        self.gridLayout.addWidget(self.baudrate_label, 6, 0, 1, 1)
        self.ip_lineEdit = QtWidgets.QLineEdit(self.groupBox)
        self.ip_lineEdit.setObjectName("ip_lineEdit")
        self.gridLayout.addWidget(self.ip_lineEdit, 5, 4, 1, 1)
        self.ethernet_radioButton = QtWidgets.QRadioButton(self.groupBox)
        self.ethernet_radioButton.setObjectName("ethernet_radioButton")
        self.gridLayout.addWidget(self.ethernet_radioButton, 3, 3, 1, 2)
        self.ip_label = QtWidgets.QLabel(self.groupBox)
        self.ip_label.setObjectName("ip_label")
        self.gridLayout.addWidget(self.ip_label, 5, 3, 1, 1)
        self.position_comboBox = QtWidgets.QComboBox(self.groupBox)
        self.position_comboBox.setObjectName("position_comboBox")
        self.position_comboBox.addItem("")
        self.position_comboBox.addItem("")
        self.gridLayout.addWidget(self.position_comboBox, 1, 0, 1, 1)
        self.baudrate_comboBox = QtWidgets.QComboBox(self.groupBox)
        self.baudrate_comboBox.setEnabled(True)
        self.baudrate_comboBox.setObjectName("baudrate_comboBox")
        self.baudrate_comboBox.addItem("")
        self.baudrate_comboBox.addItem("")
        self.baudrate_comboBox.addItem("")
        self.baudrate_comboBox.addItem("")
        self.baudrate_comboBox.addItem("")
        self.baudrate_comboBox.addItem("")
        self.baudrate_comboBox.addItem("")
        self.baudrate_comboBox.addItem("")
        self.baudrate_comboBox.addItem("")
        self.baudrate_comboBox.addItem("")
        self.baudrate_comboBox.addItem("")
        self.gridLayout.addWidget(self.baudrate_comboBox, 6, 1, 1, 2)
        self.serial_radioButton = QtWidgets.QRadioButton(self.groupBox)
        self.serial_radioButton.setEnabled(True)
        self.serial_radioButton.setChecked(True)
        self.serial_radioButton.setObjectName("serial_radioButton")
        self.gridLayout.addWidget(self.serial_radioButton, 3, 0, 1, 3)
        self.protocol_comboBox = QtWidgets.QComboBox(self.groupBox)
        self.protocol_comboBox.setObjectName("protocol_comboBox")
        self.protocol_comboBox.addItem("")
        self.protocol_comboBox.addItem("")
        self.gridLayout.addWidget(self.protocol_comboBox, 4, 4, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(5, 5, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.gridLayout.addItem(spacerItem, 0, 0, 1, 1)
        self.device_label = QtWidgets.QLabel(self.groupBox)
        self.device_label.setEnabled(True)
        self.device_label.setObjectName("device_label")
        self.gridLayout.addWidget(self.device_label, 5, 0, 1, 1)
        self.remove_horizontalLayout = QtWidgets.QHBoxLayout()
        self.remove_horizontalLayout.setObjectName("remove_horizontalLayout")
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.remove_horizontalLayout.addItem(spacerItem1)
        self.remove_toolButton = QtWidgets.QToolButton(self.groupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.remove_toolButton.sizePolicy().hasHeightForWidth())
        self.remove_toolButton.setSizePolicy(sizePolicy)
        self.remove_toolButton.setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/resources/mActionDeleteSelected.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.remove_toolButton.setIcon(icon)
        self.remove_toolButton.setAutoRaise(True)
        self.remove_toolButton.setObjectName("remove_toolButton")
        self.remove_horizontalLayout.addWidget(self.remove_toolButton)
        self.gridLayout.addLayout(self.remove_horizontalLayout, 1, 1, 1, 4)
        self.verticalLayout.addWidget(self.groupBox)
        spacerItem2 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem2)

        self.retranslateUi(DataOutputConnectionWidget)
        QtCore.QMetaObject.connectSlotsByName(DataOutputConnectionWidget)
        DataOutputConnectionWidget.setTabOrder(self.position_comboBox, self.serial_radioButton)
        DataOutputConnectionWidget.setTabOrder(self.serial_radioButton, self.ethernet_radioButton)
        DataOutputConnectionWidget.setTabOrder(self.ethernet_radioButton, self.device_lineEdit)
        DataOutputConnectionWidget.setTabOrder(self.device_lineEdit, self.baudrate_comboBox)
        DataOutputConnectionWidget.setTabOrder(self.baudrate_comboBox, self.protocol_comboBox)
        DataOutputConnectionWidget.setTabOrder(self.protocol_comboBox, self.ip_lineEdit)
        DataOutputConnectionWidget.setTabOrder(self.ip_lineEdit, self.ggaport_lineEdit)

    def retranslateUi(self, DataOutputConnectionWidget):
        _translate = QtCore.QCoreApplication.translate
        DataOutputConnectionWidget.setWindowTitle(_translate("DataOutputConnectionWidget", "Form"))
        self.groupBox.setTitle(_translate("DataOutputConnectionWidget", "GroupBox"))
        self.protocol_label.setText(_translate("DataOutputConnectionWidget", "Protocol:"))
        self.ggaport_label.setText(_translate("DataOutputConnectionWidget", "Port:"))
        self.baudrate_label.setText(_translate("DataOutputConnectionWidget", "Baud rate:"))
        self.ethernet_radioButton.setText(_translate("DataOutputConnectionWidget", "Ethernet"))
        self.ip_label.setText(_translate("DataOutputConnectionWidget", "IP:"))
        self.position_comboBox.setItemText(0, _translate("DataOutputConnectionWidget", "AUV Position"))
        self.position_comboBox.setItemText(1, _translate("DataOutputConnectionWidget", "USBL Position"))
        self.baudrate_comboBox.setItemText(0, _translate("DataOutputConnectionWidget", "1200"))
        self.baudrate_comboBox.setItemText(1, _translate("DataOutputConnectionWidget", "2400"))
        self.baudrate_comboBox.setItemText(2, _translate("DataOutputConnectionWidget", "4800"))
        self.baudrate_comboBox.setItemText(3, _translate("DataOutputConnectionWidget", "9600"))
        self.baudrate_comboBox.setItemText(4, _translate("DataOutputConnectionWidget", "14400"))
        self.baudrate_comboBox.setItemText(5, _translate("DataOutputConnectionWidget", "19200"))
        self.baudrate_comboBox.setItemText(6, _translate("DataOutputConnectionWidget", "28800"))
        self.baudrate_comboBox.setItemText(7, _translate("DataOutputConnectionWidget", "38400"))
        self.baudrate_comboBox.setItemText(8, _translate("DataOutputConnectionWidget", "56000"))
        self.baudrate_comboBox.setItemText(9, _translate("DataOutputConnectionWidget", "57600"))
        self.baudrate_comboBox.setItemText(10, _translate("DataOutputConnectionWidget", "115200"))
        self.serial_radioButton.setText(_translate("DataOutputConnectionWidget", "Serial Port"))
        self.protocol_comboBox.setItemText(0, _translate("DataOutputConnectionWidget", "TCP"))
        self.protocol_comboBox.setItemText(1, _translate("DataOutputConnectionWidget", "UDP"))
        self.device_label.setText(_translate("DataOutputConnectionWidget", "Device:"))
from iquaview_lib import resources_rc


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    DataOutputConnectionWidget = QtWidgets.QWidget()
    ui = Ui_DataOutputConnectionWidget()
    ui.setupUi(DataOutputConnectionWidget)
    DataOutputConnectionWidget.show()
    sys.exit(app.exec_())
