# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_track_treewidget.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_TrackTreeWidget(object):
    def setupUi(self, TrackTreeWidget):
        TrackTreeWidget.setObjectName("TrackTreeWidget")
        TrackTreeWidget.resize(400, 300)
        self.verticalLayout = QtWidgets.QVBoxLayout(TrackTreeWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.options_widget = QtWidgets.QWidget(TrackTreeWidget)
        self.options_widget.setObjectName("options_widget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.options_widget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout.addWidget(self.options_widget)
        self.treeView = QtWidgets.QTreeView(TrackTreeWidget)
        self.treeView.setObjectName("treeView")
        self.verticalLayout.addWidget(self.treeView)

        self.retranslateUi(TrackTreeWidget)
        QtCore.QMetaObject.connectSlotsByName(TrackTreeWidget)

    def retranslateUi(self, TrackTreeWidget):
        _translate = QtCore.QCoreApplication.translate
        TrackTreeWidget.setWindowTitle(_translate("TrackTreeWidget", "Tracks"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    TrackTreeWidget = QtWidgets.QWidget()
    ui = Ui_TrackTreeWidget()
    ui.setupUi(TrackTreeWidget)
    TrackTreeWidget.show()
    sys.exit(app.exec_())

