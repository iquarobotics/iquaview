# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_polygontemplatewidget.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_PolygonTemplateWidget(object):
    def setupUi(self, PolygonTemplateWidget):
        PolygonTemplateWidget.setObjectName("PolygonTemplateWidget")
        PolygonTemplateWidget.resize(246, 447)
        self.verticalLayout = QtWidgets.QVBoxLayout(PolygonTemplateWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.mission_area_widget = MissionAreaWidget(PolygonTemplateWidget)
        self.mission_area_widget.setObjectName("mission_area_widget")
        self.verticalLayout.addWidget(self.mission_area_widget)
        self.InitialPointgroupBox = QtWidgets.QGroupBox(PolygonTemplateWidget)
        self.InitialPointgroupBox.setMinimumSize(QtCore.QSize(0, 0))
        self.InitialPointgroupBox.setMaximumSize(QtCore.QSize(16777215, 75))
        self.InitialPointgroupBox.setStyleSheet("QGroupBox {\n"
"    border: 1px solid silver;\n"
"    border-radius: 6px;\n"
"    margin-top: 6px;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 7px;\n"
"    padding: 0px 5px 0px 5px;\n"
"}")
        self.InitialPointgroupBox.setObjectName("InitialPointgroupBox")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.InitialPointgroupBox)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.initial_point_label = QtWidgets.QLabel(self.InitialPointgroupBox)
        self.initial_point_label.setObjectName("initial_point_label")
        self.gridLayout_2.addWidget(self.initial_point_label, 1, 0, 1, 1)
        self.number_points_label = QtWidgets.QLabel(self.InitialPointgroupBox)
        self.number_points_label.setObjectName("number_points_label")
        self.gridLayout_2.addWidget(self.number_points_label, 0, 0, 1, 1)
        self.number_points_spinBox = QtWidgets.QSpinBox(self.InitialPointgroupBox)
        self.number_points_spinBox.setMinimum(3)
        self.number_points_spinBox.setMaximum(200)
        self.number_points_spinBox.setProperty("value", 20)
        self.number_points_spinBox.setObjectName("number_points_spinBox")
        self.gridLayout_2.addWidget(self.number_points_spinBox, 0, 1, 1, 1)
        self.initial_point_spinBox = QtWidgets.QSpinBox(self.InitialPointgroupBox)
        self.initial_point_spinBox.setWrapping(True)
        self.initial_point_spinBox.setMinimum(1)
        self.initial_point_spinBox.setMaximum(200)
        self.initial_point_spinBox.setProperty("value", 1)
        self.initial_point_spinBox.setObjectName("initial_point_spinBox")
        self.gridLayout_2.addWidget(self.initial_point_spinBox, 1, 1, 1, 1)
        self.verticalLayout.addWidget(self.InitialPointgroupBox)
        self.transectsGroupBox = QtWidgets.QGroupBox(PolygonTemplateWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.transectsGroupBox.sizePolicy().hasHeightForWidth())
        self.transectsGroupBox.setSizePolicy(sizePolicy)
        self.transectsGroupBox.setMaximumSize(QtCore.QSize(16777215, 390))
        self.transectsGroupBox.setStyleSheet("QGroupBox {\n"
"    border: 1px solid silver;\n"
"    border-radius: 6px;\n"
"    margin-top: 6px;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 7px;\n"
"    padding: 0px 5px 0px 5px;\n"
"}")
        self.transectsGroupBox.setObjectName("transectsGroupBox")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.transectsGroupBox)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.heave_mode_widget = HeaveModeWidget(self.transectsGroupBox)
        self.heave_mode_widget.setObjectName("heave_mode_widget")
        self.verticalLayout_2.addWidget(self.heave_mode_widget)
        self.line = QtWidgets.QFrame(self.transectsGroupBox)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.verticalLayout_2.addWidget(self.line)
        self.values_gridLayout = QtWidgets.QGridLayout()
        self.values_gridLayout.setObjectName("values_gridLayout")
        self.tolerance_label = QtWidgets.QLabel(self.transectsGroupBox)
        self.tolerance_label.setObjectName("tolerance_label")
        self.values_gridLayout.addWidget(self.tolerance_label, 2, 0, 1, 1)
        self.no_altitude_label = QtWidgets.QLabel(self.transectsGroupBox)
        self.no_altitude_label.setObjectName("no_altitude_label")
        self.values_gridLayout.addWidget(self.no_altitude_label, 0, 0, 1, 1)
        self.speed_label = QtWidgets.QLabel(self.transectsGroupBox)
        self.speed_label.setObjectName("speed_label")
        self.values_gridLayout.addWidget(self.speed_label, 1, 0, 1, 1)
        self.speed_doubleSpinBox = QtWidgets.QDoubleSpinBox(self.transectsGroupBox)
        self.speed_doubleSpinBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedKingdom))
        self.speed_doubleSpinBox.setSingleStep(0.1)
        self.speed_doubleSpinBox.setProperty("value", 0.5)
        self.speed_doubleSpinBox.setObjectName("speed_doubleSpinBox")
        self.values_gridLayout.addWidget(self.speed_doubleSpinBox, 1, 1, 1, 1)
        self.xy_tolerance_doubleSpinBox = QtWidgets.QDoubleSpinBox(self.transectsGroupBox)
        self.xy_tolerance_doubleSpinBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedKingdom))
        self.xy_tolerance_doubleSpinBox.setMaximum(100000.0)
        self.xy_tolerance_doubleSpinBox.setSingleStep(0.5)
        self.xy_tolerance_doubleSpinBox.setProperty("value", 2.0)
        self.xy_tolerance_doubleSpinBox.setObjectName("xy_tolerance_doubleSpinBox")
        self.values_gridLayout.addWidget(self.xy_tolerance_doubleSpinBox, 2, 1, 1, 1)
        self.no_altitude_comboBox = QtWidgets.QComboBox(self.transectsGroupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.no_altitude_comboBox.sizePolicy().hasHeightForWidth())
        self.no_altitude_comboBox.setSizePolicy(sizePolicy)
        self.no_altitude_comboBox.setObjectName("no_altitude_comboBox")
        self.no_altitude_comboBox.addItem("")
        self.no_altitude_comboBox.addItem("")
        self.values_gridLayout.addWidget(self.no_altitude_comboBox, 0, 1, 1, 1)
        self.verticalLayout_2.addLayout(self.values_gridLayout)
        self.verticalLayout.addWidget(self.transectsGroupBox)

        self.retranslateUi(PolygonTemplateWidget)
        QtCore.QMetaObject.connectSlotsByName(PolygonTemplateWidget)
        PolygonTemplateWidget.setTabOrder(self.number_points_spinBox, self.initial_point_spinBox)
        PolygonTemplateWidget.setTabOrder(self.initial_point_spinBox, self.no_altitude_comboBox)
        PolygonTemplateWidget.setTabOrder(self.no_altitude_comboBox, self.speed_doubleSpinBox)
        PolygonTemplateWidget.setTabOrder(self.speed_doubleSpinBox, self.xy_tolerance_doubleSpinBox)

    def retranslateUi(self, PolygonTemplateWidget):
        _translate = QtCore.QCoreApplication.translate
        PolygonTemplateWidget.setWindowTitle(_translate("PolygonTemplateWidget", "PolygonTemplate"))
        self.InitialPointgroupBox.setTitle(_translate("PolygonTemplateWidget", "Define Polygon"))
        self.initial_point_label.setText(_translate("PolygonTemplateWidget", "Initial Point:"))
        self.number_points_label.setText(_translate("PolygonTemplateWidget", "Number of points"))
        self.transectsGroupBox.setTitle(_translate("PolygonTemplateWidget", "Transects"))
        self.tolerance_label.setText(_translate("PolygonTemplateWidget", "Tolerance XY (m):"))
        self.no_altitude_label.setText(_translate("PolygonTemplateWidget", "No Altitude:"))
        self.speed_label.setText(_translate("PolygonTemplateWidget", "Speed:"))
        self.no_altitude_comboBox.setItemText(0, _translate("PolygonTemplateWidget", "Go up"))
        self.no_altitude_comboBox.setItemText(1, _translate("PolygonTemplateWidget", "Ignore"))
from iquaview.src.mission.missionedition.heavemodewidget import HeaveModeWidget
from iquaview.src.mission.missionareawidget import MissionAreaWidget


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    PolygonTemplateWidget = QtWidgets.QWidget()
    ui = Ui_PolygonTemplateWidget()
    ui.setupUi(PolygonTemplateWidget)
    PolygonTemplateWidget.show()
    sys.exit(app.exec_())
