# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_mission_info.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_missionInfo(object):
    def setupUi(self, missionInfo):
        missionInfo.setObjectName("missionInfo")
        missionInfo.resize(317, 158)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(missionInfo.sizePolicy().hasHeightForWidth())
        missionInfo.setSizePolicy(sizePolicy)
        self.gridLayout = QtWidgets.QGridLayout(missionInfo)
        self.gridLayout.setObjectName("gridLayout")
        self.total_distance_label = QtWidgets.QLabel(missionInfo)
        self.total_distance_label.setObjectName("total_distance_label")
        self.gridLayout.addWidget(self.total_distance_label, 3, 0, 1, 1)
        self.last_waypoint_onsurface = QtWidgets.QLabel(missionInfo)
        self.last_waypoint_onsurface.setObjectName("last_waypoint_onsurface")
        self.gridLayout.addWidget(self.last_waypoint_onsurface, 2, 1, 1, 1)
        self.first_waypoint_onsurface = QtWidgets.QLabel(missionInfo)
        self.first_waypoint_onsurface.setObjectName("first_waypoint_onsurface")
        self.gridLayout.addWidget(self.first_waypoint_onsurface, 1, 1, 1, 1)
        self.first_waypoint_label = QtWidgets.QLabel(missionInfo)
        self.first_waypoint_label.setObjectName("first_waypoint_label")
        self.gridLayout.addWidget(self.first_waypoint_label, 1, 0, 1, 1)
        self.estimated_time = QtWidgets.QLabel(missionInfo)
        self.estimated_time.setObjectName("estimated_time")
        self.gridLayout.addWidget(self.estimated_time, 4, 1, 1, 1)
        self.n_waypoints_label = QtWidgets.QLabel(missionInfo)
        self.n_waypoints_label.setObjectName("n_waypoints_label")
        self.gridLayout.addWidget(self.n_waypoints_label, 0, 0, 1, 1)
        self.estimated_time_label = QtWidgets.QLabel(missionInfo)
        self.estimated_time_label.setObjectName("estimated_time_label")
        self.gridLayout.addWidget(self.estimated_time_label, 4, 0, 1, 1)
        self.n_waypoints = QtWidgets.QLabel(missionInfo)
        self.n_waypoints.setObjectName("n_waypoints")
        self.gridLayout.addWidget(self.n_waypoints, 0, 1, 1, 1)
        self.last_waypoint_label = QtWidgets.QLabel(missionInfo)
        self.last_waypoint_label.setObjectName("last_waypoint_label")
        self.gridLayout.addWidget(self.last_waypoint_label, 2, 0, 1, 1)
        self.total_distance = QtWidgets.QLabel(missionInfo)
        self.total_distance.setObjectName("total_distance")
        self.gridLayout.addWidget(self.total_distance, 3, 1, 1, 1)
        self.show_table_pushButton = QtWidgets.QPushButton(missionInfo)
        self.show_table_pushButton.setObjectName("show_table_pushButton")
        self.gridLayout.addWidget(self.show_table_pushButton, 5, 0, 1, 2)

        self.retranslateUi(missionInfo)
        QtCore.QMetaObject.connectSlotsByName(missionInfo)

    def retranslateUi(self, missionInfo):
        _translate = QtCore.QCoreApplication.translate
        missionInfo.setWindowTitle(_translate("missionInfo", "Form"))
        self.total_distance_label.setText(_translate("missionInfo", "Total distance:"))
        self.last_waypoint_onsurface.setText(_translate("missionInfo", "-"))
        self.first_waypoint_onsurface.setText(_translate("missionInfo", "-"))
        self.first_waypoint_label.setText(_translate("missionInfo", "First waypoint on surface:"))
        self.estimated_time.setText(_translate("missionInfo", "-"))
        self.n_waypoints_label.setText(_translate("missionInfo", "No. of waypoints:"))
        self.estimated_time_label.setText(_translate("missionInfo", "Estimated total time:"))
        self.n_waypoints.setText(_translate("missionInfo", "-"))
        self.last_waypoint_label.setText(_translate("missionInfo", "Last waypoint on surface:"))
        self.total_distance.setText(_translate("missionInfo", "-"))
        self.show_table_pushButton.setText(_translate("missionInfo", "Show table of waypoints"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    missionInfo = QtWidgets.QWidget()
    ui = Ui_missionInfo()
    ui.setupUi(missionInfo)
    missionInfo.show()
    sys.exit(app.exec_())

