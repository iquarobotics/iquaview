# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_virtual_cage.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_VirtualCage(object):
    def setupUi(self, VirtualCage):
        VirtualCage.setObjectName("VirtualCage")
        VirtualCage.resize(222, 100)
        self.gridLayout = QtWidgets.QGridLayout(VirtualCage)
        self.gridLayout.setObjectName("gridLayout")
        self.ned_lat_label = QtWidgets.QLabel(VirtualCage)
        self.ned_lat_label.setObjectName("ned_lat_label")
        self.gridLayout.addWidget(self.ned_lat_label, 2, 0, 1, 1)
        self.ned_lon_label = QtWidgets.QLabel(VirtualCage)
        self.ned_lon_label.setObjectName("ned_lon_label")
        self.gridLayout.addWidget(self.ned_lon_label, 3, 0, 1, 1)
        self.radius_label = QtWidgets.QLabel(VirtualCage)
        self.radius_label.setObjectName("radius_label")
        self.gridLayout.addWidget(self.radius_label, 1, 0, 1, 1)
        self.spinBox = QtWidgets.QSpinBox(VirtualCage)
        self.spinBox.setMaximum(1000000)
        self.spinBox.setSingleStep(10)
        self.spinBox.setObjectName("spinBox")
        self.gridLayout.addWidget(self.spinBox, 1, 2, 1, 1)
        self.lat_horizontalLayout = QtWidgets.QHBoxLayout()
        self.lat_horizontalLayout.setObjectName("lat_horizontalLayout")
        self.lat_value_label = QtWidgets.QLabel(VirtualCage)
        self.lat_value_label.setObjectName("lat_value_label")
        self.lat_horizontalLayout.addWidget(self.lat_value_label)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.lat_horizontalLayout.addItem(spacerItem)
        self.gridLayout.addLayout(self.lat_horizontalLayout, 2, 2, 1, 1)
        self.lon_horizontalLayout = QtWidgets.QHBoxLayout()
        self.lon_horizontalLayout.setObjectName("lon_horizontalLayout")
        self.lon_value_label = QtWidgets.QLabel(VirtualCage)
        self.lon_value_label.setObjectName("lon_value_label")
        self.lon_horizontalLayout.addWidget(self.lon_value_label)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.lon_horizontalLayout.addItem(spacerItem1)
        self.gridLayout.addLayout(self.lon_horizontalLayout, 3, 2, 1, 1)

        self.retranslateUi(VirtualCage)
        QtCore.QMetaObject.connectSlotsByName(VirtualCage)

    def retranslateUi(self, VirtualCage):
        _translate = QtCore.QCoreApplication.translate
        VirtualCage.setWindowTitle(_translate("VirtualCage", "Form"))
        self.ned_lat_label.setText(_translate("VirtualCage", "NED Latitude:"))
        self.ned_lon_label.setText(_translate("VirtualCage", "NED Longitude:"))
        self.radius_label.setText(_translate("VirtualCage", "Radius (m):"))
        self.lat_value_label.setText(_translate("VirtualCage", "0"))
        self.lon_value_label.setText(_translate("VirtualCage", "0"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    VirtualCage = QtWidgets.QWidget()
    ui = Ui_VirtualCage()
    ui.setupUi(VirtualCage)
    VirtualCage.show()
    sys.exit(app.exec_())
