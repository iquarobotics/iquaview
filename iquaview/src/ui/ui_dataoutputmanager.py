# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_dataoutputmanager.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_DataOutputManagerWidget(object):
    def setupUi(self, DataOutputManagerWidget):
        DataOutputManagerWidget.setObjectName("DataOutputManagerWidget")
        DataOutputManagerWidget.resize(400, 300)
        self.gridLayout = QtWidgets.QGridLayout(DataOutputManagerWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.scrollArea = QtWidgets.QScrollArea(DataOutputManagerWidget)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 398, 269))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.gridLayout.addWidget(self.scrollArea, 1, 0, 1, 1)
        self.new_connection_toolButton = QtWidgets.QToolButton(DataOutputManagerWidget)
        self.new_connection_toolButton.setEnabled(True)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.new_connection_toolButton.sizePolicy().hasHeightForWidth())
        self.new_connection_toolButton.setSizePolicy(sizePolicy)
        self.new_connection_toolButton.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.new_connection_toolButton.setLayoutDirection(QtCore.Qt.LeftToRight)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/resources/mActionAdd.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.new_connection_toolButton.setIcon(icon)
        self.new_connection_toolButton.setAutoRaise(True)
        self.new_connection_toolButton.setObjectName("new_connection_toolButton")
        self.gridLayout.addWidget(self.new_connection_toolButton, 0, 0, 1, 1)

        self.retranslateUi(DataOutputManagerWidget)
        QtCore.QMetaObject.connectSlotsByName(DataOutputManagerWidget)

    def retranslateUi(self, DataOutputManagerWidget):
        _translate = QtCore.QCoreApplication.translate
        DataOutputManagerWidget.setWindowTitle(_translate("DataOutputManagerWidget", "Form"))
        self.new_connection_toolButton.setToolTip(_translate("DataOutputManagerWidget", "Add connection"))
        self.new_connection_toolButton.setText(_translate("DataOutputManagerWidget", "Add Connection"))
from iquaview_lib import resources_rc


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    DataOutputManagerWidget = QtWidgets.QWidget()
    ui = Ui_DataOutputManagerWidget()
    ui.setupUi(DataOutputManagerWidget)
    DataOutputManagerWidget.show()
    sys.exit(app.exec_())
