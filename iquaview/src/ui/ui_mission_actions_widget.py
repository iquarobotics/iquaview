# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_mission_actions_widget.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MissionActionsWidget(object):
    def setupUi(self, MissionActionsWidget):
        MissionActionsWidget.setObjectName("MissionActionsWidget")
        MissionActionsWidget.resize(185, 99)
        self.gridLayout = QtWidgets.QGridLayout(MissionActionsWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.actions_label = QtWidgets.QLabel(MissionActionsWidget)
        self.actions_label.setMinimumSize(QtCore.QSize(119, 0))
        self.actions_label.setObjectName("actions_label")
        self.gridLayout.addWidget(self.actions_label, 1, 0, 1, 1)
        self.action_listWidget = ActionsListWidget(MissionActionsWidget)
        self.action_listWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.action_listWidget.setObjectName("action_listWidget")
        self.gridLayout.addWidget(self.action_listWidget, 2, 0, 1, 4)
        self.addAction_toolButton = QtWidgets.QToolButton(MissionActionsWidget)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/resources/mActionAdd.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.addAction_toolButton.setIcon(icon)
        self.addAction_toolButton.setAutoRaise(True)
        self.addAction_toolButton.setObjectName("addAction_toolButton")
        self.gridLayout.addWidget(self.addAction_toolButton, 1, 2, 1, 1)
        self.removeAction_toolButton = QtWidgets.QToolButton(MissionActionsWidget)
        self.removeAction_toolButton.setEnabled(False)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.removeAction_toolButton.sizePolicy().hasHeightForWidth())
        self.removeAction_toolButton.setSizePolicy(sizePolicy)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/resources/mActionDeleteSelected.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.removeAction_toolButton.setIcon(icon1)
        self.removeAction_toolButton.setAutoRaise(True)
        self.removeAction_toolButton.setObjectName("removeAction_toolButton")
        self.gridLayout.addWidget(self.removeAction_toolButton, 1, 3, 1, 1)

        self.retranslateUi(MissionActionsWidget)
        QtCore.QMetaObject.connectSlotsByName(MissionActionsWidget)
        MissionActionsWidget.setTabOrder(self.addAction_toolButton, self.removeAction_toolButton)
        MissionActionsWidget.setTabOrder(self.removeAction_toolButton, self.action_listWidget)

    def retranslateUi(self, MissionActionsWidget):
        _translate = QtCore.QCoreApplication.translate
        MissionActionsWidget.setWindowTitle(_translate("MissionActionsWidget", "Mission Actions"))
        self.actions_label.setText(_translate("MissionActionsWidget", "Actions to call"))
        self.addAction_toolButton.setToolTip(_translate("MissionActionsWidget", "Add action"))
        self.addAction_toolButton.setText(_translate("MissionActionsWidget", "+"))
        self.removeAction_toolButton.setToolTip(_translate("MissionActionsWidget", "Remove action"))
        self.removeAction_toolButton.setText(_translate("MissionActionsWidget", "-"))


from iquaview.src.mission.missionedition.actionslistwidget import ActionsListWidget
from iquaview_lib import resources_rc


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MissionActionsWidget = QtWidgets.QWidget()
    ui = Ui_MissionActionsWidget()
    ui.setupUi(MissionActionsWidget)
    MissionActionsWidget.show()
    sys.exit(app.exec_())
