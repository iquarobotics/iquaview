# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/ui_range_bearing.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_RangeBearing(object):
    def setupUi(self, RangeBearing):
        RangeBearing.setObjectName("RangeBearing")
        RangeBearing.resize(400, 300)
        self.gridLayout = QtWidgets.QGridLayout(RangeBearing)
        self.gridLayout.setObjectName("gridLayout")
        self.treeView = QtWidgets.QTreeView(RangeBearing)
        self.treeView.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.treeView.setDragEnabled(True)
        self.treeView.setDragDropMode(QtWidgets.QAbstractItemView.InternalMove)
        self.treeView.setSelectionMode(QtWidgets.QAbstractItemView.NoSelection)
        self.treeView.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectItems)
        self.treeView.setObjectName("treeView")
        self.treeView.header().setVisible(False)
        self.gridLayout.addWidget(self.treeView, 1, 0, 1, 2)
        self.comboBox = QtWidgets.QComboBox(RangeBearing)
        self.comboBox.setObjectName("comboBox")
        self.gridLayout.addWidget(self.comboBox, 0, 1, 1, 1)
        self.label = QtWidgets.QLabel(RangeBearing)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)

        self.retranslateUi(RangeBearing)
        QtCore.QMetaObject.connectSlotsByName(RangeBearing)

    def retranslateUi(self, RangeBearing):
        _translate = QtCore.QCoreApplication.translate
        RangeBearing.setWindowTitle(_translate("RangeBearing", "Form"))
        self.label.setText(_translate("RangeBearing", "Base track"))
