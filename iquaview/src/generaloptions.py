# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

""" General options widget for iquaview """

import logging

from PyQt5.QtCore import QSettings
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QWidget

from iquaview.src.ui.ui_general_options import Ui_GeneralOptionsWidget
from iquaview_lib.utils.coordinateconverter import CoordinateFormat

logger = logging.getLogger(__name__)


class GeneralOptionsWidget(QWidget, Ui_GeneralOptionsWidget):
    """
    Dialog to configure general app.config parameters.
    It includes  coordinate display and confirmation msg state
    """

    def __init__(self, config, parent=None):
        """
        Constructor
        :param config: iquaview configuration
        :type config: Config
        """
        super().__init__(parent)
        self.setupUi(self)
        self.setWindowTitle("Options")
        self.config = config
        self.volume_slider.valueChanged.connect(self.update_volume_icon)
        self.volume_toolButton.clicked.connect(self.mute)
        self.coordinate_comboBox.currentIndexChanged.connect(self.set_min_max_precision)
        self.reload()

    def reload(self):
        """ Reload configuration """
        settings = QSettings()
        confirmation = settings.value("General/confirmation_msg", True, type=bool)
        self.confirm_exit_checkBox.setChecked(confirmation)

        # volume
        volume = settings.value("General/volume", 50, type=int)
        self.volume_slider.setValue(volume)

        self.set_coordinate_configuration()

    def set_coordinate_configuration(self):
        """
        Set coordinate format, precision, directional suffix and coordinate symbols
        """
        coordinate_format = self.config.settings.get("coordinate_format", "degree")
        coordinate_precision = self.config.settings.get("coordinate_precision", 7)
        coordinate_directional_suffix = self.config.settings.get("coordinate_directional_suffix", True)
        coordinate_symbols = self.config.settings.get("coordinate_symbols", True)

        # back compatible
        if coordinate_format == "degree":
            coordinate_format = CoordinateFormat.DEGREE
        elif coordinate_format == "degree_minute":
            coordinate_format = CoordinateFormat.DEGREE_MINUTE
        elif coordinate_format == "degree_minute_second":
            coordinate_format = CoordinateFormat.DEGREE_MINUTE_SECOND
        self.coordinate_comboBox.setCurrentIndex(coordinate_format)

        self.set_min_max_precision()
        self.precision_spinBox.setValue(coordinate_precision)
        self.directiona_suffix_checkBox.setChecked(coordinate_directional_suffix)
        self.coordinate_symbols_checkBox.setChecked(coordinate_symbols)

    def set_min_max_precision(self) -> None:
        """
        change minimum and maximum value for precision spinbox
        """
        if self.coordinate_comboBox.currentIndex() == CoordinateFormat.DEGREE:
            self.precision_spinBox.setMinimum(CoordinateFormat.MIN_DECIMAL_DEGREE_PRECISION)
            self.precision_spinBox.setMaximum(CoordinateFormat.MAX_DECIMAL_DEGREE_PRECISION)
        elif self.coordinate_comboBox.currentIndex() == CoordinateFormat.DEGREE_MINUTE:
            self.precision_spinBox.setMinimum(CoordinateFormat.MIN_DECIMAL_DEGREE_MINUTE_PRECISION)
            self.precision_spinBox.setMaximum(CoordinateFormat.MAX_DECIMAL_DEGREE_MINUTE_PRECISION)
        elif self.coordinate_comboBox.currentIndex() == CoordinateFormat.DEGREE_MINUTE_SECOND:
            self.precision_spinBox.setMinimum(CoordinateFormat.MIN_DECIMAL_DEGREE_MINUTE_SECOND_PRECISION)
            self.precision_spinBox.setMaximum(CoordinateFormat.MAX_DECIMAL_DEGREE_MINUTE_SECOND_PRECISION)

    def update_volume_icon(self):
        if self.volume_slider.value() == 0:
            self.volume_toolButton.setIcon(QIcon(":resources/volume_off.svg"))
        elif self.volume_slider.value() < 50:
            self.volume_toolButton.setIcon(QIcon(":resources/volume_lower.svg"))
        else:
            self.volume_toolButton.setIcon(QIcon(":resources/volume_on.svg"))

    def mute(self):
        if self.volume_slider.value() != 0:
            self.volume_slider.setValue(0)
        else:
            self.volume_slider.setValue(50)

    def on_accept(self):
        """ Save parameters """

        # update config
        self.config.settings["coordinate_format"] = self.coordinate_comboBox.currentIndex()
        self.config.settings["coordinate_precision"] = self.precision_spinBox.value()
        self.config.settings["coordinate_directional_suffix"] = self.directiona_suffix_checkBox.isChecked()
        self.config.settings["coordinate_symbols"] = self.coordinate_symbols_checkBox.isChecked()
        self.config.save()

        settings = QSettings()
        settings.setValue("General/confirmation_msg", self.confirm_exit_checkBox.isChecked())

        # volume
        settings.setValue("General/volume", self.volume_slider.value())

        return True
