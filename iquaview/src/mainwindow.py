# -*- coding: utf-8 -*-
# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

""" Main window of the IQUAview application. """

# =============================================================================
# Stdlib imports
# =============================================================================
import logging
import math
import os
from typing import Union
from urllib.parse import unquote

# ==============================================================================
# Qt imports
# ==============================================================================
from PyQt5.QtCore import Qt, QFileInfo, qVersion, QSize, QFile, QEvent, QSettings, pyqtSignal, QUrl, QTimer
from PyQt5.QtGui import QIcon, QPixmap, QColor
from PyQt5.QtMultimedia import QSoundEffect
from PyQt5.QtWidgets import (QMainWindow,
                             QAction,
                             QDockWidget,
                             QFileDialog,
                             QMessageBox,
                             QSizePolicy,
                             QFrame,
                             QLabel,
                             QToolBar,
                             QDialog,
                             QToolButton,
                             QCheckBox)
# ==============================================================================
# Qgis imports
# ==============================================================================
from qgis.core import (QgsApplication,
                       QgsCoordinateReferenceSystem,
                       QgsProject,
                       QgsLayerTreeModel,
                       QgsVectorLayer,
                       QgsMapLayer,
                       QgsVectorFileWriter,
                       Qgis,
                       QgsWkbTypes,
                       QgsLayerTreeLayer,
                       QgsRectangle,
                       QgsPointXY,
                       QgsLayerTree,
                       QgsLayerTreeGroup)
from qgis.gui import (QgsLayerTreeMapCanvasBridge,
                      QgsLayerTreeView,
                      QgsMapToolPan,
                      QgsMapToolZoom,
                      QgsMessageBar,
                      QgsMessageBarItem,
                      QgsScaleWidget,
                      QgsDoubleSpinBox,
                      QgsProjectionSelectionDialog)

# ==============================================================================
# Local imports
# ==============================================================================
from iquaview.src import __version__
from iquaview.src import options, resources_qgis  # pylint: disable=unused-import
from iquaview.src.connection import auvprocesseswidget
from iquaview.src.mapsetup import (addlayers,
                                   badlayerhandler,
                                   menuprovider,
                                   pointfeaturedlg,
                                   movelandmarktool)
from iquaview.src.mapsetup.decoration import scalebar, northarrow
from iquaview.src.mapsetup.maptip import MapTip
from iquaview.src.mission.missionmodule import MissionModule
from iquaview.src.plugins.pluginmanager import PluginManager
from iquaview.src.range_bearing.rangebearing_widget import RangeBearingWidget
from iquaview.src.tools import measuretool
from iquaview.src.tools.coordinateconverterwidget import CoordinateConverterDialog
from iquaview.src.tools.profile_tool.profiletoolmodule import ProfileToolModule
from iquaview.src.tools.web_server.webserver import WebServer
from iquaview.src.tracks.tracktreewidget import TrackTreeWidget
from iquaview.src.ui import ui_mainwindow
from iquaview.src.vehicle import logwidget, auvpose
# ==============================================================================
# iquaview_lib imports
# ==============================================================================
from iquaview_lib import __version__ as iquaview_lib_version
from iquaview_lib.config import Config
from iquaview_lib.entity.entitiesmanager import EntitiesManager
from iquaview_lib.utils import busywidget
from iquaview_lib.utils.coordinateconverter import format_lat_lon
from iquaview_lib.utils.qgisutils import transform_point
from iquaview_lib.vehicle import vehicledata, vehicleinfo
from iquaview_lib.vessel import vessel
from iquaview_lib.xmlconfighandler.auvconfigcheckxml import ConfigCheckXML

logger = logging.getLogger(__name__)


class MainWindow(ui_mainwindow.Ui_MainWindow, QMainWindow):
    """
    Main window class.
    Sets up the main interface elements and triggers the corresponding actions according to user inputs.
    """
    project_loaded_signal = pyqtSignal()

    def __init__(self):
        """
        initialization

        """
        super().__init__()
        self.setupUi(self)
        self.setWindowTitle("Untitled Project - IQUAview")
        self.logger_value = 2
        self.is_test = False
        self.message_muted = False
        self.all_muted = False
        # Read configs
        self.config = Config()
        self.config.load()
        config_check_xml = ConfigCheckXML(self.config)
        if not config_check_xml.exists():
            config_check_xml.exec_()
        # vehicle info
        xml_filename = f"{os.path.abspath(os.path.expanduser(self.config.settings['last_auv_config_xml']))}"
        self.vehicle_info = vehicleinfo.VehicleInfo(xml_filename)
        logger.info(self.config.settings)
        logger.info(f"Qt {qVersion()}")
        logger.info(f"Qgis {Qgis.QGIS_VERSION}")
        logger.info(f"IQUAview version: {__version__}")
        logger.info(f"IQUAview lib version: {iquaview_lib_version}")

        # Helps render multiple layers faster.
        self.canvas.setParallelRenderingEnabled(True)
        # Preview jobs allow for asynchronous rendering of layers in the background.
        self.canvas.setPreviewJobsEnabled(True)

        # Initialization of objects
        self.auv_pose = None
        self.auvp_dw = None
        self.auv_on_wifi = False
        # dialogs
        self.options_dialog = None
        self.effect = QSoundEffect(self)
        self.effect.setSource(QUrl.fromLocalFile(":/resources/beeping.wav"))
        self.effect.setLoopCount(QSoundEffect.Infinite)
        self.update_sound_effect()

        # vehicle data
        self.vehicle_data = vehicledata.VehicleData(xml_filename, self.vehicle_info)
        # Track/entities
        self.entities_manager = EntitiesManager(self.config, self.canvas, self)
        # Menu bar
        self.project_menu = self.menubar.addMenu("Project")
        self.view_menu = self.menubar.addMenu("View")
        self.mission_menu = self.menubar.addMenu("Mission")
        self.tools_menu = self.menubar.addMenu("Tools")

        # Message log handler
        self.msg_log = QgsApplication.messageLog()
        self.msg_log.messageReceived.connect(self.msgbar_catcher)

        # Project handler
        self.proj = QgsProject.instance()
        self.proj.clear()
        self.proj.setFileName("")
        self.bad_layer_handler = badlayerhandler.BadLayerHandler(callback=self.failed_layers)
        self.proj.setBadLayerHandler(self.bad_layer_handler)

        self.create_project_menu()
        self.create_view_menu()
        self.create_tools_menu()

        # set priority to expand dock widgets
        # left/right dockwidgets have more priority than top/bottom dockwidgets on corners
        self.setCorner(Qt.TopLeftCorner, Qt.LeftDockWidgetArea)
        self.setCorner(Qt.BottomLeftCorner, Qt.LeftDockWidgetArea)
        self.setCorner(Qt.TopRightCorner, Qt.RightDockWidgetArea)
        self.setCorner(Qt.BottomRightCorner, Qt.RightDockWidgetArea)

        self.create_dock_widgets()

        # Layer tree view
        self.init_layer_treeview()
        # Track tree view
        self.init_track_treeview()
        # Range bearing view
        self.init_range_bearing()

        self.mission_module = MissionModule(
            mission_menu=self.mission_menu,
            config=self.config,
            vehicle_info=self.vehicle_info,
            vehicle_data=self.vehicle_data,
            proj=self.proj,
            canvas=self.canvas,
            view=self.view,
            msg_log=self.msg_log,
            parent=self
        )
        self.mission_ctrl = self.mission_module.get_mission_controller()
        self.vehicle_status = self.mission_module.get_vehicle_status()

        self.view.currentNode().nameChanged.connect(self.mission_module.set_mission_modified)
        self.root.visibilityChanged.connect(self.mission_module.change_mission_markers_visibility)

        self.add_dock_widgets()

        self.create_project_toolbar()
        self.create_view_toolbar()
        self.create_tools_toolbar()
        self.mission_toolbar = self.mission_module.get_toolbar()
        self.addToolBar(self.mission_toolbar)

        self.addToolBarBreak()
        self.create_connection_toolbar()

        # Message Bar
        self.msg_bar = QgsMessageBar(self.canvas)
        self.msg_bar.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.gridLayout.addWidget(self.msg_bar, 0, 0, Qt.AlignTop)

        self.vehicle_msg_bar = QgsMessageBar(self.canvas)
        self.vehicle_msg_bar.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.vehicle_msg_bar.widgetRemoved.connect(self.mute_sound_effect)
        self.gridLayout.addWidget(self.vehicle_msg_bar, 1, 0, Qt.AlignTop)

        # widget to show coordinates
        self.label_xy = QLabel()
        self.label_xy.setFrameStyle(QFrame.StyledPanel)
        self.label_xy.setMinimumWidth(400)
        self.label_xy.setAlignment(Qt.AlignCenter)
        self.status_bar.setSizeGripEnabled(False)
        self.status_bar.addPermanentWidget(self.label_xy, 0)

        # Setup scale widget
        self.scale_widget = QgsScaleWidget()
        self.scale_widget.setMapCanvas(self.canvas)
        self.rotate_widget = QgsDoubleSpinBox()
        self.rotate_widget.setSuffix("\u00b0")
        self.rotate_widget.setRange(-360, 360)
        self.rotate_widget.setSingleStep(5)
        self.status_bar.addPermanentWidget(self.scale_widget, 0)
        self.status_bar.addPermanentWidget(self.rotate_widget, 0)

        # setup projection widget
        self.projection_widget = QToolButton()
        self.projection_widget.setAutoRaise(True)
        self.projection_widget.setToolButtonStyle(Qt.ToolButtonTextBesideIcon)
        self.projection_widget.setObjectName("mOntheFlyProjectionStatusButton")
        self.projection_widget.setMaximumHeight(self.scale_widget.height())
        self.projection_widget.setIcon(QIcon(":/resources/mIconProjectionEnabled.svg"))
        self.projection_widget.setToolTip("CRS status - Click to open coordinate reference system dialog")
        self.projection_widget.clicked.connect(self.open_projection_dialog)
        self.status_bar.addPermanentWidget(self.projection_widget, 0)

        self.scale_bar = scalebar.ScaleBar(self.canvas, self.config)
        self.north_arrow = northarrow.NorthArrow(self.canvas, self.config)

        # Canvas event filter
        self.canvas.installEventFilter(self)

        # Set map tools
        self.tool_pan = QgsMapToolPan(self.canvas)
        self.tool_pan.setAction(self.pan_action)
        self.tool_zoom_in = QgsMapToolZoom(self.canvas, False)
        self.tool_zoom_in.setAction(self.zoom_in_action)
        self.tool_zoom_out = QgsMapToolZoom(self.canvas, True)
        self.tool_zoom_out.setAction(self.zoom_out_action)
        self.tool_measure_distance = measuretool.MeasureDistanceTool(self.canvas, self.msg_log)
        self.tool_measure_angle = measuretool.MeasureAngleTool(self.canvas, self.msg_log)
        self.tool_measure_area = measuretool.MeasureAreaTool(self.canvas, self.msg_log)
        self.tool_move_landmark = movelandmarktool.MoveLandmarkTool(self.canvas)

        self.point_feature = pointfeaturedlg.PointFeatureDlg(
            self.canvas,
            self.proj,
            self.config,
            pointfeaturedlg.LandmarkAction.ADD,
            self
        )

        self.point_feature.landmark_added.connect(self.add_map_layer)
        self.point_feature.landmark_removed.connect(self.remove_map_layer)
        self.point_feature.map_tool_change_signal.connect(self.reset_map_tool)
        self.point_feature.finish_add_landmark_signal.connect(self.finish_add_landmark)

        self.coordinate_converter_dlg = CoordinateConverterDialog(self.config, self)

        # Set signals for Project actions
        self.new_project_action.triggered.connect(self.clear_project)
        self.open_project_action.triggered.connect(self.open_project)
        self.save_project_action.triggered.connect(self.save_project)
        self.saveprojectas_action.triggered.connect(self.save_project_as)
        self.add_layer_action.triggered.connect(self.add_layer)
        self.add_landmark_point_action.triggered.connect(self.add_landmark_point)
        self.move_landmark_point_action.triggered.connect(self.move_landmark_point)
        self.options_action.triggered.connect(self.edit_general_options)
        self.styles_action.triggered.connect(self.edit_styles)
        self.quit_action.triggered.connect(self.close)

        self.zoom_in_action.triggered.connect(self.zoom_in)
        self.zoom_out_action.triggered.connect(self.zoom_out)
        self.pan_action.triggered.connect(self.pan)
        self.measure_distance_action.triggered.connect(self.measure_distance)
        self.measure_angle_action.triggered.connect(self.measure_angle)
        self.measure_area_action.triggered.connect(self.measure_area)
        self.scale_bar_action.triggered.connect(self.scale_bar_visibility)
        self.north_arrow_action.triggered.connect(self.north_arrow_visibility)

        # self.auv_processes_widget.cc.connection_failure.connect(self.connection_failed)
        self.auv_processes_widget.processchanged.connect(self.process_changed)

        self.coordinate_converter_action.triggered.connect(self.coordinate_converter_dlg.show)

        self.profile_tool_module = ProfileToolModule(self.canvas, self)
        self.tools_menu.addAction(self.profile_tool_module.profile_tool_action)
        self.tools_toolbar.addAction(self.profile_tool_module.profile_tool_action)

        self.canvas.xyCoordinates.connect(self.show_xy)
        self.canvas.scaleChanged.connect(self.show_scale)
        self.scale_widget.scaleChanged.connect(self.on_scale_changed)
        self.rotate_widget.valueChanged.connect(self.on_rotation_changed)

        # web server
        self.web_server = WebServer(self.config, self.vehicle_data, self.vehicle_info, self.mission_ctrl)
        # load entities after init_layer_treeview
        self.entities_manager.load_entities()
        # Start connection with vehicle
        self.auv_processes_widget.connect()

        self.auv_pose = auvpose.AUVPose(self.entities_manager,
                                        self.canvas,
                                        self.config,
                                        self.vehicle_info,
                                        self.vehicle_data,
                                        self.vehicle_status,
                                        self.msg_log,
                                        self.proj,
                                        self.menubar,
                                        self.mission_menu.menuAction(),
                                        self.mission_ctrl,
                                        self)
        self.web_server.register_data_source(self.auv_pose.auventity.web_server_register())
        self.auv_pose.auventity.canvasdrawer.position_orientation_signal.connect(self.web_server.callback_data)

        self.auv_pose.auv_wifi_connected_signal.connect(self.auv_wifi_connected)

        self.vessel = vessel.Vessel(self.entities_manager,
                                    self.canvas,
                                    self.config,
                                    self.menubar,
                                    self.auv_pose.vehicle_menu.menuAction(),
                                    self.mission_toolbar,
                                    self)
        self.web_server.register_data_source(self.vessel.vesselentity.web_server_register())
        self.vessel.vesselentity.canvasdrawer.position_orientation_signal.connect(self.web_server.callback_data)
        self.auv_pose.auventity.entity_save_signal.connect(
            lambda properties, ui: self.auv_processes_widget.vehicle_info_changed())
        self.create_visibility_actions()

        self.plugin_manager = PluginManager(proj=self.proj,
                                            canvas=self.canvas,
                                            config=self.config,
                                            vehicle_info=self.vehicle_info,
                                            vehicle_data=self.vehicle_data,
                                            vehicle_status=self.vehicle_status,
                                            menubar=self.menubar,
                                            view_menu_toolbar=self.view_menu_toolbar,
                                            entities_manager=self.entities_manager,
                                            parent=self)
        self.plugin_manager.init()
        self.auv_pose.timeout_widget.set_plugin_manager(self.plugin_manager)
        self.auv_pose.timeout_widget.set_cola2_status(self.auv_processes_widget.cola2status)

        # add the help menu at the end
        self.create_help_menu()

        # options
        self.options_dialog = options.OptionsDlg(self.config, self.vehicle_info, self.vehicle_data, self.canvas,
                                                 self.mission_ctrl, self.north_arrow, self.scale_bar, self.web_server,
                                                 self.plugin_manager, self)
        self.options_dialog.accepted_signal.connect(self.update_sound_effect)

        # Create menu provider
        self.menu_provider = menuprovider.MenuProvider(self.config,
                                                       self.view,
                                                       self.canvas,
                                                       self.proj,
                                                       self.mission_module,
                                                       self.auv_pose.mission_active,
                                                       self)
        self.view.setMenuProvider(self.menu_provider)

        self.auv_wifi_connected(False)
        self.layer_changed()

        self.set_decorations_visibility()

        self.setContextMenuPolicy(Qt.NoContextMenu)

        # Activate pan tool by default
        self.pan()

        # Init map tip class for showing tooltips of canvas layers
        self.map_tip = MapTip(self.canvas)
        logger.info(f"Loading last project: {self.config.settings['last_open_project']}")
        self.load_project(self.config.settings['last_open_project'])
        self.options_dialog.update_options(force_update=True)

    def create_project_menu(self) -> None:
        """
        Create the project menu
        """
        # Actions for project
        self.new_project_action = QAction(QIcon(":/resources/mActionFileNew.svg"), "New Project", self)
        self.new_project_action.setShortcut("Ctrl+n")
        self.open_project_action = QAction(QIcon(":/resources/mActionFileOpen.svg"), "Open Project...", self)
        self.open_project_action.setShortcut("Ctrl+o")
        self.save_project_action = QAction(QIcon(":/resources/mActionSave.svg"), "Save Project", self)
        self.save_project_action.setShortcut("Ctrl+s")
        self.saveprojectas_action = QAction(QIcon(":/resources/mActionSaveAs.svg"), "Save Project As...", self)
        self.saveprojectas_action.setShortcut("Ctrl+Shift+s")
        self.add_layer_action = QAction(QIcon(":/resources/mActionAddLayer.png"), "Add Layer...", self)
        self.add_landmark_point_action = QAction(QIcon(":/resources/mActionAddLandmarkPoint.svg"),
                                                 "Add Landmark Point",
                                                 self)
        self.add_landmark_point_action.setCheckable(True)
        self.move_landmark_point_action = QAction(QIcon(":/resources/mActionMoveLandmarkPoint.svg"),
                                                  "Move Landmark Point", self)
        self.move_landmark_point_action.setCheckable(True)
        self.options_action = QAction(QIcon(":/resources/mActionOptions.svg"), "Options...", self)
        self.styles_action = QAction(QIcon(":/resources/mActionStyles.svg"), "Styles...", self)
        self.quit_action = QAction(QIcon(":/resources/mActionFileExit.png"), "Quit", self)
        self.quit_action.setShortcut("Ctrl+q")

        self.project_menu.addActions([self.new_project_action,
                                      self.open_project_action,
                                      self.save_project_action,
                                      self.saveprojectas_action,
                                      self.project_menu.addSeparator(),
                                      self.add_layer_action])

        landmarks_menu = self.project_menu.addMenu("Landmark tools")
        landmarks_menu.addActions([self.add_landmark_point_action, self.move_landmark_point_action])
        self.project_menu.addMenu(landmarks_menu)

        self.project_menu.addActions([self.project_menu.addSeparator(),
                                      self.options_action,
                                      self.styles_action,
                                      self.project_menu.addSeparator(),
                                      self.quit_action])

    def create_view_menu(self) -> None:
        """
        Create the view menu
        """
        # Actions for view
        self.zoom_in_action = QAction(QIcon(":/resources/mActionZoomIn.svg"), "Zoom In", self)
        self.zoom_in_action.setShortcut("Ctrl++")
        self.zoom_out_action = QAction(QIcon(":/resources/mActionZoomOut.svg"), "Zoom Out", self)
        self.zoom_out_action.setShortcut("Ctrl+-")
        self.pan_action = QAction(QIcon(":/resources/mActionPan.svg"), "Pan", self)
        self.pan_action.setShortcut("Ctrl+p")
        self.scale_bar_action = QAction("Scale Bar", self)
        self.scale_bar_action.setCheckable(True)
        self.north_arrow_action = QAction("North Arrow", self)
        self.north_arrow_action.setCheckable(True)

        self.view_menu.addActions([self.zoom_in_action,
                                   self.zoom_out_action,
                                   self.pan_action])
        self.view_menu.addSeparator()

    def create_tools_menu(self) -> None:
        """
        Create the tools menu
        """
        # Actions for Tools
        self.measure_distance_action = QAction(QIcon(":/resources/mActionMeasure.svg"), "Measure Distance Tool", self)
        self.measure_distance_action.setCheckable(True)
        self.measure_distance_action.setShortcut("Ctrl+m")
        self.measure_angle_action = QAction(QIcon(":/resources/mActionMeasureAngle.svg"), "Measure Angle Tool", self)
        self.measure_angle_action.setCheckable(True)
        self.measure_area_action = QAction(QIcon(":/resources/mActionMeasureArea.svg"), "Measure Area Tool", self)
        self.measure_area_action.setCheckable(True)
        self.coordinate_converter_action = QAction(QIcon(":/resources/converter.svg"), "Coordinate Converter...", self)

        measure_tools_menu = self.tools_menu.addMenu("Measure Tools")
        measure_tools_menu.addActions([self.measure_distance_action,
                                       self.measure_angle_action,
                                       self.measure_area_action])

        self.tools_menu.addAction(self.coordinate_converter_action)
        self.tools_menu.addMenu(measure_tools_menu)

    def create_help_menu(self) -> None:
        """ Create the help menu """
        self.help_menu = self.menubar.addMenu("Help")

        self.about_action = QAction(QIcon(":/resources/iquaview_vector.svg"), "About", self)
        self.about_action.triggered.connect(self.about)
        self.help_menu.addActions([self.about_action])

    def create_project_toolbar(self) -> None:
        """
        Create the project toolbar
        """
        # Add actions to project toolbar
        self.project_toolbar = QToolBar("Project tools")
        self.project_toolbar.setObjectName("Project tools")
        self.project_toolbar.addAction(self.new_project_action)
        self.project_toolbar.addAction(self.open_project_action)
        self.project_toolbar.addAction(self.save_project_action)
        self.project_toolbar.addAction(self.saveprojectas_action)
        self.project_toolbar.addAction(self.add_layer_action)

        self.landmark_toolbutton = QToolButton()
        self.landmark_toolbutton.setPopupMode(QToolButton.MenuButtonPopup)
        self.landmark_toolbutton.addActions([self.add_landmark_point_action, self.move_landmark_point_action])
        self.landmark_toolbutton.setDefaultAction(self.add_landmark_point_action)
        self.landmark_toolbutton.triggered.connect(self.change_menu_tool)
        self.project_toolbar.addWidget(self.landmark_toolbutton)

        self.addToolBar(self.project_toolbar)

    def create_view_toolbar(self) -> None:
        """
        Create the view toolbar
        """
        # Toolbar for View
        self.view_toolbar = QToolBar("View Tools")
        self.view_toolbar.setObjectName("View Tools")
        self.view_toolbar.addAction(self.zoom_in_action)
        self.view_toolbar.addAction(self.zoom_out_action)
        self.view_toolbar.addAction(self.pan_action)
        self.addToolBar(self.view_toolbar)

    def create_tools_toolbar(self) -> None:
        """
        Create the tools toolbar
        """
        # Toolbar for Tools
        self.tools_toolbar = QToolBar("Tools")
        self.tools_toolbar.setObjectName("Tools")

        tool_button = QToolButton()
        tool_button.setPopupMode(QToolButton.MenuButtonPopup)
        tool_button.addActions([self.measure_distance_action,
                                self.measure_angle_action,
                                self.measure_area_action])
        tool_button.setDefaultAction(self.measure_distance_action)
        tool_button.triggered.connect(self.change_menu_tool)
        self.tools_toolbar.addWidget(tool_button)
        self.tools_toolbar.addAction(self.coordinate_converter_action)
        self.addToolBar(self.tools_toolbar)

    def create_connection_toolbar(self) -> None:
        """
        Create the connection toolbar
        """
        # Toolbar for AUV Processes
        self.connection_toolbar = QToolBar("AUV Processes")
        self.connection_toolbar.setObjectName("AUV Processes")
        self.auv_processes_widget = auvprocesseswidget.AUVProcessesWidget(self.config, self.vehicle_info,
                                                                          self.vehicle_data, self)
        self.connection_toolbar.addWidget(self.auv_processes_widget)
        self.addToolBar(self.connection_toolbar)

    def create_dock_widgets(self) -> None:
        """
        Create the dock widgets
        """
        # Dock for Layer Legend
        self.legend_dock = QDockWidget("Layers", self)
        self.legend_dock.setObjectName("layers")
        self.legend_dock.setAllowedAreas(Qt.LeftDockWidgetArea | Qt.RightDockWidgetArea)
        self.legend_dock.setContentsMargins(9, 9, 9, 9)
        self.legend_dock.setAcceptDrops(True)
        self.legend_dock.installEventFilter(self)

        # Dock for track
        self.tracks_dock = QDockWidget("Tracks", self)
        self.tracks_dock.setObjectName("Tracks")
        self.tracks_dock.setAllowedAreas(Qt.LeftDockWidgetArea | Qt.RightDockWidgetArea)
        self.tracks_dock.setContentsMargins(9, 9, 9, 9)

        # Dock for range and bering
        self.range_bearing_dock = QDockWidget("Range/Bearing", self)
        self.range_bearing_dock.setObjectName("Range/Bearing")
        self.range_bearing_dock.setAllowedAreas(Qt.LeftDockWidgetArea | Qt.RightDockWidgetArea)
        self.range_bearing_dock.setContentsMargins(9, 9, 9, 9)
        self.range_bearing_dock.setVisible(False)

        # dock for Log Info
        self.log_dock = QDockWidget("Log info")
        self.log_dock.setObjectName("Log info Dock")
        # self.log_dock.setAllowedAreas(Qt.BottomDockWidgetArea | Qt.TopDockWidgetArea)
        self.log_dock.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.setContentsMargins(9, 9, 9, 9)
        self.log_dock.setStyleSheet("QDockWidget { font: bold; }")
        self.log_widget = logwidget.LogWidget(self.vehicle_data)
        self.log_dock.setWidget(self.log_widget)
        self.log_dock.hide()

    def add_dock_widgets(self) -> None:
        """
        Add the dock widgets to the main window
        """
        self.addDockWidget(Qt.LeftDockWidgetArea, self.legend_dock)
        self.addDockWidget(Qt.LeftDockWidgetArea, self.tracks_dock)
        self.addDockWidget(Qt.LeftDockWidgetArea, self.range_bearing_dock)
        self.addDockWidget(Qt.RightDockWidgetArea, self.mission_module.get_mission_info_dockwidget())
        self.addDockWidget(Qt.RightDockWidgetArea, self.mission_module.get_waypoint_editing_dockwidget())
        self.addDockWidget(Qt.RightDockWidgetArea, self.mission_module.get_templates_dockwidget())
        self.addDockWidget(Qt.RightDockWidgetArea, self.log_dock)

    def create_action(self,
                      name: str,
                      checkable: bool = False,
                      checked: bool = False,
                      func=None,
                      shortcut: str = "") -> QAction:
        """
        Given a name, the status of checkable and checked, a setVisible function and a shortcut creates an action.
        :param name: action name
        :type name: str

        :param checkable: true if the action can be checkable, false otherwise
        :type checkable: bool
        :param checked: true if the actions will be checked, false otherwise
        :type checked: bool
        :param func: function to connect to signal
        :type func: function
        :param shortcut: action shortcut
        :type shortcut: str
        :return: new action
        :rtype: QAction

        """
        action = QAction(name, self)
        if shortcut:
            action.setShortcut(shortcut)
        action.setCheckable(checkable)
        action.setChecked(checked)
        if func is not None:
            action.toggled.connect(func)
        return action

    @staticmethod
    def change_dock_visibility(dock_widget: QDockWidget, floating=False):
        """ Change dock visibility """
        if dock_widget.isHidden():
            if floating:
                dock_widget.setFloating(floating)
            dock_widget.show()
        else:
            dock_widget.hide()

    def create_visibility_actions(self) -> None:
        """
        Create actions for toggling the visibility of docks
        """
        # create qactions to add at view_menu
        self.log_action = self.create_action("Log Info", True, False, None, "Ctrl+l")
        self.log_action.triggered.connect(lambda: self.change_dock_visibility(self.log_dock, True))
        self.log_dock.visibilityChanged.connect(self.log_action.setChecked)

        self.legend_action = self.create_action("Layers", True, True)
        self.legend_action.triggered.connect(lambda: self.change_dock_visibility(self.legend_dock))
        self.legend_dock.visibilityChanged.connect(self.legend_action.setChecked)

        self.tracks_action = self.create_action("Tracks", True, True)
        self.tracks_action.triggered.connect(lambda: self.change_dock_visibility(self.tracks_dock))
        self.tracks_dock.visibilityChanged.connect(self.tracks_action.setChecked)

        self.range_bearing_action = self.create_action("Range and Bearing", True, False)
        self.range_bearing_action.triggered.connect(lambda: self.change_dock_visibility(self.range_bearing_dock))
        self.range_bearing_dock.visibilityChanged.connect(self.range_bearing_action.setChecked)

        view_menu_panels = self.view_menu.addMenu("Panels")
        view_menu_panels.addActions([self.legend_action,
                                     self.tracks_action,
                                     self.log_action,
                                     self.range_bearing_action])

        self.project_toolbar_action = self.create_action("Project", True, True, self.project_toolbar.setVisible)
        self.view_toolbar_action = self.create_action("View", True, True, self.view_toolbar.setVisible)
        self.vessel_toolbar_action = self.create_action("Vessel", True, True, self.vessel.vessel_toolbar.setVisible)
        self.tools_toolbar_action = self.create_action("Tools", True, True, self.tools_toolbar.setVisible)
        self.mission_toolbar_action = self.create_action("Mission", True, True, self.mission_toolbar.setVisible)
        self.connection_toolbar_action = self.create_action("AUV Processes", True, True,
                                                            self.connection_toolbar.setVisible)
        self.auvwifi_toolbar_action = self.create_action("Vehicle tools (WiFi)", True, True,
                                                         self.auv_pose.auvwifi_toolbar.setVisible)

        view_menu_decorations = self.view_menu.addMenu("Decorations")
        view_menu_decorations.addAction(self.scale_bar_action)
        view_menu_decorations.addAction(self.north_arrow_action)

        # add actions on view_menu_toolbar, to change toolbar visibility
        self.view_menu_toolbar = self.view_menu.addMenu("Toolbars")
        self.view_menu_toolbar.addActions([self.project_toolbar_action,
                                           self.view_toolbar_action,
                                           self.vessel_toolbar_action,
                                           self.tools_toolbar_action,
                                           self.mission_toolbar_action,
                                           self.connection_toolbar_action,
                                           self.auvwifi_toolbar_action])

    def showMaximized(self) -> None:
        """
        Overrides showMaximized

        Shows the mainWindow maximized.
        """
        super().showMaximized()
        try:
            zoom_scale = self.config.settings.get('map_zoom_scale')
        except:
            zoom_scale = 20000

        try:
            map_latitude = self.config.settings.get('map_latitude')
            if map_latitude > 90:
                map_latitude = 90
            elif map_latitude < -90:
                map_latitude = -90
        except:
            map_latitude = 41.775

        try:
            map_longitude = self.config.settings.get('map_longitude')
            if map_longitude > 180:
                map_longitude = 180
            elif map_longitude < -180:
                map_longitude = -180
        except:
            map_longitude = 3.034

        if zoom_scale is not None:
            zoom_scale *= 2
            self.show_scale(zoom_scale)

        try:
            if map_latitude is not None and map_longitude is not None:
                p = transform_point(
                    QgsPointXY(map_longitude, map_latitude),
                    QgsCoordinateReferenceSystem.fromEpsgId(4326),
                    self.canvas.mapSettings().destinationCrs()
                )
                self.canvas.setCenter(p)
        except:
            p = QgsPointXY(0, 0)
            self.canvas.setCenter(p)

    def set_logger_value(self, level):
        """
        Set logger value
        Sets the log level to ERROR (0), WARNING (1), INFO (2), or DEBUG (3),
        depending on the argument `level`.
    """
        self.logger_value = level

    def disconnect_auvp_dw(self):
        """ Disconnect auv pose dockwidget."""

        if self.auv_pose is not None:
            # disconnect AUVPose
            if self.auv_pose.is_connected():
                self.auv_pose.disconnect_object()

        self.auv_pose.auv_pose_action.setChecked(False)

    def clear_project(self):
        """
        clear the project - removes all settings and resets it back to an empty
        and add a basic world map layer

        """
        self.proj.clear()
        # if crs is invalid, set it to default
        if not self.canvas.mapSettings().destinationCrs().isValid():
            self.canvas.setDestinationCrs(QgsCoordinateReferenceSystem.fromEpsgId(4326))
        self.set_project_crs()
        self.project_crs_changed()
        self.setWindowTitle("Untitled Project - IQUAview")
        self.add_world_map()

        self.canvas.refresh()

    def open_project(self):
        """
        Open QFileDialog to find a project with extension .qgs

        """
        settings = QSettings()
        # get last used directory
        last_dir = settings.value("/Project/lastProjectDir", "", type=str)
        filename = QFileDialog.getOpenFileName(self, 'Open Project', last_dir, '*.qgs')
        if filename[0] != '':
            self.load_project(filename[0])
            settings.setValue("/Project/lastProjectDir", os.path.dirname(filename[0]))

    def load_project(self, filename):
        """
        Load a project with the filename name.

        :param filename: name of the project
        :type filename: str
        """
        self.clear_project()
        if filename is None or filename == '':
            return
        self.canvas.zoomScale(100000)  # reset zoom
        self.proj.read(filename)
        logger.info(f"Loaded a total of {len(self.proj.mapLayers())} layers")
        for layer in self.proj.mapLayers().values():
            layer_parent = None
            tree_layer = self.root.findLayer(layer.id())
            if tree_layer:
                layer_parent = tree_layer.parent()
            if layer.customProperty("mission_xml") is not None:
                try:
                    self.load_mission(layer, layer_parent)
                except Exception as e:
                    logger.error("Could not load " + layer.customProperty("mission_xml") + f" - {e}")
                self.proj.removeMapLayer(layer)
            elif layer.customProperty("ned_origin") is not None or layer.customProperty("virtual_cage") is not None:
                self.proj.removeMapLayer(layer)

        self.status_bar.showMessage("Project opened", 1500)
        self.config.settings['last_open_project'] = filename
        self.config.save()
        self.rotate_widget.setValue(self.canvas.rotation())

        self.setWindowTitle(f"{self.proj.fileName()} - IQUAview")
        self.set_project_crs()
        self.project_crs_changed()
        self.project_loaded_signal.emit()

    def load_mission(self, layer: QgsVectorLayer, parent: Union[QgsLayerTree, QgsLayerTreeGroup]):
        """
        Given a mission layer and its parent load the mission
        :param layer: mission layer
        :type layer: QgsVectorLayer
        :param parent: parent of the mission layer
        :type parent: Union[QgsLayerTree, QgsLayerTreeGroup]
        """
        self.mission_ctrl.load_mission(layer.customProperty("mission_xml"))
        mission_layer = self.mission_ctrl.get_current_mission_layer()
        mission_tree_layer = self.root.findLayer(mission_layer)
        if layer.customProperty("visible") is not None:
            if isinstance(layer.customProperty("visible"), str):
                visible = (layer.customProperty("visible") in ["True", "true"])
            else:
                visible = layer.customProperty("visible")
        else:
            visible = True
            layer.setCustomProperty("visible", visible)

        # set as current to get position
        self.view.setCurrentLayer(layer)
        previous_row = self.view.currentIndex().row()

        layer_cloned = mission_tree_layer.clone()
        parent.insertChildNode(previous_row, layer_cloned)
        self.root.removeChildNode(mission_tree_layer)
        layer_cloned.setItemVisibilityChecked(visible)
        self.view.setCurrentLayer(None)

    def ask_to_save(self):
        """
        Asks if user wants to save only the project or the project and the missions. Returns the answer.
        :return: Return answer
        :rtype: QMessageBox.StandardButton
        """
        modified, missions = self.are_missions_modified()
        if modified:
            str_missions = "\n\n"
            for mission in missions:
                str_missions += f"- {mission}\n"
            str_missions += "\n"
            reply = QMessageBox.question(self, 'Save project',
                                         "You are about to save the project with some unsaved missions:"
                                         f"{str_missions}"
                                         "In order not to lose the changes you can save the missions too (Save All) or "
                                         "you can just save the project (Save).",
                                         QMessageBox.SaveAll | QMessageBox.Save | QMessageBox.Close,
                                         QMessageBox.SaveAll)
            return reply
        return None

    def save_project(self, from_save_as=False, save_all=False):
        """
        Save current project.
        :param from_save_as: param to check if project will be saved for first time
        :type from_save_as: bool
        :param save_all: param to check if missions will be saved
        :type save_all: bool
        """
        if self.proj.fileName() == "":
            self.save_project_as()
            return

        save = True
        if not from_save_as:
            reply = self.ask_to_save()
            if reply == QMessageBox.Close:
                save = False
            if reply == QMessageBox.SaveAll:
                save_all = True

        if not save:
            return

        previous_selected_layer = self.view.selectedLayers()
        for l in self.proj.mapLayers().values():
            logger.info(f"Layer {l.name()} : type {l.source()}")
            # if no starts with '/' is a memory layer, should be saved
            if l.source()[0] != "/":
                if l.customProperty("mission_xml") is not None:
                    self.model.dataChanged.disconnect(self.layer_changed)
                    self.mission_ctrl.set_current_mission(l)
                    self.model.dataChanged.connect(self.layer_changed)
                    current_mission = self.mission_ctrl.get_current_mission()
                    if current_mission.is_saved():
                        self.save_vector_layer(l)
                    if save_all:
                        self.mission_ctrl.save_mission()

                    mission_layer = self.mission_ctrl.get_current_mission_layer()
                    mission_tree_layer: QgsLayerTreeLayer = self.root.findLayer(mission_layer)
                    l.setCustomProperty("visible", mission_tree_layer.isVisible())
                else:
                    self.save_vector_layer(l)
            else:
                self.save_vector_layer(l)

        self.proj.write()
        # add temporal layers to proj
        for layer in previous_selected_layer:
            self.view.setCurrentLayer(layer)
            if layer.customProperty("mission_xml") is not None:
                self.mission_ctrl.set_current_mission(layer)
        self.status_bar.showMessage("Project saved", 1500)
        self.config.settings['last_open_project'] = self.proj.fileName()
        self.config.save()
        self.setWindowTitle(f"{self.proj.fileName()} - IQUAview")

    def save_project_as(self):
        """
        Open a QfileDialog to save current project.

        """
        save = True
        save_all = False
        reply = self.ask_to_save()
        if reply == QMessageBox.Close:
            save = False
        if reply == QMessageBox.SaveAll:
            save_all = True

        if save:
            settings = QSettings()
            last_dir = settings.value("/Project/lastProjectDir", "", type=str)
            filename = QFileDialog.getSaveFileName(self, 'Save Project', last_dir, '*.qgs')
            project_filename = filename[0]
            if project_filename != '':
                if not project_filename.endswith('.qgs'):
                    project_filename += '.qgs'
                self.proj.setFileName(project_filename)
                self.save_project(True, save_all)
                settings.setValue("/Project/lastProjectDir", os.path.dirname(filename[0]))

    def save_vector_layer(self, layer):
        """
        Save a vector layer in the current project.
        :param layer: layer to save
        :type layer: QgsVectorLayer

        """
        # if it is a vector layer and has a valid geometry and layer is not a mission
        if layer.type() == QgsMapLayer.VectorLayer and layer.geometryType() not in [QgsWkbTypes.UnknownGeometry,
                                                                                    QgsWkbTypes.NullGeometry] and \
                (layer.customProperty("mission_xml") is None) and \
                (layer.customProperty("ned_origin") is None) and \
                (layer.customProperty("virtual_cage") is None):
            if not os.path.exists(os.path.dirname(self.proj.fileName()) + '/layers/'):
                os.mkdir(os.path.dirname(self.proj.fileName()) + '/layers/')

            if layer.isTemporary():
                extension = "shp"
            else:
                extension = self.get_extension(layer.source())
            if QgsVectorFileWriter.driverForExtension('.' + extension) != "S57":
                layer_name = os.path.dirname(self.proj.fileName()) + '/layers/' + layer.name() + '.' + extension
                ret = QgsVectorFileWriter.writeAsVectorFormat(layer,
                                                              layer_name,
                                                              "utf-8",
                                                              QgsCoordinateReferenceSystem.fromEpsgId(4326),
                                                              QgsVectorFileWriter.driverForExtension('.' + extension))
                if ret == QgsVectorFileWriter.NoError:
                    logger.info(layer.name() + " saved to " + layer_name)
                file_info = QFileInfo(layer_name)
                base_name = file_info.baseName()
                layer.setDataSource(layer_name, base_name, "ogr")

    @staticmethod
    def get_extension(source):
        """
        Get the extension of a file.
        :param source: file path
        :type source: str
        :return: extension
        :rtype: str
        """
        extension = source.split('.')[-1]
        if len(extension.split('|')) > 1:
            extension = extension.split('|')[0]
        return extension

    @staticmethod
    def delete_vector_layer(file_name):
        """
        Delete a vector layer of the current project.
        :param file_name: name of the layer
        :type file_name: str
        """

        # delete shapefile. *.dbf, *.prj, *.qpj, *.shp, *.shx extensions
        if file_name.endswith('.shp'):
            QgsVectorFileWriter.deleteShapeFile(file_name)
            # split fileName and extension
            file_name = file_name[:-4]
            # join fileName and .cpg
            cpg_file = file_name + ".cpg"
            # remove file
            if os.path.isfile(cpg_file):
                os.remove(cpg_file)

    def zoom_in(self):
        """
        Set 'zoom in' as a current tool.
        """
        self.reset_map_tool()
        self.canvas.setMapTool(self.tool_zoom_in)

    def zoom_out(self):
        """
        Set 'zoom out' as a current tool.
        """
        self.reset_map_tool()
        self.canvas.setMapTool(self.tool_zoom_out)

    def pan(self):
        """
        Set 'pan' as a current tool.
        """
        self.reset_map_tool()
        self.canvas.setMapTool(self.tool_pan)

    def show_xy(self, point):
        """
        Set point as text to display in self.label_xy.

        :param point: point
        :type: p: QgsPointXY
        """
        display_crs = QgsCoordinateReferenceSystem.fromEpsgId(4326)
        point = transform_point(point, self.canvas.mapSettings().destinationCrs(), display_crs)

        if math.isnan(point.x()) or math.isnan(point.y()):
            x = y = float('nan')
            self.label_xy.setText(f"{str(y)}, {str(x)}")
        else:
            longitude = point.x()
            latitude = point.y()
            coordinate_format = self.config.settings.get("coordinate_format", "degree")
            coordinate_precision = self.config.settings.get("coordinate_precision", 7)
            coordinate_directional_suffix = self.config.settings.get("coordinate_directional_suffix", True)
            coordinate_symbols = self.config.settings.get("coordinate_symbols", True)
            [lat_str, lon_str] = format_lat_lon(latitude,
                                                longitude,
                                                coordinate_format,
                                                coordinate_precision,
                                                coordinate_directional_suffix,
                                                coordinate_symbols)
            self.label_xy.setText(f"{lat_str}, {lon_str}")

    def show_scale(self, scale):
        """
        Show scale.

        :param scale: current scale
        :type: float
        """
        self.scale_widget.setScale(scale)

    def on_scale_changed(self):
        """ Set scale on canvas. """
        self.canvas.zoomScale(self.scale_widget.scale())

    def on_rotation_changed(self, rot):
        """
        Set new rotation 'rot'.

        :param rot: rotation of the canvas
        :type: float
        """
        if rot in (360.0, -360.0):
            self.rotate_widget.setValue(0)

        self.canvas.setRotation(rot)
        self.canvas.refresh()

    def reset_map_tool(self):
        """Uncheck a conflicting tool that may be set as maptool and set pan as default map tool"""
        if self.canvas.mapTool() is None:
            self.canvas.setMapTool(self.tool_pan)
        if self.canvas.mapTool() == self.mission_ctrl.get_edit_wp_mission_tool():
            self.canvas.mapTool().hide_bands()
        if self.canvas.mapTool() == self.point_feature.tool_get_point:
            self.add_landmark_point_action.setChecked(False)
        if self.canvas.mapTool() == self.tool_measure_distance:
            self.measure_distance_action.setChecked(False)
            self.tool_measure_distance.reset()
        if self.canvas.mapTool() == self.tool_measure_angle:
            self.measure_angle_action.setChecked(False)
            self.tool_measure_angle.reset()
        if self.canvas.mapTool() == self.tool_move_landmark:
            self.move_landmark_point_action.setChecked(False)
        if self.canvas.mapTool() == self.tool_measure_area:
            self.measure_area_action.setChecked(False)
            self.tool_measure_area.reset()
        if self.canvas.mapTool() == self.auv_pose.ned_origin_drawer.tool_get_point:
            self.auv_pose.move_ned_origin_action.setChecked(False)
        if self.canvas.mapTool() == self.auv_pose.tool_virtual_cage:
            self.auv_pose.virtual_cage_action.setChecked(False)
        if self.canvas.mapTool() is not None:
            self.canvas.unsetMapTool(self.canvas.mapTool())
        # set default tool
        self.canvas.setMapTool(self.tool_pan)

    def measure_distance(self):
        """
        Set 'measure' as a current tool.
        """
        self.reset_map_tool()
        if self.measure_distance_action.isChecked():
            self.canvas.setMapTool(self.tool_measure_distance)
        else:
            if self.mission_module.edit_wp_mission_action.isChecked():
                self.canvas.setMapTool(self.mission_ctrl.get_edit_wp_mission_tool())
            elif self.mission_module.select_features_mission_action.isChecked():
                self.canvas.setMapTool(self.mission_ctrl.selectfeattool)
            elif self.mission_module.move_mission_action.isChecked():
                self.canvas.setMapTool(self.mission_ctrl.move_mission())

    def measure_angle(self):
        """
        Set 'measure angle' as a current tool.
        """
        self.reset_map_tool()
        if self.measure_angle_action.isChecked():
            self.canvas.setMapTool(self.tool_measure_angle)
        else:
            if self.mission_module.edit_wp_mission_action.isChecked():
                self.canvas.setMapTool(self.mission_ctrl.get_edit_wp_mission_tool())
            elif self.mission_module.select_features_mission_action.isChecked():
                self.canvas.setMapTool(self.mission_ctrl.selectfeattool)
            elif self.mission_module.move_mission_action.isChecked():
                self.canvas.setMapTool(self.mission_ctrl.move_mission())

    def measure_area(self):
        """
        Set 'measure area' as a current tool.
        """
        self.reset_map_tool()
        if self.measure_area_action.isChecked():
            self.canvas.setMapTool(self.tool_measure_area)
        else:
            if self.mission_module.edit_wp_mission_action.isChecked():
                self.canvas.setMapTool(self.mission_ctrl.get_edit_wp_mission_tool())
            elif self.mission_module.select_features_mission_action.isChecked():
                self.canvas.setMapTool(self.mission_ctrl.selectfeattool)
            elif self.mission_module.move_mission_action.isChecked():
                self.canvas.setMapTool(self.mission_ctrl.move_mission())

    def change_menu_tool(self, action) -> None:
        """ change the measure tool from qtoolbutton"""
        sender: QToolButton = self.sender()
        sender.setDefaultAction(action)

    def init_layer_treeview(self) -> None:
        """
        Initialize layer tree view.
        """
        self.root = self.proj.layerTreeRoot()
        self.bridge = QgsLayerTreeMapCanvasBridge(self.root, self.canvas)
        self.model = QgsLayerTreeModel(self.root)
        self.model.setFlag(QgsLayerTreeModel.AllowNodeReorder)
        self.model.setFlag(QgsLayerTreeModel.AllowNodeRename)
        self.model.setFlag(QgsLayerTreeModel.AllowNodeChangeVisibility)
        self.model.setFlag(QgsLayerTreeModel.ShowLegend)
        self.view = QgsLayerTreeView(self)
        self.view.installEventFilter(self)
        self.view.setModel(self.model)
        self.legend_dock.setWidget(self.view)
        self.canvas.zoomToFullExtent()
        self.canvas.show()

        self.view.currentLayerChanged.connect(self.layer_changed)
        self.proj.layerWillBeRemoved[QgsMapLayer].connect(self.layer_will_be_removed)
        self.model.dataChanged.connect(self.layer_changed)

    def init_track_treeview(self) -> None:
        """ Initialize track tree view."""
        self.tracks_view = TrackTreeWidget(self.entities_manager)
        self.tracks_dock.setWidget(self.tracks_view)

    def init_range_bearing(self) -> None:
        """ Initialize range and bearing view."""
        self.range_bearing_widget = RangeBearingWidget(self.canvas, self.entities_manager)
        self.range_bearing_dock.setWidget(self.range_bearing_widget)

    def add_world_map(self):
        """
        Add a basic world map shp to project
        """
        home = os.path.expanduser('~')
        iquaview_default_dir = os.path.join(home, '.iquaview/default/layers')
        if not os.path.isdir(iquaview_default_dir):
            # create folder
            os.makedirs(iquaview_default_dir)

        QFile.copy(":/resources/world_map.shp", iquaview_default_dir + "/world_map.shp")
        QFile.copy(":/resources/world_map.dbf", iquaview_default_dir + "/world_map.dbf")
        QFile.copy(":/resources/world_map.prj", iquaview_default_dir + "/world_map.prj")
        QFile.copy(":/resources/world_map.qix", iquaview_default_dir + "/world_map.qix")
        QFile.copy(":/resources/world_map.qml", iquaview_default_dir + "/world_map.qml")
        QFile.copy(":/resources/world_map.shx", iquaview_default_dir + "/world_map.shx")

        vlayer = QgsVectorLayer(iquaview_default_dir + "/world_map.shp", "world_map", "ogr")
        self.proj.addMapLayer(vlayer)
        addlayers.zoom_to_layers_extent(self.canvas, [vlayer])

    def add_layer(self):
        """ Open AddLayers Dialog"""
        addlayer = addlayers.AddLayersDlg(self.proj, self.canvas, self.msg_log)
        addlayer.exec_()

    def add_landmark_point(self):
        """ Set 'Add Landmark' as a current tool."""
        self.reset_map_tool()

        if self.add_landmark_point_action.isChecked():
            self.point_feature.reset()
            self.point_feature.show()

    def move_landmark_point(self):
        """ Set 'Move Landmark' as a current tool."""
        self.reset_map_tool()
        if self.move_landmark_point_action.isChecked():
            self.tool_move_landmark.set_landmark_layer(self.view.currentLayer())
            self.canvas.setMapTool(self.tool_move_landmark)

    def finish_add_landmark(self):
        """ Uncheck add_landmark action and set pan as maptool"""
        self.add_landmark_point_action.setChecked(False)
        self.pan()

    def layer_changed(self):
        """ If layer changed, set as a current layer."""
        self.model.dataChanged.disconnect(self.layer_changed)
        self.mission_ctrl.clear_current_mission()
        self.model.dataChanged.connect(self.layer_changed)
        layer = self.view.currentLayer()
        if layer is not None and layer.type() == QgsMapLayer.VectorLayer:
            if self.mission_module.is_mission_layer(layer):
                self.mission_module.activate_mission_tools(True)
                self.auv_pose.mission_active.transfer_and_execute_action.setEnabled(True)
                self.move_landmark_point_action.setEnabled(False)
                if self.canvas.mapTool() == self.tool_move_landmark:
                    self.reset_map_tool()
                self.landmark_toolbutton.setDefaultAction(self.add_landmark_point_action)

                self.model.dataChanged.disconnect(self.layer_changed)
                self.mission_ctrl.set_current_mission(layer)
                self.mission_ctrl.highlight_mission()
                self.model.dataChanged.connect(self.layer_changed)

            elif self.is_single_point_layer(layer) and (layer.dataProvider().storageType() == "ESRI Shapefile"
                                                        or layer.dataProvider().storageType() == "Memory storage"):
                # only layers that are not missions, in .shp format or in memory and with only one point will be moved
                self.mission_module.activate_mission_tools(False)
                self.auv_pose.mission_active.transfer_and_execute_action.setEnabled(False)
                self.tool_move_landmark.set_landmark_layer(self.view.currentLayer())
                self.move_landmark_point_action.setEnabled(True)

            else:
                self.mission_module.activate_mission_tools(False)
                self.auv_pose.mission_active.transfer_and_execute_action.setEnabled(False)
                self.move_landmark_point_action.setEnabled(False)
                if self.canvas.mapTool() == self.tool_move_landmark:
                    self.reset_map_tool()
                self.landmark_toolbutton.setDefaultAction(self.add_landmark_point_action)
        else:
            self.mission_module.activate_mission_tools(False)
            self.auv_pose.mission_active.transfer_and_execute_action.setEnabled(False)
            self.move_landmark_point_action.setEnabled(False)
            if self.canvas.mapTool() == self.tool_move_landmark:
                self.reset_map_tool()
            self.landmark_toolbutton.setDefaultAction(self.add_landmark_point_action)

    @staticmethod
    def is_single_point_layer(layer):
        """
        check if layer has only one point
        :param layer: layer to check
        :type layer: QgsVectorLayer
        :return: return True if layer is a single point, otherwise False
        :rtype: bool

        """
        is_single_point = False
        if layer is not None and layer.featureCount() == 1:
            feature_it = layer.dataProvider().getFeatures()
            feature = next(feature_it, None)
            if feature is not None:
                is_single_point = feature.geometry().wkbType() == QgsWkbTypes.Point

        return is_single_point

    def layer_will_be_removed(self, layer):
        """
        Remove layer
        :param layer: layer to remove
        :type layer: QgsMapLayer
        """
        logger.info(f"Removing layer {layer.name()}")
        if self.mission_module.is_mission_layer(layer):
            self.mission_ctrl.remove_mission(layer)

    def failed_layers(self, layers):
        """
        Called when layers have failed to load from the current project
        :param layers: layers than failed
        :type layers: list
        """
        logger.info("Error while loading layers")
        for layer in layers:
            self.msg_log.logMessage("Failed to load layer " + layer, "LoadingProjectLayers", 1)

    def msgbar_catcher(self, msg, tag, level) -> None:
        """
        Called when info/warning/error messages are logged to show them in the message bar
        :param msg: msg to show
        :type msg: str
        :param tag: tag of the message
        :type tag: str
        :param level: importance level of the message
        :type level: int
        """
        # Disconnect temporaly the messageReceived signal to avoid loops from messages generated
        # when pushing a message to the msgbar.

        self.msg_log.messageReceived.disconnect(self.msgbar_catcher)
        if level <= 2:
            self.change_msg(self.msg_bar, msg, tag, level)
        else:
            self.change_msg(self.vehicle_msg_bar, msg, tag, level)
        self.msg_log.messageReceived.connect(self.msgbar_catcher)

    def change_msg(self, msg_bar, msg, tag, level):
        """
        Change the message of  the message bar
        :param msg_bar: message bar that show the msg
        :type msg_bar: QgsMessageBar
        :param msg: message to show
        :type msg: str
        :param tag: tag of the message
        :type tag: str
        :param level: importance level of the message
        :type level: int
        """
        if msg == "":
            if msg_bar == self.vehicle_msg_bar:
                self.vehicle_msg_bar.widgetRemoved.disconnect(self.mute_sound_effect)
            msg_bar.clearWidgets()
            if msg_bar == self.vehicle_msg_bar:
                self.vehicle_msg_bar.widgetRemoved.connect(self.mute_sound_effect)
            QTimer.singleShot(2000, self.mute_sound_effect)
        else:
            if level == 0:
                msg_bar.pushItem(QgsMessageBarItem("Info: ", msg, Qgis.Info, 0, self.centralwidget))
            elif level == 1:
                msg_bar.pushItem(QgsMessageBarItem("Warning: ", msg, Qgis.Warning, 0, self.centralwidget))
            elif level == 2:
                msg_bar.pushItem(QgsMessageBarItem("Error: ", msg, Qgis.Critical, 0, self.centralwidget))
            elif level == 3:
                if msg_bar.currentItem() is None:
                    msg_bar.pushItem(QgsMessageBarItem("Warning: ", msg, Qgis.Warning, 0, self.centralwidget))
                elif (msg != msg_bar.currentItem().text()
                      and msg not in msg_bar.currentItem().text()
                      and msg_bar.currentItem().level() != Qgis.Critical):
                    msg = msg + ", " + msg_bar.currentItem().text()
                    msg_bar.pushItem(QgsMessageBarItem("Warning: ", msg, Qgis.Warning, 0, self.centralwidget))

            elif level == 4:
                if msg_bar.currentItem() is None or msg != msg_bar.currentItem().text():
                    msg_item = self.get_critical_message_bar_item("Status Code: ", msg)
                    msg_bar.pushWidget(msg_item, Qgis.Critical)
            elif level == 5:
                if msg_bar.currentItem() is None or msg != msg_bar.currentItem().text():
                    msg_item = self.get_critical_message_bar_item("Error Code: ", msg)
                    msg_bar.pushWidget(msg_item, Qgis.Critical)

            elif level == 6:
                if msg_bar.currentItem() is None or msg != msg_bar.currentItem().text():
                    msg_item = self.get_critical_message_bar_item("Recovery Action: ", msg)
                    msg_bar.pushWidget(msg_item, Qgis.Critical)

            if msg_bar.currentItem().level() == Qgis.Critical \
                    and not self.effect.isPlaying() \
                    and not self.message_muted:
                self.effect.play()

    def get_critical_message_bar_item(self, title, msg) -> QgsMessageBarItem:
        """
        Get a QgsMessageBarItem customized with a mute button
        :param title: Bar item title
        :type title: str
        :param msg: Message
        :type msg: str
        :return: Custom QgsMessageBarItem with a mute button
        :rtype: QgsMessageBarItem
        """
        msg_item = QgsMessageBarItem(title, msg, Qgis.Critical, 0, self.centralwidget)

        # Create a custom widget with a mute button
        mute_button = QToolButton(self.vehicle_msg_bar)
        if not self.message_muted and not self.all_muted:
            mute_button.setToolTip("Mute")
            mute_button.setText("On")
            mute_button.setIcon(QIcon(":resources/volume_on.svg"))
        else:
            mute_button.setToolTip("Unmute")
            mute_button.setText("Off")
            mute_button.setIcon(QIcon(":resources/volume_off.svg"))
        mute_button.setAutoRaise(True)
        # Set up a lighter background when the button is hovered
        background_color: QColor = self.vehicle_msg_bar.palette().color(self.vehicle_msg_bar.backgroundRole())
        lighter_background = background_color.lighter().name(QColor.NameFormat.HexRgb)
        mute_button.setStyleSheet(
            f'QToolButton:hover {{ background-color: {lighter_background}; }}')

        mute_button.setPopupMode(QToolButton.InstantPopup)
        mute_button.clicked.connect(self.switch_volume_icon)
        msg_item.setWidget(mute_button)
        return msg_item

    def switch_volume_icon(self):
        """ Switch volume icon from mute button. Mute or unmute the sound effect"""
        sender: QToolButton = self.sender()
        if sender.text() == "On":
            sender.setIcon(QIcon(":resources/volume_off.svg"))
            sender.setToolTip("Unmute")
            sender.setText("Off")
            self.message_muted = True
            if self.effect.isPlaying():
                self.effect.stop()
        elif not self.all_muted:
            sender.setIcon(QIcon(":resources/volume_on.svg"))
            sender.setToolTip("Mute")
            sender.setText("On")
            self.message_muted = False
            if not self.effect.isPlaying():
                self.effect.play()

    def mute_sound_effect(self, item=None):
        """ Stop play sound effect"""
        # Stop sound effect if the item is Critical and no remains critical items in the msg bar.
        if item is None:
            item = self.vehicle_msg_bar.currentItem()
        if (item is None
                or (item.level() == Qgis.Critical
                    and (self.vehicle_msg_bar.currentItem() is None
                         or self.vehicle_msg_bar.currentItem().level() != Qgis.Critical))):
            self.effect.stop()
            self.message_muted = False

    def update_sound_effect(self):
        settings = QSettings()
        volume = settings.value("General/volume", 50, type=int)
        self.effect.setVolume(volume / 100)
        if volume == 0:
            self.all_muted = True
        else:
            self.all_muted = False

    def edit_general_options(self):
        """ Open Options Dialog."""
        self.options_dialog.set_current_page(options.GENERAL)
        self.options_dialog.update_options()
        self.options_dialog.show()
        self.options_dialog.activateWindow()

    def edit_styles(self):
        """ Opens Styles Dialog. """
        self.options_dialog.set_current_page(options.STYLES)
        self.options_dialog.update_options()
        self.options_dialog.show()
        self.options_dialog.activateWindow()

    def scale_bar_visibility(self):
        """
        Change Scale Bar visibility.
        """
        if self.scale_bar_action.isChecked():
            self.scale_bar.show()
        else:
            self.scale_bar.hide()
        self.canvas.refresh()
        self.config.settings['visibility_scale_bar'] = self.scale_bar_action.isChecked()
        self.config.save()

    def north_arrow_visibility(self):
        """
        Change North Arrow visibility.
        """
        if self.north_arrow_action.isChecked():
            self.north_arrow.show()
        else:
            self.north_arrow.hide()
        self.canvas.refresh()
        self.config.settings['visibility_north_arrow'] = self.north_arrow_action.isChecked()
        self.config.save()

    def set_decorations_visibility(self):
        """
        Set Decorations visibility
        """
        visibility_north_arrow = self.config.settings['visibility_north_arrow']
        visibility_scale_bar = self.config.settings['visibility_scale_bar']

        self.north_arrow_action.setChecked(visibility_north_arrow)
        self.scale_bar_action.setChecked(visibility_scale_bar)
        self.north_arrow_visibility()
        self.scale_bar_visibility()

    def open_projection_dialog(self):
        """
        Open Projection Selection Dialog and set new crs
        """
        projection_selection_dialog = QgsProjectionSelectionDialog()
        result = projection_selection_dialog.exec_()
        if result == QDialog.Accepted:
            crs = projection_selection_dialog.crs()
            if not crs.authid():
                # default crs
                crs = self.canvas.mapSettings().destinationCrs()
            self.proj.setCrs(crs)
            self.proj.setEllipsoid(crs.ellipsoidAcronym())
            self.canvas.setDestinationCrs(crs)
            self.project_crs_changed()

    def set_project_crs(self):
        """
        Set canvas crs as current project crs
        """
        crs = self.canvas.mapSettings().destinationCrs()
        self.proj.setCrs(crs)
        self.proj.setEllipsoid(crs.ellipsoidAcronym())

    def project_crs_changed(self):
        """
        change projection crs
        """
        # self.canvas.setDestinationCrs(self.proj.crs())
        crs = self.canvas.mapSettings().destinationCrs()
        if crs.isValid():
            self.projection_widget.setText(crs.authid())
            self.projection_widget.setToolTip(f"Current CRS: {crs.authid()} {crs.description()}")
            self.projection_widget.setIcon(QIcon(":/resources/mIconProjectionEnabled.svg"))

        else:
            self.projection_widget.setText("")
            self.projection_widget.setToolTip("No Projection")
            self.projection_widget.setIcon(QIcon(":/resources/mIconProjectionDisabled.svg"))

    def connection_failed(self):
        """ Show connection failed message on status bar."""
        self.status_bar.showMessage("Connection failed", 1500)

    def process_changed(self, msg):
        """
        Show message msg on status bar.
        :param msg: message
        :type msg: string
        """
        self.status_bar.showMessage(msg, 1500)

    def update_position(self, position):
        """
        Center to last received position on the map.
        """
        rect = QgsRectangle(position, position)
        self.canvas.setExtent(rect)
        # self.canvas.zoomScale(400)
        self.canvas.refresh()

    def auv_wifi_connected(self, connected):
        """
        Start connection with AUV
        :param connected: conection with AUV
        :type connected:  bool
        """
        self.auv_pose.mission_active.change_enable(connected)
        if connected:
            self.subscribe_topics()
            if self.vehicle_data.is_subscribed():
                self.auv_on_wifi = True
                self.auv_pose.enable_keep_position_action.setEnabled(True)
                self.auv_pose.enable_thrusters_action.setEnabled(True)
                self.auv_pose.goto_action.setEnabled(True)
                self.auv_pose.mission_report.get_get_report_action().setEnabled(True)

                self.auv_pose.vehicle_widgets_action.setEnabled(True)
                self.auv_pose.ned_origin_drawer.update_ned_point()
                self.auv_pose.vehicle_w_dock.show()
                return

        self.auv_on_wifi = False
        if self.auv_pose.move_ned_origin_action.isChecked():
            self.reset_map_tool()
        if self.auv_pose.virtual_cage_action.isChecked():
            self.reset_map_tool()
        self.auv_pose.enable_keep_position_action.setEnabled(False)
        self.auv_pose.enable_thrusters_action.setEnabled(False)
        self.auv_pose.goto_action.setEnabled(False)
        self.auv_pose.mission_report.get_get_report_action().setEnabled(False)
        self.auv_pose.vehicle_widgets_action.setEnabled(False)
        self.auv_pose.mission_active.disable()
        self.disconnect_objects()

        self.auv_pose.vehicle_w_dock.hide()
        self.auv_pose.update_icon_state()

    def are_missions_modified(self):
        """
        Get missions modified state and missions modified name
        :return: return True and the modified missions list if a mission has been modified, otherwise False
        :rtype: (bool, list)
        """
        missions = []
        missions_are_modified = False
        for mission in self.mission_ctrl.get_mission_list():
            if mission.is_modified():
                missions.append(mission.mission_name)
                missions_are_modified = True

        return missions_are_modified, missions

    def add_map_layer(self, layer):
        """
        Add layer to  the current project, view and canvas
        :param layer: layer
        :type layer: QgsMapLayer
        """
        self.proj.addMapLayer(layer, True)
        self.view.setCurrentLayer(layer)
        self.canvas.refresh()

    def remove_map_layer(self, layer):
        """
         Remove layer of current project, view and canvas
         :param layer: layer
         :type layer: QgsMapLayer
         """
        self.proj.removeMapLayer(layer)
        self.canvas.refresh()

    def change_toolbar_visibility(self, toolbar):
        """
        Change toolbar visibility
        :param toolbar: toolbar
        :type toolbar: QToolBar
        """
        sender: QAction = self.sender()
        if sender.isChecked():
            toolbar.setVisible(True)
        else:
            toolbar.setVisible(False)

    def subscribe_topics(self):
        """
        subscribe topics and call function to refresh data
        """
        self.vehicle_data.subscribe_topics()

        if self.vehicle_data.is_subscribed():

            busy_widget = busywidget.BusyWidget(title="Subscribing to the topics...")
            busy_widget.on_start()
            busy_widget.exec_()

            self.auv_pose.connect()
            self.log_widget.connect()

        else:
            self.auv_pose.disconnect_object()
            self.disconnect_objects()

    def about(self):
        """ Display a dialog with information about IQUAview, Qt and Qgis"""
        about_msg = QMessageBox(self)
        about_msg.setModal(False)
        about_msg.setStandardButtons(QMessageBox.Close)
        pixmap = QPixmap(":/resources/iquaview.png")
        pixmap = pixmap.scaled(QSize(550, 393), Qt.KeepAspectRatio)
        about_msg.setIconPixmap(pixmap)
        about_msg.setText("<p><b>Iqua Robotics </b><p>" +
                          f"<p>IQUAview version: {__version__}</p>" +
                          f"<p>IQUAview lib version: {iquaview_lib_version}</p>" +
                          self.plugin_manager.about() +
                          "<a href='mailto:support@iquarobotics.com'>support@iquarobotics.com</a>" +
                          "<p>Tested with: </p>" +
                          "<ul>" +
                          "<li>Qt 5.12.8</li>" +
                          "<li>Qgis 3.26.3-Buenos Aires</li>" +
                          "</ul>" +
                          "<p>Currently running: </p>" +
                          "<ul>" +
                          f"<li> Qt {qVersion()}</li>" +
                          f"<li>Qgis {Qgis.QGIS_VERSION}</li>" +
                          "</ul>")
        about_msg.setWindowTitle("About")

        about_msg.show()

    def disconnect_objects(self):
        """
        Disconnect multiple objects
        """
        self.vehicle_data.disconnect_object()
        self.auv_pose.close()
        self.log_widget.disconnect_object()

    def eventFilter(self, obj, event):
        """
        Filters events if this object has been installed as an event filter for the obj object.
        """
        if event.type() == QEvent.KeyPress:
            key = event.key()
            if key == Qt.Key_F11:
                if not self.isFullScreen():
                    self.showFullScreen()
                else:
                    self.showMaximized()
            elif key == Qt.Key_Delete and obj == self.view:
                self.menu_provider.remove_group_or_layer()

        if obj in (self.canvas, self.legend_dock):
            if event.type() == QEvent.DragEnter:
                if event.mimeData().hasUrls():
                    event.acceptProposedAction()
                return True

            if event.type() == QEvent.Drop:
                if event.mimeData().hasUrls():
                    urls = event.mimeData().urls()
                    mission_files = []
                    layer_files = []

                    for url in urls:
                        file = unquote(url.toLocalFile())
                        if QFileInfo(file).suffix() == "xml":  # is a mission file
                            mission_files.append(file)
                        else:  # is a layer file
                            layer_files.append(file)

                    if len(mission_files) > 0:
                        self.mission_ctrl.load_missions(mission_files)
                    if len(layer_files) > 0:
                        addlayers.add_layers_to_project(self.proj, self.canvas, layer_files)
                return True

            if obj == self.canvas and event.type() == QEvent.Leave and not self.canvas.underMouse():
                if self.mission_ctrl.get_edit_wp_mission_tool() == self.canvas.mapTool():
                    if self.canvas.mapTool() is not None:
                        self.canvas.mapTool().hide_bands()
                    # It is important to not return True in this case, so the event can propagate to the object
                if self.profile_tool_module.get_map_tool() == self.canvas.mapTool():
                    if self.profile_tool_module.get_map_tool_renderer() is not None:
                        self.profile_tool_module.get_map_tool_renderer().reset_rubber_band2()
                    # It is important to not return True in this case, so the event can propagate to the object

            return False

        return QMainWindow.eventFilter(self, obj, event)

    def closeEvent(self, event):
        """ Overrides closeEvent"""
        settings = QSettings()
        confirmation = settings.value("General/confirmation_msg", True, type=bool)
        # only display the confirmation message if the confirmation is true and there is no test in progress.
        if confirmation and not self.is_test:
            confirmation_msgbox = QMessageBox(self)
            checkbox = QCheckBox("Don't ask again")
            confirmation_msgbox.setText("Are you sure you want to exit?")
            confirmation_msgbox.setIcon(QMessageBox.Question)
            ok_button = confirmation_msgbox.addButton(QMessageBox.Ok)
            confirmation_msgbox.addButton(QMessageBox.Cancel)
            confirmation_msgbox.setCheckBox(checkbox)
            ok_button.setFocus()
            result = confirmation_msgbox.exec_()
            if result != QMessageBox.Ok:
                event.ignore()
                return

            is_checked = checkbox.isChecked()
            settings.setValue("General/confirmation_msg", not is_checked)

        super().closeEvent(event)

        self.range_bearing_widget.close()

        self.view.currentLayerChanged.disconnect(self.layer_changed)
        self.proj.layerWillBeRemoved[QgsMapLayer].disconnect(self.layer_will_be_removed)
        self.model.dataChanged.disconnect(self.layer_changed)

        self.web_server.stop_server_signal.emit()

        config_crs = QgsCoordinateReferenceSystem.fromEpsgId(4326)
        pos = transform_point(
            self.canvas.center(),
            self.canvas.mapSettings().destinationCrs(),
            config_crs
        )
        self.config.settings['map_latitude'] = pos.y()
        self.config.settings['map_longitude'] = pos.x()
        self.config.settings['map_zoom_scale'] = self.canvas.scale()

        self.config.save()
        logger.info("Disconnecting from vehicle")
        self.auv_processes_widget.disconnect_auvprocesses()
        self.disconnect_objects()
        self.auv_pose.timeout_widget.stop_timer_signal.emit()
        self.auv_pose.sync_status.disconnect_object()
        self.disconnect_auvp_dw()
        self.options_dialog.data_output_manager_widget.disconnect_widget(reconnect=False)
        self.plugin_manager.disconnect_plugins()
        self.plugin_manager.close()

        if self.auv_pose.process_teleop is not None:
            self.auv_pose.process_teleop.terminate()
            # wait to finish
            self.auv_pose.process_teleop.wait()
        if self.auv_pose.timer_robot_monitor:
            self.auv_pose.timer_robot_monitor.cancel()
        if self.auv_pose.process_robot_monitor is not None:
            self.auv_pose.process_robot_monitor.terminate()
            # wait to finish
            self.auv_pose.process_robot_monitor.wait()

        del self.model
        #  The application quits when the last window was successfully closed
        QgsApplication.closeAllWindows()
