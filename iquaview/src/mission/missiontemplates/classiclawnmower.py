# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Classic lawn mower pattern definition
"""

import logging
import math

from qgis.core import QgsDistanceArea, QgsProject, QgsCoordinateReferenceSystem

from iquaview_lib.cola2api.mission_types import (Mission,
                                                 MissionStep,
                                                 MissionGoto,
                                                 MissionSection)

logger = logging.getLogger(__name__)


class ClassicLawnMower:

    def __init__(self, canvas):
        self.wp = []
        self.mission = None
        crs = QgsCoordinateReferenceSystem.fromEpsgId(4326)
        self.distance = QgsDistanceArea()
        self.distance.setSourceCrs(crs, QgsProject.instance().transformContext())
        self.distance.setEllipsoid(QgsProject.instance().ellipsoid())

    def get_mission(self):
        return self.mission

    def compute_tracks(self, area_points, track_spacing, num_across_tracks):

        """
             Compute lawn-mower tracks
            :param area_points: points defining the extent of the tracks, they should be in WGS 84 lat/lon.
                                first two points define the along track direction.
            :type area_points: list(QgsPointXY)
            :param track_spacing: desired space in meters between consecutive along tracks
            :type track_spacing: float
            :param num_across_tracks: number of desired across tracks. They will be equally spaced through the area.
            :type num_across_tracks: int
            :return: list of ordered waypoints of the lawn-mower trajectory
        """

        dist_along_track = self.distance.measureLine(area_points[0], area_points[1])
        dist_across_track = self.distance.measureLine(area_points[1], area_points[2])
        bearing_along_track = self.distance.bearing(area_points[0], area_points[1])
        bearing_across_track = self.distance.bearing(area_points[1], area_points[2])

        turn_track_dist = length_one_across_track = track_spacing

        num_along_tracks = math.ceil(dist_across_track / length_one_across_track) + 1

        logger.debug("distAlongTrack: {}".format(dist_along_track))
        logger.debug("distAcrossTrack: {}".format(dist_across_track))
        logger.debug("bearingAlongTrack {}:".format(bearing_along_track))
        logger.debug("bearingAcrossTrack {}:".format(bearing_across_track))
        logger.debug("numAlongTrack: {}".format(num_along_tracks))
        logger.debug("numAcrossTrack: {}".format(num_across_tracks))
        logger.debug("lenghtOneAcrossTrack {}".format(length_one_across_track))

        # Initialize with 2 first points
        current_wp = area_points[0]
        next_wp = area_points[1]
        reverse_direction_along = False  # For changing direction of alternate along tracks
        wp_list = [current_wp]

        # Loop to generate all waypoints of along tracks
        for i in range(int(num_along_tracks) * 2 - 1):

            wp_list.append(next_wp)

            current_wp = next_wp
            if i % 2 == 0:  # even, so we just did along track, next draw across track
                next_wp = self.distance.measureLineProjected(current_wp, length_one_across_track,
                                                             bearing_across_track)[1]
                reverse_direction_along = not reverse_direction_along
            else:
                # Draw along track
                next_wp = self.distance.measureLineProjected(current_wp,
                                                             dist_along_track,
                                                             bearing_along_track + int(
                                                                 reverse_direction_along) * math.radians(180.0))[1]

        # Loop for across tracks
        if num_across_tracks > 0:
            dist_along_track_for_across = dist_along_track / (num_across_tracks + 1)
            # Distance across track after computing all the along tracks
            # (might not match with the across track distance defined on the area mission)
            if reverse_direction_along:
                real_dist_across_track = self.distance.measureLine(wp_list[0], wp_list[-2])
            else:
                real_dist_across_track = self.distance.measureLine(wp_list[0], wp_list[-1])
            next_wp = self.distance.measureLineProjected(current_wp, turn_track_dist, bearing_across_track)[1]
            reverse_direction_across = True  # For changing direction of alternate across tracks

            for i in range(num_across_tracks * 2 + 1):
                wp_list.append(next_wp)
                current_wp = next_wp
                if i % 2 == 0:  # Compute waypoint along track
                    next_wp = self.distance.measureLineProjected(current_wp,
                                                                 dist_along_track_for_across,
                                                                 bearing_along_track + int(
                                                                     reverse_direction_along) * math.radians(180.0))[1]
                else:  # Compute waypoint across track
                    next_wp = self.distance.measureLineProjected(current_wp,
                                                                 real_dist_across_track + 2 * turn_track_dist,
                                                                 bearing_across_track + int(
                                                                     reverse_direction_across) * math.radians(180.0))[1]
                    reverse_direction_across = not reverse_direction_across
        return wp_list

    def track_to_mission(self, wp_list, depth, altitude, heave_mode, speed, tolerance_xy, no_altitude_goes_up):
        """
        Creates a misison with a list of waypoints and the various parameters of a mission
        :param wp_list: Waypoints of the mission
        :type wp_list: list
        :param depth: depth of the mission
        :type depth: float
        :param altitude: Altitude of the mission
        :type altitude: float
        :param heave_mode: heave mode of the mission (depth, altitude, both)
        :type heave_mode: int
        :param speed: speed of the mission
        :type speed: float
        :param tolerance_xy: tolerance in the x direction
        :type tolerance_xy: float
        :param no_altitude_goes_up: reaction of auv when no altitude (Go up, Ignore)
        :type no_altitude_goes_up: bool

        """
        self.mission = Mission()
        for wp in range(len(wp_list)):
            if wp == 0:
                # first step type waypoint
                step = MissionStep()
                mwp = MissionGoto(wp_list[wp].y(), wp_list[wp].x(), depth, altitude,
                                  heave_mode,
                                  speed,
                                  tolerance_xy, no_altitude_goes_up)
                step.add_maneuver(mwp)
                self.mission.add_step(step)
            else:
                # rest of steps type section
                step = MissionStep()
                mwp = MissionSection(wp_list[wp - 1].y(), wp_list[wp - 1].x(), depth,
                                     wp_list[wp].y(), wp_list[wp].x(), depth, altitude,
                                     heave_mode,
                                     speed,
                                     tolerance_xy, no_altitude_goes_up)
                step.add_maneuver(mwp)
                self.mission.add_step(step)
