# -*- coding: utf-8 -*-
# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Widget to manage the inputs for classic or spiral lawn mower pattern definition
"""

import logging
import math

from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtGui import QValidator
from PyQt5.QtWidgets import QMessageBox
from qgis.core import (QgsPointXY,
                       QgsWkbTypes,
                       QgsCoordinateReferenceSystem,
                       QgsGeometry)

from iquaview.src.mission.maptools.rectangletools import (RectBy3PointsTool,
                                                          RectByFixedExtentTool,
                                                          RectFromCenterTool,
                                                          RectFromCenterFixedTool)
from iquaview.src.mission.missiontemplates.basetemplatewidget import BaseTemplateWidget
from iquaview.src.mission.missiontemplates.classiclawnmower import ClassicLawnMower
from iquaview.src.mission.missiontemplates.mappinglawnmower import MappingLawnMower
from iquaview.src.mission.missiontemplates.spirallawnmower import SpiralLawnMower
from iquaview.src.ui.ui_lawnmowerwidget import Ui_LawnMowerWidget
from iquaview_lib.cola2api import mission_types
from iquaview_lib.utils.qgisutils import transform_point
from iquaview_lib.utils.textvalidator import validate_custom_double

logger = logging.getLogger(__name__)


class LawnMowerWidget(BaseTemplateWidget, Ui_LawnMowerWidget):

    def __init__(self, canvas, msg_log, lawnmower_type, config=None, vehicle_info=None,
                 parent=None):
        super().__init__(canvas, msg_log, lawnmower_type, parent)
        self.setupUi(self)
        self.config = config
        self.vehicle_info = vehicle_info
        self.transect_to_turn_too_large = False
        self.warning_displayed = False
        self.mapping_widget.hide()

        if lawnmower_type == "Classic Lawn Mower":
            self.lawnmower = ClassicLawnMower(self.canvas)
        elif lawnmower_type == "Spiral Lawn Mower":
            self.lawnmower = SpiralLawnMower(self.canvas)
        elif lawnmower_type == "Mapping Lawn Mower":
            self.lawnmower = MappingLawnMower(self.canvas)
            self.mapping_widget.show()
            self.mapping_widget.distance_covered_to_turn_doubleSpinBox.valueChanged.connect(self.on_spinbox_changed)
            self.mapping_widget.distance_covered_to_turn_doubleSpinBox.editingFinished.connect(self.on_spinbox_changed)
            self.mapping_widget.distance_covered_to_turn_doubleSpinBox.valueChanged.connect(self.preview_tracks)
            self.mapping_widget.inside_radioButton.toggled.connect(self.preview_tracks)
            self.xy_tolerance_doubleSpinBox.valueChanged.connect(self.on_spinbox_changed)
            self.xy_tolerance_doubleSpinBox.editingFinished.connect(self.on_spinbox_changed)

        self.onTarget = False

        # Get the tools
        self.rectBy3Points_tool = RectBy3PointsTool(self.canvas)
        self.rectByFixedExtentTool = RectByFixedExtentTool(self.canvas,
                                                           self.mission_area_widget.alongTLength.value(),
                                                           self.mission_area_widget.acrossTLength.value())
        self.rect_from_center_tool = RectFromCenterTool(self.canvas)
        self.rect_from_center_fixed_tool = RectFromCenterFixedTool(self.canvas,
                                                                   self.mission_area_widget.alongTLength.value(),
                                                                   self.mission_area_widget.acrossTLength.value())

        self.rectBy3Points_tool.rb_reset_signal.connect(self.reset_preview_data)
        self.rectByFixedExtentTool.rb_reset_signal.connect(self.reset_preview_data)
        self.rect_from_center_tool.rb_reset_signal.connect(self.reset_preview_data)
        self.rect_from_center_fixed_tool.rb_reset_signal.connect(self.reset_preview_data)

        self.rectBy3Points_tool.rbFinished.connect(self.create_mission_area)
        self.rect_from_center_tool.rbFinished.connect(self.create_mission_area)
        self.rectByFixedExtentTool.rbFinished.connect(self.create_mission_area)
        self.rect_from_center_fixed_tool.rbFinished.connect(self.create_mission_area)

        self.rectBy3Points_tool.msgbar.connect(self.pass_to_message_bar)
        self.rect_from_center_tool.msgbar.connect(self.pass_to_message_bar)
        self.rectByFixedExtentTool.msgbar.connect(self.pass_to_message_bar)
        self.rect_from_center_fixed_tool.msgbar.connect(self.pass_to_message_bar)

        self.mission_area_widget.alongTLength.valueChanged.connect(self.rect_from_center_fixed_tool.set_x_length)
        self.mission_area_widget.acrossTLength.valueChanged.connect(self.rect_from_center_fixed_tool.set_y_length)
        self.mission_area_widget.alongTLength.valueChanged.connect(self.rectByFixedExtentTool.set_x_length)
        self.mission_area_widget.acrossTLength.valueChanged.connect(self.rectByFixedExtentTool.set_y_length)

        self.mission_area_widget.drawRectangleButton.clicked.connect(self.draw_mission_area)
        self.mission_area_widget.centerOnTargetButton.clicked.connect(self.draw_mission_area)
        self.mission_area_widget.center_on_target_unchecked_signal.connect(self.unset_center_on_target_tool)
        self.mission_area_widget.draw_rectangle_unchecked_signal.connect(self.unset_draw_rectangle_tool)
        self.alongTSpace.valueChanged.connect(self.preview_tracks)
        self.fovValue.valueChanged.connect(self.preview_tracks)
        self.overlapValue.valueChanged.connect(self.preview_tracks)
        self.numAcrossTracks.valueChanged.connect(self.preview_tracks)
        self.speed_doubleSpinBox.valueChanged.connect(self.preview_tracks)
        self.heave_mode_widget.altitude_doubleSpinBox.valueChanged.connect(self.preview_tracks_if_sensor_coverage)

        self.fixedSpace.setChecked(True)
        self.bySensorCoverage.setChecked(False)
        self.sensor_coverage_widget.hide()

        self.fixedSpace.toggled.connect(self.preview_tracks)
        self.fixedSpace.toggled.connect(self.fixed_space_toggled)
        self.bySensorCoverage.toggled.connect(self.sensor_coverage_toggled)
        self.heave_mode_widget.heave_mode_changed_signal.connect(self.heave_mode_changed)
        if self.config is not None and self.vehicle_info is not None:
            self.mapping_widget.starting_actions_widget.set_config_and_vehicle_namespace(
                self.config,
                self.vehicle_info.get_vehicle_namespace()
            )
            self.mapping_widget.ending_actions_widget.set_config_and_vehicle_namespace(
                self.config,
                self.vehicle_info.get_vehicle_namespace()
            )

        self.heave_mode_widget.on_heave_mode_box_changed(0)

    def get_template_mission(self):
        return self.lawnmower.get_mission()

    def fixed_space_toggled(self):
        if self.fixedSpace.isChecked():
            self.bySensorCoverage.setChecked(False)
            self.fovLabel.setEnabled(False)
            self.fovValue.setEnabled(False)
            self.overlapValue.setEnabled(False)
            self.overlapLabel.setEnabled(False)
            self.alongTSpace.setEnabled(True)

    def sensor_coverage_toggled(self):
        if self.bySensorCoverage.isChecked():
            self.fixedSpace.setChecked(False)
            self.fovLabel.setEnabled(True)
            self.fovValue.setEnabled(True)
            self.overlapValue.setEnabled(True)
            self.overlapLabel.setEnabled(True)
            self.alongTSpace.setEnabled(False)

    def heave_mode_changed(self):
        if self.heave_mode_widget.get_heave_mode() == mission_types.HEAVE_MODE_DEPTH:
            self.bySensorCoverage.setEnabled(False)
            self.fovLabel.setEnabled(False)
            self.fovValue.setEnabled(False)
            self.overlapValue.setEnabled(False)
            self.overlapLabel.setEnabled(False)
            self.fixedSpace.setChecked(True)

        if self.heave_mode_widget.get_heave_mode() == mission_types.HEAVE_MODE_ALTITUDE:
            self.bySensorCoverage.setEnabled(True)

        if self.heave_mode_widget.get_heave_mode() == mission_types.HEAVE_MODE_BOTH:
            self.bySensorCoverage.setEnabled(True)

    def draw_mission_area(self):
        if (self.mission_area_widget.centerOnTargetButton.isChecked()
                or self.mission_area_widget.drawRectangleButton.isChecked()):
            self.onTarget = False
            sender = self.sender()
            # if not self.is_mission_area_defined():
            if self.mission_area_widget.automaticExtent.isChecked():
                if sender == self.mission_area_widget.drawRectangleButton:
                    self.msg_log.logMessage("Click starting point", "Lawn Mower", 0)
                    # Draw mission area
                    self.canvas.setMapTool(self.rectBy3Points_tool)
                elif sender == self.mission_area_widget.centerOnTargetButton:
                    self.onTarget = True
                    self.msg_log.logMessage("Click center point", "Lawn Mower", 0)
                    # Draw mission area
                    self.canvas.setMapTool(self.rect_from_center_tool)
                return

            elif self.mission_area_widget.fixedExtent.isChecked():
                if self.mission_area_widget.alongTLength.value() != 0.0 and self.mission_area_widget.acrossTLength.value() != 0.0:
                    if sender == self.mission_area_widget.drawRectangleButton:
                        self.msg_log.logMessage("Click starting point", "Lawn Mower", 0)

                        self.canvas.setMapTool(self.rectByFixedExtentTool)
                    elif sender == self.mission_area_widget.centerOnTargetButton:
                        self.onTarget = True
                        self.msg_log.logMessage("Click center point", "Lawn Mower", 0)

                        self.canvas.setMapTool(self.rect_from_center_fixed_tool)
                    return

                else:
                    QMessageBox.warning(None,
                                        "Mission Template",
                                        "<center>Track lengths must be different from zero. </center>",
                                        QMessageBox.Close)

                    return
            else:
                self.deactivate_tool()
                self.start_end_marker.hide_markers()
                self.rubber_band.reset(QgsWkbTypes.LineGeometry)
                self.rubber_band_points.reset(QgsWkbTypes.PointGeometry)

        else:
            if self.is_mission_area_defined():
                self.preview_tracks()

    def create_mission_area(self, geom):
        self.pass_to_message_bar("")
        if geom is not None:
            self.set_mission_area_defined(True)
            self.set_area_points(geom)
            self.preview_tracks()

    def preview_tracks_if_sensor_coverage(self):
        if self.bySensorCoverage.isChecked():
            self.preview_tracks()

    def preview_tracks(self):
        """ preview tracks on the canvas"""
        try:
            if self.is_mission_area_defined():
                if ((self.heave_mode_widget.get_heave_mode() == mission_types.HEAVE_MODE_ALTITUDE
                     or self.heave_mode_widget.get_heave_mode() == mission_types.HEAVE_MODE_BOTH)
                        and self.heave_mode_widget.get_altitude() == 0):
                    color = '#f6989d'  # set invalid color
                    self.heave_mode_widget.altitude_doubleSpinBox.setStyleSheet(f'QDoubleSpinBox {{ background-color: {color} }}')
                elif self.alongTSpace.value() == 0 and self.fixedSpace.isChecked():
                    color = '#f6989d'  # set invalid color
                    self.alongTSpace.setStyleSheet(f'QDoubleSpinBox {{ background-color: {color} }}')
                    self.reset_track()
                    self.preview_tracks_signal.emit()
                elif self.fovValue.value() == 0 and self.bySensorCoverage.isChecked():
                    color = '#f6989d'  # set invalid color
                    self.fovValue.setStyleSheet(f'QDoubleSpinBox {{ background-color: {color} }}')
                    self.reset_track()
                    self.preview_tracks_signal.emit()
                elif (type(self.lawnmower) == MappingLawnMower
                      and self.mapping_widget.get_distance_covered_area_to_turn() == 0):
                    color = '#f6989d'  # set invalid color
                    self.mapping_widget.distance_covered_to_turn_doubleSpinBox.setStyleSheet(
                        f'QDoubleSpinBox {{ background-color: {color} }}')
                    self.reset_track()
                    self.preview_tracks_signal.emit()
                else:
                    if self.alongTSpace.value() > 0:
                        self.alongTSpace.setStyleSheet(f'QDoubleSpinBox {{ background-color: #ffffff }}')
                    if self.fovValue.value() > 0:
                        self.fovValue.setStyleSheet(f'QDoubleSpinBox {{ background-color: #ffffff }}')

                    if (type(self.lawnmower) == MappingLawnMower
                            and self.mapping_widget.get_distance_covered_area_to_turn() > 0):
                        self.mapping_widget.distance_covered_to_turn_doubleSpinBox.setStyleSheet(
                            f'QDoubleSpinBox {{ background-color: #ffffff }}')
                        self.transect_to_turn_too_large = False

                    if type(self.lawnmower) == MappingLawnMower:
                        try:
                            wp_list, start_act_wp_list, end_act_wp_list = self.lawnmower.compute_tracks(
                                self.get_area_points(),
                                self.get_track_spacing(),
                                self.get_num_across_tracks(),
                                self.mapping_widget.get_distance_covered_area_to_turn(),
                                self.mapping_widget.is_turn_inside_zone())
                        except Exception as e:
                            color = '#f6989d'  # set invalid color
                            self.mapping_widget.distance_covered_to_turn_doubleSpinBox.setStyleSheet(
                                f'QDoubleSpinBox {{ background-color: {color} }}')
                            self.transect_to_turn_too_large = True
                            raise e
                    else:
                        wp_list = self.lawnmower.compute_tracks(self.get_area_points(),
                                                                self.get_track_spacing(),
                                                                self.get_num_across_tracks())
                    self.set_wp_list(wp_list)
                    self.lawnmower.track_to_mission(self.wp_list,
                                                    self.heave_mode_widget.get_depth(),
                                                    self.heave_mode_widget.get_altitude(),
                                                    self.heave_mode_widget.get_heave_mode(),
                                                    self.get_speed(),
                                                    self.get_xy_tolerance(),
                                                    self.get_no_altitude_goes_up())

                    if type(self.lawnmower) == MappingLawnMower:
                        for wp in start_act_wp_list:
                            for i in range(0, self.mapping_widget.starting_actions_widget.action_listWidget.count()):
                                self.lawnmower.get_mission().get_step(wp).add_action(
                                    self.mapping_widget.starting_actions_widget.action_listWidget.item(i).data(
                                        Qt.UserRole))

                        for wp in end_act_wp_list:
                            for i in range(0, self.mapping_widget.ending_actions_widget.action_listWidget.count()):
                                self.lawnmower.get_mission().get_step(wp).add_action(
                                    self.mapping_widget.ending_actions_widget.action_listWidget.item(i).data(
                                        Qt.UserRole))
                    # show rubber band with temporal tracks
                    self.rubber_band.reset(QgsWkbTypes.LineGeometry)
                    self.rubber_band_points.reset(QgsWkbTypes.PointGeometry)
                    polyline = [None] * len(self.wp_list)
                    for index, wp in enumerate(self.wp_list):
                        rb_wp = transform_point(
                            QgsPointXY(wp.x(), wp.y()),
                            QgsCoordinateReferenceSystem.fromEpsgId(4326),
                            self.canvas.mapSettings().destinationCrs()
                        )
                        polyline[index] = rb_wp

                    if self.rect_from_center_tool.rb is not None:
                        self.base_rb.setToGeometry(self.rect_from_center_tool.rb.asGeometry(), None)
                    if self.rect_from_center_fixed_tool.rb is not None:
                        self.base_rb.setToGeometry(self.rect_from_center_fixed_tool.rb.asGeometry(), None)
                    if self.rectBy3Points_tool.rb is not None:
                        self.base_rb.setToGeometry(self.rectBy3Points_tool.rb.asGeometry(), None)
                    if self.rectByFixedExtentTool.rb is not None:
                        self.base_rb.setToGeometry(self.rectByFixedExtentTool.rb.asGeometry(), None)

                    geom = QgsGeometry.fromPolylineXY(polyline)
                    self.rubber_band.addGeometry(geom)
                    self.rubber_band_points.addGeometry(geom)
                    self.rubber_band_points.show()
                    self.rubber_band.show()

                    self.start_end_marker.update_markers(self.wp_list)

                    self.unset_map_tool()
                    self.preview_tracks_signal.emit()
            else:
                raise Exception("Define first an area for the mission.")
        except Exception as e:
            self.reset_track()
            self.preview_tracks_signal.emit()
            logger.exception(e)
            self.lawnmower.mission = None
            QMessageBox.warning(None,
                                "Mission Template",
                                "<center>{}</center>".format(e),
                                QMessageBox.Close)

    def get_track_spacing(self):
        if self.fixedSpace.isChecked():
            return self.alongTSpace.value()

        elif self.bySensorCoverage.isChecked():
            return self.compute_track_spacing()

    def compute_track_spacing(self):
        altitude = self.heave_mode_widget.get_altitude()
        fov = self.fovValue.value()
        overlap_percent = self.overlapValue.value()
        footprint_dist = math.tan(math.radians(fov) / 2) * altitude
        overlap_dist = footprint_dist * overlap_percent / 100
        track_spacing = (footprint_dist - overlap_dist) * 2 + overlap_dist
        return track_spacing

    def get_num_across_tracks(self):
        return int(self.numAcrossTracks.value())

    def get_speed(self):
        """
        :return: Value of the speed box
        :rtype: float
        """
        return float(self.speed_doubleSpinBox.text())

    def get_xy_tolerance(self):
        """
        :return: Value of the tolerance in the x and y direction
        :rtype: float
        """
        return float(self.xy_tolerance_doubleSpinBox.text())

    def get_no_altitude_goes_up(self):
        return self.no_altitude_comboBox.currentText() == "Go up"

    def on_spinbox_changed(self):
        """ This function is called when the value of the spinbox is changed """
        sender = self.sender()
        state = validate_custom_double(str(sender.text()))
        if state == QValidator.Acceptable:
            if sender in (self.xy_tolerance_doubleSpinBox, self.mapping_widget.distance_covered_to_turn_doubleSpinBox):
                if self.mapping_widget.distance_covered_to_turn_doubleSpinBox.value() <= self.xy_tolerance_doubleSpinBox.value():
                    if not self.warning_displayed:
                        self.warning_displayed = True
                        reply = QMessageBox.warning(None, "Mission Warning",
                                                    "The mission distance between the transect and the turn "
                                                    "is less than or equal to the tolerance. "
                                                    "This could lead to undesired mission behaviour.")

    def delete_widget(self):
        self.delete_all(self.layout())
        self.deleteLater()
        self.close()

    def unset_map_tool(self):
        """
        Unset map tool from canvas.
        """
        self.mission_area_widget.uncheck_draw_rectangle_button()
        self.mission_area_widget.uncheck_center_on_target_button()

    def unset_draw_rectangle_tool(self):
        if not self.onTarget:
            if self.mission_area_widget.automaticExtent.isChecked():
                self.canvas.unsetMapTool(self.rectBy3Points_tool)
            if self.mission_area_widget.fixedExtent.isChecked():
                self.canvas.unsetMapTool(self.rectByFixedExtentTool)

    def unset_center_on_target_tool(self):
        if self.onTarget:
            if self.mission_area_widget.automaticExtent.isChecked():
                self.canvas.unsetMapTool(self.rect_from_center_tool)
            if self.mission_area_widget.fixedExtent.isChecked():
                self.canvas.unsetMapTool(self.rect_from_center_fixed_tool)

    def is_transect_group_valid(self):
        valid = False
        # if fixed space and valid or sensor coverage and valid
        if (self.heave_mode_widget.is_valid_altitude()
                and ((self.fixedSpace.isChecked() and self.alongTSpace.value() != 0)
                     or self.bySensorCoverage.isChecked() and self.fovValue.value() != 0)):
            valid = True
        return valid

    def is_valid(self):
        valid = False
        # if mission_area valid and transect valid and (mapping exist and valid)
        if self.is_mission_area_defined() and self.is_transect_group_valid():
            valid = True
            if type(self.lawnmower) == MappingLawnMower and self.mapping_widget.get_distance_covered_area_to_turn() == 0:
                valid = False

        return valid

    def deactivate_tool(self):
        """
        Deactivate tool.
        """
        if self.rectBy3Points_tool:
            self.rectBy3Points_tool.deactivate()
        elif self.rectByFixedExtentTool:
            self.rectByFixedExtentTool.deactivate()
        if self.rect_from_center_tool:
            self.rect_from_center_tool.deactivate()
        elif self.rect_from_center_fixed_tool:
            self.rect_from_center_fixed_tool.deactivate()

    def close(self):
        self.unset_map_tool()
        self.deactivate_tool()
        super().close()
