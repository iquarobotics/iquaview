# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 mapping lawn mower pattern definition
"""

import math
import logging

from iquaview_lib.cola2api.mission_types import (Mission,
                                                 MissionStep,
                                                 MissionGoto,
                                                 MissionSection)
from qgis.core import QgsDistanceArea, QgsProject, QgsCoordinateReferenceSystem

logger = logging.getLogger(__name__)


class MappingLawnMower:

    def __init__(self, canvas):
        self.wp = []
        self.mission = None
        crs = QgsCoordinateReferenceSystem.fromEpsgId(4326)
        self.distance = QgsDistanceArea()
        self.distance.setSourceCrs(crs, QgsProject.instance().transformContext())
        self.distance.setEllipsoid(QgsProject.instance().ellipsoid())

    def get_mission(self):
        return self.mission

    def compute_tracks(self,
                       area_points,
                       track_spacing,
                       num_across_tracks,
                       dist_between_covered_area_and_turn,
                       is_turn_inside_zone):
        """
             Compute lawn-mower tracks
            :param area_points: points defining the extent of the tracks, they should be in WGS 84 lat/lon.
                                first two points define the along track direction.
            :type area_points: list(QgsPointXY)
            :param track_spacing: desired space in meters between consecutive along tracks
            :type track_spacing: float
            :param num_across_tracks: number of desired across tracks. They will be equally spaced through the area.
            :type num_across_tracks: int
            :param dist_between_covered_area_and_turn: Transect to turn distance
            :type dist_between_covered_area_and_turn: float
            :param is_turn_inside_zone: True if the turn is inside zone, otherwise False
            :type is_turn_inside_zone: bool
            :return: list of ordered waypoints of the lawn-mower trajectory
            """
        wp_list = []
        mapping_starting_actions_wp_list = []
        mapping_ending_actions_wp_list = []

        dist_along_track = self.distance.measureLine(area_points[0], area_points[1])
        dist_across_track = self.distance.measureLine(area_points[1], area_points[2])
        bearing_along_track = self.distance.bearing(area_points[0], area_points[1])
        bearing_across_track = self.distance.bearing(area_points[1], area_points[2])

        turn_track_dist = length_one_across_track = track_spacing

        num_along_tracks = math.ceil(dist_across_track / length_one_across_track) + 1

        if not is_turn_inside_zone:
            dist_along_track += dist_between_covered_area_and_turn * 2
        logger.debug("distAlongTrack: {}".format(dist_along_track))
        logger.debug("distAcrossTrack: {}".format(dist_across_track))
        logger.debug("bearingAlongTrack {}:".format(bearing_along_track))
        logger.debug("bearingAcrossTrack {}:".format(bearing_across_track))
        logger.debug("numAlongTrack: {}".format(num_along_tracks))
        logger.debug("numAcrossTrack: {}".format(num_across_tracks))
        logger.debug("lenghtOneAcrossTrack {}".format(length_one_across_track))

        if not is_turn_inside_zone:
            current_wp = self.distance.measureLineProjected(area_points[0],
                                                            -dist_between_covered_area_and_turn,
                                                            bearing_along_track)[1]
            next_wp = self.distance.measureLineProjected(area_points[1],
                                                         dist_between_covered_area_and_turn,
                                                         bearing_along_track)[1]
        else:
            # Initialize with 2 first points
            current_wp = area_points[0]
            next_wp = area_points[1]
        reverse_direction_along = False  # For changing direction of alternate along tracks
        wp_list.append(current_wp)
        wp = self.distance.measureLineProjected(current_wp,
                                                dist_between_covered_area_and_turn,
                                                bearing_along_track)[1]
        wp_two = self.distance.measureLineProjected(next_wp,
                                                    -dist_between_covered_area_and_turn,
                                                    bearing_along_track)[1]
        wp_list.append(wp)
        mapping_starting_actions_wp_list.append(len(wp_list) - 1)
        wp_list.append(wp_two)
        mapping_ending_actions_wp_list.append(len(wp_list) - 1)

        distance_a = self.distance.measureLine(current_wp, wp)
        distance_b = self.distance.measureLine(next_wp, wp_two)
        distance_c = self.distance.measureLine(current_wp, next_wp)
        if distance_c < (distance_a + distance_b):
            raise Exception("Transect to turn distance has to be less than the distance between the paths.")

        # Loop to generate all waypoints of along tracks
        for i in range(int(num_along_tracks) * 2 - 1):

            wp_list.append(next_wp)
            current_wp = next_wp

            if i % 2 == 0:  # even, so we just did along track, next draw across track
                next_wp = self.distance.measureLineProjected(current_wp,
                                                             length_one_across_track,
                                                             bearing_across_track)[1]
                reverse_direction_along = not reverse_direction_along
            else:
                wp = self.distance.measureLineProjected(current_wp,
                                                        dist_between_covered_area_and_turn,
                                                        bearing_along_track + int(
                                                            reverse_direction_along) * math.radians(180.0))[1]
                wp_list.append(wp)
                mapping_starting_actions_wp_list.append(len(wp_list) - 1)

                # Draw along track
                next_wp = self.distance.measureLineProjected(current_wp,
                                                             dist_along_track,
                                                             bearing_along_track + int(
                                                                 reverse_direction_along) * math.radians(180.0))[1]
                wp_two = self.distance.measureLineProjected(next_wp,
                                                            -dist_between_covered_area_and_turn,
                                                            bearing_along_track + int(
                                                                reverse_direction_along) * math.radians(180.0))[1]
                wp_list.append(wp_two)
                mapping_ending_actions_wp_list.append(len(wp_list) - 1)

        # Loop for across tracks
        if num_across_tracks > 0:
            dist_along_track_for_across = dist_along_track / (num_across_tracks + 1)
            # Distance across track after computing all the along tracks
            # (might not match with the across track distance defined on the area mission)
            if reverse_direction_along:
                if not is_turn_inside_zone:
                    wp_one = self.distance.measureLineProjected(wp_list[0],
                                                                - dist_between_covered_area_and_turn,
                                                                bearing_across_track)[1]
                    wp_two = self.distance.measureLineProjected(wp_list[-4],
                                                                dist_between_covered_area_and_turn,
                                                                bearing_across_track)[1]
                else:
                    wp_one = wp_list[0]
                    wp_two = wp_list[-4]

                real_dist_across_track = self.distance.measureLine(wp_one, wp_two)
            else:
                if not is_turn_inside_zone:
                    wp_one = self.distance.measureLineProjected(wp_list[0],
                                                                -dist_between_covered_area_and_turn,
                                                                bearing_across_track)[1]
                    wp_two = self.distance.measureLineProjected(wp_list[-1],
                                                                dist_between_covered_area_and_turn,
                                                                bearing_across_track)[1]
                else:
                    wp_one = wp_list[0]
                    wp_two = wp_list[-1]
                real_dist_across_track = self.distance.measureLine(wp_one, wp_two)

            if not is_turn_inside_zone:
                next_wp = self.distance.measureLineProjected(current_wp,
                                                             turn_track_dist + dist_between_covered_area_and_turn,
                                                             bearing_across_track)[1]
            else:
                next_wp = self.distance.measureLineProjected(current_wp, turn_track_dist, bearing_across_track)[1]
            reverse_direction_across = True  # For changing direction of alternate across tracks

            for i in range(num_across_tracks * 2 + 1):
                wp_list.append(next_wp)
                current_wp = next_wp
                if i % 2 == 0:  # Compute waypoint along track
                    next_wp = self.distance.measureLineProjected(current_wp,
                                                                 dist_along_track_for_across,
                                                                 bearing_along_track + int(
                                                                     reverse_direction_along) * math.radians(180.0))[1]
                else:  # Compute waypoint across track
                    wp = self.distance.measureLineProjected(current_wp,
                                                            dist_between_covered_area_and_turn,
                                                            bearing_across_track + int(
                                                                reverse_direction_across) * math.radians(180.0))[1]
                    wp_list.append(wp)
                    mapping_starting_actions_wp_list.append(len(wp_list) - 1)

                    next_wp = self.distance.measureLineProjected(current_wp,
                                                                 real_dist_across_track + 2 * turn_track_dist,
                                                                 bearing_across_track + int(
                                                                     reverse_direction_across) * math.radians(180.0))[1]

                    wp_two = self.distance.measureLineProjected(next_wp,
                                                                -dist_between_covered_area_and_turn,
                                                                bearing_across_track + int(
                                                                    reverse_direction_across) * math.radians(
                                                                    180.0))[1]
                    wp_list.append(wp_two)
                    mapping_ending_actions_wp_list.append(len(wp_list) - 1)

                    reverse_direction_across = not reverse_direction_across
        return wp_list, mapping_starting_actions_wp_list, mapping_ending_actions_wp_list

    def track_to_mission(self, wp_list, depth, altitude, heave_mode, speed, tolerance_xy, no_altitude_goes_up):
        """
        Creates a misison with a list of waypoints and the various parameters of a mission
        :param wp_list: Waypoints of the mission
        :type wp_list: list
        :param depth: depth of the mission
        :type depth: float
        :param altitude: Altitude of the mission
        :type altitude: float
        :param heave_mode: heave mode of the mission (depth, altitude, both)
        :type heave_mode: int
        :param speed: speed of the mission
        :type speed: float
        :param tolerance_xy: tolerance in the x direction
        :type tolerance_xy: float
        :param no_altitude_goes_up: reaction of auv when no altitude (Go up, Ignore)
        :type no_altitude_goes_up: bool

        """
        self.mission = Mission()
        for wp in range(len(wp_list)):
            if wp == 0:
                # first step type waypoint
                step = MissionStep()
                mwp = MissionGoto(wp_list[wp].y(), wp_list[wp].x(), depth, altitude,
                                  heave_mode,
                                  speed,
                                  tolerance_xy, no_altitude_goes_up)
                step.add_maneuver(mwp)
                self.mission.add_step(step)
            else:
                # rest of steps type section
                step = MissionStep()
                mwp = MissionSection(wp_list[wp - 1].y(), wp_list[wp - 1].x(), depth,
                                     wp_list[wp].y(), wp_list[wp].x(), depth, altitude,
                                     heave_mode,
                                     speed,
                                     tolerance_xy, no_altitude_goes_up)
                step.add_maneuver(mwp)
                self.mission.add_step(step)
