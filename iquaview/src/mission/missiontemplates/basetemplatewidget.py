# -*- coding: utf-8 -*-
# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
Base template widget.
"""
from __future__ import annotations

import logging

from PyQt5.QtCore import pyqtSignal, Qt
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QWidget
from qgis.core import QgsWkbTypes, QgsPointXY
from qgis.gui import QgsRubberBand

from iquaview.src.mission.startendmarker import StartEndMarker

logger = logging.getLogger(__name__)


class BaseTemplateWidget(QWidget):
    """ Base template widget. Required to implement a mission template."""
    preview_tracks_signal = pyqtSignal()

    def __init__(self, canvas, msg_log, template_name="Base template", parent=None):
        """
        Constructor for the BaseTemplateWidget.

        :param canvas: canvas to draw.
        :type canvas: QgsMapCanvas
        :param msg_log: Message log to write information.
        :type msg_log: QgsMessageLog
        :param template_name: (Optional) Description of the template_name parameter. Default is "Base template".
        :type template_name: str
        :param parent: (Optional) Parent widget. Default is None.
        """
        super().__init__(parent)
        self.setAttribute(Qt.WA_DeleteOnClose)
        self.canvas = canvas
        self.wp_list = []
        self.mission_area_defined = False
        self.area_points = None
        self.template_name = template_name
        self.msg_log = msg_log

        self.start_end_marker = StartEndMarker(self.canvas, self.wp_list, QColor("green"))

        red = QColor("red")
        red.setAlpha(128)
        self.base_rb = QgsRubberBand(self.canvas, QgsWkbTypes.PolygonGeometry)
        self.base_rb.setWidth(1)
        self.base_rb.setColor(red)

        self.rubber_band = QgsRubberBand(self.canvas, QgsWkbTypes.LineGeometry)
        self.rubber_band.setWidth(2)
        self.rubber_band.setColor(QColor("green"))

        self.rubber_band_points = QgsRubberBand(self.canvas, QgsWkbTypes.PointGeometry)
        self.rubber_band_points.setColor(QColor("green"))
        self.rubber_band_points.setIcon(QgsRubberBand.ICON_CIRCLE)
        self.rubber_band_points.setIconSize(10)

    def reset_track(self) -> None:
        """ Reset rubber band track and points."""
        self.start_end_marker.hide_markers()

        if self.rubber_band is not None:
            self.rubber_band.reset(QgsWkbTypes.LineGeometry)

        if self.rubber_band_points is not None:
            self.rubber_band_points.reset(QgsWkbTypes.PointGeometry)

    def reset_preview_data(self) -> None:
        """ Resets rubber bands and objects containing area info """

        if self.base_rb is not None:
            self.base_rb.reset(QgsWkbTypes.PolygonGeometry)

        self.mission_area_defined = False
        self.area_points = None

        self.reset_track()

    def set_wp_list(self, wp_list) -> None:
        """
        Set waypoint list.
        :param wp_list: waypoint list.
        :type wp_list: list
        """
        self.wp_list = wp_list

    def set_area_points(self, geom) -> None:
        """
        Set mission as defined and save the area points from geometry.
        :param geom: geom is the geometry.
        :type geom: QgsGeometry
        """
        # Store points to variables
        area_points = []
        for vertex in geom.vertices():
            area_points.append(QgsPointXY(vertex))
        self.area_points = area_points[:-1]

        self.mission_area_defined = True

    def get_area_points(self) -> list:
        """
        :return: The list of points of the defined mission area.
        :rtype: list
        """
        return self.area_points

    def is_valid(self) -> bool:
        raise NotImplementedError

    def is_mission_area_defined(self) -> bool:
        """
        True if the mission area is defined, False otherwise.
        :return: return True if the mission area is defined, False otherwise.
        :rtype bool
        """
        return self.mission_area_defined

    def set_mission_area_defined(self, state: bool) -> None:
        """
        Set mission area defined.
        :param state: set to defined or to not defined.
        :type state: bool
        """
        self.mission_area_defined = state

    def pass_to_message_bar(self, msg: str) -> None:
        """
        Given a message pass the message to the bar.
        :param msg: message to display
        :type msg: str
        """
        self.msg_log.logMessage("")
        self.msg_log.logMessage(msg, self.template_name, 0)

    def delete_all(self, layout) -> None:
        """
        Delete all widget from layout.
        :param layout: layout is a qt layout
        """
        if layout is not None:
            for i in reversed(range(layout.count())):
                item = layout.takeAt(i)
                widget = item.widget()
                if widget is not None:
                    widget.deleteLater()
                else:
                    self.delete_all(item.layout())

    def close(self) -> None:
        """ Deactivates tools, removes rubber bands and closes the widget """

        self.start_end_marker.close_markers()

        if self.base_rb is not None:
            self.canvas.scene().removeItem(self.base_rb)
            del self.base_rb
            self.base_rb = None
        if self.rubber_band is not None:
            self.canvas.scene().removeItem(self.rubber_band)
            del self.rubber_band
            self.rubber_band = None
        if self.rubber_band_points is not None:
            self.canvas.scene().removeItem(self.rubber_band_points)
            del self.rubber_band_points
            self.rubber_band_points = None
