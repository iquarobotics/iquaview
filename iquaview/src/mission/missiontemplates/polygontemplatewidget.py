# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Rectangle pattern definition
"""
import logging

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QMessageBox
from qgis.core import QgsPointXY, QgsWkbTypes, QgsDistanceArea, QgsProject, QgsCoordinateReferenceSystem, QgsGeometry
from qgis.gui import QgsRubberBand

from iquaview.src.mission.maptools.polygontools import (PolygonBy3PointsTool,
                                                        PolygonFromCenterTool,
                                                        PolygonFromCenterFixedTool,
                                                        PolygonByFixedExtentTool)
from iquaview.src.ui.ui_polygontemplatewidget import Ui_PolygonTemplateWidget
from iquaview_lib.cola2api.mission_types import (Mission,
                                                 MissionStep,
                                                 MissionGoto,
                                                 MissionSection,
                                                 HEAVE_MODE_ALTITUDE,
                                                 HEAVE_MODE_BOTH)
from iquaview_lib.utils.qgisutils import transform_point
from iquaview.src.mission.missiontemplates.basetemplatewidget import BaseTemplateWidget

logger = logging.getLogger(__name__)


class PolygonTemplateWidget(BaseTemplateWidget, Ui_PolygonTemplateWidget):

    def __init__(self, canvas, msg_log, template_name="Polygon template", parent=None):
        """
        Init of the object PolygonTemplateWidget
        :param canvas: Canvas to draw objects
        :type canvas: QgsMapCanvas
        :param msg_log: Log to write messages
        :type msg_log: QgsMessageLog
        """
        if template_name == "Star template":
            self.template_type = "star"
        else:
            self.template_type = "ellipse"

        super().__init__(canvas, msg_log, template_name, parent)
        self.setupUi(self)
        self.initial_point = None

        self.mission = None
        crs = QgsCoordinateReferenceSystem.fromEpsgId(4326)
        self.distance = QgsDistanceArea()
        self.distance.setSourceCrs(crs, QgsProject.instance().transformContext())
        self.distance.setEllipsoid(QgsProject.instance().ellipsoid())

        self.initial_point = QgsRubberBand(self.canvas, QgsWkbTypes.PointGeometry)
        self.initial_point.setColor(QColor("red"))
        self.initial_point.setIcon(QgsRubberBand.ICON_CIRCLE)
        self.initial_point.setIconSize(10)

        self.onTarget = False

        color = QColor(255, 0, 0, 128)
        self.bounding_box_rb = QgsRubberBand(self.canvas, QgsWkbTypes.PolygonGeometry)
        self.bounding_box_rb.setColor(color)
        self.bounding_box_rb.setWidth(1)
        self.bounding_box_rb.setLineStyle(Qt.DashLine)
        self.bounding_box_rb.setFillColor(Qt.transparent)

        # Initialize tools and signals
        # Draw polygon not fixed tool
        self.polygon_by_3_points_tool = PolygonBy3PointsTool(self.canvas,
                                                             self.number_points_spinBox.value(),
                                                             self.template_type)
        self.polygon_by_3_points_tool.message_signal.connect(self.pass_to_message_bar)
        self.polygon_by_3_points_tool.rb_reset_signal.connect(self.reset_preview_data)
        self.polygon_by_3_points_tool.geometry_finished_signal.connect(self.create_mission_area)

        # Draw polygon from center not fixed tool
        self.polygon_from_center_tool = PolygonFromCenterTool(self.canvas,
                                                              self.number_points_spinBox.value(),
                                                              self.template_type)
        self.polygon_from_center_tool.message_signal.connect(self.pass_to_message_bar)
        self.polygon_from_center_tool.rb_reset_signal.connect(self.reset_preview_data)
        self.polygon_from_center_tool.geometry_finished_signal.connect(self.create_mission_area)

        # Draw polygon from center fixed size tool
        self.polygon_from_center_fixed_tool = PolygonFromCenterFixedTool(self.canvas,
                                                                         self.mission_area_widget.alongTLength.value(),
                                                                         self.mission_area_widget.acrossTLength.value(),
                                                                         self.number_points_spinBox.value(),
                                                                         self.template_type)
        self.polygon_from_center_fixed_tool.message_signal.connect(self.pass_to_message_bar)
        self.polygon_from_center_fixed_tool.rb_reset_signal.connect(self.reset_preview_data)
        self.polygon_from_center_fixed_tool.geometry_finished_signal.connect(self.create_mission_area)

        # Draw polygon fixed size tool
        self.polygon_by_fixed_extent_tool = PolygonByFixedExtentTool(self.canvas,
                                                                     self.mission_area_widget.alongTLength.value(),
                                                                     self.mission_area_widget.acrossTLength.value(),
                                                                     self.number_points_spinBox.value(),
                                                                     self.template_type)
        self.polygon_by_fixed_extent_tool.message_signal.connect(self.pass_to_message_bar)
        self.polygon_by_fixed_extent_tool.rb_reset_signal.connect(self.reset_preview_data)
        self.polygon_by_fixed_extent_tool.geometry_finished_signal.connect(self.create_mission_area)

        self.mission_area_widget.drawRectangleButton.clicked.connect(self.draw_mission_area)
        self.mission_area_widget.centerOnTargetButton.clicked.connect(self.draw_mission_area)
        self.mission_area_widget.center_on_target_unchecked_signal.connect(self.unset_center_on_target_tool)
        self.mission_area_widget.draw_rectangle_unchecked_signal.connect(self.unset_draw_rectangle_tool)

        self.mission_area_widget.alongTLength.valueChanged.connect(self.polygon_from_center_fixed_tool.set_x_length)
        self.mission_area_widget.acrossTLength.valueChanged.connect(self.polygon_from_center_fixed_tool.set_y_length)
        self.mission_area_widget.alongTLength.valueChanged.connect(self.polygon_by_fixed_extent_tool.set_x_length)
        self.mission_area_widget.acrossTLength.valueChanged.connect(self.polygon_by_fixed_extent_tool.set_y_length)

        self.number_points_spinBox.valueChanged.connect(self.update_segments_to_tools)
        self.number_points_spinBox.valueChanged.connect(self.update_initial_point_spinBox)
        self.initial_point_spinBox.valueChanged.connect(self.change_initial_point)
        self.number_points_spinBox.valueChanged.connect(self.preview_tracks)
        self.initial_point_spinBox.valueChanged.connect(self.preview_tracks)
        self.speed_doubleSpinBox.valueChanged.connect(self.preview_tracks)

        self.heave_mode_widget.on_heave_mode_box_changed(0)
        self.update_initial_point_spinBox()

    def get_template_mission(self):
        """
        Returns a mission that contains the waypoints in the template
        :return: The mission template
        :rtype: Mission
        """
        return self.mission

    def draw_mission_area(self):
        """
        Called when a tool is selected.
        Change tools and clears previous drawings and variables from the other tool
        """
        if (self.mission_area_widget.centerOnTargetButton.isChecked()
                or self.mission_area_widget.drawRectangleButton.isChecked()):
            self.onTarget = False
            sender = self.sender()
            # if not self.is_mission_area_defined():
            if self.mission_area_widget.automaticExtent.isChecked():
                if sender == self.mission_area_widget.drawRectangleButton:
                    self.msg_log.logMessage("Click starting point", "Polygon Template", 0)
                    # Draw mission area
                    self.canvas.setMapTool(self.polygon_by_3_points_tool)
                elif sender == self.mission_area_widget.centerOnTargetButton:
                    self.onTarget = True
                    self.msg_log.logMessage("Click center point", "Polygon Template", 0)
                    # Draw mission area
                    self.canvas.setMapTool(self.polygon_from_center_tool)
                return

            elif self.mission_area_widget.fixedExtent.isChecked():
                if self.mission_area_widget.alongTLength.value() != 0.0 and self.mission_area_widget.acrossTLength.value() != 0.0:
                    if sender == self.mission_area_widget.drawRectangleButton:
                        self.msg_log.logMessage("Click starting point", "Polygon Template", 0)

                        self.canvas.setMapTool(self.polygon_by_fixed_extent_tool)
                    elif sender == self.mission_area_widget.centerOnTargetButton:
                        self.onTarget = True
                        self.msg_log.logMessage("Click center point", "Polygon Template", 0)
                        self.canvas.setMapTool(self.polygon_from_center_fixed_tool)
                    return

                else:
                    QMessageBox.warning(None,
                                        "Mission Template",
                                        "<center>Track lengths must be different from zero. </center>",
                                        QMessageBox.Close)

                    return
            else:

                self.deactivate_tool()
                self.update_initial_point_spinBox()
                self.start_end_marker.hide_markers()
                self.rubber_band.reset(QgsWkbTypes.LineGeometry)
                self.rubber_band_points.reset(QgsWkbTypes.PointGeometry)

        else:
            if self.is_mission_area_defined():
                self.preview_tracks()

    def clear_initial_point(self):
        """
        Reset the initial point rubber band from canvas
        """
        if self.initial_point is not None:
            self.initial_point.reset(QgsWkbTypes.PointGeometry)

    def reset_preview_data(self):
        """ Resets rubber bands and objects containing area info """
        self.clear_initial_point()
        super().reset_preview_data()
        if self.bounding_box_rb is not None:
            self.bounding_box_rb.reset(QgsWkbTypes.PolygonGeometry)
        self.mission = None

    def change_initial_point(self):
        """
        Change the initial point
        """
        self.clear_initial_point()
        if self.area_points is not None:
            index = self.initial_point_spinBox.value()
            if index > self.number_points_spinBox.value():
                index = self.number_points_spinBox.value()
            index = index - 1  # numbers start at 1 but our lists at 0
            init_point = transform_point(
                QgsPointXY(self.area_points[index].x(), self.area_points[index].y()),
                QgsCoordinateReferenceSystem.fromEpsgId(4326),
                self.canvas.mapSettings().destinationCrs()
            )
            self.initial_point.addPoint(init_point)

    def create_mission_area(self, geom):
        """
        Creates a mission area with the geometry specified
        :param geom: geometry of the area
        :type geom: QgsGeometry
        """
        self.pass_to_message_bar("")
        self.clear_initial_point()
        if geom is not None:
            self.set_mission_area_defined(True)
            self.set_area_points(geom)
            self.change_initial_point()
            self.preview_tracks()
            #
            # if self.rubber_band is not None and self.rubber_band_points is not None:
            #     transparent_green = QColor(0, 128, 0, 64)
            #     self.rubber_band.setColor(transparent_green)
            #     self.rubber_band_points.setColor(transparent_green)

    def preview_tracks(self):
        """ preview tracks on the canvas"""
        try:
            if self.is_mission_area_defined() and self.area_points is not None:
                if ((self.heave_mode_widget.get_heave_mode() == HEAVE_MODE_ALTITUDE
                     or self.heave_mode_widget.get_heave_mode() == HEAVE_MODE_BOTH)
                        and self.heave_mode_widget.get_altitude() == 0):
                    QMessageBox.warning(None,
                                        "Mission Template",
                                        "<center>Altitude must be different from zero. </center>",
                                        QMessageBox.Close)
                else:
                    self.set_wp_list(self.compute_tracks(self.get_area_points()))
                    self.track_to_mission(self.wp_list, self.heave_mode_widget.get_depth(),
                                          self.heave_mode_widget.get_altitude(),
                                          self.heave_mode_widget.get_heave_mode(),
                                          self.get_speed(),
                                          self.get_xy_tolerance(),
                                          self.get_no_altitude_goes_up())

                    # show rubber band with temporal tracks
                    self.rubber_band.reset(QgsWkbTypes.LineGeometry)
                    self.rubber_band_points.reset(QgsWkbTypes.PointGeometry)
                    self.change_initial_point()
                    polyline = [None] * len(self.wp_list)
                    for index, wp in enumerate(self.wp_list):
                        rb_wp = transform_point(
                            QgsPointXY(wp.x(), wp.y()),
                            QgsCoordinateReferenceSystem.fromEpsgId(4326),
                            self.canvas.mapSettings().destinationCrs()
                        )
                        polyline[index] = rb_wp

                    area_points_transformed = [None] * len(self.get_area_points())
                    for index_ap, wp_ap in enumerate(self.get_area_points()):
                        rb_wp = transform_point(
                            QgsPointXY(wp_ap.x(), wp_ap.y()),
                            QgsCoordinateReferenceSystem.fromEpsgId(4326),
                            self.canvas.mapSettings().destinationCrs()
                        )
                        area_points_transformed[index_ap] = rb_wp

                    if self.polygon_from_center_tool.polygon_rb is not None:
                        self.bounding_box_rb.setToGeometry(self.polygon_from_center_tool.rectangle_rb.asGeometry())
                    if self.polygon_from_center_fixed_tool.polygon_rb is not None:
                        self.bounding_box_rb.setToGeometry(self.polygon_from_center_fixed_tool.rectangle_rb.asGeometry())
                    if self.polygon_by_3_points_tool.polygon_rb is not None:
                        self.bounding_box_rb.setToGeometry(self.polygon_by_3_points_tool.rectangle_rb.asGeometry())
                    if self.polygon_by_fixed_extent_tool.polygon_rb is not None:
                        self.bounding_box_rb.setToGeometry(self.polygon_by_fixed_extent_tool.rectangle_rb.asGeometry())

                    self.base_rb.setToGeometry(QgsGeometry.fromPolygonXY([area_points_transformed]))
                    geom = QgsGeometry.fromPolylineXY(polyline)
                    self.rubber_band.addGeometry(geom)
                    self.rubber_band_points.addGeometry(geom)
                    self.rubber_band_points.show()
                    self.rubber_band.show()
                    self.start_end_marker.update_markers(self.wp_list)

                    self.rubber_band.setColor(QColor("green"))
                    self.rubber_band_points.setColor(QColor("green"))

                    # self.hide_map_tool_bands()
                    self.unset_map_tool()
                    self.preview_tracks_signal.emit()

            else:
                raise Exception("Define first an area for the mission.")

        except Exception as e:
            self.reset_track()
            self.preview_tracks_signal.emit()
            logger.exception(e)
            self.mission = None
            QMessageBox.warning(None,
                                "Mission Template",
                                "<center>{}</center>".format(e),
                                QMessageBox.Close)

    def get_speed(self):
        """
        :return: Value of the speed box
        :rtype: float
        """
        return float(self.speed_doubleSpinBox.text())

    def get_xy_tolerance(self):
        """
        :return: Value of the tolerance in the x and y direction
        :rtype: float
        """
        return float(self.xy_tolerance_doubleSpinBox.text())

    def get_no_altitude_goes_up(self):
        return self.no_altitude_comboBox.currentText() == "Go up"

    def compute_tracks(self, area_points):

        """
        Compute polygon tracks
        :param area_points: points defining the extent of the tracks, they should be in WGS 84 lat/lon.
                            first two points define the along track direction.
        :return: list of ordered waypoints of polygon trajectory
        """

        index = self.initial_point_spinBox.value() - 1
        new_area = area_points[index:] + area_points[:index + 1]

        return new_area

    def update_initial_point_spinBox(self):
        """
        Updates the maximum initial point spinBox value with the total number of vertexes
        """
        segments = self.number_points_spinBox.value()
        if self.initial_point_spinBox.value() > segments:
            self.initial_point_spinBox.setValue(segments)
        self.initial_point_spinBox.setMaximum(segments)

    def track_to_mission(self, wp_list, depth, altitude, heave_mode, speed, tolerance_xy, no_altitude_goes_up):

        """
        Creates a misison with a list of waypoints and the various parameters of a mission
        :param wp_list: Waypoints of the mission
        :type wp_list: list
        :param depth: depth of the mission
        :type depth: float
        :param altitude: Altitude of the mission
        :type altitude: float
        :param heave_mode: heave mode of the mission (depth, altitude, both)
        :type heave_mode: int
        :param speed: speed of the mission
        :type speed: float
        :param tolerance_xy: tolerance in the x direction
        :type tolerance_xy: float
        :param no_altitude_goes_up: reaction of auv when no altitude (Go up, Ignore)
        :type no_altitude_goes_up: bool

        """
        self.mission = Mission()
        for wp in range(len(wp_list)):
            if wp == 0:
                # first step type waypoint
                step = MissionStep()
                mwp = MissionGoto(wp_list[wp].y(), wp_list[wp].x(), depth, altitude,
                                  heave_mode,
                                  speed,
                                  tolerance_xy, no_altitude_goes_up)
                step.add_maneuver(mwp)
                self.mission.add_step(step)
            else:
                # rest of steps type section
                step = MissionStep()
                mwp = MissionSection(wp_list[wp - 1].y(), wp_list[wp - 1].x(), depth,
                                     wp_list[wp].y(), wp_list[wp].x(), depth, altitude,
                                     heave_mode,
                                     speed,
                                     tolerance_xy, no_altitude_goes_up)
                step.add_maneuver(mwp)
                self.mission.add_step(step)

    def delete_widget(self):
        """ Deletes widgets, layouts and itself """
        self.delete_all(self.layout())
        self.deleteLater()
        self.close()

    def unset_map_tool(self):
        """
        Unset map tool from canvas.
        """
        self.mission_area_widget.uncheck_draw_rectangle_button()
        self.mission_area_widget.uncheck_center_on_target_button()

    def unset_draw_rectangle_tool(self):
        if not self.onTarget:
            if self.mission_area_widget.automaticExtent.isChecked():
                self.canvas.unsetMapTool(self.polygon_by_3_points_tool)
            if self.mission_area_widget.fixedExtent.isChecked():
                self.canvas.unsetMapTool(self.polygon_by_fixed_extent_tool)

    def unset_center_on_target_tool(self):
        if self.onTarget:
            if self.mission_area_widget.automaticExtent.isChecked():
                self.canvas.unsetMapTool(self.polygon_from_center_tool)
            if self.mission_area_widget.fixedExtent.isChecked():
                self.canvas.unsetMapTool(self.polygon_from_center_fixed_tool)

    def update_segments_to_tools(self, segments):
        """
        Sets the number of segments for polygon tools to the provided value
        :param segments: number of segments
        :type segments: int
        """
        self.polygon_by_3_points_tool.set_segments(segments, self.bounding_box_rb.asGeometry())
        self.polygon_from_center_tool.set_segments(segments, self.bounding_box_rb.asGeometry())
        self.polygon_by_fixed_extent_tool.set_segments(segments, self.bounding_box_rb.asGeometry())
        self.polygon_from_center_fixed_tool.set_segments(segments, self.bounding_box_rb.asGeometry())

        self.polygon_from_center_tool.create_polygon_and_emit(segments,self.bounding_box_rb.asGeometry())
        self.polygon_from_center_fixed_tool.create_polygon_and_emit(segments,self.bounding_box_rb.asGeometry())
        self.polygon_by_3_points_tool.create_polygon_and_emit(segments, self.bounding_box_rb.asGeometry())
        self.polygon_by_fixed_extent_tool.create_polygon_and_emit(segments, self.bounding_box_rb.asGeometry())

    def hide_map_tool_bands(self):
        """
        Hides maptool's bands.
        """
        if self.canvas.mapTool() == self.polygon_by_3_points_tool:
            self.polygon_by_3_points_tool.hide_bands()
        elif self.canvas.mapTool() == self.polygon_from_center_tool:
            self.polygon_from_center_tool.hide_bands()
        elif self.canvas.mapTool() == self.polygon_by_fixed_extent_tool:
            self.polygon_by_fixed_extent_tool.hide_bands()
        elif self.canvas.mapTool() == self.polygon_from_center_fixed_tool:
            self.polygon_from_center_fixed_tool.hide_bands()

    def deactivate_tool(self):
        """
        Deactivate tool.
        """
        if self.polygon_by_3_points_tool:
            self.polygon_by_3_points_tool.deactivate()
        elif self.polygon_by_fixed_extent_tool:
            self.polygon_by_fixed_extent_tool.deactivate()
        if self.polygon_from_center_tool:
            self.polygon_from_center_tool.deactivate()
        elif self.polygon_from_center_fixed_tool:
            self.polygon_from_center_fixed_tool.deactivate()

    def is_valid(self) -> bool:
        return self.is_mission_area_defined()

    def close(self):
        """ Deactivates tools, removes rubber bands and closes the widget """

        if self.bounding_box_rb is not None:
            self.canvas.scene().removeItem(self.bounding_box_rb)
            del self.bounding_box_rb
            self.bounding_box_rb = None
        self.unset_map_tool()
        self.deactivate_tool()
        if self.initial_point is not None:
            self.canvas.scene().removeItem(self.initial_point)
            del self.initial_point
            self.initial_point = None
        super().close()
