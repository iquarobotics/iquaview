# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Rectangle pattern definition
"""
import math
from qgis.core import QgsPointXY, QgsCoordinateReferenceSystem, QgsGeometry, QgsProject, QgsDistanceArea

from iquaview_lib.utils.qgisutils import transform_point
from iquaview.src.mission.missiontemplates.polygontemplatewidget import PolygonTemplateWidget
from iquaview_lib.utils.calcutils import get_angle_of_line_between_two_points,distance_ellipsoid, endpoint_sphere
from cola2.utils.angles import wrap_angle


class StarTemplateWidget(PolygonTemplateWidget):

    def __init__(self, canvas, msg_log, parent=None):
        """
        Init of the object StarTemplateWidget
        :param canvas: Canvas to draw objects
        :type canvas: QgsMapCanvas
        :param msg_log: Log to write messages
        :type msg_log: QgsMessageLog
        """
        # Call the constructors of both base classes
        PolygonTemplateWidget.__init__(self, canvas, msg_log, "Star template", parent)
        self.number_points_label.setText("Number of directions:")
        self.InitialPointgroupBox.setTitle("Define Star:")

    def change_initial_point(self):
        """
        Change the initial point
        """
        self.clear_initial_point()
        if self.area_points is not None:
            index = self.initial_point_spinBox.value()
            if index > self.number_points_spinBox.value() * 2:
                index = self.number_points_spinBox.value()
            index = index - 1  # numbers start at 1 but our lists at 0

            init_point = transform_point(
                QgsPointXY(self.area_points[index].x(), self.area_points[index].y()),
                QgsCoordinateReferenceSystem.fromEpsgId(4326),
                self.canvas.mapSettings().destinationCrs()
            )

            self.initial_point.addPoint(init_point)

    def compute_tracks(self, area_points):

        """
        Compute star tracks
        :param area_points: points defining the extent of the tracks, they should be in WGS 84 lat/lon.
                            first two points define the along track direction.
        :return: list of ordered waypoints of star trajectory
        """
        crs = QgsCoordinateReferenceSystem.fromEpsgId(4326)
        distance = QgsDistanceArea()
        distance.setSourceCrs(crs, QgsProject.instance().transformContext())
        distance.setEllipsoid(crs.ellipsoidAcronym())
        new_area = []
        index = self.initial_point_spinBox.value() - 1
        geom = QgsGeometry.fromPolygonXY([area_points])
        center_point = geom.centroid().asPoint()

        first_point = geom.vertexAt(0)
        angle_one = math.pi / 2.0 - self.distance.bearing(center_point, QgsPointXY(first_point.x(), first_point.y()))

        radius = distance_ellipsoid(center_point.x(), center_point.y(), area_points[0].x(), area_points[0].y())
        # Compute coverage points
        angle = math.radians(90) - wrap_angle(angle_one)
        angles = []
        for i in range(self.number_points_spinBox.value()):
            angles.append(angle)
            angle += math.pi / float(self.number_points_spinBox.value())

        # roll list
        angles = [angle + (index * math.pi / float(self.number_points_spinBox.value())) for angle in angles]

        for angle in angles:
            lon_1, lat_1 = endpoint_sphere(center_point.x(), center_point.y(), radius, math.degrees(angle))
            lon_2, lat_2 = endpoint_sphere(center_point.x(), center_point.y(), radius, 180 + math.degrees(angle))

            p1 = QgsPointXY(lon_1, lat_1)
            p2 = QgsPointXY(lon_2, lat_2)
            new_area.append(p1)
            new_area.append(p2)
            new_area.append(p1)

        return new_area

    def update_initial_point_spinBox(self):
        """
        Updates the maximum initial point spinBox value with the total number of vertexes
        """
        segments = self.number_points_spinBox.value()
        if self.initial_point_spinBox.value() > segments:
            self.initial_point_spinBox.setValue(segments)
        self.initial_point_spinBox.setMaximum(segments * 2)
