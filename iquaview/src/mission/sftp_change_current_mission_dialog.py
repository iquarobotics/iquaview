# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
This dialog lets the user select a mission from the list of missions currently found in the UAV using SFTP connection.
The 'Run Mission' button will create a symbolic link 'default_mission.xml' and close this dialog.
"""

import logging
import subprocess
from os import path

from PyQt5 import QtWidgets
from PyQt5.QtGui import QBrush, QColor
from PyQt5.QtWidgets import QMessageBox, QHeaderView

from iquaview_lib.connection.sftp.sftp_connection_dialog import SFTPConnectionDialog
from iquaview_lib.utils import busywidget

logger = logging.getLogger(__name__)


class SFTPChangeCurrentMissionDialog(SFTPConnectionDialog):
    """ Dialog to select a mission from the list of missions currently found in the UAV using SFTP connection. """

    def __init__(self, user, ip, local_ini_path, remote_ini_path, parent=None):
        """
        Init of the object SFTPChangeCurrentMissionDialog
        :param user: User that will establish connection
        :type user: str
        :param ip: IP address of the remote machine
        :type ip: str
        :param local_ini_path: Initial path of the local machine
        :type local_ini_path: str
        :param remote_ini_path: Initial path of the remote machine
        :type remote_ini_path: str
        :param project: Project where the missions will be imported
        :type project: QgsProject
        :param mcontrol: Mission control to load the missions to the project
        :type mcontrol: MissionController
        """
        super().__init__(user, ip, local_ini_path, remote_ini_path, file_extension=[".xml"], filter_by_extension=True,
                         init_models=False, parent=parent)

        self.file_name = None
        self.show_remote_folder()

        self.local_table_view.hide()
        self.treeviews_widget.hide()
        self.localname_label.hide()
        self.remotename_label.hide()
        self.help_btn.hide()
        self.transfer_to_local_btn.hide()
        self.transfer_to_remote_btn.hide()
        self.allowed_files_label.hide()
        self.label.hide()
        self.remotepath = remote_ini_path
        self.selected_remote_path = remote_ini_path

        self.set_as_current_btn = QtWidgets.QPushButton(self)
        self.set_as_current_btn.setAutoDefault(False)
        self.set_as_current_btn.setObjectName("Execute_mission")
        self.set_as_current_btn.setText("Execute Mission")
        self.bottom_h_layout.insertWidget(1, self.set_as_current_btn)

        max_w = self.maximumWidth()
        self.setFixedWidth(675)
        self.setMaximumWidth(max_w)

        max_h = self.maximumHeight()
        self.setFixedHeight(350)
        self.setMaximumHeight(max_h)

        self.set_as_current_btn.pressed.connect(self.set_selected_mission_as_current)

        self.current_mission_color = QColor(0, 200, 255, 128)
        self.highlight_current_mission()

    def show_remote_folder(self):
        """
        Creates a busy widget to show loading while reading the contents of the remote path.
        Adds all the files found to the remote table model
        """
        flags = {"abort_flag": False}
        t = busywidget.TaskThread(self.load_remote_folder, self.sftp_connection, self.remotepath, flags=flags)
        bw = busywidget.BusyWidget("Loading folder contents...", t)
        bw.on_start()
        bw.exec_()

        if t.has_error():
            QMessageBox.warning(None, "Connection error",
                                "Error trying to connect to the AUV. Check connection settings.\n"
                                f"{t.get_error()}")

        elif bw.get_info() is None:
            QMessageBox.warning(None, "Connection error", "Connection was closed unexpectedly.")

        else:
            # Get items and add them to the table view
            _, file_list = bw.get_info()
            for file in file_list:
                self.add_file_to_remote_table_model(file)

            # resize model headers
            self.remote_table_view.horizontalHeader().resizeSections(QHeaderView.ResizeToContents)
            self.remote_table_view.horizontalHeader().setStretchLastSection(True)

            # sort all the items of the remote table by name
            self.remote_table_view.sort_table_by_name()

    def set_selected_mission_as_current(self):
        """
        Creates a symbolic link in the remote path named default_mission.xml linked to the selected file
        from remote table view. Once the link is created, closes the dialog.
        """
        indexes = self.remote_table_view.selectedIndexes()

        # Check how many items were selected. Each row has 4 items
        if len(indexes) != 4:
            QMessageBox.warning(None, "Warning",
                                "Select one mission from the remote table to set as current mission")
            self.file_name = None
            return
        item = self.remote_table_model.itemFromIndex(indexes[1])
        file_name = item.text()

        # check if selected item is a file and ends with .xml
        if file_name not in self.remote_file_list or file_name[-4:] != ".xml":
            QMessageBox.warning(None, "Warning",
                                "Selected item must be a mission file.")
            self.file_name = None
            return
        self.file_name = item.text()
        self.accept()

    def highlight_current_mission(self):
        """ Highlights the mission linked by default_mission.xml symbolic link. """
        # Find default mission link file and what mission is it pointing to
        default_mission = ""
        remote_file_rows = self.remote_table_model.get_file_rows()
        if self.selected_remote_path == self.remotepath:
            try:
                cmd = f" readlink {self.get_selected_remote_path()}/default_mission.xml"
                ssh_command = f"ssh -o BatchMode=yes {self.remote_user}@{self.remote_ip}{cmd}"
                ps = subprocess.Popen(ssh_command, shell=True, stdout=subprocess.PIPE,
                                      stderr=subprocess.STDOUT)
                output = ps.communicate()[0]
                if output is not None:
                    output_decoded = output.decode()
                for line in output_decoded.split("\n"):
                    if len(line) != 0 and "No such file or directory" not in line:
                        default_mission = path.basename(line)
                for remote_row in remote_file_rows:
                    # Paint default mission of the AUV
                    if remote_row[1].text() == default_mission:
                        for item in remote_row:
                            item.setBackground(QBrush(self.current_mission_color))
                            self.remote_table_view.scrollTo(item.index())
                    else:
                        for item in remote_row:
                            item.setBackground(QBrush())
            except IOError as e:
                logger.info("There is no 'default mission' link. %s", e)
            except AttributeError as e:
                logger.warning("Connection was closed. %s", e)

    def get_filename(self):
        """
        Returns mission file name
        :return: mission file name
        :rtype: str
        """
        return self.file_name

    def create_table_context_menu(self, pos):
        """
        Override method of SFTPConnectionDialog class. Removes the Transfer option
        :param pos: position to create context menu from the tableView perspective
        :type pos: QPoint
        """
        menu = super().create_table_context_menu(pos)
        if menu is not None:
            actions = menu.actions()
            # get the action named Transfer
            a = None
            for i in actions:
                if i.text() == "Transfer":
                    a = i
                    break
            # remove action form menu
            if a is not None:
                menu.removeAction(a)
