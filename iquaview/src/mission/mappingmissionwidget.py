# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
"""
 Widget to configure heave mode
"""

import logging

from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QWidget

from iquaview.src.ui.ui_mapping_mission import Ui_MappingMissionWidget

logger = logging.getLogger(__name__)


class MappingMissionWidget(QWidget, Ui_MappingMissionWidget):
    heave_mode_changed_signal = pyqtSignal()

    def __init__(self, parent=None):
        """ Init of the object MappingMissionWidget """
        super().__init__(parent)
        self.setupUi(self)

    def get_distance_covered_area_to_turn(self):
        return self.distance_covered_to_turn_doubleSpinBox.value()

    def is_turn_inside_zone(self):
        return self.inside_radioButton.isChecked()
