# -*- coding: utf-8 -*-
# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
Map tool to manage the addition of waypoints in a mission graphically
using a QGIS rubber band
"""

import math
import logging

from iquaview_lib.utils.calcutils import project_point_to_line, distance_to_segment, distance_plane
from iquaview_lib.utils.qgisutils import transform_point, calc_tolerance
from iquaview.src.mission.startendmarker import StartEndMarker
from qgis.core import (QgsFeature,
                       QgsGeometry,
                       QgsWkbTypes,
                       QgsPointXY,
                       QgsDistanceArea,
                       QgsProject,
                       QgsCoordinateTransform,
                       QgsCoordinateReferenceSystem)
from qgis.gui import QgsMapTool, QgsRubberBand, QgsVertexMarker
from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtGui import QColor

logger = logging.getLogger(__name__)


class EditTool(QgsMapTool):
    """
    Open a waypoint edit tool
    """
    wp_clicked = pyqtSignal(int)

    def __init__(self, mission_track, canvas, msglog):
        """
        Init of the object EditTool

        :param mission_track: Mission track to read from and write info to
        :type mission_track: MissionTrack
        :param canvas: Canvas to draw on
        :type canvas: QgsMapCanvas
        :param msglog: Message log to write information
        :type msglog: QgsMessageLog
        """
        QgsMapTool.__init__(self, canvas)
        self.setCursor(Qt.CrossCursor)
        self.mission_track = mission_track
        self.msglog = msglog
        self.dragging = False
        self.feature = None
        self.vertex = None
        self.startcoord = None
        self.current_wp = 0

        self.layer = self.mission_track.get_mission_layer()
        logger.info(self.mission_track.get_mission_name())

        self.rubber_band = QgsRubberBand(self.canvas(), QgsWkbTypes.LineGeometry)
        self.rubber_band.setWidth(2)
        self.rubber_band.setColor(QColor("green"))

        self.point_cursor_band = QgsRubberBand(self.canvas(), QgsWkbTypes.LineGeometry)
        self.point_cursor_band.setWidth(1)
        self.point_cursor_band.setLineStyle(Qt.DashLine)
        self.point_cursor_band.setColor(QColor(255, 0, 0, 100))

        self.mid_point_band = QgsRubberBand(self.canvas(), QgsWkbTypes.PointGeometry)
        self.mid_point_band.setColor(QColor(255, 0, 0, 100))
        self.mid_point_band.setIconSize(18)

        self.rubber_band_points = QgsRubberBand(self.canvas(), QgsWkbTypes.PointGeometry)
        self.rubber_band_points.setColor(QColor("green"))
        self.rubber_band_points.setIcon(QgsRubberBand.ICON_CIRCLE)
        self.rubber_band_points.setIconSize(10)

        self.mission_track.mission_changed.connect(self.update_rubber_bands)

        self.vertex_marker = QgsVertexMarker(self.canvas())
        self.start_end_marker = StartEndMarker(canvas, self.mission_track.find_waypoints_in_mission(), QColor("green"))

        self.layer.startEditing()

        self.wp = []
        self.m_ctrl = False
        # handler for mission feature
        self.update_rubber_bands(0)

        crs = canvas.mapSettings().destinationCrs()
        self.distance_calc = QgsDistanceArea()
        self.distance_calc.setSourceCrs(crs, QgsProject.instance().transformContext())
        self.distance_calc.setEllipsoid(QgsProject.instance().ellipsoid())

        canvas.destinationCrsChanged.connect(self.canvas_crs_changed)

    def canvas_crs_changed(self):
        """
        Canvas crs changed. update points to new crs
        """
        crs = self.canvas().mapSettings().destinationCrs()
        self.distance_calc.setSourceCrs(crs, QgsProject.instance().transformContext())
        self.distance_calc.setEllipsoid(QgsProject.instance().ellipsoid())

        if self.rubber_band is not None:
            self.rubber_band.reset(QgsWkbTypes.LineGeometry)
            self.rubber_band_points.reset(QgsWkbTypes.PointGeometry)
            list_wp = self.mission_track.find_waypoints_in_mission()
            for wp in list_wp:
                if wp is not None:
                    pos = transform_point(
                        QgsPointXY(wp.x(), wp.y()),
                        self.layer.sourceCrs(),
                        self.canvas().mapSettings().destinationCrs()
                    )
                    self.rubber_band.addPoint(pos)
                    self.rubber_band_points.addPoint(pos)

            self.update_rubber_bands(self.current_wp)
            self.start_end_marker.update_markers(list_wp)

    def update_rubber_bands(self, current_wp):
        """
        Updates rubber bands with the information on the mission track
        :param current_wp: waypoint to mark as selected waypoint
        :type current_wp: int
        """
        self.current_wp = current_wp
        self.rubber_band.reset(QgsWkbTypes.LineGeometry)
        self.rubber_band_points.reset(QgsWkbTypes.PointGeometry)
        self.wp = self.mission_track.find_waypoints_in_mission()

        self.start_end_marker.update_markers(self.wp)

        trans = QgsCoordinateTransform(self.layer.sourceCrs(),
                                       self.canvas().mapSettings().destinationCrs(),
                                       QgsProject.instance().transformContext())

        if len(self.wp) > 0:

            for v in self.wp:
                v_trans = trans.transform(v.x(), v.y())
                self.rubber_band.addPoint(v_trans)
                self.rubber_band_points.addPoint(v_trans)
            logger.debug("MISSION UPDATE: now we have {} waypoints".format(len(self.wp)))

            self.vertex_marker.setCenter(QgsPointXY(trans.transform(self.wp[current_wp].x(),
                                                                    self.wp[current_wp].y())))
            self.vertex_marker.setColor(QColor(25, 255, 0))
            self.vertex_marker.setIconSize(7)
            self.vertex_marker.setIconType(QgsVertexMarker.ICON_X)  # ICON_BOX, ICON_CROSS, ICON_X
            self.vertex_marker.setPenWidth(2)
            self.vertex_marker.show()

            self.set_geometry()
        else:

            self.vertex_marker.hide()

    def set_control_state(self, state):
        """
        Sets the value of mCtrl with the value of state
        :param state: Value to set
        :type state: bool
        """
        self.m_ctrl = state

    def keyPressEvent(self, event):
        """
        Overrides method keyPressEvent from QgsMapTool. If it is the control key,
        sets mCtrl to true and shows info for the first point
        :param event: key event
        :type event: QKeyEvent
        """
        if event.key() == Qt.Key_Control and not self.dragging:
            self.m_ctrl = True
            pos = self.canvas().mouseLastXY()
            if not self.find_on_feature(pos, calc_tolerance(self.canvas().width(), self.canvas().height())):
                self.show_dist_and_bearing_to_point()

    def keyReleaseEvent(self, event):
        """
        Overrides method keyReleaseEvent from QgsMapTool. It it is the control key,
        sets mCtrl to false and shows info for the last point
        :param event: key event
        :type event: QKeyEvent
        """
        if event.key() == Qt.Key_Control:
            self.m_ctrl = False
            pos = self.canvas().mouseLastXY()
            if (not self.find_on_feature(pos, calc_tolerance(self.canvas().width(), self.canvas().height()))
                    and not self.dragging):
                self.show_dist_and_bearing_to_point()

    def canvasDoubleClickEvent(self, event):
        """
        Overrides method canvasDoubleClickEvent from QgsMapTool. It passes the event to canvasPressEvent.
        :param event: click event
        :type event: QgsMapMouseEvent
        """
        self.canvasPressEvent(event)

    def canvasPressEvent(self, event):
        """
        Overrides method canvasPressEvent from QgsMapTool. This method handles the action of clicking in the canvas.
        Depending on the possition, if it's over a waypoint, between waypoints or not, it will start dragging the
        waypoint, create an in-between waypoint or create a new waypoint in case of a left click. If a right
        click is detected over a waypoint, this waypoint will be deleted
        :param event: click event
        :type event: QgsMapMouseEvent
        """
        if self.dragging:
            self.canvasReleaseEvent(event)

        layer_pt = self.toLayerCoordinates(self.layer, event.pos())
        tolerance = calc_tolerance(self.canvas().width(), self.canvas().height())
        if not self.find_on_feature(event.pos(), tolerance):
            if event.button() == Qt.LeftButton:
                # we have clicked outside the track
                logger.debug("We have clicked outside the track")
                self.point_cursor_band.reset(QgsWkbTypes.LineGeometry)
                if not self.m_ctrl:
                    # add step to mission at the end
                    self.mission_track.add_step(len(self.wp), layer_pt)
                    self.show_waypoint_distances(len(self.wp) - 1)
                else:
                    self.mission_track.add_step(0, layer_pt)
                    self.show_waypoint_distances(0)
        else:
            logger.debug("We have clicked on the track")
            vertex = self.find_vertex_at(event.pos(), tolerance)

            if event.button() == Qt.LeftButton:
                if vertex is None:
                    logger.debug("We have clicked between vertexs")
                    # we have clicked in between vertex, add intermediate point
                    initial_vertex = self.find_segment_at(event.pos())
                    # self.mission_track.add_step(initial_vertex + 1, layerPt)

                    pos_layer = self.toLayerCoordinates(self.layer, event.pos())
                    first_point = QgsPointXY(self.wp[initial_vertex])
                    second_point = QgsPointXY(self.wp[initial_vertex + 1])
                    intersection_x, intersection_y = project_point_to_line(pos_layer.x(), pos_layer.y(),
                                                                           first_point.x(), first_point.y(),
                                                                           second_point.x(), second_point.y())
                    logger.debug(f"intersection point: {str(intersection_x)} {str(intersection_y)}")
                    logger.debug(
                        f"{self.wp[initial_vertex].x()} "
                        f"{self.wp[initial_vertex].y()} "
                        f"{self.wp[initial_vertex + 1].x()} "
                        f"{self.wp[initial_vertex + 1].y()}")
                    self.mission_track.add_step(initial_vertex + 1, QgsPointXY(intersection_x, intersection_y))
                    self.mid_point_band.reset(QgsWkbTypes.PointGeometry)
                    self.show_waypoint_distances(initial_vertex + 1)
                else:
                    logger.debug(f"We have clicked on vertex {vertex}")
                    # we have clicked on a vertex

                    # Left click -> move vertex.
                    self.dragging = True
                    self.vertex = vertex
                    self.startcoord = event.pos()

            elif event.button() == Qt.RightButton:
                if vertex is not None and not self.dragging:
                    # Right click -> delete vertex.
                    self.delete_vertex(vertex)

                    if self.find_on_feature(event.pos(), tolerance):  # If cursor still over track
                        vertex = self.find_vertex_at(event.pos(), tolerance)
                        if vertex is None:  # Cursor is between vertexes
                            self.show_mid_point(event.pos())
                        else:  # Cursor is over a vertex
                            self.show_waypoint_distances(vertex)
                    else:
                        self.show_dist_and_bearing_to_point()

    def canvasMoveEvent(self, event):
        """
        Overrides method canvasMoveEvent from QgsMapTool. If a dragging action is happening, it will move the selected
        vertex to the event position. Otherwise this method will update the information shown in the msglog
        :param event: mouse event
        :type event: QgsMapMouseEvent
        """
        self.mid_point_band.reset(QgsWkbTypes.PointGeometry)
        if self.dragging:
            layer_pt = self.toLayerCoordinates(self.layer, event.pos())
            pos = transform_point(
                layer_pt,
                self.layer.sourceCrs(),
                self.canvas().mapSettings().destinationCrs()
            )
            self.move_vertex_to(pos)
            self.mission_track.hide_start_end_markers()
            self.vertex_marker.hide()
            self.start_end_marker.hide_markers()
            cursor_canvas = self.toMapCoordinates(event.pos())
            self.update_msg(self.vertex, cursor_canvas)

        else:
            tolerance = calc_tolerance(self.canvas().width(), self.canvas().height())
            if self.find_on_feature(event.pos(), tolerance):  # if mouse is over the track
                self.hide_bands()
                vertex = self.find_vertex_at(event.pos(), tolerance)

                if vertex is None:  # Cursor is between vertexes
                    self.show_mid_point(event.pos())
                else:  # Cursor is over a vertex
                    self.show_waypoint_distances(vertex)

            else:
                layer_pt = self.toLayerCoordinates(self.layer, event.pos())
                pos = transform_point(
                    layer_pt,
                    self.layer.sourceCrs(),
                    self.canvas().mapSettings().destinationCrs()
                )
                self.mid_point_band.addPoint(pos)
                self.show_dist_and_bearing_to_point()

    def show_dist_and_bearing_to_point(self):
        """
        Finds distance and bearing from the last point (first if pressing ctrl) to the specified point and shows them
        in the message log. Also draws a line between the points.
        """
        if self.canvas().underMouse():
            bearing = 0.0

            self.point_cursor_band.reset(QgsWkbTypes.LineGeometry)
            point = self.canvas().mouseLastXY()
            if len(self.wp) > 0:
                cursor_point = self.toMapCoordinates(point)
                if self.m_ctrl:
                    anchor_point = QgsPointXY(self.wp[0])
                else:
                    anchor_point = QgsPointXY(self.wp[len(self.wp) - 1])

                anchor_point = transform_point(
                    anchor_point,
                    self.layer.sourceCrs(),
                    self.canvas().mapSettings().destinationCrs()
                )

                self.point_cursor_band.addPoint(cursor_point)
                self.point_cursor_band.addPoint(anchor_point)
                distance = self.distance_calc.measureLine([anchor_point, cursor_point])
                if distance != 0.0:
                    bearing = self.distance_calc.bearing(anchor_point, cursor_point)
                self.msglog.logMessage("")
                if self.m_ctrl:
                    msg = "Distance to next point: "
                else:
                    msg = "Distance to previous point: "
                self.msglog.logMessage(msg + f"{distance:.3F} m.  Bearing: {math.degrees(bearing):.3F} °.",
                                       "Distance and bearing", 0)
            else:
                self.msglog.logMessage("")

    def show_mid_point(self, cursor):
        """
        Finds the projection of the cursor over the track and draws a circle in that point.
        Finds the distances between this projection point and the previous and next points in the mission
        and shows them in the message log.
        :param cursor: position to be projected over the track
        :type cursor: Qpoint
        """
        prev_vertex = self.find_segment_at(cursor)
        prev_point = QgsPointXY(self.wp[prev_vertex])
        next_point = QgsPointXY(self.wp[prev_vertex + 1])
        cursor_point = self.toLayerCoordinates(self.layer, cursor)
        intersection_x, intersection_y = project_point_to_line(cursor_point.x(), cursor_point.y(),
                                                               prev_point.x(), prev_point.y(),
                                                               next_point.x(), next_point.y())

        prev_point = transform_point(prev_point, self.layer.sourceCrs(), self.canvas().mapSettings().destinationCrs())
        intersection = transform_point(
            QgsPointXY(intersection_x, intersection_y),
            self.layer.sourceCrs(),
            self.canvas().mapSettings().destinationCrs()
        )
        next_point = transform_point(next_point, self.layer.sourceCrs(), self.canvas().mapSettings().destinationCrs())

        self.mid_point_band.addPoint(intersection)

        distance1 = self.distance_calc.measureLine([prev_point, intersection])
        distance2 = self.distance_calc.measureLine([intersection, next_point])
        self.msglog.logMessage("")
        self.msglog.logMessage(
            f"Distance to previous point: {distance1:.3F} m.  Distance to next point: {distance2:.3F} m.",
            "Distance between points",
            0)

    def show_waypoint_distances(self, vertex):
        """
        Finds the distances to adjacent waypoints of vertex and shows them in the message log
        :param vertex: index of the waypoint from the mission
        :type vertex: int
        """
        curr_point = QgsPointXY(self.wp[vertex])
        curr_point = transform_point(
            curr_point,
            self.layer.sourceCrs(),
            self.canvas().mapSettings().destinationCrs()
        )
        self.update_msg(vertex, curr_point)

    def update_msg(self, vertex, cursor_canvas):
        """
        Finds the distances to adjacent waypoints of vertex and shows them in the message log
        :param vertex: index of the waypoint from the mission
        :type vertex: int
        :param cursor_canvas: position to be projected over the canvas
        :type cursor_canvas: QpointXY
        """
        if vertex == 0:
            if len(self.wp) > 1:
                next_point = QgsPointXY(self.wp[vertex + 1])
                next_point = transform_point(
                    next_point,
                    self.layer.sourceCrs(),
                    self.canvas().mapSettings().destinationCrs()
                )
                distance = self.distance_calc.measureLine([cursor_canvas, next_point])
                bearing = self.distance_calc.bearing(next_point, cursor_canvas)
                msg = f"Distance to next point: {distance:.3F} m.  Bearing: {math.degrees(bearing):.3F} °."
            else:
                msg = ""
            self.msglog.logMessage("")
            self.msglog.logMessage(msg + f" (Waypoint {vertex + 1}) ", "Vertex distances", 0)
        elif vertex == len(self.wp) - 1:
            prev_point = QgsPointXY(self.wp[vertex - 1])
            prev_point = transform_point(
                prev_point,
                self.layer.sourceCrs(),
                self.canvas().mapSettings().destinationCrs()
            )
            distance = self.distance_calc.measureLine([prev_point, cursor_canvas])
            bearing = self.distance_calc.bearing(prev_point, cursor_canvas)
            msg = f"Distance to previous point: {distance:.3F} m.  Bearing: {math.degrees(bearing):.3F} °."
            self.msglog.logMessage("")
            self.msglog.logMessage(msg + f" (Waypoint {vertex + 1})", "Vertex distances", 0)
        else:
            prev_point = QgsPointXY(self.wp[vertex - 1])
            next_point = QgsPointXY(self.wp[vertex + 1])
            prev_point = transform_point(
                prev_point,
                self.layer.sourceCrs(),
                self.canvas().mapSettings().destinationCrs()
            )
            next_point = transform_point(
                next_point,
                self.layer.sourceCrs(),
                self.canvas().mapSettings().destinationCrs()
            )
            distance1 = self.distance_calc.measureLine(prev_point, cursor_canvas)
            distance2 = self.distance_calc.measureLine(cursor_canvas, next_point)
            msg = f"Distance to previous point: {distance1:.3F} m.  Distance to next point: {distance2:.3F} m."
            self.msglog.logMessage("")
            self.msglog.logMessage(msg + f" (Waypoint {vertex + 1})", "Vertex distances", 0)

    def hide_bands(self):
        """ Hides the bands created by the tool """
        self.point_cursor_band.reset(QgsWkbTypes.LineGeometry)
        self.mid_point_band.reset(QgsWkbTypes.PointGeometry)

    def canvasReleaseEvent(self, event):
        """
        Overrides the method canvasReleaseEvent form QgsMapTool. If a dragging action is happening, it will
        stop the dragging and set the point to the event position, if enough distance was made.
        :param event: mouse event
        :type event: QgsMapMouseEvent
        """
        if self.dragging and event.button() == Qt.LeftButton:
            self.dragging = False
            self.vertex_marker.show()
            layer_pt = self.toLayerCoordinates(self.layer, event.pos())
            # Check distance with initial point
            dist = distance_plane(self.startcoord.x(), self.startcoord.y(),
                                  event.pos().x(), event.pos().y())
            tolerance = calc_tolerance(self.canvas().width(), self.canvas().height())
            if dist > tolerance:
                pos = transform_point(
                    QgsPointXY(layer_pt.x(), layer_pt.y()),
                    self.layer.sourceCrs(),
                    self.canvas().mapSettings().destinationCrs()
                )
                self.move_vertex_to(pos)
                self.mission_track.change_position(self.vertex, layer_pt)
                self.wp_clicked.emit(self.vertex)
                self.feature = None
                self.vertex = None
                self.layer.updateExtents()
            else:
                # If release point is the same, has been just a click
                pos = transform_point(
                    QgsPointXY(self.wp[self.vertex]),
                    QgsCoordinateReferenceSystem.fromEpsgId(4326),
                    self.canvas().mapSettings().destinationCrs()
                )
                self.move_vertex_to(pos)
                self.wp_clicked.emit(self.vertex)
                self.feature = None
                self.vertex = None

    def move_vertex_to(self, canvas_point):
        """
        Move current vertex to canvas_point position.

        :param canvas_point: canvas point
        :type canvas_point: QgsPointXY
        """
        self.hide_bands()
        if len(self.wp) > 1:
            if self.vertex != 0:
                prev_point = transform_point(
                    self.wp[self.vertex - 1],
                    self.layer.sourceCrs(),
                    self.canvas().mapSettings().destinationCrs()
                )
                self.point_cursor_band.addPoint(prev_point)
            self.point_cursor_band.addPoint(canvas_point)
            if self.vertex < len(self.wp) - 1:
                next_point = transform_point(
                    self.wp[self.vertex + 1],
                    self.layer.sourceCrs(),
                    self.canvas().mapSettings().destinationCrs()
                )
                self.point_cursor_band.addPoint(next_point)
            self.mid_point_band.addPoint(canvas_point)
        elif len(self.wp) == 1:
            # A rubber band with PointGeometry and only 1 point acts as if it had 2 points, we need to reset it in
            # order to move the point.
            self.rubber_band_points.reset(QgsWkbTypes.PointGeometry)
            self.rubber_band_points.addPoint(canvas_point)
            self.rubber_band.reset(QgsWkbTypes.LineGeometry)
            self.rubber_band.addPoint(canvas_point)

    def delete_vertex(self, vertex):
        """
        Delete step 'vertex'.
        :param vertex: step
        :type vertex: int
        """
        self.mission_track.remove_step(vertex)
        self.dragging = False
        self.vertex = None

    def find_on_feature(self, pos, tolerance):
        """
        If the clicked point has some segment at a smaller distance than tolerance,
        it means that we've clicked on the track.

        :param pos: The point that we've clicked
        :type pos: QPoint
        :param tolerance: The tolerance of pos
        :type tolerance: float
        :return: whether pos is inside the tolerance area or not
        :rtype: bool
        """
        if len(self.wp) > 1:
            previous_wp = transform_point(
                QgsPointXY(self.wp[0]),
                self.layer.sourceCrs(),
                self.canvas().mapSettings().destinationCrs()
            )

            for v in range(1, len(self.wp)):
                next_wp = transform_point(
                    QgsPointXY(self.wp[v]),
                    self.layer.sourceCrs(),
                    self.canvas().mapSettings().destinationCrs()
                )

                # convert layer coordinates to canvas coordinates
                prev_pos = self.toCanvasCoordinates(previous_wp)
                next_pos = self.toCanvasCoordinates(next_wp)

                dist_to_segment = distance_to_segment(pos.x(), pos.y(),
                                                      prev_pos.x(), prev_pos.y(),
                                                      next_pos.x(), next_pos.y())

                if dist_to_segment < tolerance:
                    return True

                previous_wp = next_wp

        # last waypoint
        vertex = self.find_vertex_at(pos, tolerance)
        return vertex is not None

    def find_segment_at(self, pos):
        """
        get the segment that is closer to the clicked point and return its initial vertex

        :param pos: the point that we've clicked
        :type pos: QPoint
        :return: initial vertex of the segment
        :rtype: int
        """
        dist_to_segment = []
        for v in range(0, len(self.wp) - 1):
            previous_wp = transform_point(
                QgsPointXY(self.wp[v]),
                self.layer.sourceCrs(),
                self.canvas().mapSettings().destinationCrs()
            )
            next_wp = transform_point(
                QgsPointXY(self.wp[v + 1]),
                self.layer.sourceCrs(),
                self.canvas().mapSettings().destinationCrs()
            )
            # convert layer coordinates to canvas coordinates
            prev_wp_on_canvas = self.toCanvasCoordinates(previous_wp)
            next_wp_on_canvas = self.toCanvasCoordinates(next_wp)

            dist_to_segment.append(distance_to_segment(pos.x(), pos.y(),
                                                       prev_wp_on_canvas.x(), prev_wp_on_canvas.y(),
                                                       next_wp_on_canvas.x(), next_wp_on_canvas.y()))

        vertex = dist_to_segment.index(min(dist_to_segment))
        return vertex

    def find_vertex_at(self, pos, tolerance):
        """
        get the vertex that is closer to the clicked point

        :param pos: The point that we've clicked
        :type pos: QPoint
        :param tolerance: The tolerance of pos
        :type tolerance: float
        :return: vertex or None
        :rtype: int
        """
        if len(self.wp) > 0:
            dist_to_vertex = []
            for waypoint in self.wp:
                trans_pos = transform_point(
                    QgsPointXY(waypoint),
                    self.layer.sourceCrs(),
                    self.canvas().mapSettings().destinationCrs()
                )
                trans_pos_on_canvas = self.toCanvasCoordinates(trans_pos)
                dist_to_vertex.append(distance_plane(pos.x(), pos.y(),
                                                     trans_pos_on_canvas.x(), trans_pos_on_canvas.y()))

            vertex = dist_to_vertex.index(min(dist_to_vertex))
            if min(dist_to_vertex) < tolerance:
                logger.debug("ON VERTEX")
                return vertex

        return None

    def set_geometry(self):
        """
        Save rubber band to geometry of the layer
        """
        if self.layer.featureCount() == 0:
            # no feature yet created
            feat = QgsFeature()
            if len(self.wp) == 1:
                feat.setGeometry(QgsGeometry.fromPointXY(QgsPointXY(self.wp[0].x(), self.wp[0].y())))
            else:
                feat.setGeometry(QgsGeometry.fromPolyline(self.wp))
            self.layer.addFeatures([feat])
        else:
            # mission feature present, edit geometry
            feats = self.layer.getFeatures()
            for feat in feats:
                if len(self.wp) == 1:
                    self.layer.changeGeometry(feat.id(),
                                              QgsGeometry.fromPointXY(QgsPointXY(self.wp[0].x(), self.wp[0].y())))
                else:
                    self.layer.changeGeometry(feat.id(), QgsGeometry.fromPolyline(self.wp))
        self.layer.commitChanges()
        self.layer.startEditing()

    def close_band(self):
        """
        Called when closing edittool, this will hide and delete all rubber bands and markers.
        """
        self.start_end_marker.close_markers()
        self.vertex_marker.hide()
        self.canvas().scene().removeItem(self.vertex_marker)
        self.vertex_marker = None
        self.mission_track.mission_changed.disconnect()
        self.layer.commitChanges()
        self.rubber_band.hide()
        self.mid_point_band.hide()
        self.rubber_band_points.hide()
        self.point_cursor_band.hide()
        self.canvas().scene().removeItem(self.rubber_band)
        self.canvas().scene().removeItem(self.mid_point_band)
        self.canvas().scene().removeItem(self.rubber_band_points)
        self.canvas().scene().removeItem(self.point_cursor_band)
        self.rubber_band = None
        self.mid_point_band = None
        self.rubber_band_points = None
        self.point_cursor_band = None
        self.msglog.logMessage("")
