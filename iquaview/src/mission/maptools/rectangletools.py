# -*- coding: utf-8 -*-
# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Map tools for drawing Rectangles on the canvas
"""

from math import radians, sqrt, degrees, atan2

from PyQt5.QtCore import pyqtSignal, Qt
from PyQt5.QtGui import QCursor, QPixmap, QColor
from qgis.core import (QgsCoordinateTransform,
                       QgsGeometry,
                       QgsPointXY,
                       QgsMapSettings,
                       QgsDistanceArea,
                       QgsPoint,
                       QgsProject,
                       QgsCoordinateReferenceSystem,
                       QgsWkbTypes)
from qgis.gui import QgsMapTool, QgsRubberBand, QgsVertexMarker

from iquaview_lib.utils.calcutils import calc_is_collinear, get_angle_of_line_between_two_points
from iquaview_lib.utils.qgisutils import transform_point
from iquaview.src.mission.maptools.rectangle import Rectangle


class FromDrawBase(QgsMapTool):
    msgbar = pyqtSignal(str)
    rbFinished = pyqtSignal(object)
    rb_reset_signal = pyqtSignal()

    def __init__(self, canvas):
        """
        Init of the object RectBy3PointsTool
        :param canvas: canvas to draw on and get signals from
        :type canvas: QgsMapCanvas
        """
        QgsMapTool.__init__(self, canvas)
        self.canvas = canvas
        self.n_points = 0
        self.rb = None
        self.x_p1, self.y_p1, self.x_p2, self.y_p2, self.x_p3, self.y_p3, self.x_p4, self.y_p4 = None, None, None, None, None, None, None, None
        self.length = 0
        crs = QgsCoordinateReferenceSystem.fromEpsgId(4326)
        self.distance = QgsDistanceArea()
        self.distance.setSourceCrs(crs, QgsProject.instance().transformContext())
        self.distance.setEllipsoid(QgsProject.instance().ellipsoid())
        self.arrow_head = None
        self.arrow_marker = QgsVertexMarker(self.canvas)
        self.arrow_marker.setColor(QColor("green"))
        self.arrow_marker.setIconType(QgsVertexMarker.ICON_TRIANGLE)
        self.arrow_marker.setFillColor(QColor("green"))
        self.arrow_marker.setIconSize(16)
        self.arrow_marker.hide()
        # our own fancy cursor
        self.cursor = QCursor(QPixmap(["16 16 3 1",
                                       "      c None",
                                       ".     c #FF0000",
                                       "+     c #1210f3",
                                       "                ",
                                       "       +.+      ",
                                       "      ++.++     ",
                                       "     +.....+    ",
                                       "    +.     .+   ",
                                       "   +.   .   .+  ",
                                       "  +.    .    .+ ",
                                       " ++.    .    .++",
                                       " ... ...+... ...",
                                       " ++.    .    .++",
                                       "  +.    .    .+ ",
                                       "   +.   .   .+  ",
                                       "   ++.     .+   ",
                                       "    ++.....+    ",
                                       "      ++.++     ",
                                       "       +.+      "]))
        self.rectangle = Rectangle(crs)

    def activate(self):
        """
        Sets the custom cursor on canvas
        """
        self.canvas.setCursor(self.cursor)

    def deactivate(self):
        """
        Deletes started rectangles and closes all bands
        """
        self.n_points = 0
        self.x_p1, self.y_p1, self.x_p2, self.y_p2, self.x_p3, self.y_p3 = None, None, None, None, None, None
        if self.rb:
            self.rb.reset(True)
            self.rb.hide()
        self.rb = None

        if self.arrow_head is not None:
            self.arrow_head.reset(True)
        self.arrow_head = None

        self.arrow_marker.hide()
        self.canvas.refresh()

    def canvasPressEvent(self, event):
        """
        Overrides method canvasPressEvent from QgsMapTool.
        :param event: click event
        :type event: QgsMapMouseEvent
        """
        raise NotImplementedError

    def canvasMoveEvent(self, event):
        """
        Overrides method canvasMoveEvent from QgsMapTool.
        :param event: move event
        :type event: QgsMapMouseEvent
        """
        raise NotImplementedError

    def keyReleaseEvent(self, event):
        """
        Overrides method keyReleaseEvent from QgsMapTool.
        :param event: key event
        :type event: QKeyEvent
        """
        raise NotImplementedError

    def draw_arrow(self, start_point, end_point):
        if self.arrow_head is None:
            self.arrow_head = QgsRubberBand(self.canvas, QgsWkbTypes.LineGeometry)
            self.arrow_head.setWidth(4)
            self.arrow_head.setColor(QColor("green"))
        # Clear any existing items from the rubber band
        self.arrow_head.reset(QgsWkbTypes.LineGeometry)

        # Add points to the rubber band
        self.arrow_head.addPoint(start_point, False)  # False indicates not to update canvas immediately
        self.arrow_head.addPoint(end_point, True)  # True indicates to update canvas immediately

        # Now, let's add an arrowhead at the end

        # Create a QgsVertexMarker for the arrowhead
        self.arrow_marker.setCenter(QgsPointXY(end_point))
        self.arrow_marker.setRotation(90 - get_angle_of_line_between_two_points(start_point.x(),
                                                                                start_point.y(),
                                                                                end_point.x(),
                                                                                end_point.y()))
        self.arrow_marker.show()


class RectBy3PointsTool(FromDrawBase):

    def __init__(self, canvas):
        """
        Init of the object RectBy3PointsTool
        :param canvas: canvas to draw on and get signals from
        :type canvas: QgsMapCanvas
        """
        FromDrawBase.__init__(self, canvas)

    def keyReleaseEvent(self, event):
        """
        Overrides method keyPressEvent from QgsMapTool. If the scape key is released, cancel current drawings on canvas.
        :param event: key event
        :type event: QKeyEvent
        """
        if event.key() == Qt.Key_Escape:
            self.n_points = 0
            self.x_p1, self.y_p1, self.x_p2, self.y_p2, self.x_p3, self.y_p3 = None, None, None, None, None, None
            if self.rb:
                self.rb.reset(True)
            self.rb = None
            if self.arrow_head is not None:
                self.arrow_head.reset(True)
            self.arrow_head = None

            self.arrow_marker.hide()
            self.canvas.refresh()
            self.rb_reset_signal.emit()

    def canvasPressEvent(self, event):
        """
        Overrides method canvasPressEvent from QgsMapTool. When a press event is registered from the canvas,
        this method starts the drawing of a rectangle if it is not already and finishes drawing if it was
        already drawn.
        :param event: click event
        :type event: QgsMapMouseEvent
        """

        map_pos = self.toMapCoordinates(event.pos())
        point = transform_point(
            map_pos,
            self.canvas.mapSettings().destinationCrs(),
            QgsCoordinateReferenceSystem.fromEpsgId(4326)
        )

        if self.n_points == 0:
            color = QColor(255, 0, 0, 128)
            if self.rb:
                self.rb.reset()
                self.rb = None
            self.rb = QgsRubberBand(self.canvas, True)
            self.rb.setColor(color)
            self.rb.setWidth(1)
            self.msgbar.emit("Define bearing and extent along track")
            self.rb_reset_signal.emit()

            self.x_p1 = point.x()
            self.y_p1 = point.y()
            self.n_points += 1

        elif self.n_points == 1 and (self.x_p1 != point.x() or self.y_p1 != point.y()):
            # First and second points must be different to generate the geometry correctly
            self.x_p2 = point.x()
            self.y_p2 = point.y()
            self.msgbar.emit("Define extent across track")
            self.n_points += 1

        elif self.n_points == 2 and (self.x_p2 != point.x() or self.y_p2 != point.y()):
            # Second and third points also must be different
            self.canvas.refresh()

            self.x_p3 = point.x()
            self.y_p3 = point.y()

            geom = self.rectangle.get_rect_by3_points(QgsPointXY(self.x_p1, self.y_p1),
                                                      QgsPointXY(self.x_p2, self.y_p2),
                                                      QgsPointXY(self.x_p3, self.y_p3))
            self.n_points = 0
            self.x_p1, self.y_p1, self.x_p2, self.y_p2, self.x_p3, self.y_p3 = None, None, None, None, None, None

            self.rbFinished.emit(geom)

    def canvasMoveEvent(self, event):
        """
        Overrides method canvasMoveEvent from QgsMapTool. This method draws a rectangle from a set point
        to the event point if that set point exists, else does nothing.
        :param event: move event
        :type event: QgsMapMouseEvent
        """

        if not self.rb: return

        trans_wgs_to_map = QgsCoordinateTransform(QgsCoordinateReferenceSystem.fromEpsgId(4326),
                                                  self.canvas.mapSettings().destinationCrs(),
                                                  QgsProject.instance().transformContext())

        map_pos = self.toMapCoordinates(event.pos())
        current_point = transform_point(
            map_pos,
            self.canvas.mapSettings().destinationCrs(),
            QgsCoordinateReferenceSystem.fromEpsgId(4326)
        )

        if self.n_points == 1:

            point = transform_point(
                QgsPointXY(self.x_p1, self.y_p1),
                QgsCoordinateReferenceSystem.fromEpsgId(4326),
                self.canvas.mapSettings().destinationCrs()
            )
            self.rb.setToGeometry(QgsGeometry.fromPolyline([QgsPoint(point), QgsPoint(map_pos)]), None)
            curr_dist = self.distance.measureLine(QgsPointXY(self.x_p1, self.y_p1), current_point)
            curr_bearing = degrees(self.distance.bearing(QgsPointXY(self.x_p1, self.y_p1), current_point))
            if curr_bearing < 0.0:
                curr_bearing = 360 + curr_bearing
            self.msgbar.emit(
                "Current distance: {:.3F} m, Current bearing: {:.3F} degrees".format(curr_dist, curr_bearing))

            midpoint = QgsPointXY((point.x() + map_pos.x()) / 2, (point.y() + map_pos.y()) / 2)
            self.draw_arrow(point, midpoint)
        if self.n_points >= 2:
            geom = self.rectangle.get_rect_by3_points(QgsPointXY(self.x_p1, self.y_p1),
                                                      QgsPointXY(self.x_p2, self.y_p2),
                                                      current_point)
            curr_dist = self.distance.measureLine(QgsPointXY(self.x_p2, self.y_p2), current_point)
            curr_bearing = degrees(
                self.distance.bearing(QgsPointXY(self.x_p2, self.y_p2), QgsPointXY(geom.vertexAt(2))))
            if curr_bearing < 0.0:
                curr_bearing = 360 + curr_bearing
            self.msgbar.emit(
                "Current distance: {:.3F} m, Current bearing: {:.3F} degrees".format(curr_dist, curr_bearing))

            pt1 = trans_wgs_to_map.transform(geom.asPolygon()[0][0])
            pt2 = trans_wgs_to_map.transform(geom.asPolygon()[0][1])
            pt3 = trans_wgs_to_map.transform(geom.asPolygon()[0][2])
            pt4 = trans_wgs_to_map.transform(geom.asPolygon()[0][3])
            geom = QgsGeometry.fromPolygonXY([[pt1, pt2, pt3, pt4]])
            self.rb.setToGeometry(geom, None)

            midpoint = QgsPointXY((pt1.x() + pt2.x()) / 2, (pt1.y() + pt2.y()) / 2)
            self.draw_arrow(pt1, midpoint)


# RectByFixedExtentTool with fixed length values for the rectangle sides
# Tool class
class RectByFixedExtentTool(FromDrawBase):

    def __init__(self, canvas, x_length, y_length):
        """
        Init of the object RectByFixedExtentTool
        :param canvas: canvas to draw on and get signals from
        :type canvas: QgsMapCanvas
        :param x_length: horizontal length of the rectangle
        :type x_length: float
        :param y_length: vertical length of the rectangle
        :type y_length: float
        """
        FromDrawBase.__init__(self, canvas)
        self.fixed_p2, self.fixed_p3 = QgsPointXY(0, 0), QgsPointXY(0, 0)
        self.x_length = x_length
        self.y_length = y_length
        self.bearing = 0.0

    def keyReleaseEvent(self, event):
        """
        Overrides method keyPressEvent from QgsMapTool. If the scape key is released, cancel current drawings on canvas.
        :param event: key event
        :type event: QKeyEvent
        """
        if event.key() == Qt.Key_Escape:
            self.n_points = 0
            self.x_p1, self.y_p1, self.x_p2, self.y_p2, self.x_p3, self.y_p3 = None, None, None, None, None, None
            self.fixed_p2, self.fixed_p3 = None, None
            if self.rb:
                self.rb.reset(True)
            self.rb = None
            if self.arrow_head is not None:
                self.arrow_head.reset(True)
            self.arrow_head = None

            self.arrow_marker.hide()
            self.canvas.refresh()
            self.rb_reset_signal.emit()

    def canvasPressEvent(self, event):
        """
        Overrides method canvasPressEvent from QgsMapTool. When a press event is registered from the canvas,
        this method starts the drawing of a rectangle if it is not already and finishes drawing if it was
        already started.
        :param event: click event
        :type event: QgsMapMouseEvent
        """

        trans_wgs_to_map = QgsCoordinateTransform(QgsCoordinateReferenceSystem.fromEpsgId(4326),
                                                  self.canvas.mapSettings().destinationCrs(),
                                                  QgsProject.instance().transformContext())

        point_map = self.toMapCoordinates(event.pos())
        point = transform_point(
            point_map,
            self.canvas.mapSettings().destinationCrs(),
            QgsCoordinateReferenceSystem.fromEpsgId(4326)
        )

        if self.n_points == 0:
            color = QColor(255, 0, 0, 128)
            if self.rb:
                self.rb.reset()
                self.rb = None
            self.rb = QgsRubberBand(self.canvas, True)
            self.rb.setColor(color)
            self.rb.setWidth(1)
            self.msgbar.emit("Define bearing along track")
            self.rb_reset_signal.emit()

            self.x_p1 = point.x()
            self.y_p1 = point.y()
            self.n_points += 1

        elif self.n_points == 1 and (self.x_p1 != point.x() or self.y_p1 != point.y()):
            self.x_p2 = point.x()
            self.y_p2 = point.y()
            self.bearing = self.distance.bearing(QgsPointXY(self.x_p1, self.y_p1), QgsPointXY(self.x_p2, self.y_p2))
            self.msgbar.emit("Define across track direction")
            self.n_points += 1

        elif self.n_points == 2 and (self.x_p2 != point.x() or self.y_p2 != point.y()):
            self.canvas.refresh()

            self.x_p3 = point.x()
            self.y_p3 = point.y()

            geom_wgs = self.rectangle.get_rect_by3_points(QgsPointXY(self.x_p1, self.y_p1), self.fixed_p2,
                                                          self.fixed_p3,
                                                          self.y_length)

            pt1 = trans_wgs_to_map.transform(geom_wgs.asPolygon()[0][0])
            pt2 = trans_wgs_to_map.transform(geom_wgs.asPolygon()[0][1])
            pt3 = trans_wgs_to_map.transform(geom_wgs.asPolygon()[0][2])
            pt4 = trans_wgs_to_map.transform(geom_wgs.asPolygon()[0][3])
            geom = QgsGeometry.fromPolygonXY([[pt1, pt2, pt3, pt4]])

            self.rb.setToGeometry(geom, None)

            self.n_points = 0
            self.x_p1, self.y_p1, self.x_p2, self.y_p2, self.x_p3, self.y_p3 = None, None, None, None, None, None
            self.fixed_p2, self.fixed_p3 = None, None
            self.msgbar.emit("")
            self.rbFinished.emit(geom_wgs)

    def canvasMoveEvent(self, event):
        """
        Overrides method canvasMoveEvent from QgsMapTool. This method draws a rectangle from a set point
        to the event point if that set point exists, else does nothing.
        :param event: move event
        :type event: QgsMapMouseEvent
        """

        if not self.rb: return
        trans_wgs_to_map = QgsCoordinateTransform(QgsCoordinateReferenceSystem.fromEpsgId(4326),
                                                  self.canvas.mapSettings().destinationCrs(),
                                                  QgsProject.instance().transformContext())

        point_map = self.toMapCoordinates(event.pos())
        current_point = transform_point(
            point_map,
            self.canvas.mapSettings().destinationCrs(),
            QgsCoordinateReferenceSystem.fromEpsgId(4326)
        )

        if self.n_points == 1:
            self.bearing = self.distance.bearing(QgsPointXY(self.x_p1, self.y_p1), current_point)
            self.fixed_p2 = self.distance.measureLineProjected(QgsPointXY(self.x_p1, self.y_p1), self.x_length,
                                                               self.bearing)[1]

            point_1 = trans_wgs_to_map.transform(QgsPointXY(self.x_p1, self.y_p1))
            point_2 = trans_wgs_to_map.transform(QgsPointXY(self.fixed_p2))
            self.rb.setToGeometry(
                QgsGeometry.fromPolyline([QgsPoint(point_1),
                                          QgsPoint(point_2)]),
                None)
            curr_bearing = degrees(self.bearing)
            if curr_bearing < 0.0:
                curr_bearing = 360 + curr_bearing
            self.msgbar.emit(
                "Current distance: {:.3F} m, Current bearing: {:.3F} degrees".format(self.x_length, curr_bearing))

            midpoint = QgsPointXY((point_1.x() + point_2.x()) / 2, (point_1.y() + point_2.y()) / 2)
            self.draw_arrow(point_1, midpoint)
        if self.n_points >= 2:
            # test if current_point is left or right of the line defined by p1 and p2
            side = calc_is_collinear(self.x_p1, self.y_p1,
                                     self.fixed_p2.x(), self.fixed_p2.y(),
                                     current_point.x(), current_point.y())

            if side == 0:
                return None
            self.fixed_p3 = self.distance.measureLineProjected(QgsPointXY(self.x_p2, self.y_p2), self.y_length,
                                                               self.bearing + radians(90) * side)[1]

            geom = self.rectangle.get_rect_by3_points(QgsPointXY(self.x_p1, self.y_p1), self.fixed_p2, self.fixed_p3,
                                                      self.y_length)
            pt1 = trans_wgs_to_map.transform(geom.asPolygon()[0][0])
            pt2 = trans_wgs_to_map.transform(geom.asPolygon()[0][1])
            pt3 = trans_wgs_to_map.transform(geom.asPolygon()[0][2])
            pt4 = trans_wgs_to_map.transform(geom.asPolygon()[0][3])
            geom = QgsGeometry.fromPolygonXY([[pt1, pt2, pt3, pt4]])
            self.rb.setToGeometry(geom, None)

            curr_bearing = degrees(self.bearing + radians(90) * side)
            if curr_bearing < 0.0:
                curr_bearing = 360 + curr_bearing
            self.msgbar.emit(
                "Current distance: {:.3F} m, Current bearing: {:.3F} degrees".format(self.y_length, curr_bearing))

            midpoint = QgsPointXY((pt1.x() + pt2.x()) / 2, (pt1.y() + pt2.y()) / 2)
            self.draw_arrow(pt1, midpoint)

    def set_x_length(self, length):
        """
        Sets the length of the polygon in the x axis
        :param length: length of the rectangle
        :type length: float
        """

        self.x_length = length

    def set_y_length(self, length):
        """
        Sets the length of the polygon in the y axis
        :param length: length of the rectangle
        :type length: float
        """

        self.y_length = length

    def deactivate(self):
        """
        Deletes started rectangles and closes all bands
        """
        super().deactivate()
        self.fixed_p2, self.fixed_p3 = None, None


# Base class
class FromCenterBase(QgsMapTool):
    msgbar = pyqtSignal(str)
    rbFinished = pyqtSignal(object)
    rb_reset_signal = pyqtSignal()

    def __init__(self, canvas):
        QgsMapTool.__init__(self, canvas)
        self.canvas = canvas
        self.n_points = 0
        self.rb = None
        self.shift_pressed = None
        self.center = None
        crs = QgsCoordinateReferenceSystem.fromEpsgId(4326)
        self.distance = QgsDistanceArea()
        self.distance.setSourceCrs(crs, QgsProject.instance().transformContext())
        self.distance.setEllipsoid(QgsProject.instance().ellipsoid())
        self.x_length = 0
        self.y_length = 0
        # our own fancy cursor
        self.cursor = QCursor(QPixmap(["16 16 3 1",
                                       "      c None",
                                       ".     c #FF0000",
                                       "+     c #17a51a",
                                       "                ",
                                       "       +.+      ",
                                       "      ++.++     ",
                                       "     +.....+    ",
                                       "    +.  .  .+   ",
                                       "   +.   .   .+  ",
                                       "  +.    .    .+ ",
                                       " ++.    .    .++",
                                       " ... ...+... ...",
                                       " ++.    .    .++",
                                       "  +.    .    .+ ",
                                       "   +.   .   .+  ",
                                       "   ++.  .  .+   ",
                                       "    ++.....+    ",
                                       "      ++.++     ",
                                       "       +.+      "]))

        self.curr_geom = None
        self.last_point = None
        self.curr_angle = 0.0
        self.total_angle = 0.0
        self.rectangle = Rectangle(crs)
        self.angle_ini_rot = 0.0
        self.ini_geom = None
        self.arrow_head = None
        self.arrow_marker = QgsVertexMarker(self.canvas)
        self.arrow_marker.setColor(QColor("green"))
        self.arrow_marker.setIconType(QgsVertexMarker.ICON_TRIANGLE)
        self.arrow_marker.setFillColor(QColor("green"))
        self.arrow_marker.setIconSize(16)
        self.arrow_marker.hide()

    def activate(self):
        """
        Sets the custom cursor on canvas
        """
        self.canvas.setCursor(self.cursor)

    def deactivate(self):
        """
        Deletes started rectangles and closes all bands
        """
        self.n_points = 0
        self.center, self.x_p2, self.y_p2 = None, None, None
        if self.rb:
            self.rb.reset(True)
        self.rb = None

        if self.arrow_head is not None:
            self.arrow_head.reset(True)
        self.arrow_head = None

        self.arrow_marker.hide()

        self.canvas.refresh()

    def keyPressEvent(self, event):
        """
        Overrides method keyPressEvent from QgsMapTool. If the shift key is pressed, sets
        shift_pressed to True and creates apoint and angle of rotation.
        :param event: key event
        :type event: QKeyEvent
        """
        if event.key() == Qt.Key_Shift:
            self.shift_pressed = True

            map_pos = self.toMapCoordinates(self.canvas.mouseLastXY())
            point = transform_point(
                map_pos,
                self.canvas.mapSettings().destinationCrs(),
                QgsCoordinateReferenceSystem.fromEpsgId(4326)
            )
            self.point_ini_rot = point
            self.angle_ini_rot = self.curr_angle

    def keyReleaseEvent(self, event):
        """
        Overrides method keyReleaseEvent from QgsMapTool. If the shift key is released,
        set shift_pressed to false. If the scape key is released, cancel current drawings on canvas.
        :param event: key event
        :type event: QKeyEvent
        """
        if event.key() == Qt.Key_Shift:
            self.shift_pressed = False

        if event.key() == Qt.Key_Escape:
            self.n_points = 0
            self.center = None
            if self.rb:
                self.rb.reset(True)
            self.rb = None
            if self.arrow_head is not None:
                self.arrow_head.reset(True)
            self.arrow_head = None

            self.arrow_marker.hide()
            self.canvas.refresh()
            self.rb_reset_signal.emit()

    def changegeomSRID(self, geom):
        """
        Transforms the geometry of the layer if it has a different SRID than the project
        :param geom: geometry to transform
        :type geom: QgsGeometry
        :return: geometry transformed, or the same if it's not necessary
        :rtype: QgsGeometry
        """
        layer = self.canvas.currentLayer()
        layerCRSSrsid = layer.crs().srsid()
        projectCRSSrsid = QgsMapSettings().destinationCrs().srsid()
        if layerCRSSrsid != projectCRSSrsid:
            g = QgsGeometry.fromPoint(geom)
            g.transform(QgsCoordinateTransform(projectCRSSrsid, layerCRSSrsid))
            ret_point = g.asPoint()
        else:
            ret_point = geom

        return ret_point

    def canvasPressEvent(self, event):
        """
        Overrides method canvasPressEvent from QgsMapTool.
        :param event: click event
        :type event: QgsMapMouseEvent
        """
        raise NotImplementedError

    def canvasMoveEvent(self, event):
        """
        Overrides method canvasMoveEvent from QgsMapTool.
        :param event: move event
        :type event: QgsMapMouseEvent
        """
        raise NotImplementedError

    def draw_arrow(self, start_point, end_point):
        if self.arrow_head is None:
            self.arrow_head = QgsRubberBand(self.canvas, QgsWkbTypes.LineGeometry)
            self.arrow_head.setWidth(4)
            self.arrow_head.setColor(QColor("green"))
        # Clear any existing items from the rubber band
        self.arrow_head.reset(QgsWkbTypes.LineGeometry)

        # Add points to the rubber band
        self.arrow_head.addPoint(start_point, False)  # False indicates not to update canvas immediately
        self.arrow_head.addPoint(end_point, True)  # True indicates to update canvas immediately

        # Now, let's add an arrowhead at the end

        # Create a QgsVertexMarker for the arrowhead
        self.arrow_marker.setCenter(QgsPointXY(end_point))
        self.arrow_marker.setRotation(90 - get_angle_of_line_between_two_points(start_point.x(),
                                                                                start_point.y(),
                                                                                end_point.x(),
                                                                                end_point.y()))
        self.arrow_marker.show()

# Tool class
class RectFromCenterTool(FromCenterBase):
    def __init__(self, canvas):
        """
        Init of the object RectFromCenterTool
        :param canvas: Canvas to draw on and get signals from
        :type canvas: QgsMapCanvas
        """
        FromCenterBase.__init__(self, canvas)

    def canvasPressEvent(self, event):
        """
        Overrides method canvasPressEvent from RectToolBase. When a press event is registered from the canvas,
        this method starts the drawing of a rectangle if it is not already and finishes drawing if it was
        already started.
        :param event: click event
        :type event: QgsMapMouseEvent
        """

        if self.n_points == 0:
            color = QColor(255, 0, 0, 128)
            if self.rb:
                self.rb.reset()
                self.rb = None
            self.rb = QgsRubberBand(self.canvas, True)
            self.rb.setColor(color)
            self.rb.setWidth(1)
            self.canvas.refresh()
            self.rb_reset_signal.emit()

            map_pos = self.toMapCoordinates(event.pos())
            point = transform_point(
                map_pos,
                self.canvas.mapSettings().destinationCrs(),
                QgsCoordinateReferenceSystem.fromEpsgId(4326)
            )
            self.center = point
            self.n_points += 1

        elif self.n_points == 1:
            self.n_points = 0
            self.center = None
            self.last_point = None
            self.curr_angle = 0.0
            self.total_angle = 0.0
            self.shift_pressed = False

            self.rbFinished.emit(self.curr_geom)
            self.curr_geom = None

    def canvasMoveEvent(self, event):
        """
        Overrides method canvasMoveEvent from RectToolBase. This method draws a rectangle from a set point
        to the event point if that set point exists, else does nothing.
        :param event: move event
        :type event: QgsMapMouseEvent
        """
        if not self.rb or not self.center: return

        trans_wgs_to_map = QgsCoordinateTransform(QgsCoordinateReferenceSystem.fromEpsgId(4326),
                                                  self.canvas.mapSettings().destinationCrs(),
                                                  QgsProject.instance().transformContext())
        map_pos = self.toMapCoordinates(event.pos())
        current_point = transform_point(
            map_pos,
            self.canvas.mapSettings().destinationCrs(),
            QgsCoordinateReferenceSystem.fromEpsgId(4326)
        )

        self.msgbar.emit(
            "Adjust lengths along track and across track. Hold Shift to adjust track orientation. Click when finished.")
        if not self.shift_pressed:
            self.curr_geom = self.rectangle.get_rect_from_center(QgsPointXY(self.center), current_point,
                                                                 self.curr_angle)
            self.ini_geom = self.curr_geom

            if self.curr_angle != 0:
                self.curr_geom, self.curr_angle = self.rectangle.get_rect_rotated(self.curr_geom,
                                                                                  QgsPointXY(self.center),
                                                                                  delta=self.curr_angle)

            self.x_length = self.distance.measureLine(self.curr_geom.asPolygon()[0][0],
                                                      self.curr_geom.asPolygon()[0][1])
            self.y_length = self.distance.measureLine(self.curr_geom.asPolygon()[0][1],
                                                      self.curr_geom.asPolygon()[0][2])

            self.curr_geom = self.rectangle.get_rect_projection(self.curr_geom, QgsPointXY(self.center),
                                                                self.x_length, self.y_length)

        elif self.ini_geom is not None:
            if self.last_point is None:
                self.last_point = current_point

            self.curr_geom, self.curr_angle = self.rectangle.get_rect_rotated(self.ini_geom,
                                                                              QgsPointXY(self.center),
                                                                              current_point, self.point_ini_rot,
                                                                              self.angle_ini_rot)

            self.last_point = current_point
            self.curr_geom = self.rectangle.get_rect_projection(self.curr_geom, QgsPointXY(self.center),
                                                                self.x_length, self.y_length)

        if self.curr_geom is not None:
            pt1 = trans_wgs_to_map.transform(self.curr_geom.asPolygon()[0][0])
            pt2 = trans_wgs_to_map.transform(self.curr_geom.asPolygon()[0][1])
            pt3 = trans_wgs_to_map.transform(self.curr_geom.asPolygon()[0][2])
            pt4 = trans_wgs_to_map.transform(self.curr_geom.asPolygon()[0][3])
            geom = QgsGeometry.fromPolygonXY([[pt1, pt2, pt3, pt4]])
            self.rb.setToGeometry(geom, None)

            midpoint = QgsPointXY((pt1.x() + pt2.x()) / 2, (pt1.y() + pt2.y()) / 2)
            self.draw_arrow(pt1, midpoint)


# Tool class
class RectFromCenterFixedTool(FromCenterBase):
    def __init__(self, canvas, x_length=0.0, y_length=0.0):
        """
        Init of the object RectFromCenterFixedTool
        :param canvas: Canvas to draw on and get signals from
        :type canvas: QgsMapCanvas
        :param x_length: horizontal length of the rectangle
        :type x_length: float
        :param y_length: vertical length of the rectangle
        :type y_length: float
        """
        FromCenterBase.__init__(self, canvas)
        self.p2 = None
        self.x_length = x_length
        self.y_length = y_length
        self.diagonal = sqrt(self.x_length / 2 * self.x_length / 2 + self.y_length / 2 * self.y_length / 2)

    def keyReleaseEvent(self, event):
        """
        Overrides method keyPressEvent from RectToolBase. If the shift key is released,
        set shift_pressed to false. If the scape key is released, cancel current drawings on canvas.
        :param event: key event
        :type event: QKeyEvent
        """
        super().keyReleaseEvent(event)
        if event.key() == Qt.Key_Escape:
            self.p2 = None

    def canvasPressEvent(self, event):
        """
        Overrides method canvasPressEvent from RectToolBase. When a press event is registered from the canvas,
        this method starts the drawing of a rectangle if it is not already and finishes drawing if it was
        already started.
        :param event: click event
        :type event: QgsMapMouseEvent
        """
        if self.n_points == 0:
            color = QColor(255, 0, 0, 128)
            if self.rb:
                self.rb.reset()
                self.rb = None
            self.rb = QgsRubberBand(self.canvas, True)
            self.rb.setColor(color)
            self.rb.setWidth(1)
            self.canvas.refresh()
            self.rb_reset_signal.emit()
            map_pos = self.toMapCoordinates(event.pos())
            point = transform_point(
                map_pos,
                self.canvas.mapSettings().destinationCrs(),
                QgsCoordinateReferenceSystem.fromEpsgId(4326)
            )
            self.center = point
            if self.x_length != 0:
                self.diagonal = sqrt(self.x_length / 2 * self.x_length / 2 + self.y_length / 2 * self.y_length / 2)
                self.p2 = self.distance.measureLineProjected(QgsPointXY(self.center), self.diagonal,
                                                             atan2(self.y_length / 2, self.x_length / 2))[1]
            self.n_points += 1

        elif self.n_points == 1:
            self.n_points = 0
            self.center, self.p2 = None, None
            self.last_point = None
            self.rbFinished.emit(self.curr_geom)
            self.curr_geom = None
            self.curr_angle = 0.0
            self.total_angle = 0.0

    def canvasMoveEvent(self, event):
        """
        Overrides method canvasMoveEvent from QgsMapTool. This method draws a rectangle from a set point
        to the event point if that set point exists, else does nothing.

        :param event: move event
        :type event: QgsMapMouseEvent
        """
        if not self.rb or not self.center: return

        trans_wgs_to_map = QgsCoordinateTransform(QgsCoordinateReferenceSystem.fromEpsgId(4326),
                                                  self.canvas.mapSettings().destinationCrs(),
                                                  QgsProject.instance().transformContext())
        map_pos = self.toMapCoordinates(event.pos())
        current_point = transform_point(
            map_pos,
            self.canvas.mapSettings().destinationCrs(),
            QgsCoordinateReferenceSystem.fromEpsgId(4326)
        )

        self.msgbar.emit("Hold Shift to adjust track orientation. Click when finished.")

        if not self.shift_pressed:
            if self.last_point is None:
                self.last_point = self.p2
                self.curr_geom = self.rectangle.get_rect_from_center(QgsPointXY(self.center),
                                                                     self.last_point)
                self.ini_geom = self.curr_geom

                self.curr_geom = self.rectangle.get_rect_projection(self.curr_geom, QgsPointXY(self.center),
                                                                    self.x_length, self.y_length)

        elif self.ini_geom is not None:
            if self.last_point is None:
                self.last_point = current_point

            self.curr_geom, self.curr_angle = self.rectangle.get_rect_rotated(self.ini_geom,
                                                                              QgsPointXY(self.center),
                                                                              current_point, self.point_ini_rot,
                                                                              self.angle_ini_rot)

            self.last_point = current_point
            self.curr_geom = self.rectangle.get_rect_projection(self.curr_geom, QgsPointXY(self.center),
                                                                self.x_length, self.y_length)

        if self.curr_geom is not None:
            pt1 = trans_wgs_to_map.transform(self.curr_geom.asPolygon()[0][0])
            pt2 = trans_wgs_to_map.transform(self.curr_geom.asPolygon()[0][1])
            pt3 = trans_wgs_to_map.transform(self.curr_geom.asPolygon()[0][2])
            pt4 = trans_wgs_to_map.transform(self.curr_geom.asPolygon()[0][3])
            geom = QgsGeometry.fromPolygonXY([[pt1, pt2, pt3, pt4]])

            self.rb.setToGeometry(geom, None)

            midpoint = QgsPointXY((pt1.x() + pt2.x()) / 2, (pt1.y() + pt2.y()) / 2)
            self.draw_arrow(pt1, midpoint)

    def set_x_length(self, length):
        """
        Sets the length of the polygon in the x axis
        :param length: length of the rectangle
        :type length: float
        """

        self.x_length = length

        self.diagonal = sqrt(self.x_length / 2 * self.x_length / 2 + self.y_length / 2 * self.y_length / 2)

    def set_y_length(self, length):
        """
        Sets the length of the polygon in the y axis
        :param length: length of the rectangle
        :type length: float
        """

        self.y_length = length

        self.diagonal = sqrt(self.x_length / 2 * self.x_length / 2 + self.y_length / 2 * self.y_length / 2)
