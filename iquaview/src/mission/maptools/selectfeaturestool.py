# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
Map tool to select multiple features of a layer graphically
"""
import logging
import math

from PyQt5.QtCore import Qt, pyqtSignal, QPoint
from PyQt5.QtGui import QColor, QCursor
from qgis.core import QgsGeometry, QgsWkbTypes, QgsPointXY, QgsFeature
from qgis.gui import QgsMapTool, QgsRubberBand

from iquaview_lib.utils.calcutils import distance_plane
from iquaview_lib.utils.qgisutils import transform_point, calc_tolerance

logger = logging.getLogger(__name__)


class SelectFeaturesTool(QgsMapTool):
    """ Map tool to select multiple features of a layer graphically """
    selection_clicked = pyqtSignal(list)

    def __init__(self, mission_track, canvas):
        """
        Init of the object SelectFeaturesTool
        :param mission_track: Mission track to read from and write info to
        :type mission_track: MissionTrack
        :param canvas: Canvas to draw on and get signals from
        :type canvas: QgsMapCanvas
        """
        QgsMapTool.__init__(self, canvas)
        self.setCursor(Qt.ArrowCursor)
        self.mission_track = mission_track
        self.layer = self.mission_track.get_mission_layer()
        self.rubber_band = QgsRubberBand(self.canvas())
        self.dragging = False
        self.vertex = None
        self.startcoord = None
        self.rubber_band_points = None
        self.selection_polygon = []
        self.indexes_within_list = []
        self.band_finished = True
        self.m_ctrl = False
        self.p0, self.p1, self.p2, self.p3 = None, None, None, None
        self.current_crs = self.canvas().mapSettings().destinationCrs()
        self.mission_track.mission_changed.connect(self.update_rubber_band)
        self.mission_track.step_removed.connect(self.remove_rubber_band)
        self.wp = self.mission_track.find_waypoints_in_mission()
        self.layer.startEditing()
        self.rubber_band_points = QgsRubberBand(self.canvas(), QgsWkbTypes.PointGeometry)
        self.rubber_band_points.setIcon(QgsRubberBand.ICON_CIRCLE)
        self.rubber_band_points.setIconSize(10)
        self.rubber_band_points.setColor(QColor("green"))

        self.canvas().destinationCrsChanged.connect(self.canvas_crs_changed)

    def canvas_crs_changed(self):
        """
        When the canvas CRS is changed, we need to update the current CRS
        """
        if self.rubber_band_points is not None:
            list_wp = self.rubber_band_points.asGeometry().asMultiPoint()
            self.rubber_band_points.reset(QgsWkbTypes.PointGeometry)
            for wp in list_wp:
                if wp is not None:
                    pos = transform_point(wp, self.current_crs, self.canvas().mapSettings().destinationCrs())
                    self.rubber_band_points.addPoint(pos)

        self.current_crs = self.canvas().mapSettings().destinationCrs()

    def keyPressEvent(self, event):
        """
        Overrides method keyPressEvent from QgsMapTool. If the control key is pressed,
        sets mCtrl to True.
        :param event: key event
        :type event: QKeyEvent
        """
        if event.key() == Qt.Key_Control:
            self.m_ctrl = True
            self.setCursor(Qt.ArrowCursor)

    def keyReleaseEvent(self, event):
        """
        Overrides method keyPressEvent from QgsMapTool. If the control key is released,
        set mCtrl to false. If the scape key is released, cancel current drawings on canvas.
        :param event: key event
        :type event: QKeyEvent
        """
        if event.key() == Qt.Key_Control:
            self.m_ctrl = False
            pos = self.canvas().mapFromGlobal(QCursor.pos())
            self.set_cursor(pos)

        if event.key() == Qt.Key_Escape:
            self.p0, self.p1, self.p2, self.p3 = None, None, None, None
            if self.rubber_band:
                self.rubber_band.reset(True)
            self.close_polygon_band()
            self.band_finished = True
            self.canvas().refresh()

    def canvasPressEvent(self, event):
        """
        Overrides method canvasPressEvent from QgsMapTool. When a press event is registered from the canvas,
        this method starts the drawing of a rectangle if it is not already and finishes drawing if it was
        already started. All the waypoints inside the rectangle when the second click happens will be selected.
        :param event: click event
        :type event: QgsMapMouseEvent
        """
        if event.button() == Qt.LeftButton:
            point = event.pos()
            # check if we have clicked on a vertex
            vertex = self.find_vertex_at(event.pos())
            if self.m_ctrl and vertex is not None:
                # if we have clicked on a vertex, identify which one
                # check if was already in the selection list
                if vertex not in self.indexes_within_list:
                    # add it
                    self.indexes_within_list.append(vertex)
                    self.update_rubber_band()
                else:
                    self.remove_rubber_band(vertex)

                self.band_finished = True
                self.selection_clicked.emit(self.indexes_within_list)

            if not self.m_ctrl and vertex is not None and vertex in self.indexes_within_list:
                # dragging
                self.dragging = True
                self.vertex = vertex
                self.startcoord = self.toMapCoordinates(event.pos())

            # if no polygon band, start it
            elif not self.selection_polygon and self.band_finished and not self.m_ctrl:
                self.band_finished = False
                self.rubber_band = QgsRubberBand(self.canvas(), QgsWkbTypes.PolygonGeometry)
                self.rubber_band.setWidth(2)
                select_green = QColor("green")
                select_green.setAlpha(128)
                self.rubber_band.setColor(select_green)

                if event.button() == Qt.LeftButton:
                    # Left click -> add vertex
                    self.p0 = self.toMapCoordinates(QPoint(point.x(), point.y()))
                    self.selection_polygon.append(self.p0)
            elif len(self.selection_polygon) == 1 and not self.m_ctrl:
                if event.button() == Qt.LeftButton:
                    # Left click -> add vertex
                    p0_canvas = self.toCanvasCoordinates(self.p0)
                    p2_canvas = QPoint(point.x(), point.y())
                    self.p1 = self.toMapCoordinates(QPoint(p2_canvas.x(), p0_canvas.y()))
                    self.p3 = self.toMapCoordinates(QPoint(p0_canvas.x(), p2_canvas.y()))
                    self.p2 = self.toMapCoordinates(p2_canvas)

                    self.selection_polygon.append(self.p1)
                    self.selection_polygon.append(self.p2)
                    self.selection_polygon.append(self.p3)
                    self.band_finished = True
                    self.set_selection()
                    self.close_polygon_band()
                    self.selection_clicked.emit(self.indexes_within_list)

    def find_vertex_at(self, pos):
        """
        Get the vertex that is closer to the clicked point
        :param pos: The point that we've clicked
        :type pos: QPoint
        :return: vertex or None
        :rtype: int
        """
        tolerance = calc_tolerance(self.canvas().width(), self.canvas().height())
        if len(self.wp) > 0:
            dist_to_vertex = []
            for vert in self.wp:
                transf_a1 = transform_point(
                    QgsPointXY(vert.x(), vert.y()),
                    self.layer.sourceCrs(),
                    self.canvas().mapSettings().destinationCrs()
                )
                a1 = self.toCanvasCoordinates(transf_a1)
                dist_to_vertex.append(math.hypot(pos.x() - a1.x(), pos.y() - a1.y()))

            vertex = dist_to_vertex.index(min(dist_to_vertex))
            if min(dist_to_vertex) > tolerance:
                return None

            return vertex

        return None

    def set_cursor(self, pos: QPoint):
        """
        Set the cursor type based on the mouse position and conditions.

        :param pos: The mouse position.
        :type pos: QPoint
        """
        vertex = self.find_vertex_at(pos)
        if vertex is not None and vertex in self.indexes_within_list and not self.m_ctrl:
            self.setCursor(Qt.CrossCursor)
        else:
            self.setCursor(Qt.ArrowCursor)

    def move_selected_vertices(self, delta_x, delta_y, selected_indices):
        """
        Move selected vertices by a given delta_x and delta_y.

        :param delta_x: change in x-coordinate
        :type delta_x: float
        :param delta_y: change in y-coordinate
        :type delta_y: float
        :param selected_indices: list of indices of selected vertices
        :type selected_indices: list of int
        """
        for index in selected_indices:
            if 0 <= index < len(self.wp):
                transf_a1 = transform_point(
                    QgsPointXY(self.wp[index].x(), self.wp[index].y()),
                    self.layer.sourceCrs(),
                    self.canvas().mapSettings().destinationCrs()
                )

                layer_pt = self.toLayerCoordinates(self.layer, QgsPointXY(transf_a1.x() + delta_x,
                                                                          transf_a1.y() + delta_y))

                self.wp[index].setX(layer_pt.x())
                self.wp[index].setY(layer_pt.y())

                self.mission_track.change_position(index, layer_pt)

        self.layer.updateExtents()

    def canvasMoveEvent(self, event):
        """
        Overrides method canvasMoveEvent from QgsMapTool. This method draws a rectangle from a set point
        to the event point if that set point exists, else does nothing.
        :param event: move event
        :type event: QgsMapMouseEvent
        """
        self.set_cursor(event.pos())

        if self.dragging:
            if self.rubber_band_points:
                self.rubber_band_points.reset(QgsWkbTypes.PointGeometry)

            canvas_point = self.toCanvasCoordinates(self.startcoord)
            # Check distance with initial point
            dist = distance_plane(canvas_point.x(), canvas_point.y(),
                                  event.pos().x(), event.pos().y())
            tolerance = calc_tolerance(self.canvas().width(), self.canvas().height())
            if dist > tolerance:
                # Calculate the delta_x and delta_y for the selected vertex
                delta_x = event.pos().x() - canvas_point.x()
                delta_y = event.pos().y() - canvas_point.y()

                if len(self.indexes_within_list) > 0:

                    selected_vertices = self.mission_track.find_waypoints_in_mission(self.indexes_within_list)
                    for vertex in selected_vertices:
                        transf_a1 = transform_point(
                            QgsPointXY(vertex.x(), vertex.y()),
                            self.layer.sourceCrs(),
                            self.canvas().mapSettings().destinationCrs()
                        )
                        canvas_point = self.toCanvasCoordinates(transf_a1)

                        map_point = self.toMapCoordinates(QPoint(canvas_point.x() + delta_x,
                                                                 canvas_point.y() + delta_y))
                        self.rubber_band_points.addPoint(map_point)
            else:
                delta_x = 0
                delta_y = 0
                self.update_rubber_band()

        if not self.band_finished and not self.m_ctrl:
            p0_canvas = self.toCanvasCoordinates(self.p0)
            p2_canvas = event.pos()
            self.p1 = self.toMapCoordinates(QPoint(p2_canvas.x(), p0_canvas.y()))
            self.p3 = self.toMapCoordinates(QPoint(p0_canvas.x(), p2_canvas.y()))
            self.p2 = self.toMapCoordinates(p2_canvas)

            self.selection_polygon.append(self.p1)
            self.selection_polygon.append(self.p2)
            self.selection_polygon.append(self.p3)
            self.rubber_band.setToGeometry(QgsGeometry.fromPolygonXY([self.selection_polygon]), None)
            self.selection_polygon.pop()
            self.selection_polygon.pop()
            self.selection_polygon.pop()

    def canvasReleaseEvent(self, event):
        """
        Overrides the method canvasReleaseEvent from QgsMapTool. If a dragging action is happening, it will
        stop the dragging and set the point to the event position, if enough distance was made.
        :param event: mouse event
        :type event: QgsMapMouseEvent
        """
        if self.dragging and event.button() == Qt.LeftButton:
            self.dragging = False
            current_map_point = self.toMapCoordinates(event.pos())
            start_canvas_point = self.toCanvasCoordinates(self.startcoord)
            # Check distance with initial point
            dist = distance_plane(start_canvas_point.x(), start_canvas_point.y(),
                                  event.pos().x(), event.pos().y())
            tolerance = calc_tolerance(self.canvas().width(), self.canvas().height())
            if dist > tolerance:
                # Calculate the delta_x and delta_y for the selected vertex
                delta_x = current_map_point.x() - self.startcoord.x()
                delta_y = current_map_point.y() - self.startcoord.y()

                # Move other selected vertices by the same delta_x and delta_y
                self.move_selected_vertices(delta_x, delta_y, self.indexes_within_list)

            self.selection_clicked.emit(self.indexes_within_list)

            self.vertex = None
            self.set_geometry()

    def set_selection(self):
        """
        Set vertices highlight according to polygon
        """
        # Check which features are within the polygon
        mission_track = self.layer.getFeatures()  # get mission track feature

        for f in mission_track:  # loop although mission layer only has one feature
            vertices_it = f.geometry().vertices()
        polygon_geom = QgsGeometry.fromPolygonXY([self.selection_polygon])
        vertices_within_list = []
        vertex_index = 0

        # Highlight them using a point rubber band
        for v in vertices_it:
            v = transform_point(
                QgsPointXY(v.x(), v.y()),
                self.layer.sourceCrs(),
                self.canvas().mapSettings().destinationCrs()
            )
            point_geom = QgsGeometry.fromPointXY(v)
            if point_geom.within(polygon_geom):
                vertices_within_list.append(v)
                if vertex_index not in self.indexes_within_list:  # only add if not already present
                    self.indexes_within_list.append(vertex_index)
                    self.rubber_band_points.addPoint(QgsPointXY(v.x(), v.y()))
            vertex_index = vertex_index + 1

    def update_rubber_band(self):
        """ Updates rubber band """
        if self.rubber_band_points:
            self.rubber_band_points.reset(QgsWkbTypes.PointGeometry)
        self.wp = self.mission_track.find_waypoints_in_mission()

        if len(self.indexes_within_list) > 0:

            selected_vertices = self.mission_track.find_waypoints_in_mission(self.indexes_within_list)
            for vertex_index, point in enumerate(selected_vertices):
                pc = transform_point(
                    QgsPointXY(point.x(), point.y()),
                    self.layer.sourceCrs(),
                    self.canvas().mapSettings().destinationCrs()
                )
                self.rubber_band_points.addPoint(pc)

        self.set_geometry()

    def remove_rubber_band(self, wp):
        """
        Removes point form the rubber band
        :param wp: point to remove
        :type wp: int
        """
        if self.indexes_within_list:
            self.indexes_within_list.remove(wp)
            self.update_rubber_band()

    def set_geometry(self):
        """
        Save rubber band to geometry of the layer
        """
        if self.layer.featureCount() == 0:
            # no feature yet created
            f = QgsFeature()
            if len(self.wp) == 1:
                f.setGeometry(QgsGeometry.fromPointXY(QgsPointXY(self.wp[0].x(), self.wp[0].y())))
            else:
                f.setGeometry(QgsGeometry.fromPolyline(self.wp))
            # self.layer.dataProvider().addFeatures([f])
            self.layer.addFeatures([f])
        else:
            # mission feature present, edit geometry
            feats = self.layer.getFeatures()
            for f in feats:
                if len(self.wp) == 1:
                    self.layer.changeGeometry(f.id(),
                                              QgsGeometry.fromPointXY(QgsPointXY(self.wp[0].x(), self.wp[0].y())))
                else:
                    self.layer.changeGeometry(f.id(), QgsGeometry.fromPolyline(self.wp))
        self.layer.commitChanges()
        self.layer.startEditing()

    def close_polygon_band(self):
        """ Deletes rectangle rubber band """
        self.selection_polygon = []
        if self.rubber_band is not None:
            self.rubber_band.reset()
            self.canvas().scene().removeItem(self.rubber_band)
        self.rubber_band = None

    def close_highlight_band(self):
        """ Deletes waypoint higlight rubber band"""
        self.rubber_band_points.reset()
        self.canvas().scene().removeItem(self.rubber_band_points)
        self.rubber_band_points = None

    def get_indexes_within_list(self):
        """
        Returns the indexes of the selected waypoints
        :return: indexes of the selected waypoints
        :rtype: list
        """
        return self.indexes_within_list

    def close_band(self):
        """ This method closes rubber bands and connected signals to deactivate the tool safely """
        if self.rubber_band:
            self.close_polygon_band()
        if self.rubber_band_points:
            self.close_highlight_band()

        try:
            self.mission_track.mission_changed.disconnect(self.update_rubber_band)
        except:
            logger.info("no connected to signal")
        try:
            self.mission_track.step_removed.disconnect(self.remove_rubber_band)
        except:
            logger.info("no connected to signal")
        self.layer.commitChanges()
