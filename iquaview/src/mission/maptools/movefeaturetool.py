# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
Map tool for moving, resizing and rotating mission features
"""

# pylint: disable=E0611

import logging
from math import cos
from typing import Union, Tuple

from PyQt5.QtCore import Qt, QPoint
from PyQt5.QtGui import QColor, QCursor, QPixmap, QTransform
from PyQt5.QtWidgets import QMessageBox
from qgis.core import (QgsRectangle,
                       QgsFeatureRequest,
                       QgsFeature,
                       QgsWkbTypes,
                       QgsPointXY,
                       QgsGeometry,
                       QgsDistanceArea,
                       QgsProject,
                       QgsCoordinateReferenceSystem,
                       QgsApplication,
                       QgsMessageLog,
                       Qgis)
from qgis.gui import QgsMapTool, QgsRubberBand, QgsMapMouseEvent, QgsMapCanvas

from iquaview.src.mission.missiontrack import MissionTrack
from iquaview_lib.utils.calcutils import distance_plane, find_geometric_center
from iquaview_lib.utils.qgisutils import transform_point, calc_tolerance

logger = logging.getLogger(__name__)


def init_rubber_band(canvas: QgsMapCanvas, color: QColor, width: int, icon: QgsRubberBand.IconType) -> QgsRubberBand:
    """
    Initialize and return a rubber band for the mission feature
    :param canvas: Canvas to associate with the rubber band
    :type canvas: QgsMapCanvas
    :param color: Color of the rubber band
    :type color: QColor
    :param width: Width of the rubber band
    :type width: int
    :param icon: Icon of the rubber band
    :type icon: QgsRubberBand.Icon
    :return: Initialized rubber band
    :rtype: QgsRubberBand
    """
    rubber_band = QgsRubberBand(canvas, QgsWkbTypes.PointGeometry)
    rubber_band.setColor(color)
    rubber_band.setWidth(width)
    rubber_band.setIcon(icon)
    return rubber_band


class MoveFeatureTool(QgsMapTool):
    """ Map tool for moving, resizing and rotating mission features """

    def __init__(self, mission_track: MissionTrack, canvas: QgsMapCanvas, msglog: QgsMessageLog):
        """
        Init of the object MoveFeatureTool

        :param mission_track: Mission track to read from and write info to
        :type mission_track: MissionTrack
        :param canvas: Canvas to draw on
        :type canvas: QgsMapCanvas
        :param msglog: Message log for logging
        :type msglog: QgsMessageLog
        """
        QgsMapTool.__init__(self, canvas)
        self.band: Union[QgsRubberBand, None] = None
        self.feature = None
        self.startcoord = None
        self.mission_track = mission_track
        self.mission_length = len(self.mission_track.find_waypoints_in_mission())
        self.msglog = msglog
        self.layer = mission_track.get_mission_layer()
        self.clicked_outside_layer = False
        self.m_shift = False
        self.scaling_by_clicking = False
        self.rotation_by_clicking = False
        self.rot_center = None
        self.rot_center_rb = None
        self.temp_geom = None
        self.ini_point = None
        self.ini_rot_point = None
        self.last_rot_angle = 0.0
        self.curr_angle = 0.0
        self.curr_geom = next(self.layer.dataProvider().getFeatures()).geometry()
        self.geom_hist = []
        self.undoing = False
        self.rectangle_rb = None
        self.rotation_line_rb = None
        self.top_left_rb = None
        self.top_right_rb = None
        self.bottom_left_rb = None
        self.bottom_right_rb = None
        self.mid_left_rb = None
        self.mid_right_rb = None
        self.mid_top_rb = None
        self.mid_bottom_rb = None
        self.rot_top_rb = None
        self.reference_point = None
        self.scale_axis = "xy"
        # convert svg file to pixmap
        self.move_cursor = QCursor(QPixmap(":/resources/mIconMove.svg"), 0, 0)
        self.rotate_cursor = QCursor(QPixmap(":/resources/mIconRotate.svg"), 0, 0)
        self.scale_cursor = QCursor(QPixmap(":/resources/mIconScale.svg"), 0, 0)
        self.setCursor(self.move_cursor)

        if self.mission_length > 1:
            red_color = QColor(255, 0, 0, 128)
            black_color = QColor(0, 0, 0, 255)
            width = 5

            # Create new rectangle rubber band
            self.rectangle_rb = QgsRubberBand(self.canvas(), QgsWkbTypes.LineGeometry)
            self.rectangle_rb.setColor(red_color)
            self.rectangle_rb.setWidth(1)
            self.rectangle_rb.setLineStyle(Qt.DashLine)
            self.rectangle_rb.setFillColor(Qt.transparent)

            self.rotation_line_rb = QgsRubberBand(self.canvas(), QgsWkbTypes.LineGeometry)
            self.rotation_line_rb.setColor(red_color)
            self.rotation_line_rb.setWidth(1)
            self.rotation_line_rb.setLineStyle(Qt.DashLine)
            self.rotation_line_rb.setFillColor(Qt.transparent)

            self.top_left_rb = init_rubber_band(self.canvas(), black_color, width, QgsRubberBand.ICON_BOX)
            self.top_right_rb = init_rubber_band(self.canvas(), black_color, width, QgsRubberBand.ICON_BOX)
            self.bottom_left_rb = init_rubber_band(self.canvas(), black_color, width, QgsRubberBand.ICON_BOX)
            self.bottom_right_rb = init_rubber_band(self.canvas(), black_color, width, QgsRubberBand.ICON_BOX)
            self.mid_top_rb = init_rubber_band(self.canvas(), black_color, width, QgsRubberBand.ICON_BOX)
            self.mid_bottom_rb = init_rubber_band(self.canvas(), black_color, width, QgsRubberBand.ICON_BOX)
            self.mid_left_rb = init_rubber_band(self.canvas(), black_color, width, QgsRubberBand.ICON_BOX)
            self.mid_right_rb = init_rubber_band(self.canvas(), black_color, width, QgsRubberBand.ICON_BOX)
            self.rot_top_rb = init_rubber_band(self.canvas(), black_color, width, QgsRubberBand.ICON_CIRCLE)

            self.fit_rectangle_to_geom(QgsGeometry.fromRect(self.layer.extent()))

        logger.info("Move %s", mission_track.get_mission_name())

        self.msglog.logMessage(
            "Move the mission by dragging it or by clicking on the map. "
            "Rotate the mission holding the Shift key and clicking mouse button left. "
            "Scale the mission clicking mouse button left.",
            "Info message", Qgis.Info)

    def closest_vertex(self, point: QPoint) -> Tuple[QgsPointXY, int, int, int, float]:
        """
        Find the vertex under the mouse cursor

        :param point: Point to check
        :type point: QPoint
        :return: The vertex under the mouse cursor
        :rtype: QgsPointXY
        """
        # get the map coordinates of the click
        point = self.toMapCoordinates(point)
        # get the closest vertex
        points = [QgsPointXY(self.top_left_rb.getPoint(0)),
                  QgsPointXY(self.top_right_rb.getPoint(0)),
                  QgsPointXY(self.bottom_left_rb.getPoint(0)),
                  QgsPointXY(self.bottom_right_rb.getPoint(0))]
        geom = QgsGeometry.fromPolygonXY([points])
        closest_vertex = geom.closestVertex(point)
        return closest_vertex

    def closest_side(self, point: QPoint) -> Tuple[QgsPointXY, int, int, int, float]:
        """
        Find the side under the mouse cursor

        :param point: Point to check
        :type point: QPoint
        :return: The vertex under the mouse cursor
        :rtype: QgsPointXY
        """
        # get the map coordinates of the click
        point = self.toMapCoordinates(point)
        # get the closest vertex

        points = [QgsPointXY(self.mid_top_rb.getPoint(0)),
                  QgsPointXY(self.mid_left_rb.getPoint(0)),
                  QgsPointXY(self.mid_right_rb.getPoint(0)),
                  QgsPointXY(self.mid_bottom_rb.getPoint(0))]
        geom = QgsGeometry.fromPolygonXY([points])
        side = geom.closestVertex(point)
        return side

    def opposite_vertex(self, point: QgsPointXY):
        """
        Find and return the opposite vertex for the given point.

        :param point: The input point.
        :type point: QgsPointXY
        :return: The opposite vertex if found, or None if not found.
        :rtype: QgsPointXY | None
        """
        if point == QgsPointXY(self.top_left_rb.getPoint(0)):
            return QgsPointXY(self.bottom_right_rb.getPoint(0))
        if point == QgsPointXY(self.bottom_right_rb.getPoint(0)):
            return QgsPointXY(self.top_left_rb.getPoint(0))
        if point == QgsPointXY(self.top_right_rb.getPoint(0)):
            return QgsPointXY(self.bottom_left_rb.getPoint(0))
        if point == QgsPointXY(self.bottom_left_rb.getPoint(0)):
            return QgsPointXY(self.top_right_rb.getPoint(0))
        return None

    def opposite_side(self, point: QgsPointXY):
        """
        Find and return the opposite side for the given point.

        :param point: The input point.
        :type point: QgsPointXY
        :return: The opposite side if found, or None if not found.
        :rtype: QgsPointXY | None
        """
        if point == QgsPointXY(self.mid_top_rb.getPoint(0)):
            return QgsPointXY(self.mid_bottom_rb.getPoint(0))
        if point == QgsPointXY(self.mid_bottom_rb.getPoint(0)):
            return QgsPointXY(self.mid_top_rb.getPoint(0))
        if point == QgsPointXY(self.mid_left_rb.getPoint(0)):
            return QgsPointXY(self.mid_right_rb.getPoint(0))
        if point == QgsPointXY(self.mid_right_rb.getPoint(0)):
            return QgsPointXY(self.mid_left_rb.getPoint(0))
        return None

    def canvasMoveEvent(self, event: QgsMapMouseEvent):
        """
        Override of QgsMapTool mouse move event. If there is dragging happening, move or rotate the mission
        depending on whether the Shift key is pressed down or not.
        :param event: mouse event
        :type event: QgsMapMouseEvent
        """
        modifiers = QgsApplication.keyboardModifiers()
        # check modifiers
        self.m_shift = (modifiers == Qt.ShiftModifier and self.mission_length > 1)
        # check if the mouse is over the rubber band
        if self.band is None and self.mission_length > 1:
            vertex = self.closest_vertex(event.pos())
            canvas_vertex = self.toCanvasCoordinates(vertex[0])
            rotation_vertex = self.toCanvasCoordinates(QgsPointXY(self.rot_top_rb.getPoint(0)))
            side = self.closest_side(event.pos())
            canvas_side = self.toCanvasCoordinates(side[0])

            tolerance = calc_tolerance(self.canvas().width(), self.canvas().height())
            if self.m_shift:
                self.setCursor(self.rotate_cursor)
            elif distance_plane(canvas_vertex.x(), canvas_vertex.y(), event.pos().x(), event.pos().y()) < tolerance:
                extent = self.canvas().extent()
                min_point = self.toCanvasCoordinates(QgsPointXY(extent.xMinimum(), extent.yMinimum()))
                max_point = self.toCanvasCoordinates(QgsPointXY(extent.xMaximum(), extent.yMaximum()))
                transform = QTransform()
                pixmap = QPixmap(":/resources/mIconScale.svg")

                if (self.closest_vertex(min_point)[0] == vertex[0]
                        or (self.closest_vertex(max_point)[0] == vertex[0])):
                    transform.scale(-1, 1)
                    transform.rotate(-self.canvas().rotation())
                    self.setCursor(
                        QCursor(pixmap.transformed(transform, Qt.SmoothTransformation), 0, 0)
                    )
                else:
                    transform = QTransform()
                    transform.rotate(self.canvas().rotation())
                    self.setCursor(
                        QCursor(pixmap.transformed(transform, Qt.SmoothTransformation), 0, 0)
                    )
                self.scale_axis = "xy"
                self.reference_point = self.opposite_vertex(vertex[0])

            elif distance_plane(canvas_side.x(), canvas_side.y(), event.pos().x(), event.pos().y()) < tolerance:
                transform = QTransform()
                pixmap = QPixmap(":/resources/mIconSideScale.svg")

                if (QgsPointXY(self.mid_top_rb.getPoint(0)) == side[0]
                        or QgsPointXY(self.mid_bottom_rb.getPoint(0)) == side[0]):
                    transform.rotate(90)
                    transform.rotate(self.canvas().rotation())
                    self.setCursor(
                        QCursor(pixmap.transformed(transform, Qt.SmoothTransformation), 0, 0)
                    )
                    self.scale_axis = "y"
                    self.reference_point = self.opposite_side(side[0])

                else:
                    transform = QTransform()
                    transform.rotate(self.canvas().rotation())
                    self.setCursor(
                        QCursor(pixmap.transformed(transform, Qt.SmoothTransformation), 0, 0)
                    )
                    self.scale_axis = "x"
                    self.reference_point = self.opposite_side(side[0])

            elif distance_plane(rotation_vertex.x(), rotation_vertex.y(), event.pos().x(), event.pos().y()) < tolerance:
                self.setCursor(self.rotate_cursor)
            else:
                self.setCursor(self.move_cursor)

        # scale
        if (self.mission_length > 1
                and self.scaling_by_clicking):
            distance = QgsDistanceArea()
            distance.setSourceCrs(self.layer.crs(), QgsProject.instance().transformContext())
            distance.setEllipsoid(self.layer.crs().ellipsoidAcronym())
            self.scale_and_project_band(event.pos())
        # rotate
        elif (self.mission_length > 1
              and ((self.band and self.m_shift) or self.rotation_by_clicking)):
            distance = QgsDistanceArea()
            distance.setSourceCrs(self.layer.crs(), QgsProject.instance().transformContext())
            distance.setEllipsoid(self.layer.crs().ellipsoidAcronym())

            end_rot_point = self.toLayerCoordinates(self.layer, event.pos())
            self.curr_angle = (
                    distance.bearing(self.rot_center, end_rot_point)
                    - distance.bearing(self.rot_center, self.ini_rot_point)
                    + self.last_rot_angle
            )
            self.rotate_and_project_band(self.curr_angle)
        # move
        elif self.band and not self.m_shift:
            point = self.toLayerCoordinates(self.layer, event.pos())
            start_coord = self.toLayerCoordinates(self.layer, self.startcoord)
            offset_x = point.x() - start_coord.x()
            offset_y = point.y() - start_coord.y()
            new_geom = QgsGeometry(self.curr_geom)
            new_geom.translate(offset_x, offset_y)
            self.band.reset(QgsWkbTypes.LineGeometry)
            self.band.setToGeometry(new_geom, self.layer)
            self.fit_rectangle_to_geom(QgsGeometry.fromRect(new_geom.boundingBox()))

    def canvasPressEvent(self, event):
        """
        Override of QgsMapTool mouse press event.
        :param event: mouse event
        :type event: QgsMapMouseEvent
        """

        if event.button() == Qt.LeftButton and self.m_shift and self.band is None \
                and self.mission_length > 1:
            self.start_rotation()

        elif event.button() == Qt.LeftButton and not self.m_shift:
            self.handle_move(event)

    def handle_move(self, event):
        """
        Handle the move operation based on the mouse click event.
        :param event: mouse event
        :type event: QgsMapMouseEvent
        """
        if self.mission_length <= 1:
            self.start_move(event.pos())
            return

        vertex = self.closest_vertex(event.pos())
        canvas_vertex = self.toCanvasCoordinates(vertex[0])
        rotation_vertex = self.toCanvasCoordinates(QgsPointXY(self.rot_top_rb.getPoint(0)))
        side = self.closest_side(event.pos())
        canvas_side = self.toCanvasCoordinates(side[0])
        tolerance = calc_tolerance(self.canvas().width(), self.canvas().height())
        distance = distance_plane(canvas_vertex.x(), canvas_vertex.y(), event.pos().x(), event.pos().y())
        distance_to_side = distance_plane(canvas_side.x(), canvas_side.y(), event.pos().x(), event.pos().y())
        distance_to_rotation = distance_plane(rotation_vertex.x(), rotation_vertex.y(), event.pos().x(),
                                              event.pos().y())

        if self.band is None:
            if distance < tolerance or distance_to_side < tolerance:
                self.scaling_by_clicking = True
                self.start_scale(event.pos())
            elif distance_to_rotation < tolerance:
                self.rotation_by_clicking = True
                self.start_rotation()
            else:
                self.start_move(event.pos())
        else:
            self.start_move(event.pos())

    def canvasReleaseEvent(self, event):
        """
        Override of QgsMapTool mouse release event. I
        :param event: mouse event
        :type event: QgsMapMouseEvent
        """
        # scale
        if self.scaling_by_clicking:
            self.setCursor(self.move_cursor)
            self.scaling_by_clicking = False
            self.project_mission()

        # rotate
        elif ((event.button() == Qt.LeftButton and self.m_shift)
              or self.rotation_by_clicking):
            self.setCursor(self.move_cursor)
            self.rotation_by_clicking = False
            self.project_mission()

        # move
        elif event.button() == Qt.LeftButton and not self.m_shift:
            if not self.band:  # if there is no dragging
                if self.clicked_outside_layer and self.mission_length > 0:
                    confirmation_msg = "Do you want to move the mission ? \n\n" \
                                       "First waypoint will set on the marked point."
                    reply = QMessageBox.question(self.parent(), 'Movement Confirmation',
                                                 confirmation_msg, QMessageBox.Yes, QMessageBox.No)

                    if reply == QMessageBox.Yes:
                        feats = self.layer.getFeatures()
                        for f in feats:  # will be only one
                            if self.layer.geometryType() == QgsWkbTypes.LineGeometry:
                                list_wp = f.geometry().asPolyline()
                                self.startcoord = self.toMapCoordinates(self.layer, list_wp[0])
                            elif self.layer.geometryType() == QgsWkbTypes.PointGeometry:
                                wp = f.geometry().asPoint()
                                self.startcoord = self.toMapCoordinates(self.layer, wp)
                            self.feature = f
                        self.move_position(event.pos())

            # if there is dragging
            elif self.layer and self.feature:
                self.move_position(event.pos())

    def keyPressEvent(self, event):
        """
        Overrides method keyPressEvent from QgsMapTool.
        :param event: key event
        :type event: QKeyEvent
        """
        if event.key() == Qt.Key_Shift \
                and self.band is None \
                and self.mission_length > 1:
            self.setCursor(self.rotate_cursor)
            self.m_shift = True

        elif event.modifiers() == Qt.ControlModifier and event.key() == Qt.Key_Z:
            self.undo_geometry_change()

    def keyReleaseEvent(self, event):
        """
        Overrides method keyReleaseEvent from QgsMapTool.
        :param event: key event
        :type event: QKeyEvent
        :param event:
        """
        if event.key() == Qt.Key_Shift and self.m_shift and self.mission_length > 1:
            self.setCursor(self.move_cursor)
            self.m_shift = False
            self.hide_rotation_center()
            self.hide_rubber_band()
            self.fit_rectangle_to_geom(QgsGeometry.fromRect(self.curr_geom.boundingBox()))

    def start_scale(self, event_pos: QPoint):
        """
        Start scaling the mission.
        :param event_pos: mouse position
        :type event_pos: QPoint
        """
        self.show_rubber_band()
        self.ini_point = event_pos
        self.scale_and_project_band(event_pos)
        self.hide_corners()

    def start_rotation(self):
        """
        Start rotation of the mission.
        """
        self.show_rubber_band()
        self.show_center()
        self.rotate_and_project_band()
        self.hide_corners()
        self.ini_rot_point = self.toLayerCoordinates(self.layer, self.canvas().mouseLastXY())

    def start_move(self, event_pos: QPoint):
        """
        Start the move of the mission.
        :param event_pos: mouse event position
        :type event_pos: QPoint
        """

        self.hide_rubber_band()
        self.feature = None

        logger.debug("layer feature count %s", self.layer.featureCount())
        if not self.layer:
            return

        point = self.toMapCoordinates(event_pos)
        search_radius = QgsMapTool.searchRadiusMU(self.canvas())

        rect = QgsRectangle()
        rect.setXMinimum(point.x() - search_radius)
        rect.setXMaximum(point.x() + search_radius)
        rect.setYMinimum(point.y() - search_radius)
        rect.setYMaximum(point.y() + search_radius)

        canvas_crs = self.canvas().mapSettings().destinationCrs()
        layer_crs = self.layer.sourceCrs()
        layer_extent = self.layer.extent()

        # Transform rect from canvas crs to layer crs
        top_left = transform_point(
            QgsPointXY(rect.xMinimum(), rect.yMaximum()),
            canvas_crs,
            layer_crs
        )
        bot_right = transform_point(
            QgsPointXY(rect.xMaximum(), rect.yMinimum()),
            canvas_crs,
            layer_crs
        )
        rect = QgsRectangle(top_left, bot_right)

        if rect.contains(layer_extent) or rect.intersects(layer_extent):

            rq = QgsFeatureRequest().setFilterRect(rect)
            f = QgsFeature()
            self.layer.getFeatures(rq).nextFeature(f)
            if f.geometry():
                self.band = self.create_rubber_band()
                self.band.setToGeometry(f.geometry(), self.layer)
                self.band.show()
                self.startcoord = self.toMapCoordinates(event_pos)
                self.feature = f
                self.clicked_outside_layer = False
        else:
            self.clicked_outside_layer = True

    def move_position(self, pos):
        """
        Moves the mission to the specified position
        :param pos: desired final position
        :type pos: Qpoint
        """
        # Save current geometry to history
        curr_geom = next(self.layer.dataProvider().getFeatures()).geometry()
        self.geom_hist.append(curr_geom)

        start_point = self.toLayerCoordinates(self.layer, self.startcoord)
        end_point = self.toLayerCoordinates(self.layer, pos)

        distance_layer = QgsDistanceArea()
        distance_layer.setSourceCrs(self.layer.crs(), QgsProject.instance().transformContext())
        distance_layer.setEllipsoid(self.layer.crs().ellipsoidAcronym())

        # Find vertical distance to be translated
        a = distance_layer.bearing(start_point, end_point)
        d = distance_layer.measureLine(start_point, end_point)
        vertical_dist = abs(cos(a) * d)
        self.layer.startEditing()

        # If translating a point or if translation is small,
        # do a simple translation (very small error, proportional to vertical dist)
        if vertical_dist < 9000 or self.layer.geometryType() == QgsWkbTypes.PointGeometry:
            dx = end_point.x() - start_point.x()
            dy = end_point.y() - start_point.y()
            self.layer.translateFeature(self.feature.id(), dx, dy)

        # If translation is big, translate and project (small and constant error due to approximations in calculations)
        else:
            ini_coords = next(self.layer.dataProvider().getFeatures()).geometry().asPolyline()
            end_coords = []
            if len(ini_coords) > 1:
                dx = end_point.x() - start_point.x()
                dy = end_point.y() - start_point.y()
                end_c = QgsPointXY(ini_coords[0].x() + dx, ini_coords[0].y() + dy)
                end_coords.append(end_c)
                for i in range(1, len(ini_coords)):
                    dist = distance_layer.measureLine(ini_coords[i - 1], ini_coords[i])
                    angle = distance_layer.bearing(ini_coords[i - 1], ini_coords[i])
                    end_c_projected = distance_layer.measureLineProjected(end_coords[i - 1], dist, angle)[1]
                    end_coords.append(end_c_projected)

                feature = next(self.layer.dataProvider().getFeatures())
                self.layer.changeGeometry(feature.id(), QgsGeometry.fromPolylineXY(end_coords))

        self.hide_rubber_band()

        # get new waypoints and put them into mission structure
        feats = self.layer.getFeatures()
        for f in feats:  # will be only one
            if self.layer.geometryType() == QgsWkbTypes.LineGeometry:
                list_wp = f.geometry().asPolyline()
                for idx, point in enumerate(list_wp):
                    self.mission_track.change_position(idx, point)
            elif self.layer.geometryType() == QgsWkbTypes.PointGeometry:
                wp = f.geometry().asPoint()
                self.mission_track.change_position(0, wp)
        self.layer.commitChanges()

        # rotation center and geometry have changed, set rot_center to none to recalculate when needed
        self.rot_center = None
        self.last_rot_angle = 0.0
        self.curr_geom = next(self.layer.dataProvider().getFeatures()).geometry()
        self.fit_rectangle_to_geom(QgsGeometry.fromRect(self.curr_geom.boundingBox()))


    def deactivate(self):
        """
        Deactive the tool.
        """
        self.hide_rectangle_rb()
        self.hide_rotation_center()
        self.hide_rubber_band()
        self.hide_corners()
        self.msglog.logMessage("")

    def create_rubber_band(self) -> QgsRubberBand:
        """
        Creates a new rubber band.
        """
        band = QgsRubberBand(self.canvas())
        band.setColor(QColor("green"))
        band.setWidth(2)
        band.setLineStyle(Qt.DashLine)
        return band

    def show_center(self):
        """
        Shows rotation center of the mission with a point rubber band
        """
        if self.mission_length > 1:
            feature = next(self.layer.dataProvider().getFeatures())
            list_wp = feature.geometry().asPolyline()
            # if self.rot_center is None or self.rot_center_rb is None:
            center = find_geometric_center(list_wp)
            self.rot_center = QgsPointXY(center[0], center[1])
            self.rot_center_rb = QgsRubberBand(self.canvas())
            self.rot_center_rb.setColor(QColor("black"))
            self.rot_center_rb.setWidth(3)
            center = transform_point(
                self.rot_center,
                self.layer.sourceCrs(),
                self.canvas().mapSettings().destinationCrs(),
            )
            self.rot_center_rb.setToGeometry(QgsGeometry.fromPointXY(center))
            self.rot_center_rb.update()
            # else:
            #     self.rot_center_rb.show()

    def hide_rotation_center(self):
        """
        Hides the rotation center of the mission and deletes the point rubber band
        """
        if self.rot_center_rb is not None:
            self.rot_center_rb.hide()

    def show_rubber_band(self):
        """
        Creates and shows a rubber band with the geometry of the mission
        """
        if self.mission_length > 1:
            self.band = self.create_rubber_band()
            self.band.setToGeometry(self.curr_geom, self.layer)

    def hide_rubber_band(self):
        """
        Hides and deletes the rubber band of the geometry of the mission
        """
        if self.band is not None:
            self.band.hide()
            self.band = None

    def hide_rectangle_rb(self):
        """
        Hides and deletes the rectangle rubber band
        """
        if self.rectangle_rb is not None:
            self.rectangle_rb.hide()
            self.rectangle_rb = None

    def rotate_and_project_band(self, rot_angle=0.0):
        """
        Rebuilds the initial geometry of the mission rotated with the angle defined by rot_angle, and it gets stored in
        the geometry of self.band

        :param rot_angle: Angle used to rotate the geometry in radians
        :type rot_angle: float
        """
        distance = QgsDistanceArea()
        distance.setSourceCrs(self.layer.crs(), QgsProject.instance().transformContext())
        distance.setEllipsoid(self.layer.crs().ellipsoidAcronym())

        ini_coords = self.curr_geom.asPolyline()
        end_coords = []
        if len(ini_coords) > 1:
            dist = distance.measureLine(self.rot_center, ini_coords[0])
            angle = distance.bearing(self.rot_center, ini_coords[0]) + rot_angle
            end_first_wp = distance.measureLineProjected(self.rot_center, dist, angle)[1]
            end_coords.append(end_first_wp)
            for i in range(1, len(ini_coords)):
                dist = distance.measureLine(self.rot_center, ini_coords[i])
                angle = distance.bearing(self.rot_center, ini_coords[i]) + rot_angle
                end_c = distance.measureLineProjected(self.rot_center, dist, angle)[1]
                end_coords.append(end_c)

            self.temp_geom = QgsGeometry().fromPolylineXY(end_coords)
            self.band.setToGeometry(self.temp_geom, self.layer)
            self.rectangle_rb.setToGeometry(QgsGeometry.fromRect(self.temp_geom.boundingBox()), self.layer)

    def scale_and_project_band(self, pos: QPoint):
        """
        Rebuilds the initial geometry of the mission scaled and it gets stored in
        the geometry of self.band

        :param pos: pos is the current mouse position
        :type pos: QPoint
        :return:
        """
        distance = QgsDistanceArea()
        distance.setSourceCrs(self.layer.crs(), QgsProject.instance().transformContext())
        distance.setEllipsoid(self.layer.crs().ellipsoidAcronym())

        ini_coords = self.curr_geom.asPolyline()
        end_coords = []

        ref_point = transform_point(
            self.reference_point,
            self.canvas().mapSettings().destinationCrs(),
            QgsCoordinateReferenceSystem.fromEpsgId(4326),
        )
        canvas_rot_ref = self.toCanvasCoordinates(self.reference_point)

        distance_to_first_clicked_point = abs(distance_plane(
            canvas_rot_ref.x(), canvas_rot_ref.y(), self.ini_point.x(), self.ini_point.y()))
        distance_to_current_point = abs(distance_plane(
            canvas_rot_ref.x(), canvas_rot_ref.y(), pos.x(), pos.y()))
        current_scale = distance_to_current_point / distance_to_first_clicked_point

        if len(ini_coords) > 1:

            for i in range(0, len(ini_coords)):
                dist = distance.measureLine(ref_point, ini_coords[i]) * current_scale
                angle = distance.bearing(ref_point, ini_coords[i])
                if self.scale_axis == "xy":
                    end_c = distance.measureLineProjected(ref_point, dist, angle)[1]
                    end_coords.append(end_c)
                elif self.scale_axis == "x":
                    # Update only the x-coordinate with respect to a reference point
                    if ref_point:
                        reference_x = ref_point.x()  # Use the reference point's X-coordinate
                    else:
                        reference_x = self.rot_center.x()  # Use the rotation center's X-coordinate as reference
                    new_x = reference_x + (ini_coords[i].x() - reference_x) * current_scale
                    # Keep the y-coordinate unchanged
                    new_y = ini_coords[i].y()
                    # Create a new QgsPoint with updated x and y coordinates
                    updated_point = QgsPointXY(new_x, new_y)
                    end_coords.append(updated_point)
                elif self.scale_axis == "y":
                    # Update only the y-coordinate with respect to a reference point
                    if ref_point:
                        reference_y = ref_point.y()  # Use the reference point's Y-coordinate
                    else:
                        reference_y = self.rot_center.y()  # Use the rotation center's Y-coordinate as reference
                    new_y = reference_y + (ini_coords[i].y() - reference_y) * current_scale
                    # Keep the x-coordinate unchanged
                    new_x = ini_coords[i].x()
                    # Create a new QgsPoint with updated x and y coordinates
                    updated_point = QgsPointXY(new_x, new_y)
                    end_coords.append(updated_point)

            self.temp_geom = QgsGeometry().fromPolylineXY(end_coords)
            self.band.setToGeometry(self.temp_geom, self.layer)
            self.rectangle_rb.setToGeometry(QgsGeometry.fromRect(self.temp_geom.boundingBox()), self.layer)

    def project_mission(self):
        """
        Copy the changes from the rotated and projected rubber band to the geometry of the mission
        """
        # Save current geometry to history
        curr_geom = next(self.layer.dataProvider().getFeatures()).geometry()
        self.geom_hist.append(curr_geom)

        list_wp_projected = []
        list_wp = self.temp_geom.asPolyline()
        if len(list_wp) > 1:
            for index, wp in enumerate(list_wp):
                self.mission_track.change_position(index, wp)
                list_wp_projected.append(wp)

            feature = next(self.layer.dataProvider().getFeatures())
            self.layer.startEditing()
            self.layer.changeGeometry(feature.id(), QgsGeometry.fromPolylineXY(list_wp_projected))
            self.layer.commitChanges()

        self.curr_geom = next(self.layer.dataProvider().getFeatures()).geometry()

        self.hide_rotation_center()
        self.hide_rubber_band()
        self.update_corners(self.curr_geom)
        self.show_corners()

    def update_corners(self, geom: QgsGeometry):
        """
        Updates the corners of the rubber band
        :param geom: Geometry of the rubber band
        :type geom: QgsGeometry
        """
        self.top_left_rb.reset(QgsWkbTypes.PointGeometry)
        self.top_left_rb.setToGeometry(
            QgsGeometry.fromPointXY(
                self.toMapCoordinates(
                    self.layer,
                    QgsPointXY(geom.boundingBox().xMinimum(), geom.boundingBox().yMaximum())
                )
            )
        )
        self.bottom_right_rb.reset(QgsWkbTypes.PointGeometry)
        self.bottom_right_rb.setToGeometry(
            QgsGeometry.fromPointXY(
                self.toMapCoordinates(
                    self.layer,
                    QgsPointXY(geom.boundingBox().xMaximum(), geom.boundingBox().yMinimum())
                )
            )
        )
        self.bottom_left_rb.reset(QgsWkbTypes.PointGeometry)
        self.bottom_left_rb.setToGeometry(
            QgsGeometry.fromPointXY(
                self.toMapCoordinates(
                    self.layer,
                    QgsPointXY(geom.boundingBox().xMinimum(), geom.boundingBox().yMinimum())
                )
            )
        )
        self.top_right_rb.reset(QgsWkbTypes.PointGeometry)
        self.top_right_rb.setToGeometry(
            QgsGeometry.fromPointXY(
                self.toMapCoordinates(
                    self.layer,
                    QgsPointXY(geom.boundingBox().xMaximum(), geom.boundingBox().yMaximum())
                )
            )
        )

        self.mid_top_rb.reset(QgsWkbTypes.PointGeometry)
        self.mid_top_rb.setToGeometry(
            QgsGeometry.fromPointXY(
                self.toMapCoordinates(
                    self.layer,
                    QgsPointXY((geom.boundingBox().xMinimum() + geom.boundingBox().xMaximum()) / 2,
                               geom.boundingBox().yMaximum())
                )
            )
        )

        self.mid_bottom_rb.reset(QgsWkbTypes.PointGeometry)
        self.mid_bottom_rb.setToGeometry(
            QgsGeometry.fromPointXY(
                self.toMapCoordinates(
                    self.layer,
                    QgsPointXY((geom.boundingBox().xMinimum() + geom.boundingBox().xMaximum()) / 2,
                               geom.boundingBox().yMinimum())
                )
            )
        )

        self.mid_left_rb.reset(QgsWkbTypes.PointGeometry)
        self.mid_left_rb.setToGeometry(
            QgsGeometry.fromPointXY(
                self.toMapCoordinates(
                    self.layer,
                    QgsPointXY(geom.boundingBox().xMinimum(),
                               (geom.boundingBox().yMinimum() + geom.boundingBox().yMaximum()) / 2)
                )
            )
        )

        self.mid_right_rb.reset(QgsWkbTypes.PointGeometry)
        self.mid_right_rb.setToGeometry(
            QgsGeometry.fromPointXY(
                self.toMapCoordinates(
                    self.layer,
                    QgsPointXY(geom.boundingBox().xMaximum(),
                               (geom.boundingBox().yMinimum() + geom.boundingBox().yMaximum()) / 2)
                )
            )
        )

        self.rot_top_rb.reset(QgsWkbTypes.PointGeometry)
        rotation_point = QgsPointXY(
            (geom.boundingBox().xMinimum() + geom.boundingBox().xMaximum()) / 2,
            geom.boundingBox().yMaximum() + 0.2 * (geom.boundingBox().yMaximum() - geom.boundingBox().yMinimum())
        )
        middle_top_point = QgsPointXY(
            (geom.boundingBox().xMinimum() + geom.boundingBox().xMaximum()) / 2,
            geom.boundingBox().yMaximum()
        )
        rot_point = self.toMapCoordinates(
            self.layer,
            rotation_point
        )
        map_middle_top_point = self.toMapCoordinates(
            self.layer,
            middle_top_point
        )
        # convert to map coordinates again and add to the rubber band
        # apply translation to the point
        self.rot_top_rb.setToGeometry(QgsGeometry.fromPointXY(rot_point))
        self.rotation_line_rb.reset(QgsWkbTypes.LineGeometry)
        self.rotation_line_rb.addPoint(rot_point)
        self.rotation_line_rb.addPoint(map_middle_top_point)

    def hide_corners(self):
        """
        Hides the corners of the rubber band
        """
        if self.mission_length > 1:
            self.top_left_rb.hide()
            self.bottom_right_rb.hide()
            self.bottom_left_rb.hide()
            self.top_right_rb.hide()
            self.rot_top_rb.hide()
            self.rotation_line_rb.hide()
            self.mid_right_rb.hide()
            self.mid_left_rb.hide()
            self.mid_top_rb.hide()
            self.mid_bottom_rb.hide()

    def show_corners(self):
        """
        Shows the corners of the rubber band
        """
        if self.mission_length > 1:
            self.top_left_rb.show()
            self.bottom_right_rb.show()
            self.bottom_left_rb.show()
            self.top_right_rb.show()
            self.rot_top_rb.show()
            self.rotation_line_rb.show()
            self.mid_right_rb.show()
            self.mid_left_rb.show()
            self.mid_top_rb.show()
            self.mid_bottom_rb.show()

    def fit_rectangle_to_geom(self, geom: QgsGeometry):
        """
        Fits the rectangle to the geometry
        :param geom: Geometry of the rubber band
        :type geom: QgsGeometry
        """
        if self.mission_length > 1:
            self.rectangle_rb.setToGeometry(geom, self.layer)
            self.update_corners(geom)

    def undo_geometry_change(self):
        """
        Undo last geometry change
        """
        if len(self.geom_hist) <= 0 or self.undoing:
            return

        self.undoing = True
        prev_geometry = self.geom_hist.pop()

        # Change layer geometry
        feature = next(self.layer.dataProvider().getFeatures())
        self.layer.startEditing()
        self.layer.changeGeometry(feature.id(), prev_geometry)

        self.canvas().waitWhileRendering()

        # get geometry waypoints and put them into mission structure
        # This also updates start end markers
        if prev_geometry.type() == QgsWkbTypes.LineGeometry:
            list_wp = prev_geometry.asPolyline()
            for index, wp in enumerate(list_wp):
                self.mission_track.change_position(index, wp)
        elif prev_geometry.type() == QgsWkbTypes.PointGeometry:
            wp = prev_geometry.asPoint()
            self.mission_track.change_position(0, wp)
        self.layer.commitChanges()

        self.fit_rectangle_to_geom(QgsGeometry.fromRect(prev_geometry.boundingBox()))

        self.curr_geom = prev_geometry
        self.last_rot_angle = 0.0
        self.curr_angle = 0.0
        self.rot_center = None
        self.undoing = False
