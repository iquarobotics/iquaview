# -*- coding: utf-8 -*-
# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Map tools for drawing Rectangles on the canvas
"""

from math import cos, sin, radians, sqrt, atan2

from qgis.core import (QgsGeometry,
                       QgsPointXY,
                       QgsDistanceArea,
                       QgsProject,
                       QgsCoordinateReferenceSystem)

from iquaview_lib.utils.calcutils import calc_is_collinear


class Rectangle:
    def __init__(self, crs):
        """
        Init of the object Rectangle
        :param crs: coordinate reference system
        :type crs: QgsCoordinateReferenceSystem
        """
        self.distance = QgsDistanceArea()
        self.distance.setSourceCrs(crs, QgsProject.instance().transformContext())
        self.distance.setEllipsoid(QgsProject.instance().ellipsoid())

    def get_rect_from_center(self, pc, p2, angle=0.0):
        """
        Creates a rectangle from a center point, a corner point and an angle

        :param pc: center point of the geometry
        :type pc: QgsPointXY
        :param p2: a corner of the geometry
        :type p2: QgsPointXY
        :param angle: angle of the geometry, 0 by default. In radians
        :type angle: float
        :return: 4 points geometry
        :rtype: QgsGeometry
        """
        if angle == 0:
            x_offset = abs(p2.x() - pc.x())
            y_offset = abs(p2.y() - pc.y())

            pt1 = QgsPointXY(pc.x() - x_offset, pc.y() - y_offset)
            pt2 = QgsPointXY(pc.x() - x_offset, pc.y() + y_offset)
            pt3 = QgsPointXY(pc.x() + x_offset, pc.y() + y_offset)
            pt4 = QgsPointXY(pc.x() + x_offset, pc.y() - y_offset)

            geom = QgsGeometry.fromPolygonXY([[pt1, pt2, pt3, pt4]])

        else:
            x_offset = (cos(angle) * (p2.x() - pc.x()) + sin(angle) * (p2.y() - pc.y()))
            y_offset = -(-sin(angle) * (p2.x() - pc.x()) + cos(angle) * (p2.y() - pc.y()))

            pt1 = QgsPointXY(pc.x() - x_offset, pc.y() - y_offset)
            pt2 = QgsPointXY(pc.x() - x_offset, pc.y() + y_offset)
            pt3 = QgsPointXY(pc.x() + x_offset, pc.y() + y_offset)
            pt4 = QgsPointXY(pc.x() + x_offset, pc.y() - y_offset)

            geom = QgsGeometry.fromPolygonXY([[pt1, pt2, pt3, pt4]])

        return geom

    def get_rect_rotated(self, geom, cp, ep=QgsPointXY(0, 0), ip=QgsPointXY(0, 0), delta=0):
        """
        Rotates a geometry by a specified angle around a center point.

        :param geom: Geometry to be rotated
        :type geom: QgsGeometry
        :param cp: Center point of the geometry
        :type cp: QgsPointXY
        :param ep: Point marking the end of the rotation, defaults to (0, 0)
        :type ep: QgsPointXY, optional
        :param ip: Point marking the beginning of the rotation, defaults to (0, 0)
        :type ip: QgsPointXY, optional
        :param delta: Extra angle of rotation in radians, defaults to 0
        :type delta: float, optional
        :return: Rotated geometry and total angle of rotation
        :rtype: QgsGeometry, float
        """

        angle_1 = atan2(ep.y() - cp.y(), ep.x() - cp.x())
        angle_2 = atan2(ip.y() - cp.y(), ip.x() - cp.x())
        angle_rotation = delta + (angle_1 - angle_2)

        coords = []
        for ring in geom.asPolygon():
            rotated_ring = []
            for point in ring:
                dx = point.x() - cp.x()
                dy = point.y() - cp.y()
                radius = sqrt(dx ** 2 + dy ** 2)
                theta = atan2(dy, dx) + angle_rotation
                rotated_x = cp.x() + radius * cos(theta)
                rotated_y = cp.y() + radius * sin(theta)
                rotated_ring.append(QgsPointXY(rotated_x, rotated_y))
            coords.append(rotated_ring)

        rotated_geom = QgsGeometry().fromPolygonXY(coords)

        return rotated_geom, angle_rotation

    def get_rect_by3_points(self, p1, p2, p3, length=0):
        """
        Creates a projected rectangle over an spheroid from 3 different points
        :param p1: A rectangle point
        :type p1: QgsPointXY
        :param p2: A rectangle point
        :type p2: QgsPointXY
        :param p3: A rectangle point
        :type p3: QgsPointXY
        :param length: distance from p2 to p3
        :type length: float
        :return: Geometry with the 4 points of the rectangle
        :rtype: QgsGeometry
        """
        angle_exist = self.distance.bearing(p1, p2)
        side = calc_is_collinear(p1.x(), p1.y(), p2.x(), p2.y(), p3.x(),
                                 p3.y())  # check if x_p2 > x_p1 and inverse side
        if side == 0:
            return None
        if length == 0:
            length = self.distance.measureLine(p2, p3)
        p3 = self.distance.measureLineProjected(p2, length, angle_exist + radians(90) * side)[1]
        p4 = self.distance.measureLineProjected(p1, length, angle_exist + radians(90) * side)[1]
        geom = QgsGeometry.fromPolygonXY([[p1, p2, p3, p4]])
        return geom

    def get_rect_projection(self, rect_geom, cp, x_length=0, y_length=0):
        """
        Transforms the rectangle geometry to its projection on the real world, making all its angles 90°

        :param rect_geom: 4 point geometry
        :type rect_geom: QgsGeometry
        :param cp: central point of the geometry
        :type cp: QgsPointXY
        :param x_length: distance between first and second points of the rect_geom
        :type x_length: float
        :param y_length: distance between second and third points of the rect_geom
        :type y_length: float
        :return: geometry projected to map
        :rtype: QgsGeometry
        """

        if x_length == 0 and y_length == 0:
            proj_geom = self.get_rect_by3_points(rect_geom.asPolygon()[0][0],
                                                 rect_geom.asPolygon()[0][1],
                                                 rect_geom.asPolygon()[0][2])

        else:
            point_two = self.distance.measureLineProjected(rect_geom.asPolygon()[0][0],
                                                           x_length,
                                                           self.distance.bearing(rect_geom.asPolygon()[0][0],
                                                                                 rect_geom.asPolygon()[0][1]))[1]
            point_three = self.distance.measureLineProjected(rect_geom.asPolygon()[0][0],
                                                             y_length,
                                                             self.distance.bearing(rect_geom.asPolygon()[0][1],
                                                                                   rect_geom.asPolygon()[0][3]))[1]

            proj_geom = self.get_rect_by3_points(rect_geom.asPolygon()[0][0],
                                                 point_two,
                                                 point_three,
                                                 y_length)

        if proj_geom is not None:

            p1 = proj_geom.asPolygon()[0][0]
            p2 = proj_geom.asPolygon()[0][1]
            p3 = proj_geom.asPolygon()[0][2]
            p4 = proj_geom.asPolygon()[0][3]

            px = (p1.x() + p3.x()) / 2.0
            py = (p1.y() + p3.y()) / 2.0

            p1 = QgsPointXY(p1.x() - px + cp.x(), p1.y() - py + cp.y())
            p2 = QgsPointXY(p2.x() - px + cp.x(), p2.y() - py + cp.y())
            p3 = QgsPointXY(p3.x() - px + cp.x(), p3.y() - py + cp.y())
            p4 = QgsPointXY(p4.x() - px + cp.x(), p4.y() - py + cp.y())

            proj_geom = QgsGeometry.fromPolygonXY([[p1, p2, p3, p4]])

            return proj_geom

        else:
            return rect_geom
