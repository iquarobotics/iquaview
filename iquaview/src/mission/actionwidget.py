# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.


"""
Action widget to configure in options menu
"""

import logging

from qgis.gui import QgsCollapsibleGroupBox

from iquaview_lib.ui.ui_action import Ui_ActionWidget
from iquaview_lib.utils.textvalidator import check_line_edit_starts_by_slash

logger = logging.getLogger(__name__)


class ActionWidget(QgsCollapsibleGroupBox, Ui_ActionWidget):
    def __init__(self, title="Action", action_id=None, id=None, description=None, collapsed=False, parent=None):
        """
        Constructor
        """
        super().__init__(parent)
        self.setupUi(self)
        self.setObjectName(str(id))
        self.setWindowTitle("ActionWidget")
        self.setTitle(title)
        self.setCollapsed(collapsed)

        if not collapsed:
            self.show()

        self.action_id_lineEdit.setText(action_id)
        self.action_id_lineEdit.textChanged.connect(self.check_action_id)
        self.description_lineEdit.setText(description)
        self.action_name_lineEdit.setText(title)
        self.action_name_lineEdit.textChanged.connect(self.setTitle)

    def check_action_id(self):
        """ Check that action id start by / """
        check_line_edit_starts_by_slash(self.action_id_lineEdit)
