# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Manages the creation, loading, saving and uploading of missions.
"""

import datetime
import logging
import os
import subprocess
from typing import Union

from PyQt5.QtCore import pyqtSignal, QObject
from PyQt5.QtWidgets import QDialog, QMessageBox, QFileDialog
from PyQt5.QtGui import QColor
from qgis.core import QgsVectorLayer, QgsMapLayer, QgsSingleSymbolRenderer

from iquaview.src.mission.inserttemplatewidget import InsertTemplateWidget
from iquaview.src.mission.maptools import selectfeaturestool
from iquaview.src.mission.maptools.edittool import EditTool
from iquaview.src.mission.maptools.movefeaturetool import MoveFeatureTool
from iquaview.src.mission.missionedition.waypointeditwidget import WaypointEditWidget
from iquaview.src.mission.missioninfo import MissionInfo
from iquaview.src.mission.missiontrack import MissionTrack
from iquaview.src.mission.newmissiondlg import NewMissionDlg
from iquaview_lib.xmlconfighandler.missionactionshandler import MissionActionsHandler

logger = logging.getLogger(__name__)


class MissionController(QObject):
    """ Manages the creation, loading, saving and uploading of missions. """
    template_closed = pyqtSignal()
    stop_mission_editing = pyqtSignal()
    mission_added = pyqtSignal(QgsVectorLayer)
    mission_removed_signal = pyqtSignal()
    finish_mission_action_signal = pyqtSignal()

    def __init__(self, config, vehicle_info, proj, canvas, view, wp_dock, template_dock, minfo_dock, msglog):
        """
        Init of the object MissionController
        :param config: Config object with various settings for the missions
        :type config: Config
        :param vehicle_info: Object with information about the vehicle
        :type vehicle_info: VehicleInfo
        :param proj: Project currently open
        :type proj: QgsProject
        :param canvas: Map canvas to use
        :type canvas: QgsMapCanvas
        :param view: Tree view with all the layers
        :type view: QgsLayerTreeView
        :param wp_dock: Dock for the waypoint edit widget
        :type wp_dock: QDockWidget
        :param template_dock: Dock for the template edit widget
        :type template_dock: QDockWidget
        :param minfo_dock: Dock for mission info widget
        :type minfo_dock: QDockWidget
        :param msglog: Log for messages
        :type msglog: QgsMessageLog
        """
        super().__init__()
        self.config = config
        self.vehicle_info = vehicle_info
        self.proj = proj
        self.canvas = canvas
        self.view = view
        self.wp_dock = wp_dock
        self.current_mission: Union[MissionTrack, None] = None  # To keep track of which mission is currently selected
        self.editing_mission = None  # To keep track of which mission is being edited
        self.template_dock = template_dock
        self.minfo_dock = minfo_dock
        self.msglog = msglog

        self.tool_edit_wp_mission = None
        self.tool_move_feature = None
        self.selectfeattool = None
        self.template_insertion_widget = None
        self.missioninfo = None
        self.wpeditw = None

        self.editing = False
        self.mission_list = []

    def get_edit_wp_mission_tool(self):
        """ Returns the edit waypoint mission tool. """
        return self.tool_edit_wp_mission

    def get_mission_list(self):
        """ Returns mission_list"""
        return self.mission_list

    def new_mission(self):
        """
        Add new mission to current project
        :return: return True if mission is created correctly, otherwise False
        """
        # display dialog for creating mission
        new_mission_dlg = NewMissionDlg()
        result = new_mission_dlg.exec_()

        if result == QDialog.Accepted:
            name = new_mission_dlg.get_mission_name()

            date = datetime.datetime.utcnow()
            datestr = date.strftime('%y%m%d%H%M%S')
            # create  mission structure
            default_track_color = self.config.settings['style_all_missions_color']
            default_markers_color = self.config.settings['style_all_start_end_markers_color']
            mt = MissionTrack(datestr + '_' + name,
                              os.path.dirname(self.proj.fileName()) + '/missions/' + datestr + '_' + name + '.xml',
                              canvas=self.canvas, proj=self.proj, track_color=default_track_color,
                              markers_color=default_markers_color)

            # render layer
            mt.render_mission()
            mt.set_modified(True)
            self.mission_list.append(mt)
            self.mission_added.emit(mt.get_mission_layer())

            return True

        return False

    def load_mission(self, mission_path):
        """
        Load a mission from the specified path
        :param mission_path: path of the mission, can be absolute or relative
        """
        # Check if path is in relative or absolute form
        project_folder = os.path.dirname(self.proj.fileName())
        if not os.path.isabs(mission_path):
            mission_path = os.path.normpath(project_folder + "/" + mission_path)

        logger.info(f"Loading mission {mission_path}")

        # set layer name from the path specified
        name = os.path.splitext(os.path.basename(mission_path))[0]
        # Create new missiontrack and load from file
        default_track_color = self.config.settings['style_all_missions_color']
        default_markers_color = self.config.settings['style_all_start_end_markers_color']
        mt = MissionTrack(name, canvas=self.canvas, proj=self.proj, track_color=default_track_color,
                          markers_color=default_markers_color)
        try:
            mt.load_mission(mission_path)
            mt.render_mission()
            mt_layer = mt.get_mission_layer()
            self.mission_list.append(mt)
            self.check_mission_actions(mt)
            # add new layer in the project
            self.mission_added.emit(mt_layer)
            mt_layer.setAbstract(mission_path)
            mt.update_start_end_markers()
        except Exception as e:
            raise Exception(f"Load Error: {e}") from e

    def load_missions(self, files):
        """
        Load a list of missions
        :param files: list of paths to all the missions to load
        :type files: list
        """
        not_loaded_files = []
        for file in files:
            try:
                self.load_mission(file)
            except Exception as e:
                not_loaded_files.append(file)

        if len(not_loaded_files) > 0:
            QMessageBox.warning(None, "Files not imported",
                                ("Some files could not be imported as missions to the project:\n\n{0}".format(
                                    '\n'.join(['- {}'] * len(not_loaded_files)))).format(*not_loaded_files))

    def check_mission_actions(self, mt):
        """
        Checks if all actions of a mission track are present in the config xml
        :param mt: mission track
        :type mt: MissionTrack
        """
        self.config_action_set = self.get_action_id_set()
        length = mt.get_mission_length()
        mission_action_set = set()

        # find all actions from the mission and store them in a set
        for i in range(0, length):
            ms = mt.get_step(i)
            actions = ms.get_actions()
            for act in actions:
                action_id = act.get_action_id()
                mission_action_set.add(action_id)

        # find all actions from the mission that are not in the auv config xml
        if len(mission_action_set) > 0:
            diff_set = mission_action_set.difference(self.config_action_set)

            # for every action that does not appear in the auv config xml
            for e in diff_set:
                # find all waypoints that have this action
                wp_list = []
                for i in range(0, length):
                    ms = mt.get_step(i)
                    actions = ms.get_actions()
                    if len(actions) > 0:
                        for act in actions:
                            action_id = act.get_action_id()
                            if e == action_id:
                                wp_list.append(i)

                # Show warning message about this action of the mission
                self.msglog.logMessage(
                    "Mission " + mt.get_mission_name() + " has action '" + e +
                    "' that is not present in the auv config file. (wp " + str(wp_list)[1:-1] + ")")

    def get_action_id_set(self):
        """
        Returns all action ids from the config xml
        :return: action id set
        :rtype: set
        """
        namespace = self.vehicle_info.get_vehicle_namespace()
        config_filename = os.path.abspath(os.path.expanduser(self.config.settings['last_auv_config_xml']))
        ma_handler = MissionActionsHandler(config_filename)
        actions = ma_handler.get_actions()
        action_set = set()
        for action in actions:
            for value in action:
                if value.tag == 'action_id':
                    action_set.add(namespace + value.text)
        logger.info(str(len(action_set)) + " mission actions loaded from config file")
        return action_set

    def remove_mission(self, layer):
        """ Remove a mission with associated layer 'layer'
        :param layer: associtated layer to the mission
        """
        for mt in self.mission_list:
            if mt.get_mission_layer() == layer:
                logger.info(f"Removing mission {mt.get_mission_name()}")
                mt.remove_start_end_markers()
                self.mission_list.remove(mt)
                # Stop editing tool if the mission removed was being edited
                if self.editing_mission is not None and self.editing_mission.get_mission_layer() == layer:
                    self.finish_edit_wp_mission()
                    self.finish_select_features_mission()
                    self.close_template_editing()
                    self.finish_move_mission()
                    self.stop_mission_editing.emit()

        self.mission_removed_signal.emit()

    def highlight_mission(self):
        """
        Highlight the current mission
        """
        if self.current_mission is not None:
            layer = self.current_mission.get_mission_layer()
            if layer.customProperty("highlighted") is not None and layer.customProperty("highlighted"):
                return

            layer.setCustomProperty('highlighted', True)  # prepare to highlight
            renderer_symbol = self.current_mission.create_highlighted_track_renderer(
                self.current_mission.mission_symbol,
                QColor("orange")
            )
            new_renderer = QgsSingleSymbolRenderer(renderer_symbol)

            # Apply the renderer to the layer
            layer.setRenderer(new_renderer)

            # Trigger a repaint to see the changes
            layer.triggerRepaint()

    def unhighlight_mission(self):
        # check highlight and reset
        if self.current_mission is not None:
            layer = self.current_mission.get_mission_layer()
            if layer.customProperty("highlighted") is not None:
                layer.setCustomProperty('highlighted', False)
                renderer = QgsSingleSymbolRenderer(self.current_mission.mission_symbol.clone())
                layer.setRenderer(renderer)
                layer.triggerRepaint()

    def clear_current_mission(self):
        """
        Clear current mission and unhighlight it
        """
        self.unhighlight_mission()
        self.current_mission = None

    def set_current_mission(self, layer):
        """
        Set layer 'layer' to current_mission
        :param layer:
        """
        # find mt corresponding to the layer
        for mt in self.mission_list:
            if mt.get_mission_layer() == layer:
                self.current_mission = mt

    def get_current_mission(self):
        """ Returns current mission"""
        return self.current_mission

    def get_current_mission_layer(self) -> QgsMapLayer:
        """
        Returns current mission layer
        :return: Returns current mission layer
        :rtype: QgsMapLayer
        """
        return self.current_mission.get_mission_layer()

    def get_current_mission_absolute_path(self):
        """ Returns current mission's absolute path """
        return self.current_mission.get_mission_abs_path()

    def get_current_mission_name(self):
        """ Returns current mission name"""
        return self.current_mission.get_mission_name()

    def set_current_mission_name(self, name):
        """
        Set new name to current mission
        :param name: name of the mission
        """
        if self.current_mission is not None:
            self.current_mission.set_mission_name(name)

    def set_current_mission_filename(self, name):
        """
        Set the mission paths to the new mission name specified
        :param name: filename of the mission
        """
        abs_folder_path = os.path.dirname(self.get_current_mission_absolute_path())
        abs_path = abs_folder_path + "/" + name + ".xml"

        self.current_mission.set_mission_abs_path(abs_path)

    def save_mission(self):
        """Save mission"""
        try:
            # check if missions folder is exisiting:
            if not os.path.exists(os.path.dirname(self.proj.fileName()) + '/missions/'):
                os.mkdir(os.path.dirname(self.proj.fileName()) + '/missions/')
        except Exception as e:
            logger.exception(e)

        if not self.current_mission.show_saving_warnings():
            return False

        current_name = self.get_current_mission_name()
        self.set_current_mission_filename(current_name)
        saved = self.current_mission.save_mission()
        if saved:
            layer = self.get_current_mission_layer()
            layer.setAbstract(self.get_current_mission_absolute_path())
        return saved

    def saveas_mission(self):
        """Save mission as new name"""
        previous_name = self.get_current_mission_absolute_path()
        previous_row = self.view.currentIndex().row()

        try:
            # check if missions folder is existing:
            if not os.path.exists(os.path.dirname(self.proj.fileName()) + '/missions/'):
                os.mkdir(os.path.dirname(self.proj.fileName()) + '/missions/')
        except Exception as e:
            logger.exception(e)

        if not self.current_mission.show_saving_warnings():
            return False, ""

        new_abs_path, selected_filter = QFileDialog.getSaveFileName(None, 'Save mission',
                                                                    self.get_current_mission_absolute_path(),
                                                                    'XML (*.xml)')
        if new_abs_path == "":
            return False, new_abs_path

        # Set absolute paths
        self.current_mission.set_mission_abs_path(new_abs_path)

        # Set new name of the mission
        new_name = os.path.splitext(os.path.basename(new_abs_path))[0]
        self.set_current_mission_name(new_name)
        self.view.currentLayer().setName(new_name)

        previous_saved = self.current_mission.is_saved()
        saved = self.current_mission.save_mission()
        if not previous_saved and saved:
            # update current mission and no add a new one
            return True, new_abs_path
        # load previous mission
        if saved:
            root = self.proj.layerTreeRoot()
            tree_layer = root.findLayer(self.get_current_mission_layer())
            layer_parent = tree_layer.parent() if tree_layer else root
            if layer_parent is root:
                previous_row = previous_row + 1
            self.load_mission(previous_name)
            previous_mission_layer = self.get_current_mission_layer()
            mission_tree_layer = root.findLayer(previous_mission_layer)

            layer_cloned = mission_tree_layer.clone()
            layer_parent.insertChildNode(previous_row, layer_cloned)
            root.removeChildNode(mission_tree_layer)
        return saved, new_abs_path

    def edit_wp_mission(self):
        """ open a waypoint edit widget"""
        self.editing_mission = self.current_mission

        self.wpeditw = WaypointEditWidget(self.config,
                                          self.editing_mission,
                                          self.vehicle_info.get_vehicle_namespace())
        self.wp_dock.setWidget(self.wpeditw)

        self.missioninfo = MissionInfo(self.config,
                                       self.canvas,
                                       self.editing_mission,
                                       self.vehicle_info.get_vehicle_namespace())
        self.minfo_dock.setWidget(self.missioninfo)
        self.missioninfo.mission_table_dialog.select_waypoint_signal.connect(self.wpeditw.selected_waypoint)

        self.tool_edit_wp_mission = EditTool(self.editing_mission, self.canvas, self.msglog)
        self.canvas.setMapTool(self.tool_edit_wp_mission)

        self.tool_edit_wp_mission.wp_clicked.connect(self.wp_clicked)
        self.wpeditw.control_state_signal.connect(self.tool_edit_wp_mission.set_control_state)
        self.wp_dock.show()
        self.minfo_dock.show()
        self.canvas.setFocus()

    def select_features_mission(self):
        """ open a waypoint edit widget with multiple_edition."""
        self.editing_mission = self.current_mission
        self.wpeditw = WaypointEditWidget(self.config,
                                          self.editing_mission,
                                          self.vehicle_info.get_vehicle_namespace(),
                                          multiple_edition=True)
        self.wp_dock.setWidget(self.wpeditw)
        self.wpeditw.show_empty_mission_step()

        self.missioninfo = MissionInfo(self.config,
                                       self.canvas,
                                       self.editing_mission,
                                       self.vehicle_info.get_vehicle_namespace())
        self.minfo_dock.setWidget(self.missioninfo)

        self.selectfeattool = selectfeaturestool.SelectFeaturesTool(self.editing_mission, self.canvas)
        self.canvas.setMapTool(self.selectfeattool)

        self.selectfeattool.selection_clicked.connect(self.selection_clicked)
        self.missioninfo.mission_table_dialog.updated_wp_signal.connect(self.wpeditw.update_show_multiple_features)
        self.wp_dock.show()
        self.minfo_dock.show()
        self.canvas.setFocus()

    def add_template_mission(self):
        """ Open a insert template widget"""
        self.editing_mission = self.current_mission
        self.template_insertion_widget = InsertTemplateWidget(self.config,
                                                              self.vehicle_info,
                                                              self.canvas,
                                                              self.msglog,
                                                              self.view,
                                                              self.editing_mission)
        self.template_dock.setWidget(self.template_insertion_widget)
        self.template_insertion_widget.save_tracks.connect(self.close_template_editing)

        self.missioninfo = MissionInfo(self.config,
                                       self.canvas,
                                       self.editing_mission,
                                       self.vehicle_info.get_vehicle_namespace())
        self.missioninfo.set_current_mission(self.template_insertion_widget.preview_mission)
        self.template_insertion_widget.preview_mission_signal.connect(self.missioninfo.update_values)
        self.minfo_dock.setWidget(self.missioninfo)

        self.template_dock.show()
        self.minfo_dock.show()

    def save_template_tracks_to_mission(self):
        """ Merge the template tracks to mission"""
        self.template_insertion_widget.merge_template_mission()

    def close_template_editing(self):
        """ Close template editing"""
        if self.template_insertion_widget is not None:
            self.template_insertion_widget.close()
        self.template_dock.hide()
        self.missioninfo.mission_table_dialog.hide()
        self.minfo_dock.hide()
        self.template_closed.emit()
        self.template_insertion_widget = None
        self.editing_mission = None

    def is_template_modified(self):
        """
        :return: return true if template is modified, otherwise False
        """
        return self.template_insertion_widget.is_template_modified()

    def move_mission(self):
        """ Set MapTool MoveFeatureTool"""
        logger.debug("Setting move tool")
        self.editing_mission = self.current_mission
        self.tool_move_feature = MoveFeatureTool(self.editing_mission, self.canvas, self.msglog)
        self.canvas.setMapTool(self.tool_move_feature)
        self.canvas.setFocus()

    def finish_move_mission(self):
        """ Finish move mission tool """
        if self.tool_move_feature is not None:
            self.tool_move_feature = None
            self.editing_mission = None
            self.finish_mission_action_signal.emit()

    def finish_edit_wp_mission(self):
        """ Finish edit waypoint mission"""
        if self.tool_edit_wp_mission is not None:
            self.tool_edit_wp_mission.close_band()
        self.wp_dock.hide()
        self.missioninfo.mission_table_dialog.hide()
        self.minfo_dock.hide()
        self.canvas.unsetMapTool(self.tool_edit_wp_mission)
        del self.tool_edit_wp_mission
        self.tool_edit_wp_mission = None
        self.editing_mission = None
        self.finish_mission_action_signal.emit()

    def finish_select_features_mission(self):
        """ finish  multiple edition """
        if self.selectfeattool is not None:
            self.selectfeattool.close_band()
        self.wp_dock.hide()
        self.missioninfo.mission_table_dialog.hide()
        self.minfo_dock.hide()
        self.canvas.unsetMapTool(self.selectfeattool)
        del self.selectfeattool
        self.selectfeattool = None
        self.editing_mission = None
        self.finish_mission_action_signal.emit()

    def wp_clicked(self, wp):
        """
        Slot to handle when a mission step has been clicked from the map tool
        """
        logger.debug(f"I clicked a waypoint {wp}")
        self.wpeditw.show_mission_step(wp)

    def selection_clicked(self, wp_list):
        """
        Slot to handle when a select features mission has been clicked from the map tool
        """

        self.wpeditw.show_multiple_features(wp_list)

    def send_mission_to_auv(self):
        """ Send the mission to vehicle"""

        # save mission before transfer and execute
        self.save_mission()

        mission_file = self.get_current_mission_absolute_path()
        name = os.path.basename(mission_file)
        if name.rfind('\"') != -1:
            name = name.replace("\"", "\\\"")
        if name.rfind('`') != -1:
            name = name.replace("`", "\\`")
        user = self.vehicle_info.get_vehicle_user()
        server = self.vehicle_info.get_vehicle_ip()
        vehicle_package = self.vehicle_info.get_remote_vehicle_package()
        remotepath = vehicle_package + "/missions"

        # make link to last copied mission
        # name_link = remotepath + '/default_mission.xml'
        # cmd = f"ln -sf {name} {name_link}"
        try:
            subprocess.run(["scp", "-B", mission_file, f"{user}@{server}:{remotepath}"],
                           check=True,
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE)
            logger.info(f"Copied mission file. {mission_file}")

            # subprocess.run(["ssh", "-o", "BatchMode=yes", f"{user}@{server}", cmd],
            #                check=True,
            #                stdout=subprocess.PIPE,
            #                stderr=subprocess.PIPE)
            # logger.info("Linked mission file.")

        except subprocess.CalledProcessError as e:
            logger.error(f"Mission upload error: {e.stderr}")
            QMessageBox.critical(None,
                                 "Mission upload",
                                 f"Mission upload error: {e.stderr}",
                                 QMessageBox.Close)
