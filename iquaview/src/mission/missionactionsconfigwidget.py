# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.


"""
 Class to read and store the xml structure associated to the mission_actions tag in the AUV config file
"""
import os
import logging

from PyQt5.QtWidgets import QMessageBox
from lxml import etree
from qgis.gui import QgsCollapsibleGroupBox

from iquaview.src.mission.actionwidget import ActionWidget
from iquaview.src.mission.missionedition.definitions import ActionDefinition
from iquaview.src.ui.ui_collapsible_groupbox import Ui_mCollapsibleGroupBox
from iquaview_lib.xmlconfighandler.missionactionshandler import MissionActionsHandler
from iquaview_lib.xmlconfighandler.xmlconfigparser import XMLConfigParser

logger = logging.getLogger(__name__)


class MissionActionsConfigWidget(QgsCollapsibleGroupBox, Ui_mCollapsibleGroupBox):
    def __init__(self, config, parent=None):
        """
        Constructor
        :param config: configuration
        """
        super().__init__(parent)
        self.setupUi(self)
        self.setTitle("Mission Actions")
        self.add_pushButton.setText("New Mission Action")
        self.config = config
        self.filename = None
        self.action_list = []
        self.action_key_count = 0

        self.add_pushButton.clicked.connect(lambda: self.new_mission_action())

        # load ros params from xml file
        self.set_config_filename(os.path.abspath(os.path.expanduser(self.config.settings['last_auv_config_xml'])))

    def read_xml(self):
        """

        read the last auv configuration xml loaded, and save the mission parameters
        """
        try:
            config_filename = os.path.abspath(os.path.expanduser(self.config.settings['last_auv_config_xml']))
            ma_handler = MissionActionsHandler(config_filename)
            actions = ma_handler.get_actions()
            self.action_list = []
            # self.comboBox.clear()
            for action in actions:
                act = ActionDefinition()
                logger.debug(action.tag)
                for value in action:
                    if value.tag == 'action_name':
                        logger.debug("     {} {}".format(value.tag, value.text))
                        act.action_name = value.text
                    elif value.tag == 'action_id':
                        logger.debug("     {} {}".format(value.tag, value.text))
                        act.action_id = value.text
                    elif value.tag == 'action_description':
                        logger.debug("     {} {}".format(value.tag, value.text))
                        act.action_description = value.text
                self.add_action(act)

        except OSError as e:
            logger.error("OSError: {}".format(e))

    def set_config_filename(self, filename):
        """
        Set config
        :param filename: config filename
        :type filename: str
        """
        self.filename = filename
        self.load_mission_actions()

    def load_mission_actions(self):
        """
        Remove previous widgets, read xml and load mission actions
        """

        # remove previous widgets
        self.delete_all(self.vboxlayout.layout())

        self.read_xml()

        actions = self.get_actions()
        for action in actions:
            action_widget = self.add_action_widget(action.action_name,
                                                   action.action_id,
                                                   action.identifier,
                                                   action.action_description,
                                                   True)

    def new_action(self, name):
        """
        Create new Action and add it to list
        :param name: action name
        :type name: str
        :return: Action
        """
        action = ActionDefinition()
        action.action_name = name
        action.identifier = self.action_key_count

        self.action_list.append(action)
        self.action_key_count += 1

        return action

    def add_action(self, action):
        """
        add ActionDefinition to ActionDefinition list
        :param action: ActionDefinition to add
        :type action: ActionDefinition
        """
        action.identifier = self.action_key_count

        self.action_list.append(action)
        self.action_key_count += 1

    def get_actions(self):
        """
        :return: action list
        :rtype: list
        """
        return self.action_list

    def get_action_by_id(self, id):
        """
        Get action by id
        :param id: action id
        :return: if found return action, otherwise return None
        """
        for action in self.action_list:
            if action.identifier == id:
                return action

        return None

    def remove_action_by_id(self, identifier):
        """
        Remove action by id
        :param identifier: id of the action to remove
        :return: return True if removed, otherwise False
        """
        for action in self.action_list:
            if action.identifier == identifier:
                self.action_list.remove(action)
                return True

        return False

    def new_mission_action(self, name="Action"):
        """
        Create new mission action
        :param name: name of the action
        :type name: str
        """

        action = self.new_action(name)
        self.add_action_widget(name, action.action_id, action.identifier, action.action_description)

    def add_action_widget(self, name="Action", action_id=None, id=None, description=None, collapsed=False):
        """
        Add action to layout
        :param name: name of the action
        :type name: str
        :param action_id: action id of the action
        :type action_id: str
        :param id: id to identify action
        :type id: int
        :param description: description of the action
        :param description: str
        :param collapsed: collapsed state
        :type collapsed: bool
        :return: return action widget
        :rtype: ActionWidget
        """

        action_widget = ActionWidget(name, action_id, id, description, collapsed)
        self.vboxlayout.layout().addWidget(action_widget)

        action_widget.remove_toolButton.clicked.connect(lambda state, x=action_widget: self.remove_mission_action(x))
        action_widget.description_lineEdit.textChanged.connect(lambda state, x=action_widget: self.update_action(x))
        action_widget.action_name_lineEdit.textChanged.connect(lambda state, x=action_widget: self.update_action(x))
        action_widget.action_id_lineEdit.textChanged.connect(lambda state, x=action_widget: self.update_action(x))

        action_widget.description_lineEdit.setFocus()

        return action_widget

    def remove_mission_action(self, action_widget):
        """
        Remove mission action
        :param action_widget: action to remove:
        :param action_widget: ActionWidget
        """
        confirmation_msg = "Are you sure you want to remove {} action?".format(action_widget.title())

        reply = QMessageBox.question(self, 'Removal confirmation',
                                     confirmation_msg, QMessageBox.Yes | QMessageBox.No, defaultButton=QMessageBox.Yes)
        if reply == QMessageBox.Yes:
            removed = self.remove_action_by_id(int(action_widget.objectName()))
            self.delete_all(action_widget.layout())
            action_widget.deleteLater()

    def update_action(self, widget):
        """
        Update action from widget
        :param widget: ActionWidget with the information
        :type widget: ActionWidget
        """
        action = self.get_action_by_id(int(widget.objectName()))
        action.action_description = widget.description_lineEdit.text()
        action.action_id = widget.action_id_lineEdit.text()
        action.action_name = widget.action_name_lineEdit.text()

    def save(self):
        """
        save the mission actions inside the auv configuration xml
        """

        config_parser = XMLConfigParser(self.filename)
        # get ros_params
        mission_actions = config_parser.first_match(config_parser.root, "mission_actions")
        # all actions in mission_actions
        mission_actions.clear()

        for action in self.action_list:
            xml_action = etree.SubElement(mission_actions, "action")

            xml_action_name = etree.SubElement(xml_action, "action_name")
            xml_action_id = etree.SubElement(xml_action, "action_id")
            xml_action_description = etree.SubElement(xml_action, "action_description")

            xml_action_name.text = action.action_name
            xml_action_description.text = action.action_description
            xml_action_id.text = action.action_id

        config_parser.write()

    def delete_all(self, layout):
        """
        delete all widget from layout
        :param layout: layout is a qt layout
        """
        if layout is not None:
            for i in reversed(range(layout.count())):
                item = layout.takeAt(i)
                widget = item.widget()
                if widget is not None:
                    widget.deleteLater()
                else:
                    self.delete_all(item.layout())

    def is_valid(self):
        """
        Check if the mission actions are valid
        :return: True if the mission actions are valid
        :rtype: bool
        """
        for action in self.action_list:
            if not action.is_valid():
                return False
        return True
