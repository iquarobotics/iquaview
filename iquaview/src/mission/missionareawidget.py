# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
"""
 Widget to configure heave mode
"""

from PyQt5.QtWidgets import QWidget
from PyQt5.QtCore import pyqtSignal

from iquaview.src.ui.ui_mission_area_widget import Ui_MissionAreaWidget


class MissionAreaWidget(QWidget, Ui_MissionAreaWidget):
    draw_rectangle_unchecked_signal = pyqtSignal()
    center_on_target_unchecked_signal = pyqtSignal()

    def __init__(self, parent=None):
        """
        Init of the object MissionAreaWidget
        :param parent: Parent widget
        """
        super().__init__(parent)
        self.setupUi(self)

        self.alongTrackLabel.setEnabled(False)
        self.acrossTrackLabel.setEnabled(False)
        self.alongTLength.setEnabled(False)
        self.acrossTLength.setEnabled(False)
        self.fixed_extend_widget.hide()

        self.fixedExtent.toggled.connect(self.fixed_extend_toggled)

        self.centerOnTargetButton.clicked.connect(self.on_button_clicked)
        self.drawRectangleButton.clicked.connect(self.on_button_clicked)

        self.automaticExtent.toggled.connect(self.uncheck_draw_rectangle_button)
        self.automaticExtent.toggled.connect(self.uncheck_center_on_target_button)

        self.fixedExtent.toggled.connect(self.uncheck_draw_rectangle_button)
        self.fixedExtent.toggled.connect(self.uncheck_center_on_target_button)

    def on_button_clicked(self):
        """
        Slot method for handling button clicks
        Emits signals based on the clicked button
        """
        sender = self.sender()
        if sender == self.centerOnTargetButton:
            if not self.centerOnTargetButton.isChecked():
                self.center_on_target_unchecked_signal.emit()
            else:
                self.uncheck_draw_rectangle_button()
        if sender == self.drawRectangleButton:
            if not self.drawRectangleButton.isChecked():
                self.draw_rectangle_unchecked_signal.emit()
            else:
                self.uncheck_center_on_target_button()

    def uncheck_draw_rectangle_button(self):
        """
        Unchecks the "Draw Rectangle" button and emits the corresponding signal
        """
        self.drawRectangleButton.setChecked(False)
        self.draw_rectangle_unchecked_signal.emit()

    def uncheck_center_on_target_button(self):
        """
        Unchecks the "Center on Target" button and emits the corresponding signal
        """
        self.centerOnTargetButton.setChecked(False)
        self.center_on_target_unchecked_signal.emit()

    def fixed_extend_toggled(self):
        """
        Slot method for handling the toggled event of the "Fixed Extent" checkbox
        Enables or disables certain widgets based on the state of the checkbox
        """
        is_checked = self.fixedExtent.isChecked()
        self.alongTrackLabel.setEnabled(is_checked)
        self.acrossTrackLabel.setEnabled(is_checked)
        self.alongTLength.setEnabled(is_checked)
        self.acrossTLength.setEnabled(is_checked)
