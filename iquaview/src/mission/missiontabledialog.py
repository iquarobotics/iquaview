# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Widget to show information about waypoints of the mission
"""

import math
from math import degrees, radians

from PyQt5.QtCore import Qt, pyqtSignal, QPoint
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import (QMenu,
                             QDialog,
                             QHeaderView,
                             QTableWidgetItem,
                             QAction,
                             QFileDialog,
                             QVBoxLayout,
                             QDialogButtonBox)

from iquaview.src.mission.missionedition import (latitudewidget,
                                                 longitudewidget,
                                                 heavemodewidget,
                                                 noaltitudewidget,
                                                 parkwidget,
                                                 speedwidget,
                                                 missionactionswidget)
from iquaview.src.mission.missionedition.parkwidget import PARK_DISABLED, PARK_ANCHOR, PARK_HOLONOMIC
from iquaview.src.mission.missiontrack import MissionTrack
from iquaview.src.ui.ui_mission_table_dialog import Ui_MissionTableDialog
from iquaview_lib.cola2api.mission_types import (MissionStep,
                                                 MissionGoto,
                                                 MissionPark,
                                                 MissionSection,
                                                 GOTO_MANEUVER,
                                                 SECTION_MANEUVER,
                                                 PARK_MANEUVER,
                                                 HEAVE_MODE_DEPTH,
                                                 HEAVE_MODE_BOTH,
                                                 HEAVE_MODE_ALTITUDE)
from iquaview_lib.config import Config
from iquaview_lib.utils.coordinateconverter import format_lat_lon


LATITUDE_COLUMN = 0
LONGITUDE_COLUMN = 1
HEAVE_MODE_COLUMN = 2
DEPTH_COLUMN = 3
ALTITUDE_COLUMN = 4
NO_ALTITUDE_COLUMN = 5
PARK_COLUMN = 6
PARK_TIME_COLUMN = 7
PARK_YAW_COLUMN = 8
TOLERANCE_COLUMN = 9
SPEED_COLUMN = 10
ACTIONS_COLUMN = 11


class MissionTableDialog(QDialog, Ui_MissionTableDialog):
    """ Widget to show information about waypoints of the mission """
    updated_wp_signal = pyqtSignal()
    select_waypoint_signal = pyqtSignal(int)

    def __init__(self, config: Config, mission_track: MissionTrack, vehicle_namespace: str, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.config = config
        self.mission_track = mission_track
        self.vehicle_namespace = vehicle_namespace

        self.setWindowFlags(Qt.CustomizeWindowHint |
                            Qt.Window | Qt.WindowMinimizeButtonHint |
                            Qt.WindowMaximizeButtonHint |
                            Qt.WindowCloseButtonHint)

        self.save_table_action = QAction(QIcon(":/resources/export_file.svg"), "Export as csv", self)
        self.edit_action = QAction(QIcon(":/resources/mActionEditCell.svg"), "Edit", self)
        self.remove_wp_action = QAction(QIcon(":/resources/mActionDeleteSelected.svg"), "Remove Waypoint", self)

        self.toolbar.addAction(self.save_table_action)

        self.save_table_action.triggered.connect(self.save)

        # for every column and row resize to contents
        for i in range(self.tableWidget.columnCount()):
            self.tableWidget.horizontalHeader().setSectionResizeMode(i, QHeaderView.ResizeToContents)

        for i in range(self.tableWidget.rowCount()):
            self.tableWidget.verticalHeader().setSectionResizeMode(i, QHeaderView.ResizeToContents)

        self.tableWidget.cellDoubleClicked.connect(self.cell_clicked)
        self.tableWidget.setContextMenuPolicy(Qt.CustomContextMenu)
        self.tableWidget.customContextMenuRequested.connect(self.show_context_menu)
        self.edit_action.triggered.connect(self.edit_cell)
        self.remove_wp_action.triggered.connect(self.remove_waypoint)
        self.reload()

    def set_item_flags_text(self, item, flags, text):
        """
        Set the flags and text for a specific item.

        :param item: The QTableWidgetItem object to set the flags and text for.
        :type item: QTableWidgetItem
        :param flags: The flags to be set for the item.
        :type flags: Qt.ItemFlags
        :param text: The text to be set for the item.
        :type text: str
        """

        item.setFlags(flags)
        item.setText(text)

    def reload(self):
        """Reload the table"""
        self.tableWidget.setRowCount(0)
        mission_length = self.mission_track.get_mission_length()

        for i in range(mission_length):
            step = self.mission_track.get_step(i)
            self.tableWidget.insertRow(i)

            coordinate_format = self.config.settings.get("coordinate_format", "degree")
            coordinate_precision = self.config.settings.get("coordinate_precision", 7)
            coordinate_directional_suffix = self.config.settings.get("coordinate_directional_suffix", True)
            coordinate_symbols = self.config.settings.get("coordinate_symbols", True)
            [lat_str, lon_str] = format_lat_lon(step.get_maneuver().final_latitude,
                                                step.get_maneuver().final_longitude,
                                                coordinate_format,
                                                coordinate_precision,
                                                coordinate_directional_suffix,
                                                coordinate_symbols)

            item = QTableWidgetItem()
            self.set_item_flags_text(item, Qt.ItemIsEnabled, lat_str)
            item.setData(Qt.UserRole, step)
            self.tableWidget.setItem(i, LATITUDE_COLUMN, item)

            item = QTableWidgetItem()
            self.set_item_flags_text(item, Qt.ItemIsEnabled, lon_str)
            self.tableWidget.setItem(i, LONGITUDE_COLUMN, item)

            heave_mode = step.get_maneuver().heave_mode
            final_depth = step.get_maneuver().final_depth
            final_altitude = step.get_maneuver().final_altitude

            if heave_mode == HEAVE_MODE_DEPTH:
                item = QTableWidgetItem()
                self.set_item_flags_text(item, Qt.ItemIsEnabled, "DEPTH")
                self.tableWidget.setItem(i, HEAVE_MODE_COLUMN, item)

                item = QTableWidgetItem()
                self.set_item_flags_text(item, Qt.ItemIsEnabled, str(final_depth))
                self.tableWidget.setItem(i, DEPTH_COLUMN, item)

                item = QTableWidgetItem()
                self.set_item_flags_text(item, Qt.ItemIsEnabled, "")
                self.tableWidget.setItem(i, ALTITUDE_COLUMN, item)

            elif heave_mode == HEAVE_MODE_ALTITUDE:
                item = QTableWidgetItem()
                self.set_item_flags_text(item, Qt.ItemIsEnabled, "ALTITUDE")
                self.tableWidget.setItem(i, HEAVE_MODE_COLUMN, item)

                item = QTableWidgetItem()
                self.set_item_flags_text(item, Qt.ItemIsEnabled, "")
                self.tableWidget.setItem(i, DEPTH_COLUMN, item)

                item = QTableWidgetItem()
                self.set_item_flags_text(item, Qt.ItemIsEnabled, str(final_altitude))
                self.tableWidget.setItem(i, ALTITUDE_COLUMN, item)

            elif heave_mode == HEAVE_MODE_BOTH:
                item = QTableWidgetItem()
                self.set_item_flags_text(item, Qt.ItemIsEnabled, "BOTH")
                self.tableWidget.setItem(i, HEAVE_MODE_COLUMN, item)

                item = QTableWidgetItem()
                self.set_item_flags_text(item, Qt.ItemIsEnabled, str(final_depth))
                self.tableWidget.setItem(i, DEPTH_COLUMN, item)

                item = QTableWidgetItem()
                self.set_item_flags_text(item, Qt.ItemIsEnabled, str(final_altitude))
                self.tableWidget.setItem(i, ALTITUDE_COLUMN, item)

            item = QTableWidgetItem()
            self.set_item_flags_text(item, Qt.ItemIsEnabled,
                                "GO UP" if step.get_maneuver().no_altitude_goes_up else "IGNORE")
            self.tableWidget.setItem(i, NO_ALTITUDE_COLUMN, item)

            maneuver_type = step.get_maneuver().get_maneuver_type()

            if maneuver_type in [GOTO_MANEUVER, SECTION_MANEUVER]:
                item = QTableWidgetItem()
                self.set_item_flags_text(item, Qt.ItemIsEnabled, "DISABLED")
                self.tableWidget.setItem(i, PARK_COLUMN, item)

                item = QTableWidgetItem()
                self.set_item_flags_text(item, Qt.ItemIsEnabled, "")
                self.tableWidget.setItem(i, PARK_TIME_COLUMN, item)

                item = QTableWidgetItem()
                self.set_item_flags_text(item, Qt.ItemIsEnabled, "")
                self.tableWidget.setItem(i, PARK_YAW_COLUMN, item)

                item = QTableWidgetItem()
                self.set_item_flags_text(item, Qt.ItemIsEnabled, str(step.get_maneuver().tolerance_xy))
                self.tableWidget.setItem(i, TOLERANCE_COLUMN, item)

            elif maneuver_type == PARK_MANEUVER:
                item = QTableWidgetItem()
                use_yaw = step.get_maneuver().use_yaw
                self.set_item_flags_text(item, Qt.ItemIsEnabled, "Holonomic" if use_yaw else "Anchor")
                self.tableWidget.setItem(i, PARK_COLUMN, item)

                item = QTableWidgetItem()
                self.set_item_flags_text(item, Qt.ItemIsEnabled, str(step.get_maneuver().time))
                self.tableWidget.setItem(i, PARK_TIME_COLUMN, item)

                item = QTableWidgetItem()
                self.set_item_flags_text(item, Qt.ItemIsEnabled,
                                    str(math.degrees(step.get_maneuver().final_yaw)) if use_yaw else "")
                self.tableWidget.setItem(i, PARK_YAW_COLUMN, item)

                item = QTableWidgetItem()
                self.set_item_flags_text(item, Qt.ItemIsEnabled, "")
                self.tableWidget.setItem(i, TOLERANCE_COLUMN, item)

            item = QTableWidgetItem()
            self.set_item_flags_text(item, Qt.ItemIsEnabled, str(step.get_maneuver().surge_velocity))
            self.tableWidget.setItem(i, SPEED_COLUMN, item)

            item = QTableWidgetItem()
            item.setFlags(Qt.ItemIsEnabled)  # only read
            actions = step.get_actions()
            str_actions = ""
            for act in actions:
                str_actions += act.get_action_name() + "\n"
            item.setText(str_actions)
            self.tableWidget.setItem(i, ACTIONS_COLUMN, item)

        for column in range(self.tableWidget.columnCount()):
            self.tableWidget.horizontalHeader().setSectionResizeMode(column, QHeaderView.ResizeToContents)

        for row in range(self.tableWidget.rowCount()):
            self.tableWidget.verticalHeader().setSectionResizeMode(row, QHeaderView.ResizeToContents)

    def cell_clicked(self, row, column):
        """
        when a cell is clicked, open the cell editor
        :param row: row of table
        :type row: int
        :param column: column of table
        :type column: int
        """
        self.edit_cell()

    def edit_cell(self):
        row = self.tableWidget.currentRow()
        column = self.tableWidget.currentColumn()
        self.select_waypoint_signal.emit(row)
        self.tableWidget.setCurrentCell(row, column)
        dialog = QDialog()
        vertical_layout = QVBoxLayout(dialog)
        new_mission_actions = []
        item = self.tableWidget.item(row, column)
        step_data = self.tableWidget.item(row, 0).data(Qt.UserRole)

        headercolumn = self.tableWidget.horizontalHeaderItem(column)
        dialog.setWindowTitle(headercolumn.text())
        dialog.setLayout(vertical_layout)

        if column == LATITUDE_COLUMN:
            latitude_widget = latitudewidget.LatitudeWidget()
            latitude_widget.config = self.config
            latitude_widget.set_latitude(step_data.get_maneuver().final_latitude)
            vertical_layout.addWidget(latitude_widget)

        elif column == LONGITUDE_COLUMN:
            longitude_widget = longitudewidget.LongitudeWidget()
            longitude_widget.config = self.config
            longitude_widget.set_longitude(step_data.get_maneuver().final_longitude)
            vertical_layout.addWidget(longitude_widget)

        elif column in (HEAVE_MODE_COLUMN, DEPTH_COLUMN, ALTITUDE_COLUMN):
            heave_mode_widget = heavemodewidget.HeaveModeWidget()
            current_heave_mode = step_data.get_maneuver().heave_mode
            heave_mode_widget.heave_mode_comboBox.setCurrentIndex(current_heave_mode)
            heave_mode_widget.on_heave_mode_box_changed(current_heave_mode)
            heave_mode_widget.depth_doubleSpinBox.setValue(step_data.get_maneuver().final_depth)
            heave_mode_widget.altitude_doubleSpinBox.setValue(step_data.get_maneuver().final_altitude)

            vertical_layout.addWidget(heave_mode_widget)

        elif column == NO_ALTITUDE_COLUMN:
            no_altitude_widget = noaltitudewidget.NoAltitudeWidget()
            if step_data.get_maneuver().no_altitude_goes_up:
                no_altitude_widget.no_altitude_comboBox.setCurrentIndex(0)
                no_altitude_widget.on_no_altitude_box_changed(0)
            else:
                no_altitude_widget.no_altitude_comboBox.setCurrentIndex(1)
                no_altitude_widget.on_no_altitude_box_changed(1)

            vertical_layout.addWidget(no_altitude_widget)
        elif column in (PARK_COLUMN, PARK_TIME_COLUMN, PARK_YAW_COLUMN, TOLERANCE_COLUMN):
            park_widget = parkwidget.ParkWidget()
            park_widget.on_park_state_box_changed(0)
            if step_data.get_maneuver().get_maneuver_type() == PARK_MANEUVER:
                if step_data.get_maneuver().use_yaw:
                    park_widget.park_state_comboBox.setCurrentIndex(PARK_HOLONOMIC)
                    park_widget.park_yaw_doubleSpinBox.setValue(degrees(step_data.get_maneuver().final_yaw))
                    park_widget.parktime_doubleSpinBox.setValue(step_data.get_maneuver().time)

                else:
                    park_widget.park_state_comboBox.setCurrentIndex(PARK_ANCHOR)
                    park_widget.parktime_doubleSpinBox.setValue(step_data.get_maneuver().time)

            else:
                park_widget.park_state_comboBox.setCurrentIndex(PARK_DISABLED)
                park_widget.tolerance_widget.tolerance_xy_doubleSpinBox.setValue(step_data.get_maneuver().tolerance_xy)

            vertical_layout.addWidget(park_widget)

        elif column == SPEED_COLUMN:
            speed_widget = speedwidget.SpeedWidget()
            speed_widget.speed_doubleSpinBox.setValue(step_data.get_maneuver().surge_velocity)

            vertical_layout.addWidget(speed_widget)

        elif column == ACTIONS_COLUMN:
            actions_widget = missionactionswidget.MissionActionsWidget(self, self.config, self.vehicle_namespace)
            actions_widget.set_actions(step_data.get_actions())
            vertical_layout.addWidget(actions_widget)

        button_box = QDialogButtonBox(dialog)
        button_box.setOrientation(Qt.Horizontal)
        button_box.setStandardButtons(QDialogButtonBox.Cancel | QDialogButtonBox.Ok)
        button_box.setObjectName("buttonBox")
        vertical_layout.addWidget(button_box)
        button_box.accepted.connect(dialog.accept)
        button_box.rejected.connect(dialog.reject)
        result = dialog.exec_()
        if result == QDialog.Accepted:
            # update step
            if (column == LATITUDE_COLUMN
                    and latitude_widget.is_acceptable()):
                latitude = float(latitude_widget.get_latitude())
            else:
                latitude = step_data.get_maneuver().final_latitude

            if (column == LONGITUDE_COLUMN
                    and longitude_widget.is_acceptable()):
                longitude = float(longitude_widget.get_longitude())
            else:
                longitude = step_data.get_maneuver().final_longitude

            if column in (HEAVE_MODE_COLUMN, DEPTH_COLUMN, ALTITUDE_COLUMN):

                heave_mode = heave_mode_widget.get_heave_mode()
                depth = heave_mode_widget.get_depth()
                altitude = heave_mode_widget.get_altitude()
            else:
                heave_mode = step_data.get_maneuver().heave_mode
                depth = step_data.get_maneuver().final_depth
                altitude = step_data.get_maneuver().final_altitude

            if column == NO_ALTITUDE_COLUMN:
                no_altitude_goes_up = no_altitude_widget.no_altitude_goes_up()
            else:
                no_altitude_goes_up = step_data.get_maneuver().no_altitude_goes_up

            if column in (PARK_COLUMN, PARK_TIME_COLUMN, PARK_YAW_COLUMN, TOLERANCE_COLUMN):
                if park_widget.park_state_comboBox.currentIndex() != PARK_DISABLED:
                    is_park_maneuver = PARK_MANEUVER
                    use_yaw = park_widget.park_state_comboBox.currentIndex() == PARK_HOLONOMIC
                    if not use_yaw:
                        yaw = 0

                    else:
                        yaw = radians(float(park_widget.park_yaw_doubleSpinBox.value()))

                    park_time = float(park_widget.parktime_doubleSpinBox.value())

                    tolerance = step_data.get_maneuver().tolerance_xy
                else:
                    is_park_maneuver = 0
                    tolerance = park_widget.tolerance_widget.tolerance_xy_doubleSpinBox.value()

            else:
                is_park_maneuver = step_data.get_maneuver().get_maneuver_type()
                if is_park_maneuver == PARK_MANEUVER:
                    yaw = step_data.get_maneuver().final_yaw
                    use_yaw = step_data.get_maneuver().use_yaw
                    park_time = step_data.get_maneuver().time

                tolerance = step_data.get_maneuver().tolerance_xy

            if column == SPEED_COLUMN and speed_widget.is_acceptable():
                speed = float(speed_widget.speed_doubleSpinBox.value())
            else:
                speed = step_data.get_maneuver().surge_velocity

            if column == ACTIONS_COLUMN:
                new_mission_actions = actions_widget.get_actions()
            else:
                new_mission_actions = step_data.get_actions()

            mission_step = MissionStep()
            if is_park_maneuver == PARK_MANEUVER:
                maneuver = MissionPark(latitude, longitude, depth, altitude,
                                       yaw, use_yaw,
                                       heave_mode, speed, park_time, no_altitude_goes_up)
            elif row == 0:
                maneuver = MissionGoto(latitude, longitude, depth, altitude, heave_mode, speed, tolerance,
                                       no_altitude_goes_up)

            else:
                # if previous is in the list, get z from list, otherwsie get z from track
                previous_latitude = self.mission_track.get_step(row - 1).get_maneuver().final_latitude
                previous_longitude = self.mission_track.get_step(row - 1).get_maneuver().final_longitude
                previous_depth = self.mission_track.get_step(row - 1).get_maneuver().final_depth

                maneuver = MissionSection(previous_latitude, previous_longitude, previous_depth,
                                          latitude, longitude, depth, altitude, heave_mode,
                                          speed, tolerance, no_altitude_goes_up)

            if ((row + 1) <= (self.mission_track.get_mission_length() - 1)
                    and self.mission_track.get_step(
                        row + 1).get_maneuver().get_maneuver_type() == SECTION_MANEUVER):
                self.mission_track.get_step(row + 1).get_maneuver().initial_depth = depth
                self.mission_track.get_step(row + 1).get_maneuver().initial_latitude = latitude
                self.mission_track.get_step(row + 1).get_maneuver().initial_longitude = longitude

            mission_step.add_maneuver(maneuver)
            mission_step.actions = new_mission_actions
            self.mission_track.update_step(row, mission_step)
            self.updated_wp_signal.emit()
            self.reload()

    def save(self):
        """ Save mission to file """
        filename, selected_filter = QFileDialog.getSaveFileName(None, 'Save waypoints table',
                                                                self.mission_track.get_mission_name() + ".csv",
                                                                'CSV (*.csv)')

        if filename:
            if not filename.endswith(".csv"):
                filename += ".csv"

            with open(filename, 'w', encoding='utf8') as file:

                for column in range(self.tableWidget.columnCount()):
                    if column == (self.tableWidget.columnCount() - 1):
                        file.write(self.tableWidget.horizontalHeaderItem(column).text())
                    else:
                        file.write(self.tableWidget.horizontalHeaderItem(column).text() + ", ")
                file.write('\n')

                for row in range(self.tableWidget.rowCount()):
                    for column in range(self.tableWidget.columnCount()):
                        if column == (self.tableWidget.columnCount() - 1):
                            text = self.tableWidget.item(row, column).text().replace("\n", " ")
                            file.write(f"\"{text}\"")
                        else:
                            file.write(f"{self.tableWidget.item(row, column).text()}, ")

                    file.write('\n')

    def show_context_menu(self, pos: QPoint) -> None:
        """
        Only callable by signals of QTableWidget. Creates a context menu for the elements selected in the QTableWidget
        :param pos: position to create context menu from the tableView perspective
        :type pos: QPoint

        """
        menu = QMenu()
        index = self.tableWidget.indexAt(pos)
        if index.isValid():
            # get column name and set edit action text
            item = self.tableWidget.itemAt(pos)
            text = self.tableWidget.horizontalHeaderItem(item.column()).text()
            self.tableWidget.setCurrentItem(item)
            self.edit_action.setText(f"Edit {text}")
            # set remove wp action text
            self.remove_wp_action.setText(f"Remove Waypoint {self.tableWidget.currentRow()+1}")

            # create menu
            menu.addAction(self.edit_action)
            menu.addSeparator()
            menu.addAction(self.remove_wp_action)

        action = menu.exec_(self.tableWidget.mapToGlobal(pos))

    def remove_waypoint(self):
        current_row = self.tableWidget.currentRow()
        if current_row >= 0:
            self.mission_track.remove_step(current_row)
