# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
This dialog lets the user see the missions found in the local machine and the AUV using SFTP connection.
It has buttons and context menus to allow transfer, set as current, rename, import to project
and delete the mission files.
"""

import logging
import subprocess
from os import path

from PyQt5.QtCore import pyqtSignal
from PyQt5.QtGui import QBrush, QPalette, QColor
from PyQt5.QtWidgets import QMessageBox, QPushButton

from iquaview_lib.connection.sftp.sftp_connection_dialog import SFTPConnectionDialog
from iquaview_lib.connection.sftp.sftp_help_dialog import SFTPHelpDialog

logger = logging.getLogger(__name__)


class SFTPMissionTransferDialog(SFTPConnectionDialog):
    """ Dialog to show the missions found in the local machine and the AUV using SFTP connection."""
    update_transfer_colors_signal = pyqtSignal()

    def __init__(self, user, ip, local_ini_path, remote_ini_path, project, mcontrol, parent=None):
        """
        Init of the object SFTPMissionTransferDialog
        :param user: User that will establish connection
        :type user: str
        :param ip: IP address of the remote machine
        :type ip: str
        :param local_ini_path: Initial path of the local machine
        :type local_ini_path: str
        :param remote_ini_path: Initial path of the remote machine
        :type remote_ini_path: str
        :param project: Project where the missions will be imported
        :type project: QgsProject
        :param mcontrol: Mission control to load the missions to the project
        :type mcontrol: MissionController
        """
        super().__init__(user, ip, local_ini_path, remote_ini_path,
                         file_extension=[".xml"], compare_with_crc32=True, parent=parent)
        self.mcontrol = mcontrol
        self.project = project
        self.remotepath = remote_ini_path

        # Fix remote pathing
        if remote_ini_path[:2] == "~/":
            self.remotepath = "/home/" + user + remote_ini_path[1:]
        elif remote_ini_path[:6] != "/home/":
            self.remotepath = "/home/" + user + "/" + remote_ini_path

        self.selected_remote_path = self.remotepath

        # adding current mission mark in legned
        self.current_mission_color = QColor(0, 200, 255, 128)
        p = self.current_mission_label.palette()
        p.setColor(QPalette.Background, self.current_mission_color)
        self.current_mission_label.setAutoFillBackground(True)
        self.current_mission_label.setPalette(p)
        self.current_mission_label.setText("Current mission")

        # Creating mission specific buttons
        self.import_to_project_btn = QPushButton(self)
        self.import_to_project_btn.setAutoDefault(False)
        self.import_to_project_btn.setObjectName("import_to_project_btn")
        self.import_to_project_btn.setText("Import to project")
        self.right_bottom_h_layout.insertWidget(self.right_bottom_h_layout.count() - 3, self.import_to_project_btn)

        self.set_as_current_btn = QPushButton(self)
        self.set_as_current_btn.setAutoDefault(False)
        self.set_as_current_btn.setObjectName("set_as_current_btn")
        self.set_as_current_btn.setText("Set as current mission")
        self.right_bottom_h_layout.insertWidget(self.right_bottom_h_layout.count() - 3, self.set_as_current_btn)

        # Connecting buttons
        self.set_as_current_btn.pressed.connect(self.set_selected_mission_as_current)
        self.import_to_project_btn.pressed.connect(self.import_selected_to_project)

        self.update_transfer_colors_signal.connect(self.update_diff_colors)

        self.update_transfer_colors_signal.emit()

    def select_mission(self, mission_name):
        """
        Select local mission with mission_name
        :param mission_name: name of the mission:
        :type mission_name: str
        """
        for row in range(self.local_table_model.rowCount()):
            index = self.local_table_model.index(row, 1)
            item = self.local_table_model.itemFromIndex(index)
            if item.text() == mission_name:
                # select item
                self.local_table_view.selectRow(row)

    def show_remote_selection(self, index):
        """
        Override method of the SFTPConnectionDialog Class. Adds a check to the selected path to disable
        set_as_current_btn in case a path other than the missions path is selected
        """
        super().show_remote_selection(index)

        # handle set_as_current_btn visibility
        if not self.initialising:
            if self.get_selected_remote_path() == self.remotepath:
                self.set_as_current_btn.setEnabled(True)
            else:
                self.set_as_current_btn.setEnabled(False)

    def set_selected_mission_as_current(self):
        """
        Creates a symbolic link in the remote path named default_mission.xml linked to the selected file
        from remote table view.
        """
        indexes = self.remote_table_view.selectedIndexes()

        # Check how many items were selected. Each row has 4 items
        if len(indexes) != 4:
            QMessageBox.warning(None, "Warning",
                                "Select one mission from the remote table to set as current mission")
            return
        item = self.remote_table_model.itemFromIndex(indexes[1])
        file_name = item.text()

        # check if selected item is a file and ends with .xml
        if file_name not in self.remote_file_list or file_name[-4:] != ".xml":
            QMessageBox.warning(None, "Warning",
                                "Selected item must be a mission file.")
            return

        link_name = self.get_selected_remote_path() + "/default_mission.xml"

        # if a link already exists, remove it
        if self.sftp_connection.lexists(link_name):
            self.sftp_connection.remove(link_name)

        # Create the symbolic link
        self.sftp_connection.symlink(file_name, link_name)

        self.update_transfer_colors_signal.emit()

        self.remote_table_view.clearSelection()

    def import_selected_to_project(self):
        """
        Imports selected items files to the current project if they are from the local table view.
        Transfers and imports selected files to the local machine and current project if they are from the
        remote table view.
        """
        if len(self.local_table_view.selectedIndexes()) > 0:
            self.import_local_selected_files_to_project()
        elif len(self.remote_table_view.selectedIndexes()) > 0:
            self.transfer_and_import_remote_to_local()

    def import_local_selected_files_to_project(self):
        """ Imports selected files from local table view to the current project """
        # get file names of the selected indexes
        indexes = self.local_table_view.selectedIndexes()
        name_list = []
        for i, index in enumerate(indexes):
            if i % 4 == 1:
                item = self.local_table_model.itemFromIndex(index)
                name_list.append(item.text())

        # check if selected all selected items are files that end with '.xml'
        for name in name_list:
            if name not in self.local_file_list or name[-4:] != ".xml":
                QMessageBox.warning(None, "Warning", "Selected items must be mission files.")
                return

        # import all files to the project
        if self.project.fileName() == "":
            QMessageBox.warning(
                None, "No opened project",
                "You do not have any open project to load the mission. \n"
                "Close the FTP dialog and open a project or save the current one first.")

        else:
            filenames = []
            for name in name_list:
                filenames.append(self.selected_local_path + "/" + name)
            try:
                for file in filenames:
                    self.mcontrol.load_mission(file)
            except Exception as e:
                logger.error("Some mission files could not be loaded. %s", e)
                QMessageBox.critical(
                    self, e.args[0],
                    "Some mission files could not be loaded, make sure they are actual mission files.",
                    QMessageBox.Close)
                return
            QMessageBox.information(None, "Import completed", "All files imported to the project successfully.")

    def transfer_and_import_remote_to_local(self):
        """
        First transfers and then imports selected files from remote table view to the local machine and current project
        """
        # get file names of the selected indexes
        indexes = self.remote_table_view.selectedIndexes()
        name_list = []
        for i in range(0, len(indexes)):
            if i % 4 == 1:
                item = self.remote_table_model.itemFromIndex(indexes[i])
                name_list.append(item.text())

        # check if selected all selected items are files that end with '.xml'
        for name in name_list:
            if name not in self.remote_file_list or name[-4:] != ".xml":
                QMessageBox.warning(None, "Warning", "Selected items must be mission files.")
                return

        # if transfer is successful, import the files to the project
        if self.transfer_remote_selected_files_to_local():
            # import all files to the project
            if self.project.fileName() == "":
                QMessageBox.warning(
                    None, "No opened project",
                    "You do not have any project open to load the mission. \n"
                    "Close the FTP dialog and open a project or save the current one.")
            else:
                filenames = []
                for name in name_list:
                    filenames.append(self.selected_local_path + "/" + name)
                try:
                    for file in filenames:
                        self.mcontrol.load_mission(file)
                except Exception as e:
                    logger.error("Some mission files could not be loaded. %s", e)
                    QMessageBox.critical(
                        self, e.args[0],
                        "Some mission files could not be loaded, make sure they are actual mission files.",
                        QMessageBox.Close)
                    return
                QMessageBox.information(None, "Import completed", "All files imported to the project successfully.")

    def transfer_local_selected_files_to_remote(self):
        """
        Override method of SFTPConnectionDialog
        Transfers files normally but if only one mission is transferred, it is set as current mission
        """

        indexes = self.local_table_view.selectedIndexes()
        super().transfer_local_selected_files_to_remote()

        # If only one mission was transferred, set it as current mission
        if len(indexes) == 4:  # Each row has 4 items
            item = self.local_table_model.itemFromIndex(indexes[1])
            file_name = item.text()
            link_name = self.get_selected_remote_path() + "/default_mission.xml"

            # Check file exists in remote and that it was indeed a mission file
            if file_name in self.remote_file_list and file_name[-4:] == ".xml":
                # if a link already exists, remove it
                if self.sftp_connection.lexists(link_name):
                    self.sftp_connection.remove(link_name)

                # Create the symbolic link
                self.sftp_connection.symlink(file_name, link_name)

                self.update_transfer_colors_signal.emit()

    def create_table_context_menu(self, pos):
        """
        Override method of SFTPConnectionDialog class.Adds mission specific actions to the context menu created.
        :param pos: position to create context menu from the tableView perspective
        :type pos: QPoint
        """
        menu = super().create_table_context_menu(pos)
        if menu is not None:
            if menu.parent() == self.local_table_view:
                menu.addAction("Import to project", self.import_local_selected_files_to_project)
            elif menu.parent() == self.remote_table_view:
                menu.addAction("Transfer and Import to project", self.transfer_and_import_remote_to_local)
                menu.addAction("Set as current mission", self.set_selected_mission_as_current)

    def update_diff_colors(self):
        """
        Override method of SFTPConnectionDialog class. This method will mark the file referred by the symbolic
        link "default_mission.xml" found in the remote folder painting the whole row.
        """
        super().update_diff_colors()
        default_mission = None
        if not self.initialising:
            # Find default mission link file and what mission is it pointing to
            remote_file_rows = self.remote_table_model.get_file_rows()
            if self.get_selected_remote_path() == self.remotepath:
                try:
                    cmd = f" readlink {self.get_selected_remote_path()}/default_mission.xml"
                    ssh_command = f"ssh -o BatchMode=yes {self.remote_user}@{self.remote_ip}{cmd}"
                    ps = subprocess.Popen(ssh_command, shell=True, stdout=subprocess.PIPE,
                                          stderr=subprocess.STDOUT)
                    output = ps.communicate()[0]
                    if output is not None:
                        output_decoded = output.decode()
                    for line in output_decoded.split("\n"):
                        if len(line) != 0 and "No such file or directory" not in line:
                            default_mission = path.basename(line)
                    for remote_row in remote_file_rows:
                        # Paint default mission of the AUV
                        if remote_row[1].text() == default_mission:
                            for item in remote_row:
                                item.setBackground(QBrush(self.current_mission_color))
                        else:
                            for item in remote_row:
                                item.setBackground(QBrush())
                except IOError as e:
                    logger.info("There is no 'default mission' link. %s", e)
                except AttributeError as e:
                    logger.warning("Connection was closed. %s", e)

    def show_help(self):
        """ Override method of SFTPConnectionDialog class. Creates a help dialog """
        widget = SFTPHelpDialog(self.same_file_marker_icon,
                                self.file_not_found_marker_icon,
                                self.older_file_marker_icon,
                                self.newer_file_marker_icon,
                                self.current_mission_color)
        widget.exec_()
