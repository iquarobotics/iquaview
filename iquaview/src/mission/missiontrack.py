# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Class for mission track objects.
 Handles the rendering of the associated mission structure (from the mission xml file) to a layer in the interface.
 It also handles all the changes (addition, removal and update of steps) in the mission structure.
 """

import logging
import os

from PyQt5.QtCore import pyqtSignal, QObject
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QMessageBox
from qgis.core import (QgsPoint,
                       QgsPointXY,
                       QgsVectorLayer,
                       QgsProject,
                       QgsGeometry,
                       QgsMapLayer,
                       QgsFeature,
                       QgsSymbol,
                       QgsSingleSymbolRenderer,
                       QgsSymbolLayerRegistry,
                       QgsDataProvider,
                       QgsWkbTypes,
                       QgsMarkerLineSymbolLayer,
                       QgsSimpleLineSymbolLayer)

from iquaview.src.mission.startendmarker import StartEndMarker
from iquaview_lib.cola2api.convert_mission import convert_mission
from iquaview_lib.cola2api.mission_types import (SECTION_MANEUVER,
                                                 GOTO_MANEUVER,
                                                 PARK_MANEUVER,
                                                 Mission,
                                                 MissionStep,
                                                 MissionGoto,
                                                 MissionSection,
                                                 MissionPark,
                                                 HEAVE_MODE_DEPTH,
                                                 HEAVE_MODE_ALTITUDE)

logger = logging.getLogger(__name__)


class MissionTrack(QObject):
    """ Class for mission track objects."""
    mission_changed = pyqtSignal(int)
    step_removed = pyqtSignal(int)

    def __init__(self, mission_name="Mission", abs_path=None, canvas=None, proj=None, track_color='255,0,0', markers_color='205,0,0'):
        """
        Init of the object MissionTrack
        :param mission_name: name of the mission
        :type mission_name: str
        :param abs_path: absolute path where the mission is saved
        :type abs_path: str
        :param canvas: map canvas where the mission has to be rendered
        :type canvas: QgsMapCanvas
        :param track_color: color of the rendered track of the mission
        :type track_color: str
        :param markers_color: color of the rendered markers of the mission
        :type markers_color: str
        """
        super().__init__()
        self.mission_abs_path = abs_path
        self.mission_name = mission_name
        self.mission_layer = None
        self.mission_symbol = None
        self.mission = Mission()
        self.canvas = canvas
        self.proj = proj
        self.default_track_color = track_color
        self.default_markers_color = markers_color
        self.current_track_color = track_color
        self.current_markers_color = markers_color
        self.start_end_marker = StartEndMarker(canvas, self.find_waypoints_in_mission(),
                                               QColor(self.default_markers_color))

        self.saved = False
        self.modified = False

        canvas.destinationCrsChanged.connect(self.update_start_end_markers)

    def is_saved(self):
        """
        :return: return True if mission is saved,  otherwise False
        """
        return self.saved

    def is_modified(self):
        """
        :return: return True if mission is modified,  otherwise False
        """
        return self.modified

    def set_modified(self, modified: bool):
        """ Set modified state"""
        self.modified = modified

    def load_mission(self, path: str):
        """ Load mission located in the absolute path provided """
        try:
            result = self.mission.load_mission(path)
            if not result:
                reply = QMessageBox.question(None, 'Load Mission',
                                             "Invalid version. The mission file does not match the latest version. "
                                             "You want to update and load the file?",
                                             QMessageBox.Yes, QMessageBox.No)
                if reply == QMessageBox.Yes:
                    new_path = os.path.dirname(path) + '/' + os.path.splitext(os.path.basename(path))[0] + '_v2.xml'
                    convert_mission(path, new_path)
                    result = self.mission.load_mission(new_path)
                    path = new_path
            if result:
                self.mission_abs_path = path
                self.saved = True
        except Exception as e:
            logger.error(e)
            raise Exception(f"Invalid mission file {e}") from e

    def set_mission(self, mission: Mission):
        """ Set Mission."""
        self.mission = mission

    def set_mission_abs_path(self, path: str):
        """
        Set Mission absolute path
        :param path: new absolute path
        :type path: str
        """
        self.mission_abs_path = path

    def set_mission_name(self, name: str):
        """ Set Mission name."""
        self.mission_name = name

    def set_mission_layer(self, layer: QgsVectorLayer):
        """ Set mission layer."""
        self.mission_layer.setName(self.mission_name)
        self.mission_layer = layer

    def layer_renderer_changed(self):
        if self.mission_layer.customProperty("highlighted") is not None \
                and not self.mission_layer.customProperty("highlighted"):
            self.mission_symbol = self.mission_layer.renderer().symbol().clone()

    def set_mission_renderer(self, symbol: QgsSymbol):
        """ Set mission renderer."""
        self.mission_symbol = symbol.clone()
        mission_renderer = QgsSingleSymbolRenderer(symbol.clone())
        self.mission_layer.setRenderer(mission_renderer)

    def set_mission_color(self, color: QColor):
        """
        Sets mission color
        :param color: color to change to
        :type color: QColor
        """
        if self.mission_layer.geometryType() == QgsWkbTypes.LineGeometry:
            symbol = self.get_default_track_renderer()
        else:
            symbol = self.get_default_point_renderer()

        self.current_track_color = color
        symbol.setColor(color)
        self.set_mission_renderer(symbol)
        self.mission_layer.triggerRepaint()

    def set_mission_colors_to_default(self):
        """ Set mission colors to default."""
        self.set_mission_color(QColor(self.current_track_color))
        self.set_start_end_markers_color(QColor(self.current_markers_color))

    def set_start_end_markers_color(self, color: QColor):
        """
        Sets start end markers color
        :param color: color to change to
        :type color: QColor
        """
        self.current_markers_color = color
        self.start_end_marker.set_color(color)
        self.update_start_end_markers()

    def get_current_track_color(self):
        """
        Returns the current color of the mission
        :return: color
        :rtype: QColor
        """
        return QColor(self.current_track_color)

    def get_current_start_end_marker_color(self):
        """
        Returns the current color of start end markers
        :return: color
        :rtype: QColor
        """
        return self.start_end_marker.get_color()

    def get_mission(self):
        """ Returns the mission"""
        return self.mission

    def get_step(self, step: int):
        """ Returns the step 'step' of the mission"""
        return self.mission.get_step(step)

    def get_mission_length(self):
        """ Returns mission length"""
        return self.mission.num_steps

    def get_mission_layer(self) -> QgsMapLayer:
        """
        Returns mission layer
        :return: Returns mission layer
        :rtype: QgsMapLayer
        """
        return self.mission_layer

    def get_mission_name(self):
        """ Returns mission name"""
        return self.mission_name

    def get_mission_abs_path(self):
        """ Returns mission absolute path """
        return self.mission_abs_path

    def change_position(self, wp_id: int, point: QgsPointXY):
        """
        Changes a position of the waypoint 'wp_id' with a new position 'point'
        :param wp_id: Current step
        :param point: Point position
        """
        self.mission.get_step(wp_id).get_maneuver().final_latitude = point.y()
        self.mission.get_step(wp_id).get_maneuver().final_longitude = point.x()

        if wp_id != self.mission.get_length() - 1:  # not last point
            if self.mission.get_step(wp_id + 1).get_maneuver().get_maneuver_type() == SECTION_MANEUVER:
                logger.debug("Next step is a section")
                # if next step is a section change its initial position
                self.mission.get_step(wp_id + 1).get_maneuver().initial_latitude = point.y()
                self.mission.get_step(wp_id + 1).get_maneuver().initial_longitude = point.x()
        self.mission_changed.emit(wp_id)
        self.update_start_end_markers()
        self.modified = True

    def remove_step(self, wp_id: int):
        """
        Remove step 'wp_id'
        :param wp_id: the step number to delete
        """
        initial_num_steps = self.mission.num_steps
        if (wp_id == 0 and
                initial_num_steps > 1 and
                self.mission.get_step(wp_id + 1).get_maneuver().get_maneuver_type() == SECTION_MANEUVER):
            section_maneuver = self.mission.get_step(wp_id + 1).get_maneuver()
            manv = MissionGoto()
            manv.final_latitude = section_maneuver.final_latitude
            manv.final_longitude = section_maneuver.final_longitude
            manv.final_depth = section_maneuver.final_depth
            manv.final_altitude = section_maneuver.final_altitude
            manv.heave_mode = section_maneuver.heave_mode
            manv.surge_velocity = section_maneuver.surge_velocity
            manv.tolerance_xy = section_maneuver.tolerance_xy
            manv.no_altitude_goes_up = section_maneuver.no_altitude_goes_up

            self.mission.get_step(wp_id + 1).maneuver = manv

        if (initial_num_steps - 1 != wp_id and self.mission.get_step(
                wp_id + 1).get_maneuver().get_maneuver_type() == SECTION_MANEUVER):
            logger.debug("Next step is a section")
            # if it's not the last step
            # if next step is a section change its initial position to the one of the point before
            previous_latitude = self.mission.get_step(wp_id - 1).get_maneuver().final_latitude
            previous_longitude = self.mission.get_step(wp_id - 1).get_maneuver().final_longitude
            self.mission.get_step(wp_id + 1).get_maneuver().initial_latitude = previous_latitude
            self.mission.get_step(wp_id + 1).get_maneuver().initial_longitude = previous_longitude

        self.mission.remove_step(wp_id)
        self.step_removed.emit(wp_id)

        # When point is deleted, layer may need a geometry type change
        if initial_num_steps <= 2:
            self.update_layer_geometry()

        if wp_id == initial_num_steps - 1:
            # if last waypoint is removed show previous
            self.mission_changed.emit(wp_id - 1)
        else:
            self.mission_changed.emit(wp_id)

        self.update_start_end_markers()

        self.modified = True

    def update_steps(self, id_list: list, mission_step_list: list):
        """
        Update the number of the step 'wp_id' with new number 'step'
        :param id_list: list of step ids to update
        :param mission_step_list: list of mission steps to update
        :return:
        """
        if len(id_list) > 0:
            for i, step in enumerate(id_list):
                self.mission.update_step(step, mission_step_list[i])
            self.mission_changed.emit(id_list[0])
            self.update_start_end_markers()
            self.modified = True

    def update_step(self, wp_id: int, step: MissionStep):
        """
        Update step in wp_id position
        :param wp_id: wp identifier (position in list)
        :type wp_id: int
        :param step: new Mission Step
        :type step: MissionStep
        """
        self.mission.update_step(wp_id, step)
        self.mission_changed.emit(wp_id)
        self.update_start_end_markers()
        self.modified = True

    def add_step(self, wp_id: int, point: QgsPointXY):
        """ Add new step"""
        initial_num_steps = self.mission.num_steps
        logger.debug(f"Add step: Mission has {initial_num_steps} steps and wp_id is {wp_id}")
        step = MissionStep()
        # If more than one point, get maneuver from surrounding waypoint and copy data
        if initial_num_steps > 0:
            if wp_id != 0:

                # if point is at the end or in the middle copy from the previous
                manv_to_copy = self.mission.get_step(wp_id - 1).get_maneuver()

                if manv_to_copy.get_maneuver_type() == SECTION_MANEUVER:
                    # if section, initial pos will be the final pos of the previous
                    manv = MissionSection()
                    latitude = manv_to_copy.final_latitude
                    longitude = manv_to_copy.final_longitude
                    manv.initial_latitude = latitude
                    manv.initial_longitude = longitude
                    manv.initial_depth = manv_to_copy.initial_depth
                    manv.final_latitude = point.y()
                    manv.final_longitude = point.x()
                    manv.final_depth = manv_to_copy.final_depth
                    manv.final_altitude = manv_to_copy.final_altitude
                    manv.heave_mode = manv_to_copy.heave_mode
                    manv.surge_velocity = manv_to_copy.surge_velocity
                    manv.tolerance_xy = manv_to_copy.tolerance_xy
                    manv.no_altitude_goes_up = manv_to_copy.no_altitude_goes_up

                if wp_id != initial_num_steps:
                    # if point in the middle check if next is also section, we need to modify it
                    manv_next = self.mission.get_step(wp_id).get_maneuver()
                    if manv_next.get_maneuver_type() == SECTION_MANEUVER:
                        manv_next.initial_latitude = point.y()
                        manv_next.initial_longitude = point.x()

            else:  # wp_id == 0
                # copy from the next wp, next cannot be a section if was first waypoint until now, so no need to check
                manv_to_copy = self.mission.get_step(wp_id).get_maneuver()

                if self.mission.get_step(wp_id).get_maneuver().get_maneuver_type() == GOTO_MANEUVER:
                    # convert second point to section
                    manv_section = MissionSection()
                    manv_section.initial_latitude = point.y()
                    manv_section.initial_longitude = point.x()
                    manv_section.initial_depth = manv_to_copy.final_depth
                    manv_section.final_latitude = manv_to_copy.final_latitude
                    manv_section.final_longitude = manv_to_copy.final_longitude
                    manv_section.final_depth = manv_to_copy.final_depth
                    manv_section.final_altitude = manv_to_copy.final_altitude
                    manv_section.heave_mode = manv_to_copy.heave_mode
                    manv_section.surge_velocity = manv_to_copy.surge_velocity
                    manv_section.tolerance_xy = manv_to_copy.tolerance_xy
                    manv_section.no_altitude_goes_up = manv_to_copy.no_altitude_goes_up

                    step_section = MissionStep()
                    step_section.add_maneuver(manv_section)
                    step_section.actions = self.mission.get_step(wp_id).get_actions()
                    self.mission.update_step(wp_id, step_section)

            if manv_to_copy.get_maneuver_type() == GOTO_MANEUVER:
                manv = MissionGoto()
                manv.final_latitude = point.y()
                manv.final_longitude = point.x()
                manv.final_depth = manv_to_copy.final_depth
                manv.final_altitude = manv_to_copy.final_altitude
                manv.heave_mode = manv_to_copy.heave_mode
                manv.surge_velocity = manv_to_copy.surge_velocity
                manv.tolerance_xy = manv_to_copy.tolerance_xy
                manv.no_altitude_goes_up = manv_to_copy.no_altitude_goes_up

            elif manv_to_copy.get_maneuver_type() == PARK_MANEUVER:
                manv = MissionPark()
                manv.final_latitude = point.y()
                manv.final_longitude = point.x()
                manv.final_depth = manv_to_copy.final_depth
                manv.final_altitude = manv_to_copy.final_altitude
                manv.final_yaw = manv_to_copy.final_yaw
                manv.use_yaw = manv_to_copy.use_yaw
                manv.heave_mode = manv_to_copy.heave_mode
                manv.surge_velocity = manv_to_copy.surge_velocity
                manv.time = manv_to_copy.time
                manv.no_altitude_goes_up = manv_to_copy.no_altitude_goes_up

        else:
            # is first point of the mission, fill maneuver by default type waypoint with clicked position
            manv = MissionGoto(point.y(), point.x(), 0.0, 1.0, HEAVE_MODE_DEPTH, 0.5, 2.0)

        step.add_maneuver(manv)

        self.mission.insert_step(wp_id, step)
        logger.debug(f"Mission has now {self.mission.num_steps} steps")

        # When point is added, layer may need a geometry type change
        if initial_num_steps <= 1:
            self.update_layer_geometry()
        self.mission_changed.emit(wp_id)
        self.update_start_end_markers()
        self.modified = True

    def show_saving_warnings(self):
        """
        Shows warnings to be accepted or rejected in order to confirm the saving action
        :return: accepted or rejected
        :rtype: bool
        """
        msg = ""
        if self.mission.num_steps == 0:
            msg = "You are about to save an empty mission. Do you want to proceed?"

        elif ((float(self.mission.get_step(self.mission.get_length() - 1).get_maneuver().final_depth) != 0.0
               or self.mission.get_step(self.mission.get_length() - 1).get_maneuver().heave_mode == HEAVE_MODE_ALTITUDE)
              and (float(self.mission.get_step(0).get_maneuver().final_depth) != 0.0
                   or self.mission.get_step(0).get_maneuver().heave_mode == HEAVE_MODE_ALTITUDE)):
            msg = "You are about to save and the first and last waypoint are not at zero depth. Do you want to proceed?"

        elif (float(self.mission.get_step(self.mission.get_length() - 1).get_maneuver().final_depth) != 0.0
              or self.mission.get_step(self.mission.get_length() - 1).get_maneuver().heave_mode == HEAVE_MODE_ALTITUDE):
            msg = "You are about to save and the last waypoint is not at zero depth. Do you want to proceed?"

        elif (float(self.mission.get_step(0).get_maneuver().final_depth) != 0.0
              or self.mission.get_step(0).get_maneuver().heave_mode == HEAVE_MODE_ALTITUDE):
            msg = "You are about to save and the first waypoint is not at zero depth. Do you want to proceed?"

        if msg:
            reply = QMessageBox.question(None, f"Save Mission - {self.mission_name}",
                                         msg,
                                         QMessageBox.Yes |
                                         QMessageBox.No,
                                         QMessageBox.Yes)
            if reply == QMessageBox.No:
                return False
        return True

    def save_mission(self):
        """ Saves a mission"""
        logger.info(f"Saving mission to {self.mission_abs_path}")
        self.mission.write_mission(self.mission_abs_path)

        # Check if mission is in project folder
        project_folder = os.path.dirname(self.proj.fileName())
        common_path = os.path.commonpath([project_folder, self.mission_abs_path])
        if common_path == project_folder and os.path.isabs(self.mission_abs_path):
            relative_path = "./" + os.path.relpath(self.mission_abs_path, project_folder)
            self.mission_layer.setCustomProperty("mission_xml", relative_path)
        else:
            self.mission_layer.setCustomProperty("mission_xml", self.mission_abs_path)
        self.saved = True
        self.modified = False
        return True

    def render_mission(self):
        """
        Render the mission, it will render the mission layer with Point geometry if it has only 1 point
        oterwise it will be with LineString geometry.
        """
        waypoints = self.find_waypoints_in_mission()
        if self.mission_layer is None:
            if len(waypoints) == 1:
                self.mission_layer = QgsVectorLayer(
                    "Point?crs=epsg:4326",
                    self.mission_name,
                    "memory")
                self.mission_layer.rendererChanged.connect(self.layer_renderer_changed)

                # Check if mission is in project folder
                project_folder = os.path.dirname(self.proj.fileName())
                common_path = os.path.commonpath([project_folder, self.mission_abs_path])
                if common_path == project_folder and os.path.isabs(self.mission_abs_path):
                    relative_path = "./" + os.path.relpath(self.mission_abs_path, project_folder)
                    self.mission_layer.setCustomProperty("mission_xml", relative_path)
                else:
                    self.mission_layer.setCustomProperty("mission_xml", self.mission_abs_path)
                feature = QgsFeature()
                feature.setGeometry(QgsGeometry(waypoints[0]))
                self.mission_layer.dataProvider().addFeatures([feature])
            else:
                self.mission_layer = QgsVectorLayer(
                    "LineString?crs=epsg:4326",
                    self.mission_name,
                    "memory")
                self.mission_layer.rendererChanged.connect(self.layer_renderer_changed)

                # Check if mission is in project folder
                project_folder = os.path.dirname(self.proj.fileName())
                if os.path.isabs(project_folder):
                    common_path = os.path.commonpath([project_folder, self.mission_abs_path])
                    if common_path == project_folder and os.path.isabs(self.mission_abs_path):
                        relative_path = "./" + os.path.relpath(self.mission_abs_path, project_folder)
                        self.mission_layer.setCustomProperty("mission_xml", relative_path)
                    else:
                        self.mission_layer.setCustomProperty("mission_xml", self.mission_abs_path)
                else:
                    self.mission_layer.setCustomProperty("mission_xml", self.mission_abs_path)
                feature = QgsFeature()
                feature.setGeometry(QgsGeometry.fromPolyline(waypoints))
                self.mission_layer.dataProvider().addFeatures([feature])

        else:
            # mission layer exists, now check if it has already a feature or not
            feats = self.mission_layer.getFeatures()
            logger.debug(f"layer feature count {self.mission_layer.featureCount()}")
            if self.mission_layer.featureCount() == 0:
                feature = QgsFeature()
                if len(waypoints) == 1:
                    feature.setGeometry(QgsGeometry(waypoints[0]))
                else:
                    feature.setGeometry(QgsGeometry.fromPolyline(waypoints))
                self.mission_layer.dataProvider().addFeatures([feature])
            else:
                logger.debug("layer has feature mission already, updating...")
                for f in feats:
                    self.mission_layer.dataProvider().deleteFeatures([f.id()])
                    feature = QgsFeature()
                    if len(waypoints) == 1:
                        feature.setGeometry(QgsGeometry(waypoints[0]))
                    else:
                        feature.setGeometry(QgsGeometry.fromPolyline(waypoints))
                    self.mission_layer.dataProvider().addFeatures([feature])
        if len(waypoints) == 1:
            self.set_mission_renderer(self.get_default_point_renderer())
        else:
            self.set_mission_renderer(self.get_default_track_renderer())

    def find_waypoints_in_mission(self, indexes: list = None):
        """
        Gets all waypoints from a mission structure
        """
        waypoints = []
        if indexes is None:
            step_indexes = range(0, self.mission.size())
        else:
            step_indexes = indexes

        for stepindex in step_indexes:
            step = self.mission.get_step(stepindex)
            maneuver = step.get_maneuver()

            waypoints.append(QgsPoint(float(maneuver.final_longitude), float(maneuver.final_latitude)))
        return waypoints

    def create_highlighted_track_renderer(self, symbol, color):
        # Clone the base renderer's symbol
        renderer_symbol = symbol.clone()

        # Add a contour (border) layer with the specified color
        contour = QgsSimpleLineSymbolLayer()
        contour.setColor(QColor(color))
        contour.setWidth(2)  # Adjust the contour width as needed

        # Append the contour layer to the symbol
        renderer_symbol.insertSymbolLayer(0, contour)

        # Find the marker layers in the symbol and modify their properties
        for layer in renderer_symbol.symbolLayers():
            if isinstance(layer, QgsMarkerLineSymbolLayer):
                # Modify the marker layer properties using the provided dictionary
                layer.setColor(color)

        return renderer_symbol

    def get_default_track_renderer(self):
        """Renderer for track lines"""
        registry = QgsSymbolLayerRegistry()
        line_meta = registry.symbolLayerMetadata("SimpleLine")
        marker_meta = registry.symbolLayerMetadata("MarkerLine")

        symbol = QgsSymbol.defaultSymbol(self.mission_layer.geometryType())

        # Line layer
        line_layer = line_meta.createSymbolLayer(
            {'width': '0.5', 'color': self.default_track_color, 'offset': '0.0', 'penstyle': 'solid',
             'use_custom_dash': '0',
             'joinstyle': 'bevel', 'capstyle': 'square'})

        # Marker layer
        marker_layer = marker_meta.createSymbolLayer(
            {'width': '1.5', 'color': self.default_track_color, 'placement': 'vertex', 'offset': '0.0'})
        sub_symbol = marker_layer.subSymbol()

        # Replace the default layer with our own SimpleMarker
        sub_symbol.deleteSymbolLayer(0)
        cross = registry.symbolLayerMetadata("SimpleMarker").createSymbolLayer(
            {'name': 'circle', 'color': self.default_track_color, 'color_border': '0,0,0', 'offset': '0,0',
             'size': '2.5',
             'angle': '0'})
        sub_symbol.appendSymbolLayer(cross)

        # Replace the default layer with our two custom layers
        symbol.deleteSymbolLayer(0)
        symbol.appendSymbolLayer(line_layer)
        symbol.appendSymbolLayer(marker_layer)

        # Replace the renderer of the current layer
        # renderer = QgsSingleSymbolRenderer(symbol)
        return symbol

    def get_default_point_renderer(self):
        """Renderer for points (missions with one waypoint)"""
        registry = QgsSymbolLayerRegistry()
        marker_meta = registry.symbolLayerMetadata("SimpleMarker")

        symbol = QgsSymbol.defaultSymbol(self.mission_layer.geometryType())

        # Marker layer
        marker_layer = marker_meta.createSymbolLayer(
            {'size': '2.5', 'color': self.default_track_color, 'placement': 'vertex', 'offset': '0.0'})

        # Replace the default layer with our custom layer
        symbol.deleteSymbolLayer(0)
        symbol.appendSymbolLayer(marker_layer)

        # Replace the renderer of the current layer
        # renderer = QgsSingleSymbolRenderer(symbol)
        return symbol

    def update_start_end_markers(self):
        """ Updates start_end_markers"""

        root = QgsProject.instance().layerTreeRoot()
        tree_layer = root.findLayer(self.get_mission_layer())
        if tree_layer is not None and tree_layer.isVisible():
            self.start_end_marker.update_markers(self.find_waypoints_in_mission())
        else:
            self.hide_start_end_markers()

    def hide_start_end_markers(self):
        """ Hide start_end_markers"""
        self.start_end_marker.hide_markers()

    def remove_start_end_markers(self):
        """ Removes start end markers"""
        self.canvas.destinationCrsChanged.disconnect(self.update_start_end_markers)
        self.start_end_marker.close_markers()

    def update_layer_geometry(self):
        """
        This function needs to be called every time a point is added or deleted from the layer
        If needed, a new layer is created with the apropiate geometry
        Point if only one point, LineString otherwise
        """
        options = QgsDataProvider.ProviderOptions()
        waypoints = self.find_waypoints_in_mission()
        current_color = self.get_current_track_color()
        symbol = self.mission_symbol
        if (len(waypoints) != 1) and self.mission_layer.geometryType() == QgsWkbTypes.PointGeometry:
            self.mission_layer.setDataSource("LineString?crs=epsg:4326",
                                             self.mission_name,
                                             "memory",
                                             options)
            symbol = self.get_default_track_renderer()
        elif len(waypoints) == 1 and self.mission_layer.geometryType() == QgsWkbTypes.LineGeometry:
            self.mission_layer.setDataSource("Point?crs=epsg:4326",
                                             self.mission_name,
                                             "memory",
                                             options)
            symbol = self.get_default_point_renderer()

        # To not lose the current color of the mission when changing geometry
        symbol.setColor(current_color)
        self.set_mission_renderer(symbol)
