# -*- coding: utf-8 -*-
# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
    This module contains the MissionModule class. It is the class that handles the Qt interface for the mission
"""

import logging
import os

from PyQt5.QtCore import QObject, Qt, QSettings
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QAction, QToolBar, QDockWidget, QMessageBox, QFileDialog
from qgis.core import QgsLayerTreeLayer, QgsLayerTreeGroup, QgsMapLayer, QgsProject
from qgis.gui import QgsLayerTreeView

from iquaview.src.mission.missioncontroller import MissionController
from iquaview_lib.vehicle.vehiclestatus import VehicleStatus
from iquaview_lib.vehicle.vehicleinfo import VehicleInfo
from iquaview_lib.vehicle.vehicledata import VehicleData
from iquaview.src.mission.sftp_mission_transfer_dialog import SFTPMissionTransferDialog

logger = logging.getLogger(__name__)


class MissionModule(QObject):
    """
    This class handles the Qt interface for the mission
    """

    def __init__(self, **kwargs) -> None:
        super().__init__(kwargs['parent'])

        self.parent = kwargs['parent']
        self.mission_menu = kwargs['mission_menu']
        self.config = kwargs['config']
        self.vehicle_info: VehicleInfo = kwargs['vehicle_info']
        self.vehicle_data: VehicleData = kwargs['vehicle_data']
        self.proj = kwargs['proj']
        self.canvas = kwargs['canvas']
        self.view: QgsLayerTreeView = kwargs['view']
        self.msg_log = kwargs['msg_log']
        self.activated = False

        self.local_ini_path = os.path.dirname(os.path.abspath(self.config.settings['last_open_project']))
        if os.path.isdir(self.local_ini_path + '/missions/'):
            self.local_ini_path += '/missions/'
        self.sftp_transfer_dialog = None
        self.current_layer = None
        self.current_layer_visible = None
        # Actions for Mission
        self.new_mission_action = QAction(QIcon(":/resources/mActionNewMission.svg"), "New Mission...", self)
        self.load_mission_action = QAction(QIcon(":/resources/mActionLoadMissionFile.svg"), "Load Mission...", self)
        self.start_sftp_action = QAction(QIcon(":/resources/mActionTransferMission.svg"), "Transfer Mission...", self)
        self.save_mission_action = QAction(QIcon(":/resources/mActionSaveMission.svg"), "Save Mission", self)
        self.saveas_mission_action = QAction(QIcon(":/resources/mActionSaveAsMission.svg"), "Save Mission As...", self)
        self.edit_wp_mission_action = QAction(QIcon(":/resources/mActionEditWaypoints.svg"), "Edit Mission Waypoints",
                                              self)
        self.edit_wp_mission_action.setShortcut("Ctrl+e")
        self.select_features_mission_action = QAction(QIcon(":/resources/mActionEditMultipleWaypoints.svg"),
                                                      "Edit Multiple Mission Waypoints",
                                                      self)
        self.template_mission_action = QAction(QIcon(":/resources/mActionAddMissionTemplate.svg"),
                                               "Add Mission Template",
                                               self)
        self.move_mission_action = QAction(QIcon(":/resources/mActionMoveMission.svg"), "Move Mission", self)

        self.edit_wp_mission_action.setCheckable(True)
        self.select_features_mission_action.setCheckable(True)
        self.template_mission_action.setCheckable(True)
        self.move_mission_action.setCheckable(True)

        self.mission_menu.addActions([self.new_mission_action,
                                      self.load_mission_action,
                                      self.start_sftp_action,
                                      self.save_mission_action,
                                      self.saveas_mission_action,
                                      self.edit_wp_mission_action,
                                      self.select_features_mission_action,
                                      self.template_mission_action,
                                      self.move_mission_action])

        self.create_dock_widgets()

        self.new_mission_action.triggered.connect(self.new_mission)
        self.load_mission_action.triggered.connect(self.load_mission)
        self.save_mission_action.triggered.connect(self.save_mission)
        self.saveas_mission_action.triggered.connect(self.saveas_mission)
        self.start_sftp_action.triggered.connect(self.open_sftp_mission_transfer_dialog)

        self.edit_wp_mission_action.toggled.connect(self.edit_wp_mission)
        self.select_features_mission_action.toggled.connect(self.select_features_mission)
        self.template_mission_action.toggled.connect(self.add_template_mission)
        self.move_mission_action.toggled.connect(self.move_mission)

        # Handle for mission controller
        self.mission_ctrl = MissionController(
            self.config,
            self.vehicle_info,
            self.proj,
            self.canvas,
            self.view,
            self.wp_dock,
            self.templates_dock,
            self.minfo_dock,
            self.msg_log
        )
        self.mission_ctrl.mission_added.connect(self.parent.add_map_layer)
        self.mission_ctrl.template_closed.connect(self.template_closed)
        self.mission_ctrl.stop_mission_editing.connect(self.disable_mission_editing)
        self.vehicle_status = VehicleStatus(self.vehicle_data, self.msg_log)

    def create_dock_widgets(self) -> None:
        """
        Create the dock widgets
        """

        # Dock for Mission Info
        self.minfo_dock = QDockWidget("Mission Info", self.parent)
        self.minfo_dock.setObjectName("Mission Info")
        self.minfo_dock.setAllowedAreas(Qt.RightDockWidgetArea)
        self.minfo_dock.setContentsMargins(9, 9, 9, 9)
        self.minfo_dock.setStyleSheet("QDockWidget { font: bold; }")
        self.minfo_dock.setWindowTitle("Mission Info")
        self.minfo_dock.hide()

        # Dock for Waypoint Edit
        self.wp_dock = QDockWidget("Waypoint Editing", self.parent)
        self.wp_dock.setObjectName("Waypoint Editing")
        self.wp_dock.setAllowedAreas(Qt.LeftDockWidgetArea | Qt.RightDockWidgetArea)
        self.wp_dock.setContentsMargins(9, 9, 9, 9)
        self.wp_dock.setStyleSheet("QDockWidget { font: bold; }")
        self.wp_dock.setWindowTitle("Mission Waypoint Editor")
        self.wp_dock.hide()

        # Dock for Mission Templates
        self.templates_dock = QDockWidget("Mission Templates", self.parent)
        self.templates_dock.setObjectName("Mission Templates")
        self.templates_dock.setAllowedAreas(Qt.LeftDockWidgetArea | Qt.RightDockWidgetArea)
        self.templates_dock.setContentsMargins(9, 9, 9, 9)
        self.templates_dock.setStyleSheet("QDockWidget { font: bold; }")
        self.templates_dock.setWindowTitle("Mission Templates")
        self.templates_dock.hide()

        # Toolbar for Mission
        self.mission_toolbar = QToolBar("Mission Tools")
        self.mission_toolbar.setObjectName("Mission Tools")
        self.mission_toolbar.addAction(self.new_mission_action)
        self.mission_toolbar.addAction(self.load_mission_action)
        self.mission_toolbar.addAction(self.start_sftp_action)
        self.mission_toolbar.addAction(self.save_mission_action)
        self.mission_toolbar.addAction(self.saveas_mission_action)
        self.mission_toolbar.addAction(self.edit_wp_mission_action)
        self.mission_toolbar.addAction(self.select_features_mission_action)
        self.mission_toolbar.addAction(self.template_mission_action)
        self.mission_toolbar.addAction(self.move_mission_action)

    def get_toolbar(self) -> QToolBar:
        """
        Create the mission toolbar
        """
        return self.mission_toolbar

    def get_mission_controller(self) -> MissionController:
        """
        Get the mission controller
        :return: returns the mission controller
        :rtype: MissionController
        """
        return self.mission_ctrl

    def get_vehicle_status(self) -> VehicleStatus:
        """
        Get the vehicle status
        :return: returns the vehicle status
        :rtype: VehicleStatus
        """
        return self.vehicle_status

    def get_mission_info_dockwidget(self) -> QDockWidget:
        """
        Get the mission info dock widget
        :return: returns the mission info dock widget
        :rtype: QDockWidget
        """
        return self.minfo_dock

    def get_waypoint_editing_dockwidget(self) -> QDockWidget:
        """
        Get the waypoint editing dock widget
        :return: returns the waypoint editing dock widget
        :rtype: QDockWidget
        """
        return self.wp_dock

    def get_templates_dockwidget(self) -> QDockWidget:
        """
        Get the templates dock widget
        :return: returns the templates dock widget
        :rtype: QDockWidget
        """
        return self.templates_dock

    def activate_mission_tools(self, activated):
        """
        Change mission tools enable.
        :param activated: is mission tools activated
        :type activated: bool
        """
        self.activated = activated
        self.save_mission_action.setEnabled(activated)
        self.saveas_mission_action.setEnabled(activated)
        self.edit_wp_mission_action.setEnabled(activated)
        self.select_features_mission_action.setEnabled(activated)
        self.move_mission_action.setEnabled(activated)
        self.template_mission_action.setEnabled(activated)

    def new_mission(self):
        """ Create new mission."""
        if self.proj.fileName() == "":
            QMessageBox.warning(None, "No opened project",
                                "You do not have any project open to create the mission. \n "
                                "Open a project or save the current one.")
        else:
            self.disable_mission_editing()
            self.mission_ctrl.new_mission()

    def disable_mission_editing(self):
        """
        closes any mission editing mode open
        """
        # check if we were editing something, first close template edition if existing as this resorts to wp editing
        if self.template_mission_action.isChecked():
            self.template_mission_action.setChecked(False)
        if self.edit_wp_mission_action.isChecked():
            self.edit_wp_mission_action.setChecked(False)
        if self.move_mission_action.isChecked():
            self.move_mission_action.setChecked(False)
        if self.select_features_mission_action.isChecked():
            self.select_features_mission_action.setChecked(False)

    def load_mission(self):
        """ Open file dialog to load multiple missions if a project is already opened. """
        if self.proj.fileName() == "":
            QMessageBox.warning(
                None, "No opened project",
                "You do not have any project open to load the mission. \n Open a project or save the current one.")

        else:
            self.disable_mission_editing()
            settings = QSettings()
            # get last used directory
            last_dir = settings.value("/Mission/lastMissionDir", "", type=str)
            filenames = QFileDialog.getOpenFileNames(self.parent, 'Load Mission', last_dir, '*.xml')
            if filenames[0]:
                settings.setValue("/Mission/lastMissionDir", os.path.dirname(filenames[0][0]))
            for file in filenames[0]:
                try:
                    self.mission_ctrl.load_mission(file)
                except Exception as e:
                    logger.error(f"Mission file {file} could not be loaded")
                    QMessageBox.critical(
                        self.parent, e.args[0],
                        "Mission file " + file + " could not be loaded, make sure is a valid mission file.",
                        QMessageBox.Close)

    def save_mission(self):
        """ Save current mission."""
        self.disable_mission_editing()

        try:
            saved = self.mission_ctrl.save_mission()
            if saved:
                QMessageBox.information(self.parent,
                                        "Mission saved",
                                        f"Mission file saved in "
                                        f"{self.mission_ctrl.get_current_mission_absolute_path()}",
                                        QMessageBox.Close)
        except Exception as e:
            logger.error("Mission file could not be saved.")
            QMessageBox.critical(self.parent,
                                 "Mission file could not be saved.",
                                 e.args[0],
                                 QMessageBox.Close)

    def saveas_mission(self):
        """ Save current mission."""
        self.disable_mission_editing()

        try:
            saved, mission_filename = self.mission_ctrl.saveas_mission()
            if saved:
                QMessageBox.information(self.parent,
                                        "Mission saved",
                                        f"Mission file saved in {mission_filename}",
                                        QMessageBox.Close)
        except Exception as e:
            logger.error(f"Mission file could not be saved. {e}")
            QMessageBox.critical(self.parent,
                                 "Mission file could not be saved.",
                                 e.args[0],
                                 QMessageBox.Close)

    def open_sftp_mission_transfer_dialog(self):
        """ Opens a mission transfer dialog """
        user = self.vehicle_info.get_vehicle_user()
        ip = self.vehicle_info.get_vehicle_ip()
        remote_ini_path = self.vehicle_info.get_remote_vehicle_package() + "/missions"
        if self.view.currentLayer() is not None and self.view.currentLayer().customProperty("mission_xml") is not None:
            local_ini_path = os.path.dirname(self.mission_ctrl.get_current_mission_absolute_path()) + "/"
        else:
            local_ini_path = self.local_ini_path
        if self.sftp_transfer_dialog is None or self.sftp_transfer_dialog.isHidden():
            if self.proj.fileName() == "":
                QMessageBox.warning(None, "No open project", "Open a project to start the sftp connection")
            else:
                self.sftp_transfer_dialog = SFTPMissionTransferDialog(user, ip, local_ini_path, remote_ini_path,
                                                                      project=self.proj,
                                                                      mcontrol=self.mission_ctrl, parent=self.parent)
                self.sftp_transfer_dialog.local_path_changed_signal.connect(self.update_local_ini_path)
                self.sftp_transfer_dialog.show()
                if self.view.currentLayer() is not None and self.view.currentLayer().customProperty(
                        "mission_xml") is not None:
                    self.sftp_transfer_dialog.select_mission(f"{self.mission_ctrl.get_current_mission_name()}.xml")

    def update_local_ini_path(self, path: str):
        """
        update local initial path
        :param path: new path
        :type path: str
        """
        self.local_ini_path = path

    def edit_wp_mission(self):
        """ Allows edition of waypoint mission. """
        self.parent.reset_map_tool()

        if self.edit_wp_mission_action.isChecked():
            # uncheck others tools
            self.check_mission_edit_tools('editwp')
            # hide current mission layer
            self.current_layer: QgsMapLayer = self.mission_ctrl.get_current_mission_layer()
            root = QgsProject.instance().layerTreeRoot()
            tree_layer = root.findLayer(self.current_layer)
            if tree_layer is not None:
                self.current_layer_visible = tree_layer.isVisible()

            self.view.setLayerVisible(self.current_layer, False)
            self.mission_ctrl.edit_wp_mission()
        else:
            self.mission_ctrl.finish_edit_wp_mission()
            if self.current_layer is not None:
                if self.current_layer_visible:
                    self.view.setLayerVisible(self.current_layer, True)
                self.current_layer_visible = None

            self.parent.pan()

    def select_features_mission(self):
        """ Allows select features mission. """
        self.parent.reset_map_tool()

        if self.select_features_mission_action.isChecked():
            # uncheck others tools
            self.check_mission_edit_tools('selectfeatures')

            self.current_layer: QgsMapLayer = self.mission_ctrl.get_current_mission_layer()
            root = QgsProject.instance().layerTreeRoot()
            tree_layer = root.findLayer(self.current_layer)
            if tree_layer is not None:
                self.current_layer_visible = tree_layer.isVisible()
            self.view.setLayerVisible(self.current_layer, True)

            self.mission_ctrl.select_features_mission()

        else:
            self.mission_ctrl.finish_select_features_mission()
            if self.current_layer is not None:
                if not self.current_layer_visible:
                    self.view.setLayerVisible(self.current_layer, False)
                self.current_layer_visible = None
            self.parent.pan()

    def add_template_mission(self):
        """ Allow adding template in  mission."""

        self.parent.reset_map_tool()

        if self.template_mission_action.isChecked():
            self.check_mission_edit_tools('template')
            self.mission_ctrl.add_template_mission()
        else:
            if self.mission_ctrl.is_template_modified():
                reply = QMessageBox.question(self.parent,
                                             "Template track not saved to mission",
                                             "Template Tracks were not saved to mission. Do you want to save them "
                                             "before closing template edition?",
                                             QMessageBox.Yes, QMessageBox.No)
                if reply == QMessageBox.Yes:
                    self.mission_ctrl.save_template_tracks_to_mission()

            self.mission_ctrl.close_template_editing()

    def move_mission(self):
        """ Allows to move the mission."""

        self.parent.reset_map_tool()

        if self.move_mission_action.isChecked():
            self.check_mission_edit_tools('move')
            self.view.currentNode().setItemVisibilityChecked(True)
            self.mission_ctrl.move_mission()
        else:
            self.mission_ctrl.finish_move_mission()
            self.parent.pan()

    def check_mission_edit_tools(self, current):
        """
        The mission edit tools different from the current will be False.
        :param current: current edit tool
        :type current: string
l
        """
        if current == 'move':
            self.edit_wp_mission_action.setChecked(False)
            self.select_features_mission_action.setChecked(False)
            if self.template_mission_action.isChecked():
                self.template_mission_action.setChecked(False)
            self.move_mission_action.setChecked(True)
        elif current == 'editwp':
            self.move_mission_action.setChecked(False)
            self.template_mission_action.setChecked(False)
            self.select_features_mission_action.setChecked(False)
            self.edit_wp_mission_action.setChecked(True)
        elif current == 'template':
            self.move_mission_action.setChecked(False)
            self.edit_wp_mission_action.setChecked(False)
            self.select_features_mission_action.setChecked(False)
            self.template_mission_action.setChecked(True)
        elif current == 'selectfeatures':
            self.move_mission_action.setChecked(False)
            self.edit_wp_mission_action.setChecked(False)
            if self.template_mission_action.isChecked():
                self.template_mission_action.setChecked(False)
            self.select_features_mission_action.setChecked(True)

    def template_closed(self):
        """ Uncheck Template Mission Action and Check Edit Wp Mission Action"""
        self.template_mission_action.setChecked(False)
        self.edit_wp_mission_action.setChecked(True)

    @staticmethod
    def is_mission_layer(layer):
        """
        check if layer is a mission.

        :param layer: layer to check
        :type layer: QgsVectorLayer
        :return: return True if layer is mission layer, otherwise False
        :rtype: bool
        """
        if layer is None or layer.customProperty("mission_xml") is None:
            return False

        return True

    def set_mission_modified(self):
        """ Set modified state of the current mission layer to True"""
        if self.view.currentLayer() is not None and self.view.currentLayer().customProperty("mission_xml") is not None:
            self.mission_ctrl.get_current_mission().set_modified(True)

    def send_mission_to_auv(self):
        """ Send mission to AUV"""
        self.mission_ctrl.send_mission_to_auv()

    def change_mission_markers_visibility(self, treelayer):
        """
        change start end vertex markers
        :param treelayer: tree layer
        :type treelayer: QgsLayerTree
        """
        if isinstance(treelayer, QgsLayerTreeLayer) and self.is_mission_layer(treelayer.layer()):
            mission_list = self.mission_ctrl.get_mission_list()
            for mission_track in mission_list:
                if mission_track.get_mission_layer() == treelayer.layer():
                    mission_track.update_start_end_markers()
        elif isinstance(treelayer, QgsLayerTreeGroup):
            for layer_inside_group in treelayer.findLayers():
                self.change_mission_markers_visibility(layer_inside_group)
