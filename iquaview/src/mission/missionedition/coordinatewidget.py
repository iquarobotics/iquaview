# Copyright (c) 2023 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Widget to configure a coordinate
"""

import logging
from typing import Optional, Any, Union

from PyQt5.QtCore import pyqtSignal
from PyQt5.QtGui import QValidator
from PyQt5.QtWidgets import QWidget

from iquaview.src.mission.missionedition.heavemodewidget import HeaveModeWidget
from iquaview.src.ui.ui_coordinate_widget import Ui_CoordinateWidget
from iquaview_lib.config import Config
from iquaview_lib.utils.textvalidator import get_color

logger = logging.getLogger(__name__)


class CoordinateWidget(QWidget, Ui_CoordinateWidget):
    apply_signal = pyqtSignal()

    def __init__(self, name, fn, parent=None):
        """ Init of the object CoordinateWidget """
        super().__init__(parent)
        self.setupUi(self)
        self._config = None
        self._heave_mode = None
        self._converter = None

        self.name = name
        self.converter = fn

        self.coordinate_lineEdit.textChanged.connect(self.apply_changes)

    @property
    def config(self) -> Config:
        """
        Return iquaview configuration
        :return: iquaview configuration
        :rtype: Config
        """
        return self._config

    @config.setter
    def config(self, config: Config):
        """
        Set iquaview configuration
        :param config: iquaview configuration
        :type config: Config
        """
        self._config = config

    @property
    def heave_mode(self) -> Config:
        """
        Return heave mode widget
        :return: heave mode
        :rtype: HeaveModeWidget
        """
        return self._heave_mode

    @heave_mode.setter
    def heave_mode(self, heave_mode: HeaveModeWidget):
        """
        Set heave mode widget
        :param config: heave mode
        :type config: HeaveModeWidget
        """
        self._heave_mode = heave_mode

    @property
    def name(self) -> str:
        """
        Return coordinate label name
        :return:
        """
        return self.coordinate_label.text()

    @name.setter
    def name(self, coordinate_name: str):
        """
        Set label name of the coordinate
        :param coordinate_name: name of the coordinate
        :type coordinate_name: str
        """
        self.coordinate_label.setText(coordinate_name)

    @property
    def converter(self) -> Optional[Any]:
        """
        Return the converter method
        """
        return self._converter

    @converter.setter
    def converter(self, function):
        """
        Converter function to set
        :param function: converter function
        """
        self._converter = function

    def clear_coordinate(self):
        """ Clear coordinate lineEdit"""
        self.coordinate_lineEdit.clear()

    def on_text_changed(self):
        state = QValidator.Acceptable
        try:
            if self.coordinate_lineEdit.text() == "":
                state = QValidator.Intermediate
            else:
                self.converter(self.coordinate_lineEdit.text())
        except Exception as e:
            state = QValidator.Invalid
        color = get_color(state)
        self.coordinate_lineEdit.setStyleSheet('QLineEdit { background-color: %s }' % color)
        return state

    def apply_changes(self, string):
        sender = self.sender()
        state = self.on_text_changed()
        if state == QValidator.Acceptable:
            if self.heave_mode is None or self.heave_mode.is_valid_altitude():
                self.apply_signal.emit()
            else:
                sender.undo()

    def is_acceptable(self):
        state = QValidator.Acceptable
        try:
            self.converter(self.coordinate_lineEdit.text())
        except:
            state = QValidator.Invalid
        return state == QValidator.Acceptable

