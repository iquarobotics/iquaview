# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Widget to handle the editing of a clicked waypoint,
 allowing to change coordinates, depth/altitude, controller type
 and actions that should be performed at each waypoint.
"""

import logging
from math import degrees, radians, isclose

from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtGui import QValidator, QPalette
from PyQt5.QtWidgets import QWidget, QDoubleSpinBox

from iquaview.src.mission.missionedition.parkwidget import PARK_DISABLED, PARK_ANCHOR, PARK_HOLONOMIC
from iquaview.src.mission.missiontrack import MissionTrack
from iquaview.src.ui.ui_waypoint_edit import Ui_WaypointEditWidget
from iquaview_lib.cola2api.mission_types import (GOTO_MANEUVER,
                                                 SECTION_MANEUVER,
                                                 PARK_MANEUVER,
                                                 HEAVE_MODE_DEPTH,
                                                 HEAVE_MODE_ALTITUDE,
                                                 HEAVE_MODE_BOTH,
                                                 MissionStep,
                                                 MissionGoto,
                                                 MissionPark,
                                                 MissionSection,
                                                 MissionManeuver)
from iquaview_lib.utils.textvalidator import (validate_custom_double,
                                              evaluate_doublespinbox)

logger = logging.getLogger(__name__)

WHITE = '#ffffff'
YELLOW = '#fff79a'


class WaypointEditWidget(QWidget, Ui_WaypointEditWidget):
    """
    Widget to handle the editing of a clicked waypoint,
    allowing to change coordinates, depth/altitude, controller type
    and actions that should be performed at each waypoint.
    """
    control_state_signal = pyqtSignal(bool)

    def __init__(self, config, mission_track: MissionTrack, vehicle_namespace, multiple_edition=False, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.mission_actions_widget.set_config_and_vehicle_namespace(config, vehicle_namespace)
        self.displaying = False
        self.updating = False
        self.mission_track = mission_track
        self.config = config
        self.vehicle_namespace = vehicle_namespace
        self.multiple_edition = multiple_edition
        self.heave_mode_widget.set_multiple_edition(multiple_edition)
        self.heave_mode_widget.set_parent_widget(self)

        if multiple_edition:
            self.previousWpButton.setEnabled(False)
            self.previousWpButton.hide()
            self.nextWpButton.setEnabled(False)
            self.nextWpButton.hide()

        self.latitude_widget.heave_mode = self.heave_mode_widget
        self.longitude_widget.heave_mode = self.heave_mode_widget
        self.latitude_widget.config = self.config
        self.longitude_widget.config = self.config

        self.show_step = -1
        self.step_list = []
        self.m_ctrl = False

        self.heave_mode_widget.on_heave_mode_box_changed(0)
        self.park_widget.on_park_state_box_changed(0)

        if not self.multiple_edition:
            self.load_next_wp()
            self.mission_track.mission_changed.connect(self.show_mission_step)

        self.set_tab_order()

        self.delete_pushButton.clicked.connect(self.on_click_remove)
        self.previousWpButton.clicked.connect(self.load_previous_wp)
        self.nextWpButton.clicked.connect(self.load_next_wp)

    def connect_signals(self):
        """ Connect widget signals to specific slots."""
        self.latitude_widget.apply_signal.connect(self.apply_changes)
        self.longitude_widget.apply_signal.connect(self.apply_changes)
        self.park_widget.apply_signal.connect(self.apply_changes)
        self.no_altitude_widget.apply_signal.connect(self.apply_changes)
        self.mission_actions_widget.waypoint_actions_changed.connect(self.update_actions_in_mission)
        self.heave_mode_widget.heave_mode_changed_signal.connect(self.apply_changes)
        self.park_widget.parktime_doubleSpinBox.valueChanged.connect(self.on_spinbox_changed)
        self.park_widget.park_yaw_doubleSpinBox.valueChanged.connect(self.on_spinbox_changed)
        self.speed_widget.speed_doubleSpinBox.valueChanged.connect(self.on_spinbox_changed)
        self.park_widget.tolerance_widget.tolerance_xy_doubleSpinBox.valueChanged.connect(self.on_spinbox_changed)
        self.park_widget.parktime_doubleSpinBox.editingFinished.connect(self.on_spinbox_changed)
        self.park_widget.park_yaw_doubleSpinBox.editingFinished.connect(self.on_spinbox_changed)
        self.speed_widget.speed_doubleSpinBox.editingFinished.connect(self.on_spinbox_changed)
        self.park_widget.tolerance_widget.tolerance_xy_doubleSpinBox.editingFinished.connect(self.on_spinbox_changed)

    def disconnect_signals(self):
        """ Disconnects previously established connections between widget signals and slots."""
        try:
            self.latitude_widget.apply_signal.disconnect(self.apply_changes)
            self.longitude_widget.apply_signal.disconnect(self.apply_changes)
            self.park_widget.apply_signal.disconnect(self.apply_changes)
            self.no_altitude_widget.apply_signal.disconnect(self.apply_changes)
            self.mission_actions_widget.waypoint_actions_changed.disconnect(self.update_actions_in_mission)
            self.heave_mode_widget.heave_mode_changed_signal.disconnect(self.apply_changes)
            self.park_widget.parktime_doubleSpinBox.valueChanged.disconnect(self.on_spinbox_changed)
            self.park_widget.park_yaw_doubleSpinBox.valueChanged.disconnect(self.on_spinbox_changed)
            self.speed_widget.speed_doubleSpinBox.valueChanged.disconnect(self.on_spinbox_changed)
            self.park_widget.tolerance_widget.tolerance_xy_doubleSpinBox.valueChanged.disconnect(
                self.on_spinbox_changed)
            self.park_widget.parktime_doubleSpinBox.editingFinished.disconnect(self.on_spinbox_changed)
            self.park_widget.park_yaw_doubleSpinBox.editingFinished.disconnect(self.on_spinbox_changed)
            self.speed_widget.speed_doubleSpinBox.editingFinished.disconnect(self.on_spinbox_changed)
            self.park_widget.tolerance_widget.tolerance_xy_doubleSpinBox.editingFinished.disconnect(
                self.on_spinbox_changed)
        except:
            logger.info("no connected to signal")

    def set_tab_order(self):
        """ Set tab order"""
        self.setTabOrder(self.previousWpButton, self.nextWpButton)
        self.setTabOrder(self.nextWpButton, self.latitude_widget.coordinate_lineEdit)
        self.setTabOrder(self.latitude_widget.coordinate_lineEdit, self.longitude_widget.coordinate_lineEdit)
        self.setTabOrder(self.longitude_widget.coordinate_lineEdit, self.heave_mode_widget.heave_mode_comboBox)
        self.setTabOrder(self.heave_mode_widget.heave_mode_comboBox, self.heave_mode_widget.depth_doubleSpinBox)
        self.setTabOrder(self.heave_mode_widget.depth_doubleSpinBox, self.heave_mode_widget.altitude_doubleSpinBox)
        self.setTabOrder(self.heave_mode_widget.altitude_doubleSpinBox, self.no_altitude_widget.no_altitude_comboBox)
        self.setTabOrder(self.no_altitude_widget.no_altitude_comboBox, self.park_widget.park_state_comboBox)
        self.setTabOrder(self.park_widget.park_state_comboBox, self.park_widget.parktime_doubleSpinBox)
        self.setTabOrder(self.park_widget.parktime_doubleSpinBox, self.park_widget.park_yaw_doubleSpinBox)
        self.setTabOrder(self.park_widget.park_yaw_doubleSpinBox,
                         self.park_widget.tolerance_widget.tolerance_xy_doubleSpinBox)
        self.setTabOrder(self.park_widget.tolerance_widget.tolerance_xy_doubleSpinBox,
                         self.speed_widget.speed_doubleSpinBox)
        self.setTabOrder(self.speed_widget.speed_doubleSpinBox, self.mission_actions_widget.addAction_toolButton)
        self.setTabOrder(self.mission_actions_widget.addAction_toolButton,
                         self.mission_actions_widget.removeAction_toolButton)
        self.setTabOrder(self.mission_actions_widget.removeAction_toolButton,
                         self.mission_actions_widget.action_listWidget)
        self.setTabOrder(self.mission_actions_widget.action_listWidget, self.delete_pushButton)

    def get_step_list(self):
        """Return the current step list"""
        return self.step_list

    def on_spinbox_changed(self):
        """Slot called when a spinbox is changed"""
        sender = self.sender()
        state = evaluate_doublespinbox(sender, str(sender.text()))
        if state == QValidator.Acceptable:
            self.apply_changes()

    def update_show_multiple_features(self):
        """ Update the display of the multiple edition features """
        step_list = self.step_list
        self.show_empty_mission_step()
        self.show_multiple_features(step_list)

    def is_custom_double_acceptable(self, text):
        """ Check if the text is a valid custom double """
        return validate_custom_double(text) == QValidator.Acceptable

    def check_and_set_spinbox_value(self, new_value: bool, spinbox: QDoubleSpinBox, value):
        """
        If new_value is True set value on spinbox, otherwise set empty value
        :param new_value: If new_value is True set value on spinbox, otherwise set empty value
        :type new_value: bool
        :param spinbox: the spinbox to be edited
        :type spinbox: QDoubleSpinBox
        :param value: value to set
        :type value: str
        """
        if new_value:
            if spinbox.value() != value or spinbox.text() == "":
                spinbox.setValue(value)
        else:
            spinbox.clear()
        evaluate_doublespinbox(spinbox, spinbox.text())

    def on_click_remove(self):
        """ Slot called when the remove button is clicked """
        if self.multiple_edition:
            for step in reversed(self.step_list):
                self.mission_track.remove_step(step)
            self.show_empty_mission_step()

        else:
            if self.show_step >= 0:
                self.mission_track.remove_step(self.show_step)

    def get_latitude(self, step: int):
        """
        Get latitude
        :param step: mission step
        :return: if the widget's latitude value is valid it returns it, otherwise it returns the saved value.
        """
        if self.latitude_widget.is_acceptable():
            latitude = float(self.latitude_widget.get_latitude())
        else:
            latitude = self.mission_track.get_step(step).get_maneuver().final_latitude

        return latitude

    def get_longitude(self, step: int):
        """
        Get longitude
        :param step: mission step
        :return: if the widget's longitude value is valid it returns it, otherwise it returns the saved value.
        """
        if self.longitude_widget.is_acceptable():
            longitude = float(self.longitude_widget.get_longitude())
        else:
            longitude = self.mission_track.get_step(step).get_maneuver().final_longitude

        return longitude

    def get_depth(self, step: int):
        """
        Get depth
        :param step: mission step
        :return: if the widget's heave mode final depth is valid it returns it, otherwise it returns the saved value.
        """
        if ((self.heave_mode_widget.is_heave_mode_depth()
             or self.heave_mode_widget.is_heave_mode_both())
                and self.is_custom_double_acceptable(str(self.heave_mode_widget.depth_doubleSpinBox.text()))):
            depth = self.heave_mode_widget.get_depth()
        else:
            depth = self.mission_track.get_step(step).get_maneuver().final_depth

        return depth

    def get_altitude(self, step: int):
        """
        Get altitude
        :param step: mission step
        :return: if the widget's heave mode final altitude value is valid it returns it,
        otherwise it returns the saved value.
        """
        if ((self.heave_mode_widget.is_heave_mode_altitude()
             or self.heave_mode_widget.is_heave_mode_both())
                and self.is_custom_double_acceptable(
                    str(self.heave_mode_widget.altitude_doubleSpinBox.text()))):
            altitude = self.heave_mode_widget.get_altitude()
        else:
            altitude = self.mission_track.get_step(step).get_maneuver().final_altitude

        return altitude

    def get_tolerance(self, step: int):
        """
        Get tolerance
        :param step: mission step
        :return: if the tolerance value from spinbox is valid it returns it, otherwise it returns the saved value.
        """
        if self.is_custom_double_acceptable(
                str(self.park_widget.tolerance_widget.tolerance_xy_doubleSpinBox.text())):
            tolerance = self.park_widget.tolerance_widget.tolerance_xy_doubleSpinBox.value()
        else:
            tolerance = self.mission_track.get_step(step).get_maneuver().tolerance_xy

        return tolerance

    def get_heave_mode(self, step: int):
        """
        Get have mode
        :param step: mission step
        :return: if the widget's heave mode value is valid it returns it, otherwise it returns the saved value.
        """
        if self.heave_mode_widget.heave_mode_comboBox.count() < 4:
            heave_mode = self.heave_mode_widget.get_heave_mode()
        else:
            heave_mode = self.mission_track.get_step(step).get_maneuver().heave_mode

        return heave_mode

    def get_no_altitude(self, step: int):
        """
        Get no altitude
        :param step: mission step
        :return: if the widget's no altitude goes up value is valid it returns it, otherwise it returns the saved value.
        """
        if self.no_altitude_widget.no_altitude_comboBox.count() < 3:
            no_altitude = self.no_altitude_widget.no_altitude_goes_up()
        else:
            no_altitude = self.mission_track.get_step(step).get_maneuver().no_altitude_goes_up

        return no_altitude

    def get_speed(self, step: int):
        """
        Get speed
        :param step: mission step
        :return: if the widget's speed value is valid it returns it, otherwise it returns the saved value.
        """
        if self.is_custom_double_acceptable(str(self.speed_widget.speed_doubleSpinBox.text())):
            speed = float(self.speed_widget.speed_doubleSpinBox.value())
        else:
            speed = self.mission_track.get_step(step).get_maneuver().surge_velocity

        return speed

    def get_park_time(self, step: int):
        """
        Get park time
        :param step: mission step
        :return: if the widget's park time value is valid it returns it, otherwise it returns the saved value.
        """
        if ((self.park_widget.park_state_comboBox.currentIndex() == PARK_ANCHOR
             or self.park_widget.park_state_comboBox.currentIndex() == PARK_HOLONOMIC)
                and self.is_custom_double_acceptable(str(self.park_widget.parktime_doubleSpinBox.text()))):
            park_time = float(self.park_widget.parktime_doubleSpinBox.value())

        elif self.mission_track.get_step(step).get_maneuver().get_maneuver_type() == PARK_MANEUVER:
            park_time = self.mission_track.get_step(step).get_maneuver().time
        else:
            park_time = 10

        return park_time

    def get_yaw(self, step: int):
        """
        Get yaw
        :param step: mission step
        :return: if the widget's yaw and use_yaw value are valid it returns it, otherwise it returns the saved value.
        """
        if (self.park_widget.park_state_comboBox.currentIndex() == PARK_HOLONOMIC
                and self.is_custom_double_acceptable(str(self.park_widget.park_yaw_doubleSpinBox.text()))):
            yaw = radians(float(self.park_widget.park_yaw_doubleSpinBox.value()))
            use_yaw = True
        elif (self.park_widget.park_state_comboBox.count() == 4
              and self.mission_track.get_step(step).get_maneuver().get_maneuver_type() == PARK_MANEUVER
              and self.mission_track.get_step(step).get_maneuver().use_yaw is True):
            yaw = self.mission_track.get_step(step).get_maneuver().final_yaw
            use_yaw = True
        else:
            yaw = 0
            use_yaw = False

        return yaw, use_yaw

    # on click save current wp
    def apply_changes(self):
        """ Apply changes to the mission track """
        if not self.heave_mode_widget.is_valid_altitude():
            return

        if not self.updating:
            mission_steps = []
            if not self.multiple_edition:
                self.step_list = []
                if self.show_step >= 0:
                    self.step_list.append(self.show_step)
            for step in reversed(self.step_list):

                logger.debug(
                    f"Save: Mission has {self.mission_track.get_mission_length()} steps. "
                    f"Im going to update step {step}")

                latitude = self.get_latitude(step)
                longitude = self.get_longitude(step)
                depth = self.get_depth(step)
                altitude = self.get_altitude(step)
                tolerance = self.get_tolerance(step)
                heave_mode = self.get_heave_mode(step)
                no_altitude = self.get_no_altitude(step)
                speed = self.get_speed(step)
                park_time = self.get_park_time(step)
                yaw, use_yaw = self.get_yaw(step)

                mission_step = MissionStep()
                if ((self.park_widget.park_state_comboBox.currentIndex() == PARK_ANCHOR
                     or self.park_widget.park_state_comboBox.currentIndex() == PARK_HOLONOMIC)
                        or (self.park_widget.park_state_comboBox.count() == 4
                            and self.mission_track.get_step(step).get_maneuver().get_maneuver_type() == PARK_MANEUVER)):
                    logger.debug("park")
                    maneuver = MissionPark(latitude, longitude, depth, altitude,
                                           yaw, use_yaw,
                                           heave_mode, speed, park_time, no_altitude)

                elif (step == 0 and self.park_widget.park_state_comboBox.currentIndex() == PARK_DISABLED
                      or (self.park_widget.park_state_comboBox.count() == 4
                          and self.mission_track.get_step(step).get_maneuver().get_maneuver_type() == GOTO_MANEUVER)):
                    logger.debug("goto")
                    maneuver = MissionGoto(latitude, longitude, depth, altitude, heave_mode, speed, tolerance,
                                           no_altitude)

                else:  # self.park_state_comboBox.currentIndex() == PARK_DISABLED:
                    logger.debug("section")
                    # if previous is in the list, get z from list, otherwise get z from track
                    previous_latitude = self.mission_track.get_step(step - 1).get_maneuver().final_latitude
                    previous_longitude = self.mission_track.get_step(step - 1).get_maneuver().final_longitude
                    if step - 1 in self.step_list:
                        previous_depth = self.heave_mode_widget.get_depth()
                    else:
                        previous_depth = self.mission_track.get_step(step - 1).get_maneuver().final_depth

                    maneuver = MissionSection(previous_latitude, previous_longitude, previous_depth,
                                              latitude, longitude, depth, altitude, heave_mode,
                                              speed, tolerance, no_altitude)

                if ((step + 1) <= (self.mission_track.get_mission_length() - 1)
                        and self.mission_track.get_step(
                            step + 1).get_maneuver().get_maneuver_type() == SECTION_MANEUVER):
                    self.mission_track.get_step(step + 1).get_maneuver().initial_depth = depth
                    self.mission_track.get_step(step + 1).get_maneuver().initial_latitude = latitude
                    self.mission_track.get_step(step + 1).get_maneuver().initial_longitude = longitude

                # copy actions to updated step
                mission_step.add_maneuver(maneuver)
                # checks whether the waypoints have the same actions or not, by checking the color of the list
                current_color = self.mission_actions_widget.action_listWidget.palette().color(
                    QPalette.Background).name()
                if WHITE == current_color:
                    mission_step.actions = self.mission_actions_widget.get_actions()
                else:
                    mission_step.actions = self.mission_track.get_step(step).get_actions()
                mission_steps.append(mission_step)
            self.mission_track.update_steps(list(reversed(self.step_list)), mission_steps)

    def update_actions_in_mission(self):
        """
        Sort actions according list widget order
        """
        actions = self.mission_actions_widget.get_actions()
        for wp in self.step_list:
            self.mission_track.get_step(wp).set_actions(actions.copy())
        if self.multiple_edition:
            # There has been some edit, now all steps have the same actions. Set listwidget background white
            self.mission_actions_widget.setStyleSheet(f"QListWidget {{ background-color: '{WHITE}'; }}")
        self.apply_changes()

    def selected_waypoint(self, wp):
        """ clear waypoint widget and show waypoint mission step"""
        if not self.multiple_edition:
            self.show_empty_mission_step()
            self.show_mission_step(wp)

    def show_mission_step(self, wp):
        """ Show the mission step"""
        if self.displaying:
            return  # already displaying

        self.displaying = True
        logger.debug(f"Showing mission with len: {self.mission_track.get_mission_length()},  step: {wp} ")
        if wp == -1:
            self.show_empty_mission_step()
            self.displaying = False
            return

        self.disconnect_signals()
        self.show_step = wp
        self.TitleWaypointLabel.setText(f"Waypoint {self.show_step + 1}")
        mission_step = self.mission_track.get_step(self.show_step)
        maneuver = mission_step.get_maneuver()
        if maneuver.get_maneuver_type() == PARK_MANEUVER:  # for Park
            if maneuver.use_yaw:
                self.park_widget.park_state_comboBox.setCurrentIndex(2)
                self.check_and_set_spinbox_value(True,
                                                 self.park_widget.park_yaw_doubleSpinBox,
                                                 degrees(maneuver.final_yaw))
            else:
                self.park_widget.park_state_comboBox.setCurrentIndex(1)
            self.check_and_set_spinbox_value(True,
                                             self.park_widget.parktime_doubleSpinBox,
                                             maneuver.time)
        else:
            self.park_widget.park_state_comboBox.setCurrentIndex(0)
        self.heave_mode_widget.heave_mode_comboBox.setCurrentIndex(maneuver.heave_mode)

        # latitude
        self.check_and_set_spinbox_value(True,
                                         self.speed_widget.speed_doubleSpinBox,
                                         maneuver.surge_velocity)
        self.latitude_widget.set_latitude(maneuver.final_latitude)
        self.longitude_widget.set_longitude(maneuver.final_longitude)
        self.check_and_set_spinbox_value(True,
                                         self.heave_mode_widget.depth_doubleSpinBox,
                                         maneuver.final_depth)
        self.check_and_set_spinbox_value(True,
                                         self.heave_mode_widget.altitude_doubleSpinBox,
                                         maneuver.final_altitude)
        self.check_and_set_spinbox_value(True,
                                         self.park_widget.tolerance_widget.tolerance_xy_doubleSpinBox,
                                         maneuver.tolerance_xy)

        goes_up = 0 if maneuver.no_altitude_goes_up else 1
        self.no_altitude_widget.no_altitude_comboBox.setCurrentIndex(goes_up)

        self.mission_actions_widget.set_actions(mission_step.get_actions())
        # set listwidget background default white
        self.mission_actions_widget.setStyleSheet(f"QListWidget {{ background-color: '{WHITE}'; }}")
        self.apply_changes()

        self.displaying = False
        self.connect_signals()

    def show_empty_mission_step(self):
        """ Show empty mission step """
        self.show_step = -1
        self.step_list = []
        self.TitleWaypointLabel.setText("Mission is empty")

        self.heave_mode_widget.heave_mode_comboBox.setCurrentIndex(HEAVE_MODE_DEPTH)
        self.park_widget.park_state_comboBox.setCurrentIndex(0)
        self.no_altitude_widget.no_altitude_comboBox.setCurrentIndex(0)

        self.speed_widget.speed_doubleSpinBox.clear()

        self.heave_mode_widget.clear()
        self.park_widget.tolerance_widget.tolerance_xy_doubleSpinBox.clear()

        evaluate_doublespinbox(self.speed_widget.speed_doubleSpinBox, "")
        evaluate_doublespinbox(self.heave_mode_widget.depth_doubleSpinBox, "")
        evaluate_doublespinbox(self.heave_mode_widget.altitude_doubleSpinBox, "")
        evaluate_doublespinbox(self.park_widget.tolerance_widget.tolerance_xy_doubleSpinBox, "")

        self.latitude_widget.clear_coordinate()
        self.longitude_widget.clear_coordinate()

        self.mission_actions_widget.clear_actions()
        # set listwidget background yellow
        self.mission_actions_widget.setStyleSheet(f"QListWidget {{ background-color: '{YELLOW}'; }}")

    def show_multiple_features(self, step_list):
        """
        Show multiple features
        :param step_list: step list
        :type step_list: list
        """
        if self.displaying:
            return  # already displaying
        if not step_list:
            self.show_empty_mission_step()
            return  # empty list

        logger.debug(f"step list: {str(step_list)}")
        step_list.sort()
        self.step_list = step_list
        # if only have one step
        if len(step_list) == 1:
            self.show_mission_step(step_list[0])
        # more than one step
        else:
            self.displaying = True
            self.disconnect_signals()
            # set title
            title = self.create_title(step_list)
            self.TitleWaypointLabel.setText(title)

            same_lat = True
            same_lon = True
            same_heave_mode = True
            same_depth = True
            same_altitude = True
            same_no_altitude = True
            same_park_time = True
            same_park_yaw = True
            same_speed = True
            same_tolerance_xy = True
            same_maneuver = True
            same_action = True
            same_actions = []

            # every list step
            for i, show_step in enumerate(step_list):
                self.show_step = show_step
                mission_step = self.mission_track.get_step(self.show_step)
                maneuver = mission_step.get_maneuver()

                for j in range(i + 1, len(step_list)):
                    wp = step_list[j]
                    mission_step_two = self.mission_track.get_step(wp)
                    maneuver_two = mission_step_two.get_maneuver()

                    latitude = maneuver.final_latitude
                    longitude = maneuver.final_longitude
                    maneuver_type = maneuver.get_maneuver_type()

                    latitude_two = maneuver_two.final_latitude
                    longitude_two = maneuver_two.final_longitude
                    maneuver_type_two = maneuver_two.get_maneuver_type()

                    same_lat = same_lat and isclose(float(latitude), float(latitude_two))
                    same_lon = same_lon and isclose(float(longitude), float(longitude_two))
                    same_heave_mode = same_heave_mode and maneuver.heave_mode == maneuver_two.heave_mode
                    same_no_altitude = same_no_altitude and (maneuver.no_altitude_goes_up
                                                             == maneuver_two.no_altitude_goes_up)
                    same_speed = same_speed and isclose(float(maneuver.surge_velocity),
                                                        float(maneuver_two.surge_velocity))
                    same_tolerance_xy = same_tolerance_xy and isclose(float(maneuver.tolerance_xy),
                                                                      float(maneuver_two.tolerance_xy))

                    if ((maneuver.heave_mode in (HEAVE_MODE_DEPTH, HEAVE_MODE_BOTH))
                            and (maneuver_two.heave_mode in (HEAVE_MODE_DEPTH, HEAVE_MODE_BOTH))):
                        same_depth = same_depth and isclose(float(maneuver.final_depth),
                                                            float(maneuver_two.final_depth))

                    if ((maneuver.heave_mode in (HEAVE_MODE_ALTITUDE, HEAVE_MODE_BOTH))
                            and (maneuver_two.heave_mode in (HEAVE_MODE_ALTITUDE, HEAVE_MODE_BOTH))):
                        same_altitude = same_altitude and isclose(float(maneuver.final_altitude),
                                                                  float(maneuver_two.final_altitude))

                    if maneuver_type == PARK_MANEUVER and maneuver_type_two == PARK_MANEUVER:
                        # maneuver is PARK
                        same_park_time = same_park_time and isclose(float(maneuver.time), float(maneuver_two.time))

                        if maneuver.use_yaw and maneuver_two.use_yaw:
                            same_park_yaw = same_park_yaw and isclose(float(maneuver.final_yaw),
                                                                      float(maneuver_two.final_yaw))

                    elif PARK_MANEUVER in (maneuver_type, maneuver_type_two):
                        same_maneuver = False

                    if not same_actions:
                        for action in mission_step.actions:
                            same_actions.append(action)

                    if mission_step.actions == mission_step_two.actions:
                        same_actions = mission_step.actions.copy()
                    else:
                        same_action = False
                        same_actions = []

            self.show_step = step_list[0]
            mission_step = self.mission_track.get_step(self.show_step)
            maneuver = mission_step.get_maneuver()

            self.updating = True
            self.set_no_altitude_goes_up_widget(same_no_altitude, maneuver.no_altitude_goes_up)
            self.set_park_widget(same_maneuver, same_park_time, same_park_yaw, maneuver, maneuver_two)
            self.set_heave_mode_widget(same_heave_mode, same_depth, same_altitude, maneuver)

            if same_lat:
                self.latitude_widget.set_latitude(str(latitude))
            else:
                self.latitude_widget.clear_coordinate()

            if same_lon:
                self.longitude_widget.set_longitude(str(longitude))
            else:
                self.longitude_widget.clear_coordinate()

            self.check_and_set_spinbox_value(same_depth,
                                             self.heave_mode_widget.depth_doubleSpinBox,
                                             maneuver.final_depth)

            self.check_and_set_spinbox_value(same_altitude,
                                             self.heave_mode_widget.altitude_doubleSpinBox,
                                             maneuver.final_altitude)

            self.check_and_set_spinbox_value(same_speed,
                                             self.speed_widget.speed_doubleSpinBox,
                                             maneuver.surge_velocity)

            self.check_and_set_spinbox_value(same_tolerance_xy,
                                             self.park_widget.tolerance_widget.tolerance_xy_doubleSpinBox,
                                             maneuver.tolerance_xy)

            if not same_action:
                self.mission_actions_widget.clear_actions()
                # set listwidget background yellow
                self.mission_actions_widget.setStyleSheet(f"QListWidget {{ background-color: '{YELLOW}'; }}")
            else:
                self.mission_actions_widget.set_actions(same_actions)
                # set listwidget background white
                self.mission_actions_widget.setStyleSheet(f"QListWidget {{ background-color: '{WHITE}'; }}")
            self.updating = False
            self.apply_changes()
            self.connect_signals()

        self.displaying = False

    def set_no_altitude_goes_up_widget(self, same_no_altitude: bool, no_altitude_goes_up: bool):
        """
        Set the state of the no_altitude_goes_up widget.
        :param same_no_altitude: True if the no altitude goes up is the same for all maneuvers.
        :type same_no_altitude: bool
        :param no_altitude_goes_up: True if the no altitude goes up is True for all maneuvers.
        :type no_altitude_goes_up: bool
        """
        if same_no_altitude:
            if no_altitude_goes_up:
                self.no_altitude_widget.no_altitude_comboBox.setCurrentIndex(0)
            else:
                self.no_altitude_widget.no_altitude_comboBox.setCurrentIndex(1)
        elif self.no_altitude_widget.no_altitude_comboBox.count() < 3:
            self.no_altitude_widget.no_altitude_comboBox.addItem("-")
            self.no_altitude_widget.no_altitude_comboBox.setCurrentIndex(2)

    def set_park_widget(self,
                        same_maneuver: bool,
                        same_park_time: bool,
                        same_park_yaw: bool,
                        maneuver: MissionManeuver,
                        maneuver_two: MissionManeuver):
        """
        Set the state of the park widget.
        :param same_maneuver:  True if the maneuver is the same for all maneuvers.
        :type same_maneuver: bool
        :param same_park_time: True if the park time is the same for all maneuvers.
        :type same_park_time: bool
        :param same_park_yaw: True if the park yaw is the same for all maneuvers.
        :type same_park_yaw: bool
        :param maneuver: The first maneuver.
        :type maneuver: MissionManeuver
        :param maneuver_two: The second maneuver.
        :type maneuver_two: MissionManeuver
        """
        if same_maneuver:
            if maneuver.get_maneuver_type() == GOTO_MANEUVER:  # for Waypoint
                self.park_widget.park_state_comboBox.setCurrentIndex(0)

            if maneuver.get_maneuver_type() == SECTION_MANEUVER:  # for a Section
                self.park_widget.park_state_comboBox.setCurrentIndex(0)

            elif maneuver.get_maneuver_type() == PARK_MANEUVER:  # for Park

                if not maneuver.use_yaw and not maneuver_two.use_yaw:
                    self.park_widget.park_state_comboBox.setCurrentIndex(1)
                elif maneuver.use_yaw and maneuver_two.use_yaw:
                    self.park_widget.park_state_comboBox.setCurrentIndex(2)

                if same_park_time:
                    self.check_and_set_spinbox_value(same_park_time,
                                                     self.park_widget.parktime_doubleSpinBox,
                                                     maneuver.time)

                if same_park_yaw:
                    self.check_and_set_spinbox_value(same_park_yaw,
                                                     self.park_widget.park_yaw_doubleSpinBox,
                                                     degrees(maneuver.final_yaw))

                if ((maneuver.use_yaw and not maneuver_two.use_yaw)
                        or (not maneuver.use_yaw and maneuver_two.use_yaw)):
                    if self.park_widget.park_state_comboBox.count() < 4:
                        self.park_widget.park_state_comboBox.addItem("-")
                        self.park_widget.park_state_comboBox.setCurrentIndex(3)

        elif self.park_widget.park_state_comboBox.count() < 4:
            self.park_widget.park_state_comboBox.addItem("-")
            self.park_widget.park_state_comboBox.setCurrentIndex(3)

    def set_heave_mode_widget(self,
                              same_heave_mode: bool,
                              same_depth: bool,
                              same_altitude: bool,
                              maneuver: MissionManeuver):
        """
        Set the state of the heave mode widget.
        :param same_heave_mode: True if the heave mode is the same for all maneuvers.
        :type same_heave_mode: bool
        :param same_depth: True if the depth is the same for all maneuvers.
        :type same_depth: bool
        :param same_altitude: True if the altitude is the same for all maneuvers.
        :type same_altitude: bool
        :param maneuver: The first maneuver.
        :type maneuver: MissionManeuver
        """
        if same_heave_mode:
            if maneuver.heave_mode == HEAVE_MODE_DEPTH:
                self.check_and_set_spinbox_value(same_depth,
                                                 self.heave_mode_widget.depth_doubleSpinBox,
                                                 maneuver.final_depth)
                self.heave_mode_widget.heave_mode_comboBox.setCurrentIndex(0)

            elif maneuver.heave_mode == HEAVE_MODE_ALTITUDE:

                self.check_and_set_spinbox_value(same_altitude,
                                                 self.heave_mode_widget.altitude_doubleSpinBox,
                                                 maneuver.final_altitude)

                self.heave_mode_widget.heave_mode_comboBox.setCurrentIndex(1)

            elif maneuver.heave_mode == HEAVE_MODE_BOTH:

                self.check_and_set_spinbox_value(same_depth,
                                                 self.heave_mode_widget.depth_doubleSpinBox,
                                                 maneuver.final_depth)

                self.check_and_set_spinbox_value(same_altitude,
                                                 self.heave_mode_widget.altitude_doubleSpinBox,
                                                 maneuver.final_altitude)
                self.heave_mode_widget.heave_mode_comboBox.setCurrentIndex(2)

        elif self.heave_mode_widget.heave_mode_comboBox.count() < 4:
            self.heave_mode_widget.heave_mode_comboBox.addItem("-")
            self.heave_mode_widget.heave_mode_comboBox.setCurrentIndex(3)

    def load_previous_wp(self):
        """ Load previous waypoint """
        step = self.show_step
        self.show_empty_mission_step()
        length = self.mission_track.get_mission_length()
        if length > 0:
            self.show_step = step
            if self.show_step > 0:
                # switch to previous wp
                self.show_mission_step(self.show_step - 1)
            else:
                # if current wp is 0 switch to last wp
                last = length - 1
                self.show_mission_step(last)

    def load_next_wp(self):
        """ Load next waypoint """
        step = self.show_step
        self.show_empty_mission_step()
        length = self.mission_track.get_mission_length()
        if length > 0:
            self.show_step = step
            if self.show_step < (length - 1):
                # switch to next wp
                self.show_mission_step(self.show_step + 1)
            else:
                # if current wp is the last switch to first wp
                self.show_mission_step(0)

    def create_title(self, step_list):
        """
        Creates a title with a step list and returns it. In case of the title not fitting in the available space,
        it gets shortened and a tooltip is created to show all steps in step list
        """
        # Check available space
        max_width = self.scrollArea.width() - 50
        ppchar = 6.75  # pixels per character approx
        limit_achieved = False
        title = "Waypoints: "
        tool_tip_text = ""

        if len(step_list) == 1:
            title = f"Waypoint {step_list[0] + 1}"
        else:
            count = 0

            # write wp selected
            for wp in step_list:
                if not limit_achieved:
                    s = str(wp + 1)
                    if wp != step_list[-1]:
                        s += ","
                    # limit maximum length of the title
                    if not limit_achieved and (len(title) * ppchar + len(s) * ppchar) > max_width:
                        limit_achieved = True

                    title += s

                count += 1
                #  Create ToolTip text
                tool_tip_text += str(wp + 1)
                if wp != step_list[-1]:
                    tool_tip_text += ", "
                if count % 10 == 0 and wp < len(step_list) - 1:
                    tool_tip_text += "\n"

        if limit_achieved:  # Rewrite title accounting for less space due to the " ...+xx" at the end
            max_width = max_width - 75
            title = "Waypoints: "
            count = 0
            for wp in step_list:
                s = str(wp + 1) + ","
                if (len(title) * ppchar + len(s) * ppchar) > max_width:
                    title += f" ...+{str(len(step_list) - count)} points"
                    break

                title += s
                count += 1

        self.TitleWaypointLabel.setToolTip(tool_tip_text)

        return title

    def keyPressEvent(self, event):
        """ Key press event """
        if event.key() == Qt.Key_Control:
            self.m_ctrl = True
            self.control_state_signal.emit(True)

    def keyReleaseEvent(self, event):
        """ Key release event """
        if event.key() == Qt.Key_Control:
            self.m_ctrl = False
            self.control_state_signal.emit(False)

    def resizeEvent(self, event):
        """ Resize event """
        if self.multiple_edition:
            title = self.create_title(self.step_list)
            self.TitleWaypointLabel.setText(title)
        super().resizeEvent(event)
