# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
"""
 Widget to configure speed
"""

import logging

from PyQt5.QtWidgets import QWidget
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtGui import QValidator

from iquaview_lib.utils.textvalidator import validate_custom_double
from iquaview.src.ui.ui_speed_widget import Ui_SpeedWidget

logger = logging.getLogger(__name__)


class SpeedWidget(QWidget, Ui_SpeedWidget):
    apply_signal = pyqtSignal()

    def __init__(self, parent=None):
        """ Init of the object SpeedWidget """
        super().__init__(parent)
        self.setupUi(self)

    def is_acceptable(self):
        """
        check if the text of the speed double spin box is acceptable
        :return: return True if the value is acceptable, otherwise False
        """
        return validate_custom_double(self.speed_doubleSpinBox.text()) == QValidator.Acceptable
