# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
"""
 Widget to configure no altitude mode
"""

import logging

from PyQt5.QtWidgets import QWidget
from PyQt5.QtCore import pyqtSignal

from iquaview.src.ui.ui_no_altitude_widget import Ui_NoAltitudeWidget

logger = logging.getLogger(__name__)


class NoAltitudeWidget(QWidget, Ui_NoAltitudeWidget):
    apply_signal = pyqtSignal()

    def __init__(self, parent=None):
        """ Init of the object NoAltitudeWidget """
        super().__init__(parent)
        self.setupUi(self)

        self.no_altitude_comboBox.currentIndexChanged.connect(self.on_no_altitude_box_changed)

    def no_altitude_goes_up(self):
        return self.no_altitude_comboBox.currentText() == "Go up"

    def on_no_altitude_box_changed(self, i):
        """
        Apply changes
        :param i: no altitude combobox index
        :type i: int
        """
        if i < 2:
            # remove '-' item
            if self.no_altitude_comboBox.count() == 3:
                self.no_altitude_comboBox.removeItem(2)

            self.apply_signal.emit()
