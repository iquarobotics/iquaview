# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
"""
 Widget to configure heave mode
"""

import logging

from PyQt5.QtCore import pyqtSignal
from PyQt5.QtGui import QValidator
from PyQt5.QtWidgets import QWidget, QMessageBox

from iquaview.src.ui.ui_heave_mode import Ui_HeaveModeWidget
from iquaview_lib.cola2api.mission_types import (HEAVE_MODE_DEPTH,
                                                 HEAVE_MODE_ALTITUDE,
                                                 HEAVE_MODE_BOTH)
from iquaview_lib.utils.textvalidator import evaluate_doublespinbox

logger = logging.getLogger(__name__)


class HeaveModeWidget(QWidget, Ui_HeaveModeWidget):
    heave_mode_changed_signal = pyqtSignal()

    def __init__(self, parent=None):
        """ Init of the object HeaveModeWidget """
        super().__init__(parent)
        self.setupUi(self)
        self.last_valid_altitude = self.get_altitude()
        self.multiple_edition: bool = False
        self.parent_widget = None
        self.heave_mode_comboBox.currentIndexChanged.connect(self.on_heave_mode_box_changed)
        self.altitude_doubleSpinBox.valueChanged.connect(self.on_spinbox_changed)
        self.depth_doubleSpinBox.valueChanged.connect(self.on_spinbox_changed)

        self.altitude_doubleSpinBox.editingFinished.connect(self.on_spinbox_changed)
        self.depth_doubleSpinBox.editingFinished.connect(self.on_spinbox_changed)

        self.on_heave_mode_box_changed(0)

    def on_spinbox_changed(self, value=0):
        sender = self.sender()
        state = evaluate_doublespinbox(sender, str(sender.text()))

        if state == QValidator.Acceptable:
            if sender == self.altitude_doubleSpinBox:
                if (self.altitude_doubleSpinBox.value() == 0.0
                        and self.get_heave_mode() != HEAVE_MODE_DEPTH):
                    self.set_last_valid_altitude()
                    reply = QMessageBox.warning(None, "Mission Error",
                                                "Altitude can not be 0")

            if self.is_valid_altitude():
                self.heave_mode_changed_signal.emit()

    def on_heave_mode_box_changed(self, i):
        if i < 3:
            # remove '-' item
            if self.heave_mode_comboBox.count() == 4:
                self.heave_mode_comboBox.removeItem(3)

            if self.heave_mode_comboBox.currentIndex() == HEAVE_MODE_DEPTH:
                self.depth_label.setVisible(True)
                self.depth_doubleSpinBox.setVisible(True)
                self.altitude_doubleSpinBox.setVisible(False)
                self.altitude_label.setVisible(False)
            elif self.heave_mode_comboBox.currentIndex() == HEAVE_MODE_ALTITUDE:
                self.depth_label.setVisible(False)
                self.depth_doubleSpinBox.setVisible(False)
                self.altitude_doubleSpinBox.setVisible(True)
                self.altitude_label.setVisible(True)
            elif self.heave_mode_comboBox.currentIndex() == HEAVE_MODE_BOTH:
                self.depth_label.setVisible(True)
                self.depth_doubleSpinBox.setVisible(True)
                self.altitude_doubleSpinBox.setVisible(True)
                self.altitude_label.setVisible(True)

            if self.is_valid_altitude():
                self.heave_mode_changed_signal.emit()

        else:
            self.depth_label.setVisible(False)
            self.depth_doubleSpinBox.setVisible(False)
            self.altitude_doubleSpinBox.setVisible(False)
            self.altitude_label.setVisible(False)

    def is_valid_altitude(self):
        """
        Return True if altitude is valid, otherwise False.
        ( Altitude is valid if it is bigger than 0 )
        """
        is_valid = True
        if self.multiple_edition:
            for step in self.parent_widget.get_step_list():
                if ((self.heave_mode_comboBox.currentIndex() == HEAVE_MODE_ALTITUDE
                     or self.heave_mode_comboBox.currentIndex() == HEAVE_MODE_BOTH)
                        and self.altitude_doubleSpinBox.value() and self.altitude_doubleSpinBox.value() == 0.0):
                    self.set_last_valid_altitude()
                    reply = QMessageBox.warning(None, "Mission Error",
                                                "Altitude can not be 0")

                    is_valid = False
                    break

        elif ((self.heave_mode_comboBox.currentIndex() == HEAVE_MODE_ALTITUDE
               or self.heave_mode_comboBox.currentIndex() == HEAVE_MODE_BOTH)
              and (not self.altitude_doubleSpinBox.value()
                   or self.altitude_doubleSpinBox.value() == 0.0)):
            self.set_last_valid_altitude()
            reply = QMessageBox.warning(None, "Mission Error",
                                        "Altitude can not be 0")

            is_valid = False
        else:
            is_valid = True
            self.last_valid_altitude = self.altitude_doubleSpinBox.value()
        return is_valid

    def get_heave_mode(self):
        """ Get current heave mode"""
        return self.heave_mode_comboBox.currentIndex()

    def get_altitude(self):
        """
        Return altitude value
        :return: altitude value
        :rtype: float
        """
        return self.altitude_doubleSpinBox.value()

    def get_depth(self):
        """
        Return depth value
        :return: depth value
        :rtype: float
        """
        return self.depth_doubleSpinBox.value()

    def set_altitude(self, value):
        """
        Set altitude value
        :param value: altitude value
        :type value: float
        """
        self.altitude_doubleSpinBox.setValue(value)

    def set_last_valid_altitude(self):
        """
        Set  last valid altitude value
        """
        self.set_altitude(self.last_valid_altitude)

    def set_multiple_edition(self, is_multiple_edition):
        """ Set multiple edition state"""
        self.multiple_edition = is_multiple_edition

    def set_parent_widget(self, widget):
        self.parent_widget = widget

    def is_heave_mode_depth(self):
        """ Return True if current heave mode is Depth, otherwise False"""
        return self.heave_mode_comboBox.currentIndex() == HEAVE_MODE_DEPTH

    def is_heave_mode_altitude(self):
        """ Return True if current heave mode is Altitude, otherwise False"""
        return self.heave_mode_comboBox.currentIndex() == HEAVE_MODE_ALTITUDE

    def is_heave_mode_both(self):
        """ Return True if current heave mode is Both, otherwise False"""
        return self.heave_mode_comboBox.currentIndex() == HEAVE_MODE_BOTH

    def clear(self):
        """ Clear spinboxes"""
        self.depth_doubleSpinBox.clear()
        self.altitude_doubleSpinBox.clear()

