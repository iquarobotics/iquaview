# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
"""
 Redefinition of the QListWidget class to add drag and drop support
"""

import logging
import pickle
import typing

from PyQt5.QtCore import Qt, QMimeData, pyqtSignal
from PyQt5.QtGui import QDropEvent, QDragMoveEvent, QKeyEvent, QIcon
from PyQt5.QtWidgets import QListWidget, QListWidgetItem, QApplication, QAction
from iquaview_lib.cola2api.mission_types import MissionAction

logger = logging.getLogger(__name__)


class ActionsListWidget(QListWidget):
    """
    Overrides QListWidget to allow drag and drop actions
    """
    waypoint_actions_changed = pyqtSignal()

    def __init__(self, parent=None):
        """ Init of the object ActionsListWidget """
        super().__init__(parent)
        self.setAcceptDrops(True)
        self.setDragEnabled(True)
        self.setDropIndicatorShown(True)
        self.setDragDropMode(QListWidget.DragDrop)
        self.setDefaultDropAction(Qt.MoveAction)
        self.setContextMenuPolicy(Qt.ActionsContextMenu)

        # Allow copy-paste actions
        self.action_copy = QAction(QIcon(":/resources/copy.svg"), "Copy", self)
        self.action_copy.triggered.connect(self.copy_selected_items)
        self.action_paste = QAction(QIcon(":/resources/paste.svg"), "Paste", self)
        self.action_paste.triggered.connect(self.paste_items)
        # Allow delete actions
        self.action_remove = QAction(QIcon(":/resources/mActionDeleteSelected.svg"), "Delete", self)
        self.action_remove.triggered.connect(self.remove_selected_items)

        self.addAction(self.action_copy)
        self.addAction(self.action_paste)
        self.addAction(self.action_remove)

    def add_action(self, action):
        """
        Add the given action to the list. This will emit waypoint_actions_changed signal
        :param action: Action to add
        """
        actions = self.get_actions()
        action_in_list = False
        for a in actions:
            if a.get_action_id() == action.get_action_id():
                action_in_list = True

        if not action_in_list:
            action_item = QListWidgetItem()
            name = action.get_action_name()
            act_id = action.get_action_id()
            action_item.setText(name)
            action_item.setData(Qt.UserRole, action)
            action_item.setToolTip(f"{name}\nID: {act_id}")
            self.addItem(action_item)
            logger.debug(action)

        self.waypoint_actions_changed.emit()

    def get_actions(self):
        """
        Get the actions listed in the widget.
        :return:
        """
        actions = []
        for i in range(self.count()):
            item = self.item(i)
            actions.append(MissionAction(
                item.data(Qt.UserRole).get_action_name(),
                item.data(Qt.UserRole).get_action_id(),
                item.data(Qt.UserRole).get_parameters())
            )
        return actions

    def set_actions(self, actions):
        """
        Set the list of actions. This will emit waypoint_actions_changed signal
        :param actions: list of actions
        """
        self.clear()
        self.blockSignals(True)
        for a in actions:
            self.add_action(a)
        self.blockSignals(False)
        self.waypoint_actions_changed.emit()

    def clear_actions(self):
        """Clear the list of actions. This will emit waypoint_actions_changed signal."""
        self.clear()
        self.waypoint_actions_changed.emit()

    def keyPressEvent(self, event: QKeyEvent) -> None:
        """ Override the key press event to copy/paste the selected items """
        # delete
        if event.key() == Qt.Key_Delete:
            self.remove_selected_items()
        if event.key() == Qt.Key_C and event.modifiers() == Qt.ControlModifier:
            self.copy_selected_items()
        elif event.key() == Qt.Key_V and event.modifiers() == Qt.ControlModifier:
            self.paste_items()
        else:
            super().keyPressEvent(event)

    def copy_selected_items(self) -> None:
        """ Copy the selected items to the clipboard """
        selected_items = self.selectedItems()
        if len(selected_items) > 0:
            mime_data = self.mimeData(selected_items)
            QApplication.clipboard().setMimeData(mime_data)

    def remove_selected_items(self) -> None:
        """ Delete the selected items """
        selected_items = self.selectedIndexes()
        for index in reversed(sorted(selected_items)):
            self.takeItem(index.row())

        self.waypoint_actions_changed.emit()

    def paste_items(self) -> None:
        """ Paste the items from the clipboard """
        mime_data = QApplication.clipboard().mimeData()
        if mime_data.hasFormat('actions'):
            # use pickle to get the list of actions
            action_list = pickle.loads(mime_data.data('actions'))
            # add the actions to the list
            for action in action_list:
                self.add_action(MissionAction(action.get_action_name(), action.get_action_id()))

            self.waypoint_actions_changed.emit()

    def mimeTypes(self):
        """ Return the mime types supported by the model """
        return ['actions']

    def mimeData(self, items: typing.Iterable[QListWidgetItem]) -> QMimeData:
        """ Return the mime data for the given items """
        mime_data = QMimeData()
        action_list = []
        for item in items:
            action_list.append(item.data(Qt.UserRole))
        mime_data.setData('actions', pickle.dumps(action_list))
        return mime_data

    def dragMoveEvent(self, event: QDragMoveEvent) -> None:
        """ Override the drag move event to move the items """
        if event.mimeData().hasFormat('actions'):
            self.setDropIndicatorShown(True)
            super().dragMoveEvent(event)

    def dropEvent(self, event: QDropEvent) -> None:
        """ Override the drop event to move the items """
        if event.mimeData().hasFormat('actions'):
            if event.source() == self:
                super().dropEvent(event)
            else:
                # if is not internal move
                # use pickle to get the list of actions
                action_list = pickle.loads(event.mimeData().data('actions'))
                # add the actions to the list
                for action in action_list:
                    self.add_action(MissionAction(action.get_action_name(), action.get_action_id()))
            event.accept()
            self.waypoint_actions_changed.emit()
        else:
            event.ignore()

        self.setDropIndicatorShown(False)
