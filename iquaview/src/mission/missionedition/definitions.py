# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.


class ActionDefinition:
    def __init__(self, action_name="", action_id="", action_description="", identifier="", parameters=None):
        if parameters is None:
            parameters = []
        self.action_name = action_name
        self.action_id = action_id
        self.action_description = action_description
        self.param_list = parameters
        self.identifier = identifier

    def is_valid(self):
        """
        Check if the action definition is valid
        :return: True if the action definition is valid, False otherwise
        :rtype: bool
        """
        return (self.action_name != ""
                and self.action_id != ""
                and self.action_description != ""
                and self.identifier != "")


class ParamDefinition:
    def __init__(self, description="", param_type="", value=""):
        self.description = description
        self.param_type = param_type
        self.value = value

    def is_valid(self):
        """
        Check if the parameter definition is valid
        :return: True if the parameter definition is valid, False otherwise
        :rtype: bool
        """
        return (self.description != ""
                and self.param_type != ""
                and self.value != "")
