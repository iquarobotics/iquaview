# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
"""
 Widget to configure tolerance
"""

import logging

from PyQt5.QtWidgets import QWidget
from PyQt5.QtCore import pyqtSignal

from iquaview.src.ui.ui_tolerance_widget import Ui_ToleranceWidget

logger = logging.getLogger(__name__)

PARK_DISABLED = 0
PARK_ANCHOR = 1
PARK_HOLONOMIC = 2


class ToleranceWidget(QWidget, Ui_ToleranceWidget):
    apply_signal = pyqtSignal()

    def __init__(self, parent=None):
        """ Init of the object ToleranceWidget """
        super().__init__(parent)
        self.setupUi(self)
