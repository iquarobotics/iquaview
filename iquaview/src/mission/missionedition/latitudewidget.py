# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
"""
 Widget to configure latitude
"""

import logging
from typing import Union

from iquaview.src.mission.missionedition.coordinatewidget import CoordinateWidget
from iquaview_lib.utils.coordinateconverter import convert_latitude_to_decimal_degrees
from iquaview_lib.utils.coordinateconverter import format_lat_lon

logger = logging.getLogger(__name__)


class LatitudeWidget(CoordinateWidget):
    """ Latitude widget to display and take input latitude."""

    def __init__(self, parent=None):
        """ Init of the object LatitudeWidget """
        super().__init__("Latitude:", convert_latitude_to_decimal_degrees, parent)
        self._latitude: float = 0

        self.coordinate_lineEdit.textEdited.connect(self.set_latitude)

    def get_formatted_latitude(self) -> str:
        """
        Return coordinate value
        :return: coordinate value
        :rtype: str
        """
        if self.coordinate_lineEdit.text() == "":
            return ""
        dd_lat = str(self.converter(self.coordinate_lineEdit.text()))
        return dd_lat

    def set_formatted_latitude(self, value: Union[float, str]):
        """
        Set value on latitude lineedit
        :param value: value to set
        :type value: Union[float,str]
        """
        lat_converted = self.converter(str(value))
        if not self.coordinate_lineEdit.text() or (
                self.converter(self.coordinate_lineEdit.text()) != lat_converted):
            if self._config is not None:
                coordinate_format = self.config.settings.get("coordinate_format", "degree")
                coordinate_precision = self.config.settings.get("coordinate_precision", 7)
                coordinate_directional_suffix = self.config.settings.get("coordinate_directional_suffix", True)
                coordinate_symbols = self.config.settings.get("coordinate_symbols", True)
                [lat_str, _] = format_lat_lon(lat_converted,
                                              0,
                                              coordinate_format,
                                              coordinate_precision,
                                              coordinate_directional_suffix,
                                              coordinate_symbols)
                self.coordinate_lineEdit.setText(lat_str)
            else:
                self.coordinate_lineEdit.setText(str(lat_converted))

    def get_latitude(self) -> float:
        """
        Return full precision latitude
        :return: full precision latitude
        :rtype: float
        """
        dd_lat = self.converter(str(self._latitude))
        return dd_lat

    def set_latitude(self, value: Union[float, str]):
        """
        Set full precision latitude
        :param value: value to set
        :type value: Union[float,str]
        """
        lat_converted = self.converter(str(value))
        if (not self.coordinate_lineEdit.text()
                or self._latitude == 0
                or (self.converter(str(self._latitude)) != lat_converted)):
            self._latitude = lat_converted
            self.set_formatted_latitude(lat_converted)
