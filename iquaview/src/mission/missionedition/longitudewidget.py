# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
"""
 Widget to configure longitude
"""

import logging
from typing import Union

from iquaview.src.mission.missionedition.coordinatewidget import CoordinateWidget
from iquaview_lib.utils.coordinateconverter import convert_longitude_to_decimal_degrees
from iquaview_lib.utils.coordinateconverter import format_lat_lon


logger = logging.getLogger(__name__)


class LongitudeWidget(CoordinateWidget):
    """ Latitude widget to display and take input longitude."""

    def __init__(self, parent=None):
        """ Init of the object LongitudeWidget """
        super().__init__("Longitude:", convert_longitude_to_decimal_degrees, parent)
        self._longitude: float = 0

        self.coordinate_lineEdit.textEdited.connect(self.set_longitude)

    def get_formatted_longitude(self) -> str:
        """
        Return coordinate value
        :return: coordinate value
        :rtype: str
        """
        if self.coordinate_lineEdit.text() == "":
            return ""
        dd_lon = str(self.converter(self.coordinate_lineEdit.text()))
        return dd_lon

    def set_formatted_longitude(self, value: Union[float, str]):
        """
        Set value on longitude
        :param value: value to set
        :type value: Union[float,str]
        """
        lon_converted = self.converter(str(value))
        if not self.coordinate_lineEdit.text() or (
                self.converter(self.coordinate_lineEdit.text()) != lon_converted):
            if self._config is not None:
                coordinate_format = self.config.settings.get("coordinate_format", "degree")
                coordinate_precision = self.config.settings.get("coordinate_precision", 7)
                coordinate_directional_suffix = self.config.settings.get("coordinate_directional_suffix", True)
                coordinate_symbols = self.config.settings.get("coordinate_symbols", True)
                [_, lon_str] = format_lat_lon(0,
                                              lon_converted,
                                              coordinate_format,
                                              coordinate_precision,
                                              coordinate_directional_suffix,
                                              coordinate_symbols)
                self.coordinate_lineEdit.setText(lon_str)
            else:
                self.coordinate_lineEdit.setText(str(lon_converted))

    def get_longitude(self) -> str:
        """
        Return full precision longitude
        :return: full precision longitude
        :rtype: str
        """
        dd_lon = self.converter(str(self._longitude))
        return dd_lon

    def set_longitude(self, value: Union[float, str]):
        """
        Set full precision longitude
        :param value: value to set
        :type value: Union[float,str]
        """
        lon_converted = self.converter(str(value))
        if (not self.coordinate_lineEdit.text()
                or self._longitude == 0
                or (self.converter(str(self._longitude)) != lon_converted)):
            self._longitude = lon_converted
            self.set_formatted_longitude(lon_converted)
