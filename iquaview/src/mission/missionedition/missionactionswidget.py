# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
"""
 Widget to configure mission actions
"""

import logging

from PyQt5.QtCore import Qt, pyqtSignal, pyqtProperty
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QAction, QListWidgetItem, QDialog
from PyQt5.QtWidgets import QWidget

from iquaview.src.mission.missionedition.loadaddactiondialog import Loadaddactiondialog
from iquaview.src.ui.ui_mission_actions_widget import Ui_MissionActionsWidget
from iquaview_lib.cola2api.mission_types import MissionAction

logger = logging.getLogger(__name__)


class MissionActionsWidget(QWidget, Ui_MissionActionsWidget):
    waypoint_actions_changed = pyqtSignal()

    def __init__(self, parent=None, config=None, vehicle_namespace=None):
        """ Init of the object MissionActionsWidget """
        super().__init__(parent)
        self.setupUi(self)

        self.config = config
        self.vehicle_namespace = vehicle_namespace

        self.action_listWidget.waypoint_actions_changed.connect(self.update_remove_button)
        self.action_listWidget.waypoint_actions_changed.connect(self.waypoint_actions_changed.emit)

        # Allow delete actions
        self.action_remove = QAction(QIcon(":/resources/mActionDeleteSelected.svg"), "Delete", self)
        self.action_remove.triggered.connect(self.action_listWidget.remove_selected_items)
        self.removeAction_toolButton.setDefaultAction(self.action_remove)

        # Allow to add actions
        self.action_add = QAction(QIcon(":/resources/mActionAdd.svg"), "Add action", self)
        self.action_add.triggered.connect(self.show_add_action_dialog)
        self.addAction_toolButton.setDefaultAction(self.action_add)

        # Move rows and notify on last
        self.moving_rows = 0
        self.action_listWidget.model().rowsAboutToBeMoved.connect(self.start_move_rows)
        self.action_listWidget.model().rowsMoved.connect(self.rows_moved)

    def get_label(self) -> str:
        return self.actions_label.text()

    def set_label(self, text: str) -> None:
        self.actions_label.setText(text)

    def set_config_and_vehicle_namespace(self, config, vehicle_namespace):
        """
        Set the config and vehicle namespace
        :param config: config to set
        :type config: Config
        :param vehicle_namespace: vehicle namespace to set
        :type vehicle_namespace: str
        """
        self.config = config
        self.vehicle_namespace = vehicle_namespace

    def update_remove_button(self):
        """ Update the remove button state """
        if self.action_listWidget.count() > 0:
            self.removeAction_toolButton.setEnabled(True)
        else:
            self.removeAction_toolButton.setEnabled(False)

    def get_actions(self):
        """
        Get the actions listed in the widget.
        """
        return self.action_listWidget.get_actions()

    def set_actions(self, actions):
        """
        Set the list of actions.
        :param actions: list of actions
        """
        self.action_listWidget.set_actions(actions)

    def clear_actions(self):
        """Clear the list of actions."""
        self.action_listWidget.clear_actions()

    def show_add_action_dialog(self):
        """ Add new action to the current step """
        load_dialog = Loadaddactiondialog(self.config, self.get_actions())
        result = load_dialog.exec_()
        if result == QDialog.Accepted:
            checked_actions = load_dialog.checked_actions()
            for action in checked_actions:
                self.action_listWidget.add_action(MissionAction(action.action_name,
                                                  self.vehicle_namespace + action.action_id))

    def start_move_rows(self):
        """ Start move. Count how many elements have to be moved so after the last one the signal can be emitted. """
        if self.moving_rows == 0:
            self.moving_rows = len(self.action_listWidget.selectedIndexes())

    def rows_moved(self):
        """ Element moved. Discount and emit if necessary."""
        self.moving_rows = self.moving_rows - 1
        if self.moving_rows == 0:
            self.waypoint_actions_changed.emit()

    label = pyqtProperty(str, fget=get_label, fset=set_label)
