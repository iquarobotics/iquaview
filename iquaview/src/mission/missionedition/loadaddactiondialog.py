# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
Dialog to add actions to the mission
"""

import logging
import os

from PyQt5.QtCore import Qt, QObject, QEvent
from PyQt5.QtWidgets import QDialog, QListWidgetItem

from iquaview.src.mission.missionedition.definitions import ActionDefinition
from iquaview.src.ui.ui_add_actions_dlg import Ui_AddActionDlg
from iquaview_lib.xmlconfighandler.missionactionshandler import MissionActionsHandler

logger = logging.getLogger(__name__)


class Loadaddactiondialog(QDialog, Ui_AddActionDlg):
    """ Dialog to add actions to the mission"""

    def __init__(self, config, current_actions, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.config = config
        self.current_actions = current_actions
        self.action_list = []
        self.buttonBox.rejected.connect(self.on_reject)
        self.buttonBox.accepted.connect(self.on_accept)
        self.listWidget.installEventFilter(self)
        self.load_mission_actions()

    def load_mission_actions(self):
        """ Load mission actions and add it to combobox"""
        config_filename = os.path.abspath(os.path.expanduser(self.config.settings['last_auv_config_xml']))
        ma_handler = MissionActionsHandler(config_filename)
        actions = ma_handler.get_actions()
        self.action_list = []
        self.listWidget.clear()
        for action in actions:
            act = ActionDefinition()
            logger.debug(action.tag)
            for value in action:
                if value.tag == 'action_name':
                    logger.debug(f"     {value.tag} {value.text}")
                    act.action_name = value.text
                elif value.tag == 'action_id':
                    logger.debug(f"     {value.tag} {value.text}")
                    act.action_id = value.text
                elif value.tag == 'action_description':
                    logger.debug(f"     {value.tag} {value.text}")
                    act.action_description = value.text
            self.insert_action(act)

        for item in self.action_list:
            action_item = QListWidgetItem()
            action_item.setFlags(action_item.flags() | Qt.ItemIsUserCheckable)
            action_item.setText(item.action_name)
            action_item.setData(Qt.UserRole, item)
            if self.action_in_current_actions(item):
                action_item.setData(Qt.CheckStateRole, Qt.Checked)
            else:
                action_item.setData(Qt.CheckStateRole, Qt.Unchecked)
            action_item.setToolTip(f"{item.action_name}\nID: {item.action_id}\nDescription: {item.action_description}")

            self.listWidget.addItem(action_item)

    def action_in_current_actions(self, action):
        for act in self.current_actions:
            if action.action_id == act.action_id:
                return True
        return False

    def insert_action(self, action: ActionDefinition):
        """
        insert the action alphabetically into the list of actions
        :param action: action to insert
        :type action: ActionDefinition
        """
        index = len(self.action_list)
        for i, act in enumerate(self.action_list):
            if act.action_name.lower() > action.action_name.lower():
                index = i
                break

        if index == len(self.action_list):
            self.action_list = self.action_list[:index] + [action]
        else:
            self.action_list = self.action_list[:index] + [action] + self.action_list[index:]

    def checked_actions(self):
        actions = []
        for i in range(self.listWidget.count()):
            if self.listWidget.item(i).checkState() == Qt.Checked:
                action = self.listWidget.item(i).data(Qt.UserRole)
                actions.append(action)

        return actions

    def on_accept(self):
        """ check if is acceptable and accept dialog"""
        self.accept()

    def on_reject(self):
        """ reject dialog"""
        self.reject()

    def eventFilter(self, obj: QObject, event: QEvent) -> bool:
        """
        Filters events if this object has been installed as an event filter for the obj object.
        """
        if event.type() == QEvent.KeyPress and event.key() == Qt.Key_Space:
            selected_items = self.listWidget.selectedItems()
            if selected_items:
                first_item_state = selected_items[0].checkState()
                for item in selected_items:
                    if first_item_state == Qt.Checked:
                        item.setCheckState(Qt.Unchecked)
                    else:
                        item.setCheckState(Qt.Checked)

            return True

        return QObject.eventFilter(self, obj, event)
