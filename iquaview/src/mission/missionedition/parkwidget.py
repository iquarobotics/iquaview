# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
"""
 Widget to configure park
"""

import logging

from PyQt5.QtWidgets import QWidget
from PyQt5.QtCore import pyqtSignal

from iquaview.src.ui.ui_park_widget import Ui_ParkWidget

logger = logging.getLogger(__name__)

PARK_DISABLED = 0
PARK_ANCHOR = 1
PARK_HOLONOMIC = 2


class ParkWidget(QWidget, Ui_ParkWidget):
    apply_signal = pyqtSignal()

    def __init__(self, parent=None):
        """ Init of the object ParkWidget """
        super().__init__(parent)
        self.setupUi(self)

        self.park_state_comboBox.currentIndexChanged.connect(self.on_park_state_box_changed)
        self.on_park_state_box_changed(0)

    def on_park_state_box_changed(self, i):
        """
        Modify labels and widgets visibility and apply params
        :param i: park combobox index
        :type i: int
        """
        if i < 3:
            # remove '-' item
            if self.park_state_comboBox.count() == 4:
                self.park_state_comboBox.removeItem(3)

            if self.park_state_comboBox.currentIndex() == PARK_DISABLED:  # disabled
                self.parkTime_label.setVisible(False)
                self.parktime_doubleSpinBox.setVisible(False)
                self.yaw_label.setVisible(False)
                self.park_yaw_doubleSpinBox.setVisible(False)
                self.tolerance_widget.tolerance_label.setVisible(True)
                self.tolerance_widget.tolerance_xy_doubleSpinBox.setVisible(True)
                if self.tolerance_widget.tolerance_xy_doubleSpinBox.text() == "":
                    self.tolerance_widget.tolerance_xy_doubleSpinBox.setValue(2.0)
            elif self.park_state_comboBox.currentIndex() == PARK_ANCHOR:  # Anchor
                self.parkTime_label.setVisible(True)
                self.parktime_doubleSpinBox.setVisible(True)
                self.yaw_label.setVisible(False)
                self.park_yaw_doubleSpinBox.setVisible(False)
                self.tolerance_widget.tolerance_label.setVisible(False)
                self.tolerance_widget.tolerance_xy_doubleSpinBox.setVisible(False)
                if self.parktime_doubleSpinBox.text() == "":
                    self.parktime_doubleSpinBox.setValue(10)
            elif self.park_state_comboBox.currentIndex() == PARK_HOLONOMIC:  # Holonomic
                self.parkTime_label.setVisible(True)
                self.parktime_doubleSpinBox.setVisible(True)
                self.yaw_label.setVisible(True)
                self.park_yaw_doubleSpinBox.setVisible(True)
                self.tolerance_widget.tolerance_label.setVisible(False)
                self.tolerance_widget.tolerance_xy_doubleSpinBox.setVisible(False)
                if self.parktime_doubleSpinBox.text() == "":
                    self.parktime_doubleSpinBox.setValue(10)
                if self.park_yaw_doubleSpinBox.text() == "":
                    self.park_yaw_doubleSpinBox.setValue(0)
            self.apply_signal.emit()

        else:
            self.parkTime_label.setVisible(False)
            self.parktime_doubleSpinBox.setVisible(False)
            self.yaw_label.setVisible(False)
            self.park_yaw_doubleSpinBox.setVisible(False)
            self.tolerance_widget.tolerance_label.setVisible(True)
            self.tolerance_widget.tolerance_xy_doubleSpinBox.setVisible(True)
