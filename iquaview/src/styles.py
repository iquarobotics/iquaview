# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

""" This module contains the widget to configure styles. """
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon, QColor, QKeyEvent
from PyQt5.QtWidgets import QWidget

from iquaview.src.ui.ui_styles import Ui_StylesWidget


class StylesWidget(QWidget, Ui_StylesWidget):
    """
     Widget to configure styles.
    """

    def __init__(self, config, canvas, mission_ctrl, north_arrow, scale_bar):
        """
        Constructor
        :param config: config to load default styles from
        :type Config
        :param canvas: canvas where repaint events will be triggered
        :type QgsMapCanvas
        :param mission_ctrl: mission controller to get the missions from
        :type mission_ctrl: MissionController
        :param north_arrow: Canvas item to change color
        :type north_arrow: NorthArrow
        :param scale_bar: Canvas item to change color
        :type scale_bar: ScaleBar
        """
        super().__init__()
        self.setupUi(self)
        self.config = config
        self.canvas = canvas
        self.mission_ctrl = mission_ctrl
        self.north_arrow = north_arrow
        self.scale_bar = scale_bar
        self.north_arrow_color = self.north_arrow.get_current_color()
        self.scale_bar_color = self.scale_bar.get_current_color()

        # Reset color buttons
        reset_icon = QIcon(":/resources/mActionReset.svg")
        self.reset_color_north_arrow_btn.setIcon(reset_icon)
        self.reset_color_scale_bar_btn.setIcon(reset_icon)
        self.reset_color_missions_btn.setIcon(reset_icon)
        self.reset_color_start_end_btn.setIcon(reset_icon)

        self.reset_color_north_arrow_btn.pressed.connect(self.reset_north_arrow_color)
        self.reset_color_scale_bar_btn.pressed.connect(self.reset_scale_bar_color)
        self.reset_color_missions_btn.pressed.connect(self.reset_missions_color)
        self.reset_color_start_end_btn.pressed.connect(self.reset_start_end_color)

        self.north_arrow_color_button.colorChanged.connect(self.change_north_arrow_color)
        self.scalebar_color_button.colorChanged.connect(self.change_scale_bar_color)
        self.missions_color_button.colorChanged.connect(self.change_missions_color)
        self.start_end_color_button.colorChanged.connect(self.change_start_end_color)

        self.missions_combo_box.currentIndexChanged.connect(self.on_mission_selected)

        self.mission_ctrl.stop_mission_editing.connect(self.reload)
        self.mission_ctrl.mission_added.connect(self.reload)
        self.mission_ctrl.finish_mission_action_signal.connect(self.reload)
        self.mission_ctrl.mission_removed_signal.connect(self.reload)

        self.reload()

    def reload(self):
        """
        Reloads the widget with the current configuration
        """

        # Change color buttons
        self.north_arrow_color = self.north_arrow.get_current_color()
        self.scale_bar_color = self.scale_bar.get_current_color()
        self.all_missions_color = QColor(self.config.settings['style_all_missions_color'])
        self.all_start_end_color = QColor(self.config.settings['style_all_start_end_markers_color'])

        self.north_arrow_color_button.setColor(self.north_arrow_color)
        self.scalebar_color_button.setColor(self.scale_bar_color)

        # Get mission tracks
        tracks_list = self.mission_ctrl.get_mission_list()
        self.tracks_dict = {}
        for track in tracks_list:
            self.tracks_dict[track.get_mission_name()] = track

        self.missions_combo_box.clear()
        self.missions_combo_box.addItem("All missions")
        for mission_name in self.tracks_dict:
            self.missions_combo_box.addItem(mission_name)

        self.track_colors_dict = {}
        self.start_end_colors_dict = {}
        for track in tracks_list:
            track_color = track.get_current_track_color()
            start_end_color = track.get_current_start_end_marker_color()
            self.track_colors_dict[track.get_mission_name()] = track_color
            self.start_end_colors_dict[track.get_mission_name()] = start_end_color

        self.on_mission_selected()  # to update the missions color button

    def change_north_arrow_color(self, color):
        """
        Changes the color of the north arrow canvas item and triggers a canvas repaint
        :param color: new color
        :type color: QColor
        """
        self.north_arrow.change_color(color)
        self.canvas.repaint()

    def change_scale_bar_color(self, color):
        """
        Changes the color of the scale bar canvas item and triggers a canvas repaint
        :param color: new color
        :type color: QColor
        """
        self.scale_bar.change_color(color)
        self.canvas.repaint()

    def change_missions_color(self, color):
        """
        Changes the color of the track of the selected mission in the combo box
        :param color: new color
        :type color: QColor
        """
        if color.isValid():
            if self.missions_combo_box.currentIndex() != 0:
                mission_name = self.missions_combo_box.currentText()
                self.tracks_dict[mission_name].set_mission_color(color)
            else:
                for track in self.tracks_dict.values():
                    track.set_mission_color(color)

    def change_start_end_color(self, color):
        """
        Changes the color of the start end markers of the selected mission in the combo box
        :param color: new color
        :type color: QColor
        """
        if color.isValid():
            if self.missions_combo_box.currentIndex() != 0:
                mission_name = self.missions_combo_box.currentText()
                self.tracks_dict[mission_name].set_start_end_markers_color(color)
            else:
                for track in self.tracks_dict.values():
                    track.set_start_end_markers_color(color)

    def reset_north_arrow_color(self):
        """ Resets north arrow color to its default color """
        default_color = self.north_arrow.get_default_color()
        self.north_arrow_color_button.setColor(default_color)
        self.canvas.repaint()

    def reset_scale_bar_color(self):
        """ Resets scale bar color to its default color """
        default_color = self.scale_bar.get_default_color()
        self.scalebar_color_button.setColor(default_color)
        self.canvas.repaint()

    def reset_missions_color(self):
        """ Resets mission color to its default color """
        self.missions_color_button.setColor(self.all_missions_color)

    def reset_start_end_color(self):
        """ Resets start end mission markers color to their default color """
        self.start_end_color_button.setColor(self.all_start_end_color)

    def closeEvent(self, event):
        """ Override of the closeEvent method. Calls on_close to restore changed colors """
        self.on_close()
        super().closeEvent(event)

    def keyPressEvent(self, event: QKeyEvent) -> None:
        """ Override event handler keyPressEvent"""
        if event.key() == Qt.Key_Escape:
            self.on_close()
            super().keyPressEvent(event)

    def on_mission_selected(self):
        """ Called when a new item is selected in the missions combo box. Changes the color indicator. """
        if self.missions_combo_box.currentIndex() == -1:
            return

        mission_name = self.missions_combo_box.currentText()
        if mission_name != "All missions":
            track_color = self.tracks_dict[mission_name].get_current_track_color()
            start_end_color = self.tracks_dict[mission_name].get_current_start_end_marker_color()
            self.missions_color_button.setColor(track_color)
            self.start_end_color_button.setColor(start_end_color)

        elif len(self.tracks_dict) == 0:
            self.missions_color_button.setColor(self.all_missions_color)
            self.start_end_color_button.setColor(self.all_start_end_color)
        else:
            track_color = None
            start_end_color = None
            first = True
            for track in self.tracks_dict.values():
                if first:
                    track_color = track.get_current_track_color()
                    start_end_color = track.get_current_start_end_marker_color()
                    first = False
                elif track_color != track.get_current_track_color():
                    track_color = None
                elif start_end_color != track.get_current_start_end_marker_color():
                    start_end_color = None
                elif track_color is None and start_end_color is None:
                    break

            if track_color is None:
                self.missions_color_button.setToNull()
            else:
                self.missions_color_button.setColor(track_color)

            if start_end_color is None:
                self.start_end_color_button.setToNull()
            else:
                self.start_end_color_button.setColor(start_end_color)

    def on_close(self):
        """
        Called when the dialog is closed without accepting. Changes all the colors back to what they where before
        opening this dialog.
        """
        self.reset_missions_color()
        self.reset_start_end_color()
        self.reset_north_arrow_color()
        self.reset_scale_bar_color()
        self.north_arrow.change_color(self.north_arrow_color)
        self.scale_bar.change_color(self.scale_bar_color)

        # restore colors from the colors dict created when opening the dialog
        for name, track in self.tracks_dict.items():
            track_color = self.track_colors_dict[name]
            track.set_mission_color(track_color)
            start_end_color = self.start_end_colors_dict[name]
            track.set_start_end_markers_color(start_end_color)

    def save_as_defaults(self):
        """ Save current colors to config """

        self.north_arrow_color = self.north_arrow_color_button.color()
        self.scale_bar_color = self.scalebar_color_button.color()

        tracks_list = self.mission_ctrl.get_mission_list()

        for track in tracks_list:
            track_color = track.get_current_track_color()
            start_end_color = track.get_current_start_end_marker_color()
            self.track_colors_dict[track.get_mission_name()] = track_color
            self.start_end_colors_dict[track.get_mission_name()] = start_end_color

        # update config
        self.config.settings['style_north_arrow_color'] = self.north_arrow_color_button.color().name()
        self.config.settings['style_scale_bar_color'] = self.scalebar_color_button.color().name()

        if self.missions_combo_box.currentIndex() == 0:  # All missions is selected
            track_color = self.missions_color_button.color()
            if track_color.isValid():
                self.config.settings['style_all_missions_color'] = track_color.name()

            start_end_color = self.start_end_color_button.color()
            if start_end_color.isValid():
                self.config.settings['style_all_start_end_markers_color'] = start_end_color.name()

        self.config.save()

        return True
