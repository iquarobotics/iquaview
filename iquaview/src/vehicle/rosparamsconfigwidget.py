# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.


"""
 Class to read and store the xml structure associated to the ros_params tag in the AUV config file
"""
import os
import logging

from PyQt5.QtWidgets import QMessageBox
from qgis.gui import QgsCollapsibleGroupBox

from iquaview.src.ui.ui_collapsible_groupbox import Ui_mCollapsibleGroupBox
from iquaview.src.vehicle.paramwidget import ParamWidget
from iquaview.src.vehicle.sectionwidget import SectionWidget
from iquaview_lib.xmlconfighandler.rosparamsreader import RosParamsReader, Section, Param
from iquaview_lib.definitions import ROSBRIDGE_SERVER_PORT

logger = logging.getLogger(__name__)


class RosParamsConfigWidget(QgsCollapsibleGroupBox, Ui_mCollapsibleGroupBox):
    def __init__(self, config, vehicle_info, parent=None):
        """
        Constructor
        :param config: configuration
        :type config: Config
        :param vehicle_info: Information about AUV
        :type vehicle_info: VehicleInfo
        """
        super().__init__(parent)
        self.setupUi(self)
        self.setTitle("ROS Params")
        self.add_pushButton.setText("New Section")
        self.config = config
        self.vehicle_info = vehicle_info
        self.filename = None
        self.section_list = []
        self.section_key_count = 0

        self.add_pushButton.clicked.connect(lambda: self.new_ros_param_section())

        # load ros params from xml file
        self.set_config_filename(os.path.abspath(os.path.expanduser(self.config.settings['last_auv_config_xml'])))

    def read_xml(self):
        """
        read the last auv configuration xml loaded
        """
        try:
            self.section_list.clear()
            ros_param_reader = RosParamsReader(self.filename,
                                               self.vehicle_info.get_vehicle_ip(),
                                               ROSBRIDGE_SERVER_PORT,
                                               self.vehicle_info.get_vehicle_namespace())
            section_list = ros_param_reader.read_configuration(get_ros_param=False)
            for section in section_list:
                self.add_section(section)
        except OSError as e:
            logger.error("OSError: {}".format(e))

    def set_config_filename(self, filename):
        """
        Set config
        :param filename: config filename
        :type filename: str
        """
        self.filename = filename
        self.load_ros_params()

    def load_ros_params(self):
        """
        Remove previous widgets, read xml and load ros params
        """
        # remove previous widgets
        self.delete_all(self.vboxlayout.layout())

        self.read_xml()

        for section in self.section_list:
            section_widget = self.add_section_widget(section.get_description(),
                                                     section.get_id(),
                                                     True)

            for param in section.get_params():
                param_widget = self.add_param_widget(section_widget,
                                                     param.get_name(),
                                                     param.get_description(),
                                                     param.get_action_id(),
                                                     param.get_id(),
                                                     True)

                param_widget.type_comboBox.setCurrentText(param.get_type())

                if not param.is_array() and param.has_range():
                    param_widget.optional_checkBox.setChecked(True)
                    param_widget.range_radioButton.setChecked(True)

                    param_widget.range_lineEdit.setText(str(param.get_range())[1:-1])

                elif not param.is_array() and param.has_limits():
                    param_widget.optional_checkBox.setChecked(True)
                    param_widget.limits_radioButton.setChecked(True)

                    param_widget.min_doubleSpinBox.setValue(param.get_min_limit())
                    param_widget.max_doubleSpinBox.setValue(param.get_max_limit())

                elif param.is_array():
                    param_widget.optional_checkBox.setChecked(True)
                    param_widget.array_radioButton.setChecked(True)

                    param_widget.size_spinBox.setValue(int(param.get_array_size()))

                else:
                    param_widget.optional_checkBox.setChecked(False)

                param_widget.remove_toolButton.clicked.connect(lambda state, x=param_widget: self.remove_ros_param(x))
                param_widget.name_lineEdit.textChanged.connect(lambda state, x=param_widget: self.update_param(x))
                param_widget.type_comboBox.currentIndexChanged.connect(
                    lambda state, x=param_widget: self.update_param(x))
                param_widget.description_lineEdit.textChanged.connect(
                    lambda state, x=param_widget: self.update_param(x))
                param_widget.action_id_lineEdit.textChanged.connect(lambda state, x=param_widget: self.update_param(x))
                param_widget.optional_checkBox.stateChanged.connect(lambda state, x=param_widget: self.update_param(x))
                param_widget.range_radioButton.toggled.connect(lambda state, x=param_widget: self.update_param(x))
                param_widget.limits_radioButton.toggled.connect(lambda state, x=param_widget: self.update_param(x))
                param_widget.array_radioButton.toggled.connect(lambda state, x=param_widget: self.update_param(x))
                param_widget.range_lineEdit.textChanged.connect(lambda state, x=param_widget: self.update_param(x))
                param_widget.min_doubleSpinBox.valueChanged.connect(lambda state, x=param_widget: self.update_param(x))
                param_widget.max_doubleSpinBox.valueChanged.connect(lambda state, x=param_widget: self.update_param(x))
                param_widget.size_spinBox.valueChanged.connect(lambda state, x=param_widget: self.update_param(x))

    def new_section(self, description):
        """
        Create new Section and add it to list
        :param description: section description
        :type description: str
        :return: Section
        """
        section = Section(description=description, identifier=self.section_key_count)

        self.section_list.append(section)
        self.section_key_count += 1

        return section

    def add_section(self, section: Section):
        """
        add Section to section list
        :param section: Section to add
        :type section: Section
        """
        section.set_id(self.section_key_count)

        self.section_list.append(section)
        self.section_key_count += 1

    def get_section_by_id(self, id):
        """
        Get section by id
        :param id: section id
        :return: if found return section, otherwise return None
        """
        for section in self.section_list:
            if section.get_id() == id:
                return section

        return None

    def remove_section_by_id(self, identifier):
        """
        Remove section by id
        :param identifier: id of the section to remove
        :return: return True if removed, otherwise False
        """
        for section in self.section_list:
            if section.get_id() == identifier:
                self.section_list.remove(section)
                return True

        return False

    def new_ros_param_section(self, description="Section"):
        """
        Create new section
        :param description: section description
        :type description: str
        """

        section = self.new_section(description)
        self.add_section_widget(description, section.get_id())

    def add_section_widget(self, description="Section", id=None, collapsed=False):
        """
        Add section to layout
        :param description: section description
        :type description: str
        :param id: id to identify section
        :type id: int
        :param collapsed:  collapsed state
        :type collapsed: bool
        :return: return section widget
        :rtype SectionWidget
        """
        section_widget = SectionWidget(description, id, collapsed)
        self.vboxlayout.layout().addWidget(section_widget)

        section_widget.remove_section_toolButton.clicked.connect(
            lambda state, x=section_widget: self.remove_ros_param_section(x))
        section_widget.add_param_pushButton.clicked.connect(
            lambda state, x=section_widget: self.new_ros_param(section_widget=x))
        section_widget.description_lineEdit.textChanged.connect(lambda state, x=section_widget: self.update_section(x))

        section_widget.description_lineEdit.setFocus()

        return section_widget

    def new_ros_param(self, section_widget, name="", description="Param", action_id=""):
        """
        Create new param
        :param section_widget: widget where the parameter will be added
        :type section_widget: SectionWidget
        :param name: param name
        :type name: str
        :param description: param description
        :type description: str
        :param action_id: action id
        :type action_id: str
        """
        section = self.get_section_by_id(int(section_widget.objectName()))
        param = Param(description=name)
        identifier = section.add_param(param)
        param_widget = self.add_param_widget(section_widget, name, description, action_id, identifier)
        param_widget.remove_toolButton.clicked.connect(lambda state, x=param_widget: self.remove_ros_param(x))
        param_widget.name_lineEdit.textChanged.connect(lambda state, x=param_widget: self.update_param(x))
        param_widget.type_comboBox.currentIndexChanged.connect(lambda state, x=param_widget: self.update_param(x))
        param_widget.description_lineEdit.textChanged.connect(lambda state, x=param_widget: self.update_param(x))
        param_widget.action_id_lineEdit.textChanged.connect(lambda state, x=param_widget: self.update_param(x))
        param_widget.optional_checkBox.stateChanged.connect(lambda state, x=param_widget: self.update_param(x))
        param_widget.range_radioButton.toggled.connect(lambda state, x=param_widget: self.update_param(x))
        param_widget.limits_radioButton.toggled.connect(lambda state, x=param_widget: self.update_param(x))
        param_widget.array_radioButton.toggled.connect(lambda state, x=param_widget: self.update_param(x))
        param_widget.range_lineEdit.textChanged.connect(lambda state, x=param_widget: self.update_param(x))
        param_widget.min_doubleSpinBox.valueChanged.connect(lambda state, x=param_widget: self.update_param(x))
        param_widget.max_doubleSpinBox.valueChanged.connect(lambda state, x=param_widget: self.update_param(x))
        param_widget.size_spinBox.valueChanged.connect(lambda state, x=param_widget: self.update_param(x))

    def add_param_widget(self, section, name="", description="Param", action_id=None, id=None, collapsed=False):
        """
        Add param to section layout
        :param section: section widget
        :type section: SectionWidget
        :param name: param name
        :type name: str
        :param description: param description
        :type description: str
        :param action_id: action id
        :type action_id: str
        :param id: id to identify param
        :type id: int
        :param collapsed: collapsed state
        :type collapsed: bool
        :return: return param widget
        :rtype: ParamWidget
        """
        param_widget = ParamWidget(name, description, action_id, id, collapsed, section)

        section.params_vboxlayout.layout().addWidget(param_widget)

        param_widget.limits_radioButton.toggle()
        param_widget.range_radioButton.toggle()
        param_widget.optional_checkBox.toggle()

        param_widget.optional_checkBox.setChecked(False)

        param_widget.name_lineEdit.setFocus()

        return param_widget

    def remove_ros_param_section(self, section_widget):
        """
        remove section
        :param section_widget: section widget to remove
        :type section_widget: SectionWidget
        """
        confirmation_msg = "Are you sure you want to remove {} section?".format(section_widget.title())

        reply = QMessageBox.question(self, 'Removal confirmation',
                                     confirmation_msg, QMessageBox.Yes | QMessageBox.No, defaultButton=QMessageBox.Yes)
        if reply == QMessageBox.Yes:
            removed = self.remove_section_by_id(int(section_widget.objectName()))
            self.delete_all(section_widget.layout())
            section_widget.deleteLater()

    def remove_ros_param(self, param_widget):
        """
        remove param
        :param param_widget: param widget to remove
        :type param_widget: ParamWidget
        """
        confirmation_msg = "Are you sure you want to remove {} parameter?".format(param_widget.title())

        reply = QMessageBox.question(self, 'Removal confirmation',
                                     confirmation_msg, QMessageBox.Yes | QMessageBox.No, defaultButton=QMessageBox.Yes)
        if reply == QMessageBox.Yes:
            section_widget = param_widget.get_section_widget()
            section = self.get_section_by_id(int(section_widget.objectName()))
            params = section.get_params()
            for param in params:
                if param.get_id() == int(param_widget.objectName()):
                    section.get_params().remove(param)

            self.delete_all(param_widget.layout())
            param_widget.deleteLater()

    def update_section(self, widget):
        """
        Update section from widget
        :type widget: SectionWidget with the information
        :param widget: SectionWidget
        """
        section = self.get_section_by_id(int(widget.objectName()))
        section.set_description(widget.description_lineEdit.text())

    def update_param(self, param_widget):
        """
        Update param from widget
        :param param_widget: widget with the information
        :type param_widget: ParamWidget
        """
        section_widget = param_widget.get_section_widget()
        section = self.get_section_by_id(int(section_widget.objectName()))
        params = section.get_params()
        for param in params:
            if param.get_id() == int(param_widget.objectName()):

                if param_widget.optional_checkBox.isChecked() and param_widget.array_radioButton.isChecked():
                    param.set_field_array(param_widget.name_lineEdit.text(),
                                          param_widget.type_comboBox.currentText(),
                                          str(param_widget.size_spinBox.value()))
                    param.set_description(param_widget.description_lineEdit.text())
                    param.set_action_id(param_widget.action_id_lineEdit.text())
                    param.field = None
                else:
                    param.field_array = None
                    param.set_field(param_widget.name_lineEdit.text(),
                                    param_widget.type_comboBox.currentText())
                    param.set_description(param_widget.description_lineEdit.text())
                    param.set_action_id(param_widget.action_id_lineEdit.text())

                    if param_widget.optional_checkBox.isChecked():
                        if param_widget.range_radioButton.isChecked():
                            if param_widget.range_lineEdit.text() != "":
                                param.set_field_range(param_widget.range_lineEdit.text())
                        else:
                            param.set_field_limits(param_widget.min_doubleSpinBox.value(),
                                                   param_widget.max_doubleSpinBox.value())

    def save(self):
        """
        save the ros params inside the auv configuration xml
        """

        RosParamsReader.write(self.filename, self.section_list)

    def delete_all(self, layout):
        """
        delete all widget from layout
        :param layout: layout is a qt layout
        """
        if layout is not None:
            for i in reversed(range(layout.count())):
                item = layout.takeAt(i)
                widget = item.widget()
                if widget is not None:
                    widget.deleteLater()
                else:
                    self.delete_all(item.layout())

    def is_valid(self) -> bool:
        """
        Check if settings are valid
        :return: True if settings are valid
        :rtype: bool
        """
        for section in self.section_list:
            if not section.is_valid():
                return False
        return True
