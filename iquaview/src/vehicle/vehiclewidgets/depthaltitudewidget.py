# Copyright (c) 2022 Iqua Robotics SL
#
# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
Widget for graphically displaying the depth and altitude of a vehicle.
"""

import sys
import numpy as np
import matplotlib
from PyQt5.QtGui import QPalette
from PyQt5.QtWidgets import QApplication, QVBoxLayout, QWidget, QSizePolicy
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib.pyplot as plt

# Use Qt5Agg backend for rendering with PyQt5
matplotlib.use("Qt5Agg")
plt.style.use('ggplot')

# Constants for plot layout
LEFT, WIDTH = 0.05, 1
BOTTOM, HEIGHT = 0.05, 1
RIGHT = WIDTH - LEFT
TOP = HEIGHT - BOTTOM


class MyMplCanvas(FigureCanvas):
    """
    A base Matplotlib canvas integrated with PyQt5.
    """

    def __init__(self, parent=None, f_width=5, f_height=4, dpi=100):
        # Create a Matplotlib figure
        fig = Figure(figsize=(f_width, f_height), dpi=dpi)
        self.axes = fig.add_subplot(111)

        # Set the canvas background to match the parent's palette
        if parent is not None:
            fig.set_facecolor(parent.palette().color(QPalette.Window).name())

        # Customize axes visibility
        self.axes.get_xaxis().set_visible(False)

        # Adjust layout for better fitting
        fig.tight_layout()

        # Initialize the FigureCanvas
        super().__init__(fig)
        self.setParent(parent)
        self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Expanding)
        self.updateGeometry()

    def compute_initial_figure(self):
        """
        Placeholder method for initial plotting logic.
        """
        pass


class DepthAltitudeCanvas(MyMplCanvas):
    """
    A specialized canvas for dynamically plotting depth and altitude data.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.x = np.array([0.0])
        self.depth_array = np.array([0.0])
        self.total_array = np.array([0.0])
        self.data = None

    def set_data(self, data):
        """
        Updates the canvas with new data and refreshes the plot.

        :param data: Dictionary containing 'position' and 'altitude' information.
        """
        self.data = data
        self.update_figure()

    def compute_initial_figure(self):
        """
        Initializes the canvas with a placeholder plot.
        """
        self.axes.plot([0], [0], 'r')

    def update_figure(self):
        """
        Updates the plot with the current depth and altitude data.
        """
        # Extract depth and altitude values
        depth = float(self.data['position']['depth'])
        altitude = max(float(self.data['altitude']), 0)  # Ensure altitude is non-negative

        # Manage history arrays
        if len(self.depth_array) >= 30:
            self.depth_array = np.delete(self.depth_array, 0)
            self.total_array = np.delete(self.total_array, 0)

        self.depth_array = np.append(self.depth_array, depth)
        self.total_array = np.append(self.total_array, altitude + depth)

        if len(self.x) < len(self.depth_array):
            self.x = np.append(self.x, len(self.x))

        # Refresh the plot
        self.axes.cla()
        self.axes.plot(self.x, self.depth_array, 'g', label="Depth")
        self.axes.plot(self.x, self.total_array, 'r', label="Total Depth")

        if not self.axes.yaxis_inverted():
            self.axes.invert_yaxis()

        # Configure Y-axis limits
        minimum = int(np.amin(self.depth_array)) - (np.amax(self.total_array) / 20)
        minimum = minimum if minimum >= 0 else -(np.amax(self.total_array) / 20)
        self.axes.set_ylim(ymax=minimum)

        # Add text with dynamic color based on altitude
        color = 'orange' if altitude <= 5.0 else 'blue'
        self.axes.text(RIGHT, TOP, f"Depth: {depth:.1f} m\nAlt: {altitude:.1f} m",
                       horizontalalignment='right',
                       verticalalignment='top',
                       transform=self.axes.transAxes,
                       color=color)

        self.draw()


if __name__ == "__main__":
    app = QApplication(sys.argv)

    # Create and display the main widget
    window = QWidget()
    depth_altitude_canvas = DepthAltitudeCanvas(window, f_width=3, f_height=2, dpi=100)

    layout = QVBoxLayout()
    layout.addWidget(depth_altitude_canvas)
    window.setLayout(layout)

    window.show()
    sys.exit(app.exec_())
