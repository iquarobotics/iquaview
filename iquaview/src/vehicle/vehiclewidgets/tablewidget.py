# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Widget to show current and desired pose requests for all vehicle axes
"""

import math
import logging

from PyQt5.QtCore import Qt, pyqtSignal, QTimer, QPoint
from PyQt5.QtWidgets import QWidget, QHeaderView

from iquaview.src.ui.ui_table_widget import Ui_Table
from iquaview_lib.vehicle.vehicledata import VehicleData, is_navigation_new

logger = logging.getLogger(__name__)


class TablePoseWidget(QWidget, Ui_Table):
    update_table_signal = pyqtSignal()
    start_drag_drop_signal = pyqtSignal(QPoint)
    stop_timer_signal = pyqtSignal()

    def __init__(self, vehicledata, parent=None):
        super().__init__(parent)
        self.setupUi(self)

        # for every column and row resize to contents

        for i in range(self.tableWidget.columnCount()):
            self.tableWidget.horizontalHeader().setSectionResizeMode(i, QHeaderView.Stretch)

        for i in range(self.tableWidget.rowCount()):
            self.tableWidget.verticalHeader().setSectionResizeMode(i, QHeaderView.Stretch)

        self.vehicle_data: VehicleData = vehicledata

        self.connected = False

        self.timer = QTimer()
        self.timer.timeout.connect(self.refresh)
        self.stop_timer_signal.connect(self.stop_timer)

        self.data = None
        self.desired_pose_data = None
        self.desired_twist_data = None
        self.update_table_signal.connect(self.update_table)

    def connect(self):
        """ set connected state to true and start timer"""
        self.connected = True
        if not self.timer.isActive():
            self.timer.start(1000)

    def refresh(self):
        """ send signal to update table"""
        if self.connected:
            try:
                self.update_table_signal.emit()

                d_pose_data = self.vehicle_data.get_desired_pose()
                d_twist_data = self.vehicle_data.get_desired_twist()
                if d_pose_data is not None and d_pose_data['valid_data'] == 'disconnected':
                    self.vehicle_data.subscribe_topic("merged world waypoint req")
                if d_twist_data is not None and d_twist_data['valid_data'] == 'disconnected':
                    self.vehicle_data.subscribe_topic("merged body velocity req")
            except Exception as e:
                logger.warning(f"Could not refresh table widget: {e}")

    def update_table(self):
        """ Update the table widget"""
        nav_data = self.vehicle_data.get_nav_sts()
        d_pose_data = self.vehicle_data.get_desired_pose()
        d_twist_data = self.vehicle_data.get_desired_twist()
        if is_navigation_new(nav_data):
            self.data = nav_data
        if d_pose_data is not None and d_pose_data['valid_data'] == 'new_data':
            self.desired_pose_data = d_pose_data
        if d_twist_data is not None and d_twist_data['valid_data'] == 'new_data':
            self.desired_twist_data = d_twist_data

        if self.data is not None and self.desired_pose_data is not None and self.desired_twist_data is not None:
            # current pose
            item = self.tableWidget.item(0, 0)
            item.setText(f"{self.data['position']['north']:.2f}")
            item = self.tableWidget.item(0, 1)
            item.setText(f"{self.data['position']['east']:.2f}")
            item = self.tableWidget.item(0, 2)
            item.setText(f"{self.data['position']['depth']:.2f}")

            roll = math.degrees(self.data["orientation"]["roll"])
            item = self.tableWidget.item(0, 3)
            item.setText(f"{roll:.2f}")

            pitch = math.degrees(self.data["orientation"]["pitch"])
            item = self.tableWidget.item(0, 4)
            item.setText(f"{pitch:.2f}")

            yaw = math.degrees(self.data["orientation"]["yaw"])
            if yaw < 0.0:
                yaw = yaw + 360.0
            item = self.tableWidget.item(0, 5)
            item.setText(f"{yaw:.2f}")

            # desired pose
            item = self.tableWidget.item(1, 0)
            self.change_item_enabled(item, self.desired_pose_data['disable_axis']['x'])
            item.setText(f"{self.desired_pose_data['position']['north']:.2f}")
            item = self.tableWidget.item(1, 1)
            self.change_item_enabled(item, self.desired_pose_data['disable_axis']['y'])
            item.setText(f"{self.desired_pose_data['position']['east']:.2f}")
            item = self.tableWidget.item(1, 2)
            self.change_item_enabled(item, self.desired_pose_data['disable_axis']['z'])
            item.setText(f"{self.desired_pose_data['position']['depth']:.2f}")

            roll = math.degrees(self.desired_pose_data['orientation']['roll'])
            item = self.tableWidget.item(1, 3)
            self.change_item_enabled(item, self.desired_pose_data['disable_axis']['roll'])
            item.setText(f"{roll:.2f}")

            pitch = math.degrees(self.desired_pose_data['orientation']['pitch'])
            item = self.tableWidget.item(1, 4)
            self.change_item_enabled(item, self.desired_pose_data['disable_axis']['pitch'])
            item.setText(f"{pitch:.2f}")

            yaw = math.degrees(self.desired_pose_data['orientation']['yaw'])
            if yaw < 0.0:
                yaw = yaw + 360
            item = self.tableWidget.item(1, 5)
            self.change_item_enabled(item, self.desired_pose_data['disable_axis']['yaw'])
            item.setText(f"{yaw:.2f}")

            # current twist (velocity)
            item = self.tableWidget.item(2, 0)
            item.setText(f"{self.data['body_velocity']['x']:.2f}")
            item = self.tableWidget.item(2, 1)
            item.setText(f"{self.data['body_velocity']['y']:.2f}")
            item = self.tableWidget.item(2, 2)
            item.setText(f"{self.data['body_velocity']['z']:.2f}")

            roll = math.degrees(self.data['orientation_rate']['roll'])
            item = self.tableWidget.item(2, 3)
            item.setText(f"{roll:.2f}")

            pitch = math.degrees(self.data['orientation_rate']['pitch'])
            item = self.tableWidget.item(2, 4)
            item.setText(f"{pitch:.2f}")

            yaw = math.degrees(self.data['orientation_rate']['yaw'])
            item = self.tableWidget.item(2, 5)
            item.setText(f"{yaw:.2f}")

            # desired twist (velocity)
            item = self.tableWidget.item(3, 0)
            self.change_item_enabled(item, self.desired_twist_data['disable_axis']['x'])
            item.setText(f"{self.desired_twist_data['twist']['linear']['x']:.2f}")
            item = self.tableWidget.item(3, 1)
            self.change_item_enabled(item, self.desired_twist_data['disable_axis']['y'])
            item.setText(f"{self.desired_twist_data['twist']['linear']['y']:.2f}")
            item = self.tableWidget.item(3, 2)
            self.change_item_enabled(item, self.desired_twist_data['disable_axis']['z'])
            item.setText(f"{self.desired_twist_data['twist']['linear']['z']:.2f}")
            item = self.tableWidget.item(3, 3)
            self.change_item_enabled(item, self.desired_twist_data['disable_axis']['roll'])
            item.setText(f"{math.degrees(self.desired_twist_data['twist']['angular']['x']):.2f}")
            item = self.tableWidget.item(3, 4)
            self.change_item_enabled(item, self.desired_twist_data['disable_axis']['pitch'])
            item.setText(f"{math.degrees(self.desired_twist_data['twist']['angular']['y']):.2f}")
            item = self.tableWidget.item(3, 5)
            self.change_item_enabled(item, self.desired_twist_data['disable_axis']['yaw'])
            item.setText(f"{math.degrees(self.desired_twist_data['twist']['angular']['z']):.2f}")

        self.tableWidget.viewport().update()

    def change_item_enabled(self, item, disabled):
        """
        Change item enable status

        :param item: item is a QTableitemwidget
        :param disabled: disabled is a bool. True if item is disabled, False if is enabled
        """
        if disabled:
            item.setFlags(Qt.NoItemFlags)
        else:
            item.setFlags(Qt.ItemIsEnabled)

    def resizeEvent(self, event):
        super().resizeEvent(event)

    def mouseMoveEvent(self, event) -> None:
        if event.buttons() and Qt.LeftButton:
            pos = self.mapToGlobal(event.pos())
            self.start_drag_drop_signal.emit(pos)

    def stop_timer(self):
        """ Stop timer to update data"""
        self.timer.stop()

    def disconnect_object(self):
        self.connected = False

        self.stop_timer_signal.emit()
