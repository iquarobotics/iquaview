# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Widget to monitor the battery level of the vehicle.
"""

import sys

from PyQt5.QtCore import pyqtSignal, QRect, Qt, pyqtProperty, pyqtSlot
from PyQt5.QtGui import QColor, QPainter, QPen, QBrush, QPalette
from PyQt5.QtWidgets import QWidget, QApplication, QSpinBox, QVBoxLayout


class BatteryWidget(QWidget):
    chargeChanged = pyqtSignal(float)

    def __init__(self, parent=None):

        QWidget.__init__(self, parent)

        self._charge = 0

        self.background_color = QColor(self.palette().color(QPalette.Window))
        self.black = QColor(self.palette().color(QPalette.WindowText))
        self.green = QColor(Qt.green)
        self.yellow = QColor(Qt.yellow)
        self.red = QColor(231, 116, 113)

        self.setMinimumWidth(110)
        self.setMinimumHeight(75)
        self.setMaximumWidth(110)
        self.setMaximumHeight(75)
        self.data = None

        # Defining sizes of battery
        self.ex_top_left_y = 25  # top left corner, Y position, exterior border (black)
        self.in_top_left_y = self.ex_top_left_y + 1  # top left corner, Y position, interior border (background, filler)
        self.ex_top_left_x = 6  # top left corner, X position, exterior border (black)
        self.in_top_left_x = self.ex_top_left_x + 1  # top left corner, X position, interior border (background, filler)
        self.exterior_width = self.width() - 15  # Width of the drawing, exterior border (black)
        self.interior_width = self.exterior_width - 2  # Width of the drawing, interior border (background)
        self.exterior_height = self.height() - 40  # Height of the drawing, exterior border (black)
        self.interior_height = self.exterior_height - 2  # Height of the drawing, interior border (background)
        self.total_width = self.width() - 11 - self.ex_top_left_x  # Width of the battery bar drawing (green)
        self.segment_width = self.total_width / 4  # Width of a battery segment
        self.bot_p_y = self.ex_top_left_y + self.exterior_height  # Y pos of the bottom of the drawing
        self.div_p1_x = self.in_top_left_x + self.total_width // 4  # X pos of the first divider
        self.div_p2_x = self.in_top_left_x + self.total_width // 2  # X pos of the second divider
        self.div_p3_x = self.in_top_left_x + (self.total_width // 4) * 3  # X pos of the third divider

    def paintEvent(self, event):
        """ paint the battery widget"""
        painter = QPainter()
        painter.begin(self)
        painter.setRenderHint(QPainter.Antialiasing)

        # draw positive terminal
        painter.setBrush(self.black)
        rect_p_terminal = QRect(
            int(self.width() - 7),
            int(self.height() / 2),
            int(self.width() / 50),
            int(self.height() / 8)
        )
        painter.drawRect(rect_p_terminal)

        # draw battery
        painter.setBrush(self.black)
        rect = QRect(self.ex_top_left_x, self.ex_top_left_y, self.exterior_width, self.exterior_height)
        painter.drawRoundedRect(rect, 5.0, 5.0)

        # draw transparent background
        painter.setBrush(self.background_color)
        rect_transp = QRect(self.in_top_left_x, self.in_top_left_y, self.interior_width, self.interior_height)
        painter.drawRoundedRect(rect_transp.intersected(rect), 5.0, 5.0)

        # draw charge
        charge = self._charge
        # charge on pixels 0 to 100
        charge = min(charge, 100)
        charge = max(charge, 0)

        if charge > 50:
            color = self.green
        elif charge < 25:
            color = self.red
        else:
            color = self.yellow
        painter.setBrush(color)

        # Draw 1 battery segment with 3 separators
        seg_width = self.total_width * charge / 100
        rect = QRect(
            int(self.in_top_left_x),
            int(self.in_top_left_y),
            int(seg_width),
            int(self.interior_height)
        )

        painter.drawRoundedRect(rect, 5.0, 5.0)

        # Draw separators
        painter.setPen(QPen(QBrush(self.background_color), 2, Qt.SolidLine))
        painter.drawLine(self.div_p1_x, self.in_top_left_y + 2, self.div_p1_x,
                         self.in_top_left_y + self.interior_height - 2)
        painter.drawLine(self.div_p2_x, self.in_top_left_y + 2, self.div_p2_x,
                         self.in_top_left_y + self.interior_height - 2)
        painter.drawLine(self.div_p3_x, self.in_top_left_y + 2, self.div_p3_x,
                         self.in_top_left_y + self.interior_height - 2)

        # Draw rect border
        painter.setPen(QPen(QBrush(self.black), 1, Qt.SolidLine))
        painter.setBrush(Qt.transparent)
        painter.drawRoundedRect(rect, 5.0, 5.0)

        # Write charge level
        painter.drawText(
            int(self.width() / 2 - self.width() / 8),
            int(self.height() / 2 + 10),
            str(int(charge)) + '%'
        )

        painter.end()

    def charge(self):
        """ return charge value"""
        return self._charge

    def set_data(self, data):
        """ set 'data'"""
        self.data = data
        if data:
            self.set_charge(data)

    @pyqtSlot(int)
    @pyqtSlot(float)
    def set_charge(self, charge):
        if charge != self._charge:
            self._charge = charge
            self.chargeChanged.emit(charge)
            self.update()

    charge = pyqtProperty(float, charge, set_charge)


if __name__ == "__main__":
    app = QApplication(sys.argv)

    window = QWidget()
    batterywidget = BatteryWidget()
    spinBox = QSpinBox()
    spinBox.setRange(0, 359)
    spinBox.valueChanged.connect(batterywidget.set_charge)

    layout = QVBoxLayout()
    layout.addWidget(batterywidget)
    layout.addWidget(spinBox)
    window.setLayout(layout)

    window.show()
    sys.exit(app.exec_())
