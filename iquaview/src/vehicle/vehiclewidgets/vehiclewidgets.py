# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
Sets the widget containing all the graphical vehicle widgets
including battery, compass, roll/pitch, depth/altitude, velocimeter,
and table of current and desired pose requests.
"""
import typing

from PyQt5 import QtCore
from PyQt5.QtCore import QTimer, pyqtSignal, Qt, QMimeData, QPoint, QMargins, QSettings
from PyQt5.QtGui import QDrag, QPixmap, QGuiApplication, QPainter
from PyQt5.QtWidgets import QWidget, QMenu, QAction, QApplication, QRubberBand

from iquaview.src.ui.ui_vehicle_widgets import Ui_VehicleWidgets
from iquaview.src.vehicle.vehiclewidgets import (compasswidget,
                                                 rollpitchwidget,
                                                 tablewidget,
                                                 resourceswidget,
                                                 setpointswidget)
from iquaview.src.vehicle.vehiclewidgets import depthaltitudewidget, velocimeterwidget
from iquaview_lib.vehicle.vehicledata import VehicleData, is_navigation_new

DEFAULT_RESOURCES_NAME = "resources"
DEFAULT_COMPASS_NAME = "compass"
DEFAULT_ROLL_PITCH_NAME = "roll_pitch"
DEFAULT_VELOCIMETER_NAME = "velocimeter"
DEFAULT_DEPTH_ALTITUDE_NAME = "depth_altitude"
DEFAULT_TABLE_NAME = "table"
DEFAULT_SETPOINTS_NAME = "setpoints"


class VehicleWidgets(QWidget, Ui_VehicleWidgets):
    """

    This class contains all subgraphicwidgets

    """

    refresh_signal = pyqtSignal()
    stop_timer_signal = pyqtSignal()

    def __init__(self, vehicle_info, vehicledata, parent=None):
        """
        Init of the object VehicleWidgets
        :param vehicle_info: information about the AUV
        :type vehicle_info: VehicleInfo
        :param vehicledata: data received from the AUV
        :type vehicledata: VehicleData
        """
        super().__init__(parent)
        self.setupUi(self)
        self.timer = QTimer()
        self.timer.timeout.connect(self.start_timer)
        self.stop_timer_signal.connect(self.stop_timer)

        self.dragging = None
        self.rubber_band = None
        self.area_mid_point = None
        self.start_drag_position = None
        self.vehicle_widgets_dict = {}
        self.connected = False
        self.setAcceptDrops(True)

        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.vehicle_info = vehicle_info
        self.vehicle_data: VehicleData = vehicledata
        self.layout = self.scrollAreaWidgetContents.layout()
        self.refresh_signal.connect(self.refresh)
        self.setMinimumHeight(250)

        # create QAction's to change Vehicle widgets state
        self.resources_action = QAction("Resources", self)
        self.resources_action.setCheckable(True)
        self.resources_action.setChecked(True)
        self.resources_action.setEnabled(False)
        self.resources_action.triggered.connect(lambda: self.state_changed(DEFAULT_RESOURCES_NAME))

        self.compass_action = QAction("Compass", self)
        self.compass_action.setCheckable(True)
        self.compass_action.setChecked(True)
        self.compass_action.setEnabled(False)
        self.compass_action.triggered.connect(lambda: self.state_changed(DEFAULT_COMPASS_NAME))

        self.depth_altitude_action = QAction("Depth/Altitude", self)
        self.depth_altitude_action.setCheckable(True)
        self.depth_altitude_action.setChecked(True)
        self.depth_altitude_action.setEnabled(False)
        self.depth_altitude_action.triggered.connect(lambda: self.state_changed(DEFAULT_DEPTH_ALTITUDE_NAME))

        self.roll_pitch_action = QAction("Roll/Pitch", self)
        self.roll_pitch_action.setCheckable(True)
        self.roll_pitch_action.setChecked(True)
        self.roll_pitch_action.setEnabled(False)
        self.roll_pitch_action.triggered.connect(lambda: self.state_changed(DEFAULT_ROLL_PITCH_NAME))

        self.velocimeter_action = QAction("Velocimeter", self)
        self.velocimeter_action.setCheckable(True)
        self.velocimeter_action.setChecked(True)
        self.velocimeter_action.setEnabled(False)
        self.velocimeter_action.triggered.connect(lambda: self.state_changed(DEFAULT_VELOCIMETER_NAME))

        self.table_action = QAction("Table", self)
        self.table_action.setCheckable(True)
        self.table_action.setChecked(True)
        self.table_action.setEnabled(False)
        self.table_action.triggered.connect(lambda: self.state_changed(DEFAULT_TABLE_NAME))

        self.setpoints_action = QAction("Setpoints", self)
        self.setpoints_action.setCheckable(True)
        self.setpoints_action.setChecked(True)
        self.setpoints_action.setEnabled(False)
        self.setpoints_action.triggered.connect(lambda: self.state_changed(DEFAULT_SETPOINTS_NAME))

        # Create menu to group all the actions together
        self.menu_widgets = QMenu("Vehicle Widgets")
        self.menu_widgets.addActions([self.resources_action,
                                      self.compass_action,
                                      self.roll_pitch_action,
                                      self.velocimeter_action,
                                      self.depth_altitude_action,
                                      self.table_action,
                                      self.setpoints_action])

        self.set_previous_session_widgets_state()

    def set_previous_session_widgets_state(self) -> None:
        """
        Set the previous state of the widgets. Load state from QSettings
        """
        settings = QSettings()
        settings.beginGroup("VehicleWidgets")
        self.resources_action.setChecked(settings.value(f"{DEFAULT_RESOURCES_NAME}/visible", True, type=bool))
        self.compass_action.setChecked(settings.value(f"{DEFAULT_COMPASS_NAME}/visible", True, type=bool))
        self.roll_pitch_action.setChecked(settings.value(f"{DEFAULT_ROLL_PITCH_NAME}/visible", True, type=bool))
        self.velocimeter_action.setChecked(settings.value(f"{DEFAULT_VELOCIMETER_NAME}/visible", True, type=bool))
        self.depth_altitude_action.setChecked(settings.value(f"{DEFAULT_DEPTH_ALTITUDE_NAME}/visible", True, type=bool))
        self.table_action.setChecked(settings.value(f"{DEFAULT_TABLE_NAME}/visible", True, type=bool))
        self.setpoints_action.setChecked(settings.value(f"{DEFAULT_SETPOINTS_NAME}/visible", True, type=bool))
        settings.endGroup()

    def get_menu_actions(self) -> typing.List['QAction']:
        """
        Returns a list with all the actions of the menu
        :return: list with all the actions of the menu
        :rtype: list
        """
        return self.menu_widgets.actions()

    def vehicle_widgets_dock_visibility_changed(self, visible) -> None:
        """ Change vehicle widgets dock visibility."""
        if not visible:
            self.resources_action.setEnabled(False)
            self.compass_action.setEnabled(False)
            self.depth_altitude_action.setEnabled(False)
            self.roll_pitch_action.setEnabled(False)
            self.velocimeter_action.setEnabled(False)
            self.table_action.setEnabled(False)
            self.setpoints_action.setEnabled(False)
        else:
            self.resources_action.setEnabled(True)
            self.compass_action.setEnabled(True)
            self.depth_altitude_action.setEnabled(True)
            self.roll_pitch_action.setEnabled(True)
            self.velocimeter_action.setEnabled(True)
            self.table_action.setEnabled(True)
            self.setpoints_action.setEnabled(True)

    def state_changed(self, name: str) -> None:
        """ Toggle the visibility of the widgets depending on the state."""
        sender = self.sender()
        settings = QSettings()
        settings.beginGroup("VehicleWidgets")
        settings.setValue(f"{name}/visible", sender.isChecked())
        settings.endGroup()
        if sender.isChecked():
            self.vehicle_widgets_dict.get(name).show()
        else:
            self.vehicle_widgets_dict.get(name).hide()

    def connect(self) -> None:
        """ Creates the widgets and initializes data """
        settings = QSettings()

        resourcesusage = resourceswidget.ResourcesWidget()
        compass = compasswidget.CompassWidget()
        rollpitch = rollpitchwidget.RollPitchWidget()
        velocimeter = velocimeterwidget.VelocimeterWidget()
        depthaltitude = depthaltitudewidget.DepthAltitudeCanvas(self, f_width=3, f_height=2, dpi=100)
        table = tablewidget.TablePoseWidget(self.vehicle_data)
        setpoints = setpointswidget.SetpointsWidget()

        resourcesusage.setObjectName(DEFAULT_RESOURCES_NAME)
        compass.setObjectName(DEFAULT_COMPASS_NAME)
        rollpitch.setObjectName(DEFAULT_ROLL_PITCH_NAME)
        velocimeter.setObjectName(DEFAULT_VELOCIMETER_NAME)
        depthaltitude.setObjectName(DEFAULT_DEPTH_ALTITUDE_NAME)
        table.setObjectName(DEFAULT_TABLE_NAME)
        setpoints.setObjectName(DEFAULT_SETPOINTS_NAME)

        self.vehicle_widgets_dict = {
            DEFAULT_RESOURCES_NAME: resourcesusage,
            DEFAULT_COMPASS_NAME: compass,
            DEFAULT_ROLL_PITCH_NAME: rollpitch,
            DEFAULT_VELOCIMETER_NAME: velocimeter,
            DEFAULT_DEPTH_ALTITUDE_NAME: depthaltitude,
            DEFAULT_TABLE_NAME: table,
            DEFAULT_SETPOINTS_NAME: setpoints,
        }
        order = settings.value("VehicleWidgets/layout_order",
                               [DEFAULT_RESOURCES_NAME,
                                DEFAULT_COMPASS_NAME,
                                DEFAULT_ROLL_PITCH_NAME,
                                DEFAULT_VELOCIMETER_NAME,
                                DEFAULT_DEPTH_ALTITUDE_NAME,
                                DEFAULT_TABLE_NAME,
                                DEFAULT_SETPOINTS_NAME,
                                ],
                               type=list)
        self.vehicle_widgets_dict.get(DEFAULT_DEPTH_ALTITUDE_NAME).setAttribute(Qt.WA_TransparentForMouseEvents)
        # self.table.setAttribute(Qt.WA_TransparentForMouseEvents)
        self.vehicle_widgets_dict.get(DEFAULT_TABLE_NAME).start_drag_drop_signal.connect(self.start_drag_and_drop)

        # add widgets with the order specified in the settings
        for widget_name in order:
            if widget_name in self.vehicle_widgets_dict:
                self.layout.addWidget(self.vehicle_widgets_dict.get(widget_name))

        self.scrollAreaWidgetContents.setFocus()

        if not self.resources_action.isChecked():
            self.vehicle_widgets_dict.get(DEFAULT_RESOURCES_NAME).hide()
        if not self.compass_action.isChecked():
            self.vehicle_widgets_dict.get(DEFAULT_COMPASS_NAME).hide()
        if not self.roll_pitch_action.isChecked():
            self.vehicle_widgets_dict.get(DEFAULT_ROLL_PITCH_NAME).hide()
        if not self.velocimeter_action.isChecked():
            self.vehicle_widgets_dict.get(DEFAULT_VELOCIMETER_NAME).hide()
        if not self.depth_altitude_action.isChecked():
            self.vehicle_widgets_dict.get(DEFAULT_DEPTH_ALTITUDE_NAME).hide()
        if not self.table_action.isChecked():
            self.vehicle_widgets_dict.get(DEFAULT_TABLE_NAME).hide()
        if not self.setpoints_action.isChecked():
            self.vehicle_widgets_dict.get(DEFAULT_SETPOINTS_NAME).hide()

        self.connected = True
        self.start_updating_data()

    def start_updating_data(self) -> None:
        """ Starts updating data of the table widget"""
        self.vehicle_widgets_dict.get(DEFAULT_TABLE_NAME).connect()
        if not self.timer.isActive():
            self.timer.start(1000)

    def start_timer(self) -> None:
        """ emits refresh_signal if connected """
        if self.connected:
            self.refresh_signal.emit()

    def refresh(self) -> None:
        """ Updates all widgets with the info provided by vehicle_data """

        data = self.vehicle_data.get_nav_sts()
        if is_navigation_new(data):
            self.vehicle_widgets_dict.get(DEFAULT_DEPTH_ALTITUDE_NAME).set_data(data)
            self.vehicle_widgets_dict.get(DEFAULT_COMPASS_NAME).set_data(data)
            self.vehicle_widgets_dict.get(DEFAULT_ROLL_PITCH_NAME).set_data(data)
            self.vehicle_widgets_dict.get(DEFAULT_VELOCIMETER_NAME).set_data(data)

        battery_charge = self.vehicle_data.get_battery_charge()
        cpu_usage = self.vehicle_data.get_cpu_usage()
        ram_usage = self.vehicle_data.get_ram_usage()
        self.vehicle_widgets_dict.get(DEFAULT_RESOURCES_NAME).set_data(battery_charge, cpu_usage, ram_usage)

        thruster_setpoints = self.vehicle_data.get_thruster_setpoints()

        if thruster_setpoints is not None and thruster_setpoints['valid_data'] == 'new_data':
            self.vehicle_widgets_dict.get(DEFAULT_SETPOINTS_NAME).set_data(thruster_setpoints)

    def mousePressEvent(self, event) -> None:
        """ Override mousePressEvent. Handles starting drag and drop and creating context menus """
        if event.button() == Qt.LeftButton:
            self.start_drag_position = event.pos()

        elif event.button() == Qt.RightButton:
            self.menu_widgets.popup(self.mapToGlobal(event.pos()))

    def mouseMoveEvent(self, event) -> None:
        """ Override mouseMoveEvent. Handles dragging of widgets """
        if event.buttons() and Qt.LeftButton:
            if self.start_drag_position is not None and \
                    (event.pos() - self.start_drag_position).manhattanLength() < QApplication.startDragDistance():
                pos_global = self.mapToGlobal(event.pos())
                self.start_drag_and_drop(pos_global)

    def dragEnterEvent(self, event) -> None:
        """ Override dragEnterEvent. Accepts the event drag and drop action """
        event.acceptProposedAction()

    def dragLeaveEvent(self, event):
        """ Override dragLeaveEvent. Hides the rubber band """
        if self.rubber_band is not None:
            self.rubber_band.hide()

    def dragMoveEvent(self, event):
        """ Override dragMoveEvent. Calls in_between """
        if self.dragging is not None:
            self.in_between(event.pos())

    def dropEvent(self, event) -> None:
        """ Override dropEvent. Ends drag and drop event """
        event.acceptProposedAction()
        if self.dragging is not None:
            self.end_drag_and_drop(event.pos())
            self.dragging = None

    def start_drag_and_drop(self, pos) -> None:
        """
        If a widget is clicked, starts drag and drop for that widget.
        :param pos: Point of the drag and drop event in global position
        :type pos: QPoint
        """

        pos_container = self.scrollAreaWidgetContents.mapFromGlobal(pos)
        pos_vehiclewidgets = self.mapFromGlobal(pos)

        self.rubber_band = QRubberBand(QRubberBand.Line)
        self.rubber_band.resize(1, 150)
        self.area_mid_point = self.scrollArea_widgets.mapToGlobal(
            QPoint(self.scrollArea_widgets.width() / 2, self.scrollArea_widgets.height() / 2))

        for widget in self.vehicle_widgets_dict.values():
            if widget.geometry().contains(pos_vehiclewidgets) and widget.isVisible():
                self.start_drag_event(pos_container, widget)
                break

    def start_drag_event(self, pos, widget) -> None:
        """
        Creates a miniature image of the widget to indicate dragging action.
        :param pos: Point of the drag and drop event
        :type pos: QPoint
        :param widget: Widget being dragged
        :type widget: QWidget
        """
        self.dragging = widget

        drag = QDrag(self)
        mdata = QMimeData()

        pos_global = self.mapToGlobal(pos)
        screen = QGuiApplication.screenAt(pos_global)

        mainwindow = self.parentWidget().parentWidget()
        top_left_corner = self.scrollAreaWidgetContents.mapToGlobal(widget.geometry().topLeft())
        tlc_from_mw = mainwindow.mapFromGlobal(top_left_corner)

        img = screen.grabWindow(mainwindow.winId(), x=tlc_from_mw.x(), y=tlc_from_mw.y(),
                                height=widget.height(), width=widget.width())

        pixmap = QPixmap(img.size())
        pixmap.fill(Qt.transparent)
        painter = QPainter(pixmap)
        painter.setOpacity(0.7)
        painter.drawPixmap(0, 0, img)
        painter.end()

        drag.setMimeData(mdata)
        drag.setPixmap(pixmap)

        pos_global = self.mapToGlobal(pos)
        hotspot_x = pos_global.x() - top_left_corner.x()
        hotspot_y = pos_global.y() - top_left_corner.y()
        drag.setHotSpot(QPoint(hotspot_x, hotspot_y))

        drag.exec_()

    def in_between(self, pos):
        """
        Checks if the position is in between widgets or at the far left and far right side. If it is, shows the
        rubber band in that position
        :param pos: position to check
        :type pos: QPoint
        """
        pos_global = self.mapToGlobal(pos)
        pos_container = self.scrollAreaWidgetContents.mapFromGlobal(pos_global)

        w_list = []
        for i in range(0, self.layout.count()):
            if not self.layout.itemAt(i).widget().isHidden():
                w_list.append(self.layout.itemAt(i).widget())

        found = False

        # check if pos is to the left of first widget
        right = w_list[0].geometry().bottomLeft()
        if pos_container.x() < right.x() + 20:
            point = self.scrollAreaWidgetContents.mapToGlobal(right)
            self.rubber_band.move(point.x(), self.area_mid_point.y() - (self.rubber_band.height() / 2))
            self.rubber_band.show()
            found = True

        if not found:
            for i in range(0, len(w_list) - 1):
                # check if pos is in between two widgets

                left = w_list[i].geometry().bottomRight().x()
                right = w_list[i + 1].geometry().bottomLeft().x()
                if left - 20 < pos_container.x() < right + 20:
                    mid_point_x = self.scrollAreaWidgetContents.mapToGlobal(w_list[i].geometry().bottomRight()).x()
                    self.rubber_band.move(mid_point_x, self.area_mid_point.y() - (self.rubber_band.height() / 2))
                    self.rubber_band.show()
                    found = True
                    break

        # check if pos is to the right of last widget
        if not found:
            left = w_list[len(w_list) - 1].geometry().bottomRight()
            if left.x() - 20 < pos_container.x():
                point = self.scrollAreaWidgetContents.mapToGlobal(left)
                self.rubber_band.move(point.x(), self.area_mid_point.y() - (self.rubber_band.height() / 2))
                self.rubber_band.show()
                found = True

        if not found:
            self.rubber_band.hide()

    def end_drag_and_drop(self, pos):
        """
        If pos is over the far left and right side of a widget, insert dragging widget into that position
        :param pos: position to check
        :type pos: QPoint
        """
        pos_global = self.mapToGlobal(pos)
        pos_container = self.scrollAreaWidgetContents.mapFromGlobal(pos_global)
        # Find the widget where the dragged widget is dropped
        released_over = None
        for item in self.vehicle_widgets_dict.values():
            if item.geometry().marginsAdded(QMargins(7, 0, 7, 0)).contains(pos_container):
                released_over = item
                break

        if released_over is not None:
            # Find on which side of the widget is the dragged widget dropped
            if self.dragging != released_over:
                left = released_over.geometry().bottomLeft().x()
                right = released_over.geometry().bottomRight().x()
                if abs(pos_container.x() - left) < 25:
                    self.layout.takeAt(self.layout.indexOf(self.dragging))
                    self.layout.insertWidget(self.layout.indexOf(released_over), self.dragging)
                elif abs(pos_container.x() - right) < 25:
                    self.layout.takeAt(self.layout.indexOf(self.dragging))
                    self.layout.insertWidget(self.layout.indexOf(released_over) + 1, self.dragging)

                # save current layout order to settings
                self.save_layout_order()
        self.rubber_band.hide()

    def save_layout_order(self) -> None:
        """ Save current layout order to settings """
        settings = QSettings()
        layout_order = []
        for i in range(self.layout.count()):
            layout_order.append(self.layout.itemAt(i).widget().objectName())

        settings.setValue("VehicleWidgets/layout_order", layout_order)

    def stop_timer(self) -> None:
        """ Stop timer to update data"""
        self.timer.stop()

    def disconnect_object(self) -> None:
        """
        Disconnect all objects
        """
        # clear previous widgets
        for i in reversed(range(self.layout.count())):
            self.layout.itemAt(i).widget().setParent(None)
        # disconnect
        self.connected = False
        # close thread and timer
        self.stop_timer_signal.emit()
        if self.vehicle_widgets_dict.get(DEFAULT_TABLE_NAME):
            self.vehicle_widgets_dict.get(DEFAULT_TABLE_NAME).disconnect_object()
