# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Class to trigger the service to calibrate the vehicle's magnetometer
"""

import logging
import os
import subprocess
import yaml
from PyQt5.QtCore import pyqtSignal, QObject, QTimer
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QDialog, QMessageBox
from iquaview.src.ui.ui_imu_calibration import Ui_CalibrationIMU
from iquaview_lib.cola2api.cola2_interface import send_trigger_service, set_ros_param
from iquaview_lib.definitions import ROSBRIDGE_SERVER_PORT
from iquaview_lib.vehicle.vehicledata import VehicleData
from iquaview_lib.vehicle.vehicleinfo import VehicleInfo

logger = logging.getLogger(__name__)

ACTIVE = 0
SUCCESS = 1
FAILURE = 2
STOPPED = 3

CALIBRATION_FILENAME = "calibration_iron.yaml"


class CalibrateMagnetometer(QObject):
    calibrate_magnetometer_signal = pyqtSignal(bool, str)
    success_signal = pyqtSignal(str)
    timer_disconnected_signal = pyqtSignal()
    stop_timer_signal = pyqtSignal()

    def __init__(self, vehicle_info: VehicleInfo, vehicle_data: VehicleData) -> None:
        super().__init__()

        self.ip = vehicle_info.get_vehicle_ip()
        self.vehicle_namespace = vehicle_info.get_vehicle_namespace()
        self.vehicle_data = vehicle_data
        self.vehicle_info = vehicle_info

        self.timer = QTimer()
        self.timer.timeout.connect(self.refresh_feedback)

        self.success_signal.connect(self.on_success_calibration)
        self.stop_timer_signal.connect(self.stop_timer)

    def start_calibrate_magnetometer(self) -> None:
        """
        Start calibrate magnetometer in a new thread.
        Send service to start calibrate magnetometer
        """
        try:
            enable_thrusters = self.vehicle_data.get_enable_thrusters_service()
            calibrate_magnetometer = self.vehicle_data.get_calibrate_magnetometer_service()
            thruster = send_trigger_service(self.ip, ROSBRIDGE_SERVER_PORT, self.vehicle_namespace + enable_thrusters)
            response = send_trigger_service(
                self.ip, ROSBRIDGE_SERVER_PORT, self.vehicle_namespace + calibrate_magnetometer
            )

            self.calibrate_magnetometer_signal.emit(response["values"]["success"], response["values"]["message"])

        except:
            self.calibrate_magnetometer_signal.emit(False, "")

    def start_updating_data(self) -> None:
        if not self.timer.isActive():
            self.timer.start(1000)

    def refresh_feedback(self) -> None:
        """update state of state feedback"""
        if not self.vehicle_data.is_subscribed_to_topic("state feedback"):
            self.vehicle_data.subscribe_topic("state feedback")

        state_feedback = self.vehicle_data.get_captain_state_feedback()

        if state_feedback is not None and state_feedback["valid_data"] == "new_data":
            state = state_feedback["state"]
            # if state == ACTIVE:
            if state == SUCCESS:
                if state_feedback["keyvalues"][0]["key"] == "file_name":
                    file_name = state_feedback["keyvalues"][0]["value"]
                self.success_signal.emit(file_name)
                self.disconnect_timer()
                logger.info("SUCCESS")

            if state == FAILURE:
                self.disconnect_timer()
                logger.info("FAILURE")

            if state == STOPPED:
                self.disconnect_timer()
                logger.info("STOPPED")

    def on_success_calibration(self, filename_yaml: str) -> None:
        path_calibrations = os.path.join(self.vehicle_info.get_remote_vehicle_package(), "calibrations")
        path_yaml = os.path.join(path_calibrations, filename_yaml)

        filename_png = filename_yaml.replace(".yaml", ".png")
        path_png = os.path.join(path_calibrations, filename_png)

        path_temp = "/tmp"
        path_temp_png = os.path.join(path_temp, filename_png)
        path_temp_yaml = os.path.join(path_temp, filename_yaml)

        server = f"{self.vehicle_info.get_vehicle_user()}@{self.vehicle_info.get_vehicle_ip()}"
        subprocess.Popen(
            [
                "scp",
                "-B",  # Selects batch mode (prevents asking for passwords or passphrases).
                f"{server}:{path_png}",
                path_temp,  # local temp for visualization
            ]
        ).wait()

        calibration_imu_dialog = CalibrationIMUDialog(path_temp_png)
        result = calibration_imu_dialog.exec_()
        if result == QDialog.Accepted:
            reply = QMessageBox.question(
                None,
                "Calibration Confirmation",
                "Do you want to use this calibration?",
                QMessageBox.Yes,
                QMessageBox.No,
            )
            if reply == QMessageBox.Yes:
                # copy calibration file to the path that is loaded in the launchfile
                path_destination_yaml = os.path.join(path_calibrations, CALIBRATION_FILENAME)
                subprocess.Popen(
                    [
                        "scp",
                        "-B",
                        f"{server}:{path_yaml}",
                        f"{server}:{path_destination_yaml}",
                    ]
                ).wait()

                # copy calibration file to the local temp
                subprocess.Popen(
                    [
                        "scp",
                        "-B",
                        f"{server}:{path_yaml}",
                        path_temp_yaml,
                    ]
                ).wait()

                # rosparam load new calibration
                with open(path_temp_yaml, "r") as f:
                    last_calibration_file = yaml.safe_load(f)
                    set_ros_param(
                        self.ip,
                        ROSBRIDGE_SERVER_PORT,
                        self.vehicle_namespace + "/imu_angle_estimator/hard_iron",
                        str(last_calibration_file["hard_iron"]),
                    )

                    set_ros_param(
                        self.ip,
                        ROSBRIDGE_SERVER_PORT,
                        self.vehicle_namespace + "/imu_angle_estimator/soft_iron",
                        str(last_calibration_file["soft_iron"]),
                    )

                response = send_trigger_service(
                    self.ip, ROSBRIDGE_SERVER_PORT, self.vehicle_namespace + "/imu_angle_estimator/reload_params"
                )
                if not response["values"]["success"]:
                    QMessageBox.critical(self, "Reload params failed", response["values"]["message"], QMessageBox.Close)

    def stop_magnetometer_calibration(self) -> None:
        """Stop magnetometer calibration"""
        stop_magnetometer_calibration_service = self.vehicle_data.get_stop_magnetometer_calibration_service()
        response = send_trigger_service(
            self.ip, ROSBRIDGE_SERVER_PORT, self.vehicle_namespace + stop_magnetometer_calibration_service
        )
        if not response["values"]["success"]:
            QMessageBox.critical(
                self, "Stop magnetometer calibration failed", response["values"]["message"], QMessageBox.Close
            )

    def stop_timer(self) -> None:
        """Stop timer to update data"""
        self.timer.stop()

    def disconnect_timer(self) -> None:
        """Disconnect timer and unsubscribe state feedback"""
        self.stop_timer_signal.emit()
        if self.vehicle_data.is_subscribed_to_topic("state feedback"):
            self.vehicle_data.unsubscribe_topic("state feedback")
        self.timer_disconnected_signal.emit()


class CalibrationIMUDialog(QDialog, Ui_CalibrationIMU):
    def __init__(self, filename, parent=None) -> None:
        super().__init__(parent)
        self.setupUi(self)
        self.filename = filename

        pixmap = QPixmap(filename)
        self.image_label.setPixmap(pixmap)
