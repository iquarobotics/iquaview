# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.


"""
"""

import logging

from PyQt5.QtWidgets import QWidget, QComboBox, QSpinBox, QDoubleSpinBox
from PyQt5.QtCore import QEvent

from iquaview_lib.ui.ui_param import Ui_ParamWidget
from iquaview_lib.utils.textvalidator import (validate_param_range,
                                              get_color,
                                              check_line_edit_starts_by_slash)

from qgis.gui import QgsCollapsibleGroupBox

logger = logging.getLogger(__name__)


class ParamWidget(QgsCollapsibleGroupBox, Ui_ParamWidget):
    def __init__(self, name="", description="Param", action_id=None, id=None, collapsed=False, section=None,
                 parent=None):
        """
        Constructor
        """
        super().__init__(parent)
        self.setupUi(self)
        self.setObjectName(str(id))
        self.section_widget = section
        self.setWindowTitle("ParamWidget")
        self.setTitle(description)
        self.setCollapsed(collapsed)

        if not collapsed:
            self.show()

        self.type_comboBox.installEventFilter(self)
        self.min_doubleSpinBox.installEventFilter(self)
        self.max_doubleSpinBox.installEventFilter(self)
        self.size_spinBox.installEventFilter(self)
        self.description_lineEdit.setText(description)
        self.action_id_lineEdit.setText(action_id)
        self.action_id_lineEdit.textChanged.connect(self.check_action_id)
        self.name_lineEdit.setText(name)
        self.name_lineEdit.textChanged.connect(self.check_name)

        self.description_lineEdit.textChanged.connect(self.setTitle)

        self.type_comboBox.currentIndexChanged.connect(self.validate_range)
        self.range_lineEdit.textChanged.connect(self.validate_range)

    def get_section_widget(self):
        return self.section_widget

    def validate_range(self):
        """validate param range"""

        int_values = True if self.type_comboBox.currentText() == "int" else False
        state = validate_param_range(self.range_lineEdit.text(), int_values)
        color = get_color(state)
        self.range_lineEdit.setStyleSheet('QLineEdit { background-color: %s }' % color)

    def eventFilter(self, source, event):
        # event filter to ignore wheel on combobox
        if (event.type() == QEvent.Wheel
                and (isinstance(source, QComboBox)
                     or isinstance(source, QSpinBox)
                     or isinstance(source, QDoubleSpinBox))):
            event.ignore()
            return True
        else:
            return QWidget.eventFilter(self, source, event)

    def check_action_id(self, text):
        """ Check that action id start by / """
        check_line_edit_starts_by_slash(self.action_id_lineEdit)

    def check_name(self, text):
        """ Check that name start by / """
        check_line_edit_starts_by_slash(self.name_lineEdit)
