#!/usr/bin/env python
# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Object to monitor the position of the AUV provided that there is Wi-Fi (or umbilical) connection.
"""
import logging
import math
import os
import subprocess
import threading
import time

from PyQt5.QtCore import QObject, Qt, QThreadPool, QTimer, pyqtSignal
from PyQt5.QtGui import QColor, QCursor, QIcon, QPixmap
from PyQt5.QtWidgets import QAction, QDialog, QDockWidget, QMenu, QMenuBar, QMessageBox, QSizePolicy, QToolBar
from qgis.core import QgsMessageLog, QgsPointXY, QgsProject
from qgis.gui import QgsMapCanvas

from iquaview.src.mapsetup import nedorigindrawer, virtualcagedrawer, virtualcageeditor
from iquaview.src.mission import missionactive
from iquaview.src.mission.missioncontroller import MissionController
from iquaview.src.tools import getpointtool
from iquaview.src.vehicle import calibratemagnetometer, goto, keeppositionstatus, thrustersstatus, timeoutwidget
from iquaview.src.vehicle.auvdriver import AUVDriver
from iquaview.src.vehicle.auvposeentity import AUVEntity
from iquaview.src.vehicle.checklist import check_list, check_list_selector
from iquaview.src.vehicle.config_auv_driver import ConfigAUVDriver
from iquaview.src.vehicle.mission_report import MissionReport
from iquaview.src.vehicle.syncstatus import SyncStatus
from iquaview.src.vehicle.vehiclewidgets import vehiclewidgets
from iquaview_lib.baseclasses.webserverbase import WebServerPoint
from iquaview_lib.canvasdrawer.canvasmarker import CanvasMarker
from iquaview_lib.canvasdrawer.canvastrack import CanvasTrack
from iquaview_lib.canvasdrawer.canvastrackstyle import CanvasTrackStyle
from iquaview_lib.cola2api import cola2_interface
from iquaview_lib.cola2api.basic_message import BasicMessage
from iquaview_lib.config import Config
from iquaview_lib.connection.sftp.sftp_connection_dialog import SFTPConnectionDialog
from iquaview_lib.definitions import ROSBRIDGE_SERVER_PORT
from iquaview_lib.entity.entitiesmanager import EntitiesManager
from iquaview_lib.utils.workerthread import Worker
from iquaview_lib.vehicle.auvconfigparams import AUVConfigParamsDlg
from iquaview_lib.vehicle.vehicledata import VehicleData, VehicleTopicKey, is_gps_new, is_navigation_new
from iquaview_lib.vehicle.vehicleinfo import VehicleInfo
from iquaview_lib.vehicle.vehiclestatus import VehicleStatus

logger = logging.getLogger(__name__)


class AUVPose(QObject):
    auv_wifi_connected_signal = pyqtSignal(bool)
    stop_timer_signal = pyqtSignal()
    basic_message_signal = pyqtSignal(BasicMessage)

    def __init__(self,
                 entities_manager: EntitiesManager,
                 canvas: QgsMapCanvas,
                 config: Config,
                 vehicle_info: VehicleInfo,
                 vehicle_data: VehicleData,
                 vehicle_status: VehicleStatus,
                 msg_log: QgsMessageLog,
                 proj: QgsProject,
                 menubar: QMenuBar,
                 before_action: QAction,
                 mission_ctrl: MissionController,
                 parent=None) -> None:
        """
        Init of the object AUVPose
        :param entities_manager: class is used to manage the entities
        :type entities_manager: EntitiesManager
        :param canvas: canvas to update markers and tracks into
        :type canvas: QgsMapCanvas
        :param config: iquaview configuration
        :type config: Config
        :param vehicle_info: info of the vehicle
        :type vehicle_info: VehicleInfo
        :param vehicle_data: data from the vehicle
        :type vehicle_data: VehicleData
        :param vehicle_status: status of the vehicle
        :type vehicle_status: VehicleStatus
        :param msg_log:  Interface for logging messages from QGIS in GUI independent way
        :type msg_log: QgsMessageLog
        :param proj: Encapsulates a QGIS project, including sets of map layers and their styles,
                     layouts, annotations, canvases, etc.
        :type proj: QgsProject
        :param menubar: A menu bar consists of a list of pull-down menu items.
        :type menubar: QMenuBar
        :param before_action: insert before action before_action
        :type before_action: QAction
        :param mission_ctrl: mission controller to get the missions from
        :type mission_ctrl: MissionController
        :param parent: parent widget
        :type parent: QWidget
        """

        super().__init__(parent)

        self.entities_manager = entities_manager
        self.canvas = canvas
        self.config = config
        self.vehicle_info = vehicle_info
        self.vehicle_data = vehicle_data
        self.vehicle_status = vehicle_status
        self.msg_log = msg_log
        self.proj = proj
        self.mission_ctrl = mission_ctrl
        self.default_color = Qt.yellow
        self.driver = None
        # memory
        self.last_data = BasicMessage()
        # process launch teleoperation
        self.process_teleop = None
        # process rqt_robot_monitor
        self.process_robot_monitor = None
        self.timer_robot_monitor = None

        self.sftp_transfer_dialog = None
        self.virtual_cage_dock = None
        self.virtual_cage_editor = None

        self.threadpool = QThreadPool()
        self.timer = QTimer()
        self.timer.timeout.connect(self.auv_pose_update_canvas)

        self.vehicle_menu = QMenu("Vehicle")
        menubar.insertMenu(before_action, self.vehicle_menu)

        # Actions for Vehicle (Wi-Fi)
        self.auv_config_parameters_action = QAction(QIcon(":/resources/mActionAUVConfigParameters.svg"),
                                                    "AUV Configuration Parameters...", self)
        self.checklist_action = QAction(QIcon(":/resources/mActionCheckList.svg"), "Check List...", self)
        self.joystick_teleop_action = QAction(QIcon(":/resources/joystick.png"), "Enable Joystick Teleoperation", self)
        self.joystick_teleop_action.setCheckable(True)
        self.joystick_teleop_action.setShortcut("Ctrl+j")

        if self.vehicle_data.get_calibrate_magnetometer_service() is not None:
            self.calibrate_magnetometer_action = QAction(QIcon(":/resources/compass.svg"),
                                                         "Calibrate Magnetometer",
                                                         self)
            self.calibrate_magnetometer_action.setCheckable(True)

        self.robot_monitor_action = QAction(QIcon(":/resources/robot_monitor.svg"), "Open Robot Monitor...", self)
        self.robot_monitor_action.setCheckable(True)
        self.robot_monitor_action.setShortcut("Ctrl+r")
        self.move_ned_origin_action = QAction(QIcon(":/resources/NED_move.svg"), "Move NED Origin", self)
        self.move_ned_origin_action.setCheckable(True)
        self.virtual_cage_action = QAction(QIcon(":/resources/virtual_cage.svg"), "Virtual Cage Editor", self)
        self.virtual_cage_action.setCheckable(True)

        self.reset_timeout_action = QAction(QIcon(":/resources/actionResetTimeout.svg"), "Reset Timeout", self)

        self.auv_pose_action = QAction(QIcon(":/resources/"
                                             + self.vehicle_info.get_vehicle_type()
                                             + "/mActionAUVPose.svg"),
                                       "Monitor AUV Pose",
                                       self)

        self.auv_pose_action.setCheckable(True)
        self.auv_pose_action.setShortcut("Ctrl+a")
        self.enable_keep_position_action = QAction(QIcon(":resources/mActionEnableKeepPosition.svg"),
                                                   "Enable Keep Position", self)
        self.enable_keep_position_action.setCheckable(True)
        self.enable_thrusters_action = QAction(QIcon(":resources/mActionEnableThrusters.svg"), "Enable Thrusters",
                                               self)
        self.enable_thrusters_action.setCheckable(True)
        self.goto_action = QAction(QIcon(":resources/goto.svg"), "Go To...", self)
        self.goto_action.setCheckable(True)
        self.goto_action.setShortcut("Ctrl+g")

        self.transfer_bag_action = QAction("Transfer Bag...", self)

        self.mission_report = MissionReport(self.vehicle_info, parent=parent)

        self.vehicle_menu.addActions([self.auv_config_parameters_action,
                                      self.checklist_action,
                                      self.joystick_teleop_action])
        if self.vehicle_data.get_calibrate_magnetometer_service() is not None:
            self.vehicle_menu.addAction(self.calibrate_magnetometer_action)
        self.vehicle_menu.addActions([self.robot_monitor_action,
                                      self.move_ned_origin_action,
                                      self.virtual_cage_action,
                                      self.reset_timeout_action,
                                      self.transfer_bag_action,
                                      self.auv_pose_action,
                                      self.enable_keep_position_action,
                                      self.enable_thrusters_action,
                                      self.goto_action,
                                      self.mission_report.get_get_report_action(),
                                      self.mission_report.get_open_report_action()])

        self.vehicle_widgets_action = QAction("Vehicle Widgets", self)
        self.vehicle_widgets_action.setCheckable(True)

        # dock for Graphical vehicle Widgets
        self.vehicle_w_dock = QDockWidget("Vehicle Widgets")
        self.vehicle_w_dock.setObjectName("Vehicle Widgets Dock")
        self.vehicle_w_dock.setAllowedAreas(Qt.BottomDockWidgetArea | Qt.TopDockWidgetArea)
        self.vehicle_w_dock.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.vehicle_w_dock.setContentsMargins(9, 9, 9, 9)
        self.vehicle_w_dock.setStyleSheet("QDockWidget { font: bold; }")
        self.vehicle_widgets = vehiclewidgets.VehicleWidgets(self.vehicle_info, self.vehicle_data)
        self.vehicle_w_dock.setWidget(self.vehicle_widgets)
        self.vehicle_w_dock.visibilityChanged.connect(self.vehicle_widgets.vehicle_widgets_dock_visibility_changed)
        self.vehicle_w_dock.visibilityChanged.connect(self.vehicle_widgets_action.setChecked)
        self.parent().addDockWidget(Qt.BottomDockWidgetArea, self.vehicle_w_dock)

        view_menu_widgets = self.parent().view_menu.addMenu("Vehicle Widgets")
        view_menu_widgets.addAction(self.vehicle_widgets_action)
        view_menu_widgets.addSeparator()
        view_menu_widgets.addActions(self.vehicle_widgets.get_menu_actions())

        # thrusters status
        self.thrusters_status = thrustersstatus.ThrustersStatus(self.vehicle_data)
        self.thrusters_status.thrusters_signal.connect(self.change_icon_thrusters)

        # keep position
        self.keep_position_status = keeppositionstatus.KeepPositionStatus(self.vehicle_data, self.msg_log)
        self.keep_position_status.keep_position_signal.connect(self.change_icon_keep_position)

        # sync status
        self.sync_status = SyncStatus(self.vehicle_info)
        self.sync_status.update_signal.connect(self.set_sync)

        # mission active status
        self.mission_active = missionactive.MissionActive(self.config,
                                                          self.vehicle_data,
                                                          self.vehicle_info,
                                                          self.mission_ctrl,
                                                          self.thrusters_status,
                                                          self.keep_position_status,
                                                          self.sync_status,
                                                          self.joystick_teleop_action,
                                                          self.enable_thrusters_action,
                                                          self.enable_keep_position_action,
                                                          self.parent())
        self.mission_active.terminate_teleoperation_signal.connect(self.terminate_teleoperation)

        # goto
        self.goto_dialog = goto.GoToDialog(self.config,
                                           self.canvas,
                                           self.vehicle_info.get_vehicle_ip(),
                                           ROSBRIDGE_SERVER_PORT,
                                           self.vehicle_info.get_vehicle_namespace(),
                                           self.vehicle_data,
                                           self.thrusters_status,
                                           self.keep_position_status,
                                           self.enable_thrusters_action,
                                           self.enable_keep_position_action,
                                           self.parent())
        self.goto_dialog.going_signal.connect(self.change_icon_goto)
        self.goto_dialog.map_tool_change_signal.connect(self.parent().reset_map_tool)
        self.goto_dialog.dialog_finished_signal.connect(self.parent().pan)

        self.vehicle_menu.addMenu(self.mission_active.get_execute_mission_menu())
        self.vehicle_menu.addAction(self.mission_active.get_pause_mission_action())
        self.vehicle_menu.addMenu(self.mission_active.get_stop_mission_menu())

        # Toolbar for Vehicle (Wi-Fi)
        self.auvwifi_toolbar = QToolBar("Vehicle tools (WiFi)")
        self.auvwifi_toolbar.setObjectName("Vehicle tools (WiFi)")

        self.auvwifi_toolbar.addAction(self.auv_config_parameters_action)
        self.auvwifi_toolbar.addAction(self.checklist_action)
        self.auvwifi_toolbar.addAction(self.joystick_teleop_action)
        if self.vehicle_data.get_calibrate_magnetometer_service() is not None:
            self.auvwifi_toolbar.addAction(self.calibrate_magnetometer_action)
        self.auvwifi_toolbar.addAction(self.robot_monitor_action)
        self.auvwifi_toolbar.addAction(self.move_ned_origin_action)
        self.auvwifi_toolbar.addAction(self.virtual_cage_action)
        self.auvwifi_toolbar.addAction(self.reset_timeout_action)
        self.timeout_widget = timeoutwidget.TimeoutWidget(self.vehicle_info, self.vehicle_data, self.config,
                                                          self.parent())
        self.auvwifi_toolbar.addWidget(self.timeout_widget)

        self.auvwifi_toolbar.addAction(self.auv_pose_action)
        self.auvwifi_toolbar.addAction(self.enable_keep_position_action)
        self.auvwifi_toolbar.addAction(self.enable_thrusters_action)
        self.auvwifi_toolbar.addAction(self.goto_action)
        self.auvwifi_toolbar.addWidget(self.mission_report.get_tool_button())
        # self.auvwifi_toolbar.addAction(self.execute_mission_action)

        self.auvwifi_toolbar.addWidget(self.mission_active.get_execute_mission_toolbutton())
        self.auvwifi_toolbar.addAction(self.mission_active.get_pause_mission_action())
        self.auvwifi_toolbar.addWidget(self.mission_active.get_stop_mission_toolbutton())

        self.parent().addToolBar(self.auvwifi_toolbar)

        # Handle for NED origin drawer
        self.ned_origin_drawer = nedorigindrawer.NEDOriginDrawer(self.vehicle_info, self.proj, self.canvas, self.config,
                                                                 self.parent())
        self.ned_origin_drawer.landmark_added.connect(self.parent().add_map_layer)
        self.ned_origin_drawer.map_tool_change_signal.connect(self.parent().reset_map_tool)
        self.ned_origin_drawer.finish_add_landmark_signal.connect(self.finish_ned_origin)
        # Connect NED move tool to the ned origin drawer
        # Handle for virtual cage drawer
        self.virtual_cage_drawer = virtualcagedrawer.VirtualCageDrawer(self.proj, self.vehicle_info,
                                                                       self.canvas, self.ned_origin_drawer,
                                                                       self.parent())

        self.tool_virtual_cage = getpointtool.GetPointTool(self.canvas)
        self.tool_virtual_cage.setCursor(QCursor(QPixmap(["16 16 3 1",
                                                          "      c None",
                                                          "+     c #000000",
                                                          ".     c #008000",
                                                          "                ",
                                                          "        .       ",
                                                          "        .       ",
                                                          "        .       ",
                                                          "   +    .    +  ",
                                                          "    +   .   +   ",
                                                          "     +  .  +    ",
                                                          "      + . +     ",
                                                          "+++++++ . ++++++",
                                                          "      + . +     ",
                                                          "     +  .  +    ",
                                                          "    +   .   +   ",
                                                          "   +    .    +  ",
                                                          "        .       ",
                                                          "        .       ",
                                                          "        .       "])))

        if self.vehicle_data.get_calibrate_magnetometer_service() is not None:
            self.calibrate_magnetometer = calibratemagnetometer.CalibrateMagnetometer(self.vehicle_info,
                                                                                      self.vehicle_data)
            self.calibrate_magnetometer.calibrate_magnetometer_signal.connect(self.calibrate_magnetometer_result)
            self.calibrate_magnetometer.timer_disconnected_signal.connect(self.uncheck_calibrate_magnetometer)

        self.auv_config_parameters_action.triggered.connect(self.auv_config_params)
        self.checklist_action.triggered.connect(self.checklist)
        self.joystick_teleop_action.toggled.connect(self.launch_teleoperation)
        self.move_ned_origin_action.triggered.connect(self.set_move_ned_origin_tool)
        self.virtual_cage_action.toggled.connect(self.set_virtual_cage_tool)
        self.reset_timeout_action.triggered.connect(self.reset_timeout)
        if self.vehicle_data.get_calibrate_magnetometer_service() is not None:
            self.calibrate_magnetometer_action.triggered.connect(self.on_click_calibrate_magnetometer)
        self.robot_monitor_action.triggered.connect(self.robot_monitor)
        self.enable_keep_position_action.triggered.connect(self.enable_keep_position)
        self.enable_thrusters_action.triggered.connect(self.enable_thrusters)
        self.goto_action.triggered.connect(self.goto)
        self.transfer_bag_action.triggered.connect(self.transfer_bag)
        self.vehicle_widgets_action.triggered.connect(self.vehicle_widgets_visibility)
        self.stop_timer_signal.connect(self.stop_timer)

        vehicle_settings = self.entities_manager.get_entity_settings("vehicle")
        if vehicle_settings is None:
            identifier = 0
            position = 0
            config_auv_driver = ConfigAUVDriver()
            config_auv_driver.joystick_device = "/dev/input/js0"

            style = self.get_default_track_style(vehicle_info=vehicle_info)

        else:
            position, item = vehicle_settings
            identifier = item.get("id", 0)
            driver_values = item["driver"]
            config_auv_driver = ConfigAUVDriver()
            config_auv_driver.joystick_device = driver_values.get('joystick_device')

            style = self.get_default_track_style(vehicle_info=vehicle_info)
            if 'canvasdrawer' in item:
                style.from_dict(item['canvasdrawer']['style'])
            elif 'style' in item:
                style_dict = item['style']
                style_dict['color_hex_str'] = style_dict['color']
                style_dict.pop('color')
                item.pop('style')
                style.from_dict(style_dict)

        self.chrony_available = self.vehicle_data.is_topic_in_config(VehicleTopicKey.ChronyTracking)
        self.driver = AUVDriver(config_auv_driver, self.config, self.vehicle_info)
        self.auventity = AUVEntity(
            id=identifier,
            position=position,
            name=self.vehicle_info.get_vehicle_name(),
            visible=True,
            driver=self.driver,
            canvasdrawer=CanvasTrack(canvas=self.canvas, style=style,
                                     marker=CanvasMarker(canvas=self.canvas, style=style)),
            config=self.config,
            canvas=self.canvas,
            permanent="vehicle",
            connect_when_starts=False,
            chrony_available=self.chrony_available,
        )

        self.entities_manager.append_entity(self.auventity)
        self.auventity.save_changes(self.auventity.properties_widgets())
        self.auventity.auv_connect_signal.connect(self.emit_connection)
        self.auventity.auv_disconnect_signal.connect(self.emit_connection)
        self.auv_pose_action.triggered.connect(lambda: self.entities_manager.connect_entity(self.auventity))
        self.auventity.entity_updated_signal.connect(self.update_icon_state)
        self.auventity.basic_message_signal.connect(self.timeout_widget.update_usbl_time)
        self.timeout_widget.new_time_signal.connect(self.set_time)

    def get_default_track_style(self, vehicle_info: VehicleInfo):
        return CanvasTrackStyle(color_hex_str=QColor(self.default_color).name(),
                                marker_active=True,
                                marker_svg=f":/resources/{vehicle_info.get_vehicle_type()}/vehicle.svg",
                                marker_width=float(vehicle_info.get_vehicle_width()),
                                marker_length=float(vehicle_info.get_vehicle_length()))

    def update_icon_state(self):
        """
        If vessel is connected set checked boat pose action , otherwise unchecked
        """
        self.auv_pose_action.setChecked(self.auventity.is_connected())

    def emit_connection(self):
        """ Emits a signal to connect if not connected, else disconnects. """
        if not self.auventity.connected:
            self.auv_wifi_connected_signal.emit(True)
        else:
            self.disconnect_object("Disconnected")

    def connect(self):
        """ Sets the widget as connected """
        try:
            self.auventity.set_label(
                identifier=self.auventity.LABEL_STATUS,
                value="Connected",
                color=QColor("green")
            )
            self.auventity.connected = True
            if not self.timer.isActive():
                self.timer.start(1000)
            self.vehicle_status.init_wifi()
            if self.auventity.canvasdrawer.connection_lost_marker.isVisible():
                self.auventity.canvasdrawer.hide_connection_lost_marker()

            self.thrusters_status.update_thrusters_status()
            self.mission_active.update_mission_status()
            self.keep_position_status.update_keep_position_status()
            self.goto_dialog.update_goto_status()
            self.vehicle_widgets.connect()
        except:
            logger.error("No connection with COLA2")
            self.disconnect_object("No connection with COLA2")

    def stop_timer(self):
        """ Stop timer to update data"""
        self.timer.stop()

    def disconnect_object(self, msg=''):
        """
        Sets the widget as disconnected
        :param msg: Sets the status label as msg
        :type msg: str
        """
        self.auv_wifi_connected_signal.emit(False)
        self.stop_timer_signal.emit()
        self.vehicle_status.disconnect_object()
        self.auventity.connected = False
        self.auventity.set_label(
            identifier=self.auventity.LABEL_STATUS,
            value=msg,
            color=QColor("red")
        )
        if not self.auventity.canvasdrawer.connection_lost_marker.isVisible():
            self.auventity.canvasdrawer.show_connection_lost_marker()

        self.auventity.canvasdrawer.position_orientation_signal.emit(
            WebServerPoint(
                name=self.auventity.name,
                connected=self.auventity.connected,
                latitude=0,
                longitude=0,
            )
        )

    def auv_pose_update_canvas(self) -> None:
        """ If connected, this method will update position data to the canvas track and status labels. """
        if self.auventity.connected:
            watchdog = self.vehicle_data.get_watchdog()
            if watchdog is None or watchdog['valid_data'] == 'disconnected':
                self.disconnect_object("No data from AUV")

            if self.chrony_available:
                self.set_chrony_label()

            data = self.vehicle_data.get_nav_sts()
            gps_data = self.vehicle_data.get_gps()
            if is_navigation_new(data) or is_gps_new(gps_data):

                if is_navigation_new(data):
                    tim = float(data['header']['stamp']['secs']) + 1e-9 * float(data['header']['stamp']['nsecs'])
                    lat = float(data['global_position']['latitude'])
                    lon = float(data['global_position']['longitude'])
                    heading = float(data['orientation']['yaw'])
                    depth = float(data['position']['depth'])
                    altitude = float(data['altitude'])
                else:
                    # Log warning message. No navigation, using gps to display position.
                    self.msg_log.logMessage("Navigation not initialized. Using the GPS to display position",
                                            "Navigation not initialized",
                                            3)
                    tim = float(gps_data['header']['stamp']['secs']) + 1e-9 * float(
                        gps_data['header']['stamp']['nsecs'])
                    lat = float(gps_data['latitude'])
                    lon = float(gps_data['longitude'])
                    heading = 0
                    depth = 0
                    altitude = 0

                # emit basic message on change
                mtype = 'S' if self.vehicle_info.get_vehicle_type() == 'sparus2' else 'G'
                sc = self.vehicle_data.get_status_code()
                status_code = 0 if sc is None else sc
                bmsg = BasicMessage(
                    mtype=mtype,
                    time=tim,
                    latitude=lat,
                    longitude=lon,
                    depth=depth,
                    heading_accuracy=heading,
                    command_error=status_code,
                    altitude=altitude,
                    elapsed_time=watchdog['data'],
                )
                if bmsg.time != self.last_data.time:
                    self.basic_message_signal.emit(bmsg)
                    self.last_data = bmsg

                pos = QgsPointXY(lon, lat)
                self.auventity.canvasdrawer.track_update_canvas(
                    pos, heading, epsg_id=4326, update=self.auventity.visible)
                self.auventity.canvasdrawer.position_orientation_signal.emit(
                    WebServerPoint(
                        name=self.auventity.name,
                        connected=self.auventity.connected,
                        latitude=lat,
                        longitude=lon,
                        orientation_deg=math.degrees(heading),
                        labels=[('Depth', f'{depth:.2F} m'),
                                ('Altitude', f'{altitude:.2F} m')]
                    )
                )
                [status, status_color] = self.vehicle_status.get_status()
                if status != "":
                    self.auventity.set_label(
                        identifier=self.auventity.LABEL_STATUS,
                        value=status,
                        color=QColor(status_color)
                    )
                else:
                    if is_navigation_new(data):
                        self.auventity.set_label(
                            identifier=self.auventity.LABEL_STATUS,
                            value="Receiving data from AUV",
                            color=QColor("green")
                        )
                    else:
                        self.auventity.set_label(
                            identifier=self.auventity.LABEL_STATUS,
                            value="No navigation data from AUV",
                            color=QColor("red")
                        )

                self.auventity.set_label(
                    identifier=self.auventity.LABEL_DEPTH,
                    value=f"{depth:.2F} m"
                )
                self.auventity.set_label(
                    identifier=self.auventity.LABEL_ALTITUDE,
                    value=f"{altitude:.2F} m"
                )
            else:
                # Catch new_data and disconnected.
                if data['valid_data'] == 'new_data':
                    self.msg_log.logMessage("Navigation not initialized",
                                            "Navigation not initialized",
                                            3)
                elif data['valid_data'] == 'disconnected':
                    self.msg_log.logMessage("WiFi connection with the AUV has been lost",
                                            "Connection lost",
                                            3)
                self.auventity.set_label(
                    identifier=self.auventity.LABEL_STATUS,
                    value="No navigation data from AUV",
                    color=QColor("red")
                )

    def is_connected(self):
        """
        Returns whether the widget is set as connected
        :return: True if connected
        :rtype: bool
        """
        return self.auventity.connected

    def auv_config_params(self):
        """ Open AUV Config Params dialog."""
        filename = os.path.abspath(os.path.expanduser(self.config.settings['last_auv_config_xml']))
        acpd = AUVConfigParamsDlg(filename, self.vehicle_info)
        acpd.applied_changes.connect(self.timeout_widget.set_timeout)
        try:
            acpd.load_sections()
            acpd.exec_()
            self.ned_origin_drawer.update_ned_point()

        except Exception as e:
            logger.error("Connection with COLA2 could not be established")
            QMessageBox.critical(self.parent(),
                                 "Connection with AUV Failed",
                                 f"Connection with COLA2 could not be established \nException: {e}",
                                 QMessageBox.Close)

    def checklist(self):
        """ Open a Checklist selector Dialog."""
        try:
            cl_selectord = check_list_selector.ChecklistSelectorDlg(self.config)
            result = cl_selectord.exec_()
            if result == QDialog.Accepted:
                check_list.read_configuration(self.vehicle_info,
                                              self.config,
                                              cl_selectord.get_current_check_list())
        except Exception as e:
            logger.error("Connection with COLA2 could not be established")
            QMessageBox.critical(self.parent(),
                                 "Connection with AUV Failed",
                                 f"Connection with COLA2 could not be established \nException: {e}",
                                 QMessageBox.Close)

    def teleop_process(self):
        launch_command = self.vehicle_data.get_teleoperation_launch()
        # get vehicle ip
        ip = self.vehicle_info.get_vehicle_ip()
        my_env = os.environ.copy()
        my_env["ROS_MASTER_URI"] = "http://" + ip + ":11311/"
        # launch a subprocess to teleoperate robot with joystick
        launch = "exec " + launch_command + " joystickDevice:=" + self.auventity.driver.config.joystick_device
        # launch teleoperation on background
        self.process_teleop = subprocess.Popen(launch,
                                               env=my_env,
                                               shell=True)
        self.joystick_teleop_action.setToolTip("Disable Joystick Teleoperation")

    def launch_teleoperation(self):
        """ Launch a teleoperation."""
        if self.joystick_teleop_action.isChecked():
            worker = Worker(self.teleop_process)
            self.threadpool.start(worker)
        else:
            self.terminate_teleoperation()

    def terminate_teleoperation(self):
        if self.process_teleop is not None:
            self.process_teleop.terminate()
            # wait to finish
            self.process_teleop.wait()
            self.process_teleop.kill()
        self.joystick_teleop_action.setToolTip("Enable Joystick Teleoperation")
        self.joystick_teleop_action.setChecked(False)

    def set_move_ned_origin_tool(self):
        """
        Sets getpointtool as the canvas tool if the action is checked, unsets it otherwise.
        When the user selects a point with this tool, it will move the NED origin to that point.
        """
        try:
            self.parent().reset_map_tool()
            if self.move_ned_origin_action.isChecked():
                self.ned_origin_drawer.reset()
                self.ned_origin_drawer.update_ned_point()
                self.ned_origin_drawer.show()
            else:
                self.ned_origin_drawer.reject()

        except Exception as e:
            self.ned_origin_drawer.finish_add_landmark_signal.emit()
            logger.error(f"Error: {e}")
            QMessageBox.critical(self.canvas, "Failure to get NED origin", e.args[0], QMessageBox.Close)

    def finish_ned_origin(self):
        """ Uncheck move ned origin action and set pan as maptool"""
        self.move_ned_origin_action.setChecked(False)
        self.parent().pan()

    def disconnect_virtual_cage_editor(self) -> None:
        """ Disconnect Virtual cage editor."""
        self.virtual_cage_dock = None
        self.virtual_cage_action.setChecked(False)

    def set_virtual_cage_tool(self):
        """
        Sets virtual cage canvas tool if the action is checked, unsets it otherwise.
        """
        self.parent().reset_map_tool()
        try:
            if self.virtual_cage_action.isChecked():
                self.ned_origin_drawer.reject()
                self.canvas.setMapTool(self.tool_virtual_cage)
                if self.virtual_cage_dock is None:
                    self.virtual_cage_editor = virtualcageeditor.VirtualCageEditor(self.config,
                                                                                   self.canvas,
                                                                                   self.ned_origin_drawer,
                                                                                   self.virtual_cage_drawer,
                                                                                   self.parent())
                    self.virtual_cage_editor.init()
                    self.tool_virtual_cage.press_point_signal.connect(self.virtual_cage_editor.set_pressed)
                    self.tool_virtual_cage.move_point_signal.connect(self.virtual_cage_editor.update_spinbox)
                    self.tool_virtual_cage.point_signal.connect(self.virtual_cage_editor.set_release)
                    self.virtual_cage_editor.spinBox.editingFinished.connect(
                        lambda:
                        self.virtual_cage_drawer.update_virtual_cage(
                            self.virtual_cage_editor.spinBox.value()
                        )
                    )
                    self.virtual_cage_editor.spinBox.editingFinished.connect(
                        lambda: self.virtual_cage_drawer.update_param_value(self.virtual_cage_editor.spinBox.value()))

                    self.virtual_cage_dock = QDockWidget("Virtual Cage Drawer", self.parent())
                    self.virtual_cage_dock.setWidget(self.virtual_cage_editor)
                    self.virtual_cage_action.toggled.connect(self.virtual_cage_editor.setVisible)
                    self.virtual_cage_dock.setObjectName("Virtual Cage Drawer")
                    self.virtual_cage_dock.setContentsMargins(9, 9, 9, 9)
                    self.virtual_cage_dock.setStyleSheet("QDockWidget { font: bold; }")
                    self.virtual_cage_dock.setAttribute(Qt.WA_DeleteOnClose)
                    self.virtual_cage_dock.destroyed.connect(self.disconnect_virtual_cage_editor)
                    self.virtual_cage_dock.setFloating(True)
                    self.virtual_cage_dock.move(self.parent().geometry().center())

                self.virtual_cage_dock.show()

            else:
                if self.virtual_cage_dock is not None:
                    self.virtual_cage_dock.hide()
        except Exception as e:
            logger.warning(f"Error: Virtual cage editor. {e}")
            QMessageBox.warning(self.parent(),
                                "Error: Virtual cage editor",
                                f"Error when editing the virtual cage. {e}",
                                QMessageBox.Close)
            self.disconnect_virtual_cage_editor()

    def set_time(self, time: int, color: str):
        """
        Set time label
        :param time: new time
        :type time: int
        :param color: color to set
        :type color: str
        """
        self.auventity.set_label(
            identifier=self.auventity.LABEL_TIMEOUT,
            value=str(time),
            color=QColor(color)
        )

    def set_sync(self):
        """
        Set sync status label
        """
        status = self.sync_status.get_sync_status()
        if status == "Yes":
            color = QColor('green')
        else:
            color = QColor('red')
        self.auventity.set_label(
            identifier=self.auventity.LABEL_SYNC,
            value=status,
            color=color
        )

    def set_chrony_label(self) -> None:
        data = self.vehicle_data.get_chrony_tracking()
        tim = cola2_interface.get_time(data)
        if (data is None) or (tim is None):
            self.auventity.set_label(
                identifier=self.auventity.LABEL_CHRONY,
                value="no data",
                color=QColor("red"),
            )
        else:
            text = data["tracking"]["reference_id"]
            if time.time() - tim > 60.0:
                self.auventity.set_label(
                    identifier=self.auventity.LABEL_CHRONY,
                    value=text,
                    color=QColor("orange"),
                )
            else:
                self.auventity.set_label(
                    identifier=self.auventity.LABEL_CHRONY,
                    value=text,
                    color=QColor("black"),
                )


    def reset_timeout(self):
        """ Reset Timeout."""
        self.timeout_widget.reset_timeout()

    def on_click_calibrate_magnetometer(self):
        """ Start calibrate magnetometer."""

        if self.calibrate_magnetometer_action.isChecked():
            confirmation_msg = "Are you sure you want to initiate the procedure to calibrate the magnetometer?\n\n" \
                               "The vehicle must be submerged and once you start, it will perform clockwise turns " \
                               "during 150 seconds followed by counterclockwise turns for another 150 seconds."
            reply = QMessageBox.question(self.parent(), 'Calibrate Confirmation',
                                         confirmation_msg, QMessageBox.Yes, QMessageBox.No)
            if reply == QMessageBox.Yes:

                calibrate = self.vehicle_data.get_calibrate_magnetometer_service()
                if calibrate is not None:
                    self.calibrate_magnetometer.start_calibrate_magnetometer()
                else:
                    QMessageBox.critical(self.parent(),
                                         "Calibrate Magnetometer error",
                                         "The service 'Calibrate Magnetometer' could not be sent.",
                                         QMessageBox.Close)

            else:
                self.calibrate_magnetometer_action.setChecked(False)
        else:
            self.calibrate_magnetometer.disconnect_timer()
            stop_magnetometer_calibration = self.vehicle_data.get_stop_magnetometer_calibration_service()
            if stop_magnetometer_calibration is not None:
                self.calibrate_magnetometer.stop_magnetometer_calibration()

    def calibrate_magnetometer_result(self, result, message):
        """
        Calibrate Magnetometer result.
        :param result: calibrate magnetometer result
        :type result: bool
        :param message: calibrate magnetometer result
        :type message: dict
        """
        # receive calibrate magnetometer server status

        try:
            if not result:
                if not message:
                    message = "Connection with COLA2 could not be established"
                QMessageBox.critical(self.parent(),
                                     "Calibrate magnetometer failed",
                                     message,
                                     QMessageBox.Close)

            else:
                self.calibrate_magnetometer.start_updating_data()

        # back compatibility
        except Exception as e:
            logger.warning(f"The calibrate magnetometer service response can not be read. {e}")
            QMessageBox.critical(self.parent(),
                                 "Connection with AUV Failed",
                                 "Connection with COLA2 could not be established",
                                 QMessageBox.Close)

        logger.info(f"result {result} {message}")

    def uncheck_calibrate_magnetometer(self):
        """Uncheck calibrate magnetometer"""
        if self.calibrate_magnetometer_action.isChecked():
            self.calibrate_magnetometer_action.setChecked(False)

    def robot_monitor(self):
        """ Open rqt robot monitor."""

        if self.robot_monitor_action.isChecked():
            ip = self.vehicle_info.get_vehicle_ip()
            my_env = os.environ.copy()
            my_env["ROS_MASTER_URI"] = "http://" + ip + ":11311/"
            logger.info(f"ROS_MASTER_URI: {my_env['ROS_MASTER_URI']}")
            # command = "ROS_MASTER_URI=http://"+ ip + ":11311/" #rosrun rqt_robot_monitor rqt_robot_monitor"
            self.process_robot_monitor = subprocess.Popen(
                ['rosrun',
                 'rqt_robot_monitor',
                 'rqt_robot_monitor',
                 f'diagnostics_agg:={self.vehicle_info.get_vehicle_namespace()}/diagnostics_agg'],
                env=my_env)  # shell=True,executable = "/bin/bash")
            self.robot_monitor_action.setToolTip("Close Robot Monitor")
            self.check_robot_monitor()
        else:
            if self.process_robot_monitor is not None:
                self.process_robot_monitor.terminate()
                # wait to finish
                self.process_robot_monitor.wait()
            self.robot_monitor_action.setToolTip("Open Robot Monitor")

    def check_robot_monitor(self):
        """ Check if robot monitor is running."""

        # if exist robot monitor process
        if self.process_robot_monitor is not None:
            # timer to check state
            self.timer_robot_monitor = threading.Timer(3.0, self.check_robot_monitor).start()
            # if robot monitor process state is diferent to none and 0 or less
            if self.process_robot_monitor.poll() is not None and self.process_robot_monitor.poll() <= 0:
                # if exist timer
                if self.timer_robot_monitor:
                    self.timer_robot_monitor.cancel()
                # terminate robot monitor process
                self.process_robot_monitor.terminate()
                # wait to finish
                self.process_robot_monitor.wait()
                self.process_robot_monitor = None
                # change tip and uncheck action
                self.robot_monitor_action.setToolTip("Open Robot Monitor")
                self.robot_monitor_action.setChecked(False)

    def enable_keep_position(self):
        """ Change keep position status."""

        ip = self.vehicle_info.get_vehicle_ip()
        port = ROSBRIDGE_SERVER_PORT
        vehicle_namespace = self.vehicle_info.get_vehicle_namespace()
        if self.keep_position_status.get_keep_position_enabled():

            disable_keep_position = self.vehicle_data.get_disable_all_keep_positions_service()

            if disable_keep_position is not None:
                response = cola2_interface.send_trigger_service(ip, port,
                                                                vehicle_namespace + disable_keep_position)
                try:
                    if not response['values']['success']:
                        QMessageBox.critical(self.parent(),
                                             "Disable keep position failed",
                                             response['values']['message'],
                                             QMessageBox.Close)
                # back compatibility
                except Exception as e:
                    logger.warning(f"Disable keep position response can not be read. {e}")
            else:
                QMessageBox.critical(self.parent(),
                                     "Keep Position error",
                                     "The service 'Disable Keep Position' could not be sent.",
                                     QMessageBox.Close)
        else:
            keep_position = self.vehicle_data.get_keep_position_service()
            if keep_position is not None:
                response = cola2_interface.send_trigger_service(ip, port,
                                                                vehicle_namespace + keep_position)
                try:
                    if not response['values']['success']:
                        QMessageBox.critical(self.parent(),
                                             "Enable keep position failed",
                                             response['values']['message'],
                                             QMessageBox.Close)
                # back compatibility
                except Exception as e:
                    logger.warning(f"Enable keep position response can not be read. {e}")
            else:
                QMessageBox.critical(self.parent(),
                                     "Keep Position error",
                                     "The service 'Keep Position' could not be sent.",
                                     QMessageBox.Close)

        self.change_icon_keep_position()

    def change_icon_keep_position(self):
        """ change keep position icon."""
        if self.keep_position_status.get_keep_position_enabled():
            self.enable_keep_position_action.setToolTip("Disable Keep Position")
            self.enable_keep_position_action.setChecked(True)
        else:
            self.enable_keep_position_action.setToolTip("Enable Keep Position")
            self.enable_keep_position_action.setChecked(False)
            self.msg_log.logMessage("", "Keep position", 3)

    def enable_thrusters(self):
        """ Change thrusters status."""
        ip = self.vehicle_info.get_vehicle_ip()
        port = ROSBRIDGE_SERVER_PORT
        vehicle_namespace = self.vehicle_info.get_vehicle_namespace()
        if self.thrusters_status.get_thrusters_enabled():
            disable_thrusters = self.vehicle_data.get_disable_thrusters_service()
            if disable_thrusters is not None:
                response = cola2_interface.send_trigger_service(ip, port,
                                                                vehicle_namespace + disable_thrusters)
                if not response['values']['success']:
                    QMessageBox.critical(self.parent(),
                                         "Disable thrusters failed",
                                         response['values']['message'],
                                         QMessageBox.Close)

            else:
                QMessageBox.critical(self.parent(),
                                     "Disable thrusters error",
                                     "The service 'Disable Thrusters' could not be sent.",
                                     QMessageBox.Close)

        else:
            enable_thrusters = self.vehicle_data.get_enable_thrusters_service()
            if enable_thrusters is not None:
                response = cola2_interface.send_trigger_service(ip, port,
                                                                vehicle_namespace + enable_thrusters)
                if not response['values']['success']:
                    QMessageBox.critical(self.parent(),
                                         "Enable thrusters failed",
                                         response['values']['message'],
                                         QMessageBox.Close)
                # /girona500/controller/enable_thrusters
            else:
                QMessageBox.critical(self.parent(),
                                     "Enable thrusters error",
                                     "The service 'Enable Thrusters' could not be sent.",
                                     QMessageBox.Close)

    def change_icon_thrusters(self):
        """ Change thrusters icon."""
        # if self.actionDisableThrusters.isChecked():
        if self.thrusters_status.get_thrusters_enabled():
            self.enable_thrusters_action.setToolTip("Disable Thrusters")
            self.enable_thrusters_action.setChecked(True)

        else:
            self.enable_thrusters_action.setToolTip("Enable Thrusters")
            self.enable_thrusters_action.setChecked(False)

    def goto(self):
        """ Show goto dialog."""
        ip = self.vehicle_info.get_vehicle_ip()
        port = ROSBRIDGE_SERVER_PORT
        if self.goto_action.isChecked():
            self.goto_action.setChecked(False)
            # set ip and port
            self.goto_dialog.set_ip(ip)
            self.goto_dialog.set_port(port)
            # show go to dialog
            self.goto_dialog.show()
        else:
            # on actionGoTo uncheck
            # disable goto
            self.goto_dialog.disable_goto()

    def change_icon_goto(self):
        """
        Set icon depending goto status
        """
        going = self.goto_dialog.is_going()

        self.goto_action.setChecked(going)

    def transfer_bag(self):
        """ Opens a transfer dialog """
        user = self.vehicle_info.get_vehicle_user()
        ip = self.vehicle_info.get_vehicle_ip()
        remote_ini_path = self.vehicle_info.get_remote_vehicle_package()

        if self.sftp_transfer_dialog is None or self.sftp_transfer_dialog.isHidden():
            self.sftp_transfer_dialog = SFTPConnectionDialog(user, ip, os.path.expanduser("~"), remote_ini_path,
                                                             file_extension=[".bag", ".active"],
                                                             filter_by_extension=True,
                                                             compare_with_crc32=True, parent=self.parent())
            self.sftp_transfer_dialog.show()

    def vehicle_widgets_visibility(self):
        """ Change vehicle widgets visibility."""

        if self.vehicle_widgets_action.isChecked():
            self.vehicle_w_dock.show()
        else:
            self.vehicle_w_dock.hide()

    def close(self):
        self.thrusters_status.disconnect_object()
        self.keep_position_status.disconnect_object()
        self.mission_active.disconnect_object()
        self.vehicle_widgets.disconnect_object()
        self.goto_dialog.disconnect_object()
