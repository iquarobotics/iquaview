# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Dialog to send a goto command to the vehicle.
 It allows to define graphically the destination point by clicking the map.
"""
import logging

from PyQt5 import QtCore
from PyQt5.QtCore import pyqtSignal, QEvent, QTimer
from PyQt5.QtGui import QIcon, QColor
from PyQt5.QtWidgets import QDialog, QWidget, QMessageBox, QAction
from qgis.core import (QgsWkbTypes,
                       QgsPointXY,
                       QgsDistanceArea,
                       QgsProject,
                       QgsCoordinateReferenceSystem)
from qgis.gui import QgsMapToolEmitPoint, QgsMapToolPan, QgsRubberBand

from iquaview.src.ui.ui_go_to_dlg import Ui_GoToDialog
from iquaview.src.vehicle.thrustersstatus import ThrustersStatus
from iquaview.src.vehicle.keeppositionstatus import KeepPositionStatus
from iquaview_lib.cola2api.cola2_interface import (send_goto_service,
                                                   send_trigger_service,
                                                   get_ros_param)
from iquaview_lib.cola2api.mission_types import (HEAVE_MODE_ALTITUDE,
                                                 HEAVE_MODE_BOTH)
from iquaview_lib.definitions import ROSBRIDGE_SERVER_PORT
from iquaview_lib.utils.qgisutils import transform_point
from iquaview_lib.utils.textvalidator import evaluate_doublespinbox
from iquaview_lib.vehicle.vehicledata import VehicleData, is_navigation_valid
from iquaview_lib.utils.status_checker import check_status


logger = logging.getLogger(__name__)


class GoToDialog(QDialog, Ui_GoToDialog):
    """
    opens a goto dialog to send a service goto
    """
    going_signal = pyqtSignal()
    map_tool_change_signal = pyqtSignal()
    dialog_finished_signal = pyqtSignal()
    stop_timer_signal = pyqtSignal()

    def __init__(self, config, canvas, ip, port, vehicle_namespace, vehicledata: VehicleData,
                 thrusters_status: ThrustersStatus, keep_position_status: KeepPositionStatus,
                 enable_thrusters_action: QAction, enable_keep_position_action: QAction, parent=None):
        super().__init__(parent)
        self.config = config
        self.canvas = canvas
        self.ip = ip
        self.port = port
        self.vehicle_namespace = vehicle_namespace
        self.vehicle_data = vehicledata
        self.thrusters_status = thrusters_status
        self.keep_position_status = keep_position_status
        self.enable_thrusters_action = enable_thrusters_action
        self.enable_keep_position_action = enable_keep_position_action
        self.setupUi(self)
        self.getCoordinatesButton.setIcon(QIcon(":/resources/pickPointInMap.svg"))
        self.installEventFilter(self)
        self.timer = QTimer()
        self.timer.timeout.connect(self.refresh_goto_status)
        self.subscribed = False

        self.heave_mode_widget.set_parent_widget(self)

        self.goto_status = None

        crs = self.canvas.mapSettings().destinationCrs()
        self.distance_calc = QgsDistanceArea()
        self.distance_calc.setSourceCrs(crs, QgsProject.instance().transformContext())
        self.distance_calc.setEllipsoid(QgsProject.instance().ellipsoid())

        self.canvas.destinationCrsChanged.connect(self.canvas_crs_changed)

        self.buttonBox.accepted.connect(self.on_accept)
        self.buttonBox.rejected.connect(self.on_reject)

        self.latitude_widget.heave_mode = self.heave_mode_widget
        self.longitude_widget.heave_mode = self.heave_mode_widget
        self.latitude_widget.config = self.config
        self.longitude_widget.config = self.config

        self.speed_widget.speed_doubleSpinBox.valueChanged.connect(self.on_spinbox_changed)
        self.tolerance_widget.tolerance_xy_doubleSpinBox.valueChanged.connect(self.on_spinbox_changed)
        self.speed_widget.speed_doubleSpinBox.editingFinished.connect(self.on_spinbox_changed)
        self.tolerance_widget.tolerance_xy_doubleSpinBox.editingFinished.connect(self.on_spinbox_changed)
        self.stop_timer_signal.connect(self.stop_timer)

        self.point_tool = QgsMapToolEmitPoint(self.canvas)
        self.point_tool.canvasClicked.connect(self.map_clicked)
        self.getCoordinatesButton.clicked.connect(self.get_coordinates)

        self.rubber_band_points = QgsRubberBand(self.canvas, QgsWkbTypes.PointGeometry)
        self.rubber_band_points.setColor(QColor("red"))
        self.rubber_band_points.setIcon(QgsRubberBand.ICON_CROSS)
        self.rubber_band_points.setIconSize(15)

        self.heave_mode_widget.on_heave_mode_box_changed(0)

    def canvas_crs_changed(self):
        """
        If canvas crs changed updates crs
        """
        crs = self.canvas.mapSettings().destinationCrs()
        self.distance_calc.setSourceCrs(crs, QgsProject.instance().transformContext())
        self.distance_calc.setEllipsoid(QgsProject.instance().ellipsoid())

        self.update_position()
        if not self.is_going():
            self.rubber_band_points.reset(QgsWkbTypes.PointGeometry)

    def update_position(self):
        if self.is_acceptable():
            lat = self.latitude_widget.get_latitude()
            lon = self.longitude_widget.get_longitude()
            self.rubber_band_points.reset(QgsWkbTypes.PointGeometry)

            point_crs = QgsCoordinateReferenceSystem.fromEpsgId(4326)
            point = transform_point(
                QgsPointXY(lon, lat),
                point_crs,
                self.canvas.mapSettings().destinationCrs()

            )
            self.rubber_band_points.addPoint(point)

    def set_ip(self, ip):
        """ Set IP"""
        self.ip = ip

    def set_port(self, port):
        """ Set Port """
        self.port = port

    def is_going(self):
        """
        Get goto state
        :return: Return True if auv is going, otherwise False
        """

        going = False

        data = self.vehicle_data.get_captain_state()
        # if receive data from auv
        if data is not None:
            # 1 GO TO
            if data == 1:
                going = True
        return going


    def check_and_send_goto_service(self, ip, port, service, lat, lon, depth, altitude, heave_mode, surge, tolerance_xy,
                                    no_altitude_goes_up_value):

        # thrusters disabled
        if not self.thrusters_status.get_thrusters_enabled():
            return check_status("Go to Service",
                                "'Go to' cannot send. Thrusters are disabled.",
                                self.thrusters_status.get_thrusters_enabled,
                                True,
                                self.check_and_send_goto_service,
                                ip,
                                port,
                                service,
                                lat, lon,
                                depth,
                                altitude,
                                heave_mode,
                                surge,
                                tolerance_xy,
                                no_altitude_goes_up_value,
                                action=self.enable_thrusters_action,
                                action_msg="Enable thrusters",
                                enabling_msg="Enabling thrusters",
                                )
        elif self.keep_position_status.get_keep_position_enabled():
            return check_status("Go to Service",
                                "'Go to' cannot send. Keep position is enabled.",
                                self.keep_position_status.get_keep_position_enabled,
                                False,
                                self.check_and_send_goto_service,
                                ip,
                                port,
                                service,
                                lat, lon,
                                depth,
                                altitude,
                                heave_mode,
                                surge,
                                tolerance_xy,
                                no_altitude_goes_up_value,
                                action=self.enable_keep_position_action,
                                action_msg="Disable keep position",
                                enabling_msg="Disabling keep position",
                                )
        else:
            # send goto service with params
            result = send_goto_service(ip, port, service, lat, lon,
                                       depth, altitude, heave_mode, surge, tolerance_xy,
                                       no_altitude_goes_up_value)
            return result

    def on_accept(self):
        """ On click accept, send service with the values on the fields and subscribes to goto service"""
        try:
            if self.is_acceptable():
                lat = float(self.latitude_widget.get_latitude())
                lon = float(self.longitude_widget.get_longitude())
                depth = float(self.heave_mode_widget.get_depth())
                altitude = float(self.heave_mode_widget.get_altitude())
                heave_mode = self.heave_mode_widget.get_heave_mode()
                surge = float(self.speed_widget.speed_doubleSpinBox.value())
                tolerance_xy = float(self.tolerance_widget.tolerance_xy_doubleSpinBox.value())
                no_altitude_goes_up_value = (self.no_altitude_widget.no_altitude_goes_up())

                if self.is_allowed_distance(lat, lon):
                    if heave_mode in (HEAVE_MODE_ALTITUDE, HEAVE_MODE_BOTH) or depth != 0:
                        reply = QMessageBox.question(self.parent(),
                                                     'Go To Confirmation',
                                                     "You are about to send the AUV to a point below the surface.\n"
                                                     "Are you sure you want to continue?",
                                                     QMessageBox.Yes, QMessageBox.No)
                        if reply == QMessageBox.No:
                            return

                    enable_goto = self.vehicle_data.get_goto_service()
                    result = self.check_and_send_goto_service(self.ip, self.port, self.vehicle_namespace + enable_goto,
                                                              lat, lon, depth, altitude, heave_mode, surge,
                                                              tolerance_xy,no_altitude_goes_up_value)

                    if result['result']:

                        if result['values']['success']:
                            self.update_position()
                            self.goto_status = None
                            self.dialog_finished_signal.emit()
                            self.accept()
                        else:
                            try:
                                message = result['values']['message']
                            # back compatibility
                            except Exception as e:
                                message = "There is another execution in progress."
                            logger.warning("'Go To' failed")
                            QMessageBox.critical(self,
                                                 "Go To failed",
                                                 message,
                                                 QMessageBox.Close)
                            self.rubber_band_points.reset(QgsWkbTypes.PointGeometry)
                            self.on_reject()
                    else:
                        message = "Error sending 'Go to' Service"
                        logger.error(message)
                        QMessageBox.critical(self,
                                             "Go To failed",
                                             message,
                                             QMessageBox.Close)
                        self.rubber_band_points.reset(QgsWkbTypes.PointGeometry)
                        self.on_reject()

                else:
                    message = "Enter a point within the allowed distance: " \
                              + str(self.get_max_dist_allowed()) \
                              + "m"
                    logger.error(message)
                    QMessageBox.critical(self,
                                         "Go To failed",
                                         message,
                                         QMessageBox.Close)
                    self.rubber_band_points.reset(QgsWkbTypes.PointGeometry)

        except (ConnectionRefusedError, OSError) as oe:
            logger.error(f"Connection Refused: {oe.strerror}")
            QMessageBox.critical(self,
                                 "Go To failed",
                                 f"Connection Refused: {oe.strerror}",
                                 QMessageBox.Close)
            self.rubber_band_points.reset(QgsWkbTypes.PointGeometry)

    def update_goto_status(self):
        self.subscribed = True
        if not self.timer.isActive():
            self.timer.start(1000)

    def refresh_goto_status(self):
        """ check the goto status"""
        if self.subscribed:
            data = self.vehicle_data.get_captain_state()
            # if receive data from auv
            if data is not None:
                if data != self.goto_status:
                    # 1 GO TO
                    if data == 1:
                        logger.info("Going")
                        self.going_signal.emit()
                        self.update_position()
                    elif self.goto_status == 1 and data != 1:
                        self.rubber_band_points.reset(QgsWkbTypes.PointGeometry)
                        self.going_signal.emit()
                    elif (self.goto_status == 0 or self.goto_status is None) and data == 0:
                        self.rubber_band_points.reset(QgsWkbTypes.PointGeometry)
                        self.going_signal.emit()

                    self.goto_status = data

    def is_acceptable(self):
        """
        check if the text of the fields of goto are acceptables
        :return: return True if the values are acceptable, otherwise False
        """
        return self.latitude_widget.is_acceptable() and self.longitude_widget.is_acceptable()

    def get_coordinates(self):
        """Set the maptool to pointTool"""
        self.map_tool_change_signal.emit()  # Emit signal to warn mainwindow that we are changing a maptool
        self.canvas.setMapTool(self.point_tool)

    def map_clicked(self, point):
        """Set the coordinates from 'point' to longitude and latitude lineEdits"""
        # reset icon_cross rubber_band_points
        self.rubber_band_points.reset(QgsWkbTypes.PointGeometry)
        self.rubber_band_points.addPoint(point)

        point_crs = QgsCoordinateReferenceSystem.fromEpsgId(4326)
        point = transform_point(
            point,
            self.canvas.mapSettings().destinationCrs(),
            point_crs
        )
        self.longitude_widget.set_longitude(point[0])
        self.latitude_widget.set_latitude(point[1])
        self.point_tool.deactivate()
        # add new icon_cross rubber_band_point
        self.canvas.setMapTool(QgsMapToolPan(self.canvas))

    def is_allowed_distance(self, x_goal, y_goal):
        """

        :param x_goal: longitude
        :param y_goal: latitude
        :return: return true if the distance is allowed, otherwise false
        """
        allowed = False
        data = self.vehicle_data.get_nav_sts()
        if is_navigation_valid(data):
            lat = float(data['global_position']['latitude'])
            lon = float(data['global_position']['longitude'])
            pos = QgsPointXY(lon, lat)
            pos_goal = QgsPointXY(y_goal, x_goal)

            distance = self.distance_calc.measureLine([pos, pos_goal])
            max_dist = self.get_max_dist_allowed()

            if distance <= max_dist:
                allowed = True

        return allowed

    def get_max_dist_allowed(self):
        """ Get the maximum distance allowed to waypoint"""
        return float(get_ros_param(self.ip, ROSBRIDGE_SERVER_PORT,
                                   self.vehicle_namespace + '/captain/max_distance_to_waypoint')['value'])

    def disable_goto(self):
        """ Send a service that disables goto"""
        disable_goto = self.vehicle_data.get_disable_goto_service()
        if disable_goto is not None:
            self.rubber_band_points.reset(QgsWkbTypes.PointGeometry)
            response = send_trigger_service(self.ip, self.port, self.vehicle_namespace + disable_goto)
            try:
                if not response['values']['success']:
                    QMessageBox.critical(self,
                                         "Disable 'Go To' failed",
                                         response['values']['message'],
                                         QMessageBox.Close)
            # back compatibility
            except Exception as e:
                logger.warning("The disable goto response can not be read")

    def on_reject(self):
        """ reject goto"""
        self.going_signal.emit()
        self.rubber_band_points.reset(QgsWkbTypes.PointGeometry)
        self.dialog_finished_signal.emit()
        self.reject()

    def closeEvent(self, event):
        """ close event and call function on_reject"""
        self.on_reject()

    def eventFilter(self, widget, event):
        """ Event filter"""
        if (event.type() == QEvent.KeyPress
                and isinstance(widget, GoToDialog)):
            key = event.key()
            if key == QtCore.Qt.Key_Escape:
                # capture Key Escape for emit going_signal
                self.going_signal.emit()

        return QWidget.eventFilter(self, widget, event)

    def stop_timer(self):
        """ Stop timer to update data"""
        self.timer.stop()

    def disconnect_object(self):
        """ Disconnect timer and topic """
        self.subscribed = False
        self.goto_status = None
        if self.timer:
            self.stop_timer_signal.emit()

    def on_spinbox_changed(self, value=0):
        sender = self.sender()
        state = evaluate_doublespinbox(sender, str(sender.text()))
