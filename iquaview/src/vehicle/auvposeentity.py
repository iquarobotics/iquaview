#!/usr/bin/env python
# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

import logging
import os
import os.path
import sys
from typing import Dict

from PyQt5.QtCore import pyqtSignal
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QWidget, QMessageBox

from iquaview.src.connection.settings.auvconnectionwidget import AUVConnectionWidget, resolve_filename
from iquaview.src.connection.settings.teleoperationconnectionwidget import TeleoperationConnectionWidget
from iquaview.src.customizationsettings import CustomizationSettings
from iquaview.src.vehicle.config_auv_driver import ConfigAUVDriver
from iquaview_lib.baseclasses.entitybase import EntityBase, LabelData
from iquaview_lib.baseclasses.webserverbase import WebServerRegister
from iquaview_lib.cola2api.basic_message import BasicMessage

logger = logging.getLogger(__name__)


class AUVEntity(EntityBase):
    auv_connect_signal = pyqtSignal()
    auv_disconnect_signal = pyqtSignal()
    basic_message_signal = pyqtSignal(BasicMessage)

    LABEL_STATUS = "status"
    LABEL_DEPTH = "depth"
    LABEL_TIMEOUT = "timeout"
    LABEL_ALTITUDE = "altitude"
    LABEL_SYNC = "sync"
    LABEL_CHRONY = "chrony"  # optional, depends on configuration xml

    PROPERTIES_CUSTOMIZATION_SETTINGS = "customization_settings"
    PROPERTIES_TELEOPERATION = "teleoperation"

    def __init__(self, chrony_available: bool, **kwargs) -> None:
        super().__init__(**kwargs)
        labels_and_names = [
            (self.LABEL_DEPTH, "Depth: "),
            (self.LABEL_ALTITUDE, "Altitude: "),
            (self.LABEL_TIMEOUT, "Timeout: "),
            (self.LABEL_STATUS, "Status: "),
            (self.LABEL_SYNC, "Time sync: "),
        ]
        if chrony_available:
            labels_and_names.append(
                (self.LABEL_CHRONY, "Time source: ")
            )
        for label, name in labels_and_names:
            self.declare_label_data(
                identifier=label,
                label_data=LabelData(
                    label_name=name,
                    text="-",
                    color=QColor("black"),
                ),
            )

    def connect(self) -> None:
        self.auv_connect_signal.emit()

    def close(self) -> None:
        self.auv_disconnect_signal.emit()

    def properties_widgets(self) -> Dict[str, QWidget]:
        widgets = {}
        auv_widget = self.driver.get_configuration_widget()

        style_widget = self.canvasdrawer.style.config_widget()

        customization_settings_widget = CustomizationSettings(self.config, self.driver.vehicle_info)

        teleoperation_connection_widget = TeleoperationConnectionWidget()
        teleoperation_connection_widget.from_dict(self.driver.to_dict())

        widgets[self.PROPERTIES_DRIVER] = auv_widget
        widgets[self.PROPERTIES_STYLE] = style_widget
        widgets[self.PROPERTIES_CUSTOMIZATION_SETTINGS] = customization_settings_widget
        widgets[self.PROPERTIES_TELEOPERATION] = teleoperation_connection_widget
        return widgets

    def save_changes(self, widgets: Dict[str, QWidget]) -> bool:
        try:
            saved = False
            driver_configuration: AUVConnectionWidget = widgets[self.PROPERTIES_DRIVER]
            teleop_configuration: TeleoperationConnectionWidget = widgets[self.PROPERTIES_TELEOPERATION]
            customization_settings: CustomizationSettings = widgets[self.PROPERTIES_CUSTOMIZATION_SETTINGS]

            self.driver.vehicle_info.set_vehicle_ip(str(driver_configuration.ip))
            self.driver.vehicle_info.set_vehicle_port(str(driver_configuration.port))
            self.driver.vehicle_info.save()

            customization_settings.on_accept()

            config_auv_driver = ConfigAUVDriver()
            config_auv_driver.joystick_device = teleop_configuration.joystickdevice
            self.driver.set_configuration(config_auv_driver.to_dict())

            saved = super().save_changes(widgets)

            reply = QMessageBox.No
            if resolve_filename(str(driver_configuration.auv_config)) != resolve_filename(
                self.config.settings["last_auv_config_xml"]
            ):
                message = "AUV configuration xml changed. Do you want to restart Iquaview?"
                reply = QMessageBox.question(None, "Restart Confirmation", message, QMessageBox.Yes, QMessageBox.No)

            self.driver.vehicle_info.read_xml()
            # update vehicle info
            self.driver.vehicle_info.set_remote_vehicle_package(driver_configuration.path)
            self.driver.vehicle_info.set_user(driver_configuration.user)

            self.driver.vehicle_info.save()

            # update config
            self.config.settings["last_auv_config_xml"] = driver_configuration.auv_config

            self.config.save()

            if reply == QMessageBox.Yes:
                logger.info(f"argv was {sys.argv}")
                logger.info(f"sys.executable was {sys.executable}")
                logger.info("restart now")

                os.execv(sys.executable, ["python3"] + sys.argv)

        except Exception as e:
            logger.exception(f"Entity not saved: {e}")

        return saved

    def web_server_register(self) -> WebServerRegister:
        web_server_register = super().web_server_register()
        web_server_register.icon_detail_name = self.driver.vehicle_info.get_vehicle_type()
        web_server_register.icon_detail_size = (
            self.driver.vehicle_info.get_vehicle_width(),
            self.driver.vehicle_info.get_vehicle_length(),
        )
        return web_server_register

    def send_timeout(self, basic_message: BasicMessage):
        """
        Emit signal to send timeout as basic message to timeout widget
        :param basic_message: timeout message
        :type basic_message: BasicMessage
        """
        self.basic_message_signal.emit(basic_message)
