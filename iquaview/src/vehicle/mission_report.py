#!/usr/bin/env python
# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Object to retrieve mission reports from the vehicle.
"""

import re
import subprocess
import os

from markdown import markdown
from markdown.extensions.tables import TableExtension

from PyQt5.QtCore import QObject, Qt, QSize
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QAction, QMessageBox, QWidget, QTextEdit, QFileDialog, QToolButton, QMenu

from iquaview_lib.definitions import ROSBRIDGE_SERVER_PORT
from iquaview_lib.utils.busywidget import BusyWidget, TaskThread
from iquaview_lib.vehicle.vehicleinfo import VehicleInfo
from iquaview_lib.cola2api import cola2_interface


class MissionReport(QObject):
    """Class to retrieve and show mission reports."""

    def __init__(self, vehicle_info: VehicleInfo, parent: QWidget = None) -> None:
        # Initialize parent
        super().__init__(parent)

        # Vehicle information
        self.vehicle_info: VehicleInfo = vehicle_info

        # Get report action
        self.action_get_report: QAction = QAction(
            QIcon(":resources/mActionMissionReportDownload.svg"), "Get Mission Report", self)
        self.action_get_report.triggered.connect(
            self.action_get_report_triggered_callback)
        self.action_open_report: QAction = QAction(
            QIcon(":resources/mActionMissionReport.svg"), "Open Mission Report", self)
        self.action_open_report.triggered.connect(
            self.action_open_report_triggered_callback)
        self.tool_button: QToolButton = QToolButton()
        self.tool_button.setDefaultAction(self.action_get_report)
        menu: QMenu = QMenu()
        menu.setDefaultAction(self.action_get_report)
        menu.addAction(self.action_open_report)
        self.tool_button.setMenu(menu)
        self.tool_button.setPopupMode(QToolButton.MenuButtonPopup)

        # Text edit to show report
        self.text_edit = QTextEdit(parent)
        self.text_edit.setWindowTitle("Mission report")
        self.text_edit.setWindowFlag(Qt.Window, True)
        self.text_edit.setReadOnly(True)
        self.text_edit.setMinimumSize(QSize(800, 600))

        # Task
        self.mission_report_path_task = TaskThread(
            self.get_mission_report_path)
        self.mission_report_path_task.finished.connect(
            self.get_mission_report_path_finished)
        self.mission_report_task = TaskThread(self.get_mission_report)
        self.mission_report_task.finished.connect(
            self.get_mission_report_finished)

        # Retrieved report
        self.remote_report_path: str = ""
        self.output_folder: str = ""

    def get_get_report_action(self) -> QAction:
        """
        Get the retrieve mission report action.

        :return: Mission retrieve action.
        :rtype: QAction
        """
        return self.action_get_report

    def get_open_report_action(self) -> QAction:
        """
        Get the retrieve mission open report action.

        :return: Mission report open action.
        :rtype: QAction
        """
        return self.action_open_report

    def get_tool_button(self) -> QToolButton:
        """
        Get the mission report tool button.

        :return: Mission report tool button.
        :rtype: QToolButton
        """
        return self.tool_button

    def get_mission_report_path(self) -> None:
        """
        Get the mission report path from the vehicle.
        This function is meant to be used with BusyWidget and TaskThread, therefore it raises errors
        to be processed on task finished signal catch.

        :raises RuntimeError: Error if call to service was unsuccessful.
        """
        ip: str = self.vehicle_info.get_vehicle_ip()
        port: int = ROSBRIDGE_SERVER_PORT
        service_name: str = f"{self.vehicle_info.get_vehicle_namespace()}" \
            "/mission_reporter/get_last_report"
        response = cola2_interface.send_trigger_service(ip, port, service_name)
        if not response['values']['success']:
            # This uses raise because its enclosed in a try catch in the worker.
            raise RuntimeError(f"Service call {service_name} unsuccessful.")
        self.remote_report_path = response['values']['message']

    def get_mission_report_path_finished(self) -> None:
        """Get mission report path finished"""
        if self.mission_report_path_task.has_error():
            QMessageBox.warning(
                None, "Error", "Error retrieving mission report path:\n"
                f"{self.mission_report_path_task.get_error()}")
            return
        output_folder = QFileDialog.getExistingDirectory(
            None, "Select output folder")
        if len(output_folder) == 0:
            return
        self.output_folder = output_folder
        bw = BusyWidget("Getting mission report", self.mission_report_task)
        bw.on_start()
        bw.exec()

    def get_local_file_name(self) -> str:
        """
        Get the local file name of the mission report from the internal variables.

        :return: Local file name of the mission report.
        :rtype: str
        """
        if self.output_folder == "" or self.remote_report_path == "":
            return ""
        output_file_name = self.output_folder
        if not output_file_name.endswith("/"):
            output_file_name += "/"
        output_file_name += os.path.basename(self.remote_report_path)
        return output_file_name

    def get_mission_report(self) -> None:
        """
        Get the mission report from the vehicle.

        :raises RuntimeError: When retrieving report from vehicle.
        """
        command = f"scp -B {self.vehicle_info.get_vehicle_user()}@"\
            f"{self.vehicle_info.get_vehicle_ip()}:{self.remote_report_path} "\
            f"{self.get_local_file_name()}"
        ps = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE,
                              stderr=subprocess.STDOUT)
        output = ps.communicate()[0].decode()
        if ps.returncode != 0:
            # This uses raise because its enclosed in a try catch in the worker.
            raise RuntimeError(
                f"Error retrieving report from vehicle: {output}")

    def set_html_table_color(self, match_object: re.Match) -> str:
        """
        Set the correct color to a given html table row.

        :param match_object: regex match object.
        :type match_object: re.Match
        :return: String with the same table row with the right style color.
        :rtype: str
        """
        row: str = match_object.group(0)
        color: str = "#000000"
        if row.find("<td>DEBUG</td>") >= 0:
            color = "#38761d"
        elif row.find("<td>WARN</td>") >= 0:
            color = "#ff9900"
        elif row.find("<td>ERROR</td>") >= 0:
            color = "#ff0000"
        elif row.find("<td>FATAL</td>") >= 0:
            color = "#980000"
        return match_object.group(0).replace("<tr>", f'<tr style="color: {color}">')

    def get_mission_report_finished(self) -> None:
        """Slot to execute on mission report task finished."""
        if self.mission_report_task.has_error():
            QMessageBox.warning(
                None, "Error", f"Error retrieving mission:\n{self.mission_report_task.get_error()}")
            return
        self.show_markdown(self.get_local_file_name())
        self.output_folder = ""
        self.remote_report_path = ""

    def show_markdown(self, file_name: str) -> None:
        """
        Show the input markdown

        :param file_name: Markdown filename
        :type file_name: str
        """
        if not file_name.endswith('.md'):
            QMessageBox.warning(None, "Mission report error.",
                                f"Error opening mission report {file_name}. Wrong file extension. Expecting markdown (*.md)")
            return
        with open(file_name,  encoding="utf-8", mode='r') as file:
            html_text = markdown(file.read(), extensions=[
                                 TableExtension(use_align_attribute=True)])
        html_text = html_text.replace("<table>", "<table border = 1>")
        html_text = re.sub(r"<tr>\n<td>.*</td>\n<td>.*</td>\n<td>.*</td>\n<td>.*</td>\n</tr>",
                           self.set_html_table_color, html_text)
        self.text_edit.setHtml(html_text)
        self.text_edit.show()

    def action_get_report_triggered_callback(self, clicked: bool) -> None:
        """
        Callback slot for action get report triggered.

        :param clicked: Action clicked state.
        :type clicked: bool
        """
        bw = BusyWidget("Getting mission report",
                        self.mission_report_path_task)
        bw.on_start()
        bw.exec()

    def action_open_report_triggered_callback(self, clicked: bool) -> None:
        """
        Callback slot for action open report triggered.

        :param clicked: Action clicked state.
        :type clicked: bool
        """
        file_name, _ = QFileDialog.getOpenFileName(
            None, "Open mission report", "", "Markdown (*.md)")
        if not file_name:
            return
        self.show_markdown(file_name)
