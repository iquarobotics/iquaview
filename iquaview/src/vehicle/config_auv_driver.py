# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
AUV config driver.
"""
import logging
from copy import copy
from dataclasses import dataclass
from typing import Union

logger = logging.getLogger(__name__)


@dataclass
class ConfigAUVDriver:
    """Configuration options for the AUVDriver class."""
    joystick_device: Union[str, None] = None

    def from_dict(self, config: dict) -> None:
        """Load from a dictionary."""
        self.joystick_device = config['joystick_device']

    def to_dict(self) -> dict:
        """
        Convert to dictionary.
        :return: dictionary with the configuration parameters
        :rtype: dict
        """
        config_dict = {'joystick_device': copy(self.joystick_device)}

        return config_dict
