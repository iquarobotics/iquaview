#!/usr/bin/env python
# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.


import logging

from iquaview.src.connection.settings.auvconnectionwidget import AUVConnectionWidget
from iquaview.src.vehicle.config_auv_driver import ConfigAUVDriver
from iquaview_lib.baseclasses.driverbase import DriverBase
from iquaview_lib.config import Config
from iquaview_lib.vehicle.vehicleinfo import VehicleInfo

logger = logging.getLogger(__name__)


class AUVDriver(DriverBase):
    """
        class for a auv driver
    """

    def __init__(self, config: ConfigAUVDriver, iquaview_config: Config, vehicle_info: VehicleInfo, parent=None):
        """
        Constructor
        :param config: driver configuration
        :type config: ConfigAUVDriver
        :param iquaview_config: iquaview configuration
        :type iquaview_config: Config
        :param vehicle_info: Information about AUV
        :type vehicle_info:  VehicleInfo
        :param parent: parent
        """
        super().__init__(parent)
        self.config = config
        self.iquaview_config = iquaview_config
        self.vehicle_info = vehicle_info

    def get_configuration_widget(self) -> AUVConnectionWidget:
        auv_connection = AUVConnectionWidget(self.vehicle_info.get_vehicle_ip(),
                                             self.vehicle_info.get_vehicle_port(),
                                             self.vehicle_info.get_remote_vehicle_package(),
                                             self.vehicle_info.get_vehicle_user(),
                                             self.iquaview_config.settings['last_auv_config_xml'])
        return auv_connection

    def set_configuration(self, configuration: dict) -> None:
        self.config = ConfigAUVDriver()
        self.config.from_dict(configuration)

    def to_dict(self) -> dict:
        """
        Convert to dictionary.
        :return: dictionary with the configuration parameters
        :rtype: dict
        """
        return self.config.to_dict()
