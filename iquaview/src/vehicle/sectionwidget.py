# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.


"""
Section widget to configure in options menu
"""

import logging

from iquaview_lib.ui.ui_section import Ui_SectionWidget

from qgis.gui import QgsCollapsibleGroupBox

logger = logging.getLogger(__name__)


class SectionWidget(QgsCollapsibleGroupBox, Ui_SectionWidget):
    def __init__(self, title="Section", id=None, collapsed=False, parent=None):
        """
        Constructor
        """
        super().__init__(parent)
        self.setupUi(self)
        self.setObjectName(str(id))
        self.setWindowTitle("SectionWidget")
        self.setTitle(title)
        self.setCollapsed(collapsed)

        if not collapsed:
            self.show()

        self.description_lineEdit.setText(title)
        self.description_lineEdit.textChanged.connect(self.setTitle)
