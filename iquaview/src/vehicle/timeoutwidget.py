# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
Widget to display the general timeout of the vehicle software architecture and allow to reset it.
"""
import logging
import os
import time as t

from PyQt5.QtCore import QTimer, pyqtSignal, QThreadPool, QSize, Qt
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtWidgets import QWidget, QMessageBox, QApplication, QPushButton, QLabel, QDialog, QHBoxLayout

from iquaview.src.connection.cola2status import Cola2Status
from iquaview.src.plugins.pluginmanager import PluginManager
from iquaview_lib.cola2api.basic_message import BasicMessage
from iquaview_lib.cola2api.cola2_interface import send_trigger_service, get_ros_param, SubscribeToTopic
from iquaview_lib.config import Config
from iquaview_lib.definitions import ROSBRIDGE_SERVER_PORT
from iquaview_lib.utils.workerthread import Worker
from iquaview_lib.vehicle.vehicledata import VehicleData
from iquaview_lib.vehicle.vehicleinfo import VehicleInfo
from iquaview_lib.xmlconfighandler.vehicledatahandler import VehicleDataHandler

logger = logging.getLogger(__name__)


class TimeoutWidget(QWidget):
    timeout_warning = pyqtSignal()
    reconnect_signal = pyqtSignal()
    update_data_signal = pyqtSignal()
    new_time_signal = pyqtSignal(int, str)  # time value and color
    stop_timer_signal = pyqtSignal()

    def __init__(self, vehicle_info: VehicleInfo, vehicle_data: VehicleData, config: Config, parent=None):
        super().__init__(parent)
        self.config = config
        self.subscribed = False
        self.closing = False
        self.watchdog_timer_topic = None
        self.timeout = self.config.settings['timeout']
        self.time = self.timeout
        self.timer = QTimer()
        self.usbl_message_received = False
        self.usbl_elapsed_time = 0
        self.last_usbl_et = 0
        self.usbl_time = 0
        self.vehicle_info = vehicle_info
        self.vehicle_data = vehicle_data
        self.plugin_manager = None
        self.cola2_status = None
        self.warning_messagebox = QMessageBox(parent)
        self.threadpool = QThreadPool()
        self.auv_config_xml = os.path.abspath(os.path.expanduser(config.settings['last_auv_config_xml']))
        reset_timeout_pushbutton = QPushButton("Reset timeout ", self.warning_messagebox)
        reset_timeout_pushbutton.clicked.connect(self.reset_timeout)
        reset_timeout_pushbutton.setIcon(QIcon(":/resources/actionResetTimeout.svg"))
        self.warning_messagebox.addButton(reset_timeout_pushbutton, QMessageBox.NoRole)
        self.warning_messagebox.addButton(QMessageBox.Close)
        self.warning_messagebox.setIcon(QMessageBox.Warning)
        self.warning_messagebox.setWindowTitle("Vehicle timeout")
        self.warning_messagebox.setModal(False)

        self.timeout_warning.connect(self.show_warning)
        self.timer.timeout.connect(self.refresh_timeout)
        self.stop_timer_signal.connect(self.stop_timer)
        self.reconnect_signal.connect(self.reconnect_watchdog_thread)
        self.update_data_signal.connect(self.start_timer)

        self.t_time_text = None
        vd_handler = VehicleDataHandler(self.auv_config_xml)
        # get vehicle data topics
        xml_vehicle_data_topics = vd_handler.read_topics()
        # back compatibility
        for topic in xml_vehicle_data_topics:
            if topic.get('id') == "watchdog timer":
                self.t_time_text = topic.text

        self.start_watchdog_thread()

    def set_plugin_manager(self, plugin_manager: PluginManager):
        """
        Set plugin manager
        :param plugin_manager: class to manage multiple plugins
        :type plugin_manager: PluginManager
        """
        self.plugin_manager = plugin_manager

    def set_cola2_status(self, cola2_status: Cola2Status):
        """
        Set cola2 status
        :param cola2_status: Class to periodically check cola2 state.
        :type cola2_status: Cola2Status
        """
        self.cola2_status = cola2_status

    def reconnect_watchdog_thread(self):
        if not self.subscribed:
            QTimer.singleShot(5000, self.start_watchdog_thread)

    def start_watchdog_thread(self):
        """ Start watchdog thread """
        if not self.closing:
            worker = Worker(self.update_watchdog_status)
            worker.signals.finished.connect(self.reconnect_watchdog_thread)
            self.threadpool.start(worker)

    def update_watchdog_status(self):
        """ connect to AUV"""
        try:
            self.subscribed = True
            self.watchdog_timer_topic = SubscribeToTopic(self.vehicle_info.get_vehicle_ip(),
                                                         ROSBRIDGE_SERVER_PORT,
                                                         self.vehicle_info.get_vehicle_namespace() + self.t_time_text)
            self.watchdog_timer_topic.subscribe()
            self.set_timeout()
            self.update_data_signal.emit()
        except OSError as oe:
            logger.error(f"Disconnecting watchdog status: {oe}")
            self.disconnect_timeout()
        except Exception as e:
            logger.error(f"Disconnecting watchdog status: {e}")
            self.disconnect_timeout()
        except:
            logger.error("Unexpected error")
            self.disconnect_timeout()

    def start_timer(self):
        """ Start timer"""
        if not self.timer.isActive():
            self.timer.start(1000)

    def refresh_timeout(self):
        """ Refresh timeout"""
        try:
            data = None
            if self.subscribed:
                if self.watchdog_timer_topic is not None:
                    data = self.watchdog_timer_topic.get_data()
                if data is not None and data.get('valid_data') == 'new_data':
                    self.time = int(self.timeout) - data['data']
                elif data is not None and data.get('valid_data') == 'disconnected':
                    self.disconnect_timeout()
                    self.reconnect_signal.emit()
                else:
                    self.time -= 1
            elif self.usbl_message_received:
                self.usbl_message_received = False
                is_new = abs(self.usbl_time - t.time()) < 10
                if is_new and self.last_usbl_et != self.usbl_elapsed_time:
                    self.time = (int(self.timeout) - self.usbl_elapsed_time)
                    self.last_usbl_et = self.usbl_elapsed_time
                else:
                    self.time -= 1
            else:
                self.time -= 1

            self.send_time()
        except OSError as oe:
            logger.error(f"Disconnecting watchdog status: {oe}")
            self.disconnect_timeout()
            self.reconnect_signal.emit()
        except Exception as e:
            logger.error(f"Disconnecting watchdog status: {e}")
            self.disconnect_timeout()
            self.reconnect_signal.emit()
        except:
            logger.error("Unexpected error")
            self.disconnect_timeout()
            self.reconnect_signal.emit()

    def send_time(self):
        """ set time"""
        # set color
        if self.time < 0:
            color = 'red'
        elif self.time <= 300:
            if self.time > 0 and self.time % 60 == 0:
                self.timeout_warning.emit()
            color = 'orange'
        else:
            color = 'black'

        # set time
        self.new_time_signal.emit(self.time, color)

    def set_timeout(self):
        self.timeout = (
            get_ros_param(
                self.vehicle_info.get_vehicle_ip(),
                ROSBRIDGE_SERVER_PORT,
                f'{self.vehicle_info.get_vehicle_namespace()}/safety_supervisor/watchdog_timer/timeout'
            )['value']
        )
        if self.timeout != "null":
            self.config.settings['timeout'] = int(self.timeout)

    def update_usbl_time(self, message: BasicMessage):

        if message.elapsed_time != -1:
            self.update_data_signal.emit()
            self.usbl_time = message.time
            self.usbl_message_received = True
            self.usbl_elapsed_time = message.elapsed_time

    def show_warning(self):
        warning_msg = f"Less than {str(self.time)} seconds for timeout expiration. You might want to restart it."
        logger.warning(warning_msg)
        self.warning_messagebox.setText(warning_msg)
        self.warning_messagebox.show()
        QApplication.beep()
        self.warning_messagebox.raise_()
        self.warning_messagebox.activateWindow()

    def is_subscribed(self):
        """
        :return: return subscribed status
        :rtype: bool
        """
        return self.subscribed

    def disconnect_timeout(self):
        """ Stop timer. Set connected state to False"""
        if not self.closing:
            self.subscribed = False

            if self.watchdog_timer_topic:
                self.watchdog_timer_topic.close()
            self.watchdog_timer_topic = None
            self.threadpool.clear()

    def stop_timer(self):
        """ Stops timer"""
        self.timer.stop()
        self.disconnect_timeout()
        self.closing = True

    def reset_timeout(self):
        """ Send service that reset timeout"""
        try:
            evologics_usbl = self.plugin_manager.get_plugin("iquaview_evologics_usbl")
            sonardyne_usbl = self.plugin_manager.get_plugin("iquaview_sonardyne_usbl")

            if self.cola2_status.is_connected():
                reset_timeout = self.vehicle_data.get_reset_timeout_service()
                if reset_timeout is not None:
                    response = send_trigger_service(self.vehicle_info.get_vehicle_ip(), ROSBRIDGE_SERVER_PORT,
                                                    self.vehicle_info.get_vehicle_namespace() + reset_timeout)
                    if not response['values']['success']:
                        raise Exception(response['values']['message'])

                    self.set_timeout()
                    logger.info("Reset timeout")
                else:
                    raise Exception("The service 'Reset Timeout' could not be sent.")
            elif (evologics_usbl is not None
                  and evologics_usbl.get_usbl_entity() is not None
                  and evologics_usbl.get_usbl_entity().is_connected()):
                if not evologics_usbl.action_reset_timeout.isChecked():
                    evologics_usbl.action_reset_timeout.trigger()
                    self.show_usbl_info()

            elif (sonardyne_usbl is not None
                  and sonardyne_usbl.get_usbl_entity() is not None
                  and sonardyne_usbl.get_usbl_entity().is_connected()):
                if not sonardyne_usbl.action_reset_timeout.isChecked():
                    sonardyne_usbl.action_reset_timeout.trigger()
                    self.show_usbl_info()
            else:
                raise Exception("The service 'Reset Timeout' could not be sent.")

        except Exception as e:
            logger.exception(e)
            QMessageBox.critical(self,
                                 "Reset timeout error",
                                 f"Error: {e}",
                                 QMessageBox.Close)

    def show_usbl_info(self):
        """
        Show information about the reset timeout via USBL.
        """
        msg_box = QMessageBox(self)
        msg_box.setModal(False)
        msg_box.setStandardButtons(QMessageBox.Close)
        msg_box.setIcon(QMessageBox.Information)
        pixmap = QPixmap(":/resources/mActionUSBLResetTimeout.png")
        msg_box.setIconPixmap(pixmap)
        msg_box.setText(f"<p>The reset timeout command has been sent via USBL. "
                        "Remember to uncheck it once the vehicle has received it.</p>")

        msg_box.setWindowTitle("Reset timeout via USBL")
        msg_box.show()
