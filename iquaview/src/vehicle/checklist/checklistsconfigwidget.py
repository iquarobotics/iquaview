# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.


"""
 Class to read and store the xml structure associated to the check_lists tag in the AUV config file
"""
import os
import logging
from lxml import etree

from qgis.gui import QgsCollapsibleGroupBox

from PyQt5.QtWidgets import QMessageBox

from iquaview_lib.xmlconfighandler.checklisthandler import CheckListHandler
from iquaview_lib.xmlconfighandler.xmlconfigparser import XMLConfigParser

from iquaview.src.vehicle.checklist.checklistwidget import (CheckListWidget,
                                                            CheckItemWidget,
                                                            CheckTopicWidget,
                                                            CheckFieldWidget,
                                                            CheckActionWidget,
                                                            CheckParamWidget
                                                            )
from iquaview.src.vehicle.checklist.check_list import CheckItem, CheckAction, CheckTopic, CheckList
from iquaview.src.vehicle.checklist.topic import CheckListField
from iquaview.src.ui.ui_collapsible_groupbox import Ui_mCollapsibleGroupBox

logger = logging.getLogger(__name__)


class CheckListsWidget(QgsCollapsibleGroupBox, Ui_mCollapsibleGroupBox):
    def __init__(self, config, parent=None):
        """
        Constructor
        :param config: configuration
        """
        super().__init__(parent)
        self.setupUi(self)
        self.setTitle("Check Lists")
        self.add_pushButton.setText("New Check List")
        self.config = config
        self.filename = None
        self.check_lists = []
        self.checklist_key_count = 0

        self.add_pushButton.clicked.connect(lambda: self.new_checklist_widget())
        # load ros params from xml file
        self.set_config_filename(os.path.abspath(os.path.expanduser(self.config.settings['last_auv_config_xml'])))

    def read_xml(self):
        """
        read the last auv configuration xml loaded, and save the vehicle information
        """
        try:
            config_filename = self.filename
            cl_handler = CheckListHandler(config_filename)
            check_lists = cl_handler.get_check_lists()
            self.check_lists = []
            for checklist in check_lists:
                items = cl_handler.get_items_from_checklist(checklist.get('id'))
                checklist_list = []
                for item in items:
                    # get all matches
                    description = cl_handler.get_description_from_item(item)
                    check_topics = cl_handler.get_check_topics_from_item(item)
                    check_actions = cl_handler.get_check_actions_from_item(item)

                    check_item = CheckItem()
                    check_item.description = description.text

                    if check_topics:
                        for c_topic in check_topics:
                            check_topic = CheckTopic(c_topic[0].text, c_topic[1])
                            check_item.check_topics.append(check_topic)

                    # check action
                    elif check_actions:
                        for c_action in check_actions:
                            check_action = CheckAction(c_action[0].text, c_action[1].text, c_action[2])
                            check_item.check_actions.append(check_action)

                    checklist_list.append(check_item)

                self.new_checklist(checklist.get('id'))
                self.check_lists[-1].items = checklist_list

        except OSError as e:
            logger.error("OSError: {}".format(e))

    def set_config_filename(self, filename):
        """
        Set config
        :param filename: config filename
        :type filename: str
        """
        self.filename = filename
        self.load_check_lists()

    def get_check_list(self, key):
        """
        Get check list
        :param key: key
        :type key: str
        :return: check list
        :rtype: CheckList
        """
        for check_list in self.check_lists:
            if check_list.name == key:
                return check_list

    def load_check_lists(self):
        """
        Remove previous widgets, read xml and load checklists
        """
        # remove previous widgets
        self.delete_all(self.vboxlayout.layout())

        self.read_xml()

        for cl in self.check_lists:

            checklist_widget = self.add_checklist_widget(cl.name,
                                                         True)

            for item in cl.items:
                checklist_item = self.add_checklist_item_widget(checklist_widget, item.description, item, True)

                for check_topic in item.check_topics:
                    checklist_topic = self.add_checklist_topic_widget(checklist_item,
                                                                      check_topic.name,
                                                                      check_topic.topic.topic_name,
                                                                      check_topic,
                                                                      True)

                    for field in check_topic.topic.fields:
                        checklist_field = self.add_checklist_field_widget(checklist_topic,
                                                                          field.field_name,
                                                                          field.field_description,
                                                                          field,
                                                                          True)

                for action in item.check_actions:
                    checklist_action = self.add_checklist_action_widget(checklist_item,
                                                                        action.name,
                                                                        action.action_id,
                                                                        action,
                                                                        True)

                    params = action.get_parameters()
                    for param in params:
                        self.add_checklist_param_widget(checklist_action, param, True)

    def new_checklist(self, id):
        """
        Create new checklist and add it to dict
        :param id: checklist id
        :type id: int
        """
        cl = CheckList(id)
        self.check_lists.append(cl)
        self.checklist_key_count += 1

    def new_checklist_widget(self):
        """
        Add new checklist widget
        """
        id = "Checklist_{}".format(self.checklist_key_count)
        self.new_checklist(id)
        self.add_checklist_widget(id)

    def new_checklist_item(self, checklist_widget, description=""):
        """
        Create new checklist item
        :param checklist_widget: widget where the item will be added:
        :type checklist_widget: CheckListWidget
        :param description: item description
        :type description: str
        """
        cl = self.get_check_list(checklist_widget.objectName())
        if cl.name == checklist_widget.objectName():
            check_item = CheckItem(description)
            cl.items.append(check_item)
            self.add_checklist_item_widget(checklist_widget, description, check_item)

    def new_checklist_topic(self, checklist_item_widget, name="Topic", topic=None):
        """
        Create new checklist topic
        :param checklist_item_widget: item where topic will be added
        :type checklist_item_widget: CheckItemWidget
        :param name: topic name
        :type name: str
        :param topic: topic
        :type: Topic
        """
        check_topic = CheckTopic(name, topic)
        checklist_item_widget.get_item().check_topics.append(check_topic)

        checklist_topic = self.add_checklist_topic_widget(checklist_item_widget, name,
                                                          topic, check_topic)

    def new_checklist_action(self, checklist_item_widget, name="Action", action=None):
        """
        Create new checklist action
        :param checklist_item_widget: item where topic will be added
        :type checklist_item_widget: CheckItemWidget
        :param name: action name
        :type name: str
        :param action: action id
        :type action: str
        """
        check_action = CheckAction(name, action)
        checklist_item_widget.get_item().check_actions.append(check_action)

        checklist_topic = self.add_checklist_action_widget(checklist_item_widget, name,
                                                           action, check_action)

    def new_checklist_field(self, checklist_topic_widget, name="Field", description=""):
        """
        Create new checklist field
        :param checklist_topic_widget: topic where field will be added
        :type checklist_topic_widget: CheckTopicWidget
        :param name: field name
        :type name: str
        :param description: field description
        :type description: str
        """
        checklist_field = CheckListField(name, description)
        checklist_topic_widget.get_topic().topic.fields.append(checklist_field)

        self.add_checklist_field_widget(checklist_topic_widget, name,
                                        description, checklist_field)

    def new_checklist_param(self, checklist_action_widget, param=0):
        """
        Create new checklist param
        :param checklist_action_widget: action where param will be added
        :type checklist_action_widget: CheckActionWidget
        :param param: param value
        :type param: float
        """
        self.add_checklist_param_widget(checklist_action_widget, param)

    def add_checklist_widget(self, id="Checklist", collapsed=False):
        """
        Add checklist widget to layout
        :param id: checklist identifier
        :type id: str
        :param collapsed: collapsed state
        :type collapsed: bool
        :return: return checklist widget
        :rtype: CheckListWidget
        """
        check_list_widget = CheckListWidget(id, collapsed)
        self.vboxlayout.layout().addWidget(check_list_widget)

        check_list_widget.remove_checklist_toolButton.clicked.connect(lambda state, x=check_list_widget:
                                                                      self.remove_checklist(x))
        check_list_widget.description_lineEdit.textChanged.connect(lambda state, x=check_list_widget:
                                                                   self.update_checklist(x))
        check_list_widget.add_item_pushButton.clicked.connect(lambda state, x=check_list_widget:
                                                              self.new_checklist_item(x))

        check_list_widget.description_lineEdit.setFocus()

        return check_list_widget

    def add_checklist_item_widget(self, checklist_widget, description="Item", item=None, collapsed=False):
        """
        Add checklist item to checklist widget
        :param checklist_widget: widget where the item will be added
        :type checklist_widget: CheckListWidget
        :param description: item description:
        :type description: str
        :param item: checklist item
        :type item: CheckItem
        :param collapsed: collapsed state
        :type collapsed: bool
        :return: return checkitem widget
        :rtype: CheckItemWidget
        """
        checklist_item_widget = CheckItemWidget(description, item, collapsed, checklist_widget)

        checklist_widget.params_vboxlayout.layout().addWidget(checklist_item_widget)

        checklist_item_widget.remove_toolButton.clicked.connect(lambda state,
                                                                       x=checklist_item_widget,
                                                                       y=checklist_widget:
                                                                self.remove_checklist_item(x, y))
        checklist_item_widget.add_topic_pushButton.clicked.connect(lambda state,
                                                                          x=checklist_item_widget:
                                                                   self.new_checklist_topic(x))
        checklist_item_widget.add_action_pushButton.clicked.connect(lambda state,
                                                                           x=checklist_item_widget:
                                                                    self.new_checklist_action(x))

        checklist_item_widget.description_lineEdit.setFocus()
        return checklist_item_widget

    def add_checklist_topic_widget(self, checklist_item_widget, name="Topic", topic=None, topic_object=None,
                                   collapsed=False):
        """
        Add checklist topic widget
        :param checklist_item_widget: widget where the topic will be added
        :type checklist_item_widget: CheckItemWidget
        :param name: topic name
        :type name: str
        :param topic: topic
        :type topic: str
        :param topic_object: topic object
        :type topic_object: CheckTopic
        :param collapsed: collapsed state
        :return: return check topic widget
        :rtype: CheckTopicWidget
        """
        checklist_topic_widget = CheckTopicWidget(name, topic, topic_object, collapsed, checklist_item_widget)

        checklist_item_widget.topics_vboxlayout.layout().addWidget(checklist_topic_widget)

        checklist_topic_widget.remove_toolButton.clicked.connect(lambda state,
                                                                        x=checklist_topic_widget,
                                                                        y=checklist_item_widget:
                                                                 self.remove_checklist_topic(x, y))
        checklist_topic_widget.add_field_pushButton.clicked.connect(lambda state,
                                                                           x=checklist_topic_widget:
                                                                    self.new_checklist_field(x))

        checklist_topic_widget.description_lineEdit.setFocus()
        return checklist_topic_widget

    def add_checklist_field_widget(self, checklist_topic, name="Field", description="", checklist_field=None,
                                   collapsed=False):
        """
        Add checklist field widget
        :param checklist_topic: widget where the field will be added
        :type checklist_topic: CheckTopicWidget
        :param name: field name
        :type name: str
        :param description: field description
        :type description: str
        :param checklist_field: checklist field
        :type checklist_field: CheckListField
        :param collapsed: collapsed state
        :type collapsed: bool
        :return: return check field widget
        :rtype: CheckFieldWidget
        """
        checklist_field_widget = CheckFieldWidget(name, description, checklist_field, collapsed, checklist_topic)

        checklist_topic.fields_vboxlayout.layout().addWidget(checklist_field_widget)

        checklist_field_widget.remove_field_toolButton.clicked.connect(lambda state,
                                                                              x=checklist_field_widget,
                                                                              y=checklist_topic:
                                                                       self.remove_checklist_field(x, y))

        checklist_field_widget.field_name_lineEdit.setFocus()
        return checklist_field_widget

    def add_checklist_action_widget(self, checklist_item_widget, name, action_id, action_object, collapsed=False):
        """
        Add checklist action widget
        :param checklist_item_widget: widget where the action will be added
        :type checklist_item_widget: CheckItemWidget
        :param name: action name
        :type name: str
        :param action_id: action id
        :type action_id: str
        :param action_object: action
        :type action_object: CheckAction
        :param collapsed: collapsed state
        :return: return check action widget
        :rtype: CheckActionWidget
        """
        checklist_action_widget = CheckActionWidget(name, action_id, action_object, collapsed, checklist_item_widget)

        checklist_item_widget.topics_vboxlayout.layout().addWidget(checklist_action_widget)

        checklist_action_widget.remove_toolButton.clicked.connect(lambda state,
                                                                         x=checklist_action_widget,
                                                                         y=checklist_item_widget:
                                                                  self.remove_checklist_action(x, y))
        checklist_action_widget.add_param_pushButton.clicked.connect(lambda state,
                                                                            x=checklist_action_widget:
                                                                     self.new_checklist_param(x))
        checklist_action_widget.service_lineEdit.setFocus()
        return checklist_action_widget

    def add_checklist_param_widget(self, checklist_action_widget, param=0, collapsed=False):
        """
        Add checklist param widget
        :param checklist_action_widget: action where the param will be added
        :type checklist_action_widget: CheckActionWidget
        :param param: param value
        :type param: int
        :param collapsed: collapsed state
        :type  collapsed: bool
        :return: return check param widget
        :rtype: CheckParamWidget
        """
        checklist_param_widget = CheckParamWidget(str(param), collapsed, checklist_action_widget)

        checklist_action_widget.parameters_vboxlayout.layout().addWidget(checklist_param_widget)

        checklist_param_widget.remove_toolButton.clicked.connect(lambda state,
                                                                        x=checklist_param_widget,
                                                                        y=checklist_action_widget:
                                                                 self.remove_checklist_param(x, y))

        self.update_checklist_param(checklist_action_widget)
        checklist_param_widget.value_lineEdit.textChanged.connect(lambda state,
                                                                         x=checklist_action_widget:
                                                                  self.update_checklist_param(x))
        checklist_param_widget.value_lineEdit.setFocus()
        return checklist_param_widget

    def update_checklist(self, checklist_widget):
        identifier = checklist_widget.objectName()
        cl = self.get_check_list(identifier)
        cl.name = checklist_widget.description_lineEdit.text()
        checklist_widget.setObjectName(cl.name)

    def update_checklist_param(self, checklist_action_widget, checklist_param_widget=None):
        params = []
        action = checklist_action_widget.get_action()
        for i in (range(checklist_action_widget.parameters_vboxlayout.layout().count())):
            item = checklist_action_widget.parameters_vboxlayout.layout().itemAt(i)
            widget = item.widget()
            if checklist_param_widget is None or widget != checklist_param_widget:
                params.append(widget.value_lineEdit.text())

        action.parameters = params

    def remove_checklist_by_id(self, identifier):
        """
        Remove checklist by id
        :param identifier: id of the checklist to remove
        :return: return True if removed, otherwise False
        """
        cl = self.get_check_list(identifier)
        try:
            self.check_lists.remove(cl)
            return True
        except Exception as e:
            logger.error("Error removing checklist: {}".format(e))
            return False

    def remove_checklist(self, check_list_widget):
        """
        remove checklist
        :param check_list_widget:  widget to remove
        :type check_list_widget: CheckListWidget
        """
        confirmation_msg = "Are you sure you want to remove {} check list?".format(check_list_widget.title())

        reply = QMessageBox.question(self, 'Removal confirmation',
                                     confirmation_msg, QMessageBox.Yes | QMessageBox.No, defaultButton=QMessageBox.Yes)
        if reply == QMessageBox.Yes:
            removed = self.remove_checklist_by_id(check_list_widget.objectName())
            self.delete_all(check_list_widget.layout())
            check_list_widget.deleteLater()

    def remove_checklist_item(self, checklist_item_widget, checklist_widget):
        """
        Remove checklist item from checklist
        :param checklist_item_widget: item to remove
        :type checklist_item_widget: CheckItemWidget
        :param checklist_widget: checklist
        :type checklist_widget: CheckListWidget
        """
        confirmation_msg = "Are you sure you want to remove {} item?".format(checklist_item_widget.title())

        reply = QMessageBox.question(self, 'Removal confirmation',
                                     confirmation_msg, QMessageBox.Yes | QMessageBox.No, defaultButton=QMessageBox.Yes)
        if reply == QMessageBox.Yes:
            cl = self.get_check_list(checklist_widget.objectName())
            cl.items.remove(checklist_item_widget.get_item())
            self.delete_all(checklist_item_widget.layout())
            checklist_item_widget.deleteLater()

    def remove_checklist_topic(self, checklist_topic_widget, checklist_item_widget):
        """
        Remove checklist topic from checklist item
        :param checklist_topic_widget:  topic widget to remove
        :type checklist_topic_widget: CheckTopicWidget
        :param checklist_item_widget: item widget
        :type checklist_item_widget: CheckItemWidget
        """
        confirmation_msg = "Are you sure you want to remove {} topic?".format(checklist_topic_widget.title())

        reply = QMessageBox.question(self, 'Removal confirmation',
                                     confirmation_msg, QMessageBox.Yes | QMessageBox.No, defaultButton=QMessageBox.Yes)
        if reply == QMessageBox.Yes:
            item = checklist_item_widget.get_item()
            item.check_topics.remove(checklist_topic_widget.get_topic())
            self.delete_all(checklist_topic_widget.layout())
            checklist_topic_widget.deleteLater()

    def remove_checklist_field(self, checklist_field_widget, checklist_topic_widget):
        """
        Remove checklist field from checklist topic
        :param checklist_field_widget: field to remove
        :type checklist_field_widget: CheckFieldWidget
        :param checklist_topic_widget:  topic widget
        :type checklist_topic_widget: CheckTopicWidget
        """
        confirmation_msg = "Are you sure you want to remove {} field?".format(checklist_field_widget.title())

        reply = QMessageBox.question(self, 'Removal confirmation',
                                     confirmation_msg, QMessageBox.Yes | QMessageBox.No, defaultButton=QMessageBox.Yes)
        if reply == QMessageBox.Yes:
            topic = checklist_topic_widget.get_topic()
            topic.topic.fields.remove(checklist_field_widget.get_field())
            self.delete_all(checklist_field_widget.layout())
            checklist_field_widget.deleteLater()

    def remove_checklist_action(self, checklist_action_widget, checklist_item_widget):
        """
        Remove checklist action from checklist item
        :param checklist_action_widget: action to remove
        :type checklist_action_widget: CheckActionWidget
        :param checklist_item_widget:  item widget
        :type checklist_item_widget: CheckItemWidget
        """
        confirmation_msg = "Are you sure you want to remove {} action?".format(checklist_action_widget.title())

        reply = QMessageBox.question(self, 'Removal confirmation',
                                     confirmation_msg, QMessageBox.Yes | QMessageBox.No, defaultButton=QMessageBox.Yes)
        if reply == QMessageBox.Yes:
            item = checklist_item_widget.get_item()
            item.check_actions.remove(checklist_action_widget.get_action())
            self.delete_all(checklist_action_widget.layout())
            checklist_action_widget.deleteLater()

    def remove_checklist_param(self, checklist_param_widget, checklist_action_widget):
        """
        Remove checklist param from action
        :param checklist_param_widget: param to remove
        :type checklist_param_widget: CheckParamWidget
        :param checklist_action_widget: action widget
        :type checklist_action_widget: CheckActionWidget
        """
        confirmation_msg = "Are you sure you want to remove {} parameter?".format(checklist_param_widget.title())

        reply = QMessageBox.question(self, 'Removal confirmation',
                                     confirmation_msg, QMessageBox.Yes | QMessageBox.No, defaultButton=QMessageBox.Yes)
        if reply == QMessageBox.Yes:
            self.update_checklist_param(checklist_action_widget, checklist_param_widget)

            self.delete_all(checklist_param_widget.layout())
            checklist_param_widget.deleteLater()

    def save(self):
        """
        save the checklists inside the auv configuration xml
        """
        config_parser = XMLConfigParser(self.filename)
        # get check_lists
        xml_check_lists = config_parser.first_match(config_parser.root, "check_lists")
        # all checklists in check_lists
        xml_check_lists.clear()

        for cl in self.check_lists:
            xml_check_list = etree.SubElement(xml_check_lists, "check_list", id=cl.name)

            for item in cl.items:
                xml_check_item = etree.SubElement(xml_check_list, "check_item")
                xml_check_item_description = etree.SubElement(xml_check_item, "description")

                xml_check_item_description.text = item.description

                for check_topic in item.check_topics:
                    xml_check_topic = etree.SubElement(xml_check_item, "check_topic")
                    xml_check_topic_name = etree.SubElement(xml_check_topic, "name")
                    xml_check_topic_topic = etree.SubElement(xml_check_topic, "topic")
                    xml_check_topic_topic_name = etree.SubElement(xml_check_topic_topic, "topic_name")

                    xml_check_topic_name.text = check_topic.name
                    xml_check_topic_topic_name.text = check_topic.topic.topic_name

                    for field in check_topic.topic.fields:
                        xml_check_topic_field = etree.SubElement(xml_check_topic_topic, 'field')
                        xml_check_topic_field_name = etree.SubElement(xml_check_topic_field, 'field_name')
                        xml_check_topic_field_description = etree.SubElement(xml_check_topic_field, 'field_description')

                        xml_check_topic_field_name.text = field.field_name
                        xml_check_topic_field_description.text = field.field_description

                for action in item.check_actions:
                    xml_check_action = etree.SubElement(xml_check_item, "check_action")
                    xml_check_action_name = etree.SubElement(xml_check_action, "name")
                    xml_check_action_action_id = etree.SubElement(xml_check_action, "action_id")
                    xml_check_action_params = etree.SubElement(xml_check_action, "parameters")

                    xml_check_action_name.text = action.name
                    xml_check_action_action_id.text = action.action_id
                    for param in action.get_parameters():
                        xml_check_action_param = etree.SubElement(xml_check_action_params, "param")
                        xml_check_action_param.text = str(param)

        config_parser.write()

    def delete_all(self, layout):
        """
        delete all widget from layout
        :param layout: layout is a qt layout
        """
        if layout is not None:
            for i in reversed(range(layout.count())):
                item = layout.takeAt(i)
                widget = item.widget()
                if widget is not None:
                    widget.deleteLater()
                else:
                    self.delete_all(item.layout())

    def is_valid(self):
        """
        check if the checklists are valid
        :return: True if the checklists are valid
        """
        for cl in self.check_lists:
            if not cl.is_valid():
                return False
        return True
