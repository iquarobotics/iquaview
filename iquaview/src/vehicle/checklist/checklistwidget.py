# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.


"""
Checklist widget to configure in options menu
"""

import logging

from qgis.gui import QgsCollapsibleGroupBox

from iquaview_lib.ui.ui_checkaction import Ui_CheckActionWidget
from iquaview_lib.ui.ui_checkfield import Ui_FieldWidget
from iquaview_lib.ui.ui_checkitem import Ui_CheckItemWidget
from iquaview_lib.ui.ui_checklist import Ui_ChecklistWidget
from iquaview_lib.ui.ui_checkparam import Ui_CheckParameterWidget
from iquaview_lib.ui.ui_checktopic import Ui_CheckTopicWidget
from iquaview_lib.utils.textvalidator import check_line_edit_starts_by_slash

logger = logging.getLogger(__name__)


class CheckListWidget(QgsCollapsibleGroupBox, Ui_ChecklistWidget):
    def __init__(self, id="Checklist", collapsed=False, parent=None):
        """
        Constructor
        """
        super().__init__(parent)
        self.setupUi(self)
        self.setObjectName(str(id))
        self.setWindowTitle("Checklist")
        self.setTitle(id)
        self.setCollapsed(collapsed)

        if not collapsed:
            self.show()

        self.description_lineEdit.setText(id)
        self.description_lineEdit.textChanged.connect(self.setTitle)


class CheckItemWidget(QgsCollapsibleGroupBox, Ui_CheckItemWidget):
    def __init__(self, description="Checkitem", item=None, collapsed=False, parent=None):
        """
        Constructor
        """
        super().__init__(parent)
        self.setupUi(self)
        self.setObjectName(str(description))
        self.setWindowTitle("Checkitem")
        self.setTitle(description)
        self.setCollapsed(collapsed)
        self.item = item

        if not collapsed:
            self.show()

        self.description_lineEdit.setText(description)
        self.description_lineEdit.textChanged.connect(self.setTitle)
        self.description_lineEdit.textChanged.connect(self.set_description)

    def get_item(self):
        return self.item

    def set_description(self):
        self.item.description = self.description_lineEdit.text()


class CheckTopicWidget(QgsCollapsibleGroupBox, Ui_CheckTopicWidget):
    def __init__(self, name="Checktopic", topic="", topic_object=None, collapsed=False, parent=None):
        """
        Constructor
        """
        super().__init__(parent)
        self.setupUi(self)
        self.setObjectName(str(name))
        self.setWindowTitle("Checktopic")
        self.setTitle(name)
        self.setCollapsed(collapsed)
        self.topic_object = topic_object
        if not collapsed:
            self.show()

        self.topic_lineEdit.setText(topic)
        self.description_lineEdit.setText(name)
        self.description_lineEdit.textChanged.connect(self.setTitle)
        self.description_lineEdit.textChanged.connect(self.set_name)
        self.topic_lineEdit.textChanged.connect(self.set_name)

    def get_topic(self):
        return self.topic_object

    def set_name(self):
        check_line_edit_starts_by_slash(self.topic_lineEdit)
        self.topic_object.name = self.description_lineEdit.text()
        self.topic_object.topic.topic_name = self.topic_lineEdit.text()


class CheckFieldWidget(QgsCollapsibleGroupBox, Ui_FieldWidget):
    def __init__(self, name="field", description="", checklist_field=None, collapsed=False, parent=None):
        """
        Constructor
        """
        super().__init__(parent)
        self.setupUi(self)
        self.setObjectName(str(name))
        self.setWindowTitle("Checkfield")
        self.setTitle(name)
        self.setCollapsed(collapsed)
        self.field = checklist_field

        if not collapsed:
            self.show()
        self.field_description_lineEdit.setText(description)
        self.field_name_lineEdit.setText(name)
        self.field_name_lineEdit.textChanged.connect(self.setTitle)

        self.field_description_lineEdit.textChanged.connect(self.set_field)
        self.field_name_lineEdit.textChanged.connect(self.set_field)

    def get_field(self):
        return self.field

    def set_field(self):
        field_name = self.field_name_lineEdit.text()
        if field_name.startswith("/"):
            position = self.field_name_lineEdit.cursorPosition()
            self.field_name_lineEdit.setText(field_name[1:])
            self.field_name_lineEdit.setCursorPosition(position-1)
        self.field.field_name = field_name
        self.field.field_description = self.field_description_lineEdit.text()


class CheckActionWidget(QgsCollapsibleGroupBox, Ui_CheckActionWidget):
    def __init__(self, name="action", service="", action_object=None, collapsed=False, parent=None):
        """
        Constructor
        """
        super().__init__(parent)
        self.setupUi(self)
        self.setObjectName(str(name))
        self.setWindowTitle("Checkaction")
        self.setTitle(name)
        self.setCollapsed(collapsed)
        self.action = action_object

        if not collapsed:
            self.show()
        self.service_lineEdit.setText(service)
        self.name_lineEdit.setText(name)
        self.name_lineEdit.textChanged.connect(self.setTitle)

        self.service_lineEdit.textChanged.connect(self.set_action)
        self.name_lineEdit.textChanged.connect(self.set_action)

    def get_action(self):
        return self.action

    def set_action(self):
        check_line_edit_starts_by_slash(self.service_lineEdit)
        self.action.name = self.name_lineEdit.text()
        self.action.action_id = self.service_lineEdit.text()


class CheckParamWidget(QgsCollapsibleGroupBox, Ui_CheckParameterWidget):
    def __init__(self, value="0", collapsed=False, parent=None):
        """
        Constructor
        """
        super().__init__(parent)
        self.setupUi(self)
        self.setObjectName(str(value))
        self.setWindowTitle("CheckParam")
        self.setTitle(value)
        self.setCollapsed(collapsed)

        if not collapsed:
            self.show()
        self.value_lineEdit.setText(value)
        self.value_lineEdit.textChanged.connect(self.setTitle)
