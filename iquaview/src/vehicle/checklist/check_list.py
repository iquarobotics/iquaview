# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Classes to parse a check list xml file and dynamically load the
 items into dialogs to show the data to the user so that he/she
 can evaluate the correctness of the data.
"""
import os
import logging

from PyQt5.QtWidgets import QDialog

from iquaview.src.vehicle.checklist.loadcheckactiondlg import LoadCheckActionDialog
from iquaview.src.vehicle.checklist.loadcheckparamdlg import LoadCheckParamDialog
from iquaview.src.vehicle.checklist.loadsummarydlg import LoadSummaryDialog
from iquaview.src.vehicle.checklist.topic import Topic
from iquaview_lib.xmlconfighandler.checklisthandler import CheckListHandler
from iquaview_lib.definitions import ROSBRIDGE_SERVER_PORT

logger = logging.getLogger(__name__)


class CheckTopic:
    """Checklist Topic"""

    def __init__(self, name=None, topic=None):
        self.name = name
        self.topic = Topic()
        logger.debug("           Name: {}".format(self.name))
        if topic is not None:
            self.topic.set_topic(topic)

    def get_ct_topic(self, ip, port, vehicle_namespace):
        return self.topic.get_topic_data(ip, port, vehicle_namespace)

    def is_valid(self):
        """
        Check if the topic is valid
        :return: True if the topic is valid, False otherwise
        :rtype: bool
        """
        if self.name is None or self.name == "":
            return False
        return self.topic.is_valid()


class CheckAction:
    """Checklist Action"""

    def __init__(self, name=None, action_id=None, parameters=None):
        self.name = name
        self.action_id = action_id
        self.parameters = []
        logger.debug("           Name: {}".format(self.name))
        logger.debug("           Action ID: {}".format(self.action_id))

        if parameters is not None:
            for param in parameters:
                self.parameters.append(param.text)
                logger.debug("             Param: ".format(param.text))

    def get_name(self):
        return self.name

    def get_action_id(self):
        return self.action_id

    def get_parameters(self):
        return self.parameters

    def is_valid(self):
        """
        Check if the action is valid
        :return: True if the action is valid, False otherwise
        :rtype: bool
        """
        if self.name is None or self.name == "":
            return False
        if self.action_id is None or self.action_id == "":
            return False

        for param in self.parameters:
            if param is None or param == "":
                return False
        return True


class CheckItem:
    """ Checklist Item"""

    def __init__(self, description=None):
        self.description = description
        self.check_topics = []
        self.check_actions = []
        self.correct_values = False

    def is_valid(self):
        """
        Check if the item is valid
        :return: True if the item is valid, False otherwise
        """
        if (self.description is None
                or self.description == ""
                or (len(self.check_topics) == 0
                    and len(self.check_actions) == 0)):
            return False

        for topic in self.check_topics:
            if not topic.is_valid():
                return False

        for action in self.check_actions:
            if not action.is_valid():
                return False

        return True


class CheckList:
    """Checklist"""

    def __init__(self, name=None, items=None):
        self.name = name
        self.items = []

    def is_valid(self):
        """
        Check if the checklist is valid
        :return: True if the checklist is valid, False otherwise
        """
        if self.name is None or self.name == "":
            return False

        for item in self.items:
            if not item.is_valid():
                return False

        return True


def read_configuration(vehicle_info, config, chk_name):
    """
    Read the checklist 'chk_name' from configuration 'config' and
    load Actions and Params from checklist.
    Finally load Summary Dialog.

    :param vehicle_info:
    :type vehicle_info: VehicleInfo
    :param config:
    :type config: Config
    :param chk_name:
    :type  chk_name: String

    """
    ip = vehicle_info.get_vehicle_ip()
    vehicle_namespace = vehicle_info.get_vehicle_namespace()

    config_filename = os.path.abspath(os.path.expanduser(config.settings['last_auv_config_xml']))
    cl_handler = CheckListHandler(config_filename)
    # all check_list items
    check_items = cl_handler.get_items_from_checklist(chk_name)

    n = 0
    check_list = []
    logger.debug("-------ITEMS--------")
    # for each list item
    while 0 <= n < len(check_items):
        result = False
        load_dialog = None
        item = check_items[n]
        check_item = CheckItem()
        logger.debug(item.tag)
        # get all matches
        description = cl_handler.get_description_from_item(item)
        check_topics = cl_handler.get_check_topics_from_item(item)
        check_actions = cl_handler.get_check_actions_from_item(item)

        logger.debug("     {}".format(description.tag))
        # description
        logger.debug("          {}".format(description.text))
        check_item.description = description.text
        # check topic
        if check_topics:
            for c_topic in check_topics:
                check_topic = CheckTopic(c_topic[0].text, c_topic[1])
                check_item.check_topics.append(check_topic)

            load_dialog = LoadCheckParamDialog(check_item.description, check_item.check_topics, n,
                                               ip, ROSBRIDGE_SERVER_PORT, vehicle_namespace)
            result = load_dialog.exec_()

        # check action
        elif check_actions:
            for c_action in check_actions:
                check_action = CheckAction(c_action[0].text, c_action[1].text, c_action[2])
                check_item.check_actions.append(check_action)
            # TODO accept more than one check_action in dialog
            load_dialog = LoadCheckActionDialog(ip,
                                                ROSBRIDGE_SERVER_PORT,
                                                vehicle_namespace,
                                                check_item.description, check_action.get_name(),
                                                check_action.get_action_id(),
                                                check_action.get_parameters(), n)
            result = load_dialog.exec_()

        if result == QDialog.Accepted:
            if not load_dialog.back_clicked():
                check_item.correct_values = load_dialog.isChecked()
                check_list.append(check_item)
                n += 1
            else:
                n -= 1
                if n >= 0:
                    check_list.pop()
        else:
            # on close dialog, break while and put n=0 to no show summary
            n = 0
            break

    logger.debug("--------------------")
    # if check more than 1 item, show summary
    if n > 0:
        summary = LoadSummaryDialog(chk_name)
        summary.set_summary(check_list)
        summary.exec_()
