# Copyright (c) 2023 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Class to periodically check the status of the NTP offset between the control station and the AUV.
"""
import subprocess
import re

from iquaview_lib.vehicle.vehicleinfo import VehicleInfo
from iquaview_lib.utils.workerthread import Worker
from iquaview_lib.connection.ippingchecker import IPPingChecker

from PyQt5.QtCore import pyqtSignal, QObject, QTimer, QThreadPool


class SyncStatus(QObject):
    update_signal = pyqtSignal()
    reconnect_signal = pyqtSignal()

    def __init__(self, vehicle_info: VehicleInfo):
        super().__init__()
        self.vehicle_info = vehicle_info
        self.closing = False
        self.connected = False
        self.status = "-"
        self.timer = QTimer()
        self.timer.setSingleShot(True)
        self.timer.timeout.connect(self.start_sync_thread)

        self.ip_ping_checker = IPPingChecker(self.vehicle_info.get_vehicle_ip())
        self.ip_ping_checker.is_connected_signal.connect(self.set_connected)

        self.threadpool = QThreadPool()

        self.reconnect_signal.connect(self.reconnect_sync_thread)

        self.start_sync_thread()

    def set_connected(self, connected):
        """ Set connected state """
        self.connected = connected

    def reconnect_sync_thread(self):
        """ Start timer """
        self.timer.start(15000)
        self.ip_ping_checker.ip_address = self.vehicle_info.get_vehicle_ip()

    def start_sync_thread(self):
        """ Start sync thread """
        if not self.closing:
            worker = Worker(self.update_sync_status)
            worker.signals.finished.connect(self.reconnect_sync_thread)
            self.threadpool.start(worker)

    def update_sync_status(self):
        """Refresh sync status. Timer to get periodically sync status. """
        if self.connected:
            # ntpdate
            proc = subprocess.Popen(["ntpdate", "-q", self.vehicle_info.get_vehicle_ip()],
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE)
            output, error = proc.communicate()
            output = output.decode()

            match = re.search(r"offset ([\d.-]+) sec", output)
            if match:
                offset_sec = abs(float(match.group(1)))
                offset_ms = offset_sec * 1000  # Convert to milliseconds
                # A time difference of less than 128ms is required to maintain NTP synchronization.
                if offset_ms <= 128:
                    self.status = "Yes"
                else:
                    self.status = "No"
            else:
                self.status = "Error"

        else:
            self.status = "Unavailable"

        self.update_signal.emit()

    def get_sync_status(self) -> str:
        """ Get sync status"""
        return self.status

    def is_sync(self) -> bool:
        """ Sync state"""
        return self.status == "Yes"

    def disconnect_object(self):
        """ Disconnect sync status"""
        self.closing = True
        self.status = "-"
        self.timer.stop()
        self.ip_ping_checker.stop_ping()
        self.threadpool.clear()
