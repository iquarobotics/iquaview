# -*- coding: utf-8 -*-
# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
Profile Tool
"""
import bisect
import logging
from typing import Tuple

import matplotlib
import numpy as np
from PyQt5.QtCore import pyqtSignal, Qt
from PyQt5.QtGui import QPalette
from PyQt5.QtWidgets import QWidget, QSizePolicy, QButtonGroup

from iquaview_lib.utils.qgisutils import transform_point
from iquaview_lib.utils.qt_signal import reconnect_signal_to_slot

matplotlib.use("Qt5Agg")

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.widgets import Cursor
from matplotlib import colormaps
from qgis.core import (QgsRasterLayer, QgsPointXY, QgsRaster, QgsMapLayer, QgsGeometry, QgsDistanceArea, QgsProject,
                       QgsCoordinateReferenceSystem)

from iquaview.src.tools.profile_tool.maptoolrenderer import MapToolRenderer
from iquaview.src.ui.ui_profile_tool import Ui_ProfileTool

logger = logging.getLogger(__name__)


def get_dynamic_color(index: int, total_colors: int, cmap_name: str = 'plasma') -> Tuple[float, float, float, float]:
    """
    Generate a color dynamically using a colormap.
    :param index: Current data index.
    :param total_colors: Total number of colors needed.
    :param cmap_name: Name of the colormap to use.

    :return: A tuple representing the RGBA color, e.g., (0.1, 0.2, 0.3, 1.0).
      - The first three values are the RGB components (range 0 to 1).
      - The last value is the alpha channel (opacity), also in the range [0, 1].
    """
    cmap = colormaps[cmap_name]
    color = cmap(index / total_colors)

    return color


class ProfileTool(QWidget, Ui_ProfileTool):
    """
    Profile Tool widget.

    :param canvas: Parent canvas
    :param parent: Parent widget
    """

    map_tool_change_signal = pyqtSignal()
    start_timer_signal = pyqtSignal()
    stop_timer_signal = pyqtSignal()

    def __init__(self, canvas, parent):
        """
        Constructor.
        """

        super().__init__(parent)
        self.setupUi(self)
        self.setAttribute(Qt.WA_DeleteOnClose)

        self.depths = {"l": [], "z": []}
        self.point_list = []  # type: list[QgsPointXY]

        self.canvas = canvas
        self.parent = parent

        self.map_tool_renderer = MapToolRenderer(self.canvas, self)

        self.plot = FigureCanvas(Figure(figsize=(15, 3)))
        self.plot_axes = self.plot.figure.subplots()
        self.plot_axes.set_xlabel("Length [m]")
        self.plot_axes.set_ylabel("Depth [m]")
        self.figure_widget.layout().addWidget(self.plot)
        self.plot.sizePolicy().horizontalPolicy = QSizePolicy.Expanding
        if parent is not None:
            self.plot.figure.set_facecolor(parent.palette().color(QPalette.Window).name())
        self.plot.figure.tight_layout()

        self.plot.mpl_connect('motion_notify_event', self.mouse_event_mpl)
        self.plot.mpl_connect('figure_leave_event', self.mouse_exit_mpl)
        self.plot.mpl_connect('axes_leave_event', self.mouse_exit_mpl)

        self.plot_cursor = Cursor(self.plot_axes, horizOn=False, vertOn=True, useblit=True, color='black', linewidth=1)
        self.annot = None

        self.layers = []
        self.infos = []

        self.selection_method = "temp_polyline"

        self.layer_view = self.parent.view
        self.layer_view.currentLayerChanged.connect(self.change_selection_method)

        self.layer_root = self.parent.root
        self.layer_root.removedChildren.connect(self.update_plot)
        self.layer_root.addedChildren.connect(self.update_plot)

        self.mission_module = self.parent.mission_module
        self.mission_controller = self.parent.mission_ctrl

        self.mission_controller.finish_mission_action_signal.connect(self.update_plot)
        self.canvas.destinationCrsChanged.connect(self.canvas_crs_changed)
        self.current_crs = self.canvas.mapSettings().destinationCrs()

        # call to set tool
        self.set_point_tool()

        self.buttonGroup = QButtonGroup()
        self.buttonGroup.addButton(self.draw_path_radioButton, 1)
        self.buttonGroup.addButton(self.plot_mission_radioButton, 2)
        self.buttonGroup.buttonToggled.connect(self.selection_type_changed)

    def layer_is_mission(self, layer, node):
        """
        Checks if the layer is a mission layer

        :param layer: Layer to check
        :type layer: QgsMapLayer
        :param node: Node of the layer
        :type node: QgsLayerTreeNode
        :return: True if the layer is a mission layer and is currently visible; False otherwise"""

        return layer is not None and layer.type() == QgsMapLayer.VectorLayer and self.mission_module.is_mission_layer(
            layer)

    def change_selection_method(self):
        """Handles what happens when the selected layer changes"""

        node = self.layer_view.currentNode()
        layer = self.layer_view.currentLayer()

        self.plot_mission_radioButton.setChecked(self.layer_is_mission(layer, node))
        self.draw_path_radioButton.setChecked(not self.layer_is_mission(layer, node))

    def canvas_crs_changed(self):
        """ Handles what happens when the canvas CRS changes """

        # Transform all points in one batch
        self.current_crs = self.canvas.mapSettings().destinationCrs()

        # Update the map tool renderer in the main thread
        self.full_plot_update()

    def update_plot(self):
        """ Handles what happens when a layer is hidden or shown """
        self.layers = self.get_raster_layers()

        if self.selection_method == "mission_layer":
            self.use_current_mission()
        else:
            self.clear()
            self.full_plot_update()

    def use_current_mission(self):
        """ Uses the currently selected mission layer to plot the profile """
        node = self.layer_view.currentNode()
        layer = self.layer_view.currentLayer()

        if self.layer_is_mission(layer, node):
            self.clear()
            layer_crs = layer.crs()

            self.point_list = []
            mission_track = self.mission_controller.get_current_mission()  # MissionTrack

            reconnect_signal_to_slot(mission_track.get_mission_layer().afterCommitChanges, self.update_plot)

            for i in range(mission_track.get_mission_length()):
                step = mission_track.get_step(i)
                lat = step.get_maneuver().final_latitude
                lon = step.get_maneuver().final_longitude

                layer_point = QgsPointXY(lon, lat)

                map_point = transform_point(
                    layer_point,
                    layer_crs,
                    QgsCoordinateReferenceSystem.fromEpsgId(3857)
                )

                self.point_list.append(map_point)

            if self.point_list:
                self.mission_name_label.setText(f"{self.mission_controller.get_current_mission_name()}")
                self.full_plot_update()
            else:
                self.clear()

    def selection_type_changed(self, button, checked):
        """ Handles what happens when the selection type is changed """

        if self.buttonGroup.id(button) == 1 and checked:  # Temp Polyline
            self.selection_method = "temp_polyline"
            self.clear()
            self.set_point_tool()
            try:
                self.layer_view.currentLayerChanged.disconnect(self.use_current_mission)
            except TypeError:
                pass
        elif self.buttonGroup.id(button) == 2 and checked:  # Mission Layer
            self.selection_method = "mission_layer"
            self.clear()
            self.layer_view.currentLayerChanged.connect(self.use_current_mission)

            self.map_tool_renderer.deactivate()
            self.use_current_mission()

    def get_raster_layers(self):
        """ Gets all the visible raster layers in the project """

        layers = []

        for layer in self.layer_root.checkedLayers():
            if isinstance(layer, QgsRasterLayer) and layer.bandCount() == 1 and layer.providerType() == "gdal":
                layers.append(layer)

        return layers

    def mouse_exit_mpl(self, _):
        """ Handles what happens when the mouse leaves the matplotlib widget """
        if self.annot:
            self.annot.set_visible(False)
            self.annot = None
        self.map_tool_renderer.rubberbandpoint.hide()
        self.plot.draw()

    def mouse_event_mpl(self, event):
        """
        Handles what happens when the mouse is moved over the plot

        :param event: The mouse event
        :type event: matplotlib.backend_bases.MouseEvent
        """
        if not self.infos:
            return

        if event.xdata and event.ydata:
            if self.annot is None:
                self.annot = self.plot_axes.annotate(event.ydata, xy=(event.xdata, event.ydata), xytext=(0.5, 0.5),
                                                     bbox=dict(boxstyle='round4', fc='linen', ec='k', lw=1),
                                                     color='black')
                self.annot.set_visible(True)

            try:
                max_x = self.infos[0]["lengths"][-1]
            except IndexError:
                max_x = 1.0

            xdata = float(event.xdata)
            clamped_xdata = max(0.0, min(xdata, max_x))

            ydata = float(event.ydata)

            min_y_dist_depth, min_y_dist_layer_idx, min_y_dist_idx = self.find_layer_closest_to_mouse(clamped_xdata,
                                                                                                      ydata)
            if min_y_dist_layer_idx is None:
                min_y_dist_depth = None
            elif xdata < 0.0:
                min_y_dist_depth = self.infos[min_y_dist_layer_idx]["depths"][0]
            elif xdata > max_x:
                min_y_dist_depth = self.infos[min_y_dist_layer_idx]["depths"][-1]
            else:
                min_y_dist_depth = self.interpolate_depth_at_x(clamped_xdata, min_y_dist_layer_idx, min_y_dist_idx)

            self.annot.xy = (event.xdata, event.ydata)

            if min_y_dist_depth is None or np.isnan(min_y_dist_depth):
                self.annot.set_text("No data")
                self.annot.set_position((clamped_xdata, event.ydata))
                self.annot.set_color('black')
            else:
                self.annot.set_text(f"{min_y_dist_depth:.2f}m")
                self.annot.set_position((clamped_xdata, min_y_dist_depth))
                color = get_dynamic_color(min_y_dist_layer_idx, len(self.infos))  # Generate a unique color
                self.annot.set_color(color)

            if max_x > 0.0:
                self.update_cursor_on_map(clamped_xdata / max_x)
            else:
                self.update_cursor_on_map(0.0)
            self.plot.draw()

    def find_layer_closest_to_mouse(self, x_pos, y_pos):
        """
        Finds the closest layer (in the vertical axis) to the mouse in the matplotlib plot widget

        :param x_pos: The x position of the mouse
        :type x_pos: float
        :param y_pos: The y position of the mouse
        :type y_pos: float
        :return: The following information of the closest layer: the depth at x_pos, the layer index, and the index of
        the length closest to x_pos
        :rtype: (float, int, int)
        """

        closest_layer_info = self.infos[0]
        min_distance = float('inf')

        for i, info in enumerate(self.infos):
            if info["lengths"] and info["depths"]:
                dist_idx = bisect.bisect_left(info["lengths"], x_pos)
                depth = self.interpolate_depth_at_x(x_pos, i, dist_idx)

                if depth is not None:
                    distance = abs(depth - y_pos)

                    if distance < min_distance:
                        min_distance = distance
                        closest_layer_info = info

        min_y_dist_layer_idx = self.infos.index(closest_layer_info)
        min_y_dist_idx = bisect.bisect_left(closest_layer_info["lengths"], x_pos)
        min_y_dist_depth = self.interpolate_depth_at_x(x_pos, min_y_dist_layer_idx, min_y_dist_idx)

        return min_y_dist_depth, min_y_dist_layer_idx, min_y_dist_idx

    def interpolate_depth_at_x(self, x_pos, layer_idx, dist_idx):
        """
        Given an x position it interpolates the depth from the layer infos between the 2 closest points

        :param x_pos: The X position (length)
        :type x_pos: float
        :param layer_idx: The index of the layer to interpolate from its data
        :type layer_idx: int
        :param dist_idx: The index of x_pos in the "lengths" array
        :type dist_idx: int
        :return: The new interpolated depth
        :rtype: float
        """

        if layer_idx is not None:
            try:
                lengths = self.infos[layer_idx]["lengths"]
                depths = self.infos[layer_idx]["depths"]

                if dist_idx < 0:
                    return depths[0]

                if dist_idx >= len(lengths) - 1:
                    return depths[-1]

                lo_l, hi_l = lengths[dist_idx], lengths[dist_idx + 1]
                lo_d, hi_d = depths[dist_idx], depths[dist_idx + 1]

                if x_pos < lo_l:
                    return lo_d

                if x_pos > hi_l:
                    return hi_d

                rel_index = (x_pos - lo_l) / (hi_l - lo_l)
                delta_d = hi_d - lo_d

                return lo_d + rel_index * delta_d

            except (TypeError, IndexError):
                return None

        return None

    def update_cursor_on_map(self, relative_x):
        """
        Shows a dot on the map where the cursor is on the plot

        :param relative_x: The x_coord coordinate of the cursor, from 0 to 1
        :type relative_x: float
        """

        node = self.layer_view.currentNode()

        if self.point_list and node.itemVisibilityChecked():
            if relative_x is not None:

                geom = QgsGeometry().fromPolylineXY(self.point_list)
                x_coord = relative_x * geom.length()

                try:
                    if len(self.point_list) > 1:
                        pointprojected = geom.interpolate(x_coord).asPoint()
                    else:
                        pointprojected = self.point_list[0]
                except (IndexError, AttributeError, ValueError):
                    pointprojected = None

                if pointprojected:
                    cursor_point = transform_point(
                        pointprojected,
                        QgsCoordinateReferenceSystem.fromEpsgId(3857),
                        self.canvas.mapSettings().destinationCrs(),
                    )
                    self.map_tool_renderer.rubberbandpoint.setCenter(cursor_point)

            self.map_tool_renderer.rubberbandpoint.show()
        else:
            self.map_tool_renderer.rubberbandpoint.hide()

    def reset_graph(self):
        """ Resets the graph plot """

        self.plot_axes.clear()
        self.plot_axes.set_xlabel("Length [m]")
        self.plot_axes.set_ylabel("Depth [m]")
        self.mission_name_label.setText("No mission selected")
        self.depths = {"l": [], "z": []}
        self.point_list = []
        self.layers = self.get_raster_layers()
        self.reset_layer_infos()
        self.annot = None
        if self.plot.figure.get_axes():
            self.plot.figure.tight_layout()
        self.plot.draw()

    def reset_layer_infos(self):
        """ Resets the layer infos """

        self.infos = []
        for layer in self.layers:
            self.infos.append({"layer": layer, "depths": [], "lengths": []})

    def set_point_tool(self):
        """ Connects the rubberband tool if needed """
        self.layer_view.setCurrentLayer(None)
        self.map_tool_change_signal.emit()

        self.layers = self.get_raster_layers()
        self.reset_layer_infos()

        self.draw_path_radioButton.setChecked(True)
        self.mission_name_label.hide()
        self.line.hide()
        self.map_tool_renderer.connect_tool()

    def full_plot_update(self):
        """ Updates the layer infos and plot based on the current point list """
        self.plot_axes.clear()
        self.reset_layer_infos()

        for i in range(len(self.layers)):
            if len(self.point_list) > 1:
                for p_start, p_end in zip(self.point_list[:-1], self.point_list[1:]):
                    self.update_layer_depths_and_lengths(p_start, p_end, i)

            self.plot_single_layer(i)

        self.finalize_plot()

    def update_layer_depths_and_lengths(self, start_point, end_point, i):
        """
        Updates the depths and lengths of the specified layer using two points.

        :param start_point: Starting point for the calculation
        :param end_point: Ending point for the calculation
        :param i: Index of the layer in self.infos
        """
        layer = self.infos[i]["layer"]
        last_length = self.infos[i]["lengths"][-1] if self.infos[i]["lengths"] else 0.0
        newlist = self.calculate_depths_between_two_points(layer, start_point, end_point, last_length)
        self.infos[i]["depths"] += newlist["z"]
        self.infos[i]["lengths"] += newlist["l"]

    def plot_single_layer(self, i):
        """ Plots a single layer on the axes """
        depths = self.infos[i]["depths"]
        lengths = self.infos[i]["lengths"]
        if lengths:
            self.plot_axes.set_xlim(right=lengths[-1])

        if not np.all(np.isnan(depths)):  # Check if all elements in depths are NOT NaN
            color = get_dynamic_color(i, len(self.infos))
            self.plot_axes.plot(lengths, depths, label=self.infos[i]["layer"].name(), color=color)

    def finalize_plot(self):
        """ Finalizes the plot with common settings """
        self.plot_axes.invert_yaxis()
        self.plot_axes.set_xlim(left=0)
        self.plot_axes.legend()
        self.plot_axes.grid(True)
        self.plot_axes.set_xlabel("Length [m]")
        self.plot_axes.set_ylabel("Depth [m]")
        self.plot.figure.tight_layout()
        self.plot.draw()

    def add_point_and_update_profile(self, new_map_point):
        """
        Adds a point to the point list and updates the layer infos and the plot

        :param new_map_point: The new point to add, in map coordinates
        :type new_map_point: QgsPointXY
        """
        map_point = transform_point(
            new_map_point,
            self.canvas.mapSettings().destinationCrs(),
            QgsCoordinateReferenceSystem.fromEpsgId(3857)
        )

        self.point_list.append(map_point)
        if len(self.point_list) > 1:
            for i in range(len(self.layers)):
                self.update_layer_depths_and_lengths(self.point_list[-2], self.point_list[-1], i)

            self.full_plot_update()

    def calculate_depths_between_two_points(self, layer, point_a, point_b, length_before):
        """
        Calculates the depths between two points on a raster layer

        :param layer: The raster layer to use
        :type layer: QgsRasterLayer
        :param point_a: The first point
        :type point_a: QgsPointXY
        :param point_b: The second point
        :type point_b: QgsPointXY
        :param length_before: The length before the first point; 0 if this is the absolute first point
        :type length_before: float
        :return: The depths and lengths
        :rtype: dict
        """
        # Get the values on the lines
        lengths = []
        x_coords = []
        y_coords = []
        crs = QgsCoordinateReferenceSystem.fromEpsgId(3857)

        points_to_cal_1 = transform_point(point_a, crs, layer.crs())
        points_to_cal_2 = transform_point(point_b, crs, layer.crs())

        x1_c, y1_c = float(points_to_cal_1.x()), float(points_to_cal_1.y())
        x2_c, y2_c = float(points_to_cal_2.x()), float(points_to_cal_2.y())

        self.distance_calc = QgsDistanceArea()
        self.distance_calc.setSourceCrs(crs, QgsProject.instance().transformContext())
        self.distance_calc.setEllipsoid(QgsProject.instance().ellipsoid())

        # Points in lat,lon(y,x)
        distance = self.distance_calc.measureLine(point_a, point_b)
        # Calculate resolution to use
        try:
            res = min(layer.rasterUnitsPerPixelX(), layer.rasterUnitsPerPixelY()) * distance / max(abs(x2_c - x1_c),
                                                                                                   abs(y2_c - y1_c))
            # Ensure a minimum resolution to avoid divide-by-zero errors
            res = max(res, 0.1)  # Minimum resolution (e.g., 0.1 meters)
        except ZeroDivisionError:
            res = min(layer.rasterUnitsPerPixelX(), layer.rasterUnitsPerPixelY()) * 1.2
        except AttributeError:
            # Handle layers without rasterUnitsPerPixel attributes
            res = 1
        # Safeguard against zero or undefined resolution
        if res <= 0:
            res = 1  # Set a reasonable default resolution

        # Determine number of steps
        steps = max(int(distance / res), 1)  # Ensure at least 1 step

        dx_c = (x2_c - x1_c) / steps
        dy_c = (y2_c - y1_c) / steps
        dl_d = distance / steps

        for step_n in range(0, steps + 1):
            x_c = x1_c + dx_c * step_n
            y_c = y1_c + dy_c * step_n
            l_d = dl_d * step_n + length_before
            x_coords.append(x_c)
            y_coords.append(y_c)
            lengths.append(l_d)

        # Extract the profile for the whole path
        depths = self.extract_depth_values(layer, x_coords, y_coords)
        profiles = {"l": lengths, "z": depths, "x": x_coords, "y": y_coords}

        return profiles

    @staticmethod
    def extract_depth_values(layer, x_coords, y_coords):
        chosen_band = 1
        depths = []

        layer_extent = layer.extent()  # Get the extent of the raster layer

        for x, y in zip(x_coords, y_coords):
            point = QgsPointXY(x, y)
            attr = np.nan
            # Check if the point is within the extent of the raster layer
            if layer_extent.contains(point):
                ident = layer.dataProvider().identify(point, QgsRaster.IdentifyFormatValue)
                if chosen_band in ident.results():
                    if ident.results()[chosen_band] is not None:
                        # negative to display depth as positive
                        attr = -ident.results()[chosen_band]

            depths.append(attr)

        return depths

    def clear(self):
        """ Remove everything and reset the tool """
        self.point_list = []
        self.map_tool_renderer.clear()
        self.reset_graph()

    def resizeEvent(self, event):
        """ Resize the graph when the widget is resized """
        self.plot.figure.tight_layout()

    def closeEvent(self, event):
        """ Overrides closeEvent """
        super().closeEvent(event)
        self.clear()
        self.map_tool_renderer.deactivate()
        try:
            self.layer_view.currentLayerChanged.disconnect(self.use_current_mission)
        except TypeError:
            pass

        try:
            self.layer_view.currentLayerChanged.disconnect(self.change_selection_method)
        except TypeError:
            pass

        try:
            self.buttonGroup.buttonToggled.disconnect(self.selection_type_changed)

        except TypeError:
            pass

        try:
            self.mission_controller.finish_mission_action_signal.disconnect(self.update_plot)
        except TypeError:
            pass
        try:
            self.layer_root.removedChildren.disconnect(self.update_plot)
        except TypeError:
            pass

        try:
            self.layer_root.addedChildren.disconnect(self.update_plot)
        except TypeError:
            pass

        try:
            self.canvas.destinationCrsChanged.disconnect(self.canvas_crs_changed)
        except TypeError:
            pass
