# -*- coding: utf-8 -*-
# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
Profile Tool Map Tool
"""

from PyQt5.QtCore import pyqtSignal, Qt
from PyQt5.QtGui import QCursor
from qgis.gui import QgsMapTool


class MapTool(QgsMapTool):
    """
    Profile Tool Map Tool.

    :param canvas: Parent canvas
    """

    moved = pyqtSignal(dict)
    right_clicked = pyqtSignal(dict)
    left_clicked = pyqtSignal(dict)
    double_clicked = pyqtSignal(dict)
    desactivate = pyqtSignal()

    def __init__(self, canvas):
        QgsMapTool.__init__(self, canvas)
        self.canvas = canvas
        self.cursor = QCursor(Qt.CrossCursor)

    def canvasMoveEvent(self, event):
        """ Mouse move event """
        self.moved.emit({'x': event.pos().x(), 'y': event.pos().y()})
        super().canvasMoveEvent(event)

    def canvasReleaseEvent(self, event):
        """ Mouse release event """
        if event.button() == Qt.RightButton:
            self.right_clicked.emit({'x': event.pos().x(), 'y': event.pos().y()})
        else:
            self.left_clicked.emit({'x': event.pos().x(), 'y': event.pos().y()})

        super().canvasReleaseEvent(event)

    def canvasDoubleClickEvent(self, event):
        """ Mouse double click event """
        self.double_clicked.emit({'x': event.pos().x(), 'y': event.pos().y()})
        super().canvasDoubleClickEvent(event)

    def activate(self):
        """ Activate map tool """
        QgsMapTool.activate(self)
        self.canvas.setCursor(self.cursor)

    def deactivate(self):
        self.desactivate.emit()
        QgsMapTool.deactivate(self)
