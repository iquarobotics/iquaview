# -*- coding: utf-8 -*-
# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
    This module contains the ProfileToolModule class. It is the class that handles the Qt interface for the Profile tool
"""

from PyQt5.QtCore import QObject, Qt, QEvent, QSize
from PyQt5.QtGui import QIcon, QCloseEvent, QResizeEvent
from PyQt5.QtWidgets import QAction, QDockWidget, QMainWindow

from iquaview.src.tools.profile_tool.profiletool import ProfileTool


class ProfileToolModule(QObject):
    """ This class handles the Qt interface for the Profile tool"""

    def __init__(self, canvas, parent: QMainWindow = None) -> None:
        super().__init__(parent)
        self.profile_tool_dockwidget = None  # Ensure singleton instance
        self.canvas = canvas
        self.profile_tool_action = QAction(QIcon(":/resources/mActionProfile.svg"), "Profile Tool", self)
        self.profile_tool_action.setCheckable(True)
        self.profile_tool_action.triggered.connect(self.profile_tool)

    def profile_tool(self):
        """ Open Profile Tool dockwidget"""
        if self.profile_tool_dockwidget is None:
            self.parent().reset_map_tool()
            self.profile_tool_dockwidget = QDockWidget("Profile Tool")
            self.profile_tool_dockwidget.installEventFilter(self.parent())
            self.profile_tool_dockwidget.setAllowedAreas(Qt.BottomDockWidgetArea | Qt.TopDockWidgetArea)
            profile_tool_wdg = ProfileTool(self.canvas, self.parent())
            self.profile_tool_dockwidget.setWidget(profile_tool_wdg)
            self.parent().addDockWidget(Qt.BottomDockWidgetArea, self.profile_tool_dockwidget)
            self.profile_tool_dockwidget.installEventFilter(self)
            self.profile_tool_dockwidget.show()
        elif not self.profile_tool_action.isChecked():
            self.profile_tool_dockwidget.widget().clear()
            self.profile_tool_dockwidget.widget().deleteLater()
            self.profile_tool_dockwidget.close()
            self.profile_tool_dockwidget = None

    def get_map_tool(self):
        if self.profile_tool_dockwidget is not None and self.profile_tool_dockwidget.widget() is not None:
            return self.profile_tool_dockwidget.widget().map_tool_renderer.tool
        return None

    def get_map_tool_renderer(self):
        if self.profile_tool_dockwidget is not None and self.profile_tool_dockwidget.widget() is not None:
            return self.profile_tool_dockwidget.widget().map_tool_renderer
        return None

    def eventFilter(self, obj: QObject, event: QEvent) -> bool:
        """
        Filters events if this object has been installed as an event filter for the obj object.
        """
        # Take advantage of lazy evaluation to avoid calling eventFilter() on the parent object if the
        # profile_tool_dockwidget is not ready yet
        if event.type() == QCloseEvent().type() and obj == self.profile_tool_dockwidget:
            self.profile_tool_dockwidget.widget().closeEvent(event)
            self.profile_tool_action.setChecked(False)
            self.profile_tool_dockwidget = None

            if self.parent().mission_module.edit_wp_mission_action.isChecked():
                self.parent().canvas.setMapTool(self.parent().mission_ctrl.get_edit_wp_mission_tool())
            elif self.parent().mission_module.select_features_mission_action.isChecked():
                self.parent().canvas.setMapTool(self.parent().mission_ctrl.selectfeattool)
            elif self.parent().mission_module.move_mission_action.isChecked():
                self.parent().canvas.setMapTool(self.parent().mission_ctrl.move_mission())

            return True
        elif event.type() == QResizeEvent(QSize(), QSize()).type() and obj == self.profile_tool_dockwidget:
            self.profile_tool_dockwidget.widget().resizeEvent(event)
            return True

        return QObject.eventFilter(self, obj, event)
