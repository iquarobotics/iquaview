# -*- coding: utf-8 -*-
# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
Profile Tool Map Tool Renderer
"""
import logging

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QColor
from qgis.core import QgsPointXY, QgsWkbTypes
from qgis.gui import QgsMapToolPan, QgsRubberBand, QgsVertexMarker, QgsMapCanvas

from iquaview.src.tools.profile_tool.maptool import MapTool

logger = logging.getLogger(__name__)


class MapToolRenderer:
    """
    Map Tool Renderer.
    """

    def __init__(self, canvas: QgsMapCanvas, profile_tool: "ProfileTool"):
        """
        Constructor
        :param canvas: Parent canvas
        :type canvas: QgsMapCanvas
        :param profile_tool: Profile Tool core widget
        :param profile_tool: ProfileTool
        """
        self.profile_tool = profile_tool  # type: ProfileTool
        self.canvas = canvas

        self.dblclktemp = None  # Enable disctinction between leftclick and doubleclick

        self.point_list = []  # Polyline being drawn in freehand mode

        self.tool = MapTool(self.canvas)  # the mouselistener
        self.tool_pan = QgsMapToolPan(self.canvas)

        color = QColor("orange")
        # The rubberband
        self.rubberband = QgsRubberBand(canvas, QgsWkbTypes.LineGeometry)
        self.rubberband.setWidth(2)
        self.rubberband.setColor(color)

        self.rubberband2 = QgsRubberBand(canvas, QgsWkbTypes.LineGeometry)
        self.rubberband2.setWidth(1)
        self.rubberband2.setColor(color)
        self.rubberband2.setLineStyle(Qt.DashLine)

        self.rubberbandpoint = QgsVertexMarker(canvas)
        self.rubberbandpoint.setColor(QColor(Qt.black))
        self.rubberbandpoint.setIconSize(5)
        self.rubberbandpoint.setIconType(QgsVertexMarker.ICON_BOX)  # or ICON_CROSS, ICON_X
        self.rubberbandpoint.setPenWidth(3)

        self.rubberbandbuf = QgsRubberBand(canvas)
        self.rubberbandbuf.setWidth(1)
        self.rubberbandbuf.setColor(color)

        self.selection_method = "temp_polyline"

        self.left_clicked_bool = False

    def set_point_list(self, point_list):
        """
        Set the point list. We assume that if this is called, we are not adding any new points. So this results in a
        new rubberband that is already finished (as if the user had doubleclicked)
        :param point_list: Point list
        :type point_list: list
        """

        self.reset_rubber_band()
        self.reset_rubber_band2()
        for point in point_list:
            self.rubberband.addPoint(point)

    def reset_rubber_band(self):
        """ Reset the rubberband """
        self.rubberband.reset(QgsWkbTypes.LineGeometry)

    def reset_rubber_band2(self):
        """ Reset the rubberband """
        self.rubberband2.reset(QgsWkbTypes.LineGeometry)

    # ************************************* Mouse listener actions ***********************************************

    def moved(self, position):
        """
        Draw the polyline on the temp layer (rubberband)

        :param position: Mouse position
        :type position: dict
        """

        if self.selection_method == "temp_polyline":
            if len(self.point_list) > 0:
                # Get mouse coords
                map_pos = self.canvas.getCoordinateTransform().toMapCoordinates(position["x"], position["y"])

                # Draw on temp layer
                if self.left_clicked_bool:
                    # Only draw if there's a new point
                    self.left_clicked_bool = False
                    self.reset_rubber_band()

                    for point in self.point_list:
                        self.rubberband.addPoint(point)

                self.reset_rubber_band2()
                self.rubberband2.addPoint(self.point_list[-1])
                self.rubberband2.addPoint(QgsPointXY(map_pos.x(), map_pos.y()))
        else:
            return

    def right_clicked(self):
        """ Used to quit the current action """
        self.profile_tool.clear()

    def left_clicked(self, position):
        """
        Add point to analyse
        :param position: Mouse position
        :type position: dict
        """

        map_pos = self.canvas.getCoordinateTransform().toMapCoordinates(position["x"], position["y"])
        new_points = [[map_pos.x(), map_pos.y()]]
        self.left_clicked_bool = True
        mouse_pos = position

        # if self.profiletool.doTracking :
        self.rubberbandpoint.hide()

        if self.selection_method == "temp_polyline":
            if new_points == self.dblclktemp:
                self.dblclktemp = None
            else:
                if len(self.point_list) == 0:
                    self.reset_rubber_band()
                    self.rubberbandbuf.reset()
                    self.clear()
                    self.profile_tool.reset_graph()

                position = QgsPointXY(map_pos.x(), map_pos.y())
                self.point_list.append(position)
                self.moved(mouse_pos)
                self.profile_tool.add_point_and_update_profile(position)
        else:
            return

    def set_buffer_geometry(self, geoms):
        """
        Set the buffer geometry
        :param geoms: Buffer geometry
        :type geoms: list
        """
        self.rubberbandbuf.reset()
        for geom in geoms:
            self.rubberbandbuf.addGeometry(geom, None)

    def clear(self):
        """ Used on right click """
        self.point_list = []
        self.rubberbandpoint.hide()
        self.reset_rubber_band()
        self.reset_rubber_band2()
        self.rubberbandbuf.reset()

    def connect_tool(self):
        """ Connect the mouse listener to the actions """
        if self.canvas.mapTool() != self.tool:
            self.canvas.setMapTool(self.tool)
            self.tool.moved.connect(self.moved)
            self.tool.right_clicked.connect(self.right_clicked)
            self.tool.left_clicked.connect(self.left_clicked)
            self.tool.desactivate.connect(self.deactivate)

    def deactivate(self):
        """ Enable clean exit of the plugin """
        self.clear()

        try:
            self.tool.moved.disconnect(self.moved)
            self.tool.right_clicked.disconnect(self.right_clicked)
            self.tool.left_clicked.disconnect(self.left_clicked)
            self.tool.desactivate.disconnect(self.deactivate)

            self.canvas.unsetMapTool(self.tool)
            self.canvas.setMapTool(self.tool_pan)

            if self.profile_tool.draw_path_radioButton.isChecked():
                self.profile_tool.buttonGroup.buttonToggled.disconnect(self.profile_tool.selection_type_changed)

                self.profile_tool.buttonGroup.setExclusive(False)
                self.profile_tool.draw_path_radioButton.setChecked(False)
                self.profile_tool.buttonGroup.setExclusive(True)

                self.profile_tool.buttonGroup.buttonToggled.connect(self.profile_tool.selection_type_changed)

        except TypeError:
            pass
        except RuntimeError:
            pass
