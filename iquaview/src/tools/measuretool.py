# -*- coding: utf-8 -*-
# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
Measure Tools for IQUAview. This tool allows to measure distances, angles and areas.
"""
from math import degrees, sqrt, atan2, sin, cos

from PyQt5.QtCore import pyqtSignal, Qt
from PyQt5.QtGui import QColor
from qgis.core import (QgsDistanceArea,
                       QgsWkbTypes,
                       QgsProject,
                       QgsGeometry,
                       QgsCircularString,
                       QgsPoint,
                       QgsUnitTypes)
from qgis.gui import QgsMapTool, QgsRubberBand


class MeasureDistanceTool(QgsMapTool):
    """
    Map tool for measuring a distance in the map canvas.
    """
    finished = pyqtSignal()

    def __init__(self, canvas, msglog):
        super().__init__(canvas)
        self.canvas = canvas
        self.msglog = msglog
        self.start_point = self.end_point = None
        self.rubber_band = QgsRubberBand(self.canvas, QgsWkbTypes.LineGeometry)
        self.rubber_band.setColor(QColor(255, 0, 0, 100))
        self.rubber_band.setWidth(3)
        self.rubber_band_points = QgsRubberBand(self.canvas, QgsWkbTypes.PointGeometry)
        self.rubber_band_points.setIcon(QgsRubberBand.ICON_CIRCLE)
        self.rubber_band_points.setIconSize(10)
        self.rubber_band_points.setColor(QColor(255, 0, 0, 150))
        self.rubber_band_points.setColor(QColor(255, 0, 0, 150))
        self.distance_calc = QgsDistanceArea()
        self.proj_changed()
        self.reset()

        self.canvas.destinationCrsChanged.connect(self.proj_changed)

    def proj_changed(self):
        """
        Projection changed
        """
        crs = self.canvas.mapSettings().destinationCrs()
        ellipsoid = QgsProject.instance().ellipsoid()
        if self.distance_calc.sourceCrs() != crs:
            self.distance_calc.setSourceCrs(crs, QgsProject.instance().transformContext())
        if self.distance_calc.ellipsoid() != ellipsoid:
            self.distance_calc.setEllipsoid(ellipsoid)

    def reset(self):
        self.msglog.logMessage("")
        self.start_point = self.end_point = None
        self.rubber_band.reset(QgsWkbTypes.LineGeometry)
        self.rubber_band_points.reset(QgsWkbTypes.PointGeometry)

    def canvasPressEvent(self, event):
        pass

    def canvasReleaseEvent(self, event):
        if self.start_point:
            self.end_point = event.snapPoint()
            self.rubber_band.addPoint(self.end_point)
            self.rubber_band_points.addPoint(self.end_point)
            self.proj_changed()
            self.finish()

        else:
            self.reset()
            self.start_point = event.snapPoint()
            self.rubber_band.addPoint(self.start_point)
            self.rubber_band_points.addPoint(self.start_point)

    def canvasMoveEvent(self, e):
        if self.start_point and not self.end_point:
            point = e.snapPoint()
            self.rubber_band.movePoint(point)
            self.proj_changed()
            distance = self.distance_calc.measureLine(self.start_point, point)
            bearing = self.distance_calc.bearing(self.start_point, point)
            text = self.distance_calc.formatDistance(distance, 3, QgsUnitTypes.DistanceMeters, False)
            self.msglog.logMessage("")
            self.msglog.logMessage(f"Current distance: {text}. Bearing: {degrees(bearing):.3F} °",
                                   "Measure distance:", 0)

    def keyPressEvent(self, event):
        """
        When escape key is pressed, line is restarted
        """
        if event.key() == Qt.Key_Escape:
            self.reset()

    def finish(self):
        self.start_point = None
        self.finished.emit()


class MeasureAngleTool(QgsMapTool):
    finished = pyqtSignal()

    def __init__(self, canvas, msglog):
        super().__init__(canvas)
        self.canvas = canvas
        self.msglog = msglog
        self.start_point = self.middle_point = self.end_point = None
        self.rubber_band = QgsRubberBand(self.canvas, QgsWkbTypes.LineGeometry)
        self.rubber_band.setColor(QColor(255, 0, 0, 100))
        self.rubber_band.setWidth(3)
        self.rubber_band_points = QgsRubberBand(self.canvas, QgsWkbTypes.PointGeometry)
        self.rubber_band_points.setIcon(QgsRubberBand.ICON_CIRCLE)
        self.rubber_band_points.setIconSize(10)
        self.rubber_band_points.setColor(QColor(255, 0, 0, 150))
        self.rubber_band_curve = QgsRubberBand(self.canvas)
        self.rubber_band_curve.setWidth(2)
        self.rubber_band_curve.setColor(QColor(255, 153, 0, 100))

        crs = self.canvas.mapSettings().destinationCrs()
        self.distance_calc = QgsDistanceArea()
        self.proj_changed()
        self.reset()

        self.canvas.destinationCrsChanged.connect(self.proj_changed)

    def proj_changed(self):
        """
        Projection changed
        """
        crs = self.canvas.mapSettings().destinationCrs()
        ellipsoid = QgsProject.instance().ellipsoid()
        if self.distance_calc.sourceCrs() != crs:
            self.distance_calc.setSourceCrs(crs, QgsProject.instance().transformContext())
        if self.distance_calc.ellipsoid() != ellipsoid:
            self.distance_calc.setEllipsoid(ellipsoid)

    def reset(self):
        self.msglog.logMessage("")
        self.start_point = self.middle_point = self.end_point = None
        self.rubber_band.reset(QgsWkbTypes.LineGeometry)
        self.rubber_band_points.reset(QgsWkbTypes.PointGeometry)
        self.rubber_band_curve.reset()

    def canvasPressEvent(self, event):
        pass

    def canvasReleaseEvent(self, event):
        if self.start_point and self.middle_point:
            self.proj_changed()
            self.end_point = event.snapPoint()
            self.rubber_band.addPoint(self.end_point)
            self.rubber_band_points.addPoint(self.end_point)
            self.finish()

        elif self.start_point:
            self.middle_point = event.snapPoint()
            self.rubber_band.addPoint(self.middle_point)
            self.rubber_band_points.addPoint(self.middle_point)

        else:
            self.reset()
            self.start_point = event.snapPoint()
            self.rubber_band.addPoint(self.start_point)
            self.rubber_band_points.addPoint(self.start_point)

    def canvasMoveEvent(self, e):
        point = e.snapPoint()
        if self.start_point and not self.end_point:
            self.rubber_band.movePoint(point)

        if self.start_point and self.middle_point and not self.end_point:
            self.proj_changed()
            angle_start_to_middle = self.distance_calc.bearing(self.middle_point, self.start_point)
            angle_end_to_middle = self.distance_calc.bearing(self.middle_point, point)
            angle = degrees(angle_end_to_middle - angle_start_to_middle)

            if angle < -180:
                angle = 360 + angle
            elif angle > 180:
                angle = angle - 360

            self.msglog.logMessage("")
            self.msglog.logMessage(f"Current angle: {abs(angle):.3F} °", "Measure angle:", 0)

            self.rubber_band_curve.reset()

            # get the distance from center to point
            dist_mid_to_p = sqrt((point.x() - self.middle_point.x()) * (point.x() - self.middle_point.x()) +
                                 (point.y() - self.middle_point.y()) * (point.y() - self.middle_point.y()))
            dist_mid_to_start = sqrt(
                (self.start_point.x() - self.middle_point.x()) * (self.start_point.x() - self.middle_point.x()) +
                (self.start_point.y() - self.middle_point.y()) * (self.start_point.y() - self.middle_point.y()))

            # get angle
            angle_start = atan2(self.start_point.y() - self.middle_point.y(),
                                self.start_point.x() - self.middle_point.x())
            angle_p = atan2(point.y() - self.middle_point.y(), point.x() - self.middle_point.x())

            # smaller distance
            if dist_mid_to_p < dist_mid_to_start:
                dist = dist_mid_to_p
            else:
                dist = dist_mid_to_start

            y_p = dist * sin(angle_p)
            x_p = dist * cos(angle_p)
            y_start = dist * sin(angle_start)
            x_start = dist * cos(angle_start)

            circular_ring = QgsCircularString()
            circular_ring = circular_ring.fromTwoPointsAndCenter(
                QgsPoint(self.middle_point.x() + x_start / 2,
                         self.middle_point.y() + y_start / 2),
                QgsPoint(self.middle_point.x() + x_p / 2,
                         self.middle_point.y() + y_p / 2),
                QgsPoint(self.middle_point.x(),
                         self.middle_point.y()),
                True)

            circular_geometry = QgsGeometry(circular_ring)

            self.rubber_band_curve.addGeometry(circular_geometry, self.canvas.mapSettings().destinationCrs())

    def keyPressEvent(self, event):
        """
        When escape key is pressed, line is restarted
        """
        if event.key() == Qt.Key_Escape:
            self.reset()

    def finish(self):
        self.start_point = self.middle_point = None
        self.finished.emit()


class MeasureAreaTool(QgsMapTool):
    finished = pyqtSignal()

    def __init__(self, canvas, msglog):
        super().__init__(canvas)
        self.canvas = canvas
        self.msglog = msglog
        self.start_point = self.middle_point = self.end_point = None
        self.rubber_band = QgsRubberBand(self.canvas, QgsWkbTypes.PolygonGeometry)
        self.rubber_band.setColor(QColor(255, 0, 0, 100))
        self.rubber_band.setWidth(3)
        self.rubber_band_points = QgsRubberBand(self.canvas, QgsWkbTypes.PointGeometry)
        self.rubber_band_points.setIcon(QgsRubberBand.ICON_CIRCLE)
        self.rubber_band_points.setIconSize(10)
        self.rubber_band_points.setColor(QColor(255, 0, 0, 150))

        crs = self.canvas.mapSettings().destinationCrs()
        self.area_calc = QgsDistanceArea()
        self.proj_changed()
        self.reset()

        self.canvas.destinationCrsChanged.connect(self.proj_changed)

    def proj_changed(self):
        """
        Projection changed
        """
        crs = self.canvas.mapSettings().destinationCrs()
        ellipsoid = QgsProject.instance().ellipsoid()
        if self.area_calc.sourceCrs() != crs:
            self.area_calc.setSourceCrs(crs, QgsProject.instance().transformContext())
        if self.area_calc.ellipsoid() != ellipsoid:
            self.area_calc.setEllipsoid(ellipsoid)

    def reset(self):
        """ Reset log message and rubber band"""
        self.msglog.logMessage("")
        self.start_point = self.end_point = None
        self.rubber_band.reset(QgsWkbTypes.PolygonGeometry)
        self.rubber_band_points.reset(QgsWkbTypes.PointGeometry)

    def canvasPressEvent(self, event):
        pass

    def canvasReleaseEvent(self, event):

        if self.start_point and event.button() == Qt.RightButton:
            self.proj_changed()
            self.end_point = event.snapPoint()
            self.rubber_band.addPoint(self.end_point)
            self.rubber_band_points.addPoint(self.end_point)
            self.finish()

        elif self.start_point:
            self.rubber_band.addPoint(event.snapPoint())
            self.rubber_band_points.addPoint(event.snapPoint())

        else:
            self.reset()
            self.start_point = event.snapPoint()
            self.rubber_band.addPoint(self.start_point)
            self.rubber_band_points.addPoint(self.start_point)

    def canvasMoveEvent(self, e):
        if self.start_point and not self.end_point:
            self.rubber_band.movePoint(e.snapPoint())

            self.proj_changed()
            multipoint = self.rubber_band.asGeometry()
            area = self.area_calc.measureArea(multipoint)
            self.msglog.logMessage("")
            self.msglog.logMessage("Current area: {} ".format(self.area_calc.formatArea(area,
                                                                                        3,
                                                                                        QgsUnitTypes.AreaSquareMeters,
                                                                                        False)), "Measure Area:", 0)

    def keyPressEvent(self, event):
        """
        When escape key is pressed, line is restarted
        """
        if event.key() == Qt.Key_Escape:
            self.reset()

    def finish(self):
        self.start_point = self.end_point = None
        self.finished.emit()
