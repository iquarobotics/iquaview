# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
    This is the main file of the web server. It starts the web server and handles the requests.
"""

import logging
import os
import sys
from distutils.version import LooseVersion
from shutil import copytree, rmtree
from threading import Event
from typing import Dict

import bs4
from PyQt5.QtCore import QThreadPool, pyqtSignal, Qt, QPoint
from PyQt5.QtWidgets import QWidget, QListWidgetItem, QMenu
from flask import Flask, render_template
from flask_socketio import SocketIO

from iquaview.src.mission.missioncontroller import MissionController
from iquaview.src.mission.missiontrack import MissionTrack
from iquaview.src.ui.ui_web_server import Ui_WebServerWidget
from iquaview_lib.baseclasses.webserverbase import DataSource, RegisterResponse, WebServerRegister, WebServerPoint
from iquaview_lib.cola2api.basic_message import BasicMessage
from iquaview_lib.cola2api.gps_driver import GPSPosition, GPSOrientation
from iquaview_lib.config import Config
from iquaview_lib.utils.switchbuttonwidget import SwitchButton
from iquaview_lib.utils.textvalidator import (validate_port,
                                              get_color,
                                              get_int_validator)
from iquaview_lib.utils.workerthread import Worker
from iquaview_lib.vehicle.vehicledata import VehicleData
from iquaview_lib.vehicle.vehicleinfo import VehicleInfo

if sys.version_info >= (3, 8):
    from importlib import metadata as importlib_metadata
else:
    import importlib_metadata

logger = logging.getLogger(__name__)
logging.getLogger('werkzeug').setLevel(logging.ERROR)
logging.getLogger('socketio').setLevel(logging.ERROR)
logging.getLogger('engineio').setLevel(logging.ERROR)

HOME_PATH = os.path.expanduser('~')
FILE_PATH = os.path.dirname(os.path.abspath(__file__))
WEB_SERVER_PATH = os.path.join(HOME_PATH, '.iquaview/default/web_server')
ORIGIN_STATIC_DIR = os.path.join(FILE_PATH, 'static')
ORIGIN_TEMP_DIR = os.path.join(FILE_PATH, 'templates')
DESTINATION_STATIC_DIR = os.path.join(WEB_SERVER_PATH, 'static')
DESTINATION_TEMP_DIR = os.path.join(WEB_SERVER_PATH, 'templates')


class WebServer(QWidget, Ui_WebServerWidget):
    """
        This class implements the web server.
    """
    stop_server_signal = pyqtSignal()

    global app
    app = Flask(__name__, template_folder=DESTINATION_TEMP_DIR, static_folder=DESTINATION_STATIC_DIR)

    def __init__(self, config: Config,
                 vehicle_data: VehicleData,
                 vehicle_info: VehicleInfo,
                 mission_ctrl: MissionController):
        """
        Constructor
        :param config: configuration
        :type config: Config
        :param vehicle_data: data from the vehicle
        :type vehicle_data: VehicleData
        :param vehicle_info: vehicle info to extract the ned position from
        :type vehicle_info: VehicleInfo
        :param mission_ctrl: mission controller to get the missions from
        :type mission_ctrl: MissionController
        """
        super().__init__()
        self.setupUi(self)
        # registration of data sources
        self.data_sources: Dict[str, DataSource] = {}
        # Thread
        self.thread = None
        self.thread_stop_event = Event()
        self.threadpool = QThreadPool()
        self.switch_button = SwitchButton(self, "On", 10, "Off", 25, 60)
        self.state_hboxlayout.addWidget(self.switch_button)

        self.port_text.setValidator(get_int_validator(0, 65535))
        self.port_text.textChanged.connect(self.port_validator)

        self.config = config
        self.vehicle_data = vehicle_data
        self.vehicle_info = vehicle_info
        self.mission_ctrl = mission_ctrl
        self.usbl_data = BasicMessage()
        self.auv_data = BasicMessage()
        self.gps_position = GPSPosition(time=0.0, latitude=0.0, longitude=0.0, altitude=0.0, quality=-1)
        self.gps_orientation = GPSOrientation(time=0.0, orientation_deg=0.0)

        if os.path.isdir(DESTINATION_TEMP_DIR):
            rmtree(DESTINATION_TEMP_DIR)
        if os.path.isdir(DESTINATION_STATIC_DIR):
            rmtree(DESTINATION_STATIC_DIR)

        copytree(ORIGIN_TEMP_DIR, DESTINATION_TEMP_DIR)
        copytree(ORIGIN_STATIC_DIR, DESTINATION_STATIC_DIR)

        with open(f"{ORIGIN_TEMP_DIR}/index.html", encoding='utf8') as index:

            html = index.read()
            soup = bs4.BeautifulSoup(html, "lxml")
            for link in soup.find_all('script'):
                if link.get('socket') is not None:
                    if LooseVersion(importlib_metadata.version("flask_socketio")) < LooseVersion("5.0.0"):
                        link['src'] = "static/js/lib/socket_io/1_3_6/socket.io.min.js"
                        link['socket'] = "1.3.6"
                    else:
                        link['src'] = "static/js/lib/socket_io/4_7_4/socket.io.min.js"
                        link['socket'] = "4.7.4"
        with open(f"{DESTINATION_TEMP_DIR}/index.html", 'w', encoding='utf8') as index:
            index.write(str(soup))

        self.select_menu = QMenu("Select")

        self.all_action = self.select_menu.addAction("All")
        self.none_action = self.select_menu.addAction("None")

        self.mission_ctrl.stop_mission_editing.connect(self.reload_missions)
        self.mission_ctrl.mission_added.connect(self.reload_missions)
        self.mission_ctrl.finish_mission_action_signal.connect(self.reload_missions)
        self.mission_ctrl.mission_removed_signal.connect(self.reload_missions)
        self.select_checkBox.clicked.connect(self.select_missions)
        self.missions_listWidget.itemChanged.connect(self.update_check_box)
        self.select_toolButton.clicked.connect(self.show_select_menu)

        self.all_action.triggered.connect(self.select_all)
        self.none_action.triggered.connect(self.unselect_all)

        self.socketio = SocketIO(app, async_mode=None, logger=False, engineio_logger=False)
        self.socketio.on_event('connect', self.connect, namespace='/viewer')
        self.socketio.on_event('disconnect', self.disconnect_client, namespace='/viewer')
        self.stop_server_signal.connect(self.stop_server)

        self.reload()
        self.on_accept()

        if self.switch_button.is_on():
            self.start_server_thread()

    def register_data_source(self, data: WebServerRegister, override: bool = False) -> RegisterResponse:
        """
        Register a data source.
        :param data: The data source to register.
        :type data: DataSource
        :param override: If true, the data source will be registered even if it is already registered.
        :type override: bool
        :return: The response of the registration.
        :rtype: RegisterResponse
        """
        # already registered (i.e. when wifi and usbl both publish vehicle)
        if data is None:
            return RegisterResponse.NotRegisteredGotNone
        ans = RegisterResponse.NewRegistration
        if data.name in self.data_sources:
            if not override:
                return RegisterResponse.AlreadyRegistered
            ans = RegisterResponse.OverwrittenRegistration

        # register
        icon_path = self.generate_icon(data)
        icon_detail_path = icon_path
        icon_detail_size = (0.0, 0.0)
        if data.icon_detail_name is not None and data.icon_detail_size is not None:
            icon_detail_path = f"static/images/{data.icon_detail_name}.svg"
            icon_detail_size = data.icon_detail_size

        # preserve last position if exists to be sent to new clients
        last_position = None
        if data.name in self.data_sources:
            last_position = self.data_sources[data.name]['last_position']

        # finish registration
        self.data_sources[data.name] = {
            'register_data': data,
            'icon': (icon_path, (20.0, 20.0)),  # TODO: offsets to center?
            'icon_detail': (icon_detail_path, icon_detail_size),  # TODO: offsets to center?
            'color': data.hex_color,  # hex color
            'last_position': last_position,
        }

        return ans

    def callback_data(self, data: WebServerPoint) -> None:
        """
        Callback for data.
        :param data: The data to callback.
        :type data: WebServerPoint
        """
        # check registered
        if data.name not in self.data_sources:
            logger.error(f"{data.name} not registered in web server")
            return

        # not connected
        if not data.connected:
            self.socketio.emit('no_signal', {'name': data.name}, namespace='/viewer')
        else:
            # save last position
            self.data_sources[data.name]['last_position'] = data
            # connected
            position = data.to_dict()
            position['color'] = self.data_sources[data.name]['color']
            position['icon'] = self.data_sources[data.name]['icon']
            position['icon_detail'] = self.data_sources[data.name]['icon_detail']
            self.socketio.emit('new_position', position, namespace='/viewer')

    @staticmethod
    def generate_icon(data: WebServerRegister) -> str:
        """
        Generate the icon for the data source.
        :param data: The data source.
        :type data: DataSource
        :return: The path to the icon.
        :rtype: str
        """
        # filename
        hexcolor = data.hex_color
        filename = f"{DESTINATION_STATIC_DIR}/images/icon_{hexcolor[1:]}.svg"
        filename_rel = f"static/images/icon_{hexcolor[1:]}.svg"
        if data.has_orientation:
            filename = f"{DESTINATION_STATIC_DIR}/images/icon_orientation_{hexcolor[1:]}.svg"
            filename_rel = f"static/images/icon_orientation_{hexcolor[1:]}.svg"

        # exists
        if os.path.isfile(filename):
            return filename_rel
        # create it
        with open(filename, "w") as fout:
            template = f"{DESTINATION_STATIC_DIR}/images/template_icon.svg"
            if data.has_orientation:
                template = f"{DESTINATION_STATIC_DIR}/images/template_icon_orientation.svg"
            with open(template) as fin:
                lines = fin.readlines()
                for i, line in enumerate(lines):
                    if "red" in line:
                        lines[i] = lines[i].replace("red", hexcolor)
                fout.writelines(lines)
            return filename_rel

    def reload(self):
        """ Reload the parameters: Switch button and port. """
        self.switch_button.set_state(self.config.settings.get('web_server_state'))
        self.port_text.setText(str(self.config.settings.get('web_server_port')))
        self.reload_missions()

    def reload_missions(self):
        """ Reload the missions. """
        missions = []
        mission_list = self.mission_ctrl.get_mission_list()
        for i in range(self.missions_listWidget.count()):
            item = self.missions_listWidget.item(i)
            name = item.text()
            j = 0
            while j < len(mission_list):
                if name == mission_list[j].get_mission_name():
                    state = self.missions_listWidget.item(i).checkState()
                    missions.append([name, state])
                    break
                j += 1

        self.missions_listWidget.clear()
        self.clear_missions()
        for mission in mission_list:
            mission_item = QListWidgetItem()
            mission_item.setFlags(mission_item.flags() | Qt.ItemIsUserCheckable)
            mission_item.setText(mission.get_mission_name())
            mission_item.setData(Qt.UserRole, mission)
            mission_item.setCheckState(True)
            mission_item.setData(Qt.CheckStateRole, Qt.Checked)
            for j in range(0, len(missions)):
                if mission.get_mission_name() == missions[j][0]:
                    if missions[j][1] == 0:
                        mission_item.setData(Qt.CheckStateRole, Qt.Unchecked)
                    else:
                        mission_item.setData(Qt.CheckStateRole, Qt.Checked)
            self.missions_listWidget.addItem(mission_item)

            if mission_item.data(Qt.CheckStateRole) != 0:
                self.add_mission(mission)

        self.update_check_box()

    def update_check_box(self):
        """ Update check box according to the missions in the list"""
        state = None
        for row in range(self.missions_listWidget.count()):
            mission_item = self.missions_listWidget.item(row)
            check_state_role = mission_item.data(Qt.CheckStateRole)
            if state is None and check_state_role == Qt.Checked:
                state = Qt.Checked
            elif state is None and check_state_role == Qt.Unchecked:
                state = Qt.Unchecked
            elif state is not None and check_state_role != state:
                state = Qt.PartiallyChecked

        if state is not None:
            self.select_checkBox.setCheckState(state)
            self.update_checkbox()

    def update_checkbox(self) -> bool:
        """ Update checkbox tooltip and return next state"""
        if self.select_checkBox.checkState() == Qt.Checked:
            next_state = Qt.Checked
            self.select_checkBox.setToolTip("Unselect all")
        else:
            next_state = Qt.Unchecked
            self.select_checkBox.setToolTip("Select all")

        return next_state

    def select_all(self):
        """ Select all missions"""
        self.select_checkBox.setChecked(Qt.Checked)
        self.select_missions()

    def unselect_all(self):
        """Unselect all missions"""
        self.select_checkBox.setChecked(Qt.Unchecked)
        self.select_missions()

    def show_select_menu(self):
        """ Show select menu"""
        self.select_menu.popup(self.select_toolButton.mapToGlobal(QPoint(0, 0)))

    def select_missions(self):
        """ Select or deselect all the missions in the list according to the checkbox """
        if self.select_checkBox.checkState() == Qt.PartiallyChecked:
            self.select_checkBox.setCheckState(Qt.Checked)

        next_state = self.update_checkbox()

        for row in range(self.missions_listWidget.count()):
            mission_item = self.missions_listWidget.item(row)
            mission_item.setData(Qt.CheckStateRole, next_state)

    def on_accept(self):
        """ Save the settings. Start the web server if it is not running and switch is on. """
        if not self.switch_button.is_on():
            self.stop_server_signal.emit()

        elif self.switch_button.is_on() and self.config.settings['web_server_state'] is True:
            pass

        elif self.switch_button.is_on():
            self.start_server_thread()

        self.reload_missions()

        self.config.settings['web_server_state'] = self.switch_button.is_on()
        self.config.settings['web_server_port'] = int(self.port_text.text())
        self.config.save()

    def port_validator(self):
        """ Validate text(port) of sender"""
        sender = self.sender()
        state = validate_port(sender.text())
        color = get_color(state)
        sender.setStyleSheet(f'QLineEdit {{ background-color: {color} }}')

    def start_server_thread(self):
        """ Start server thread"""
        worker = Worker(self.run_server)  # Any other args, kwargs are passed to the run function
        self.threadpool.start(worker)

    def run_server(self):
        """ Run server in thread """
        self.socketio.run(app, host="0.0.0.0", port=int(self.port_text.text()))

    def stop_server(self):
        try:
            # gevent stop socketio run
            if self.socketio.wsgi_server is not None and self.socketio.server.eio.async_mode == 'gevent':
                self.socketio.stop()
            elif self.socketio.server.eio.async_mode == 'threading' or self.socketio.server.eio.async_mode == 'eventlet':
                self.socketio.stop()
        except Exception as e:
            logger.error(f"Error stopping socketio {e}")

        try:
            self.threadpool.clear()
        except Exception as e:
            logger.error(f"Error stopping threadpool {e}")

    def position_generator(self):
        """
        emit to a socketio instance (broadcast)
        Ideally to be run in a separate thread?
        """
        while not self.thread_stop_event.isSet():
            self.socketio.sleep(1)

    def add_mission(self, mission_track: MissionTrack):
        """ Add a mission to the list of missions """
        positions = []
        for step in mission_track.get_mission().mission:
            positions.append([step.get_maneuver().final_latitude, step.get_maneuver().final_longitude])
        self.socketio.emit('new_mission', {'positions': positions}, namespace='/viewer')

    def clear_missions(self):
        """ Clear all missions from the viewer """
        self.socketio.emit('clear_missions', {}, namespace='/viewer')

    @staticmethod
    @app.route('/')
    def index():
        """ Route for index.html """
        # only by sending this page first will the client be connected to the socketio instance
        return render_template('index.html')

    # @socketio.on('connect', namespace='/viewer')
    def connect(self):
        """ Connect new client to the socketio instance """
        # need visibility of the global thread object
        logger.info('Client connected')

        # Start the position generator thread only if the thread has not been started before.
        if self.thread is None:
            self.thread = self.socketio.start_background_task(self.position_generator)

        self.reload_missions()

        # reload last positions with disconnected signal for each registered publisher
        for name in self.data_sources:
            register_data = self.data_sources[name]
            if register_data['last_position'] is not None:
                # last position
                last_position = register_data['last_position']
                position = last_position.to_dict()
                position['color'] = self.data_sources[last_position.name]['color']
                position['icon'] = self.data_sources[last_position.name]['icon']
                position['icon_detail'] = self.data_sources[last_position.name]['icon_detail']
                self.socketio.emit('new_position', position, namespace='/viewer')
                # signal lost
                self.socketio.emit('no_signal', {'name': last_position.name}, namespace='/viewer')

    # @socketio.on('disconnect', namespace='/viewer')
    def disconnect_client(self):
        """ Disconnect client """
        logger.info('Client disconnected')
