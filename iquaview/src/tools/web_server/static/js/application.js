function getColor(d) {
    return d === 'Vehicle' ? '#ffff00' :
        d === 'Vessel' ? '#008000' :
        d === 'USBL' ? '#ff0000' :
        '#ff7f00';
}

function style(feature) {
    return {
        weight: 1.5,
        opacity: 1,
        fillOpacity: 1,
        radius: 6,
        fillColor: getColor(feature.properties.TypeOfIssue),
        color: "grey"

    };
}
$(document).ready(function() {

    let labels_template = ['<strong>Positions</strong>'];

    var map = L.map('map').setView([41.77, 3.033], 14);

    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>',
        maxNativeZoom: 19,
        maxZoom: 24
    }).addTo(map);

    L.control.scale().addTo(map);

    var div_pos = L.DomUtil.create('div', 'info legend_pos');

    var legend_pos = L.control({
        position: 'bottomleft'
    });

    var missions_pathlines = [];
    var missions_markers = [];

    //connect to the socket server.
    var socket = io.connect('http://' + document.domain + ':' + location.port + '/viewer');
    var numbers_received = [];

    // dynamic markers and labels
    let markers = {};
    let labels = {};
    let trace = {};
    let trace_polyline = {};
    const trace_n = 60;

    // generic new position
    socket.on('new_position', function(msg) {
        // point data
        let name = msg.name;
        let latitude = msg.latitude;
        let longitude = msg.longitude;
        let orientation = msg.orientation;
        let msg_labels = msg.labels;
        let x_default_icon_size = 20;
        let y_default_icon_size = 20;
        let connection_lost_path = 'static/images/connectionLost.svg'
        if (orientation) {
            y_default_icon_size = 28;
        }
        // icons
        let color = msg.color;
        let [icon, icon_size] = msg.icon;
        let [icon_detail, icon_detail_size] = msg.icon_detail;
        // new markers for position and disconnected
        if (!(name in markers)) {
            // new markers
            markers[name] = {
                position: L.marker([0, 0], {
                    rotationAngle: 0
                }).addTo(map),
                disconnected: L.marker([0, 0], {
                    rotationAngle: 0
                }).addTo(map),
                icon: icon,
                icon_detail: icon_detail,
                icon_detail_size: icon_detail_size,
                x_default_icon_size: x_default_icon_size,
                y_default_icon_size: y_default_icon_size,
                lock_to: false,
            };

            // update legend of positions
            let disconnected_icon = resizeIcon(markers[name].disconnected, icon_detail_size[0], icon_detail_size[1], x_default_icon_size, y_default_icon_size, connection_lost_path, connection_lost_path);
            markers[name].disconnected.setIcon(disconnected_icon);
            legend_pos.onAdd = function(map) {
                const button_goto = '<i id="' + name + '" class="dot" style="background:' + color + '"></i>';
                const label_name = name;
                const button_lockto = '<i id="' + name + '_lockto" class="lock_open"></i>';
                div_pos.innerHTML +=
                    labels_template.push(button_goto + label_name + button_lockto);
                div_pos.innerHTML = labels_template.join('<br>');
                return div_pos;
            };
            legend_pos.addTo(map);

            // redo all onclick events for legend entries because innerHTML was rewritten
            for (const [key, value] of Object.entries(markers)) {
                // goto button
                let dot_category = document.getElementById(key);
                dot_category.style.cursor = "pointer";
                dot_category.onclick = function() {
                    latlng = markers[key].position.getLatLng();
                    map.setView(latlng, map.getZoom());
                };
                // lockto button
                const lockname = key + '_lockto';
                let lock_category = document.getElementById(lockname);
                lock_category.style.cursor = "pointer";
                lock_category.onclick = function() {
                    markers[key].lock_to = !markers[key].lock_to;
                    if (markers[key].lock_to) {
                        lock_category.className = "lock_close";
                    } else {
                        lock_category.className = "lock_open";
                    }
                };
            }
        }

        // draw
        let latlon = new L.LatLng(latitude, longitude);
        markers[name].position.setLatLng(latlon);
        let position_icon = resizeIcon(
            markers[name].position, icon_detail_size[0], icon_detail_size[1], x_default_icon_size, y_default_icon_size, icon_detail, icon
        );
        markers[name].position.setIcon(position_icon);
        if (orientation) {
            markers[name].position.setRotationOrigin('center center');
            markers[name].position.setRotationAngle(orientation);
        }
        // hide disconnected
        markers[name].disconnected.setOpacity(0);
        markers[name].disconnected.setLatLng(latlon);

        if (trace[name] === undefined) {
            trace[name] = [];
        }
        if (trace[name].length >= trace_n) {
            let removed = trace[name].shift();
            removed.removeFrom(map);
        }
        for (let i = 0; i < trace[name].length; i++) {
            // update opacity according to old/new
            trace[name][i].setStyle({
                fillOpacity: i / trace[name].length
            });

        }
        let circle = L.circleMarker(latlon, {
            color: color,
            fillColor: color,
            stroke: false,
            fillOpacity: 1,
            radius: 5
        }).addTo(map);
        trace[name].push(circle);

        // labels
        if (msg_labels != null) {
            // new labels
            let legend_name = 'legend_' + name;
            if (!(legend_name in labels)) {
                labels[legend_name] = {};
                let legend = L.control({
                    position: 'bottomright'
                });
                legend.onAdd = function(map) {
                    let div = L.DomUtil.create('div', 'info ' + legend_name);
                    legend_label = ['<strong>' + name + '</strong>'];
                    for (let i = 0; i < msg_labels.length; i++) {
                        let legend_label_id = 'legend_' + name + '_' + msg_labels[i][0];
                        legend_label.push(
                            '<p style="font-size:160%; margin-top: 5px; margin-bottom: 5px; id="' +
                            legend_label_id + '" class="">' +
                            msg_labels[i][0] + ':&nbsp' +
                            '<span style="float:right" id="' + legend_label_id + '_value" class="">' +
                            msg_labels[i][1] + '</span>' +
                            '</p>');
                    }
                    div.innerHTML = legend_label.join('');
                    return div;
                };
                legend.addTo(map);
                console.log("register label: " + legend_name);
            } else {
                // just update values
                for (let i = 0; i < msg_labels.length; i++) {
                    let legend_value = msg_labels[i][1];
                    let legend_value_id = 'legend_' + name + '_' + msg_labels[i][0] + '_value';
                    document.getElementById(legend_value_id).innerHTML = legend_value;
                }
            }
        }

        // follow the locked objects
        let bounds = [];
        for (const [key, value] of Object.entries(markers)) {
            if (markers[key].lock_to) {
                bounds.push(markers[key].position.getLatLng());
            }
        }
        if (bounds.length > 0) {
            const zoom_current = map.getZoom();
            const zoom_necessary = map.getBoundsZoom(bounds, false, L.point(30, 30));
            let zoom = zoom_current;
            if (zoom_necessary < zoom_current) {
                zoom = zoom_necessary;
            }
            map.flyToBounds(bounds, {
                maxZoom: zoom
            });
        }
    });

    // generic disconnected
    socket.on('no_signal', function(msg) {
        let name = msg.name;
        if (!(name in markers)) {
            console.log("marker for disconnected ", name, " does not exist");
            return;
        }
        markers[name].disconnected.setOpacity(100);
    });

    socket.on('clear_missions', function(msg) {
        for (var i = 0; i < missions_markers.length; i++) {
            missions_markers[missions_markers.length - 1 - i].remove(map)
        };
        for (var i = 0; i < missions_pathlines.length; i++) {
            missions_pathlines[missions_pathlines.length - 1 - i].remove(map)
        };
        missions_markers = [];
        missions_pathlines = [];
    });

    socket.on('new_mission', function(msg) {

        var mission_polyline = [];
        var mission_marker_list = [];
        for (var i = 0; i < msg.positions.length; i++) {
            position = msg.positions[i];
            mission_marker = new L.CircleMarker(position, {
                color: '#000000',
                opacity: '1.0',
                weight: '1',
                fillColor: '#ff0000',
                fillOpacity: '1'
            }).addTo(map);
            mission_marker.setRadius(5)
            mission_marker_list.push(mission_marker);
            mission_polyline.push(position);
            missions_markers.push(mission_marker);

        };
        var current_mission_pathline = L.polyline(mission_polyline, {
            color: '#ff0000'
        }).addTo(map);
        current_mission_pathline.bringToBack()
        missions_pathlines.push(current_mission_pathline);
    });

    map.on('zoomend', function() {

        for (const [key, value] of Object.entries(markers)) {
            let icon = resizeIcon(
                markers[key].position,
                markers[key].icon_detail_size[0],
                markers[key].icon_detail_size[1],
                markers[key].x_default_icon_size,
                markers[key].y_default_icon_size,
                markers[key].icon_detail,
                markers[key].icon
            );
            markers[key].position.setIcon(icon);
        }
    });


    function resizeIcon(marker, width, length, x_default_icon_size, y_default_icon_size, url_image, url_icon) {

        var url = url_image;
        var centerLatLng = marker.getLatLng();
        var pointC = map.latLngToContainerPoint(centerLatLng); // convert to containerpoint (pixels)
        var pointX = [pointC.x, pointC.y]; // add one pixel to x
        var pointY = [pointC.x, pointC.y]; // add one pixel to y

        var distanceX = 0;
        var distanceY = 0;
        let i = 1;
        var X_size = i * 2;
        var Y_size = i * 2;
        while (distanceX < width || distanceY < length) {
            if (distanceX < width) {
                var pointX = [pointC.x + i, pointC.y]; // add one pixel to x
                var X_size = i;
            }
            if (distanceY < length) {
                var pointY = [pointC.x, pointC.y + i]; // add one pixel to y
                var Y_size = i;
            }

            // convert containerpoints to latlng's
            var latLngC = map.containerPointToLatLng(pointC);
            var latLngX = map.containerPointToLatLng(pointX);
            var latLngY = map.containerPointToLatLng(pointY);

            var distanceX = latLngC.distanceTo(latLngX); // calculate distance between c and x (latitude)
            var distanceY = latLngC.distanceTo(latLngY); // calculate distance between c and y (longitude)
            i++;
        }

        if (width == 0 || length == 0 || (X_size < x_default_icon_size && Y_size < y_default_icon_size)) {
            X_size = x_default_icon_size;
            Y_size = y_default_icon_size;
            anchor_X = X_size / 2;
            if (y_default_icon_size > x_default_icon_size) {
                anchor_Y = 18;
            } else {
                anchor_Y = Y_size / 2;
            }
            url = url_icon;
        } else {
            anchor_X = X_size / 2;
            anchor_Y = Y_size / 2;
        }

        var newIcon = L.icon({
            iconUrl: url,
            iconSize: [X_size, Y_size], // size of the icon
            iconAnchor: [anchor_X, anchor_Y] // point of the icon which will correspond to marker's location
            //        popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
        });
        //        console.log(newIcon, width+ " "+ length +" "+ X_size + " " +Y_size + " " + anchor_X + " " + anchor_Y);
        return newIcon;
    }
});