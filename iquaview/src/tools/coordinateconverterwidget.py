# -*- coding: utf-8 -*-
# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Widget for converting lat lon coordinates between decimal degrees/degrees decimal minutes/degrees minutes seconds
"""
import logging
import sys

from PyQt5.QtCore import pyqtSignal, Qt
from PyQt5.QtWidgets import QDialog, QLineEdit, QSizePolicy, QApplication

from iquaview.src.ui.ui_coordinateconverterwidget import Ui_CoordinateConverterDialog
from iquaview_lib.config import Config
from iquaview_lib.utils.coordinateconverter import (CoordinateFormat,
                                                    format_lat_lon,
                                                    convert_latitude_to_decimal_degrees,
                                                    convert_longitude_to_decimal_degrees)

logger = logging.getLogger(__name__)

WHITE = ''
RED = '#f6989d'


class CoordinateConverterDialog(QDialog, Ui_CoordinateConverterDialog):
    """
    Widget for converting lat lon coordinates
    between decimal degrees/degrees decimal minutes/degrees minutes seconds
    """

    def __init__(self, config: Config, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.config = config
        self.setWindowTitle("Coordinate Converter")

        self.coordinate_converter_lineedit = CoordinateConverterLineEdit(self.config)

        size_policy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        size_policy.setHorizontalStretch(1)
        size_policy.setVerticalStretch(0)
        size_policy.setHeightForWidth(self.coordinate_converter_lineedit.sizePolicy().hasHeightForWidth())
        self.coordinate_converter_lineedit.setSizePolicy(size_policy)
        self.gridLayout.addWidget(self.coordinate_converter_lineedit, 0, 1)

        self.coordinate_converter_lineedit.converted_signal.connect(self.update_values)

    def update_values(self):
        """
        Update the values in the dialog
        """
        sender: CoordinateConverterLineEdit = self.sender()
        self.degree_latlon_label.setText(sender.get_decimal_degrees())
        self.degreeminute_latlon_label.setText(sender.get_degrees_minutes())
        self.degreeminutesecond_latlon_label.setText(sender.get_degrees_minutes_seconds())


class CoordinateConverterLineEdit(QLineEdit):
    """ LineEdit for converting lat lon coordinates """
    converted_signal = pyqtSignal()

    def __init__(self, config: Config):
        super().__init__()
        self.config = config
        self.setAlignment(Qt.AlignCenter)
        self.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        self.setPlaceholderText("latitude, longitude")
        self.setToolTip(
            """
            <html>
                <dl>
                    <dt><b>Decimal Degrees: DD.DDDD</b></dt>
                    <dd>41.40338°, 2.17403°</dd>
                    <dd>41.40338N, 2.17403E</dd>
                    
                    <dt><b>Degrees, Minutes: DDMM.MMMM</b></dt>
                    <dd>41° 24.2028', 122° 10.441'</dd>
                    <dd>4124.2028N, 12210.441E</dd>
                    
                    <dt><b>Degrees, Minutes, Seconds: DDMMSS.SSSS</b></dt>
                    <dd>41° 24' 12.2", 2° 10' 26.5"</dd>
                    <dd>412412.2N, 21026.5E</dd>
                </dl>
                Specify hemisphere with +, -, N, S, E, or W. If omitted, positive assumed (N for latitude, E for longitude).<br>
                <br>
                Degrees (°), minutes ('), and seconds (") symbols are optional.<br>
            </html>
    
            """
        )
        self.clear_coordinates()

        self.textEdited.connect(self.convert)

    def convert(self):
        """Convert current coordinate format to others coordinate formats.
                example:
                    DDD.DDDD°       -> DDD°MM.MM' and DDD°MM'SS.SSS''
                    DDD°MM.MM'      -> DDD.DDDD° and DDD°MM'SS.SSS''
                    DDD°MM'SS.SSS'' -> DDD.DDDD° and DDD°MM.MM'
                """
        color = WHITE

        try:
            lat_lon = self.text().upper()
            lat, lon = lat_lon.split(',')

            converted_lat = convert_latitude_to_decimal_degrees(lat)
            converted_lon = convert_longitude_to_decimal_degrees(lon)

            self.latitude = converted_lat
            self.longitude = converted_lon
            coordinate_precision = self.config.settings.get("coordinate_precision", 7)
            decimal_degree_precision = max(coordinate_precision, CoordinateFormat.MIN_DECIMAL_DEGREE_PRECISION)
            decimal_degree_minute_precision = max(coordinate_precision,
                                                  CoordinateFormat.MIN_DECIMAL_DEGREE_MINUTE_PRECISION)
            decimal_degree_minute_second_precision = max(coordinate_precision,
                                                         CoordinateFormat.MIN_DECIMAL_DEGREE_MINUTE_SECOND_PRECISION)
            coordinate_directional_suffix = self.config.settings.get("coordinate_directional_suffix", True)
            coordinate_symbols = self.config.settings.get("coordinate_symbols", True)
            [dd_lat, dd_lon] = format_lat_lon(converted_lat,
                                              converted_lon,
                                              CoordinateFormat.DEGREE,
                                              decimal_degree_precision,
                                              coordinate_directional_suffix,
                                              coordinate_symbols)
            [dm_lat, dm_lon] = format_lat_lon(converted_lat,
                                              converted_lon,
                                              CoordinateFormat.DEGREE_MINUTE,
                                              decimal_degree_minute_precision,
                                              coordinate_directional_suffix,
                                              coordinate_symbols)
            [dms_lat, dms_lon] = format_lat_lon(converted_lat,
                                                converted_lon,
                                                CoordinateFormat.DEGREE_MINUTE_SECOND,
                                                decimal_degree_minute_second_precision,
                                                coordinate_directional_suffix,
                                                coordinate_symbols)
            self.decimal_degrees = f"{dd_lat}, {dd_lon}"
            self.degrees_minutes = f"{dm_lat}, {dm_lon}"
            self.degrees_minutes_seconds = f"{dms_lat}, {dms_lon}"

        except Exception as e:
            logger.exception("Error converting coordinates: %s", e)
            self.clear_coordinates()
            if self.text() != '':
                color = RED

        self.setStyleSheet(f'QLineEdit {{ background-color: {color} }}')
        self.converted_signal.emit()

    def clear_coordinates(self):
        """ Clear coordinates display"""
        self.latitude = None
        self.longitude = None
        self.decimal_degrees = "000.000000°, 000.000000°"
        self.degrees_minutes = "000°00.0000', 000°00.0000'"
        self.degrees_minutes_seconds = "000°00'00.000'', 000°00'00.000''"

    def get_latitude(self):
        """ Return latitude in degrees """
        return self.latitude

    def get_longitude(self):
        """ Return longitude in degrees """
        return self.longitude

    def get_decimal_degrees(self):
        """ Return decimal degrees """
        return self.decimal_degrees

    def get_degrees_minutes(self):
        """ Return degrees minutes """
        return self.degrees_minutes

    def get_degrees_minutes_seconds(self):
        """ Return degrees minutes seconds """
        return self.degrees_minutes_seconds


if __name__ == "__main__":
    app = QApplication(sys.argv)
    coordinate_converter = CoordinateConverterDialog()
    coordinate_converter.show()
    sys.exit(app.exec_())
