# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
CustomizationSettingsWidget is the widget that allows to customize: ROS parameters, checklists and mission actions.
"""

import logging

from PyQt5.QtCore import pyqtSignal
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QWidget

from iquaview.src.mission.missionactionsconfigwidget import MissionActionsConfigWidget
from iquaview.src.ui.ui_customizationsettingswidget import Ui_CustomizationSettingsWidget
from iquaview.src.vehicle.checklist.checklistsconfigwidget import CheckListsWidget
from iquaview.src.vehicle.rosparamsconfigwidget import RosParamsConfigWidget

logger = logging.getLogger(__name__)


class CustomizationSettings(QWidget, Ui_CustomizationSettingsWidget):
    """
    Class to customize ros parameters, checklists and mission actions
    """
    refresh_signal = pyqtSignal()

    def __init__(self, config, vehicle_info):
        """
        Constructor
        :param config: iquaview configuration
        :type config: Config
        :param vehicle_info: Information about AUV
        :type vehicle_info:  VehicleInfo
        """
        super().__init__()
        self.setupUi(self)
        self.config = config
        self.vehicle_info = vehicle_info
        self.ros_params = RosParamsConfigWidget(config, self.vehicle_info)
        self.mission_actions = MissionActionsConfigWidget(config)
        self.check_lists = CheckListsWidget(config)
        self.rosparam_widget.layout().addWidget(self.ros_params)
        self.rosparam_widget.layout().addWidget(self.mission_actions)
        self.rosparam_widget.layout().addWidget(self.check_lists)

    @staticmethod
    def get_icon() -> QIcon:
        """
        Get icon
        :return: widget icon
        :rtype: QIcon
        """
        return QIcon(":/resources/customizationSettings.svg")

    def reload(self):
        """
        Reload widgets
        """
        self.ros_params.load_ros_params()
        self.mission_actions.load_mission_actions()
        self.check_lists.load_check_lists()

    def on_accept(self):
        """ Save settings"""
        self.ros_params.save()
        self.mission_actions.save()
        self.check_lists.save()

    def is_valid(self) -> bool:
        """
        Check if settings are valid
        :return: True if settings are valid
        :rtype: bool
        """
        return self.ros_params.is_valid() and self.mission_actions.is_valid() and self.check_lists.is_valid()
