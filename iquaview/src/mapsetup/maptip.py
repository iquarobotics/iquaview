# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
Class to show tooltips when the mouse pointer is over a layer without moving
"""

import logging

from PyQt5.QtCore import QTimer, QPoint
from PyQt5.QtWidgets import QToolTip
from qgis.core import QgsRectangle, QgsFeatureRequest, QgsFeature, QgsVectorLayer, QgsPointXY, QgsRasterLayer
from qgis.gui import QgsMapTool

from iquaview_lib.utils.qgisutils import transform_point

logger = logging.getLogger(__name__)


class MapTip:
    """
    Class to show tooltips when the mouse pointer is over a layer without moving
    """

    def __init__(self, canvas):
        """
        Init of the object MapTip. Creates a timer to show the tool tip message
        :param canvas: canvas where the map tip will show
        :type canvas: QgsMapCanvas
        """
        self.canvas = canvas

        # create timer for map tips
        self.map_tips_timer = QTimer(self.canvas)
        self.map_tips_timer.timeout.connect(self.show_map_tip)
        self.map_tips_timer.setInterval(850)
        self.map_tips_timer.setSingleShot(True)

        # every time the mouse moves over the canvas, reset map_tips_timer
        self.canvas.xyCoordinates.connect(self.reset_map_tip)

    def reset_map_tip(self):
        """ Resets map tip text and timer """
        if QToolTip.isVisible():
            QToolTip.hideText()
        if self.canvas.underMouse():
            self.map_tips_timer.start()

    def show_map_tip(self):
        """ Shows name of the layer the mouse is hovering over, if any. """
        if self.canvas.underMouse():
            pos = self.canvas.mouseLastXY()
            layer_name, value = self.get_name_of_layer_and_value_at(pos)
            if layer_name:
                text = f"{layer_name}: {value:.2f}m" if value else layer_name
                QToolTip.showText(self.canvas.mapToGlobal(pos), text)

    def get_name_of_layer_and_value_at(self, pos: QPoint) -> (str, float):
        """
        Given a canvas position, returns the name of the vector layer that is in that position
        :param pos: Position in canvas XY coordinates
        :type pos: QPoint
        :return: Name of the vector layer found in that position and the value at that position,
                 None if no layer is there.
        :rtype: (str, float)
        """
        layer_name = ""
        value = None
        layers = self.canvas.layers()
        canvas_rect = QgsRectangle(0, 0, self.canvas.width(), self.canvas.height())
        canvas_crs = self.canvas.mapSettings().destinationCrs()
        coord_transform = self.canvas.getCoordinateTransform()

        for layer in layers:
            try:
                if isinstance(layer, QgsVectorLayer):
                    # check if layer is in the extent of the canvas
                    layer_extent = layer.extent()
                    layer_crs = layer.sourceCrs()

                    top_left = transform_point(
                        QgsPointXY(layer_extent.xMinimum(), layer_extent.yMaximum()),
                        layer_crs,
                        canvas_crs
                    )
                    bot_right = transform_point(
                        QgsPointXY(layer_extent.xMaximum(), layer_extent.yMinimum()),
                        layer_crs,
                        canvas_crs
                    )
                    layer_extent = QgsRectangle(top_left, bot_right)

                    # Transform layer extent from map coordinates to canvas coordinates
                    top_left = coord_transform.transform(layer_extent.xMinimum(), layer_extent.yMaximum())
                    bot_right = coord_transform.transform(layer_extent.xMaximum(), layer_extent.yMinimum())
                    layer_rect = QgsRectangle(top_left.x(), top_left.y(), bot_right.x(), bot_right.y())

                    if canvas_rect.contains(layer_rect) or canvas_rect.intersects(layer_rect):
                        # At least a part of the layer is visible in canvas, check if pos is over the layer
                        # The check is made with a rectangle centered in pos, with a search radius.
                        radius = QgsMapTool.searchRadiusMU(self.canvas)  # search radius
                        p = coord_transform.toMapCoordinates(pos.x(), pos.y())
                        rect = QgsRectangle(p.x() - radius, p.y() - radius, p.x() + radius, p.y() + radius)

                        # Transform rect from canvas crs to layer crs
                        top_left = transform_point(
                            QgsPointXY(rect.xMinimum(), rect.yMaximum()),
                            canvas_crs,
                            layer_crs
                        )
                        bot_right = transform_point(
                            QgsPointXY(rect.xMaximum(), rect.yMinimum()),
                            canvas_crs,
                            layer_crs
                        )
                        rect = QgsRectangle(top_left, bot_right)

                        request = QgsFeatureRequest()
                        request.setFilterRect(rect)
                        request.setFlags(QgsFeatureRequest.ExactIntersect)

                        it = layer.getFeatures(request)
                        if it.nextFeature(QgsFeature()):
                            # If pos is over the layer feature, return layer's name
                            layer_name = layer.sourceName()
                            break
                elif isinstance(layer, QgsRasterLayer) and layer.bandCount() == 1 and layer.providerType() == 'gdal':
                    layer_crs = layer.crs()

                    layer_data_provider = layer.dataProvider()

                    p = coord_transform.toMapCoordinates(pos.x(), pos.y())
                    qgs_pos = QgsPointXY(p.x(), p.y())
                    qgs_pos2 = transform_point(
                        QgsPointXY(qgs_pos.x(), qgs_pos.y()),
                        canvas_crs,
                        layer_crs
                    )

                    value, ok = layer_data_provider.sample(qgs_pos2, 1)

                    if ok:
                        layer_name = layer.name()
                        break
            except Exception as e:
                # Just in case that the transformation can't be applied to the layer, pass
                # logger.debug("Error in the transformation. {} {}".format(layer_extent, e))
                pass

        return layer_name, value
