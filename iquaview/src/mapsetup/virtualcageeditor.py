# -*- coding: utf-8 -*-
# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Class to edit the radius of the virtual cage
"""

import logging
import math

from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QWidget
from cola2.utils.ned import NED
from qgis.core import (QgsPointXY,
                       QgsGeometry,
                       QgsWkbTypes,
                       QgsCoordinateReferenceSystem)
from qgis.gui import QgsRubberBand, QgsMapCanvas

from iquaview.src.mapsetup.nedorigindrawer import NEDOriginDrawer
from iquaview.src.mapsetup.virtualcagedrawer import VirtualCageDrawer
from iquaview.src.ui.ui_virtual_cage import Ui_VirtualCage
from iquaview_lib.config import Config
from iquaview_lib.utils.calcutils import distance_ellipsoid
from iquaview_lib.utils.coordinateconverter import format_lat_lon
from iquaview_lib.utils.qgisutils import transform_point

logger = logging.getLogger(__name__)


class VirtualCageEditor(QWidget, Ui_VirtualCage):
    """ Class to edit the radius of the virtual cage"""

    def __init__(self,
                 config: Config,
                 canvas: QgsMapCanvas,
                 ned_origin_drawer: NEDOriginDrawer,
                 virtual_cage_drawer: VirtualCageDrawer,
                 parent=None):
        """
        Init of the object VirtualCageDrawer
        """
        super().__init__(parent)
        self.setupUi(self)
        self.polygon_rb = None
        self.pressed = False
        self.canvas = canvas
        self.config = config
        self.ned_origin_drawer = ned_origin_drawer
        self.virtual_cage_drawer = virtual_cage_drawer

        self.config.saved_signal.connect(self.update_ned_labels)

    def init(self):
        """
        Initialize virtual cage editor params
        """
        if self.ned_origin_drawer.ned_lat is None or self.ned_origin_drawer.ned_lon is None:
            self.ned_origin_drawer.update_ned_point()
        color = QColor("green")
        color.setAlpha(128)
        # Create new polygon rubber band
        self.polygon_rb = QgsRubberBand(self.canvas, QgsWkbTypes.LineGeometry)
        self.polygon_rb.setColor(color)
        self.polygon_rb.setWidth(1)

        self.spinBox.valueChanged.connect(self.update_virtual_cage)
        self.spinBox.editingFinished.connect(self.update_virtual_cage)

        self.update_ned_labels()

        self.spinBox.setValue(int(self.virtual_cage_drawer.cage_radius))

    def set_pressed(self) -> None:
        """ set pressed to True and update the virtual cage rubber band """
        self.pressed = True
        self.update_virtual_cage()

    def update_spinbox(self, point: QgsPointXY) -> None:
        """
        If pressed, calculate distance ellipsoid between ned origin and current mouse position"
        :param point: mouse position point
        :type point: QgsPointXY
        """""
        if self.pressed:
            self.calculate_radius_from_point(point)

    def set_release(self, point: QgsPointXY) -> None:
        """
        set pressed to False, calculate distance ellipsoid between ned origin and current mouse position
        and update the virtual cage rubber band
        :param point: mouse position point
        :type point: QgsPointXY
        """
        self.pressed = False
        radius = self.calculate_radius_from_point(point)
        self.virtual_cage_drawer.set_cage_radius(radius)

    def calculate_radius_from_point(self, point: QgsPointXY) -> int:
        """
        Calculate distance ellipsoid between ned origin and current mouse position
        :param point: mouse position point
        :type point: QgsPointXY
        :return: distance ellipsoid between ned origin and current mouse position
        :rtype: int
        """
        # Transform point if it comes from a different crs
        default_crs = QgsCoordinateReferenceSystem.fromEpsgId(4326)
        point = transform_point(point, self.canvas.mapSettings().destinationCrs(), default_crs)
        radius = distance_ellipsoid(self.ned_origin_drawer.ned_lon, self.ned_origin_drawer.ned_lat,
                                    point.x(), point.y())
        if radius > self.spinBox.maximum():
            radius = self.spinBox.maximum()
        self.spinBox.setValue(int(radius))
        self.update_virtual_cage()
        return radius

    def update_ned_labels(self) -> None:
        """ update ned labels"""
        x = self.ned_origin_drawer.ned_lon
        y = self.ned_origin_drawer.ned_lat

        coordinate_format = self.config.settings.get("coordinate_format", "degree")
        coordinate_precision = self.config.settings.get("coordinate_precision", 7)
        coordinate_directional_suffix = self.config.settings.get("coordinate_directional_suffix", True)
        coordinate_symbols = self.config.settings.get("coordinate_symbols", True)
        [lat_str, lon_str] = format_lat_lon(y,
                                            x,
                                            coordinate_format,
                                            coordinate_precision,
                                            coordinate_directional_suffix,
                                            coordinate_symbols)
        self.lat_value_label.setText(lat_str)
        self.lon_value_label.setText(lon_str)

    def update_virtual_cage(self) -> None:
        """Update rubber band of virtual cage"""
        segments = 500
        ned_origin_lat = 0
        ned_origin_lon = 0
        points = []
        ned = NED(self.ned_origin_drawer.ned_lat, self.ned_origin_drawer.ned_lon, 0.0)
        for i in range(segments):
            theta = i * (2.0 * math.pi / segments)
            latlonh = ned.ned2geodetic([ned_origin_lon + self.spinBox.value() * math.cos(theta),
                                        ned_origin_lat + self.spinBox.value() * math.sin(theta),
                                        0])
            rb_wp = transform_point(
                QgsPointXY(latlonh[1], latlonh[0]),
                QgsCoordinateReferenceSystem.fromEpsgId(4326),
                self.canvas.mapSettings().destinationCrs()
            )
            points.append(rb_wp)

        map_poly_geom = QgsGeometry.fromPolygonXY([points])
        self.polygon_rb.setToGeometry(map_poly_geom)
        self.update_ned_labels()

    def show(self) -> None:
        """ Override show method"""
        self.update_virtual_cage()
        super().show()

    def hide(self) -> None:
        """ Override hide method"""
        self.polygon_rb.reset(QgsWkbTypes.LineGeometry)
        super().hide()

    def setVisible(self, visible: bool) -> None:
        """ Override setVisible method"""
        if visible:
            self.update_virtual_cage()
        else:
            self.polygon_rb.reset(QgsWkbTypes.LineGeometry)
        super().setVisible(visible)
