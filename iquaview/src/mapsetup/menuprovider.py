# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Manages the context menu options of the layers
 when the user right-clicks on a layer of the layers panel.
"""
import logging
import os

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QMenu, QFileDialog, QDialog, QVBoxLayout, QDialogButtonBox, QMessageBox
from qgis.core import (QgsVectorFileWriter,
                       QgsVectorLayer,
                       QgsStyle,
                       QgsCoordinateReferenceSystem,
                       QgsMapLayer,
                       QgsWkbTypes,
                       QgsLayerTreeGroup,
                       QgsRasterLayer,
                       QgsProject)
from qgis.gui import (QgsLayerTreeViewMenuProvider,
                      QgsLayerTreeViewDefaultActions,
                      QgsRendererPropertiesDialog,
                      QgsSingleBandPseudoColorRendererWidget,
                      QgsProjectionSelectionDialog,
                      QgsLayerTreeView,
                      QgsMapCanvas)

from iquaview.src.mapsetup.pointfeaturedlg import PointFeatureDlg, LandmarkAction
from iquaview.src.mission.missionmodule import MissionModule
from iquaview.src.mission.missionactive import MissionActive
from iquaview_lib.config import Config

logger = logging.getLogger(__name__)


def is_landmark(layer):
    """
    Returns if a layer is considered a landmark point. For a layer to be a landmark point, it needs to be a
    QgsVectorLayer, not be a mission and have only one point in its geometry. Any layer that checks that criteria
    will be considered a landmark point, even if it was not created with the landmark tool.
    :param layer: layer to check
    :type layer: QgsVectorLayer
    :return: Whether if the layer is a landmark or not
    :rtype: bool
    """
    if (isinstance(layer, QgsVectorLayer)
            and layer.customProperty("mission_xml") is None and layer.featureCount() == 1):
        feature_it = layer.dataProvider().getFeatures()
        feature = next(feature_it, None)
        if feature is not None and feature.geometry().wkbType() in [QgsWkbTypes.Point, QgsWkbTypes.Point25D]:
            return True

    return False


class MenuProvider(QgsLayerTreeViewMenuProvider):
    """
    Manages the context menu options of the layers.
    """

    def __init__(self,
                 config: Config,
                 view: QgsLayerTreeView,
                 canvas: QgsMapCanvas,
                 proj: QgsProject,
                 mission_module: MissionModule,
                 mission_active: MissionActive,
                 parent=None):
        """
        Init of the object MenuProvider
        :param config: Configuration object
        :type config: Config
        :param view: view where the menu provider will operate
        :type view: QgsLayerTreeView
        :param canvas: canvas associated with the layers of view
        :type canvas: QgsMapCanvas
        :param proj: project where the layers are at
        :type proj: QgsProject
        :param mission_module: mission
        :type mission_module: MissionModule
        """
        QgsLayerTreeViewMenuProvider.__init__(self)
        self.config = config
        self.view = view
        self.canvas = canvas
        self.proj = proj
        self.mission_module = mission_module
        self.mission_active = mission_active
        self.mission_ctrl = self.mission_module.get_mission_controller()
        self.defActions = QgsLayerTreeViewDefaultActions(self.view)
        self.name = None
        self.parent = parent

    def mission_menu(self, parent_menu: QMenu) -> QMenu:
        """
        Creates the menu for the missions.
        :param parent_menu: menu where the mission menu will be added
        :type parent_menu: QMenu
        :return: the mission menu
        :rtype: QMenu
        """
        menu = QMenu("Mission", parent_menu)
        menu.addMenu(self.mission_active.execute_mission_menu)
        menu.addSeparator()
        menu.addAction(self.mission_module.start_sftp_action)
        menu.addAction(self.mission_module.save_mission_action)
        menu.addAction(self.mission_module.saveas_mission_action)
        menu.addAction(self.mission_module.edit_wp_mission_action)
        menu.addAction(self.mission_module.select_features_mission_action)
        menu.addAction(self.mission_module.template_mission_action)
        menu.addAction(self.mission_module.move_mission_action)

        return menu

    def layer_context_menu(self) -> QMenu:
        """
        Creates the context menu of the layers
        :return: the context menu
        :rtype: QMenu
        """
        layer = self.view.currentLayer()
        self.connect_signal_name()
        menu = QMenu()
        menu.addAction("Zoom to Layer", self.zoom_to_layer)
        menu.addAction("Rename Layer", self.rename_layer)
        # we only offer the option to save layer if it is a vector layer and has a valid geometry
        if (layer.type() == QgsMapLayer.VectorLayer
                and layer.geometryType() not in [QgsWkbTypes.UnknownGeometry, QgsWkbTypes.NullGeometry]):
            menu.addAction("Save as Layer", self.save_layer)
        menu.addAction("Remove Layer", self.remove_group_or_layer)
        # properties option only for vector or raster layers that are single band (0 or 1)
        if layer.type() != QgsMapLayer.RasterLayer or layer.rasterType() < 2:
            menu.addAction("Properties", self.layer_properties)
        if layer.customProperty("mission_xml") is None:
            menu.addAction("Select CRS", self.layer_crs)
        if is_landmark(layer) or layer.customProperty("ned_origin") is not None:
            menu.addAction("Show/Edit coordinates", self.show_coordinates)

        if layer.customProperty("mission_xml") is not None:
            menu.addSeparator()
            menu.addMenu(self.mission_menu(menu))
            menu.addSeparator()

        menu.addSeparator()
        menu.addAction("Add Group", self.add_group)
        return menu

    def group_context_menu(self) -> QMenu:
        """
        Creates the context menu of the groups
        :return: the context menu
        :rtype: QMenu
        """
        self.connect_signal_name()
        menu = QMenu()
        menu.addAction("Zoom to Group", self.zoom_to_group)
        menu.addAction("Rename Group", self.rename_group)
        menu.addSeparator()
        menu.addAction("Remove Group", self.remove_group_or_layer)
        menu.addSeparator()
        menu.addAction("Add Group", self.add_group)

        # TODO: change crs
        # if layer.customProperty("mission_xml") is None:
        #     m.addAction("Select Group CRS", self.layer_crs)
        return menu

    def createContextMenu(self) -> QMenu:
        """ Overrides method createContexMenu of QgsLayerTreeViewMenuProvider """

        if self.view.currentLayer():
            return self.layer_context_menu()
        # checks that it can only be of type QgsLayerTreeGroup, not if it inherits
        if type(self.view.currentNode()) is QgsLayerTreeGroup:
            return self.group_context_menu()

        menu = QMenu()
        menu.addAction("Add layer", self.add_layer)
        menu.addSeparator()
        menu.addAction("Add Group", self.add_group)

        return menu

    def zoom_to_layer(self):
        """ Zooms the layer on the canvas."""
        self.defActions.zoomToLayer(self.canvas)

    def zoom_to_group(self):
        """ Zooms the group on the canvas"""
        self.defActions.zoomToGroup(self.canvas)

    def rename_layer(self):
        """ Starts renaming the layer """
        self.name = self.view.currentLayer().name()
        self.defActions.renameGroupOrLayer()

    def rename_group(self):
        """ Starts renaming the group"""
        self.name = self.view.currentNode().name()
        self.defActions.renameGroupOrLayer()

    def name_changed(self, node, name):
        """
         Checks if more than one layer has the same name or the forbidden char '/' and shows a messagebox warning
        :param node: node that name is changed
        :type node: QgsLayerTreeNode
        :param name: new name
        :type name: str
        """
        n = 0
        # check every layer
        for layer in self.proj.mapLayers().values():
            if layer.name() == name:
                n += 1
        # if exists more than one layer with the same name
        if n > 1:
            logger.error("More than one layer was found with this name")
            # set previous name
            self.view.currentLayer().setName(self.name)
            QMessageBox.critical(None,
                                 "Error in renaming",
                                 "More than one layer was found with this name. Please enter another name.",
                                 QMessageBox.Close)
        elif self.view.currentLayer():
            if name.rfind('/') != -1:
                if self.view.currentLayer().name() == name:
                    name = name.replace('/', '')
                    self.view.currentLayer().setName(name)
                    QMessageBox.warning(None, "Warning", "Usage of '/' is forbidden \n\n You may use '\\' instead")

            if self.view.currentLayer().customProperty("mission_xml") is not None:
                self.mission_ctrl.set_current_mission_name(name)

    def add_group(self):
        """ Add new group"""
        root = self.proj.layerTreeRoot()
        root.insertGroup(0, "group")

    def add_layer(self):
        """ Add new layer"""
        self.parent.add_layer()

    def remove_group_or_layer(self):
        """ Removes the selected group or layer """
        # if layer selected
        n_indexes = len(self.view.selectionModel().selectedIndexes())
        if not n_indexes:
            return
        if n_indexes == 1:
            confirmation_msg = f"Are you sure you want to remove {self.view.currentNode().name()}?"
        else:
            groups = 0
            layers = 0
            for node in self.view.selectedNodes():
                if isinstance(node, QgsLayerTreeGroup):
                    groups += 1
                else:
                    layers += 1
            confirmation_msg = "Are you sure you want to remove"
            if groups == 1:
                confirmation_msg += f" {groups} group"
            elif groups > 1:
                confirmation_msg += f" {groups} groups"
            if groups and layers:
                confirmation_msg += " and"
            if layers == 1:
                confirmation_msg += f" {layers} layer"
            elif layers > 1:
                confirmation_msg += f" {layers} layers"
            confirmation_msg += "?"

        reply = QMessageBox.question(self.parent, 'Removal confirmation',
                                     confirmation_msg, QMessageBox.Yes | QMessageBox.No, defaultButton=QMessageBox.Yes)
        if reply == QMessageBox.Yes:
            self.defActions.removeGroupOrLayer()

    def save_layer(self):
        """ Saves the layer with the name and format specified """
        layer = self.view.currentLayer()
        # if it is a vector layer and has a valid geometry
        if (layer.type() == QgsMapLayer.VectorLayer
                and layer.geometryType() not in [QgsWkbTypes.UnknownGeometry, QgsWkbTypes.NullGeometry]):
            path = os.path.dirname(os.path.abspath(self.config.settings.get('last_open_project', "")))
            path = os.path.join(path, layer.name())
            layer_name, selected_filter = QFileDialog.getSaveFileName(None,
                                                                      'Save Layer',
                                                                      path,
                                                                      'Shapefile (*.shp);;KML (*.kml);;GPX (*.gpx)')
            if layer_name != '':

                if selected_filter == "Shapefile (*.shp)":

                    if not layer_name.endswith('.shp'):
                        layer_name = layer_name + '.shp'
                    ret = QgsVectorFileWriter.writeAsVectorFormat(layer, layer_name, "utf-8",
                                                                  QgsCoordinateReferenceSystem.fromEpsgId(4326),
                                                                  "ESRI Shapefile")

                elif selected_filter == "KML (*.kml)":

                    if not layer_name.endswith('.kml'):
                        layer_name = layer_name + '.kml'

                    ret = QgsVectorFileWriter.writeAsVectorFormat(layer, layer_name, "utf-8",
                                                                  QgsCoordinateReferenceSystem.fromEpsgId(4326),
                                                                  "KML")
                elif selected_filter == "GPX (*.gpx)":

                    if not layer_name.endswith('.gpx'):
                        layer_name = layer_name + '.gpx'
                    ds_options = ["GPX_USE_EXTENSIONS=TRUE"]
                    ret = QgsVectorFileWriter.writeAsVectorFormat(layer, layer_name, "utf-8",
                                                                  QgsCoordinateReferenceSystem.fromEpsgId(4326),
                                                                  "GPX",
                                                                  datasourceOptions=ds_options)

                if ret[0] == QgsVectorFileWriter.NoError:
                    logger.info(f"Layer {layer.name()} saved to {layer_name}")

    def layer_properties(self):
        """ Opens a dialog to view and edit the properties of the selected layer """
        layer = self.view.currentLayer()
        # if it is a vector layer and has a valid geometry
        if (layer.type() == QgsMapLayer.VectorLayer
                and layer.geometryType() not in [QgsWkbTypes.UnknownGeometry, QgsWkbTypes.NullGeometry]):
            # wrap style dialog with the buttons ok and cancel so that we can apply changes
            dlg = QDialog()
            dlg.widget = QgsRendererPropertiesDialog(self.view.currentLayer(), QgsStyle.defaultStyle(), True)
            dlg.layout = QVBoxLayout(dlg)
            dlg.buttons = QDialogButtonBox(dlg)
            dlg.layout.addWidget(dlg.widget)
            dlg.layout.addWidget(dlg.buttons)
            dlg.buttons.setOrientation(Qt.Horizontal)
            dlg.buttons.setStandardButtons(QDialogButtonBox.Cancel | QDialogButtonBox.Ok)
            max_width = max(dlg.width(), self.parent.width() / 3)
            max_height = max(dlg.height(), self.parent.height() * 2 / 3)
            dlg.resize(max_width, max_height)

            # set signals
            def on_style_edit_accept(d):
                # this will update the layer's style
                d.widget.onOK()
                d.accept()

            dlg.buttons.accepted.connect(lambda d=dlg: on_style_edit_accept(d))
            dlg.buttons.rejected.connect(dlg.reject)
            dlg.exec_()
            self.canvas.refresh()
        elif layer.type() == QgsMapLayer.RasterLayer and layer.rasterType() != QgsRasterLayer.Multiband:
            dlg = QDialog()
            dlg.widget = QgsSingleBandPseudoColorRendererWidget(layer)
            dlg.layout = QVBoxLayout(dlg)
            dlg.buttons = QDialogButtonBox(dlg)
            dlg.layout.addWidget(dlg.widget)
            dlg.layout.addWidget(dlg.buttons)
            dlg.buttons.setOrientation(Qt.Horizontal)
            dlg.buttons.setStandardButtons(QDialogButtonBox.Cancel | QDialogButtonBox.Ok)

            # set signals
            def on_rasterstyle_edit_accept(d):
                # this will update the layer's style
                renderer = d.widget.renderer()
                layer.setRenderer(renderer)
                d.accept()

            dlg.buttons.accepted.connect(lambda d=dlg: on_rasterstyle_edit_accept(d))
            dlg.buttons.rejected.connect(dlg.reject)
            dlg.exec_()
            self.canvas.refresh()
        elif layer.type() == QgsMapLayer.RasterLayer and layer.rasterType() == QgsRasterLayer.Multiband:
            logger.info("multiband")

    # TODO: Check that it really changes CRS
    def layer_crs(self):
        """ Opens a coordinate reference system dialog to view and modify the crs of the layer """
        projection_selector = QgsProjectionSelectionDialog()
        projection_selector.exec()
        crs = (projection_selector.crs())
        layer = self.view.currentLayer()
        layer.setCrs(crs)
        self.zoom_to_layer()

    def connect_signal_name(self):
        """ Catch signal nameChanged on name_changed slot. """
        while True:
            try:
                if self.view.currentNode() is not None:
                    # disconnect function name_changed for signal nameChanged
                    self.view.currentNode().nameChanged.disconnect(self.name_changed)
                else:
                    break
            except TypeError:
                break
        if self.view.currentNode() is not None:
            # connect function name_changed on signal nameChanged
            self.view.currentNode().nameChanged.connect(self.name_changed)

    def show_coordinates(self):
        """
        Open landmark coordinates dialog that shows the coordinates of the current layer
        """
        layer: QgsMapLayer = self.view.currentLayer()

        dialog = PointFeatureDlg(self.canvas, self.proj, self.config, LandmarkAction.EDIT, self.parent)
        dialog.set_initial_point(layer)
        layer.willBeDeleted.connect(dialog.close)
        dialog.map_tool_change_signal.connect(self.parent.reset_map_tool)
        dialog.finish_add_landmark_signal.connect(self.parent.finish_add_landmark)

        dialog.show()
