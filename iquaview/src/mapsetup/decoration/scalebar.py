# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
Class to draw a scale bar in the map canvas
"""

import math

from PyQt5.QtGui import QPen, QPolygon, QFont, QFontMetrics, QColor
from qgis.gui import QgsMapCanvasItem
from qgis.core import QgsDistanceArea, QgsProject


class ScaleBar(QgsMapCanvasItem):
    """
    Class to draw a scale bar in the map canvas
    """

    def __init__(self, canvas, config):
        """
        Init of the object ScaleBar
        :param canvas: canvas where scale bar will be drawn
        :type canvas: QgsMapCanvas
        """
        super().__init__(canvas)
        self.canvas = canvas
        # set font
        self.font = QFont("helvetica", 10)
        self.font_metrics = QFontMetrics(self.font)

        # set default color
        self.default_color = QColor(config.settings['style_scale_bar_color'])
        self.color = self.default_color

        # set crs and ellipsoid
        crs = self.canvas.mapSettings().destinationCrs()
        self.distance_calc = QgsDistanceArea()
        self.distance_calc.setSourceCrs(crs, QgsProject.instance().transformContext())
        self.distance_calc.setEllipsoid(QgsProject.instance().ellipsoid())

        self.canvas.destinationCrsChanged.connect(self.canvas_crs_changed)

    def canvas_crs_changed(self):
        """
        If canvas crs changed updates crs
        """
        crs = self.canvas.mapSettings().destinationCrs()
        self.distance_calc.setSourceCrs(crs, QgsProject.instance().transformContext())
        self.distance_calc.setEllipsoid(QgsProject.instance().ellipsoid())

    def paint(self, painter, option=None, widget=None):
        """
        Overrides paint method of QgsMapCanvasItem. Paints scalebar to the canvas.

        :param painter: painter to use
        :type painter: QPainter
        :param option: describes the parameters needed to draw a QGraphicsItem
        :type option: QStyleOptionGraphicsItem
        :param widget: A concrete widget to paint over, None by default
        :type widget: QWidget
        """
        tick_size = 8
        text_offset = 3

        # set origins
        x_origin = self.canvas.width() - 40
        y_origin = self.canvas.height() - 20

        start_x = self.canvas.width() / 2
        start_y = self.canvas.height() / 2
        # set a default size
        scalebar_width = self.canvas.width() / 6.0

        # get the distance between 2 points
        transform = self.canvas.getCoordinateTransform()
        start_point = transform.toMapCoordinates(start_x - scalebar_width, start_y)
        end_point = transform.toMapCoordinates(start_x, start_y)

        crs = self.canvas.mapSettings().destinationCrs()
        self.distance_calc.setSourceCrs(crs, QgsProject.instance().transformContext())
        self.distance_calc.setEllipsoid(QgsProject.instance().ellipsoid())
        distance = self.distance_calc.measureLine([start_point, end_point])

        if math.isnan(distance) or distance == 0:
            return  # nothing to draw
        # Round distance to the nearest whole number
        if distance > 1:
            length = int(math.log10(distance))
            rounded = round(distance, -length)
        else:
            # count how many zeroes the number has
            zeroes = abs(math.ceil(math.log10(distance)))
            rounded = round(distance, zeroes + 1)

        # Get pixel width of the rounded distance
        scalebar_width = int(scalebar_width * rounded / distance)

        scalebar_unit = "m"
        if distance > 1000.0:
            scalebar_unit = "km"
            rounded = rounded / 1000
        elif distance < 0.1:
            if distance < 0.01:
                scalebar_unit = "mm"
                # round again after changing units to control length of floats
                distance = distance * 1000
            else:
                scalebar_unit = "cm"
                distance = distance * 100

            # round again after changing units to control length of floats
            zeroes = abs(math.ceil(math.log10(distance)))
            rounded = round(distance, zeroes + 1)

        if rounded != 0.0:
            # save previous painter
            painter.save()
            # set rotation
            painter.rotate(- self.canvas.rotation())
            # set translation
            painter.translate(x_origin, y_origin)

            # set qpen
            background_pen = QPen(self.color, 4)
            # create bar
            bar_array = QPolygon(2)
            bar_array.putPoints(0,
                                0 - scalebar_width, 0 + tick_size / 2,
                                0, 0 + tick_size / 2)

            painter.setPen(background_pen)
            # draw line
            painter.drawPolyline(bar_array)

            # draw 0
            painter.drawText(0 - scalebar_width - (self.font_metrics.width("0") / 2),
                             0 - (self.font_metrics.height() / 4),
                             "0")
            # draw max
            painter.drawText(0 - (self.font_metrics.width(str(rounded)) / 2),
                             0 - (self.font_metrics.height() / 4),
                             str(rounded))

            # draw unit label
            painter.drawText((0 + text_offset),
                             (0 + tick_size),
                             str(scalebar_unit))
            # restore painter
            painter.restore()

    def change_color(self, color):
        """
        Changes the color of the painted object
        :param color:
        :type color: QColor
        """
        self.color = color

    def get_current_color(self):
        """
        Returns the current color of the scale bar
        :return: current color
        :rtype: QColor
        """
        return QColor(self.color)

    def get_default_color(self):
        """
        Returns the default color of the scale bar
        :return: default color
        :rtype: QColor
        """
        return self.default_color
