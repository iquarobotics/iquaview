# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
Class to draw a north arrow in the map canvas
"""

from PyQt5.QtCore import QSize, QRectF, QFile, QIODevice, QByteArray
from PyQt5.QtGui import QColor
from PyQt5.QtSvg import QSvgRenderer
from qgis.gui import QgsMapCanvasItem
from qgis.core import QgsBearingUtils, QgsPointXY


class NorthArrow(QgsMapCanvasItem):
    def __init__(self, canvas, config):
        """
        Init of the object NorthArrow
        :param canvas: canvas where north arrow will be drawn
        :type canvas: QgsMapCanvas
        """
        super().__init__(canvas)
        self.canvas = canvas
        self.map_pos = QgsPointXY(0.0, 0.0)
        self.size = QSize(42, 64)
        self.corner = 1
        self.default_color = None
        self.current_color = None

        file = QFile(":/resources/arrow.svg")
        file.open(QIODevice.ReadOnly)
        self.file_data = file.readAll()

        # find the base color in the metadata of the svg file
        metadata_id = "base_color"
        pos = self.file_data.indexOf("base_color")
        if pos != -1:
            metadata_value = self.file_data.mid(pos + len(metadata_id) + 2, 7)
            self.current_color = bytes(metadata_value).decode()

        self.default_color = QColor(config.settings['style_north_arrow_color'])
        self.change_color(self.default_color)

    def paint(self, painter, option=None, widget=None):
        """
        Overrides paint method of QgsMapCanvasItem. Paints north arrow to the canvas.

        :param painter: painter to use
        :type painter: QPainter
        :param option: describes the parameters needed to draw a QGraphicsItem
        :type option: QStyleOptionGraphicsItem
        :param widget: A concrete widget to paint over, None by default
        :type widget: QWidget
        """
        svg_renderer = QSvgRenderer(self.file_data)
        if svg_renderer.isValid():
            pos = self.set_position(self.corner, painter.device().width(), painter.device().height())
            try:
                rotation = QgsBearingUtils.bearingTrueNorth(self.canvas.mapSettings().destinationCrs(),
                                                            self.canvas.mapSettings().transformContext(),
                                                            self.canvas.extent().center())
            except:
                return

            rotation += self.canvas.rotation()

            painter.save()
            painter.rotate(-rotation)  # To translate correctly
            painter.translate(pos.x(), pos.y())
            painter.rotate(rotation)  # To rotate north arrow along with canvas, always pointing north

            # do the drawing, and draw a smooth north arrow even when rotated
            rectangle = QRectF(-self.size.width() / 2, -self.size.height() / 2, self.size.width(), self.size.height())
            svg_renderer.render(painter, rectangle)

            painter.restore()

    def set_position(self, corner, width, height):
        """
        Returns the position of the specified corner with a concrete width and height
        :param corner: can be 1, 2, 3 or 4. top left, top right, bot left and bot right respectively
        :type corner: int
        :param width: width of the paint space
        :type width: int
        :param height: height of the paint space
        :type height: int
        :return: point of the specified corner
        :rtype: QgsPointXY
        """
        if corner == 1:  # top left corner
            return QgsPointXY(0 + self.size.height() / 2, 0 + self.size.height() / 2)
        elif corner == 2:  # top right corner
            return QgsPointXY(width - self.size.height() / 2, 0 + self.size.height() / 2)
        elif corner == 3:  # bottom left corner
            return QgsPointXY(0 + self.size.height() / 2, height - self.size.height() / 2)
        elif corner == 4:  # bottom right corner
            return QgsPointXY(width - self.size.height() / 2, height - self.size.height() / 2)

    def change_color(self, color):
        """
        Changes the color of the painted object. This will only work if svg_color was found in the
        metadata of the svg when constructing this object.
        :param color:
        :type color: QColor
        """
        if self.current_color is not None:
            self.file_data.replace(QByteArray().append(self.current_color), QByteArray().append(color.name()))
            self.current_color = color.name()

    def get_current_color(self):
        """
        Returns the current color of the north arrow
        :return: current color
        :rtype: QColor
        """
        if self.current_color is not None:
            return QColor(self.current_color)

    def get_default_color(self):
        """
        Returns the default color of the north arrow
        :return: default color
        :rtype: QColor
        """
        return self.default_color
