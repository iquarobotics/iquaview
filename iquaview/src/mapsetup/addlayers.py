# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Dialog to select layer of different type (Vector, Raster or Tiled Web Map).
"""

import logging
import urllib

from PyQt5.QtCore import QFileInfo, QTextStream, QTextCodec, QFile, QIODevice, QSize, QUrl, QUrlQuery
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QDialog, QMessageBox, QTableWidgetItem
from qgis.core import (QgsVectorLayer,
                       QgsRasterLayer,
                       QgsVectorDataProvider,
                       QgsFeature,
                       QgsGeometry,
                       QgsRectangle,
                       QgsCoordinateTransform,
                       QgsProject)
from qgis.gui import QgsFileWidget, QgsSublayersDialog

from iquaview.src.ui.ui_addlayer_dlg import Ui_AddLayer

logger = logging.getLogger(__name__)

VECTOR_LAYER = 0
DELIMITED_TEXT_LAYER = 1
RASTER_LAYER = 2
TILED_WEB_MAP_LAYER = 3

ICON_SIZE = 32


class AddLayersDlg(QDialog, Ui_AddLayer):
    """
    Dialog to select layer of different type (Vector, Raster or Tiled Web Map).
    """

    def __init__(self, proj, canvas, msglog, parent=None):
        super().__init__(parent)
        self.proj = proj
        self.canvas = canvas
        self.msglog = msglog
        self.setupUi(self)
        self.setWindowTitle("Add Layer")

        self.vector_filename = None
        self.vector_base_name = None
        self.vector_file_extension = None
        self.raster_filename = None
        self.raster_base_name = None

        # set images
        self.mOptionsListWidget.item(VECTOR_LAYER).setIcon(QIcon(":/resources/mActionAddOgrLayer.svg"))
        self.mOptionsListWidget.item(DELIMITED_TEXT_LAYER).setIcon(
            QIcon(":/resources/mActionAddDelimitedTextLayer.svg"))
        self.mOptionsListWidget.item(RASTER_LAYER).setIcon(QIcon(":/resources/mActionAddRasterLayer.svg"))
        self.mOptionsListWidget.item(TILED_WEB_MAP_LAYER).setIcon(QIcon(":/resources/mActionAddWmsLayer.svg"))

        self.mOptionsListWidget.setIconSize(QSize(ICON_SIZE, ICON_SIZE))

        crs = self.canvas.mapSettings().destinationCrs()
        self.crsGeometry.setCrs(crs)

        self.cmbEncoding.clear()
        self.cmbEncoding.addItems(QgsVectorDataProvider.availableEncodings())
        self.cmbEncoding.setCurrentIndex(self.cmbEncoding.findText("UTF-8"))

        self.tblSample.clear()
        self.tblSample.setColumnCount(0)
        self.tblSample.setRowCount(0)

        self.vector_mQgsFileWidget.setStorageMode(QgsFileWidget.GetMultipleFiles)
        self.vector_mQgsFileWidget.setFilter("All files (*.*);;"
                                             "ESRI Shapefile (*.shp) ;;"
                                             "Keyhole Markup Language [KML] (*.kml) ;;"
                                             "OpenStreetMap (*.osm) ;;"
                                             "S-57 [ENC] (*.000) ;;"
                                             "GPS eXchange Format [GPX] (*.gpx)")

        self.raster_mQgsFileWidget.setStorageMode(QgsFileWidget.GetMultipleFiles)
        self.raster_mQgsFileWidget.setFilter("GeoTIFF(*.tif) ;; All files (*.*)")

        self.mOptionsListWidget.currentRowChanged.connect(self.set_current_page)

        self.vector_mQgsFileWidget.fileChanged.connect(self.set_name_linedit)
        self.raster_mQgsFileWidget.fileChanged.connect(self.set_name_linedit)
        self.delimited_text_mFileWidget.fileChanged.connect(self.set_name_linedit)

        self.cmbEncoding.currentIndexChanged.connect(self.update_field_lists)
        self.rowCounter.valueChanged.connect(self.update_field_lists)
        self.cbxUseHeader.clicked.connect(self.update_field_lists)

        self.buttonBox.accepted.connect(self.on_accept)

    def set_current_page(self, item):
        """
        Change item in Stacked widget
        :param item: position of the page
        :type item: int
        """
        self.mOptionsStackedWidget.setCurrentIndex(item)

    def set_name_linedit(self, name):
        """
        Set name in the corresponding lineedit only one file is selected.
        Otherwise set a blank name and disable line edit
        :param name: file name
        :type name: str
        """
        is_one_layer = False
        file_info = QFileInfo(name)
        if len(name.split()) == 1:
            is_one_layer = True
            base_name = file_info.baseName()
        else:
            base_name = ""

        if self.sender() == self.vector_mQgsFileWidget:
            self.vector_name_lineEdit.setEnabled(is_one_layer)
            self.vector_name_lineEdit.setText(base_name)

        elif self.sender() == self.raster_mQgsFileWidget:
            self.raster_name_lineEdit.setEnabled(is_one_layer)
            self.raster_name_lineEdit.setText(base_name)

        elif self.sender() == self.delimited_text_mFileWidget:
            self.txtLayerName.setText(base_name)
            if file_info.exists() and file_info.isFile():
                self.update_field_lists()

    def update_field_lists(self) -> None:
        """
        Update the field lists of the delimited text page
        :return: None
        """

        self.cmbYField.clear()
        self.cmbXField.clear()
        self.tblSample.clear()

        max_n_sample = 20
        file = QFile(self.delimited_text_mFileWidget.filePath())

        if file != "":

            file.open(QIODevice.ReadOnly)
            stream = QTextStream(file)
            stream.seek(0)
            codec = QTextCodec.codecForName(self.cmbEncoding.currentText())
            stream.setCodec(codec)

            # lines to discard
            if self.rowCounter.value() > 0:
                for i in range(self.rowCounter.value()):
                    stream.readLine()

            self.tblSample.setRowCount(max_n_sample)

            for nrow in range(0, max_n_sample):
                new_line = stream.readLine()
                fields = new_line.split(",")

                sample_row = nrow - 1 if self.cbxUseHeader.isChecked() else nrow

                if nrow == 0:
                    if self.cbxUseHeader.isChecked():
                        for field in fields:
                            self.cmbXField.addItem(field)
                            self.cmbYField.addItem(field)

                        is_valid_coordinate = False
                        is_valid_coordinate = self.try_set_xy_field(fields, is_valid_coordinate, "longitude",
                                                                    "latitude")
                        is_valid_coordinate = self.try_set_xy_field(fields, is_valid_coordinate, "lon", "lat")
                        is_valid_coordinate = self.try_set_xy_field(fields, is_valid_coordinate, "east", "north")
                        is_valid_coordinate = self.try_set_xy_field(fields, is_valid_coordinate, "x", "y")
                        is_valid_coordinate = self.try_set_xy_field(fields, is_valid_coordinate, "e", "n")

                        self.tblSample.setColumnCount(len(fields))
                        self.tblSample.setHorizontalHeaderLabels(fields)

                        # To no add header to data
                        continue
                    else:
                        labels = []
                        for field in range(0, len(fields)):
                            labels.append(str(f"field_{field + 1}"))
                            self.cmbXField.addItem(str(f"field_{field + 1}"))
                            self.cmbYField.addItem(str(f"field_{field + 1}"))

                        self.tblSample.setColumnCount(len(fields))
                        self.tblSample.setHorizontalHeaderLabels(labels)

                for i, field in enumerate(fields):
                    self.tblSample.setItem(sample_row, i, QTableWidgetItem(field))
            file.close()

    def try_set_xy_field(self, field_list, is_valid_coordinate, xfield, yfield) -> bool:
        """
        Try to set X and Y combobox fields
        :param field_list: list of field
        :type field_list: list
        :param is_valid_coordinate: if valid coordinate was set before
        :type is_valid_coordinate: bool
        :param xfield: x field
        :type xfield: str
        :param yfield: y field
        :type yfield: str
        :return: return true if is valid coordinate, otherwise false
        :rtype: bool
        """
        if not is_valid_coordinate:
            valid_x = False
            valid_y = False
            for index, field in enumerate(field_list):
                if not valid_x and (xfield in field):
                    self.cmbXField.setCurrentIndex(index)
                    valid_x = True
                if not valid_y and (yfield in field):
                    self.cmbYField.setCurrentIndex(index)
                    valid_y = True

            is_valid_coordinate = (valid_x and valid_y)
        return is_valid_coordinate

    def on_accept(self):
        """ Adds a layer depending on its specified type in the radio buttons. """
        if self.mOptionsStackedWidget.currentIndex() == VECTOR_LAYER:
            self.add_vector_layer()

        elif self.mOptionsStackedWidget.currentIndex() == DELIMITED_TEXT_LAYER:
            self.add_delimited_text_layer()

        elif self.mOptionsStackedWidget.currentIndex() == RASTER_LAYER:
            self.add_raster_layer()

        elif self.mOptionsStackedWidget.currentIndex() == TILED_WEB_MAP_LAYER:
            self.add_tiled_web_map_layer()

    def add_vector_layer(self):
        """ Add vector layer to project."""
        file_paths = self.vector_mQgsFileWidget.filePath()
        name = self.vector_name_lineEdit.text()
        self.add_layers(file_paths, name)

    def add_raster_layer(self):
        """ Add raster layer to project."""
        file_paths = self.raster_mQgsFileWidget.filePath()
        name = self.raster_name_lineEdit.text()
        self.add_layers(file_paths, name)

    def add_layers(self, file_paths, name):
        """
         Adds layers to project, specified in the list files.
        :param file_paths: list containing all the file paths to add to the project
        :type file_paths: list
        :param name: Name to give to the layer, in case there is only one
        :type name: str
        """
        if len(file_paths) > 0:
            files = parse_layers(file_paths)
            add_layers_to_project(self.proj, self.canvas, files, name)
            self.accept()
        else:
            QMessageBox.warning(self, "Add Layer", "No layer selected to add", QMessageBox.Close)

    def add_delimited_text_layer(self):
        """ Add delimited text (CSV) layer to project."""

        if self.txtLayerName.text() == "":
            QMessageBox.warning(self, "No layer name", "Please enter a layer name before adding the layer to the map")
            return

        use_header = "Yes" if self.cbxUseHeader.isChecked() else "No"

        # Construct the URL and query for the CSV file
        url = QUrl.fromLocalFile(self.delimited_text_mFileWidget.filePath())
        query = QUrlQuery(url)
        query.addQueryItem("encoding", self.cmbEncoding.currentText())
        query.addQueryItem("type", 'csv')
        query.addQueryItem("delimiter", ',')
        query.addQueryItem("xField", self.cmbXField.currentText())
        query.addQueryItem("yField", self.cmbYField.currentText())
        query.addQueryItem("skipLines", str(self.rowCounter.value()))
        query.addQueryItem("useHeader", use_header)

        url.setQuery(query)

        csv_base_name = self.txtLayerName.text()

        vlayer = QgsVectorLayer(str(url.toEncoded(), 'latin1'), csv_base_name, "delimitedtext")
        vlayer.setCrs(self.crsGeometry.crs())

        if not vlayer.isValid():
            logger.warning("Layer failed to load!")
            QMessageBox.critical(self,
                                 "Add Layer",
                                 "Layer format is not recognised as a supported file format",
                                 QMessageBox.Close)
            self.msglog.logMessage("Failed to load layer " + vlayer.name(), "LoadingLayer", 1)
        else:

            if self.points_radioButton.isChecked():
                self.proj.addMapLayer(vlayer)
                self.canvas.setExtent(vlayer.extent())
                self.accept()

            else:
                vertices = []
                list_features = vlayer.dataProvider().getFeatures()

                for feat in list_features:  # loop
                    vertices_it = feat.geometry().vertices()
                    for vert in vertices_it:
                        vertices.append(vert)
                geometry = QgsGeometry.fromPolyline(vertices)

                geometric_object = "LineString?crs=epsg:4326"

                layer = QgsVectorLayer(
                    geometric_object,
                    csv_base_name,
                    "memory")
                feature = QgsFeature()
                feature.setGeometry(geometry)
                layer.dataProvider().addFeatures([feature])

                self.proj.addMapLayer(layer)
                self.canvas.setExtent(layer.extent())
                self.accept()

    def add_tiled_web_map_layer(self):
        """ Add tiled web map layer to project."""
        zmin = "&zmin=0"
        zmax = ""

        if self.mCheckBoxZMax_2.isChecked():
            zmax = "&zmax=" + self.mSpinZMax_2.text()

        if self.wms_predefined_radioButton.isChecked():
            if self.url_comboBox.currentText() == "OpenStreetMap":
                # open street map
                url_with_params = f'type=xyz&' \
                                  f'url=http://a.tile.openstreetmap.org/%7Bz%7D/%7Bx%7D/%7By%7D.png{zmax}{zmin}'
            elif self.url_comboBox.currentText() == "OpenSeaMap":
                # open sea map
                url_with_params = f'type=xyz&' \
                                  f'url=http://t1.openseamap.org/seamark/%7Bz%7D/%7Bx%7D/%7By%7D.png{zmax}{zmin}'
            elif self.url_comboBox.currentText() == "EMODnet":
                # EMODnet
                url_with_params = 'crs=EPSG:3857&' \
                                  'dpiMode=7&' \
                                  'format=image/png&' \
                                  'layers=emodnet:mean&' \
                                  'styles&' \
                                  'url=https://ows.emodnet-bathymetry.eu/wms'
            elif self.url_comboBox.currentText() == "Global Multi-Resolution Topography (GMRT)":
                # GMRT https://www.gmrt.org/services/
                url_with_params = 'crs=EPSG:3857&' \
                                  'dpiMode=7&' \
                                  'format=image/png&' \
                                  'layers=topo&' \
                                  'styles&' \
                                  'url=http://www.gmrt.org/services/mapserver/wms_merc_mask?version%3D1.3.0'
                if self.mEditName_2.text() == "":
                    self.mEditName_2.setText("GMRT")
            elif self.url_comboBox.currentText() == "Google Hybrid":
                # per google maps
                url_with_params = f'type=xyz&' \
                                  f'url=https://mt1.google.com/vt/lyrs%3Dy%26x%3D%7Bx%7D%26y%3D%7By%7D%26z%3D%7Bz%7D{zmax}{zmin}'
            elif self.url_comboBox.currentText() == "Google Map":
                # per google maps
                url_with_params = f'type=xyz&' \
                                  f'url=https://mt1.google.com/vt/lyrs%3Dm%26x%3D%7Bx%7D%26y%3D%7By%7D%26z%3D%7Bz%7D{zmax}{zmin}'
            elif self.url_comboBox.currentText() == "Google Satelite":
                # per google maps
                url_with_params = f'type=xyz&' \
                                  f'url=https://mt1.google.com/vt/lyrs%3Ds%26x%3D%7Bx%7D%26y%3D%7By%7D%26z%3D%7Bz%7D{zmax}{zmin}'
        elif self.xyz_tiles_radioButton.isChecked():
            url_with_params = 'type=xyz&url=' + urllib.parse.unquote(self.xyz_tiles_lineEdit.text()) + f'{zmax}{zmin}'
            if self.mEditName_2.text() == "":
                self.mEditName_2.setText("XYZ Tile")

        if self.mEditName_2.text() == "":
            self.mEditName_2.setText(self.url_comboBox.currentText())

        layer = QgsRasterLayer(url_with_params, self.mEditName_2.text(), 'wms')
        if not layer.isValid():
            logger.error("Failed to load layer!")
            QMessageBox.critical(self,
                                 "Add Layer",
                                 "Layer format is not recognised as a supported file format",
                                 QMessageBox.Close)
            self.msglog.logMessage("Failed to load layer " + layer.name(), "LoadingLayer", 1)
        else:
            self.proj.addMapLayer(layer)
            self.accept()


def parse_layers(file_paths) -> list:
    """
    parse one or multiples layers of string file_paths
    :param file_paths: String containing all the file paths to add to the project
    :type file_paths: str
    :return: list of layers
    """
    files = file_paths.split("\" \"")
    # format when selecting multiple files is: "file1" "file2" "file3" ... we need the '"' out of the strings
    if len(files) > 1:
        files[0] = files[0][1:]
        files[len(files) - 1] = files[len(files) - 1][:-1]

    return files


def add_layers_to_project(project, canvas, files, name="") -> None:
    """
    Loads a list of layer files to the project (only vector and raster type layers)
    :param project: Project to load layers to
    :type project: QgsProject
    :param canvas: Canvas to zoom
    :type canvas: QgsMapCanvas
    :param files: List of paths to all the layer files to load
    :type files: list
    :param name: Name of the layer, will only have effect if only one layer is given
    :type name: str
    """

    # check if an open project exists
    if project.fileName() == "":
        QMessageBox.warning(None, "No open project",
                            "You do not have any open project to add layers to. \n"
                            "Open a project or save the current one first.")
        return

    layers = []
    for file_name in files:
        # get name and extension of the file
        file_info = QFileInfo(file_name)
        if len(files) == 1 and name != "":
            base_name = name
        else:
            base_name = file_info.baseName()
        file_extension = file_info.suffix()

        # Depending on the extension, the file will need a different treatment
        if file_extension.lower() == "gpx":
            routes_layer = QgsVectorLayer(file_name + "?type=route", base_name + "_route", "gpx")
            tracks_layer = QgsVectorLayer(file_name + "?type=track", base_name + "_track", "gpx")
            waypoints_layer = QgsVectorLayer(file_name + "?type=waypoint", base_name + "_waypoints", "gpx")

            if routes_layer.dataProvider().featureCount() > 0:
                layers.append(routes_layer)
            if tracks_layer.dataProvider().featureCount() > 0:
                layers.append(tracks_layer)
            if waypoints_layer.dataProvider().featureCount() > 0:
                layers.append(waypoints_layer)

        elif file_extension.lower() == "tif" or file_extension.lower() == "tiff":
            raster_layer = QgsRasterLayer(file_name, base_name)
            layers.append(raster_layer)

        else:
            vector_layer = QgsVectorLayer(file_name, base_name)
            if len(vector_layer.dataProvider().subLayers()) > 1:
                sublayers_def = []
                for sub_layer in vector_layer.dataProvider().subLayers():
                    layer_def = QgsSublayersDialog.LayerDefinition()
                    layer_def.layerId = int(sub_layer.split("!!::!!")[0])
                    layer_def.layerName = sub_layer.split("!!::!!")[1]
                    layer_def.count = int(sub_layer.split("!!::!!")[2])
                    layer_def.type = sub_layer.split("!!::!!")[3]
                    sublayers_def.append(layer_def)

                sublayers = []
                sublayersdialog = QgsSublayersDialog(QgsSublayersDialog.Ogr, file_name)
                sublayersdialog.populateLayerTable(sublayers_def)
                if sublayersdialog.exec_():
                    for layer_definition in sublayersdialog.selection():
                        vector_layer = QgsVectorLayer(file_name + "|layername=" + layer_definition.layerName,
                                                      base_name + " " + layer_definition.layerName)
                        sublayers.append(vector_layer)

                    root = project.layerTreeRoot()
                    group = root.insertGroup(0, base_name)
                    for layer in reversed(sublayers):
                        map_layer = project.addMapLayer(layer)
                        layer_tree_layer = root.findLayer(map_layer)
                        clone = layer_tree_layer.clone()
                        group.insertChildNode(0, clone)
                        root.removeChildNode(layer_tree_layer)
            else:
                layers.append(vector_layer)

    invalid_sources = []
    added_layers = []
    for layer in layers:
        if layer.isValid():
            map_layer = project.addMapLayer(layer)
            added_layers.append(map_layer)
        else:
            invalid_sources.append(layer.source())

    if len(invalid_sources) > 0:
        QMessageBox.warning(None, "Invalid layers",
                            ("The following layers have an unrecognised file format and have not been loaded:\n\n"
                             + '\n'.join(['- {}'] * len(invalid_sources))).format(
                                *invalid_sources))

    zoom_to_layers_extent(canvas, added_layers)


def zoom_to_layers_extent(canvas, layers):
    """
    Zooms the canvas to the combined extent of all the layers
    :param canvas: Canvas to zoom
    :type canvas: QgsMapCanvas
    :param layers: List of layers that define the extent
    :type layers: list
    """
    if layers is not None and len(layers) > 0:
        extent = QgsRectangle()
        canvas_crs = canvas.mapSettings().destinationCrs()

        for layer in layers:
            layer_extent = QgsRectangle(layer.extent())
            layer_crs = layer.crs()

            # transform layer extent to current crs
            if canvas_crs.authid() != layer_crs.authid():
                trans = QgsCoordinateTransform(layer_crs, canvas_crs, QgsProject.instance().transformContext())
                layer_extent = trans.transformBoundingBox(layer_extent)

            extent.combineExtentWith(layer_extent)

        extent.scale(1.2)  # To not zoom all the way in. Better to locate where the zoom was made visually.

        canvas.setExtent(extent)
