# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Class to automatically generate a layer with the virtual cage to display in the map canvas
 The virtual cage is retrieved from the navigator ROS params.
 """

import logging
import math

from PyQt5.QtCore import QObject
from PyQt5.QtWidgets import QMessageBox
from cola2.utils.ned import NED
from qgis.core import (QgsProject,
                       QgsVectorLayer,
                       QgsFeature,
                       QgsGeometry,
                       QgsPointXY,
                       QgsSimpleLineSymbolLayer,
                       QgsSymbol,
                       QgsSingleSymbolRenderer)
from qgis.gui import QgsMapCanvas

from iquaview.src.mapsetup.nedorigindrawer import NEDOriginDrawer
from iquaview_lib.cola2api import cola2_interface
from iquaview_lib.cola2api.cola2_interface import send_trigger_service
from iquaview_lib.utils import busywidget
from iquaview_lib.vehicle.vehicleinfo import VehicleInfo
from iquaview_lib.definitions import ROSBRIDGE_SERVER_PORT

logger = logging.getLogger(__name__)


class VirtualCageDrawer(QObject):
    """
    Class to automatically generate a layer with the virtual cage to display in the map canvas
    The virtual cage is retrieved from the navigator ROS params.
    """

    def __init__(self,
                 proj: QgsProject,
                 vehicle_info: VehicleInfo,
                 canvas: QgsMapCanvas,
                 ned_origin: NEDOriginDrawer,
                 parent=None):
        """
        Init of the object VirtualCageDrawer
        :param proj: project to add ned origin layer into
        :type proj: QgsProject
        :param vehicle_info: vehicle info to extract the ned position from
        :type vehicle_info: VehicleInfo
        :param canvas: Map canvas to use
        :type canvas: QgsMapCanvas
        :param ned_origin: NED origin drawer
        :type ned_origin: NEDOriginDrawer
        """
        super().__init__(parent)
        self.proj = proj
        self.vehicle_info = vehicle_info
        self.canvas = canvas
        self.ned_origin = ned_origin
        self.ned_lat = 0
        self.ned_lon = 0
        self.cage_radius = 0
        self.virtual_cage_layer = None
        self.feature = None

        self.ned_origin.reload_ned_point_signal.connect(lambda:
                                                        self.update_virtual_cage(
                                                            self.get_radius_from_ros_param_server()))
        self.ned_origin.reload_ned_point_signal.connect(lambda:
                                                        self.update_param_value(
                                                            self.get_radius_from_ros_param_server()))

    def virtual_cage_renderer(self) -> QgsSingleSymbolRenderer:
        """
        Creates a renderer for the virtual cage layer
        :return: renderer for the virtual cage layer
        :rtype: QgsSingleSymbolRenderer
        """
        symbol = QgsSymbol.defaultSymbol(self.virtual_cage_layer.geometryType())
        style = {'capstyle': 'square',
                 'customdash': '5;2',
                 'customdash_map_unit_scale': '3x:0,0,0,0,0,0',
                 'customdash_unit': 'MM',
                 'draw_inside_polygon': '0',
                 'joinstyle': 'bevel',
                 'line_color': '0,0,0,255',
                 'line_style': 'dash',
                 'line_width': '0.36',
                 'line_width_unit': 'MM',
                 'offset': '0',
                 'offset_map_unit_scale': '3x:0,0,0,0,0,0',
                 'offset_unit': 'MM',
                 'ring_filter': '0',
                 'use_custom_dash': '0',
                 'width_map_unit_scale': '3x:0,0,0,0,0,0'}

        simple_line_symbol_marker = QgsSimpleLineSymbolLayer.create(style)
        symbol.deleteSymbolLayer(0)
        symbol.appendSymbolLayer(simple_line_symbol_marker)
        renderer = self.virtual_cage_layer.renderer()
        renderer.setSymbol(symbol)
        return renderer

    def get_radius_from_ros_param_server(self) -> int:
        """ get cage radius from ROS param server and update virtual cage"""
        vehicle_ip = self.vehicle_info.get_vehicle_ip()
        vehicle_namespace = self.vehicle_info.get_vehicle_namespace()
        cage_radius = int(float(cola2_interface.get_ros_param(vehicle_ip,
                                                              ROSBRIDGE_SERVER_PORT,
                                                              f'{vehicle_namespace}/virtual_cage/cage_radius')[
                                    'value']))
        return cage_radius

    def set_cage_radius(self, cage_radius: int) -> None:

        self.update_virtual_cage(cage_radius)
        self.update_param_value(cage_radius)

    def update_param_value(self, cage_radius) -> None:
        """ update cage radius param value"""
        try:
            cage_radius_from_vehicle = self.get_radius_from_ros_param_server()
        except Exception as e:
            self.update_virtual_cage(self.cage_radius)
            logger.error(f"Changing the virtual cage failed. {e}")
            QMessageBox.critical(self.parent(), "Changing the virtual cage failed.", "Connection Refused",
                                 QMessageBox.Close)
        else:
            if cage_radius != cage_radius_from_vehicle:
                ip = self.vehicle_info.get_vehicle_ip()
                port = ROSBRIDGE_SERVER_PORT
                vehicle_name = self.vehicle_info.get_vehicle_namespace()
                virtual_cage_name = f"{vehicle_name}/virtual_cage/cage_radius"
                try:
                    response = cola2_interface.set_ros_param(ip, port, virtual_cage_name, str(cage_radius))
                except ConnectionRefusedError:
                    logger.error("Changing the virtual cage failed")
                    QMessageBox.critical(self.parent(), "Changing the virtual cage failed.", "Connection Refused",
                                         QMessageBox.Close)
                except Exception as e:
                    logger.error("Changing the virtual cage failed")
                    QMessageBox.critical(self.parent(),
                                         "Changing the virtual cage failed.",
                                         e.args[0],
                                         QMessageBox.Close)
                else:
                    # Introduce a delay between changing parameters and resetting navigation
                    bw = busywidget.BusyWidget(title="Updating virtual cage...")
                    bw.on_start()
                    bw.exec_()

                    response = send_trigger_service(ip,
                                                    port,
                                                    vehicle_name + "/virtual_cage/reload_params")

                    if not response['values']['success']:
                        QMessageBox.critical(self.parent(),
                                             "Reload params failed",
                                             response['values']['message'],
                                             QMessageBox.Close)

                    self.cage_radius = cage_radius

            self.cage_radius = cage_radius_from_vehicle

    def update_virtual_cage(self, cage_radius) -> None:
        """
        Updates virtual cage layer with the data from ned origin and virtual cage from auv
        Adds the virtual cage layer if it's not already in the project.
        """
        self.feature = QgsFeature()
        segments = 500
        pts = []
        ned_origin_lat = 0
        ned_origin_lon = 0

        if self.virtual_cage_layer is None or len(self.proj.mapLayersByName("Virtual Cage")) == 0:
            self.virtual_cage_layer = QgsVectorLayer("Polygon", "Virtual Cage", "memory")
            self.virtual_cage_layer.setRenderer(self.virtual_cage_renderer())
            self.virtual_cage_layer.setCustomProperty("virtual_cage", "Virtual Cage")
            self.virtual_cage_layer.setOpacity(0.5)
        self.ned_lon, self.ned_lat = self.ned_origin.get_ned_origin()
        ned = NED(self.ned_lat, self.ned_lon, 0.0)

        for i in range(segments):
            theta = i * (2.0 * math.pi / segments)
            latlonh = ned.ned2geodetic([ned_origin_lon + cage_radius * math.cos(theta),
                                        ned_origin_lat + cage_radius * math.sin(theta),
                                        0])
            p = QgsPointXY(latlonh[1], latlonh[0])
            pts.append(p)

        if len(self.proj.mapLayersByName("Virtual Cage")) == 0:
            self.feature.setGeometry(QgsGeometry.fromPolygonXY([pts]))
            self.virtual_cage_layer.startEditing()
            self.virtual_cage_layer.dataProvider().addFeatures([self.feature])
            self.virtual_cage_layer.commitChanges()
            self.proj.addMapLayer(self.virtual_cage_layer, True)

        else:
            self.virtual_cage_layer.startEditing()
            self.feature.setGeometry(QgsGeometry.fromPolygonXY([pts]))
            feature_it = self.virtual_cage_layer.getFeatures()
            feature = next(feature_it)
            self.virtual_cage_layer.dataProvider().deleteFeatures([feature.id()])
            self.virtual_cage_layer.dataProvider().addFeatures([self.feature])
            self.virtual_cage_layer.commitChanges()

        self.virtual_cage_layer.triggerRepaint()

        self.ned_origin.refresh_ned_origin()
