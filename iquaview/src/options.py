# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

""" This file contains dialog to configure app.config parameters. """

import logging

from PyQt5.QtCore import Qt, QSize, pyqtSignal
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QDialog, QDialogButtonBox

from iquaview.src.connection.dataoutputmanager import DataOutputManager
from iquaview.src.generaloptions import GeneralOptionsWidget
from iquaview.src.plugins.pluginmanager import PluginManager
from iquaview.src.styles import StylesWidget
from iquaview.src.tools.web_server.webserver import WebServer
from iquaview.src.ui.ui_options import Ui_OptionsDlg

logger = logging.getLogger(__name__)

GENERAL = 0
DATA_OUTPUT_CONNECTION = 1
STYLES = 2
WEB_SERVER = 3

ICON_SIZE = 32


class OptionsDlg(QDialog, Ui_OptionsDlg):
    """
     Dialog to configure app.config parameters.
    """
    accepted_signal = pyqtSignal()

    def __init__(self,
                 config,
                 vehicle_info,
                 vehicle_data,
                 canvas,
                 mission_ctrl,
                 north_arrow,
                 scale_bar,
                 web_server: WebServer,
                 plugin_manager: PluginManager,
                 parent=None):
        """
        Constructor
        :param config: iquaview configuration
        :type config: Config
        :param vehicle_info: Information about AUV
        :type vehicle_info:  VehicleInfo
        :param vehicle_data: data received from the AUV
        :type vehicle_data: VehicleData
        :param canvas: canvas where repaint events will be triggered
        :type canvas: QgsMapCanvas
        :param mission_ctrl: mission controller to get the missions from
        :type mission_ctrl: MissionController
        :param north_arrow: Canvas item to change color
        :type north_arrow: NorthArrow
        :param scale_bar: Canvas item to change color
        :type scale_bar: ScaleBar
        :param web_server: web server to start/stop
        :type web_server: WebServer
        :param plugin_manager: plugin manager:
        :type plugin_manager: PluginManager
        """
        super().__init__(parent)
        self.setupUi(self)
        self.setWindowTitle("Options")
        self.config = config
        self.vehicle_info = vehicle_info
        self.vehicle_data = vehicle_data
        self.canvas = canvas
        self.mission_ctrl = mission_ctrl
        self.north_arrow = north_arrow
        self.scale_bar = scale_bar
        self.plugin_manager = plugin_manager

        # icons
        general_icon = QIcon(":/resources/mActionOptions.svg")
        styles_icon = QIcon(":/resources/mActionStyles.svg")
        data_output_icon = QIcon(":/resources/uploadPosition.svg")
        web_server_icon = QIcon(":/resources/web_server_icon.svg")

        # set icons
        self.mOptionsListWidget.item(GENERAL).setIcon(general_icon)
        self.mOptionsListWidget.item(STYLES).setIcon(styles_icon)
        self.mOptionsListWidget.item(DATA_OUTPUT_CONNECTION).setIcon(data_output_icon)
        self.mOptionsListWidget.item(WEB_SERVER).setIcon(web_server_icon)

        self.mOptionsListWidget.setIconSize(QSize(ICON_SIZE, ICON_SIZE))

        self.general_options_widget = GeneralOptionsWidget(self.config, self)
        self.styles_widget = StylesWidget(self.config, self.canvas, self.mission_ctrl, self.north_arrow,
                                          self.scale_bar)
        self.data_output_manager_widget = DataOutputManager(self.config, self.vehicle_data, self.plugin_manager)
        self.web_server_widget = web_server

        self.mOptionsStackedWidget.addWidget(self.general_options_widget)
        self.mOptionsStackedWidget.addWidget(self.data_output_manager_widget)
        self.mOptionsStackedWidget.addWidget(self.styles_widget)
        self.mOptionsStackedWidget.addWidget(self.web_server_widget)

        self.mOptionsListWidget.currentRowChanged.connect(self.set_current_page)

        self.buttonBox.button(QDialogButtonBox.Save).clicked.connect(self.on_accept)
        self.buttonBox.button(QDialogButtonBox.Close).clicked.connect(self.on_close)

    def set_current_page(self, item):
        """
        Change item in Stacked widget
        :param item: position of the page
        :type item: int
        """
        self.mOptionsListWidget.currentRowChanged.disconnect(self.set_current_page)
        self.mOptionsStackedWidget.setCurrentIndex(item)
        self.mOptionsListWidget.setCurrentRow(item)
        self.mOptionsListWidget.currentRowChanged.connect(self.set_current_page)

    def update_options(self, force_update=False):
        """Update and reload menu options"""
        if self.mOptionsStackedWidget.currentIndex() == GENERAL or force_update:
            self.general_options_widget.reload()

        if self.mOptionsStackedWidget.currentIndex() == STYLES or force_update:
            self.styles_widget.reload()

        if self.mOptionsStackedWidget.currentIndex() == DATA_OUTPUT_CONNECTION or force_update:
            self.data_output_manager_widget.reload()

        if self.mOptionsStackedWidget.currentIndex() == WEB_SERVER or force_update:
            self.web_server_widget.reload()

    def keyPressEvent(self, event) -> None:
        """ Override event handler keyPressEvent"""
        if event.key() == Qt.Key_Escape:
            self.on_close()
            super().keyPressEvent(event)

    def closeEvent(self, event):
        """ Override of the closeEvent method. Calls on close to restore changed colors """
        self.on_close()
        super().closeEvent(event)

    def on_accept(self):
        """On click 'Save as Defaults' accept"""
        if self.mOptionsStackedWidget.currentIndex() == GENERAL:
            self.general_options_widget.on_accept()

        elif self.mOptionsStackedWidget.currentIndex() == STYLES:
            self.styles_widget.save_as_defaults()

        elif self.mOptionsStackedWidget.currentIndex() == DATA_OUTPUT_CONNECTION:
            self.data_output_manager_widget.on_accept()

        elif self.mOptionsStackedWidget.currentIndex() == WEB_SERVER:
            self.web_server_widget.on_accept()

        self.accepted_signal.emit()

    def on_close(self):
        """On click 'Close' restore colors"""
        self.data_output_manager_widget.on_close()
        self.styles_widget.on_close()
        self.update_options(force_update=True)
        self.reject()
