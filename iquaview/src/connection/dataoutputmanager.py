# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Class to export auv and usbl position by serial or ethernet
"""
import logging

from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QWidget, QVBoxLayout

from iquaview.src.connection.dataoutputwidget import DataOutputWidget
from iquaview.src.plugins.pluginmanager import PluginManager
from iquaview.src.ui.ui_dataoutputmanager import Ui_DataOutputManagerWidget
from iquaview_lib.config import Config
from iquaview_lib.utils.ordered_set import OrderedSet
from iquaview_lib.vehicle.vehicledata import VehicleData

logger = logging.getLogger(__name__)


class DataOutputManager(QWidget, Ui_DataOutputManagerWidget):
    refresh_signal = pyqtSignal()

    def __init__(self, config: Config, vehicle_data: VehicleData, plugin_manager: PluginManager):
        super().__init__()
        self.setupUi(self)
        self.config = config
        self.vehicle_data = vehicle_data
        self.plugin_manager = plugin_manager
        self.connections = OrderedSet()
        self.temp_connections = OrderedSet()
        self.connections_ids = set()
        self.count = 1
        self.scrollAreaWidgetContents.setLayout(QVBoxLayout())

        self.new_connection_toolButton.clicked.connect(self.new_connection)

        self.load()

    def new_connection(self):
        """ Add new connection"""
        while f"connection_{self.count}" in self.connections_ids:
            self.count += 1
        identifier = f"connection_{self.count}"
        self.count += 1
        self.add_connection(identifier, identifier)

    def add_connection(self, identifier, connection_name):
        """
        Create and add connection to layout and temp connections
        :param identifier: connection identifier
        :type identifier: str
        :param connection_name: connection name
        :type connection_name: str
        """
        connection = DataOutputWidget(identifier,
                                      connection_name,
                                      self.config,
                                      self.vehicle_data,
                                      self.plugin_manager)
        connection.remove_connection_signal.connect(self.remove_from_temp_connections)
        self.temp_connections.add(connection)
        self.connections_ids.add(identifier)
        self.scrollAreaWidgetContents.layout().addWidget(connection)

    def remove_from_temp_connections(self):
        """Remove connections from temp_connections set"""
        self.temp_connections.remove(self.sender())

    def remove_connection(self, widget):
        """ Remove a connection"""
        widget.disconnect_widget(reconnect=False)
        if widget in self.temp_connections:
            self.temp_connections.remove(widget)
        layout = self.scrollAreaWidgetContents.layout()
        for i in reversed(range(layout.count())):
            if layout.itemAt(i).widget() == widget:
                widget_to_remove = layout.takeAt(i).widget()
                # remove it from the layout list
                layout.removeWidget(widget_to_remove)
                widget_to_remove.hide()
                # remove it
                widget.setParent(None)
                break

    def clear(self):
        """ Clear all connections from layout and disconnect all"""
        self.disconnect_widget(reconnect=False)
        self.connections_ids.clear()
        self.temp_connections.clear()
        self.connections.clear()

        layout = self.scrollAreaWidgetContents.layout()
        for i in reversed(range(layout.count())):
            widget_to_remove = layout.takeAt(i).widget()
            # remove it from the layout list
            layout.removeWidget(widget_to_remove)
            # remove it
            widget_to_remove.setParent(None)

    def load(self):
        """ Load connections from config"""
        self.clear()
        if (self.config.settings.get('data_output_connection')
                and isinstance(self.config.settings.get('data_output_connection'), dict)):
            for key, item in self.config.settings.get('data_output_connection').items():
                if item.get('connection_name') is not None:
                    name = item['connection_name']
                else:
                    name = key
                self.add_connection(key, name)

        self.reload()
        self.connect()

    def reload(self):
        """ Reload connections"""
        for connection in self.temp_connections:
            self.connections.add(connection)
            connection.reload()

    def connect(self):
        """ start connexions"""
        for connection in self.connections:
            connection.connect()

    def disconnect_widget(self, reconnect=False):
        """ Disconnect widgets"""
        for connection in self.connections:
            connection.disconnect_widget(reconnect)

    def valid_connections(self):
        """ Check if the connections has valid data"""
        is_valid = True
        for connection in self.connections:
            if not connection.is_valid_data():
                is_valid = False
                break
        return is_valid

    def on_accept(self):
        """ On accept, saves connections to config"""
        if not self.valid_connections():
            return False

        # remove previous connections
        if self.config.settings.get('data_output_connection'):
            self.config.settings['data_output_connection'] = {}
            self.config.save()

        # if removed from connections, disconnect
        for old_connection in self.connections:
            if old_connection not in self.temp_connections:
                old_connection.disconnect_widget(reconnect=False)
                self.remove_connection(old_connection)
        # add new connections
        self.connections = OrderedSet()
        for connection in self.temp_connections:
            connection.on_accept()
            self.connections.add(connection)
        self.reload()
        self.connect()

        return True

    def on_close(self):
        """restore state"""

        # remove temp connections not saved
        for temp_connection in self.temp_connections:
            if temp_connection not in self.connections:
                self.remove_connection(temp_connection)
        # show hidden connections
        self.temp_connections = OrderedSet()
        for connection in self.connections:
            if connection not in self.temp_connections:
                connection.show()
            self.temp_connections.add(connection)
