# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.


"""
Dialog to Add/Edit a process
"""

import logging

from PyQt5.QtWidgets import QDialog
from iquaview.src.ui.ui_edit_process import Ui_EditProcessDialog

logger = logging.getLogger(__name__)


class EditProcessDialog(QDialog, Ui_EditProcessDialog):
    def __init__(self, title="", process="", parent=None):
        """
        Constructor
        """
        super().__init__(parent)
        self.setupUi(self)
        self.title_lineEdit.setText(title)
        self.process_lineEdit.setText(process)
