# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Widget to control the processes that are launched/terminated inside the vehicle.
"""

import logging
from typing import Union

from PyQt5.QtCore import pyqtSignal, QThreadPool, QSize, QPoint
from PyQt5.QtGui import QPixmap, QIcon
from PyQt5.QtWidgets import (QMessageBox,
                             QDialogButtonBox,
                             QWidget,
                             QWidgetAction,
                             QHBoxLayout,
                             QLabel, QSizePolicy,
                             QAction,
                             QMenu)

from iquaview.src.connection.cola2status import Cola2Status
from iquaview.src.connection.connectionclient import ConnectionClient
from iquaview.src.connection.editprocessdialog import EditProcessDialog
from iquaview.src.ui.ui_auvprocesses import Ui_AUVProcessesWidget
from iquaview_lib.config import Config
from iquaview_lib.connection.ipdialog import IPDialog
from iquaview_lib.connection.ippingchecker import IPPingChecker
from iquaview_lib.utils.workerthread import Worker
from iquaview_lib.vehicle.vehicledata import VehicleData
from iquaview_lib.vehicle.vehicleinfo import VehicleInfo

logger = logging.getLogger(__name__)


class AUVProcessesWidget(QWidget, Ui_AUVProcessesWidget):
    """ Widget to control the processes that are launched/terminated inside the vehicle. """
    processchanged = pyqtSignal(str)
    refresh_processes_signal = pyqtSignal(list)

    def __init__(self, config: Config, vehicle_info: VehicleInfo, vehicle_data: VehicleData, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.config = config
        self.vehicle_info = vehicle_info
        self.vehicle_data = vehicle_data
        self.threadpool = QThreadPool()
        self.ip_dialog: Union[IPDialog, None] = None
        self.opened_dialog = {}

        self.master_antenna_ip_label = QLabel(self)
        self.master_antenna_indicator_label = QLabel(self)
        self.edit_master_antenna_ip_action = QAction("Edit IP...", self)

        self.master_antenna_label.setStyleSheet('font:italic; color:red')

        ip = self.config.settings.get('master_antenna_ip', '192.168.113.73')
        self.master_antenna_ip_label.setText(ip)
        self.master_antenna_ip_label.setStyleSheet('font:italic; color:red')

        size_policy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.master_antenna_indicator_label.setSizePolicy(size_policy)
        self.master_antenna_indicator_label.setMinimumSize(QSize(12, 12))
        self.master_antenna_indicator_label.setMaximumSize(QSize(12, 12))
        self.master_antenna_indicator_label.setText("")
        self.master_antenna_indicator_label.setPixmap(QPixmap(":/resources/red_led.svg"))
        self.master_antenna_indicator_label.setScaledContents(True)
        self.master_antenna_indicator_label.setObjectName("state_indicator")

        self.master_antenna_indicator_widget = QWidget(self)
        self.master_antenna_indicator_widget.setLayout(QHBoxLayout())
        self.master_antenna_indicator_widget.layout().addWidget(self.master_antenna_indicator_label)
        self.master_antenna_indicator_widget.layout().addWidget(self.master_antenna_ip_label)

        self.master_antenna_indicator_action = QWidgetAction(self)
        self.master_antenna_indicator_action.setDefaultWidget(self.master_antenna_indicator_widget)
        self.master_antenna_indicator_action.setIcon(QIcon(":/resources/red_led.svg"))
        self.master_antenna_indicator_action.setIconText(ip)
        self.master_antenna_indicator_action.setText(ip)

        self.master_antenna_toolButton.addActions([self.master_antenna_indicator_action,
                                                   self.edit_master_antenna_ip_action])
        self.master_antenna_toolButton.setDefaultAction(self.master_antenna_indicator_action)

        self.processes_menu = QMenu("Processes")

        add_action = self.processes_menu.addAction("Add...")
        add_action.triggered.connect(self.add_process)
        self.processes_menu.addSeparator()

        vehicle_launch_dict = self.vehicle_data.get_vehicle_launch_dict()
        for item in vehicle_launch_dict:
            self.create_menu(item)

        self.master_antenna_ip_checker = IPPingChecker(ip)
        self.master_antenna_ip_checker.is_connected_signal.connect(self.set_master_antenna_state)
        self.cc = ConnectionClient(self.vehicle_info.get_vehicle_ip(), self.vehicle_info.get_vehicle_port())
        self.cc.connection_failure.connect(self.connection_failed)
        self.cc.connection_ok.connect(self.connection_ok)
        self.cc.update_list.connect(lambda: self._to_thread(self.set_processes_items))
        self.refresh_processes_signal.connect(self.refresh_processes)
        self.edit_master_antenna_ip_action.triggered.connect(self.edit_ip)
        self.processes_toolButton.clicked.connect(self.show_menu)
        self.vehicle_info_changed()

        self.cola2status = Cola2Status(self.config, self.vehicle_info, self.cola2_label, self.cola2_indicator)

    def vehicle_info_changed(self) -> None:
        logger.info(
            f"Changing iquaview server connection to: "
            f"{self.vehicle_info.get_vehicle_ip()}:{self.vehicle_info.get_vehicle_port()}"
        )
        self.cc.ip = self.vehicle_info.get_vehicle_ip()
        self.cc.port = self.vehicle_info.get_vehicle_port()
        self.cc.disconnect_object()
        self.cc.reconnect_signal.emit()

        self.cc.start_cc_thread()
        self.connected = False

        self.update_connection_indicator()

    def create_menu(self, name: str):
        """
        Create a new menu inside processes menu
        :param name: name of the menu
        :type name: str
        """
        proc = self.processes_menu.addMenu(name)
        proc.setIcon(QIcon(":/resources/red_led.svg"))
        start_action = proc.addAction("Start")
        start_action.triggered.connect(self.send)
        terminate_action = proc.addAction("Terminate")
        terminate_action.triggered.connect(self.terminate)
        proc.addSeparator()
        edit_action = proc.addAction("Edit...")
        edit_action.triggered.connect(self.edit_process)
        remove_action = proc.addAction("Remove")
        remove_action.triggered.connect(self.remove_process)

    def edit_ip(self):
        """Edit the remote IP Address of the master antenna."""
        if self.ip_dialog is None:
            self.ip_dialog = IPDialog(self)
            self.ip_dialog.ip_lineEdit.setText(self.config.settings.get('master_antenna_ip', '192.168.113.73'))

            self.ip_dialog.rejected.connect(self.reject_ip)
            self.ip_dialog.accepted.connect(lambda: self.set_master_antenna_ip(self.ip_dialog.ip_lineEdit.text()))

            self.ip_dialog.show()

        self.ip_dialog.activateWindow()

    def reject_ip(self):
        self.ip_dialog = None

    def set_master_antenna_ip(self, ip):
        """IP address accepted"""
        self.ip_dialog = None
        self.config.settings['master_antenna_ip'] = ip
        self.master_antenna_ip_label.setText(self.config.settings.get('master_antenna_ip', '192.168.113.73'))
        self.master_antenna_indicator_action.setText(self.config.settings.get('master_antenna_ip', '192.168.113.73'))
        self.config.save()
        self.master_antenna_ip_checker.ip_address = self.config.settings.get('master_antenna_ip')

    def _to_thread(self, method):
        """ Send method to thread"""
        worker = Worker(method)
        self.threadpool.start(worker)

    def set_processes_items(self):
        """ Set processes to menu"""
        try:
            processes = list()
            processes_str = ""
            vehicle_launch_dict = self.vehicle_data.get_vehicle_launch_dict()
            for item in vehicle_launch_dict:
                processes.append(item)
                processes_str += vehicle_launch_dict[item]

                if len(processes) != len(vehicle_launch_dict):
                    processes_str += ","

            logger.info(vehicle_launch_dict)
            list_result = self.cc.send("list " + processes_str)
            data = self.cc.send("on")
            logger.info(data)
            processes = data[:-1].split(',')
            self.refresh_processes_signal.emit(processes)
        except Exception as e:
            logger.error(f"Error on get process items: {e}")

    def refresh_processes(self, processes):
        """
        Refresh processes to menu
        :param processes: list of processes
        :type processes: list
        """
        for action in self.processes_menu.actions():
            if action.menu() is not None and action.menu().title() != "Add...":
                process = self.vehicle_data.get_vehicle_launch_dict()[action.text()]
                if process in processes:
                    action.menu().setIcon(QIcon(":/resources/green_led.svg"))
                    for sub_action in action.menu().actions():
                        if sub_action.text() == "Start":
                            sub_action.setEnabled(False)
                        elif sub_action.text() == "Terminate":
                            sub_action.setEnabled(True)
                else:
                    action.menu().setIcon(QIcon(":/resources/red_led.svg"))
                    for sub_action in action.menu().actions():
                        if sub_action.text() == "Start":
                            sub_action.setEnabled(True)
                        elif sub_action.text() == "Terminate":
                            sub_action.setEnabled(False)

    def connect(self):
        """Update connection with current values """
        self.cc.ip = self.vehicle_info.get_vehicle_ip()
        self.cc.port = self.vehicle_info.get_vehicle_port()

    def disconnect_auvprocesses(self):
        """ Disconnect auvprocesses"""
        self.master_antenna_ip_checker.stop_ping()
        self.cc.close()
        self.connected = False
        self.update_connection_indicator()
        self.cola2status.disconnect_cola2status()

    def send(self):
        """ Send start process to auv"""
        sender: QAction = self.sender()
        menu = sender.parent()
        action = menu.menuAction()
        logger.info(f"Send: {action.text()}")

        process = self.vehicle_data.get_vehicle_launch_dict()[action.text()]
        self.cc.send(process)
        self.processchanged.emit(f"Process {action.text()} has been started")
        self._to_thread(self.set_processes_items)

    def terminate(self):
        """ send terminate process to auv"""
        sender: QAction = self.sender()
        menu = sender.parent()
        action = menu.menuAction()
        logger.info(f"Terminate: {action.text()}")
        process = self.vehicle_data.get_vehicle_launch_dict()[action.text()]
        self.cc.send("terminate " + process)
        self.processchanged.emit(f"Process {action.text()} has been terminated")
        self._to_thread(self.set_processes_items)

    def edit_process(self):
        """
        Edit current process
        """
        sender: QAction = self.sender()
        menu = sender.parent()
        action = menu.menuAction()
        if not self.opened_dialog.get(action.text(), False):
            process = self.vehicle_data.get_vehicle_launch_dict()[action.text()]
            edit_process_dialog = EditProcessDialog(action.text(), process, self.parent())
            edit_process_dialog.show()
            edit_process_dialog.buttonBox.button(QDialogButtonBox.Ok).clicked.connect(
                lambda: self.update_process(edit_process_dialog, action)
            )
            edit_process_dialog.accepted.connect(lambda: self.dialog_closed(action.text()))
            edit_process_dialog.rejected.connect(lambda: self.dialog_closed(action.text()))
            self.opened_dialog[action.text()] = True

    def remove_process(self):
        """
        Remove process from menu and xml
        """
        sender: QAction = self.sender()
        menu = sender.parent()
        action = menu.menuAction()
        launch_dict = self.vehicle_data.get_vehicle_launch_dict()
        del launch_dict[action.text()]
        # save and accept
        self.vehicle_data.save()
        self.processes_menu.removeAction(action)

    def dialog_closed(self, text: str):
        """
        Set current opened dialog state as Fasle
        :param text: name of the current process
        :type text: str
        """
        self.opened_dialog[text] = False

    def update_process(self, edit_process_dialog: EditProcessDialog, action: Union[QAction, None]):
        """
        Updates the process with the new names
        :param edit_process_dialog: dialogue to enter the name of the title and the process
        :param action: current menu action (None if is adding new process)
        :type action: Union[QAction, None]
        """
        title = edit_process_dialog.title_lineEdit.text()
        process = edit_process_dialog.process_lineEdit.text()
        launch_dict = self.vehicle_data.get_vehicle_launch_dict()
        if title == "" or process == "":
            QMessageBox.warning(None,
                                "Process not saved",
                                "There cannot be any empty fields.")
        elif (action is None or title != action.text()) and title in launch_dict:
            QMessageBox.warning(None,
                                "Process not saved",
                                "More than one process has the same title, "
                                "try to give a different title to differentiate the processes.")
        else:
            if action is not None:
                # remove previous launch
                del launch_dict[action.text()]
            # add new launch
            launch_dict[title] = process
            if action is None:
                # create new one
                self.create_menu(title)
            else:
                # new name
                action.setText(title)
            # update state
            self.set_processes_items()
            # save and accept
            self.vehicle_data.save()
            edit_process_dialog.accept()

    def add_process(self):
        """
        Add new process. A dialog is created for entering the values
        """
        dialog_name = "add new process"
        if not self.opened_dialog.get(dialog_name, False):
            add_process_dialog = EditProcessDialog(parent=self.parent())
            add_process_dialog.setWindowTitle("Add Process")
            add_process_dialog.show()
            add_process_dialog.buttonBox.button(QDialogButtonBox.Ok).clicked.connect(
                lambda: self.update_process(add_process_dialog, None)
            )
            add_process_dialog.accepted.connect(lambda: self.dialog_closed(dialog_name))
            add_process_dialog.rejected.connect(lambda: self.dialog_closed(dialog_name))
            self.opened_dialog[dialog_name] = True

    def show_menu(self):
        """ Show processes menu"""
        self.processes_menu.popup(self.processes_toolButton.mapToGlobal(QPoint(0, 0)))

    def connection_failed(self):
        """ disable items if connection failed"""
        self.connected = False
        self.update_connection_indicator()
        self.connect()

    def connection_ok(self):
        """ enable items if connection with auv is established"""
        self.connected = True
        self.update_connection_indicator()
        self._to_thread(self.set_processes_items)

    def update_connection_indicator(self):
        """ update indicators according to the connection with AUV"""
        if not self.connected:
            self.connection_indicator.setPixmap(QPixmap(":/resources/red_led.svg"))
            self.connection_label.setStyleSheet('font:italic; color:red')
        else:
            self.connection_indicator.setPixmap(QPixmap(":/resources/green_led.svg"))
            self.connection_label.setStyleSheet('font:italic; color:green')

    def set_master_antenna_state(self, state):
        """ set master antenna state"""
        if state:
            self.master_antenna_indicator_label.setPixmap(QPixmap(":/resources/green_led.svg"))
            self.master_antenna_label.setStyleSheet('font:italic; color:green')
            self.master_antenna_indicator_action.setIcon(QIcon(":/resources/green_led.svg"))
            self.master_antenna_ip_label.setStyleSheet('font:italic; color:green')
        else:
            self.master_antenna_indicator_label.setPixmap(QPixmap(":/resources/red_led.svg"))
            self.master_antenna_label.setStyleSheet('font:italic; color:red')
            self.master_antenna_indicator_action.setIcon(QIcon(":/resources/red_led.svg"))
            self.master_antenna_ip_label.setStyleSheet('font:italic; color:red')
