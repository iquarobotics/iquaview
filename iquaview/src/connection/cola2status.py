# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Class to periodically check cola2 state.
"""
import logging
import os

from PyQt5.QtCore import pyqtSignal, QObject, QTimer, QThreadPool
from PyQt5.QtGui import QPixmap

from iquaview_lib.cola2api.cola2_interface import SubscribeToTopic
from iquaview_lib.definitions import ROSBRIDGE_SERVER_PORT
from iquaview_lib.utils.workerthread import Worker
from iquaview_lib.xmlconfighandler.vehicledatahandler import VehicleDataHandler

logger = logging.getLogger(__name__)


class Cola2Status(QObject):
    """ Class to periodically check cola2 state. """
    reconnect_signal = pyqtSignal()
    cola2_connected = pyqtSignal(bool)
    update_data_signal = pyqtSignal()
    stop_timer_signal = pyqtSignal()

    def __init__(self, config, vehicleinfo, label, indicator):
        super().__init__()
        self.vehicleinfo = vehicleinfo
        self.timer = QTimer()
        self.timer.timeout.connect(self.update_data)
        self.subscribed = False
        self.watchdog_timer_topic = None
        self.t_time = 0
        self.auv_config_xml = os.path.abspath(os.path.expanduser(config.settings['last_auv_config_xml']))
        self.cola2_label = label
        self.cola2_indicator = indicator
        self.status = False
        self.set_indicator_off()

        self.reconnect_signal.connect(self.reconnect_cola2_thread)
        self.update_data_signal.connect(self.start_timer)
        self.cola2_connected.connect(self.set_indicator_state)
        self.stop_timer_signal.connect(self.stop_timer)

        self.t_time_text = None
        vd_handler = VehicleDataHandler(self.auv_config_xml)
        # get vehicle data topics
        xml_vehicle_data_topics = vd_handler.read_topics()
        # back compatibility
        for topic in xml_vehicle_data_topics:
            if topic.get('id') == "watchdog timer":
                self.t_time_text = topic.text

        self.threadpool = QThreadPool()
        self.start_cola2_thread()

    def reconnect_cola2_thread(self):
        """ Reconnect cola2 thread """
        if not self.subscribed:
            QTimer.singleShot(5000, self.start_cola2_thread)

    def start_cola2_thread(self):
        """ Start cola2 thread """
        worker = Worker(self.update_cola2_status)  # Any other args, kwargs are passed to the run function
        worker.signals.finished.connect(self.reconnect_cola2_thread)
        self.threadpool.start(worker)

    def update_cola2_status(self):
        """ connect to AUV"""
        try:
            self.subscribed = True
            self.watchdog_timer_topic = SubscribeToTopic(self.vehicleinfo.get_vehicle_ip(),
                                                         ROSBRIDGE_SERVER_PORT,
                                                         self.vehicleinfo.get_vehicle_namespace() + self.t_time_text)
            self.watchdog_timer_topic.subscribe()
            self.update_data_signal.emit()
        except OSError as oe:
            logger.error(f"Disconnecting cola2 status {oe}")
            self.disconnect_cola2status()
        except:
            logger.error("Disconnecting cola2 status")
            self.disconnect_cola2status()

    def start_timer(self):
        """ Start timer"""
        if not self.timer.isActive():
            self.timer.start(1000)

    def update_data(self):
        """ Refresh data from auv and update indicators
            Green: connected
            Red: disconnected
        """
        try:
            if self.subscribed:
                if self.watchdog_timer_topic:
                    data = self.watchdog_timer_topic.get_data()
                    if data and data['valid_data'] == 'new_data':
                        if self.t_time != data['data']:
                            self.t_time = data['data']
                            self.cola2_connected.emit(True)
                    else:
                        if data and data['valid_data'] == 'disconnected':
                            self.disconnect_cola2status()
                            self.reconnect_signal.emit()
                        else:
                            self.cola2_connected.emit(False)

                else:
                    self.cola2_connected.emit(False)
        except:
            self.disconnect_cola2status()
            self.reconnect_signal.emit()

    def set_indicator_state(self, is_on: bool):
        """ Set indicator state"""
        if is_on:
            self.set_indicator_on()
        else:
            self.set_indicator_off()

    def set_indicator_on(self):
        """ Set indicator on"""
        self.status = True
        self.cola2_label.setStyleSheet('font:italic; color:green')
        self.cola2_indicator.setPixmap(QPixmap(":/resources/green_led.svg"))

    def set_indicator_off(self):
        """ Set indicator off"""
        self.status = False
        self.cola2_label.setStyleSheet('font:italic; color:red')
        self.cola2_indicator.setPixmap(QPixmap(":/resources/red_led.svg"))

    def is_subscribed(self):
        """
        :return: return subscribed status
        :rtype: bool
        """
        return self.subscribed

    def is_connected(self):
        """
        Return True if cola2 is on, otherwise returns False.
        :return: True if COLA2 is on, otherwsie returns False.
        """
        return self.status

    def stop_timer(self):
        """ Stop timer to update data"""
        self.timer.stop()

    def disconnect_cola2status(self):
        """ disconnect to AUV"""
        self.subscribed = False
        self.cola2_connected.emit(False)

        if self.watchdog_timer_topic:
            # close subscription
            self.watchdog_timer_topic.close()

        self.stop_timer_signal.emit()

        self.watchdog_timer_topic = None

        self.threadpool.clear()
