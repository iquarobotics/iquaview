# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Class to export auv and usbl position by serial or ethernet
"""

import logging

from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QWidget, QInputDialog, QLineEdit

from iquaview.src.connection.dataoutputconnection import DataOutputConnection
from iquaview.src.plugins.pluginmanager import PluginManager
from iquaview.src.ui.ui_dataoutputconnection import Ui_DataOutputConnectionWidget
from iquaview_lib.config import Config
from iquaview_lib.utils.textvalidator import (line_edit_validator, validate_line_edits,
                                              get_ip_validator, get_port_validator)
from iquaview_lib.vehicle.vehicledata import VehicleData

logger = logging.getLogger(__name__)


class DataOutputWidget(QWidget, Ui_DataOutputConnectionWidget):
    """
    Class to export auv and usbl position by serial or ethernet
    """
    remove_connection_signal = pyqtSignal()

    def __init__(self, identifier,
                 connection_name,
                 config: Config,
                 vehicle_data: VehicleData,
                 plugin_manager: PluginManager):
        super().__init__()
        self.setupUi(self)
        self.new_updates = False
        self.last_auv_time = None
        self.last_usbl_time = None
        self.connection = DataOutputConnection(identifier,
                                               connection_name,
                                               config,
                                               vehicle_data,
                                               plugin_manager)

        self.groupBox.setTitle(connection_name)

        # validators
        self.ip_lineEdit.setValidator(get_ip_validator())
        self.ggaport_lineEdit.setValidator(get_port_validator())

        self.ip_lineEdit.textChanged.connect(lambda: line_edit_validator(self.ip_lineEdit))
        self.ggaport_lineEdit.textChanged.connect(lambda: line_edit_validator(self.ggaport_lineEdit))
        self.remove_toolButton.clicked.connect(self.remove_connection)
        self.remove_connection_signal.connect(self.hide)

        self.ethernet_radioButton.toggled.connect(self.change_enable)
        self.serial_radioButton.toggled.connect(self.change_enable)

        self.change_enable()

    def get_identifier(self) -> str:
        """
        Returns the identifier of the connection
        :return: identifier
        :rtype: str
        """
        return self.connection.id

    def mouseDoubleClickEvent(self, event) -> None:
        """
        Override mouseDoubleClickEvent to add rename file input dialog
        :param event: mouse event
        :type event: QMouseEvent
        """
        new_name = QInputDialog.getText(self, "Rename file", "New connection name:",
                                        QLineEdit.Normal, self.groupBox.title())
        if new_name[1]:
            self.groupBox.setTitle(new_name[0])
            self.connection.name = new_name[0]

    def updated(self):
        index_protocol = self.protocol_comboBox.findText(self.connection.socket_server.output_protocol)
        index_type = self.position_comboBox.findText(self.connection.output_type)
        index_baudrate = self.baudrate_comboBox.findText(self.connection.socket_server.output_serial_baudrate)
        if (self.connection.socket_server.output_port != self.ggaport_lineEdit.text()
                or self.connection.socket_server.output_ip != self.ip_lineEdit.text()
                or self.connection.socket_server.output_serial_device != self.device_lineEdit.text()
                or index_protocol != self.protocol_comboBox.currentIndex()
                or index_type != self.position_comboBox.currentIndex()
                or index_baudrate != self.baudrate_comboBox.currentIndex()
                or self.connection.socket_server.output_is_serial != self.serial_radioButton.isChecked()):
            return True
        return False

    def reload(self):
        """ Reload values on widget"""
        self.connection.reload()
        self.ggaport_lineEdit.setText(self.connection.socket_server.output_port)
        self.ip_lineEdit.setText(self.connection.socket_server.output_ip)
        self.device_lineEdit.setText(self.connection.socket_server.output_serial_device)
        index_protocol = self.protocol_comboBox.findText(self.connection.socket_server.output_protocol)
        if index_protocol != -1:
            self.protocol_comboBox.setCurrentIndex(index_protocol)

        index_type = self.position_comboBox.findText(self.connection.output_type)
        if index_type != -1:
            self.position_comboBox.setCurrentIndex(index_type)

        index_baudrate = self.baudrate_comboBox.findText(self.connection.socket_server.output_serial_baudrate)
        if index_baudrate != -1:
            self.baudrate_comboBox.setCurrentIndex(index_baudrate)

        if self.connection.socket_server.output_is_serial:
            self.serial_radioButton.setChecked(True)
        else:
            self.ethernet_radioButton.setChecked(True)
        self.change_enable(self.connection.socket_server.output_is_serial)
        self.show()

    def connect(self):
        if self.new_updates:
            self.disconnect_widget(False)
        self.connection.start_connection_thread()

    def disconnect_widget(self, reconnect: bool = True):
        """
        Sets the widget as disconnected
        :param reconnect: if reconnect is False, disconnect reconnect signal
        :type reconnect: bool
        """
        self.connection.disconnect_widget(reconnect)

    def remove_connection(self):
        """
        Send signal to remove connection
        """
        self.remove_connection_signal.emit()

    def change_enable(self, enable=False):
        """
        Change widgets enable state
        """
        ethernet_state = self.ethernet_radioButton.isChecked()
        serial_state = self.serial_radioButton.isChecked()

        self.protocol_label.setEnabled(ethernet_state)
        self.protocol_comboBox.setEnabled(ethernet_state)
        self.ip_label.setEnabled(ethernet_state)
        self.ip_lineEdit.setEnabled(ethernet_state)
        self.ggaport_lineEdit.setEnabled(ethernet_state)
        self.ggaport_label.setEnabled(ethernet_state)

        self.device_label.setEnabled(serial_state)
        self.device_lineEdit.setEnabled(serial_state)
        self.baudrate_label.setEnabled(serial_state)
        self.baudrate_comboBox.setEnabled(serial_state)

    def is_valid_data(self):
        """ Check if gps data is valid"""
        return (self.serial_radioButton.isChecked()
                or (self.ethernet_radioButton.isChecked()
                    and validate_line_edits([self.ip_lineEdit, self.ggaport_lineEdit])))

    def on_accept(self):
        """
        Check if data is valid and set new values
        """
        if self.is_valid_data():
            self.new_updates = self.updated()
            self.connection.output_type = self.position_comboBox.currentText()
            self.connection.socket_server.output_is_serial = self.serial_radioButton.isChecked()
            self.connection.socket_server.output_ip = self.ip_lineEdit.text()
            self.connection.socket_server.output_port = self.ggaport_lineEdit.text()
            self.connection.socket_server.output_protocol = self.protocol_comboBox.currentText()
            self.connection.socket_server.output_serial_device = self.device_lineEdit.text()
            self.connection.socket_server.output_serial_baudrate = self.baudrate_comboBox.currentText()

            self.connection.on_accept()
