# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Class to export auv and usbl position by serial or ethernet
"""

import logging
import time
from typing import Optional, Tuple
from copy import copy
from math import degrees

from PyQt5.QtCore import pyqtSignal, QTimer, QThreadPool, QObject
from PyQt5.QtGui import QValidator

from iquaview.src.plugins.pluginmanager import PluginManager
from iquaview_lib.config import Config
from iquaview_lib.connection.socket_server import SocketServer
from iquaview_lib.utils.textvalidator import validate_ip, validate_port
from iquaview_lib.utils.workerthread import Worker
from iquaview_lib.vehicle.vehicledata import VehicleData, is_navigation_new, is_gps_new
from iquaview_lib.cola2api.basic_message import BasicMessage

logger = logging.getLogger(__name__)


class DataOutputConnection(QObject):
    """
    Class to export auv and usbl position by serial or ethernet
    """
    start_timer_signal = pyqtSignal()
    stop_timer_signal = pyqtSignal()

    def __init__(self, identifier,
                 connection_name,
                 config: Config,
                 vehicle_data: VehicleData,
                 plugin_manager: PluginManager):
        super().__init__()

        self.id = identifier
        self.name = connection_name
        self.last_auv_time: float = 0.0
        self.last_usbl_time: float = 0.0
        self.config = config
        self.vehicle_data = vehicle_data
        self.plugin_manager = plugin_manager
        self.output_type = None
        self.socket_server = SocketServer()

        self.threadpool = QThreadPool()

        self.timer = QTimer()
        self.timer.timeout.connect(self.start_refresh_thread)
        self.start_timer_signal.connect(self.start_timer)
        self.stop_timer_signal.connect(self.stop_timer)

    def get_identifier(self) -> str:
        """
        Returns the identifier of the connection
        :return: identifier
        :rtype: str
        """
        return self.id

    def reload(self):
        """ Reload values on widget"""
        if (self.config.settings.get('data_output_connection')
                and self.config.settings['data_output_connection'].get(self.id)):
            connection_config = self.config.settings['data_output_connection'][self.id]
            self.output_type = connection_config['output_type']
            self.socket_server.output_is_serial = connection_config['output_is_serial']
            self.socket_server.output_protocol = connection_config['ethernet_protocol']
            try:
                self.socket_server.output_ip = connection_config['ethernet_ip']
            except:
                self.socket_server.output_ip = ""
            try:
                self.socket_server.output_port = str(connection_config['ethernet_port'])
            except:
                self.socket_server.output_port = ""
            self.socket_server.output_serial_device = connection_config['serial_device']
            self.socket_server.output_serial_baudrate = str(connection_config['serial_baudrate'])

    def start_timer(self):
        """ Start timer"""
        if not self.timer.isActive():
            self.timer.start(1000)

    def start_connection_thread(self):
        """ Start cola2 thread """
        self.socket_server.start_connection_thread()
        self.start_timer_signal.emit()

    def stop_timer(self):
        """ Stop timer to update data"""
        self.timer.stop()

    def disconnect_widget(self, reconnect: bool = True):
        """
        Sets the widget as disconnected
        :param reconnect: if reconnect is False, disconnect reconnect signal
        :type reconnect: bool
        """
        self.socket_server.close(reconnect)
        self.stop_timer_signal.emit()

    def start_refresh_thread(self):
        """ start refresh thread if connected """
        if self.socket_server.is_connected():
            worker = Worker(self.refresh)  # Any other args, kwargs are passed to the run function
            self.threadpool.start(worker)

    def refresh(self):
        """ Refresh data. Get position and send position"""
        try:
            gga_message, hdt_message = self.get_position()
            if gga_message is not None:
                self.socket_server.send_data(gga_message)

            if hdt_message is not None:
                self.socket_server.send_data(hdt_message)
        except Exception as e:
            logger.error(e)

    def get_position(self) -> Tuple[Optional[str], Optional[str]]:
        """
        Get data from auv or usbl and create a gga and hdt messages
        """
        gga_message = None
        hdt_message = None

        evologics_usbl = self.plugin_manager.get_plugin("iquaview_evologics_usbl")
        sonardyne_usbl = self.plugin_manager.get_plugin("iquaview_sonardyne_usbl")

        if (self.socket_server.is_connected()
            and ((not self.socket_server.output_is_serial
                  and validate_ip(self.socket_server.output_ip) == QValidator.Acceptable
                  and validate_port(self.socket_server.output_port) == QValidator.Acceptable)
                 or self.socket_server.output_is_serial)):
            # output vehicle position
            if self.output_type == "AUV Position":
                # vehicle position through wifi
                if self.vehicle_data.is_subscribed():
                    data = self.vehicle_data.get_nav_sts()
                    gps_data = self.vehicle_data.get_gps()
                    if is_navigation_new(data) or is_gps_new(gps_data):
                        if is_navigation_new(data):
                            lat = float(data['global_position']['latitude'])
                            lon = float(data['global_position']['longitude'])
                            heading = float(data['orientation']['yaw'])
                            depth = float(data['position']['depth'])
                        else:
                            lat = float(gps_data['latitude'])
                            lon = float(gps_data['longitude'])
                            heading = 0
                            depth = 0

                        gga_message = self.socket_server.gen_gga(
                            time.gmtime(), lat, "N", lon, "E", 6, 0, 1.5, -depth, 0, 0, 0)
                        hdt_message = self.socket_server.gen_hdt(degrees(heading))

                # vehicle position through evologics usbl
                elif (evologics_usbl is not None
                        and evologics_usbl.get_usbl_entity() is not None
                        and evologics_usbl.get_usbl_entity().is_connected()):
                    auv_data: BasicMessage = evologics_usbl.get_usbl_entity().driver.get_auv_data()
                    if auv_data.is_valid() and auv_data.time != self.last_auv_time:
                        self.last_auv_time = float(auv_data.time)  # ensure that is a copy
                        gga_message = self.socket_server.gen_gga(
                            time.gmtime(), auv_data.latitude, "N", auv_data.longitude, "E", 6, 0, 1.5, -auv_data.depth,
                            0, 0, 0)
                        hdt_message = self.socket_server.gen_hdt(degrees(auv_data.heading_accuracy))
                # vehicle position through sonardyne usbl
                elif (sonardyne_usbl is not None
                        and sonardyne_usbl.get_usbl_entity() is not None
                        and sonardyne_usbl.get_usbl_entity().is_connected()):
                    auv_data: BasicMessage = sonardyne_usbl.get_usbl_entity().driver.get_auv_navigation()
                    if auv_data.is_valid() and auv_data.time != self.last_auv_time:
                        self.last_auv_time = float(auv_data.time)  # ensure that is a copy
                        gga_message = self.socket_server.gen_gga(
                            time.gmtime(), auv_data.latitude, "N", auv_data.longitude, "E", 6, 0, 1.5, -auv_data.depth,
                            0, 0, 0)
                        hdt_message = self.socket_server.gen_hdt(degrees(auv_data.heading_accuracy))
            # output usbl measurement
            else:
                # usbl measurement through evologics usbl
                if (evologics_usbl is not None
                        and evologics_usbl.get_usbl_entity() is not None
                        and evologics_usbl.get_usbl_entity().is_connected()):
                    usbl_data: BasicMessage = evologics_usbl.get_usbl_entity().driver.get_data()
                    if usbl_data.is_valid() and usbl_data.time != self.last_usbl_time:
                        self.last_usbl_time = float(usbl_data.time)  # ensure that is a copy
                        gga_message = self.socket_server.gen_gga(
                            time.gmtime(), usbl_data.latitude, "N", usbl_data.longitude, "E", 6, 0, 1.5,
                            -usbl_data.depth, 0, 0, 0)
                # usbl measurement through sonardyne usbl
                if (sonardyne_usbl is not None
                        and sonardyne_usbl.get_usbl_entity() is not None
                        and sonardyne_usbl.get_usbl_entity().is_connected()):
                    usbl_data = sonardyne_usbl.get_usbl_entity().driver.get_usbl_measurement()
                    if usbl_data is not None:
                        if usbl_data.time != 0.0 and usbl_data.time != self.last_usbl_time:
                            self.last_usbl_time = float(usbl_data.time)  # ensure that is a copy
                            gga_message = self.socket_server.gen_gga(
                                time.gmtime(), usbl_data.latitude, "N", usbl_data.longitude, "E", 6, 0, 1.5,
                                -usbl_data.depth, 0, 0, 0)
        return gga_message, hdt_message

    def change_enable(self, enable=False):
        """
        Change widgets enable state
        """
        ethernet_state = self.ethernet_radioButton.isChecked()
        serial_state = self.serial_radioButton.isChecked()

        self.protocol_label.setEnabled(ethernet_state)
        self.protocol_comboBox.setEnabled(ethernet_state)
        self.ip_label.setEnabled(ethernet_state)
        self.ip_lineEdit.setEnabled(ethernet_state)
        self.ggaport_lineEdit.setEnabled(ethernet_state)
        self.ggaport_label.setEnabled(ethernet_state)

        self.device_label.setEnabled(serial_state)
        self.device_lineEdit.setEnabled(serial_state)
        self.baudrate_label.setEnabled(serial_state)
        self.baudrate_comboBox.setEnabled(serial_state)

    def on_accept(self):
        """
        Check if data is valid and set new values
        """
        if not self.config.settings['data_output_connection']:
            self.config.settings['data_output_connection'] = {}

        if not self.config.settings['data_output_connection'].get(self.id):
            self.config.settings['data_output_connection'][self.id] = {}

        self.config.settings['data_output_connection'][self.id]['connection_name'] = copy(self.name)
        self.config.settings['data_output_connection'][self.id]['output_type'] = copy(self.output_type)
        self.config.settings['data_output_connection'][self.id]['output_is_serial'] = copy(
            self.socket_server.output_is_serial)
        self.config.settings['data_output_connection'][self.id]['ethernet_protocol'] = copy(
            self.socket_server.output_protocol)
        if validate_ip(self.socket_server.output_ip) == QValidator.Acceptable:
            self.config.settings['data_output_connection'][self.id]['ethernet_ip'] = copy(
                self.socket_server.output_ip)
        if validate_port(self.socket_server.output_port) == QValidator.Acceptable:
            self.config.settings['data_output_connection'][self.id]['ethernet_port'] = copy(
                int(self.socket_server.output_port))
        self.config.settings['data_output_connection'][self.id]['serial_device'] = copy(
            self.socket_server.output_serial_device)
        self.config.settings['data_output_connection'][self.id]['serial_baudrate'] = copy(int(
            self.socket_server.output_serial_baudrate))

        self.config.save()
