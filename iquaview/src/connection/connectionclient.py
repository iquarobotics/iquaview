# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Class to handle the connection to the init server of the vehicle
"""

import logging
import socket

from PyQt5.QtCore import pyqtSignal, QObject, QTimer, QThreadPool

from iquaview_lib.utils.workerthread import Worker

logger = logging.getLogger(__name__)


class ConnectionClient(QObject):
    connection_failure = pyqtSignal()
    connection_ok = pyqtSignal()
    reconnect_signal = pyqtSignal()
    update_list = pyqtSignal()
    stop_timer_signal = pyqtSignal()

    def __init__(self, ip, port):
        super().__init__()
        self.ip = ip
        self.port = port
        self.sock = None
        self.threadpool = QThreadPool()
        self.timer = QTimer()
        self.timer.timeout.connect(self.watchdog)
        self.reconnect_signal.connect(self.reconnect_cc_thread)
        self.connection_ok.connect(self.start_watchdog_timer)
        self.connection_failure.connect(self.disconnect_object)
        self.stop_timer_signal.connect(self.stop_timer)
        self.connected = False
        self.closing = False

    @property
    def ip(self):
        """
        :return: return ip address
        :rtype: str
        """
        return self.__ip

    @ip.setter
    def ip(self, ip):
        """
        Set ip address
        :param ip: new ip address
        :type ip: str
        """
        self.__ip = ip

    @property
    def port(self):
        """
        :return: return port
        :rtype: str
        """
        return self.__port

    @port.setter
    def port(self, port):
        """
        set port
        :param port: new port
        :type port: str
        """
        self.__port = port

    def reconnect_cc_thread(self):
        if not self.connected:
            QTimer.singleShot(5000, self.start_cc_thread)

    def start_cc_thread(self):
        """ Start connectionClient thread """
        if not self.closing:
            worker = Worker(self.do_connection)  # Any other args, kwargs are passed to the run function
            worker.signals.finished.connect(self.reconnect_cc_thread)
            self.threadpool.start(worker)

    def do_connection(self):
        """ create socket and connect"""
        try:
            try:
                # Create a TCP/IP socket
                self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                # Set timeout
                self.sock.settimeout(3.0)
                server_address = (self.__ip, int(self.__port))
                self.sock.connect(server_address)
                self.connected = True
                # send first message to check if connection is ok
                if not self.closing:
                    self.send("ping")
                    self.connection_ok.emit()
                    logger.info(f"Connected {self.__ip} {self.__port}")

            except socket.error as e:
                self.connected = False
                self.connection_failure.emit()

            except OSError as e:
                self.connected = False
                self.connection_failure.emit()

            except Exception as e:
                self.connected = False
                self.connection_failure.emit()
        except RuntimeError as e:
            logger.error(f"{str(e.args[0])}")

    def start_watchdog_timer(self):
        """ start watchdog timer"""
        if not self.timer.isActive():
            self.timer.start(5000)

    def watchdog(self):
        """ Start connectionClient thread """
        worker = Worker(self.watchdog_thread)
        self.threadpool.start(worker)

    def watchdog_thread(self):
        """ start watchdog thread"""
        try:
            data = self.send("watchdog")
            if data == "watchdogupdate":
                self.update_list.emit()
            elif data != "watchdogack":
                self.connection_failure.emit()
                self.reconnect_signal.emit()

        except socket.timeout:
            logger.error("timeout error")
            self.connection_failure.emit()
            self.reconnect_signal.emit()
        except socket.error:
            logger.error("socket error occured")
            self.connection_failure.emit()
            self.reconnect_signal.emit()

        except Exception as e:
            logger.error(f"{e} fail to receive ack from the server")
            self.connection_failure.emit()
            self.reconnect_signal.emit()

    def send(self, message):
        """
        send message to AUV
        :param message: message to send
        :type message: str
        :return: received data
        :rtype: str
        """
        self.sock.sendall(message.encode())
        data = self.sock.recv(4096).decode()
        return data

    def stop_timer(self):
        """ Stop timer to update data"""
        self.timer.stop()

    def disconnect_object(self):
        """ Disconnect and stop timer,thread and close socket"""
        self.connected = False
        self.stop_timer_signal.emit()
        if self.sock:
            self.sock.close()

        self.threadpool.clear()

    def close(self):
        """ Disconnect object and set closing state to True"""
        self.disconnect_object()
        self.closing = True
