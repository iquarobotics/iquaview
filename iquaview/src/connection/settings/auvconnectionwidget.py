# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
"""
 Widget to define the ip and port to establish the connection to the AUV
"""
import logging
import os
import os.path

from PyQt5.QtCore import QFileInfo
from PyQt5.QtGui import QValidator, QIcon
from PyQt5.QtWidgets import QWidget, QFileDialog

from iquaview.src.ui.ui_auv_connection import Ui_AUVConnectionWidget
from iquaview_lib.utils.textvalidator import (validate_ip,
                                              validate_port,
                                              get_color,
                                              get_ip_validator,
                                              get_int_validator)

logger = logging.getLogger(__name__)


def resolve_filename(filename):
    """
    Resolve filename
    :param filename: file to resolve
    :type filename: str
    :return: filename resolved
    :rtype: str
    """
    return os.path.abspath(os.path.expanduser(filename))


class AUVConnectionWidget(QWidget, Ui_AUVConnectionWidget):
    """ Widget to define the ip and port to establish the connection to the AUV """

    def __init__(self, ip=None, port=None, path=None, user=None, auv_config=None, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.ip = ip
        self.port = port
        self.path = path
        self.user = user
        self.auv_config = auv_config

        # set validators
        self.ip_auv_text.setValidator(get_ip_validator())
        self.port_auv_text.setValidator(get_int_validator(0, 65535))

        # set signals
        self.ip_auv_text.textChanged.connect(self.ip_validator)
        self.port_auv_text.textChanged.connect(self.port_validator)
        self.auv_config_pushButton.clicked.connect(self.load_auv_config)

    @staticmethod
    def get_icon() -> QIcon:
        """
        Get icon
        :return: widget icon
        :rtype: QIcon
        """
        return QIcon(":/resources/mActionConnectionSettings.svg")

    @property
    def ip(self):
        """
        :return: return ip address
        :rtype: str
        """
        self.__ip = self.ip_auv_text.text()
        return self.__ip

    @ip.setter
    def ip(self, ip):
        """
        Set ip address
        :param ip: new ip address
        :type ip: str
        """
        self.__ip = ip
        self.ip_auv_text.setText(ip)

    @property
    def port(self):
        """
        :return: return port
        :rtype: int
        """
        self.__port = self.port_auv_text.text()
        return self.__port

    @port.setter
    def port(self, port):
        """
        Set port
        :param port: new port
        :type port: str
        """
        self.__port = port
        self.port_auv_text.setText(str(port))

    @property
    def auv_config(self):
        """
        :return: return auv_config
        :rtype: str
        """
        self.__auv_config = self.auv_config_lineEdit.text()
        return self.__auv_config

    @auv_config.setter
    def auv_config(self, last_auv_config):
        """
        set auv_config
        :param last_auv_config: new auv config
        :type last_auv_config: str
        """
        self.__auv_config = last_auv_config
        self.auv_config_lineEdit.setText(last_auv_config)

    @property
    def path(self):
        """
        :return: return path
        :rtype: str
        """
        self.__path = self.path_rm_lineEdit.text()
        return self.__path

    @path.setter
    def path(self, new_path):
        """
        set path
        :param new_path: new auv config
        :type new_path: str
        """
        self.__path = new_path
        self.path_rm_lineEdit.setText(new_path)

    @property
    def user(self):
        """
        :return: return user
        :rtype: str
        """
        self.__user = self.user_rm_lineEdit.text()
        return self.__user

    @user.setter
    def user(self, new_user):
        """
        set user
        :param new_user: new user
        :type new_user: str
        """
        self.__user = new_user
        self.user_rm_lineEdit.setText(new_user)

    def ip_validator(self):
        """ Validate text(ip) of sender"""
        sender = self.sender()
        state = validate_ip(sender.text())
        color = get_color(state)
        sender.setStyleSheet(f'QLineEdit {{ background-color: {color} }}')

    def port_validator(self):
        """ Validate text(port) of sender"""
        sender = self.sender()
        state = validate_port(sender.text())
        color = get_color(state)
        sender.setStyleSheet(f'QLineEdit {{ background-color: {color} }}')

    def is_valid(self):
        """ check if parameters (IP and port) are valid"""
        return (validate_ip(self.ip_auv_text.text()) == QValidator.Acceptable and
                validate_port(self.port_auv_text.text()) == QValidator.Acceptable and
                os.path.exists(resolve_filename(self.auv_config_lineEdit.text())))

    def load_auv_config(self):
        """Open dialog to load auv config"""
        configuration_filename, __ = QFileDialog.getOpenFileName(self, 'AUV configuration',
                                                                 os.path.dirname(resolve_filename(
                                                                     self.auv_config_lineEdit.text())
                                                                 ),
                                                                 "AUV configuration(*.xml) ;; All files (*.*)")
        if configuration_filename:
            file_info = QFileInfo(configuration_filename)
            filename = file_info.canonicalFilePath()

            self.auv_config_lineEdit.setText(str(filename))
