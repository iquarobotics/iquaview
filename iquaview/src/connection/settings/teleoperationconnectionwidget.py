# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Widget to setup the device for the joystick connection
"""
import sys

from PyQt5.QtWidgets import QWidget
from PyQt5.QtGui import QIcon
from iquaview.src.ui.ui_teleop_connection import Ui_TeleopConnectionWidget

if sys.version_info >= (3, 8):
    from typing import TypedDict
else:
    from typing_extensions import TypedDict


class Config(TypedDict):
    """ Configuration dictionary for the teleoperation connection widget """
    joystick_device: str


class TeleoperationConnectionWidget(QWidget, Ui_TeleopConnectionWidget):
    """ Widget to setup the device for the joystick connection """

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.joystickdevice = None

    @staticmethod
    def get_icon() -> QIcon:
        """
        Get widget icon
        :return: widget icon
        :rtype: QIcon
        """
        return QIcon(":/resources/joystick.png")

    @property
    def joystickdevice(self):
        """
        :return: joystick device
        :rtype: str
        """
        self.__joystickdevice = self.joystick_device_text.text()
        return self.__joystickdevice

    @joystickdevice.setter
    def joystickdevice(self, joystickdevice):
        """
        Set joystick device
        :param joystickdevice: joystick device adress
        :type joystickdevice: str
        """
        self.__joystickdevice = joystickdevice
        self.joystick_device_text.setText(joystickdevice)

    def from_dict(self, config: Config):
        """ Set the widget values from a configuration dictionary """
        self.joystickdevice = config.get("joystick_device")

    def is_valid(self):
        """is valid"""
        return self.joystickdevice != ""
