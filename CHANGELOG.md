# Changelog
All notable changes to IQUAview are documented in this file.

## [24.1.6] - 2024-11-29
### Added
* Show chrony status if "chrony tracking" topic is in the configuration xml.

### Fixed
* Resolved an issue where the profile tool was not deleted upon closing, leading to unnecessary profile processing.

### Changed
* Updated SVGs for the measurement tools.

## [24.1.5] - 2024-10-04
### Fixed
* Updated link to user manual on README.md.

## [24.1.4] - 2024-09-24
### Fixed
* An issue when editing multiple waypoints in altitude heave mode.

## [24.1.3] - 2024-03-01
### Changed
* Parallel rendering was activated to render layers in parallel.
* Updated jQuery to version 3.7.1.
* Updated socket.io to version 4.7.4.
* Updated Leaflet to version 1.9.4.

### Fixed
* An issue that did not allow the tiles to be displayed correctly in the web server.

## [24.1.2] - 2024-02-22
### Changed
* Use common session time definition.

### Fixed
* An issue when calling the acoustic reset timeout in the timeout notification dialog.

## [24.1.1] - 2024-02-07
### Fixed
* An issue that did not allow to execute the mission actions.

## [24.1.0] - 2024-01-30

### Added
* Profile Tool for plotting height profiles of bathymetry raster layers by drawing on the map or by selecting mission layers from the layer tree.
* A mission axis can be scaled independently of the other axis.
* The points can be moved simultaneously in the multiple point selection tool.
* Field to check the controlstation-AUV synchronization.
* An emergency beep will sound when there is a critical message in the vehicle message bar.
* Arrow to indicate the direction of the lawn mower in the template mission tool.
* New options(format, decimal precision, directional suffix, coordinate symbols) for the display of latitude and longitude. For computations the maximum precision will still be always used.
* Button to reset timeout in the timeout notification dialog.
* Highlight selected mission.
* Box to select/unselect all missions in the web server options menu.
* Option to disable position keeping and enable thrusters when sending a goto.

### Removed
* The mission can no longer be scaled by holding down the Ctrl button.
* The preview tracks button in the template mission tool.
* Moved MissionStatus to iquaview_lib with name VehicleStatus.

### Changed
* The template mission area is calculated at each parameter change.
* Moved 'Transfer and execute mission' action to 'Execute mission' menu.
* The log files are saved in *~/logs/iquaview/*.

### Fixed
* Minor bugs.

## [22.10.7] - 2023-10-26
### Fixed
* Can decode the missing status code bits.

## [22.10.6] - 2023-09-12
### Fixed
* An issue when adding a layer from a CSV file.

## [22.10.5] - 2023-06-22
### Fixed
* An issue that shows the mission process in the warning bar.

## [22.10.4] - 2023-06-19
### Fixed
* An issue that caused the multipoint editing tool to rotate with the map.
* The performance of the point selection tools has been improved.
* An issue that did not allow to see that there was an abort and surface if there was low battery charge/voltage.
* The goto marker remains on the screen even if the connection is lost.

## [22.10.3] - 2023-02-02
### Fixed
* An issue that did not allow the bags to be transferred.

## [22.10.2] - 2022-11-22
### Fixed
* An issue when moving the NED Origin to another position.

## [22.10.1] - 2022-11-09
### Fixed
* An issue that did not allow inserting more than one point with the landmark point tool.

## [22.10.0] - 2022-10-26
### Added
* New track widget that contains all entities that display tracks on the map and allows the configuration (connection, styles and visualization options) for each of them.
* Plugin manager to install/uninstall plugins.
* Range & Bearing widget to show distance and angles from a given entity to another.
* New options for tiled web maps layers: opensea map, emodnet and GMRT bathymetric grids and option to add any XYZ tiled map by providing the url.
* New mapping template for lawn mower missions, with extra waypoints to call services on transects and turns.
* Context menu to edit and delete waypoints from the waypoints table.
* Mission resize feature added to the Move tool.
* Copy-paste mission actions between waypoints (with Ctrl+C, Ctrl+V).
* Action to transfer and execute the mission from the layers panel.
* Virtual cage display and widget to easily edit the cage radius.
* Display of missions on the web server.
* Canvas lock on vehicle/usbl position for the web server.
* Visual indicator of connection to the master antenna.
* Option to enable the thrusters and disable keep position when asked at the start of a mission.
* Saving of the position and visibility of the vehicle widgets to be restored at the next session.
* Visibility state of the missions is stored in the project.
* Option to save unsaved missions when saving the project.
* Support for Sonardyne USBL.

### Changed
* Moved the connection settings and the configuration regarding the AUV, user and vehicle package inside the properties of the vehicle entity.
* Allow to call actions that do not depend on navigation regardless of the AUV monitoring being enabled or not.
* Thrusters action is now "Enable thrusters", so they are activated by checking the action and deactivated by unchecking it.
* When saving a mission as save as, the previous mission remains in the project.
* AUV processes widget has been reorganized and allows to edit/remove processes.
* Bags with .active extension are also allowed for transfer.
* The tool for setting the NED Origin allows now to enter the coordinates manually or select a point on the map.
* Improved usability, restyled some widgets and unified naming across different menus.
* Logging time in UTC.

### Fixed
* Changes due to new Ubuntu 20.04 packages.
* Time error on the IQUAview log file.
* Preserving the order of the mission layers when loading a project.
* Minor bugs.

## [20.10.8] - 2021-09-06
### Added
* Support for Iqua Strobe Lights.

## [20.10.7] - 2021-05-31
### Added
* Support for Norbit WBMS Multibeam.

## [20.10.6] - 2021-04-13
### Fixed
* CRS transformation when adding a new landmark.

## [20.10.5] - 2021-04-06
### Fixed
* Rendering problem when clicking on waypoints with the edit tool.

## [20.10.4] - 2021-03-26
### Added
* Support for KVH GEOFOG3D INS.

### Fixed
* Added support for Qgis version 3.16 to fix mission editing issue.

## [20.10.3] - 2021-03-17
### Fixed
* The new version of socket.io (3.1.3) is used on the client side of the web server to maintain compatibility.
* The gevent-websocket libraries are used to improve performance.
* Bug fix when some values are saved as default.

## [20.10.2] - 2021-03-04
### Fixed
* The message when editing the mission.
* The order in which icons are displayed on the canvas.

## [20.10.1] - 2021-01-15
### Fixed
* Bug fixed in the temporary data structure.

## [20.10.0] - 2020-10-23
### Added

* Support for S-57 (ENC) vector layers.
* Option to create layer groups.
* Widget to edit customization settings in options menu (no need to manually edit the XML from now on).
* Lock/unlock buttons in track widgets to keep the map centered on the track extent.
* Waypoint table view for summarizing mission waypoints.
* Integration of web server for displaying basic canvas view and AUV/USBL positions.
* Functionality to download bagfiles.
* New star template for missions.
* Display of message describing recovery action reason in message bar. 
* New plugin for FLIR Spinnaker Camera (upon request). 

### Changed

* GNSS position and heading are set separately.
* Each AUV parameter has an independent reload service associated in the configuration XML.
* Last canvas position and zoom is set on opening.

### Removed

* Removed backward compatibility with COLA2 releases prior to 201902.

### Fixed
* Minor bugs.

## [1.2.1] - 2020-8-26

* Added support for Qgis version 3.10.8

## [1.2.0] - 2019-10-26
### Added

* Method to check if all actions in a mission are present in the config XML.
* Multiple missions can be loaded at once.
* Vehicle roslaunch list added to XML configuration.
* Support to import CSV layers.
* Selector to change Project Coordinate Reference System (CRS).
* World map layer as default layer.
* Buttons to pause and resume a mission.
* Drag&Drop functionality to import missions and layers (vector and raster) to the project.
* SFTP client to get and transfer files from/to the AUV.
* Undo (ctrl+z) functionality when moving and rotating missions.
* Show coordinates of single landmark points by right clicking them in the layer legend.
* Now hovering the mouse over a mission or a vector layer displays the name of that layer in a tooltip.
* Button to move NED Origin.
* Menu to export positions through serial or ethernet.
### Changed
* Tracks remain on the canvas even if the connection is lost.
* Rotation key changed from Ctrl to Shift.
* Service calls are handled as Trigger services instead of Empty services.
* Re-styling of battery and thrusters widgets.
* Re-styling of options menu.
### Fixed
* Minor bugs

## [1.1.0] - 2019-02-26

* First Release version.

## [1.0.0] - 2017-07-30

* Initial version.
