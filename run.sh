#!/bin/bash

#source
. ~/.bashrc

if [ -n "$1" ]; then
    DEBUG=$1
fi

if [ -n "$2" ]; then
    VALUE=$2
fi

if [ -z  "$BASH_SOURCE" ]; then
    relative_path="$0"
else
    relative_path="$BASH_SOURCE"
fi

# Create dir if not existing
mkdir -p ~/.iquaview/

# Execute main and save output to log file
CURR_DIR="$( cd "$( dirname ${relative_path} )" ; pwd )"
python3 -u ${CURR_DIR}/iquaview/src/__main__.py ${DEBUG} ${VALUE}
